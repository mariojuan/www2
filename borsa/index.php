<?php
namespace webtortosa;

require_once("../config.cfg");
require_once(FOLDER_CONTROLLER . "/web_controller.php");

//se instancia al controlador
$controller = new web_controller();

/*
 * Paràmetres:
 * 1: llenguatge
 * 2: ENS de la Borsa (1: AJT)
 * 3: Pàgina de la paginació
 */

$controller->borsa($_GET['lang'], 1, $_GET['page']);
?>