<?php
namespace webtortosa;

require_once("../config.cfg");
require_once(FOLDER_CONTROLLER . "/web_controller.php");

//se instancia al controlador
$controller = new web_controller();

// Paràmetre tipus: ENS de la borsa de treball.
$controller->fitxa_ofertes_ocupacio($_GET['lang'], $_GET['id'], $_GET['tipus']);
?>