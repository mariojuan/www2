<?php
/**
 * Created by Ajuntament de Tortosa.
 * User: Mario Juan / Charlie
 * Date: 19/03/18
 * Time: 12:55
 */

namespace webtortosa;

session_start();
require_once (FOLDER_CONTROLLER."/functions_controller.php");
require_once (FOLDER_LIBS."/smarty/Smarty_web.class.php");
require_once (FOLDER_MODEL."/db.class.php");
require_once (FOLDER_MODEL."/logs.class.php");

class web_controller_votacio_telematica extends web_controller {
    public function backup_pressupostos_participatius($file, $path_file_disabled) {
        require_once(FOLDER_MODEL . "/pressupostos-participatius.class.php");
        $ppdb = new \webtortosa\pressupostos_participatius();

        $sufix = '_' . date('Y') . date('m') . date('d') . date('H');
        $this->desactiva($file);
        $ppdb->realitzaBackup($sufix);
        $this->activa($path_file_disabled);
    }
    public function desactiva($file_disabled) {
        $mensaje = "Manel-Carlos-Mario";
        if($archivo = fopen($file_disabled, "a"))
        {
            fwrite($file_disabled, date("d m Y H:m:s"). " ". $mensaje. "\n");
            fclose($file_disabled);
            echo "web desactivada<br>";
        }
    }
    public function activa($file_disabled) {
        unlink($file_disabled);
        echo "web activada<br>";
    }
    public function votacio_telematica_manteniment($input_lang) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");
        require_once (FOLDER_LANGUAGES."/participacio-ciutadana/votacio-telematica/".$lang."_votacio_telematica.cfg");


        $smarty->assign('url_ca','/participacio-ciutadana/votacio-telematica/');
        $smarty->assign('url_es','/participacio-ciutadana/votacio-telematica/?lang=es');

        //echo "El lang = ".$lang;
        $smarty->display("participacio-ciutadana/votacio-telematica/manteniment.tpl");
    }

    public function votacio_telematica_fora_termini($input_lang, $vars_post) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");        
        require_once (FOLDER_LANGUAGES."/participacio-ciutadana/votacio-telematica/".$lang."_votacio_telematica.cfg");
        

        $smarty->assign('url_ca','/participacio-ciutadana/votacio-telematica/');
        $smarty->assign('url_es','/participacio-ciutadana/votacio-telematica/?lang=es');

        //echo "El lang = ".$lang;
        $smarty->display("participacio-ciutadana/votacio-telematica/fora_termini.tpl");
    }

    public function votacio_telematica_inici($input_lang, $vars_post) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");        
        require_once (FOLDER_LANGUAGES."/participacio-ciutadana/votacio-telematica/".$lang."_votacio_telematica.cfg");
		

        $fecha_actual = strtotime(date("d-m-Y H:i:00",time()));
        //$fecha_actual = strtotime("28-04-2018 15:00:00");
        $fecha_inici = strtotime("28-04-2018 00:00:00");
        $fecha_fi = strtotime("11-05-2018 23:59:59");
          
        $en_termini = false;  
        if ( ($fecha_actual > $fecha_inici) && ($fecha_actual < $fecha_fi) ) {
            $en_termini = true;  
        } 
        $smarty->assign('en_termini',$en_termini);

        $smarty->assign('url_ca','/participacio-ciutadana/votacio-telematica/');
        $smarty->assign('url_es','/participacio-ciutadana/votacio-telematica/?lang=es');

		//echo "El lang = ".$lang;
        $smarty->display("participacio-ciutadana/votacio-telematica/inici.tpl");
        
    }

    /*
    public function votacio_telematica_ident($input_lang, $vars_post) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LIBS . '/recaptchalib.php');
        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");
        require_once (FOLDER_LANGUAGES."/participacio-ciutadana/votacio-telematica/".$lang."_votacio_telematica.cfg");
        require_once (FOLDER_MODEL . "/ciutat/ciutat.class.php");

        $fc = new \webtortosa\functions_controller();

        if($vars_post && $vars_post['FirstTime']!="1" && $_SERVER['HTTP_REFERER'] = 'http://www2.tortosa.cat/participacio-ciutadana/votacio-telematica/') {
            require_once (FOLDER_MODEL."/pressupostos-participatius.class.php");
            //Recepció de dades i se consulta al Cens.
            $params = array(
                'tdocument'                     => $vars_post['tdocument'] ? $fc->test_input($vars_post['tdocument']) : null,
                'dni_number'                    => $vars_post['dni_number'] ? $fc->test_input($vars_post['dni_number']) : null,
                'dni_letter'                    => $vars_post['dni_letter'] ? $fc->test_input($vars_post['dni_letter']) : null,
                'passaport'                     => $vars_post['option2_passport_number'] ? $fc->test_input($vars_post['option2_passport_number']) : null,
                'tr_letter1'                   => $vars_post['tr_letter_1'] ? $fc->test_input($vars_post['tr_letter_1']) : null,
                'tr_number'                     => $vars_post['tr_number'] ? $fc->test_input($vars_post['tr_number']) : null,
                'tr_letter2'                   => $vars_post['tr_letter_2'] ? $fc->test_input($vars_post['tr_letter_2']) : null,
                'proposta1'                     => $vars_post['proposta1'] ? $fc->test_input($vars_post['proposta1']) : null,
                'proposta2'                     => $vars_post['proposta2'] ? $fc->test_input($vars_post['proposta2']) : null,
                'proposta3'                     => $vars_post['proposta3'] ? $fc->test_input($vars_post['proposta3']) : null
                //'PIN'                           => $fc->test_input($vars_post['pin'])
            );

            $proposta1 = trim($fc->test_input($vars_post['proposta1']));
            $proposta2 = trim($fc->test_input($vars_post['proposta2']));
            $proposta3 = trim($fc->test_input($vars_post['proposta3']));

            $continua = true;


            //if (($proposta1 == '') && ($proposta2 == '') && ($proposta3 == '')) { 
            //    $message = "NO_HI_HA_PROPOSTA";
            //    $continua = false;               
            //}

            if ($continua) {

                if (($proposta1 != '') && (($proposta1 == $proposta2) || ($proposta1 == $proposta3))) { 
                    $message = "MATEIXA_PROPOSTA";
                    $fc->escribir_log_file(FOLDER_LOGS."/Vot_".date('d-m-Y').".log", 'VOT ERROR 1 ::: Dades: '.implode(' | ', $params));
                } else if (($proposta2 != '') && (($proposta2 == $proposta1) || ($proposta2 == $proposta3))) { 
                    $message = "MATEIXA_PROPOSTA";
                    $fc->escribir_log_file(FOLDER_LOGS."/Vot_".date('d-m-Y').".log", 'VOT ERROR 1 ::: Dades: '.implode(' | ', $params));
                } else if (($proposta3 != '') && (($proposta3 == $proposta1) || ($proposta3 == $proposta2))) { 
                    $message = "MATEIXA_PROPOSTA";
                    $fc->escribir_log_file(FOLDER_LOGS."/Vot_".date('d-m-Y').".log", 'VOT ERROR 1 ::: Dades: '.implode(' | ', $params));
                } else { 

                    $ppdb = new \webtortosa\pressupostos_participatius();
                    $lst_people = $ppdb->getRegistrat($params);

                    if(count($lst_people)==1) {
                        // LA CONSULTA getRegistrat JA COMPROVA QUE ESTEM REGISTRATS EN EL PROCÉS PARTICIPATIU QUE TOCA
                        $message = "OK!!";                        

                        if ($lst_people[0]->PIN != $fc->test_input($vars_post['pin'])) {
                             $message = "PIN_INCORRECTE";
                             $fc->escribir_log_file(FOLDER_LOGS."/Vot_".date('d-m-Y').".log", 'VOT ERROR 2 ::: id_cens: '.$lst_people[0]->ID_CENS);
                        } else if ($lst_people[0]->VALIDAT==0) { 
                             $message = "NO_VALIDAD";
                             $fc->escribir_log_file(FOLDER_LOGS."/Vot_".date('d-m-Y').".log", 'VOT ERROR 3 ::: id_cens: '.$lst_people[0]->ID_CENS);
                        } else if ($lst_people[0]->VOTACIO_EFECTUADA==1) { 
                             $message = "JA_HA_VOTAT";
                             $fc->escribir_log_file(FOLDER_LOGS."/Vot_".date('d-m-Y').".log", 'VOT ERROR 4 ::: id_cens: '.$lst_people[0]->ID_CENS);
                        } else { 
                            // POT VOTAR -> PIN CORRECTE, VALIDAT i NO HA VOTAT x INTERNET !!!
                            // OJO !!! POT HAVER VOTAT PRESENCIALMENT !!!

                            $haVotatCens = $ppdb->haVotatPPartCens($lst_people[0]->ID_CENS);

                           
                            if ($haVotatCens==true) { 
                                $message = "JA_HA_VOTAT_PRESENCIAL";
                                $fc->escribir_log_file(FOLDER_LOGS."/Vot_".date('d-m-Y').".log", 'VOT ERROR 5 ::: id_cens: '.$lst_people[0]->ID_CENS);
                            } else { 
                                // ARA SEGUR QUE NO HA VOTAT !!!

                                $idPapereta = $ppdb->getNextIdPapereta($lst_people[0]->ID_PROCESP);
                                $data = date('Y/m/d');
                                $hora = date('H:i:s');
                                //$hash1 = $fc->simple_encrypt($lst_people[0]->ID_CENS . "|" .$proposta1 . "|" .$lang);
                                //$hash2 = $fc->simple_encrypt($lst_people[0]->ID_CENS . "|" .$proposta2 . "|" .$lang);
                                //$hash3 = $fc->simple_encrypt($lst_people[0]->ID_CENS . "|" .$proposta3 . "|" .$lang);
                                $hash1 = $fc->simple_encrypt($proposta1 . "|" .$lang);
                                $hash2 = $fc->simple_encrypt($proposta2 . "|" .$lang);
                                $hash3 = $fc->simple_encrypt($proposta3 . "|" .$lang);

                                //echo "ID_PAPERETA = : ".$idPapereta . "<br>";
                                //echo $data ." - ". $hora . "<br>";
                                //echo "1 - ". $proposta1 ." - ". $hash1 . "<br>";
                                //echo "2 - ". $proposta2 ." - ". $hash2 . "<br>";
                                //echo "3 - ". $proposta3 ." - ". $hash3 . "<br>";

                                if ($proposta1 != '') { 
                                    $inserta1 = $ppdb->UpdateSumaVotProposta($lst_people[0]->ID_PROCESP, $proposta1);
                                }
                                if ($proposta2 != '') { 
                                    $inserta2 = $ppdb->UpdateSumaVotProposta($lst_people[0]->ID_PROCESP, $proposta2);
                                }
                                if ($proposta3 != '') {
                                    $inserta3 = $ppdb->UpdateSumaVotProposta($lst_people[0]->ID_PROCESP, $proposta3);
                                }

                                // INSERTEM VOTS
                                //$idVot1 = $ppdb->addVot($lst_people[0]->ID_PROCESP, $idPapereta, 1, $proposta1, $data, $hora, $hash1);
                                //$idVot2 = $ppdb->addVot($lst_people[0]->ID_PROCESP, $idPapereta, 2, $proposta2, $data, $hora, $hash2);
                                //$idVot3 = $ppdb->addVot($lst_people[0]->ID_PROCESP, $idPapereta, 3, $proposta3, $data, $hora, $hash3);
                                $idVot1 = $ppdb->addVotUrna($lst_people[0]->ID_PROCESP, $proposta1, $hash1);
                                $idVot2 = $ppdb->addVotUrna($lst_people[0]->ID_PROCESP, $proposta2, $hash2);
                                $idVot3 = $ppdb->addVotUrna($lst_people[0]->ID_PROCESP, $proposta3, $hash3);
                                $fc->escribir_log_file(FOLDER_LOGS."/Vot_".date('d-m-Y').".log", 'VOTACIO ::: id_cens: '.$lst_people[0]->ID_CENS);

                                // ACTUALITZEM CENS COM A VOTAT
                                $censActualitzat = $ppdb->UpdateVotacio($lst_people[0]->ID_CENS, "Internet");

                                // AFEGIM PERSONA A LA LLISTA DE VOTANTS
                                $idLlistaVotants = $ppdb->addLlistaVotants($lst_people[0]->ID_PROCESP, $lst_people[0]->ID_CENS , $data, $hora, 
                                                                           $fc->get_client_ip(), $fc->detectDevice());

                                $registreActualitzat = $ppdb->UpdateRegistratVotacioEfectuada($lst_people[0]->ID_CENS);

                                $fc->escribir_log_file(FOLDER_LOGS."/Vot_".date('d-m-Y').".log",' VOTACIO REGISTRADA ::: id_registrat: '.$lst_people[0]->ID.'. -- id_cens: '.$lst_people[0]->ID_CENS);

                            }

                        }
                    }
                    else {
                        //Persona no trobada al cens o hi ha més d'una
                        if(count($lst_people)==0) { 
                            $message = "usuari_no_registrat";
                            $fc->escribir_log_file(FOLDER_LOGS."/Vot_".date('d-m-Y').".log", 'VOT ERROR 6 ::: Dades: '.implode(' | ', $params));
                        }
                        else { 
                            $message = "probrema_dades_registre";
                        }
                    }
                }
            }
        }

        //echo "Message = ".$message;
        $smarty->assign('message', $message);
        $smarty->assign('proposta1', $proposta1);
        $smarty->assign('proposta2', $proposta2);
        $smarty->assign('proposta3', $proposta3);

        $smarty->assign('tdocument', $vars_post['tdocument'] ? $fc->test_input($vars_post['tdocument']) : null);
        $smarty->assign('dni_number', $vars_post['dni_number'] ? $fc->test_input($vars_post['dni_number']) : null);
        $smarty->assign('dni_letter', $vars_post['dni_letter'] ? $fc->test_input($vars_post['dni_letter']) : null);
        $smarty->assign('option2_passport_number', $vars_post['option2_passport_number'] ? $fc->test_input($vars_post['option2_passport_number']) : null);
        $smarty->assign('tr_letter_1', $vars_post['tr_letter_1'] ? $fc->test_input($vars_post['tr_letter_1']) : null);
        $smarty->assign('tr_number', $vars_post['tr_number'] ? $fc->test_input($vars_post['tr_number']) : null);
        $smarty->assign('tr_letter_2', $vars_post['tr_letter_2'] ? $fc->test_input($vars_post['tr_letter_2']) : null);


        $publickey = "6Lfdwh4TAAAAAKajPw-1Rj8msRd8YVZx14Vwova-";
        $smarty->assign("recaptcha",recaptcha_get_html($publickey));

        $smarty->assign('url_ca','/participacio-ciutadana/votacio-telematica/');
        $smarty->assign('url_es','/participacio-ciutadana/votacio-telematica/?lang=es');

        $smarty->display("participacio-ciutadana/votacio-telematica/ident.tpl");
    }
    */
    public function votacio_telematica_ident_v2($input_lang, $vars_post) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LIBS . '/recaptchalib.php');
        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");
        require_once (FOLDER_LANGUAGES."/participacio-ciutadana/votacio-telematica/".$lang."_votacio_telematica.cfg");
        require_once (FOLDER_MODEL . "/ciutat/ciutat.class.php");

        require_once(FOLDER_MODEL . "/pressupostos-participatius.class.php");
        $ppdb = new \webtortosa\pressupostos_participatius();
        $logs = new \webtortosa\logs();
        $numero_sequencia = $logs->getNumSequencia('PPART');

        $fc = new \webtortosa\functions_controller();

        if($vars_post && $vars_post['FirstTime']!="1" && $_SERVER['HTTP_REFERER'] = 'http://www2.tortosa.cat/participacio-ciutadana/votacio-telematica/') {
            //Recepció de dades i se consulta al Cens.
            $params = array(
                'tdocument'  => $vars_post['tdocument'] ? $fc->test_input($vars_post['tdocument']) : null,
                'dni_number' => $vars_post['dni_number'] ? $fc->test_input($vars_post['dni_number']) : null,
                'dni_letter' => $vars_post['dni_letter'] ? $fc->test_input($vars_post['dni_letter']) : null,
                'passaport'  => $vars_post['option2_passport_number'] ? $fc->test_input($vars_post['option2_passport_number']) : null,
                'tr_letter1' => $vars_post['tr_letter_1'] ? $fc->test_input($vars_post['tr_letter_1']) : null,
                'tr_number'  => $vars_post['tr_number'] ? $fc->test_input($vars_post['tr_number']) : null,
                'tr_letter2' => $vars_post['tr_letter_2'] ? $fc->test_input($vars_post['tr_letter_2']) : null,
                'proposta1'  => $vars_post['proposta1'] ? $fc->test_input($vars_post['proposta1']) : null,
                'proposta2'  => $vars_post['proposta2'] ? $fc->test_input($vars_post['proposta2']) : null,
                'proposta3'  => $vars_post['proposta3'] ? $fc->test_input($vars_post['proposta3']) : null
                //'PIN'                           => $fc->test_input($vars_post['pin'])
            );
            // Logs
            switch($fc->test_input($vars_post['tdocument'])) {
                case "1":
                    $content_log = $fc->setSpacesToString(8, $numero_sequencia, 1) . " VOT1 " . $params['dni_number'] . " - ". $params['dni_letter'];
                    break;
                case "2":
                    $content_log = $fc->setSpacesToString(8, $numero_sequencia, 1) . " VOT1 " . ' ' . $params['passaport'];
                    break;
                case "3":
                    $content_log = $fc->setSpacesToString(8, $numero_sequencia, 1) . " VOT1 " . ' ' . $params['tr_letter1'] . " - " . $params['tr_number'] . " - " . $params['tr_letter2'];
                    break;
            }
            $fc->escribir_log_file(FOLDER_LOGS . "/Vot_".date('d-m-Y').".log", $content_log);

            foreach($vars_post['propostes'] as $key=>$proposta) {
                switch($key) {
                    case 0:
                        $proposta1 = trim($fc->test_input($proposta));
                        $numvots1 = $ppdb->getNumVotsProposta($proposta1);
                        $numvots1++;
                        $hashVotsProposta1 = $fc->simple_encrypt($proposta1 . "|" .$numvots1. "|" .$lang);
                        break;
                    case 1:
                        $proposta2 = trim($fc->test_input($proposta));
                        $numvots2 = $ppdb->getNumVotsProposta($proposta2);
                        $numvots2++;
                        $hashVotsProposta2 = $fc->simple_encrypt($proposta2 . "|" .$numvots2. "|" .$lang);

                        break;
                    case 2:
                        $proposta3 = trim($fc->test_input($proposta));
                        $numvots3 = $ppdb->getNumVotsProposta($proposta3);
                        $numvots3++;
                        $hashVotsProposta3 = $fc->simple_encrypt($proposta3 . "|" .$numvots3. "|" .$lang);
                        break;
                }
            }

            //echo "P1: ".$proposta1 . " - P2: " . $proposta2 . " - P3: " . $proposta3."<br>";
            //echo "P1: ".$hashVotsProposta1 . " - P2: " . $hashVotsProposta2 . " - P3: " . $hashVotsProposta2."<br>";

            $continua = true;


            //if (($proposta1 == '') && ($proposta2 == '') && ($proposta3 == '')) {
            //    $message = "NO_HI_HA_PROPOSTA";
            //    $continua = false;
            //}

            if ($continua) {
                $lst_people = $ppdb->getRegistrat($params);

                if(count($lst_people)==1) {
                    // LA CONSULTA getRegistrat JA COMPROVA QUE ESTEM REGISTRATS EN EL PROCÉS PARTICIPATIU QUE TOCA
                    $message = "OK!!";

                    if ($fc->simple_decrypt($lst_people[0]->PIN) != $fc->test_input($vars_post['pin'])) {
                        $message = "PIN_INCORRECTE";
                        $fc->escribir_log_file(FOLDER_LOGS."/Vot_".date('d-m-Y').".log", $fc->setSpacesToString(8, $numero_sequencia, 1) . " VOT  ERRO 1" . $fc->setSpacesToString(6, $lst_people[0]->ID_CENS) . ' ' . $fc->setSpacesToString(6, $lst_people[0]->ID));
                    } else if ($lst_people[0]->VALIDAT==0) {
                        $message = "NO_VALIDAD";
                        $fc->escribir_log_file(FOLDER_LOGS."/Vot_".date('d-m-Y').".log", $fc->setSpacesToString(8, $numero_sequencia, 1) . " VOT  ERRO 2" . $fc->setSpacesToString(6, $lst_people[0]->ID_CENS) . ' ' . $fc->setSpacesToString(6, $lst_people[0]->ID));
                    } else if ($lst_people[0]->VOTACIO_EFECTUADA==1) {
                        $message = "JA_HA_VOTAT";
                        $fc->escribir_log_file(FOLDER_LOGS."/Vot_".date('d-m-Y').".log", $fc->setSpacesToString(8, $numero_sequencia, 1) . " VOT  ERRO 3" . $fc->setSpacesToString(6, $lst_people[0]->ID_CENS) . ' ' . $fc->setSpacesToString(6, $lst_people[0]->ID));
                    } else {
                        // POT VOTAR -> PIN CORRECTE, VALIDAT i NO HA VOTAT x INTERNET !!!
                        // OJO !!! POT HAVER VOTAT PRESENCIALMENT !!!

                        $haVotatCens = $ppdb->haVotatPPartCens($lst_people[0]->ID_CENS);


                        if ($haVotatCens==true) {
                            $message = "JA_HA_VOTAT_PRESENCIAL";
                            $fc->escribir_log_file(FOLDER_LOGS."/Vot_".date('d-m-Y').".log", $fc->setSpacesToString(8, $numero_sequencia, 1) . " VOT  ERRO 4" . $fc->setSpacesToString(6, $lst_people[0]->ID_CENS) . ' ' . $fc->setSpacesToString(6, $lst_people[0]->ID));
                        } else {
                            // ARA SEGUR QUE NO HA VOTAT !!!

                            $data = date('Y/m/d');
                            $hora = date('H:i:s');
                            //$hash1 = $fc->simple_encrypt($lst_people[0]->ID_CENS . "|" .$proposta1 . "|" .$lang);
                            //$hash2 = $fc->simple_encrypt($lst_people[0]->ID_CENS . "|" .$proposta2 . "|" .$lang);
                            //$hash3 = $fc->simple_encrypt($lst_people[0]->ID_CENS . "|" .$proposta3 . "|" .$lang);
                            $hash1 = $fc->simple_encrypt($proposta1 . "|" .$lang);
                            $hash2 = $fc->simple_encrypt($proposta2 . "|" .$lang);
                            $hash3 = $fc->simple_encrypt($proposta3 . "|" .$lang);

                            //echo "ID_PAPERETA = : ".$idPapereta . "<br>";
                            //echo $data ." - ". $hora . "<br>";
                            //echo "1 - ". $proposta1 ." - ". $hash1 . "<br>";
                            //echo "2 - ". $proposta2 ." - ". $hash2 . "<br>";
                            //echo "3 - ". $proposta3 ." - ". $hash3 . "<br>";

                            if ($proposta1 != '' || !$proposta1) {
                                $inserta1 = $ppdb->UpdateSumaVotProposta($lst_people[0]->ID_PROCESP, $proposta1, $hashVotsProposta1);
                            }
                            if ($proposta2 != '' || !$proposta2) {
                                $inserta2 = $ppdb->UpdateSumaVotProposta($lst_people[0]->ID_PROCESP, $proposta2, $hashVotsProposta2);
                            }
                            if ($proposta3 != '' || !$proposta3) {
                                $inserta3 = $ppdb->UpdateSumaVotProposta($lst_people[0]->ID_PROCESP, $proposta3, $hashVotsProposta3);
                            }

                            // INSERTEM VOTS
                            //$idVot1 = $ppdb->addVot($lst_people[0]->ID_PROCESP, $idPapereta, 1, $proposta1, $data, $hora, $hash1);
                            //$idVot2 = $ppdb->addVot($lst_people[0]->ID_PROCESP, $idPapereta, 2, $proposta2, $data, $hora, $hash2);
                            //$idVot3 = $ppdb->addVot($lst_people[0]->ID_PROCESP, $idPapereta, 3, $proposta3, $data, $hora, $hash3);
                            $idVot1 = $ppdb->addVotUrna($lst_people[0]->ID_PROCESP, $proposta1, $hash1);
                            $idVot2 = $ppdb->addVotUrna($lst_people[0]->ID_PROCESP, $proposta2, $hash2);
                            $idVot3 = $ppdb->addVotUrna($lst_people[0]->ID_PROCESP, $proposta3, $hash3);
                            $fc->escribir_log_file(FOLDER_LOGS."/Vot_".date('d-m-Y').".log", $fc->setSpacesToString(8, $numero_sequencia, 1) . " VOT2 " . $fc->setSpacesToString(6, $lst_people[0]->ID_CENS));

                            // ACTUALITZEM CENS COM A VOTAT
                            $censActualitzat = $ppdb->UpdateVotacio($lst_people[0]->ID_CENS, "Internet");

                            // AFEGIM PERSONA A LA LLISTA DE VOTANTS
                            $idLlistaVotants = $ppdb->addLlistaVotants($lst_people[0]->ID_PROCESP, $lst_people[0]->ID_CENS , $data, $hora,
                                $fc->get_client_ip(), $fc->detectDevice());

                            $registreActualitzat = $ppdb->UpdateRegistratVotacioEfectuada($lst_people[0]->ID_CENS);

                            $fc->escribir_log_file(FOLDER_LOGS."/Vot_".date('d-m-Y').".log", $fc->setSpacesToString(8, $numero_sequencia, 1) . " VOT3 " . $fc->setSpacesToString(6, $lst_people[0]->ID_CENS));
                        }
                    }
                }
                else {
                    //Persona no trobada a INSCRIPCIO o hi ha més d'una
                    if(count($lst_people)==0) {
                        $message = "usuari_no_registrat";
                        $fc->escribir_log_file(FOLDER_LOGS."/Vot_".date('d-m-Y').".log", 'VOT ERRO 5');
                    }
                    else {
                        $message = "probrema_dades_registre";
                    }
                }
            }
        }
        $lst_propostes = $ppdb->getPropostes();

        //Depurem les propostes descripcions
        /*
        foreach($lst_propostes as &$proposta) {
            $proposta->DESC_CURTA = str_replace('.', '. ',$proposta->DESC_CURTA);
        }
        */

        // Propostes secció Accessibilitat i voreres
        $id_propostes_accessibilitat = array(15, 61, 65, 158, 164, 271, 326, 370, 371, 468, 474, 493, 499, 561, 666, 667, 704, 780);
        $propostes_accessibilitat = array();
        foreach($lst_propostes as &$proposta) {
            if(in_array($proposta->NUM_PROPOSTA, $id_propostes_accessibilitat)) {
                $proposta->DESC_CURTA = utf8_encode($proposta->DESC_CURTA);
                $propostes_accessibilitat[] = $proposta;
            }
        }
        $smarty->assign('propostes_accessibilitat', $propostes_accessibilitat);

        // Propostes secció camins
        $id_propostes_camins = array(58, 79, 82, 83, 229, 297, 303, 401, 456);
        $propostes_camins = array();
        foreach($lst_propostes as &$proposta) {
            if(in_array($proposta->NUM_PROPOSTA, $id_propostes_camins)) {
                $proposta->DESC_CURTA = utf8_encode($proposta->DESC_CURTA);
                $propostes_camins[] = $proposta;
            }
        }
        $smarty->assign('propostes_camins', $propostes_camins);

        // Propostes secció carrers
        $id_propostes_carrers = array(24, 223, 315, 384, 470, 604);
        $propostes_carrers = array();
        foreach($lst_propostes as &$proposta) {
            if(in_array($proposta->NUM_PROPOSTA, $id_propostes_carrers)) {
                $proposta->DESC_CURTA = utf8_encode($proposta->DESC_CURTA);
                $propostes_carrers[] = $proposta;
            }
        }
        $smarty->assign('propostes_carrers', $propostes_carrers);

        // Propostes secció enllumenat
        $id_propostes_enllumenat = array(12, 230, 254, 314, 321, 322, 430, 501, 503, 568, 765);
        $propostes_enllumenat = array();
        foreach($lst_propostes as &$proposta) {
            if(in_array($proposta->NUM_PROPOSTA, $id_propostes_enllumenat)) {
                $proposta->DESC_CURTA = utf8_encode($proposta->DESC_CURTA);
                $propostes_enllumenat[] = $proposta;
            }
        }
        $smarty->assign('propostes_enllumenat', $propostes_enllumenat);

        // Propostes secció equipaments
        $id_propostes_equipaments = array(121, 225, 246, 385, 411, 469, 721);
        $propostes_equipaments = array();
        foreach($lst_propostes as &$proposta) {
            if(in_array($proposta->NUM_PROPOSTA, $id_propostes_equipaments)) {
                $proposta->DESC_CURTA = utf8_encode($proposta->DESC_CURTA);
                $propostes_equipaments[] = $proposta;
            }
        }
        $smarty->assign('propostes_equipaments', $propostes_equipaments);

        // Propostes secció esports
        $id_propostes_esports = array(10, 11, 45, 51, 78, 99, 112, 299, 319, 324, 387, 434, 608, 612, 684, 729);
        $propostes_esports = array();
        foreach($lst_propostes as &$proposta) {
            if(in_array($proposta->NUM_PROPOSTA, $id_propostes_esports)) {
                $proposta->DESC_CURTA = utf8_encode($proposta->DESC_CURTA);
                $propostes_esports[] = $proposta;
            }
        }
        $smarty->assign('propostes_esports', $propostes_esports);

        // Propostes secció jocs
        $id_propostes_jocs = array(13, 32, 39, 60, 175, 251, 492, 630, 755);
        $propostes_jocs = array();
        foreach($lst_propostes as &$proposta) {
            if(in_array($proposta->NUM_PROPOSTA, $id_propostes_jocs)) {
                $proposta->DESC_CURTA = utf8_encode($proposta->DESC_CURTA);
                $propostes_jocs[] = $proposta;
            }
        }
        $smarty->assign('propostes_jocs', $propostes_jocs);

        // Propostes secció mascotes
        $id_propostes_mascotes = array(200, 285, 441, 442);
        $propostes_mascotes = array();
        foreach($lst_propostes as &$proposta) {
            if(in_array($proposta->NUM_PROPOSTA, $id_propostes_mascotes)) {
                $proposta->DESC_CURTA = utf8_encode($proposta->DESC_CURTA);
                $propostes_mascotes[] = $proposta;
            }
        }
        $smarty->assign('propostes_mascotes', $propostes_mascotes);

        // Propostes secció mobiliari_urba
        $id_propostes_mobiliari_urba = array(7, 111, 113, 174, 444, 445, 541, 578, 730);
        $propostes_mobiliari_urba = array();
        foreach($lst_propostes as &$proposta) {
            if(in_array($proposta->NUM_PROPOSTA, $id_propostes_mobiliari_urba)) {
                $proposta->DESC_CURTA = utf8_encode($proposta->DESC_CURTA);
                $propostes_mobiliari_urba[] = $proposta;
            }
        }
        $smarty->assign('propostes_mobiliari_urba', $propostes_mobiliari_urba);

        // Propostes secció mobilitat
        $id_propostes_mobilitat = array(5, 18, 115, 123, 279, 327, 360, 414, 449, 542, 558, 560, 607);
        $propostes_mobilitat = array();
        foreach($lst_propostes as &$proposta) {
            if(in_array($proposta->NUM_PROPOSTA, $id_propostes_mobilitat)) {
                $proposta->DESC_CURTA = utf8_encode($proposta->DESC_CURTA);
                $propostes_mobilitat[] = $proposta;
            }
        }
        $smarty->assign('propostes_mobilitat', $propostes_mobilitat);

        // Propostes secció Parcs i places
        $id_propostes_parcs = array(14, 241, 295, 535, 683, 728);
        $propostes_parcs = array();
        foreach($lst_propostes as &$proposta) {
            if(in_array($proposta->NUM_PROPOSTA, $id_propostes_parcs)) {
                $proposta->DESC_CURTA = utf8_encode($proposta->DESC_CURTA);
                $propostes_parcs[] = $proposta;
            }
        }
        $smarty->assign('propostes_parcs', $propostes_parcs);

        // Propostes secció Patrimoni
        $id_propostes_patrimoni = array(170, 258, 335, 372, 551);
        $propostes_patrimoni = array();
        foreach($lst_propostes as &$proposta) {
            if(in_array($proposta->NUM_PROPOSTA, $id_propostes_patrimoni)) {
                $proposta->DESC_CURTA = utf8_encode($proposta->DESC_CURTA);
                $propostes_patrimoni[] = $proposta;
            }
        }
        $smarty->assign('propostes_patrimoni', $propostes_patrimoni);

        // Propostes secció Reciclatge
        $id_propostes_reciclatge = array(68, 510, 545, 550, 555);
        $propostes_reciclatge = array();
        foreach($lst_propostes as &$proposta) {
            if(in_array($proposta->NUM_PROPOSTA, $id_propostes_reciclatge)) {
                $proposta->DESC_CURTA = utf8_encode($proposta->DESC_CURTA);
                $propostes_reciclatge[] = $proposta;
            }
        }
        $smarty->assign('propostes_reciclatge', $propostes_reciclatge);

        // Propostes secció Els Reguers
        $id_propostes_reguers = array(103, 215, 277, 278, 318, 377, 378, 452, 633, 660);
        $propostes_reguers = array();
        foreach($lst_propostes as &$proposta) {
            if(in_array($proposta->NUM_PROPOSTA, $id_propostes_reguers)) {
                $proposta->DESC_CURTA = utf8_encode($proposta->DESC_CURTA);
                $propostes_reguers[] = $proposta;
            }
        }
        $smarty->assign('propostes_reguers', $propostes_reguers);

        // Propostes secció Vinallop
        $id_propostes_vinallop = array(267, 347, 422, 702, 709, 710, 711, 718, 719, 720);
        $propostes_vinallop = array();
        foreach($lst_propostes as &$proposta) {
            if(in_array($proposta->NUM_PROPOSTA, $id_propostes_vinallop)) {
                $proposta->DESC_CURTA = utf8_encode($proposta->DESC_CURTA);
                $propostes_vinallop[] = $proposta;
            }
        }
        $smarty->assign('propostes_vinallop', $propostes_vinallop);

        //echo "Message = ".$message;
        $smarty->assign('message', $message);
        $smarty->assign('proposta1', $proposta1);
        $smarty->assign('proposta2', $proposta2);
        $smarty->assign('proposta3', $proposta3);

        $smarty->assign('tdocument', $vars_post['tdocument'] ? $fc->test_input($vars_post['tdocument']) : null);
        $smarty->assign('dni_number', $vars_post['dni_number'] ? $fc->test_input($vars_post['dni_number']) : null);
        $smarty->assign('dni_letter', $vars_post['dni_letter'] ? $fc->test_input($vars_post['dni_letter']) : null);
        $smarty->assign('option2_passport_number', $vars_post['option2_passport_number'] ? $fc->test_input($vars_post['option2_passport_number']) : null);
        $smarty->assign('tr_letter_1', $vars_post['tr_letter_1'] ? $fc->test_input($vars_post['tr_letter_1']) : null);
        $smarty->assign('tr_number', $vars_post['tr_number'] ? $fc->test_input($vars_post['tr_number']) : null);
        $smarty->assign('tr_letter_2', $vars_post['tr_letter_2'] ? $fc->test_input($vars_post['tr_letter_2']) : null);


        $publickey = "6Lfdwh4TAAAAAKajPw-1Rj8msRd8YVZx14Vwova-";
        $smarty->assign("recaptcha",recaptcha_get_html($publickey));

        $smarty->assign('url_ca','/participacio-ciutadana/votacio-telematica/');
        $smarty->assign('url_es','/participacio-ciutadana/votacio-telematica/?lang=es');

        $smarty->display("participacio-ciutadana/votacio-telematica/ident_v2.tpl");

    }

}
?>