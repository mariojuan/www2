<?php
/**
 * Created by Ajuntament de Tortosa.
 * User: Mario Juan
 * Date: 19/03/18
 * Time: 12:55
 */

namespace webtortosa;

session_start();
require_once (FOLDER_CONTROLLER."/functions_controller.php");
require_once (FOLDER_LIBS."/smarty/Smarty_web.class.php");
require_once (FOLDER_MODEL."/db.class.php");
require_once (FOLDER_MODEL."/logs.class.php");

class web_controller_registre_telematic extends web_controller {
    //Mostra formulari de registre o Rep les dades de petició de registre. Consulta al Cens i fa una inserció parcial a la taula de registrats.
    public function registre_telematic($input_lang, $vars_post) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LIBS . '/recaptchalib.php');
        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");
        require_once (FOLDER_LANGUAGES."/inscripcio-participacio/".$lang."_inscripcio_participacio.cfg");
        require_once (FOLDER_MODEL . "/ciutat/ciutat.class.php");

        $fc = new \webtortosa\functions_controller();
        //Se rep les dades.
        if($vars_post) {
            $params = array(
                'tdocument' => $vars_post['tdocument'] ? $fc->test_input($vars_post['tdocument']) : null,
                'dni_number' => $vars_post['dni_number'] ? $fc->test_input($vars_post['dni_number']) : null,
                'dni_letter' => $vars_post['dni_letter'] ? strtoupper($fc->test_input($vars_post['dni_letter'])) : null,
                'passaport' => $vars_post['option2_passport_number'] ? $fc->test_input($vars_post['option2_passport_number']) : null,
                'tr_letter1' => $vars_post['tr_letter_1'] ? $fc->test_input($vars_post['tr_letter_1']) : null,
                'tr_number' => $vars_post['tr_number'] ? $fc->test_input($vars_post['tr_number']) : null,
                'tr_letter2' => $vars_post['tr_letter_2'] ? $fc->test_input($vars_post['tr_letter_2']) : null,
                'data_naixement' => $fc->normaldate_to_mysql($fc->test_input($vars_post['dia'])."/".$fc->test_input($vars_post['mes'])."/".$fc->test_input($vars_post['any'])),
                'primer_cognom' => utf8_decode($fc->test_input(strtoupper ($fc->delete_accents($vars_post['primer_cognom'])))),
                'nucli' => $fc->test_input($vars_post['nucli']),
                'email' => $fc->test_input($vars_post['email'])
            );
        }

        if($vars_post && $_SERVER['HTTP_REFERER'] = 'https://www2.tortosa.cat/registre-telematic/' && $vars_post['back']!="1") {
            require_once (FOLDER_MODEL."/pressupostos-participatius.class.php");
            //Se consulta al Cens.

            $ppdb = new \webtortosa\pressupostos_participatius();
            $lst_people = $ppdb->getDataCens($params);

            if(count($lst_people)==1) {
                //Persona que està al cens. Ara mirem si no està registrada.
                //$params['EMAIL'] = $params['email'];
                $lst_registrats = $ppdb->getRegistrat($params);
                //echo "Num registrats: ".count($lst_registrats) . "<br>";
                if(count($lst_registrats)==0) {
                    //Se mira si ha ha algun registrat amb aquest email
                    $params_email = array('EMAIL' => $params['email']);
                    $lst_registrats_email = $ppdb->getRegistrat($params_email);
                    if(count($lst_registrats_email)==0) {
                        //Preparació de les dades per al registre
                        if($params['tdocument']==1) {
                            $params['tr_number']    = $params['dni_number'];
                            $params['tr_letter2']  = $params['dni_letter'];
                        }
                        $params['PIN']                  = '';
                        $params['ID_CENS']              = $lst_people[0]->pinume;
                        $params['DATA_REGISTRE']        = date('Y/m/d');
                        $params['HORA_REGISTRE']        = date('H:i:s');
                        $params['IP_DISPOSITIU']        = $fc->get_client_ip();
                        $params['TIPUS_DISPOSITIU']     = $fc->detectDevice();
                        $params['VALIDAT']              = 0;
                        $params['VOTACIO_EFECTUADA']    = 0;
                        $params['ID_PROCESP']           = $lst_people[0]->ID_PROCESP;

                        //Registro com a la taula les dades del nou regsitrat i li envio un email
                        $id_registrat = $ppdb->addRegistrat($params);
                        //Log persona trobada al Cens
                        //Gestió logs registre telematic
                        switch($params['tdocument']) {
                            case "1":
                                $content_log = "INS1 SOLI ok " . $fc->setSpacesToString(6, $params['ID_CENS']) . ' '. $fc->setSpacesToString(6, $id_registrat) . ' ' . $params['dni_number'] . " - ". $params['dni_letter'];
                                break;
                            case "2":
                                $content_log = "INS1 SOLI ok " . $fc->setSpacesToString(6, $params['ID_CENS']) . ' '. $fc->setSpacesToString(6, $id_registrat) . ' ' . $params['option2_passport_number'];
                                break;
                            case "3":
                                $content_log = "INS1 SOLI ok " . $fc->setSpacesToString(6, $params['ID_CENS']) . ' '. $fc->setSpacesToString(6, $id_registrat) . ' ' . $params['tr_letter1'] . " - " . $params['tr_number'] . " - " . $params['tr_letter2'];
                                break;
                        }
                        $fc->escribir_log_file(FOLDER_LOGS."/Ins_".date('d-m-Y').".log", $content_log);

                        //Envio email amb el pin a l'usuari registrat
                        $hash = $fc->simple_encrypt($params['ID_CENS'] . "|" .$params['email']. "|" .$lang);
                        //echo "<br>".$hash."<br>";
                        switch($lang) {
                            case "ca":
                                $email_text1 = "Benvolgut/da";
                                $email_text2 = "Rebeu aquest email atenent la sol·licitud d'inscripció per a la votació online dels Pressupostos Participatius 2018.";
                                $email_text3 = "Si heu rebut aquest email sense el vostre coneixement, poseu-vos en contacte amb l'Ajuntament de Tortosa mitjançant correu electrònic a <a href='mailto:info@tortosa.cat'>info@tortosa.cat</a> o per telèfon al número 977 585 884 en horari d'atenció al públic.";
                                $email_text4 = "Per finalitzar el procés d'inscripció cal que féu click al següent";
                                $email_text5 = "enllaç";
                                $email_text5_1 = "(cliqueu només un cop)";
                                $email_text6 = "Quan ho feu, rebreu en aquest mateix compte de correu el PIN amb el que podreu efectuar la votació online, si així ho desitjeu.";
                                $email_text7 = "Gràcies per participar";
                                $email_text8 = "Sol·licitud inscripció per votació online";
                                $email_text9 = "Aquest correu és informatiu, no respongueu a aquesta adreça.";
                                $email_text10 = "Nota:";
                                $email_text11 = "Aquest correu pot trigar un temps màxim de 15 minuts.";
                                $email_text12 = "Si transcorregut aquest temps no l'heu rebut:";
                                $email_text13 = "Verifiqueu la bústia de l'spam.";
                                $email_text14 = "Podeu contactar per correu electrònic i ens posarem en contacte per resoldre la incidència a l'adreça info@tortosa.cat.";
                                $email_text15 = "Podeu trucar per telèfon al 977 585 884 en horaris d'atenció al públic.";
                                break;
                            case "es":
                                $email_text1 = "Estimado/a";
                                $email_text2 = "Recibe este email atendiendo la solicitud de inscripción para la votación online de los Presupuestos Participativos 2018.";
                                $email_text3 = "Si ha recibido este email sin su conocimiento, póngase en contacto con el Ayuntamiento de Tortosa mediante correo electrónico a <a href='mailto:info@tortosa.cat'>info@tortosa.cat</a> o por teléfono al número 977 585 884 en horario de atención al público.";
                                $email_text4 = "Para finalizar el proceso de inscripción debe hacer click en el siguiente";
                                $email_text5 = "enlace";
                                $email_text5_1 = "(pulse solo una vez)";
                                $email_text6 = "Cuando lo haga, recibirá en esta misma cuenta de correo el PIN con el que podrá efectuar la votación online, si así lo desea.";
                                $email_text7 = "Gracias por participar";
                                $email_text8 = "Solicitud inscripción por votación online";
                                $email_text9 = "Este correo es informativo, no responda a esta dirección.";
                                $email_text10 = "Nota:";
                                $email_text11 = "Este correo puede tardar un tiempo máximo de 15 minutos.";
                                $email_text12 = "Si transcurrido este tiempo no lo ha recibido:";
                                $email_text13 = "Verificad la bandeja de spam.";
                                $email_text14 = "Puede contactar por correo electrónico y nos pondremos en contacto para resolver la incidencia en la dirección info@tortosa.cat.";
                                $email_text15 = "Puede llamar por teléfono al 977 585 884 en horarios de atención al público.";
                                break;
                        }
                        $content = '
                            <html>
                            <head>
                            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                            </head>
                            <body>
                            <div></div>
                            <p style="font-family:Arial, Helvetica, "Bitstream Vera Sans", sans-serif">'.$email_text1.'</p>
                            <p style="font-family:Arial, Helvetica, "Bitstream Vera Sans", sans-serif">'.$email_text2.'</p>
                            <p style="font-family:Arial, Helvetica, "Bitstream Vera Sans", sans-serif">'.$email_text3.'</p>
                            <p style="font-family:Arial, Helvetica, "Bitstream Vera Sans", sans-serif; margin-top:30px">'.$email_text4.' <a href="http://www2.tortosa.cat/inscripcio-participacio/validacio.php?t='.urlencode($hash).'">'.$email_text5.'</a>. '.$email_text5_1.'</p>
                            <p style="font-family:Arial, Helvetica, "Bitstream Vera Sans", sans-serif">'.$email_text6.'</p>
                            <p style="font-family:Arial, Helvetica, "Bitstream Vera Sans", sans-serif">'.$email_text10.' <label style="color:#ED1F33;">'.$email_text11.'</label><br/>'.$email_text12.'<ul><li style="font-family:Arial, Helvetica, sans-serif">'.$email_text13.'</li><li style="font-family:Arial, Helvetica, sans-serif">'.$email_text14.'</li><li style="font-family:Arial, Helvetica, sans-serif">'.$email_text15.'</li></ul></p>
                            <p style="font-family:Arial, Helvetica, "Bitstream Vera Sans", sans-serif">'.$email_text7.'</p>
                            <p style="font-family:Arial, Helvetica, sans-serif; font-size:10px; margin-top:100px">'.$email_text9.'</p>
                            </body>
                            </html>
                        ';

                        if($fc->sendEmail($lst_people[0]->PROCESP . " - " . $email_text8, $content, $params['email'], null, EMAIL_AVERIA)) {
                        //if($fc->sendEmail_phpMailer($lst_people[0]->PROCESP . " - " . $email_text8, $content, $params['email'], null, EMAIL_AVERIA, 'no-reply@tortosa.cat')) {
                            $message = 'missatge_validacio';
                            $fc->escribir_log_file(FOLDER_LOGS . "/Ins_".date('d-m-Y').".log", 'INS2 ENVI e1 '. $fc->setSpacesToString(6, $params['ID_CENS']) .' '. $fc->setSpacesToString(6, $id_registrat). ' ' . $params['email']);
                        }
                    }
                    else {
                        // Cas de que s'intenti registrar amb un email que ja estava registrat.
                        $message = "missatge_registrat_amb_mateix_email";
                        $fc->escribir_log_file(FOLDER_LOGS . "/Ins_".date('d-m-Y').".log", 'INS1 ERRO  1 :: Dades: '.implode(' | ', $params));
                    }
                }
                else {
                    if($lst_registrats[0]->VALIDAT==1) {
                        // Cas usuari inscrit anteriorment i validat.
                        $message = "missatge_inscrit_anteriorment_i_validat";
                        $fc->escribir_log_file(FOLDER_LOGS . "/Ins_".date('d-m-Y').".log", 'INS1 ERRO  2 :: Dades: '.implode(' | ', $params));
                        if($lst_registrats[0]->VOTACIO_EFECTUADA==1) {
                            // Cas usuari inscrit anteriorment, validat i Votat .
                            $message = "missatge_inscrit_anteriorment_validat_i_votat";
                            $fc->escribir_log_file(FOLDER_LOGS . "/Ins_".date('d-m-Y').".log", 'INS1 ERRO  3 :: Dades: '.implode(' | ', $params));
                        }
                    }
                    else {
                        // Cas usuari inscrit anteriorment i NO validat.
                        $message = "missatge_inscrit_anteriorment_i_no_validat";
                        $fc->escribir_log_file(FOLDER_LOGS . "/Ins_".date('d-m-Y').".log", 'INS1 ERRO  4 :: Dades: '.implode(' | ', $params));

                    }
                }
            }
            else {
                if(count($lst_people)==0) {
                    // Persona no trobada al cens
                    $message = "missatge_no_censat";
                    $fc->escribir_log_file(FOLDER_LOGS . "/Ins_".date('d-m-Y').".log", 'INS1 ERRO  5 :: Dades: '.implode(' | ', $params));
                }
                else {
                    // Hi ha més d'un resultat en la cerca del Cens.
                    $message = "probrema_dades_cens";
                    $fc->escribir_log_file(FOLDER_LOGS . "/Ins_".date('d-m-Y').".log", 'INS1 ERRO  6 :: Dades: '.implode(' | ', $params));
                }
            }
        }
        $smarty->assign('message', $message);

        $data_ciutat    = new \webtortosa\ciutat();
        $nuclis         = $data_ciutat->getNucli();
        $carrers        = $data_ciutat->getCarrer();
        $publickey      = "6Lfdwh4TAAAAAKajPw-1Rj8msRd8YVZx14Vwova-";

        $smarty->assign('nuclis', $nuclis);
        $smarty->assign('carrers', $carrers);

        $smarty->assign('tdocument',$params['tdocument']);
        $smarty->assign('dni_number',$params['dni_number']);
        $smarty->assign('dni_letter',$params['dni_letter']);
        $smarty->assign('option2_passport_number',$params['passaport']);
        $smarty->assign('tr_letter_1',$params['tr_letter1']);
        $smarty->assign('tr_number',$params['tr_number']);
        $smarty->assign('tr_letter_2',$params['tr_letter2']);
        $smarty->assign('dia',$vars_post['dia']);
        $smarty->assign('mes',$vars_post['mes']);
        $smarty->assign('any',$vars_post['any']);
        $smarty->assign('primer_cognom',$params['primer_cognom']);
        $smarty->assign('nucli',$params['nucli']);
        $smarty->assign('email',$params['email']);


        $smarty->assign("recaptcha",recaptcha_get_html($publickey));
        $smarty->assign('url_ca','/inscripcio-participacio/');
        $smarty->assign('url_es','/inscripcio-participacio/?lang=es');
        $smarty->display("inscripcio-participacio/formulari_registre.tpl");
    }

    //Rep click de l'enllaç enviat per email i confirma un usuari registrat.
    public function validacio_registre_telematic($vars_get) {
        $smarty = new \webtortosa\Smarty_web();

        require_once (FOLDER_LIBS . '/recaptchalib.php');
        require_once (FOLDER_MODEL."/pressupostos-participatius.class.php");

        $fc = new \webtortosa\functions_controller();
        if($vars_get) {
            //echo $vars_get["t"] . "<br>";
            //$parameters = $fc->simple_decrypt(urldecode($vars_get["t"]));
            $parameters = $fc->simple_decrypt($vars_get["t"]);
            $parameters_arr = explode("|", $parameters);
            $params = array(
                "EMAIL"     => $parameters_arr[1],
                "ID_CENS"   => $parameters_arr[0]
            );

            $ppdb = new \webtortosa\pressupostos_participatius();
            $lst_registrats = $ppdb->getRegistrat($params);
            $lang = $parameters_arr[2];
            if(count($lst_registrats)==1) {
                if($lst_registrats[0]->PIN=="" && $lst_registrats[0]->VALIDAT==0) {
                    //Se confirma el correu electrònic i s'envia el PIN
                    $pin = $fc->pinGenerate(6);
                    $params = array(
                        'ID' => $lst_registrats[0]->ID,
                        'PIN' => $fc->simple_encrypt($pin),
                        'VALIDAT' => 1
                    );
                    $ppdb->updateRegistrat($params);
                    $fc->escribir_log_file(FOLDER_LOGS . "/Ins_".date('d-m-Y').".log", 'INS3 CONF ok '. $fc->setSpacesToString(6, $lst_registrats[0]->ID_CENS) .' '. $fc->setSpacesToString(6, $params['ID']));

                    switch ($lang) {
                        case "es":
                            $email_text1 = "Estimado/a,";
                            $email_text2 = "El PIN personal que le permitirá efectuar la votación online es: ";
                            $email_text3 = "(Tenga en cuenta las mayúsculas y las minúsculas)";
                            $email_text4 = "Puede votar desde el 28 de abril hasta las 14:00 h del 11 de mayo accediendo al siguiente ";
                            $email_text5 = "enlace";
                            $email_text6 = "o bien accediendo a la web participa.tortosa.cat";
                            $email_text7 = "Gracias por participar.";
                            $subject_email = "Confirmación de la inscripción por votación online";
                            break;
                        default:
                            //Textes email
                            $email_text1 = "Benvolgut/da,";
                            $email_text2 = "El vostre PIN personal que us permetrà efectuar la votació online és: ";
                            $email_text3 = "(Teniu en compte les majúscules i les minúscules)";
                            $email_text4 = "Podeu votar des del 28 d'abril fins a les 14:00 h de l'11 de maig accedint al següent ";
                            $email_text5 = "enllaç";
                            $email_text6 = "o bé accedint al web participa.tortosa.cat";
                            $email_text7 = "Gràcies per participar.";
                            $subject_email = "Confirmació de la inscripció per votació online";
                            break;
                    }
                    $email_text2 .= " " . $pin;
                    $content = '
                            <html>
                            <head>
                            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                            </head>
                            <body>
                            <div></div>
                            <p style="font-family:Arial, Helvetica, sans-serif">' . $email_text1 . ' </p>
                            <p style="font-family:Arial, Helvetica, sans-serif">' . $email_text2 . ' <span style="font-size:11px">'.$email_text3.'</span></p>
                            <p style="font-family:Arial, Helvetica, sans-serif">' . $email_text4 . ' <a href="http://www2.tortosa.cat/participacio-ciutadana/votacio-telematica/">' . $email_text5 . '</a> '.$email_text6.'</p>
                            <p style="font-family:Arial, Helvetica, sans-serif">' . $email_text7 . ' </p>
                            </body>
                            </html>
                        ';
                    if ($fc->sendEmail($lst_registrats[0]->PROCESP . " - " . $subject_email, $content, $parameters_arr[1], null, EMAIL_AVERIA)) {
                    //if ($fc->sendEmail_phpMailer($lst_registrats[0]->PROCESP . " - " . $subject_email, $content, $parameters_arr[1], null, EMAIL_AVERIA, 'no-reply@tortosa.cat')) {
                        $message = 'missatge_confirmacio';
                        $fc->escribir_log_file(FOLDER_LOGS . "/Ins_".date('d-m-Y').".log", 'INS4 ENVI e2 '. $fc->setSpacesToString(6, $lst_registrats[0]->ID_CENS) .' '. $fc->setSpacesToString(6, $params['ID']). ' ' . $parameters_arr[1]);
                    }
                }
                else {
                    if($lst_registrats[0]->VOTACIO_EFECTUADA) {
                        // Ja estava inscrit  anteriorment, validat i votat.
                        $message = 'missatge_inscrit_anteriorment_validat_i_votat';
                        $fc->escribir_log_file(FOLDER_LOGS . "/Ins_".date('d-m-Y').".log", 'INS3 ERRO  7 :: Dades: '.implode(' | ', $params). " - " . $fc->detectDevice());
                    }
                    else {
                        // Ja estava inscrit anteriorment i validat, però no votat.
                        $message = 'missatge_inscrit_anteriorment_i_validat';
                        $fc->escribir_log_file(FOLDER_LOGS . "/Ins_".date('d-m-Y').".log", 'INS3 ERRO  8 :: Dades: '.implode(' | ', $params) . " - " . $fc->detectDevice());
                    }
                }
            }
            else {
                if(count($lst_registrats)==0) {
                    // No hi ha un usuari registrat per confirmar.
                    $lang = $this->languages(null);
                    $message = 'missatge_no_confirmacio';
                    $fc->escribir_log_file(FOLDER_LOGS . "/Ins_".date('d-m-Y').".log", 'INS3 ERRO  9 :: Dades: '.implode(' | ', $params));
                }
                else {
                    // Hi ha més d'un registre per confirmar.
                    $message = 'missatge_no_confirmacio';
                    $fc->escribir_log_file(FOLDER_LOGS . "/Ins_".date('d-m-Y').".log", 'INS3 ERRO 10 :: Dades: '.implode(' | ', $params));
                }
            }
        }
        else {
            // Intent de confirmació sense pas de paràmetres. (Possiblement un bot)
            $lang = $this->languages(null);
            $message = 'missatge_no_confirmacio';
            $fc->escribir_log_file(FOLDER_LOGS . "/Ins_".date('d-m-Y').".log", 'INS3 ERRO 11');
        }

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");
        require_once (FOLDER_LANGUAGES."/inscripcio-participacio/".$lang."_inscripcio_participacio.cfg");
        $smarty->assign('lang',$lang);
        $smarty->assign('message', $message);
        $smarty->display("inscripcio-participacio/formulari_registre.tpl");
    }

    //Mostra formulari de recuperació de dades o consulta dades de registrat per confirmar i envia email.
    function recuperacio_dades_acces($input_lang, $vars_post) {
        $smarty = new \webtortosa\Smarty_web();
        require_once (FOLDER_LIBS . '/recaptchalib.php');
        $fc = new \webtortosa\functions_controller();
        $lang = $this->languages($input_lang);

        if($vars_post && $_SERVER['HTTP_REFERER'] = 'http://www2.tortosa.cat/inscripcio-participacio/recuperacio_dades_acces.php') {
            require_once (FOLDER_MODEL."/pressupostos-participatius.class.php");
            $params = array(
                'tdocument'                     => $vars_post['tdocument'] ? $fc->test_input($vars_post['tdocument']) : null,
                'dni_number'                    => $vars_post['dni_number'] ? $fc->test_input($vars_post['dni_number']) : null,
                'dni_letter'                    => $vars_post['dni_letter'] ? $fc->test_input($vars_post['dni_letter']) : null,
                'passaport'                     => $vars_post['option2_passport_number'] ? $fc->test_input($vars_post['option2_passport_number']) : null,
                'tr_letter1'                    => $vars_post['tr_letter_1'] ? $fc->test_input($vars_post['tr_letter_1']) : null,
                'tr_number'                     => $vars_post['tr_number'] ? $fc->test_input($vars_post['tr_number']) : null,
                'tr_letter2'                    => $vars_post['tr_letter_2'] ? $fc->test_input($vars_post['tr_letter_2']) : null,
                'EMAIL'                         => $fc->test_input($vars_post['email'])
            );
            $ppdb = new \webtortosa\pressupostos_participatius();
            $lst_registrats = $ppdb->getRegistrat($params);
            if(count($lst_registrats)==1) {
                if($lst_registrats[0]->PIN!="" && $lst_registrats[0]->VALIDAT==1) {
                    //Insercio als logs en fitxer
                    switch($params['tdocument']) {
                        case "1":
                            $content_log = "REC1 SOLI ok " . $fc->setSpacesToString(6, $lst_registrats[0]->ID_CENS) . ' '. $fc->setSpacesToString(6, $lst_registrats[0]->ID) . ' ' . $params['dni_number'] . " - ". $params['dni_letter'];
                            break;
                        case "2":
                            $content_log = "REC1 SOLI ok " . $fc->setSpacesToString(6, $lst_registrats[0]->ID_CENS) . ' '. $fc->setSpacesToString(6, $lst_registrats[0]->ID) . ' ' . $params['option2_passport_number'];
                            break;
                        case "3":
                            $content_log = "REC1 SOLI ok " . $fc->setSpacesToString(6, $lst_registrats[0]->ID_CENS) . ' '. $fc->setSpacesToString(6, $lst_registrats[0]->ID) . ' ' . $params['tr_letter1'] . " - " . $params['tr_number'] . " - " . $params['tr_letter2'];
                            break;
                    }
                    $fc->escribir_log_file(FOLDER_LOGS."/Ins_".date('d-m-Y').".log", $content_log);

                    //S'envia email
                    switch ($lang) {
                        case "es":
                            //Textes email
                            $email_text1 = "Estimado/a,";
                            $email_text2 = "El PIN personal que le permitirá efectuar la votación online: ";
                            $email_text3 = "(Teniu en compte les majúscules i les minúscules)";
                            $email_text4 = "Puede votar accediendo al siguiente";
                            $email_text5 = "enlace";
                            $email_text6 = "Gracias por participar.";
                            $email_text7 = "Este correo es informativo, no responda a esta dirección.";
                            $subject_email = "Reenvío del PIN por votación online";
                            break;
                        default:
                            //Textes email
                            $email_text1 = "Benvolgut/da,";
                            $email_text2 = "El vostre PIN personal que us permetrà efectuar la votació online: ";
                            $email_text3 = "(Teniu en compte les majúscules i les minúscules)";
                            $email_text4 = "Podeu votar accedint al següent";
                            $email_text5 = "enllaç";
                            $email_text6 = "Gràcies per participar.";
                            $email_text7 = "Aquest correu és informatiu, no respongueu a aquesta adreça.";
                            $subject_email = "Reenviament del PIN per votació online";
                            break;
                    }
                    $email_text2 .= " " . $fc->simple_decrypt($lst_registrats[0]->PIN);
                    $content = '
                            <html>
                            <head>
                            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                            </head>
                            <body>
                            <div></div>
                            <p style="font-family:Arial, Helvetica, sans-serif">' . $email_text1 . ' </p>
                            <p style="font-family:Arial, Helvetica, sans-serif">' . $email_text2 . ' <span style="font-size:11px">'.$email_text3.'</span></p>
                            <p style="font-family:Arial, Helvetica, sans-serif">' . $email_text4 . ' <a href="http://www2.tortosa.cat/participacio-ciutadana/votacio-telematica/">' . $email_text5 . '</a></p>
                            <p style="font-family:Arial, Helvetica, sans-serif">' . $email_text6 . ' </p>
                            <p style="font-family:Arial, Helvetica, sans-serif; font-size:10px; margin-top:100px">' . $email_text7 . ' </p>
                            </body>
                            </html>
                        ';
                    if ($fc->sendEmail($lst_registrats[0]->PROCESP . " - " . $subject_email, $content, $params['EMAIL'], null, EMAIL_AVERIA)) {
                        $message = 'rda_missatge_confirmacio';
                        $fc->escribir_log_file(FOLDER_LOGS . "/Ins_".date('d-m-Y').".log", 'REC2 ENVI e1 '. $fc->setSpacesToString(6, $lst_registrats[0]->ID_CENS) .' '. $fc->setSpacesToString(6, $lst_registrats[0]->ID). ' ' . $params['EMAIL']);
                    }
                }
                else {
                    // Està registrat però no està confirmat.
                    $message = "rda_no_validat";
                    $fc->escribir_log_file(FOLDER_LOGS . "/Ins_".date('d-m-Y').".log", 'REC  ERRO 12 :: Dades: '.implode(' | ', $params));
                }
            }
            else {
                // No està a la taula de Registrats.
                $message = "rda_error_recuperacio";
                $fc->escribir_log_file(FOLDER_LOGS . "/Ins_".date('d-m-Y').".log", 'REC  ERRO 13 :: Dades: '.implode(' | ', $params));
            }
        }
        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");
        require_once (FOLDER_LANGUAGES."/inscripcio-participacio/".$lang."_inscripcio_participacio.cfg");
        $smarty->assign('url_ca','/inscripcio-participacio/recuperacio_dades_acces.php');
        $smarty->assign('url_es','/inscripcio-participacio/recuperacio_dades_acces.php?lang=es');
        $smarty->assign('lang',$lang);
        $smarty->assign('message', $message);
        $smarty->display("inscripcio-participacio/formulari_recuperacio_dades_acces.tpl");
    }

    function desencripta($str) {
        $fc = new \webtortosa\functions_controller();
        echo "text a desencriptar: [". $str ."].<br>";
        echo $fc->simple_decrypt($str);
    }

    function reenviament_inscripcio_v1($id_cens = null) {
        require_once (FOLDER_MODEL."/pressupostos-participatius.class.php");

        $fc = new \webtortosa\functions_controller();
        $ppdb = new \webtortosa\pressupostos_participatius();
        if($id_cens) {
            $params_inscrit = array(
                'ID_CENS'   => $id_cens
            );
        }
        //var_dump($params_inscrit);
        $lst_inscrits = $ppdb->getRegistrat($params_inscrit);

        //Enviament email
        $email_text1 = "Benvolgut/da";
        $email_text2 = "Rebeu aquest email atenent la sol·licitud d'inscripció per a la votació online dels Pressupostos Participatius 2018.";
        $email_text3 = "Si heu rebut aquest email sense el vostre coneixement, poseu-vos en contacte amb l'Ajuntament de Tortosa mitjançant correu electrònic a <a href='mailto:info@tortosa.cat'>info@tortosa.cat</a> o per telèfon al número 977 585 884 en horari d'atenció al públic.";
        $email_text4 = "Per finalitzar el procés d'inscripció cal que féu click al següent";
        $email_text5 = "enllaç";
        $email_text5_1 = "(cliqueu només un cop)";
        $email_text6 = "Quan ho feu, rebreu en aquest mateix compte de correu el PIN amb el que podreu efectuar la votació online, si així ho desitjeu.";
        $email_text7 = "Gràcies per participar";
        $email_text8 = "Sol·licitud inscripció per votació online";
        $email_text9 = "Aquest correu és informatiu, no respongueu a aquesta adreça.";
        $email_text10 = "Nota:";
        $email_text11 = "aquest correu pot trigar un temps màxim de 15 minuts.";
        $email_text12 = "Si transcorregut aquest temps no l'heu rebut:";
        $email_text13 = "Verifiqueu la bústia de l'spam.";
        $email_text14 = "Podeu contactar per correu electrònic: <a href='mailto:info@tortosa.cat'>info@tortosa.cat</a>.";
        $email_text15 = "Podeu trucar per telèfon al 977 585 884 en horari d'atenció al públic.";



        foreach($lst_inscrits as $item_inscrit) {
            if(($item_inscrit->PIN=="")&&($item_inscrit->VOTACIO_EFECTUADA==0)) {
                $hash = $fc->simple_encrypt($item_inscrit->ID_CENS . "|" .$item_inscrit->EMAIL. "|ca");
                $content = '
                    <html>
                    <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                    </head>
                    <body>
                    <div></div>
                    <p style="font-family:Arial, Helvetica, "Bitstream Vera Sans", sans-serif">'.$email_text1.'</p>
                    <p style="font-family:Arial, Helvetica, "Bitstream Vera Sans", sans-serif">'.$email_text2.'</p>
                    <p style="font-family:Arial, Helvetica, "Bitstream Vera Sans", sans-serif; margin-top:30px">'.$email_text4.' <a href="http://www2.tortosa.cat/inscripcio-participacio/validacio.php?t='.urlencode($hash).'">'.$email_text5.'</a>. '.$email_text5_1.'</p>
                    <p style="font-family:Arial, Helvetica, "Bitstream Vera Sans", sans-serif">'.$email_text6.'</p>
                    <p style="font-family:Arial, Helvetica, "Bitstream Vera Sans", sans-serif">'.$email_text10.' '.$email_text11.'<br/>'.$email_text12.'<ul><li style="font-family:Arial, Helvetica, sans-serif; color:#ED1F33;">'.$email_text13.'</li><li style="font-family:Arial, Helvetica, sans-serif">'.$email_text14.'</li><li style="font-family:Arial, Helvetica, sans-serif">'.$email_text15.'</li></ul></p>
                    <p style="font-family:Arial, Helvetica, "Bitstream Vera Sans", sans-serif">'.$email_text3.'</p>
                    <p style="font-family:Arial, Helvetica, "Bitstream Vera Sans", sans-serif">'.$email_text7.'</p>
                    <p style="font-family:Arial, Helvetica, sans-serif; font-size:10px; margin-top:100px">'.$email_text9.'</p>
                    </body>
                    </html>
                ';
                if($fc->sendEmail_v2($item_inscrit->PROCESP . " - " . $email_text8, $content, $item_inscrit->EMAIL, null, EMAIL_AVERIA)) {
                //if($fc->sendEmail($item_inscrit->PROCESP . " - " . $email_text8, $content, 'mariojuan@tortosa.cat', null, EMAIL_AVERIA)) {
                //if($fc->sendEmail_phpMailer($item_inscrit->PROCESP . " - " . $email_text8, $content, $item_inscrit->EMAIL, null, EMAIL_AVERIA, 'info@tortosa.cat')) {
                    $fc->escribir_log_file(FOLDER_LOGS . "/Ins_".date('d-m-Y').".log", 'INS9 ENVI e1 '. $fc->setSpacesToString(6, $item_inscrit->ID_CENS) .' '. $fc->setSpacesToString(6, $item_inscrit->ID). ' ' . $item_inscrit->EMAIL);
                    echo "Email enviat a: ". $item_inscrit->EMAIL ."<br>";
                    sleep(1);
                }
            }
        }
    }

    function reenviament_pins_v1($id_cens = null) {
        require_once (FOLDER_MODEL."/pressupostos-participatius.class.php");

        $fc = new \webtortosa\functions_controller();
        $ppdb = new \webtortosa\pressupostos_participatius();
        if($id_cens) {
            $text1 = '<p style="font-family:Arial, Helvetica, sans-serif">Benvolgut/da,</p>';
            $text2 = '<p style="font-family:Arial, Helvetica, sans-serif">Us informem que la fase de votació finalitza a les 14:00 h del proper divendres dia 11 de maig.</p>';
            $text5 = '<p style="font-family:Arial, Helvetica, sans-serif">Podeu votar fent click <a href="https://www2.tortosa.cat/participacio-ciutadana/votacio-telematica/ident.php?lang=ca" style="font-weight:bold">aquí</a></p>';
            $text6 = '<p style="font-family:Arial, Helvetica, sans-serif; margin-top:50px;">Si necessiteu assistència o informació addicional podeu contactar amb nosaltres: <ul><li style="font-family:Arial, Helvetica, sans-serif">per correu electrònic: <a href="mailto:info@tortosa.cat">info@tortosa.cat</a></li><li style="font-family:Arial, Helvetica, sans-serif">per telèfon: 977 585 884 (en horari d\'atenció al públic)</li></ul></p>';
            $text7 = '<p style="font-family:Arial, Helvetica, sans-serif">Gràcies per participar!</p>';
            $text8 = "<p style='font-family:Arial, Helvetica, sans-serif; font-size: 8px; margin-top:100px'>De conformitat amb l’article 5.1 de la Llei orgànica 15/1999, de 13 de desembre, de protecció de dades de caràcter personal (LOPD), us informem que les dades de caràcter personal que ens faciliteu en aquesta sol·licitud s'incorporaran al fitxer \"Gestió consistorial i institucional\" titularitat de l'Ajuntament de Tortosa amb la finalitat de la gestió de l'agenda institucional, gestió administrativa i correu electrònic de l'Ajuntament i dels serveis municipals. Les dades de caràcter personal que sol·licitem són necessàries per a la gestió de la vostra sol·licitud, per la qual cosa cal que empleneu els diferents camps, ja que en cas contrari ens serà impossible atendre degudament la vostra sol·licitud. Com a persona interessada podreu exercir gratuïtament els drets d'accés, de rectificació, de cancel·lació i, si escau, d'oposició, en els termes establerts per la LOPD enviant una sol·licitud per escrit, acompanyada d'una fotocòpia del vostre document d'identitat, a l’Ajuntament de Tortosa, plaça d'Espanya, 1. 43500 Tortosa.</p>";
            $params = array(
                'VALIDAT'   => 1,
                'ID_CENS'   => $id_cens
            );
            $lst_registrats = $ppdb->getRegistrat($params);
            $cont = 1;
            foreach($lst_registrats as $key=>$item_registrat) {
                if (($item_registrat->VOTACIO_EFECTUADA == 0) && ($item_registrat->VALIDAT == 1) && (!$ppdb->haVotatPPartCens($item_registrat->ID_CENS))) {
                    $text4 = '<p style="font-family:Arial, Helvetica, "Bitstream Vera Sans", sans-serif">Us recordem que el vostre PIN personal és <label style="font-weight:bold">' . $fc->simple_decrypt($item_registrat->PIN) . '</label></p>';
                    $content = $text1 . $text2 . $text4 . $text5 . $text6 . $text7 . $text8;
                    //if ($fc->sendEmail($item_registrat->PROCESP, $content, 'mariojuan@tortosa.cat', null, EMAIL_AVERIA)) {
                    if ($fc->sendEmail_v2($item_registrat->PROCESP, $content, strtolower($item_registrat->EMAIL), null, EMAIL_AVERIA)) {
                    //if ($fc->sendEmail_phpMailer($item_registrat->PROCESP, $content, strtolower($item_registrat->EMAIL), null, EMAIL_AVERIA, 'info@tortosa.cat')) {
                        $fc->escribir_log_file(FOLDER_LOGS . "/Ins_" . date('d-m-Y') . ".log", 'INS8 ENVI e1 ' . $fc->setSpacesToString(6, $item_registrat->ID_CENS) . ' ' . $fc->setSpacesToString(6, $item_registrat->ID) . ' ' . $item_registrat->EMAIL);
                        echo $cont . ":: email enviat a: " . strtolower($item_registrat->EMAIL) . " :: Id: " . $item_registrat->ID."<br>";
                        $cont++;
                        sleep(1);
                    }
                }
            }
        }
    }

    function test_email() {
        $fc = new \webtortosa\functions_controller();
        $subject = 'Chiste test paràula ámb áccents de test';
        $from = 'test@tortosa.cat';

        //$cc = 'cbardi@tortosa.cat';
        $to = 'mariojuan771@gmail.com';
        //$cco = 'mariojuan@tortosa.cat';

        $texto = '<p>- ¿Cuál es el portero que mejor para?
                  <p>¡El de PARA-GUAY!</p>';


        if($fc->sendEmail_phpMailer($subject, $texto, $to, $cc, $cco, $from)) {
            echo "email enviat";
        }
    }

    function reenviament_inscripcio_v2($id_cens = null) {

        require_once (FOLDER_MODEL."/pressupostos-participatius.class.php");

        $fc = new \webtortosa\functions_controller();
        $ppdb = new \webtortosa\pressupostos_participatius();
        if($id_cens) {
            $params_inscrit = array(
                'ID_CENS' => $id_cens
            );

            //var_dump($params_inscrit);
            $lst_inscrits = $ppdb->getRegistrat($params_inscrit);

            //Enviament email
            $assumpte = "Ampliat termini per votar fins les 24h";

            foreach ($lst_inscrits as $item_inscrit) {
                if (($item_inscrit->PIN == "") && ($item_inscrit->VOTACIO_EFECTUADA == 0)) {
                    $hash = $fc->simple_encrypt($item_inscrit->ID_CENS . "|" . $item_inscrit->EMAIL . "|ca");
                    $content = '
                        <html>
                        <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                        </head>
                        <body>
                        <div></div>
                        <p style="font-family:Arial, Helvetica, "Bitstream Vera Sans", sans-serif">Benvolgut/da</p>
                        <p style="font-family:Arial, Helvetica, "Bitstream Vera Sans", sans-serif">Encara sou a temps de votar.</p>
                        <p style="font-family:Arial, Helvetica, "Bitstream Vera Sans", sans-serif;"><label style="font-weight:bold">El termini s\'ha ampliat fins a les 24h d\'avui, divendres 11 de maig</label>.</p>
                        <p style="font-family:Arial, Helvetica, "Bitstream Vera Sans", sans-serif">Heu sol·licitat la inscripció però no l\'heu confirmat.</p>
                        <p style="font-family:Arial, Helvetica, "Bitstream Vera Sans", sans-serif">Podeu confirmar fent click <a href="http://www2.tortosa.cat/inscripcio-participacio/validacio.php?t=' . urlencode($hash) . '"><button style="background-color: #ED1F33; border: none; color: white; padding: 15px 32px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; cursor: pointer">AQUÍ</button></a></p>
                        <p style="font-family:Arial, Helvetica, "Bitstream Vera Sans", sans-serif">Quan ho feu, rebreu en aquest mateix compte de correu el PIN amb el que podreu efectuar la votació online, si així ho desitjeu.</p>
                        <p style="font-family:Arial, Helvetica, "Bitstream Vera Sans", sans-serif">Gràcies per participar!.</p>
                        <p style="font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-top:25px">Nota: aquest correu pot trigar un temps màxim de 15 minuts.<br/>Si transcorregut aquest temps no l\'heu rebut:<ul><li style="font-family:Arial, Helvetica, sans-serif; color:#ED1F33;font-size:13px;">Verifiqueu la bústia de l\'spam.</li><li style="font-family:Arial, Helvetica, sans-serif;font-size:13px;">Podeu contactar per correu electrònic: <a href=\'mailto:info@tortosa.cat\'>info@tortosa.cat</a>.</li><li style="font-family:Arial, Helvetica, sans-serif; font-size:13px;">Podeu trucar per telèfon al 977 585 884 en horari d\'atenció al públic.</li></ul></p>
                        <p style="font-family:Arial, Helvetica, sans-serif; font-size:10px; margin-top:100px">Aquest correu és informatiu, no respongueu a aquesta adreça.</p>
                        <p style="font-family:Arial, Helvetica, sans-serif; font-size: 8px; margin-top:100px">De conformitat amb l\'article 5.1 de la Llei orgànica 15/1999, de 13 de desembre, de protecció de dades de caràcter personal (LOPD), us informem que les dades de caràcter personal que ens faciliteu en aquesta sol·licitud s\'incorporaran al fitxer "Gestió consistorial i institucional" titularitat de l\'Ajuntament de Tortosa amb la finalitat de la gestió de l\'agenda institucional, gestió administrativa i correu electrònic de l\'Ajuntament i dels serveis municipals. Les dades de caràcter personal que sol·licitem són necessàries per a la gestió de la vostra sol·licitud, per la qual cosa cal que empleneu els diferents camps, ja que en cas contrari ens serà impossible atendre degudament la vostra sol·licitud. Com a persona interessada podreu exercir gratuïtament els drets d\'accés, de rectificació, de cancel·lació i, si escau, d\'oposició, en els termes establerts per la LOPD enviant una sol·licitud per escrit, acompanyada d\'una fotocòpia del vostre document d\'identitat, a l\'Ajuntament de Tortosa, plaça d\'Espanya, 1. 43500 Tortosa.</p>
                        </body>
                        </html>';

                    if($fc->sendEmail_v2($assumpte . ' - ' . $item_inscrit->PROCESP, $content, $item_inscrit->EMAIL, null, EMAIL_AVERIA)) {
                    //if ($fc->sendEmail_v2($assumpte . ' - ' . $item_inscrit->PROCESP, $content, 'mariojuan@tortosa.cat', null, EMAIL_AVERIA)) {
                        $fc->escribir_log_file(FOLDER_LOGS . "/Ins_".date('d-m-Y').".log", 'INS9 ENVI e1 '. $fc->setSpacesToString(6, $item_inscrit->ID_CENS) .' '. $fc->setSpacesToString(6, $item_inscrit->ID). ' ' . $item_inscrit->EMAIL);
                        echo "Email enviat a: " . $item_inscrit->EMAIL . "<br>";
                        sleep(1);
                    }
                }
            }
        }
        else {
            echo "falta valor a id_cens.";
        }
    }

    function reenviament_pins_v2($id_cens = null) {
        require_once (FOLDER_MODEL."/pressupostos-participatius.class.php");

        $fc = new \webtortosa\functions_controller();
        $ppdb = new \webtortosa\pressupostos_participatius();

        if($id_cens) {
            $assumpte = "Ampliat termini per votar fins les 24h";

            $content1 = '
                <p style="font-family:Arial, Helvetica, sans-serif">Benvolgut/da,</p>
                <p style="font-family:Arial, Helvetica, sans-serif">Encara sou a temps de votar.</p>
                <p style="font-family:Arial, Helvetica, "Bitstream Vera Sans", sans-serif;"><label style="font-weight:bold">El termini s\'ha ampliat fins a les 24h d\'avui, divendres 11 de maig</label>.</p>
                <p style="font-family:Arial, Helvetica, sans-serif">Us heu inscrit però no heu votat.</p>
            ';

            $content3 = '
                <p style="font-family:Arial, Helvetica, "Bitstream Vera Sans", sans-serif">Podeu votar fent click <a href="https://www2.tortosa.cat/participacio-ciutadana/votacio-telematica/ident.php?lang=ca"><button style="background-color: #ED1F33; border: none; color: white; padding: 15px 32px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; cursor: pointer">AQUÍ</button></a></p>
                <p style="font-family:Arial, Helvetica, "Bitstream Vera Sans", sans-serif">Gràcies per participar!.</p>
                <p style="font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-top:25px">Si necessiteu assistència o informació addicional podeu contactar amb nosaltres:<ul><li style="font-family:Arial, Helvetica, sans-serif;font-size:13px;">per correu electrònic: <a href=\'mailto:info@tortosa.cat\'>info@tortosa.cat</a>.</li><li style="font-family:Arial, Helvetica, sans-serif; font-size:13px;">per telèfon al 977 585 884 en horari d\'atenció al públic.</li></ul></p>
                <p style="font-family:Arial, Helvetica, sans-serif; font-size:10px; margin-top:100px">Aquest correu és informatiu, no respongueu a aquesta adreça.</p>
                <p style="font-family:Arial, Helvetica, sans-serif; font-size: 8px; margin-top:100px">De conformitat amb l\'article 5.1 de la Llei orgànica 15/1999, de 13 de desembre, de protecció de dades de caràcter personal (LOPD), us informem que les dades de caràcter personal que ens faciliteu en aquesta sol·licitud s\'incorporaran al fitxer "Gestió consistorial i institucional" titularitat de l\'Ajuntament de Tortosa amb la finalitat de la gestió de l\'agenda institucional, gestió administrativa i correu electrònic de l\'Ajuntament i dels serveis municipals. Les dades de caràcter personal que sol·licitem són necessàries per a la gestió de la vostra sol·licitud, per la qual cosa cal que empleneu els diferents camps, ja que en cas contrari ens serà impossible atendre degudament la vostra sol·licitud. Com a persona interessada podreu exercir gratuïtament els drets d\'accés, de rectificació, de cancel·lació i, si escau, d\'oposició, en els termes establerts per la LOPD enviant una sol·licitud per escrit, acompanyada d\'una fotocòpia del vostre document d\'identitat, a l\'Ajuntament de Tortosa, plaça d\'Espanya, 1. 43500 Tortosa.</p>

            ';
            $params = array(
                'VALIDAT'   => 1,
                'ID_CENS'   => $id_cens
            );
            $lst_registrats = $ppdb->getRegistrat($params);
            $cont = 1;
            foreach($lst_registrats as $key=>$item_registrat) {
                if (($item_registrat->VOTACIO_EFECTUADA == 0) && ($item_registrat->VALIDAT == 1) && (!$ppdb->haVotatPPartCens($item_registrat->ID_CENS))) {
                    $content2 = '<p style="font-family:Arial, Helvetica, "Bitstream Vera Sans", sans-serif">Us recordem que el vostre PIN personal és <label style="font-weight:bold">' . $fc->simple_decrypt($item_registrat->PIN) . '</label></p>';
                    $content = $content1 . $content2 . $content3;
                    //if ($fc->sendEmail($assumpte . ' - ' . $item_registrat->PROCESP, $content, 'mariojuan@tortosa.cat', null, EMAIL_AVERIA)) {
                    if ($fc->sendEmail_v2($assumpte . ' - ' . $item_registrat->PROCESP, $content, strtolower($item_registrat->EMAIL), null, EMAIL_AVERIA)) {
                        //if ($fc->sendEmail_phpMailer($item_registrat->PROCESP, $content, strtolower($item_registrat->EMAIL), null, EMAIL_AVERIA, 'info@tortosa.cat')) {
                        $fc->escribir_log_file(FOLDER_LOGS . "/Ins_" . date('d-m-Y') . ".log", 'INS8 ENVI e1 ' . $fc->setSpacesToString(6, $item_registrat->ID_CENS) . ' ' . $fc->setSpacesToString(6, $item_registrat->ID) . ' ' . $item_registrat->EMAIL);
                        echo $cont . ":: email enviat a: " . strtolower($item_registrat->EMAIL) . " :: Id: " . $item_registrat->ID."<br>";
                        $cont++;
                        sleep(1);
                    }
                }
            }
        }
    }
}
?>