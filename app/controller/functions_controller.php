<?php
/**
 * Created by SeGeTIC.
 * User: Mario Juan
 * Date: 14/04/14
 * Time: 08:00
 * To change this template use File | Settings | File Templates.
 */

namespace webtortosa;

class functions_controller {
    function __construct() {
        
    }
    
    function resizeImagen($ruta, $nombre, $alto, $ancho,$nombreN,$extension){
        $rutaImagenOriginal = $ruta.$nombre;
        if($extension == 'GIF' || $extension == 'gif'){
        $img_original = imagecreatefromgif($rutaImagenOriginal);
        }
        if($extension == 'jpg' || $extension == 'JPG'){
        $img_original = imagecreatefromjpeg($rutaImagenOriginal);
        }
        if($extension == 'png' || $extension == 'PNG'){
        $img_original = imagecreatefrompng($rutaImagenOriginal);
        }
        $max_ancho = $ancho;
        $max_alto = $alto;
        list($ancho,$alto)=getimagesize($rutaImagenOriginal);
        $x_ratio = $max_ancho / $ancho;
        $y_ratio = $max_alto / $alto;
        if( ($ancho <= $max_ancho) && ($alto <= $max_alto) ){//Si ancho 
      	$ancho_final = $ancho;
    		$alto_final = $alto;
    	} elseif (($x_ratio * $alto) < $max_alto){
    		$alto_final = ceil($x_ratio * $alto);
    		$ancho_final = $max_ancho;
    	} else{
    		$ancho_final = ceil($y_ratio * $ancho);
    		$alto_final = $max_alto;
    	}
        $tmp=imagecreatetruecolor($ancho_final,$alto_final);
        imagecopyresampled($tmp,$img_original,0,0,0,0,$ancho_final, $alto_final,$ancho,$alto);
        imagedestroy($img_original);
        $calidad=100;
        imagejpeg($tmp,$ruta.$nombreN,$calidad);
        
    }

    function normaldate_to_mysql ($fecha) {
        if($fecha!="") {
            list($dd, $mm, $yy) = explode("/", $fecha);
            $mifecha = new \DateTime();
            $mifecha->setDate($yy, $mm, $dd);
            return $mifecha->format('Y/m/d');
        }
        else {
            return null;
        }
    }

    function normaldate_to_spanish ($fecha) {
        if($fecha!="") {
            list($dd, $mm, $yy) = explode("/", $fecha);
            if(($dd==00)&&($mm==00)&&($yy==0000))
                return null;
            $mifecha = new \DateTime();
            $mifecha->setDate($dd, $mm, $yy);
            return $mifecha->format('d/m/Y');
        }
        else {
            return null;
        }
    }

    function utf8_converter($array) {
        array_walk_recursive($array, function(&$item, $key){
            foreach ($item as &$value) {
                if(!mb_detect_encoding($value, 'utf-8', true)){
                    $value = utf8_encode($value);
                }
            }
        });
        return $array;
    }

    /*
     * Funció per evitar l'injecció SQL i el XSS o Cross-site scripting
     */
    function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    function escribir_log_file($archivo, $contenido){

        $output = date('d/m/Y G:i:s').' '.$this->setSpacesToString(15, $this->get_client_ip()).' :: '.$this->delete_accents($contenido).PHP_EOL;
        file_put_contents($archivo, $output, FILE_APPEND);
    }

    function checkRemoteFile($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if(curl_exec($ch)!==FALSE)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //envia un email informando
    function sendEmail($subject, $texto, $to, $cc = null, $cco = null){
        $cabeceras = "Content-type: text/html\r\n".
            "From: Ajuntament de Tortosa <no-reply@tortosa.cat>\r\n";
        if($cc)
            $cabeceras .= "Cc: ".$cc."\r\n";
        if($cco)
            $cabeceras .= "Bcc: ".$cco."\r\n";
        return mail($to, "=?utf-8?B?".base64_encode($subject)."?=",$texto, $cabeceras);
    }

    //envia un email informando
    function sendEmail_v2($subject, $texto, $to, $cc = null, $cco = null){
        $cabeceras = "Content-type: text/html\r\n".
            "From: Ajuntament de Tortosa <info@tortosa.cat>\r\n";
        if($cc)
            $cabeceras .= "Cc: ".$cc."\r\n";
        if($cco)
            $cabeceras .= "Bcc: ".$cco."\r\n";
        return mail($to, "=?utf-8?B?".base64_encode($subject)."?=",$texto, $cabeceras);
    }

    //Envia email con libreria PHPMailer
    function sendEmail_phpMailer($subject, $texto, $to, $cc = null, $cco = null, $from = null) {
        require_once (FOLDER_LIBS."/SMTP.php");
        require_once (FOLDER_LIBS."/Exception.php");
        require_once (FOLDER_LIBS."/PHPMailer.php");

        // Import PHPMailer classes into the global namespace
        // These must be at the top of your script, not inside a function
        //use PHPMailer\PHPMailer\PHPMailer;
        //use PHPMailer\PHPMailer\Exception;


        $mail = new \PHPMailer\PHPMailer\PHPMailer();             // Passing `true` enables exceptions
        try {
            //Server settings
            $mail->CharSet = 'UTF-8';
            $mail->SMTPDebug = 0;                                 // Enable verbose debug output
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = 'smtp.altanet.org';                     // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                              // Enable SMTP authentication
            $mail->Username = 'infotortosa';                      // SMTP username
            $mail->Password = 'zewaqy';                           // SMTP password
            //$mail->SMTPSecure = 'tls';                          // Enable TLS encryption, `ssl` also accepted
            //$mail->SMTPSecure = 'ssl';
            $mail->Port = 25;                                     // TCP port to connect to

            //Recipients
            if($from)
                $mail->setFrom($from, 'Ajuntament de Tortosa');
            else
                $mail->setFrom('info@tortosa.cat', 'Ajuntament de Tortosa');

            $mail->addAddress($to);                               // Add a recipient

            if($cc)
                $mail->addCC($cc);

            if($cco)
                $mail->addBCC($cco);

            //Attachments
            //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
            //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $subject = "=?utf-8?B?".base64_encode($subject)."=?=";
            $mail->Subject = $subject;
            $mail->Body    = $texto;
            $mail->AltBody = strip_tags($texto);

            $mail->send();
            return true;

        } catch (Exception $e) {
            echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
            return false;
        }
    }

    // Obtiene la IP address de un usuario
    function get_client_ip() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    //Obté el tipus de dispositiu de l'usuari
    function detectDevice(){
        $userAgent = $_SERVER["HTTP_USER_AGENT"];
        $devicesTypes = array(
            "computer" => array("msie 10", "msie 9", "msie 8", "windows.*firefox", "windows.*chrome", "x11.*chrome", "x11.*firefox", "macintosh.*chrome", "macintosh.*firefox", "opera"),
            "tablet"   => array("tablet", "android", "ipad", "tablet.*firefox"),
            "mobile"   => array("mobile ", "android.*mobile", "iphone", "ipod", "opera mobi", "opera mini"),
            "bot"      => array("googlebot", "mediapartners-google", "adsbot-google", "duckduckbot", "msnbot", "bingbot", "ask", "facebook", "yahoo", "addthis")
        );
        foreach($devicesTypes as $deviceType => $devices) {
            foreach($devices as $device) {
                if(preg_match("/" . $device . "/i", $userAgent)) {
                    $deviceName = $deviceType;
                }
            }
        }
        return ucfirst($deviceName);
    }

    //Filtra Strings de las consultas de la bbdd de SQL Server
    function filtrar_mssql($input) {
        $text2_arr = str_split(utf8_encode($input));
        $text2_arr_def = array();
        foreach($text2_arr as &$item) {
            //echo $item." > ". ord($item) ."<br>";
            switch(ord($item)) {
                case 146:
                    $item = "'";
                    break;
                case 243:
                    $item = "'";
                    break;
                case 183:
                    $item = "'";
                    break;
                case 231:
                    $item = "'";
                    break;
                case 237:
                    $item = "'";
                    break;
                case 147:
                    $item = "'";
                    break;
                case 233:
                    $item = "'";
                    break;
                case 180:
                    $item = "'";
                    break;
                case 148:
                    $item = "\"";
                    break;
            }
            if(ord($item)!=194)
                $text2_arr_def[] = $item;
        }
        $output = addslashes(str_replace("''", "'", implode($text2_arr_def)));
        return $output;
    }

    /*
     * Funció que guarda un cap de traducció.
     */
    function save_traduccio($idioma, $taula, $id_registre, $camp, $valor) {
        $dades_traduccio = new \webtortosa\traduccio();
        $traduccio_arr = array(
            'IDIOMA'        => $idioma,
            'TAULA'         => $taula,
            'ID_REGISTRE'   => $id_registre,
            'CAMP'          => $camp,
            'VALOR'         => $valor
        );
        $dades_traduccio->deleteTraduccio($traduccio_arr);
        $dades_traduccio->addTraduccio($traduccio_arr);
    }

    /*
     * Funció que obté les dades d'una traducció.
     */
    function get_traduccio($idioma, $taula, $id_registre, $camp) {
        $dades_traduccio = new \webtortosa\traduccio();
        $traduccio_arr = array(
            'ID_REGISTRE'   => $id_registre,
            'IDIOMA'        => $idioma,
            'TAULA'         => $taula,
            'CAMP'          => $camp
        );
        $item_traduccio = $dades_traduccio->getTraduccio($traduccio_arr);
        return $item_traduccio[0]->VALOR;
    }

    /*
     * Funció que esborra les dades d'una traducció.
     */
    function delete_traduccio($idioma, $taula, $id_registre, $camp) {
        $dades_traduccio = new \webtortosa\traduccio();
        $params = array(
            'ID_REGISTRE' => $id_registre,
            'IDIOMA' => $idioma,
            'TAULA' => $taula,
            'CAMP' => $camp
        );
        $dades_traduccio->deleteTraduccio($params);
    }

    //Genera un pin de n caràcters alfanumerics
    function pinGenerate($num_cars) {
        //$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $alphabet = "abcdefghijkmnopqrstuwxyzABCDEFGHJKLMNOPQRSTUWXYZ123456789";
        $pin = '';
        for ($i = 0; $i < $num_cars; $i++) {
            $n = rand(0, strlen($alphabet)-1);
            $pin .= $alphabet[$n];
        }
        return $pin;
    }

    //Encriptació en base64
    function simple_encrypt($text)     {
        return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, TEXT_ENCRYPT, $text, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));
    }

    //Desencriptació en base64
    function simple_decrypt($text)     {
        return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, TEXT_ENCRYPT, base64_decode($text), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));
    }

    //Esborra els accents d'un string
    function delete_accents($cadena){
        //Codificamos la cadena en formato utf8 en caso de que nos de errores
        //$cadena = utf8_encode($cadena);

        //Ahora reemplazamos las letras
        $cadena = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
            array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $cadena
        );

        $cadena = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
            array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $cadena );

        $cadena = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
            array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $cadena );

        $cadena = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
            array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $cadena );

        $cadena = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
            array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $cadena );

        $cadena = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),
            array('n', 'N', 'c', 'C'),
            $cadena
        );

        return $cadena;
    }

    //Omple un número determinat d'espais amb un string. S'utilitza per composar els logs.
    function setSpacesToString($num_spaces = 0, $input_text, $location = null) {
        $num_cars = strlen($input_text);
        $diferencia = $num_spaces - $num_cars;
        for($i=0;$i<$diferencia;$i++) {
            if($location)
                $input_text = $input_text . ' ';
            else
                $input_text = ' ' . $input_text;
        }
        return $input_text;
    }

    function setTimeStamp($url_target, $path) {
        $url = "https://freetsa.org/screenshot.php";

        $data = array(
            'screenshot' => $url_target,
            'delay'      => n

        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        //curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/timestamp-query'));
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0");
        $binary_response_string = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        file_put_contents($path, $binary_response_string);
    }
}