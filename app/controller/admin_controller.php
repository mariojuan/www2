<?php
/**
 * Created by Ajuntament de Tortosa.
 * User: Mario Juan
 * Date: 19/01/14
 * Time: 23:18
 * To change this template use File | Settings | File Templates.
 */

namespace webtortosa;

session_start();
require_once (FOLDER_CONTROLLER."/functions_controller.php");
require_once (FOLDER_LIBS."/smarty/Smarty_admin.class.php");
require_once (FOLDER_MODEL."/db.class.php");
require_once (FOLDER_MODEL."/usuari.class.php");
require_once (FOLDER_MODEL."/urbanisme.class.php");
require_once (FOLDER_MODEL."/eleccions.class.php");
require_once (FOLDER_MODEL."/tipologia.class.php");
require_once (FOLDER_MODEL."/tematica.class.php");
require_once (FOLDER_MODEL."/banners/banner.class.php");
require_once (FOLDER_MODEL."/telefons/telefon.class.php");
require_once (FOLDER_MODEL."/headline.class.php");
require_once (FOLDER_MODEL."/presparticipa.class.php");
require_once (FOLDER_MODEL."/indicadors-transparencia/indicador.class.php");
require_once (FOLDER_MODEL."/ofertes-ocupacio/oferta-ocupacio.class.php");
require_once (FOLDER_MODEL."/traduccio.class.php");
require_once (FOLDER_MODEL."/logs.class.php");
require_once (FOLDER_MODEL."/fitxers.class.php");

class admin_controller extends functions_controller {
    function __construct($redirect=true) {
        if(isset($_POST))
            $this->timeout_users($_SESSION['tiempo'], $redirect);
    }

    function login($user, $password) {

        $smarty = new \webtortosa\Smarty_admin();

        if((!isset($user)&&(!isset($password)))) {
            $smarty->display("login.tpl");
        }
        else {
            //echo "user: ". $user;
            $dades_usuari = new \webtortosa\usuari();
            $params = array(
                'ORG_USUARI.LOGIN' => $user,
                'ORG_USUARI.PASSWORD' => $password,
                'ORG_USUARI.ACTIU' => 1
            );
            $items_usuari = $dades_usuari->getDataUser($params);
            if(count($items_usuari)>0) {
                $_SESSION['info_usuari'] = array('rol'=> $items_usuari[0]->ROL, 'contacte'=>$items_usuari[0]->NOM . " " . $items_usuari[0]->COGNOMS, 'unitat'=>$items_usuari[0]->UNITAT, 'login'=>$items_usuari[0]->LOGIN, 'id_usuari'=>$items_usuari[0]->ID, 'id_servei'=>$items_usuari[0]->ID_SERVEI);
                $tiempo_caducidad = time() + SESSION_TIME; //1 minuto
                setcookie('AJT', 'session', $tiempo_caducidad);
                header("Location:principal.php");
            }
            else {
                $smarty->assign('error','Dades d\'accés incorrectes');
                $smarty->display("login.tpl");
            }
        }
    }

    function principal() {
        $smarty = new \webtortosa\Smarty_admin();
        $smarty->display("head.tpl");
        $smarty->assign('info_usuari', $_SESSION['info_usuari']);
        $smarty->display("header.tpl");
        $smarty->assign('info_usuari', $_SESSION['info_usuari']);
        $smarty->display("menu.tpl");
        $smarty->display("principal.tpl");
        $smarty->display("footer.tpl");
    }

    /********************************************
     ********************************************
     ********************************************
             Ofertes Ocupació Functions
     ********************************************
     ********************************************
     ********************************************/
    function edit_oferta_ocupacio($params_values_get, $params_values_post) {
        $ofertes_ocupacio = new \webtortosa\oferta();
        $dades_organitzacio = new \webtortosa\usuari();
        $dades_traduccio = new \webtortosa\traduccio();
        $dades_file = new \webtortosa\fitxers();
        $smarty = new \webtortosa\Smarty_admin();
        $utils = new \webtortosa\functions_controller();

        $usuari = $_SESSION['info_usuari'];

        //var_dump($params_values_post);

        //Registre de les dades
        if($params_values_post){
            $lang = $params_values_post["lang"]!="" ? $params_values_post["lang"] : "ca";
            if(!array_key_exists ('BAIXA', $params_values_post))
                $params_values_post['BAIXA'] = 0;
            if(!array_key_exists ('VISIBLE', $params_values_post))
                $params_values_post['VISIBLE'] = 0;
            if($params_values_post["ID"]){
                //Edició del registre
                if($lang=="ca") {
                    // Llenguatge catala
                    $params_values_post["ID_USUARI"] = $usuari["id_usuari"];
                    $params_values_post["ID_SERVEI"] = $usuari["id_servei"];
                    $params_values_post["DATA_INICI_PRESENT_SOLLICITUDS"]       = $this->normaldate_to_mysql($params_values_post["DATA_INICI_PRESENT_SOLLICITUDS"]);
                    $params_values_post["DATA_FI_PRESENT_SOLLICITUDS"]          = $this->normaldate_to_mysql($params_values_post["DATA_FI_PRESENT_SOLLICITUDS"]);
                    $params_values_post["DATA_INICI_PRESENT_ALEGACIONS"]        = $this->normaldate_to_mysql($params_values_post["DATA_INICI_PRESENT_ALEGACIONS"]);
                    $params_values_post["DATA_FI_PRESENT_ALEGACIONS"]           = $this->normaldate_to_mysql($params_values_post["DATA_FI_PRESENT_ALEGACIONS"]);
                    $params_values_post["DATA_INICI_VIGENCIA_BORSA_TREBALL"]    = $this->normaldate_to_mysql($params_values_post["DATA_INICI_VIGENCIA_BORSA_TREBALL"]);
                    $params_values_post["DATA_INICI_VISUALITZACIO"]             = $this->normaldate_to_mysql($params_values_post["DATA_INICI_VISUALITZACIO"]);
                    $params_values_post["DATA_FI_VISUALITZACIO"]                = $this->normaldate_to_mysql($params_values_post["DATA_FI_VISUALITZACIO"]);
                    $params_values_post["DATA_PUBLICACIO"]                      = date('Y/m/d');
                    $params_values_post["HORA_PUBLICACIO"]                      = date("H:i:s");
                    $ofertes_ocupacio->updateOfertaOcupacio($params_values_post);
                }
                else {
                    // Altres Llenguatges
                    $utils->save_traduccio($lang, 'OO_OFERTA_OCUPACIO', $params_values_post["ID"], 'DESCRIPCIO_CURTA', $params_values_post['DESCRIPCIO_CURTA']);
                    $utils->save_traduccio($lang, 'OO_OFERTA_OCUPACIO', $params_values_post["ID"], 'OBJECTE', $params_values_post['OBJECTE']);
                    $utils->save_traduccio($lang, 'OO_OFERTA_OCUPACIO', $params_values_post["ID"], 'DENOMINACIO', $params_values_post['DENOMINACIO']);
                    $utils->save_traduccio($lang, 'OO_OFERTA_OCUPACIO', $params_values_post["ID"], 'REGIM_JURIDIC', $params_values_post['REGIM_JURIDIC']);
                    $utils->save_traduccio($lang, 'OO_OFERTA_OCUPACIO', $params_values_post["ID"], 'CARACTER', $params_values_post['CARACTER']);
                    $utils->save_traduccio($lang, 'OO_OFERTA_OCUPACIO', $params_values_post["ID"], 'ESCALA', $params_values_post['ESCALA']);
                    $utils->save_traduccio($lang, 'OO_OFERTA_OCUPACIO', $params_values_post["ID"], 'SUBESCALA', $params_values_post['SUBESCALA']);
                    $utils->save_traduccio($lang, 'OO_OFERTA_OCUPACIO', $params_values_post["ID"], 'CLASSE', $params_values_post['CLASSE']);
                    $utils->save_traduccio($lang, 'OO_OFERTA_OCUPACIO', $params_values_post["ID"], 'GRUP', $params_values_post['GRUP']);
                    $utils->save_traduccio($lang, 'OO_OFERTA_OCUPACIO', $params_values_post["ID"], 'SISTEMA_SELECCIO', $params_values_post['SISTEMA_SELECCIO']);
                    $utils->save_traduccio($lang, 'OO_OFERTA_OCUPACIO', $params_values_post["ID"], 'REQUISITS', $params_values_post['REQUISITS']);
                    $utils->save_traduccio($lang, 'OO_OFERTA_OCUPACIO', $params_values_post["ID"], 'DRETS_EXAMEN', $params_values_post['DRETS_EXAMEN']);
                }
                $id_oferta = $params_values_post["ID"];
            } else{
                //Alta de Registre
                $params_values_post["ID_USUARI"] = $usuari["id_usuari"];
                $params_values_post["ID_SERVEI"] = $usuari["id_servei"];
                // Aquestes dades han de ser segell de temps.
                $params_values_post["DATA_PUBLICACIO"] = date('Y/m/d');
                $params_values_post["HORA_PUBLICACIO"] = date("H:i:s");
                $id_oferta = $ofertes_ocupacio->addOfertaOcupacio($params_values_post);
            }
            //Registre dels fitxers
            $files_arr = explode(',',$params_values_post['FITXERS']);
            var_dump($files_arr);
            if(count($files_arr)>0) {
                $params_files = array(
                    'ID_TAULA'      => $id_oferta,
                    'TAULA'         => 'OO_OFERTA_OCUPACIO'
                );
                foreach($files_arr as $items_files) {
                    $params_files['NOM_FITXER'] = $items_files;
                    $dades_file->passaDeTemporalABoFitxer($params_files);
                }
            }
            //Captura amb segell de temps
            $nom_fitxer = 'ev-'.$id_oferta.'.pdf';
            $utils->setTimeStamp('http://www2.tortosa.cat/ofertes-ocupacio/fitxa.php?id='.$id_oferta.'&lang='.$lang, FOLDER_PUBLIC_FILES . '/ofertes_ocupacio/'.$nom_fitxer);
            $params_files_ev = array(
                'ID_TAULA'      => $id_oferta,
                'TAULA'         => 'OO_OFERTA_OCUPACIO',
                'NOM_FITXER'    => $nom_fitxer,
                'DESCRIPCIO'    => 'Evidencia amb segell de temps',
                'TIPUS_FITXER'  => 'pdf',
                'TAMANY'        => filesize ( FOLDER_PUBLIC_FILES . '/ofertes_ocupacio/'.$nom_fitxer ),
                'BAIXA'         => 0
            );
            $dades_file->deleteFitxer($params_files_ev);
            $dades_file->addFitxer($params_files_ev);

            $this->list_oferta_ocupacio(null);
            exit();
        }


        // Accés a la edició.
        if($params_values_get) {
            $lang = $params_values_get["lang"]!="" ? $params_values_get["lang"] : "ca";
            if($params_values_get["id"]){
                if($lang=="ca") {
                    $item = $ofertes_ocupacio->getOfertaOcupacio($params_values_get["id"], $usuari["id_usuari"]);
                    $params_files = array(
                        'ID_TAULA'  =>  $params_values_get["id"],
                        'TAULA'     =>  'OO_OFERTA_OCUPACIO'
                    );
                    if($usuari['rol']!="SuperAdministrador") {
                        $params_files['BAIXA'] = 1;
                    }
                    $items_files = $dades_file->getFitxers($params_files);
                    $smarty->assign('items_files', $items_files);
                    //var_dump($items_files);
                }
                else {
                    $item_traduccio = array();
                    $traduccio_arr = array(
                        'ID_REGISTRE'   => $params_values_get["id"],
                        'IDIOMA'        => $lang,
                        'TAULA'         => 'OO_OFERTA_OCUPACIO',
                        'CAMP'          => 'DESCRIPCIO_CURTA'
                    );
                    // Obtenim dades de traduccions.
                    $descripcio_curta = $utils->get_traduccio($lang, 'OO_OFERTA_OCUPACIO', $params_values_get["id"], 'DESCRIPCIO_CURTA');
                    $item_arr["DESCRIPCIO_CURTA"] = $descripcio_curta;
                    $objecte = $utils->get_traduccio($lang, 'OO_OFERTA_OCUPACIO', $params_values_get["id"], 'OBJECTE');
                    $item_arr["OBJECTE"] = $objecte;
                    $denominacio = $utils->get_traduccio($lang, 'OO_OFERTA_OCUPACIO', $params_values_get["id"], 'DENOMINACIO');
                    $item_arr["DENOMINACIO"] = $denominacio;
                    $regim_juridic = $utils->get_traduccio($lang, 'OO_OFERTA_OCUPACIO', $params_values_get["id"], 'REGIM_JURIDIC');
                    $item_arr["REGIM_JURIDIC"] = $regim_juridic;
                    $caracter = $utils->get_traduccio($lang, 'OO_OFERTA_OCUPACIO', $params_values_get["id"], 'CARACTER');
                    $item_arr["CARACTER"] = $caracter;
                    $escala = $utils->get_traduccio($lang, 'OO_OFERTA_OCUPACIO', $params_values_get["id"], 'ESCALA');
                    $item_arr["ESCALA"] = $escala;
                    $subescala = $utils->get_traduccio($lang, 'OO_OFERTA_OCUPACIO', $params_values_get["id"], 'SUBESCALA');
                    $item_arr["SUBESCALA"] = $subescala;
                    $classe = $utils->get_traduccio($lang, 'OO_OFERTA_OCUPACIO', $params_values_get["id"], 'CLASSE');
                    $item_arr["CLASSE"] = $classe;
                    $grup = $utils->get_traduccio($lang, 'OO_OFERTA_OCUPACIO', $params_values_get["id"], 'GRUP');
                    $item_arr["GRUP"] = $grup;
                    $sistema_seleccio = $utils->get_traduccio($lang, 'OO_OFERTA_OCUPACIO', $params_values_get["id"], 'SISTEMA_SELECCIO');
                    $item_arr["SISTEMA_SELECCIO"] = $sistema_seleccio;
                    $requisits = $utils->get_traduccio($lang, 'OO_OFERTA_OCUPACIO', $params_values_get["id"], 'REQUISITS');
                    $item_arr["REQUISITS"] = $requisits;
                    $drets_examen = $utils->get_traduccio($lang, 'OO_OFERTA_OCUPACIO', $params_values_get["id"], 'DRETS_EXAMEN');
                    $item_arr["DRETS_EXAMEN"] = $drets_examen;
                    /*

                    $item_traduccio[] = $dades_traduccio->getTraduccio($traduccio_arr);
                    $traduccio_arr['CAMP'] = "ESCALA";
                    $item_traduccio[] = $dades_traduccio->getTraduccio($traduccio_arr);
                    $traduccio_arr['CAMP'] = "SUBESCALA";
                    $item_traduccio[] = $dades_traduccio->getTraduccio($traduccio_arr);
                    $traduccio_arr['CAMP'] = "CLASSE";
                    $item_traduccio[] = $dades_traduccio->getTraduccio($traduccio_arr);
                    $traduccio_arr['CAMP'] = "GRUP";
                    $item_traduccio[] = $dades_traduccio->getTraduccio($traduccio_arr);
                    $traduccio_arr['CAMP'] = "SISTEMA_SELECCIO";
                    $item_traduccio[] = $dades_traduccio->getTraduccio($traduccio_arr);
                    $traduccio_arr['CAMP'] = "REQUISITS";
                    $item_traduccio[] = $dades_traduccio->getTraduccio($traduccio_arr);
                    $traduccio_arr['CAMP'] = "DRETS_EXAMEN";
                    $item_traduccio[] = $dades_traduccio->getTraduccio($traduccio_arr);
                    //Converteixo elements de traducció a indicador.
                    $item_arr = array();
                    foreach($item_traduccio as $traduccio) {
                        switch($traduccio->CAMP) {
                            case "DESCRIPCIO_CURTA":
                                $item_arr["DESCRIPCIO_CURTA"] = $traduccio->VALOR;
                                break;
                            case "OBJECTE":
                                $item_arr["OBJECTE"] = $traduccio->VALOR;
                                break;
                            case "DENOMINACIO":
                                $item_arr["DENOMINACIO"] = $traduccio->VALOR;
                                break;
                            case "REGIM_JURIDIC":
                                $item_arr["REGIM_JURIDIC"] = $traduccio->VALOR;
                                break;
                            case "CARACTER":
                                $item_arr["CARACTER"] = $traduccio->VALOR;
                                break;
                            case "ESCALA":
                                $item_arr["ESCALA"] = $traduccio->VALOR;
                                break;
                            case "SUBESCALA":
                                $item_arr["SUBESCALA"] = $traduccio->VALOR;
                                break;
                            case "CLASSE":
                                $item_arr["CLASSE"] = $traduccio->VALOR;
                                break;
                            case "GRUP":
                                $item_arr["GRUP"] = $traduccio->VALOR;
                                break;
                            case "SISTEMA_SELECCIO":
                                $item_arr["SISTEMA_SELECCIO"] = $traduccio->VALOR;
                                break;
                            case "REQUISITS":
                                $item_arr["REQUISTS"] = $traduccio->VALOR;
                                break;
                            case "DRETS_EXAMEN":
                                $item_arr["DRETS_EXAMEN"] = $traduccio->VALOR;
                                break;
                        }
                    }
                    */
                    $item_arr["ID"] = $params_values_get["id"];
                    $item = array();
                    $item[] = (object) $item_arr;
                }
            }
        }
        //S'obté els ENS
        $items_ens = $dades_organitzacio->getDataENS();
        $smarty->assign('items_ens', $items_ens);

        $smarty->assign('item', $item);


        $smarty->display("../templates/head.tpl");
        $smarty->assign('info_usuari', $_SESSION['info_usuari']);
        $smarty->display("../templates/header.tpl");
        $smarty->display("../templates/menu.tpl");

        $smarty->assign('lang', $lang);
        $smarty->display("ofertes_ocupacio/edit_oferta_ocupacio.tpl");
        $smarty->display("../templates/footer.tpl");
    }

    function list_oferta_ocupacio($params_values_get)
    {
        $usuari = $_SESSION['info_usuari'];
        $smarty = new \webtortosa\Smarty_admin();
        $smarty->display("head.tpl");
        $smarty->assign('info_usuari', $usuari);

        $smarty->display("header.tpl");
        $smarty->display("menu.tpl");

        $dades_oferta = new \webtortosa\oferta();

        $items_ofertes = $dades_oferta->getOfertaOcupacio(null, $usuari["id_usuari"]);

        /* Pagination */
        $total = count($items_ofertes);
        $page = $params_values_get['page'];
        $start = $page ? ($page - 1) * ITEMS_LIMIT : 0;

        if ($page == 0)
            $page = 1;
        $prev = $page - 1; //previous page is current page - 1
        $next = $page + 1; //next page is current page + 1
        $lastpage = ceil($total / ITEMS_LIMIT); //lastpage.

        $items_ofertes = $dades_oferta->getOfertaOcupacio(null, $usuari["id_usuari"], null, $start, ITEMS_LIMIT);

        $smarty->assign('items', $items_ofertes);
        $smarty->assign("lastpage",$lastpage);
        $smarty->assign("page",$page);
        $smarty->display("ofertes_ocupacio/list_oferta_ocupacio.tpl");
        $smarty->display("footer.tpl");

    }


    /********************************************
     ********************************************
     ********************************************
                Indicadors Functions
     ********************************************
     ********************************************
     ********************************************/

    function edit_indicador_grup($params_values_get, $params_values_post) {
        $dades_indicador = new \webtortosa\indicador();
        $dades_traduccio = new \webtortosa\traduccio();
        $smarty = new \webtortosa\Smarty_admin();
        $utils = new \webtortosa\functions_controller();

        $usuari = $_SESSION['info_usuari'];

        //Registre de les dades
        if($params_values_post){
            $lang = $params_values_post["lang"]!="" ? $params_values_post["lang"] : "ca";
            if(!array_key_exists ('BAIXA', $params_values_post))
                $params_values_post['BAIXA'] = 0;
            if(!array_key_exists ('VISIBLE', $params_values_post))
                $params_values_post['VISIBLE'] = 0;
            if($params_values_post["ID"]){
                //Edició del registre
                if($lang=="ca") {
                    // Llenguatge catala
                    $params_values_post["ID_USUARI"] = $usuari["id_usuari"];
                    $params_values_post["ID_SERVEI"] = $usuari["id_servei"];
                    $dades_indicador->updateIndicadorGrup($params_values_post);
                }
                else {
                    // Altres Llenguatges
                    $utils->save_traduccio($lang, 'IND_INDICADOR_GRUP', $params_values_post["ID"], 'GRUP_INDICADOR', $params_values_post['GRUP_INDICADOR']);

                }
            } else{
                $params_values_post["ID_USUARI"] = $usuari["id_usuari"];
                $params_values_post["ID_SERVEI"] = $usuari["id_servei"];
                $dades_indicador->addIndicadorGrup($params_values_post);
            }
            $this->list_indicador_grup(null);
            exit();
        }

        // Accés a la edició.
        if($params_values_get) {
            $lang = $params_values_get["lang"]!="" ? $params_values_get["lang"] : "ca";
            if($params_values_get["id"]){
                if($lang=="ca") {
                    $item = $dades_indicador->getIndicadorGrup($params_values_get["id"], $usuari["id_usuari"]);
                    //var_dump($item);
                }
                else {
                    $traduccio_arr = array(
                        'ID_REGISTRE'   => $params_values_get["id"],
                        'IDIOMA'        => $lang,
                        'TAULA'         => 'IND_INDICADOR_GRUP',
                        'CAMP'          => 'GRUP_INDICADOR'
                    );
                    $item_traduccio = $dades_traduccio->getTraduccio($traduccio_arr);
                    //Converteixo elements de traducció a indicador.
                    $item_arr = array();
                    foreach($item_traduccio as $traduccio) {
                        switch($traduccio->CAMP) {
                            case "GRUP_INDICADOR":
                                $item_arr["GRUP_INDICADOR"] = $traduccio->VALOR;
                                break;
                        }
                    }
                    $item_arr["ID"] = $params_values_get["id"];
                    $item = array();
                    $item[] = (object) $item_arr;
                }
            }
        }

        $smarty->assign('item', $item);


        $smarty->display("../templates/head.tpl");
        $smarty->assign('info_usuari', $_SESSION['info_usuari']);
        $smarty->display("../templates/header.tpl");
        $smarty->display("../templates/menu.tpl");


        $smarty->assign('lang', $lang);
        $smarty->display("indicadors/edit_indicador_grup.tpl");
        $smarty->display("../templates/footer.tpl");
    }

    function list_indicador_grup($params_values_get)
    {
        $usuari = $_SESSION['info_usuari'];
        $smarty = new \webtortosa\Smarty_admin();
        $smarty->display("head.tpl");
        $smarty->assign('info_usuari', $usuari);

        $smarty->display("header.tpl");
        $smarty->display("menu.tpl");

        $dades_indicador = new \webtortosa\indicador();

        $items_indicadors = $dades_indicador->getIndicadorGrup(null, $usuari["id_usuari"]);

        /* Pagination */
        $total = count($items_indicadors);
        $page = $params_values_get['page'];
        $start = $page ? ($page - 1) * ITEMS_LIMIT : 0;

        if ($page == 0)
            $page = 1;
        $prev = $page - 1; //previous page is current page - 1
        $next = $page + 1; //next page is current page + 1
        $lastpage = ceil($total / ITEMS_LIMIT); //lastpage.

        $items_indicadors = $dades_indicador->getIndicadorGrup(null, $usuari["id_usuari"], null, $start, ITEMS_LIMIT);

        $smarty->assign('items',$items_indicadors);
        $smarty->assign("lastpage",$lastpage);
        $smarty->assign("page",$page);
        $smarty->display("indicadors/list_indicador_grup.tpl");
        $smarty->display("footer.tpl");

    }

    function delete_indicador_grup ($params_values_get, $params_values_post) {

        $dades_indicador = new \webtortosa\indicador();
        $dades_traduccio = new \webtortosa\traduccio();

        $dades_indicador->deleteIndicadorGrup($params_values_get["id"]);

        $langs = unserialize (LANGUAGES_LIST);
        foreach($langs as $lang) {
            unset($params);
            $params = array(
                'ID_REGISTRE' => $params_values_get["id"],
                'IDIOMA' => $lang,
                'TAULA' => 'IND_INDICADOR_GRUP',
                'CAMP' => 'GRUP_INDICADOR'
            );
            //Com que sempre ho gestiona el rol de Administrador, se poden esborrar.
            $dades_traduccio->deleteTraduccio($params);
        }


        $this->list_indicador_grup(null);

    }

    /* Subgrups */

    function edit_indicador_subgrup($params_values_get, $params_values_post) {
        $dades_indicador = new \webtortosa\indicador();
        $dades_traduccio = new \webtortosa\traduccio();
        $utils = new \webtortosa\functions_controller();
        $smarty = new \webtortosa\Smarty_admin();

        $usuari = $_SESSION['info_usuari'];

        //Registre de les dades
        if($params_values_post){
            $lang = $params_values_post["lang"]!="" ? $params_values_post["lang"] : "ca";
            if(!array_key_exists ('BAIXA', $params_values_post))
                $params_values_post['BAIXA'] = 0;
            if(!array_key_exists ('VISIBLE', $params_values_post))
                $params_values_post['VISIBLE'] = 0;
            if($params_values_post["ID"]){
                //Edició del registre
                if($lang=="ca") {
                    // Llenguatge catala
                    $params_values_post["ID_USUARI"] = $usuari["id_usuari"];
                    $params_values_post["ID_SERVEI"] = $usuari["id_servei"];
                    $dades_indicador->updateIndicadorSubGrup($params_values_post);
                }
                else {
                    // Altres Llenguatges
                    $utils->save_traduccio($lang, 'IND_INDICADOR_SUBGRUP', $params_values_post["ID"], 'SUBGRUP_INDICADOR', $params_values_post['SUBGRUP_INDICADOR']);
                }
            } else{
                $params_values_post["ID_USUARI"] = $usuari["id_usuari"];
                $params_values_post["ID_SERVEI"] = $usuari["id_servei"];
                $dades_indicador->addIndicadorSubGrup($params_values_post);
            }
            $this->list_indicador_subgrup(null);
            exit();
        }

        // Accés a la edició.
        if($params_values_get) {
            $lang = $params_values_get["lang"]!="" ? $params_values_get["lang"] : "ca";
            if($params_values_get["id"]){
                if($lang=="ca") {
                    $item = $dades_indicador->getIndicadorSubGrup($params_values_get["id"], $usuari["id_usuari"]);
                    //var_dump($item);
                }
                else {
                    $traduccio_arr = array(
                        'ID_REGISTRE'   => $params_values_get["id"],
                        'IDIOMA'        => $lang,
                        'TAULA'         => 'IND_INDICADOR_SUBGRUP',
                        'CAMP'          => 'SUBGRUP_INDICADOR'
                    );
                    $item_traduccio = $dades_traduccio->getTraduccio($traduccio_arr);
                    //Converteixo elements de traducció a indicador.
                    $item_arr = array();
                    foreach($item_traduccio as $traduccio) {
                        switch($traduccio->CAMP) {
                            case "SUBGRUP_INDICADOR":
                                $item_arr["SUBGRUP_INDICADOR"] = $traduccio->VALOR;
                                break;
                        }
                    }
                    $item_arr["ID"] = $params_values_get["id"];
                    $item = array();
                    $item[] = (object) $item_arr;
                }
            }
        }

        //Grups d'Indicadors
        $grups = $dades_indicador->getIndicadorGrup();

        $smarty->assign('grups', $grups);
        $smarty->assign('item', $item);


        $smarty->display("../templates/head.tpl");
        $smarty->assign('info_usuari', $_SESSION['info_usuari']);
        $smarty->display("../templates/header.tpl");
        $smarty->display("../templates/menu.tpl");


        $smarty->assign('lang', $lang);
        $smarty->display("indicadors/edit_indicador_subgrup.tpl");
        $smarty->display("../templates/footer.tpl");
    }

    function list_indicador_subgrup($params_values_get)
    {
        $usuari = $_SESSION['info_usuari'];
        $smarty = new \webtortosa\Smarty_admin();
        $smarty->display("head.tpl");
        $smarty->assign('info_usuari', $usuari);

        $smarty->display("header.tpl");
        $smarty->display("menu.tpl");

        $dades_indicador = new \webtortosa\indicador();

        $items_indicadors = $dades_indicador->getIndicadorSubGrup(null, $usuari["id_usuari"]);

        /* Pagination */
        $total = count($items_indicadors);
        $page = $params_values_get['page'];
        $start = $page ? ($page - 1) * ITEMS_LIMIT : 0;

        if ($page == 0)
            $page = 1;
        $prev = $page - 1; //previous page is current page - 1
        $next = $page + 1; //next page is current page + 1
        $lastpage = ceil($total / ITEMS_LIMIT); //lastpage.

        $items_indicadors = $dades_indicador->getIndicadorSubGrup(null, $usuari["id_usuari"], null, $start, ITEMS_LIMIT);

        $smarty->assign('items',$items_indicadors);
        $smarty->assign("lastpage",$lastpage);
        $smarty->assign("page",$page);
        $smarty->display("indicadors/list_indicador_subgrup.tpl");
        $smarty->display("footer.tpl");

    }

    function delete_indicador_subgrup ($params_values_get, $params_values_post) {

        $dades_indicador = new \webtortosa\indicador();
        $dades_traduccio = new \webtortosa\traduccio();

        $dades_indicador->deleteIndicadorSubGrup($params_values_get["id"]);

        $langs = unserialize (LANGUAGES_LIST);
        foreach($langs as $lang) {
            unset($params);
            $params = array(
                'ID_REGISTRE' => $params_values_get["id"],
                'IDIOMA' => $lang,
                'TAULA' => 'IND_INDICADOR_SUBGRUP',
                'CAMP' => 'SUBGRUP_INDICADOR'
            );
            $dades_traduccio->deleteTraduccio($params);
        }
        $this->list_indicador_subgrup(null);

    }

    /* Indicadors */
    function edit_indicador($params_values_get, $params_values_post) {
        $dades_indicador = new \webtortosa\indicador();
        $dades_traduccio = new \webtortosa\traduccio();
        $utils = new \webtortosa\functions_controller();
        $smarty = new \webtortosa\Smarty_admin();

        $usuari = $_SESSION['info_usuari'];

        //Registre de les dades
        if($params_values_post){
            $lang = $params_values_post["lang"]!="" ? $params_values_post["lang"] : "ca";
            if(!array_key_exists ('BAIXA', $params_values_post))
                $params_values_post['BAIXA'] = 0;
            if(!array_key_exists ('VISIBLE', $params_values_post))
                $params_values_post['VISIBLE'] = 0;
            if($params_values_post["ID"]){
                //Modificació del registre
                if($lang=="ca") {
                    // Llenguatge catala
                    $params_values_post["ID_USUARI"] = $usuari["id_usuari"];
                    $params_values_post["ID_SERVEI"] = $usuari["id_servei"];
                    $dades_indicador->updateIndicador($params_values_post);
                    $dades_indicador->deleteIndicadorLink($params_values_post["ID"], 'ca');
                    for($i = 1; $i<=$params_values_post["max_links"];$i++) {
                        if($params_values_post["DESCRIPCIO".$i]!="" && $params_values_post["LINK".$i]!="") {
                            $params_link = array(
                                'DESCRIPCIO'    => $params_values_post["DESCRIPCIO" . $i],
                                'LINK'          => $params_values_post["LINK" . $i],
                                'ID_INDICADOR'  => $params_values_post["ID"],
                                'LLENGUATGE'    => 'ca'
                            );
                            $dades_indicador->addIndicadorLink($params_link);
                        }
                    }
                }
                else {
                    // Altres Llenguatges
                    // Camp Indicador
                    $utils->save_traduccio($lang, 'IND_INDICADOR', $params_values_post["ID"], 'INDICADOR', $params_values_post["INDICADOR"]);
                    //Camp Comentaris
                    $utils->save_traduccio($lang, 'IND_INDICADOR', $params_values_post["ID"], 'COMENTARIS', $params_values_post["COMENTARIS"]);
                    $dades_indicador->deleteIndicadorLink($params_values_post["ID"], $lang);
                    for($i = 1; $i<=$params_values_post["max_links"];$i++) {
                        if($params_values_post["DESCRIPCIO".$i]!="" && $params_values_post["LINK".$i]!="") {
                            $params_link = array(
                                'DESCRIPCIO' => $params_values_post["DESCRIPCIO" . $i],
                                'LINK' => $params_values_post["LINK" . $i],
                                'ID_INDICADOR' => $params_values_post["ID"],
                                'LLENGUATGE'    => $lang
                            );
                            $dades_indicador->addIndicadorLink($params_link);
                        }
                    }
                }
            } else{
                //Alta del registre.
                $params_values_post["ID_USUARI"] = $usuari["id_usuari"];
                $params_values_post["ID_SERVEI"] = $usuari["id_servei"];
                $id = $dades_indicador->addIndicador($params_values_post);
                for($i = 1; $i<=$params_values_post["max_links"]; $i++) {
                    $params_link = array(
                        'DESCRIPCIO'    => $params_values_post["DESCRIPCIO".$i],
                        'LINK'          => $params_values_post["LINK".$i],
                        'ID_INDICADOR'  => $id,
                        'LLENGUATGE'    => 'ca'
                    );
                    $dades_indicador->addIndicadorLink($params_link);
                }
            }
            $this->list_indicador(null);
            exit();
        }

        // Accés a la edició.
        if($params_values_get) {
            $lang = $params_values_get["lang"]!="" ? $params_values_get["lang"] : "ca";
            if($params_values_get["id"]){
                if($lang=="ca") {
                    $item = $dades_indicador->getIndicador($params_values_get["id"], $usuari["id_usuari"]);
                }
                else {
                    // Obtenim dades de traduccions.
                    $indicador = $utils->get_traduccio($lang, 'IND_INDICADOR', $params_values_get["id"], 'INDICADOR');
                    $item_arr["INDICADOR"] = $indicador;
                    $comentaris = $utils->get_traduccio($lang, 'IND_INDICADOR', $params_values_get["id"], 'COMENTARIS');
                    $item_arr["COMENTARIS"] = $comentaris;

                    $item_arr["ID"] = $params_values_get["id"];
                    $item = array();
                    $item[] = (object) $item_arr;
                }
                // Enllaços d'Indicadors.
                $links = $dades_indicador->getIndicadorLink($params_values_get["id"], $lang);
                $smarty->assign('links', $links);
            }
        }

        //Grups d'Indicadors
        $grups = $dades_indicador->getIndicadorGrup();
        $smarty->assign('grups', $grups);

        //Subgrups d'Indicadors
        $subgrups = $dades_indicador->getIndicadorSubGrup();
        $smarty->assign('subgrups', $subgrups);

        $smarty->assign('item', $item);


        $smarty->display("../templates/head.tpl");
        $smarty->assign('info_usuari', $_SESSION['info_usuari']);
        $smarty->display("../templates/header.tpl");
        $smarty->display("../templates/menu.tpl");


        $smarty->assign('lang', $lang);
        $smarty->display("indicadors/edit_indicador.tpl");
        $smarty->display("../templates/footer.tpl");
    }

    function list_indicador($params_values_get)
    {
        $usuari = $_SESSION['info_usuari'];
        $smarty = new \webtortosa\Smarty_admin();
        $smarty->display("head.tpl");
        $smarty->assign('info_usuari', $usuari);

        $smarty->display("header.tpl");
        $smarty->display("menu.tpl");

        $dades_indicador = new \webtortosa\indicador();

        $items_indicadors = $dades_indicador->getIndicador(null, $usuari["id_usuari"]);

        /* Pagination */
        $total = count($items_indicadors);
        $page = $params_values_get['page'];
        $start = $page ? ($page - 1) * ITEMS_LIMIT : 0;

        if ($page == 0)
            $page = 1;
        $prev = $page - 1; //previous page is current page - 1
        $next = $page + 1; //next page is current page + 1
        $lastpage = ceil($total / ITEMS_LIMIT); //lastpage.

        $items_indicadors = $dades_indicador->getIndicador(null, $usuari["id_usuari"], null, $start, ITEMS_LIMIT);
        //var_dump($items_indicadors);
        $smarty->assign('items',$items_indicadors);
        $smarty->assign("lastpage",$lastpage);
        $smarty->assign("page",$page);
        $smarty->display("indicadors/list_indicador.tpl");
        $smarty->display("footer.tpl");
    }

    function delete_indicador ($params_values_get, $params_values_post) {

        $dades_indicador = new \webtortosa\indicador();
        $dades_traduccio = new \webtortosa\traduccio();
        $utils = new \webtortosa\functions_controller();

        $dades_indicador->deleteIndicador($params_values_get["id"]);

        $langs = unserialize (LANGUAGES_LIST);
        //Esborro valors de traducció per a tots els llenguatges.
        foreach($langs as $lang) {
            $utils->delete_traduccio($lang, 'IND_INDICADOR', $params_values_get["id"], 'INDICADOR');
            $utils->delete_traduccio($lang, 'IND_INDICADOR', $params_values_get["id"], 'LINK');
            $utils->delete_traduccio($lang, 'IND_INDICADOR', $params_values_get["id"], 'COMENTARIS');
            $dades_indicador->deleteIndicadorLink($params_values_get["id"], $lang);
        }


        $this->list_indicador(null);

    }


    /********************************************
     ********************************************
     ********************************************
                Urbanisme Functions
     ********************************************
     ********************************************
     ********************************************/

    function delete_urbanisme_element ($params_values_get, $params_values_post) {
        $dades_elements_urbanisme = new \webtortosa\urbanisme();
        $params = array(
            "ID" => $params_values_get["id"]
        );
        $items = $dades_elements_urbanisme->getElementUrbanismeData($params);
        if(count($items)>0) {
            $element_urbanisme = $dades_elements_urbanisme->deleteRow("IMA_ELEMENT", "ID", $params_values_get["id"]);
            $link_element_urbanisme = $dades_elements_urbanisme->deleteRow("IMA_ELEMENT_LINK", "ID_ELEMENT", $params_values_get["id"]);
        }
        $this->list_urbanisme_elements();
    }

    function list_urbanisme_elements($params_values_get, $params_values_post) {
        $smarty = new \webtortosa\Smarty_admin();
        $smarty->display("head.tpl");
        $smarty->assign('info_usuari', $_SESSION['info_usuari']);
        $smarty->display("header.tpl");
        $smarty->assign('info_usuari', $_SESSION['info_usuari']);
        $smarty->display("menu.tpl");

        $dades_elements_urbanisme = new \webtortosa\urbanisme();
        $params = array(
            "TITOL" => $params_values_post["titol"]
        );
        $items_elements_urbanisme = $dades_elements_urbanisme->getElementUrbanismeData($params);

        // Setup Pagination vars
        $total = count($items_elements_urbanisme);
        $page = $params_values_get['page'];
        $start = $page ? ($page - 1) * ITEMS_LIMIT : 0;

        if($page==0)
            $page = 1;
        $prev = $page - 1; //previous page is current page - 1
        $next = $page + 1; //next page is current page + 1
        $lastpage = ceil($total/ITEMS_LIMIT); //lastpage.

        $items_elements_urbanisme = $dades_elements_urbanisme->getElementUrbanismeData($params, $start, ITEMS_LIMIT);
        $smarty->assign('items',$items_elements_urbanisme);
        $smarty->assign("lastpage",$lastpage);
        $smarty->assign("page",$page);
        $smarty->display("list_urbanisme_element.tpl");
        $smarty->display("footer.tpl");
    }

    function edit_urbanisme_element ($params_values_get, $params_values_post) {
        $dades_elements_urbanisme = new \webtortosa\urbanisme();
        if($params_values_post) {
            $params_values_post_links = $this->apostrofFilter($params_values_post);
            //Elimino les variables dels enllaços del post per poder fer el insert o update amb el pas de paràmetres
            //correcte
            for($i=0;$i<=100;$i++) {
                unset($params_values_post["TEXT_LINK_".$i]);
                unset($params_values_post["LINK_".$i]);
                unset($params_values_post["TIPUS_".$i]);
                unset($params_values_post["link_active_".$i]);
            }
            $params_values_post["ACTIVA"] = $params_values_post["ACTIVA"]=="on" ? "1" : "0";
            $params_values_post["DATA"] = $this->normaldate_to_mysql($params_values_post["DATA"]);
            unset($params_values_post["accio"]);
            if($params_values_post["ID"]=="") {
                $element = $dades_elements_urbanisme->addRow("IMA_ELEMENT", $this->apostrofFilter($params_values_post));
                $id_element = $element["mysql_insert_id"];
            }
            else {
                $id_element = $params_values_post["ID"];
                unset($params_values_post["ID"]);
                $params_values_post["ID"] = $id_element;
                $id_element_temp = $dades_elements_urbanisme->updateRow("IMA_ELEMENT", $params_values_post);
            }
            $link_element_urbanisme = $dades_elements_urbanisme->deleteRow("IMA_ELEMENT_LINK", "ID_ELEMENT", $id_element);
            for($i=0;$i<100;$i++) {
                if(($params_values_post_links["TEXT_LINK_".$i]!="") && ($params_values_post_links["LINK_".$i]!="") && ($params_values_post_links["link_active_".$i]=="1")) {
                    $params = array(
                        "ID_ELEMENT" => $id_element,
                        "TITOL"      => $params_values_post_links["TEXT_LINK_".$i],
                        "LINK"       => $params_values_post_links["LINK_".$i],
                        "TIPUS"      => $params_values_post_links["TIPUS_".$i]
                    );
                    $element = $dades_elements_urbanisme->addRow("IMA_ELEMENT_LINK", $params);
                }
            }
            $this->list_urbanisme_elements();
        }
        else {
            $smarty = new \webtortosa\Smarty_admin();
            $smarty->display("head.tpl");
            $smarty->assign('info_usuari', $_SESSION['info_usuari']);
            $smarty->display("header.tpl");
            $smarty->assign('info_usuari', $_SESSION['info_usuari']);
            $smarty->display("menu.tpl");

            //Carreguem els elements de menú
            $items_menu = $dades_elements_urbanisme->getMenuUrbanismeData(NULL);
            $smarty->assign('items_menu', $items_menu);

            if($params_values_get["id"]) {
                $params = array(
                    "ID"=>$params_values_get["id"]
                );
                $items = $dades_elements_urbanisme->getElementUrbanismeData($params);
                if (count($items)>0)
                    $smarty->assign('item', $items[0]);

                //Carreguem els enllaços de l'element
                $params = array(
                    "ID_ELEMENT" => $params_values_get["id"]
                );
                $link_items = $dades_elements_urbanisme->getLinksUrbanismeData($params);
                if (count($link_items)>0)
                    $smarty->assign('link_items', $link_items);

            }
            $smarty->display("edit_urbanisme_element.tpl");
            $smarty->display("footer.tpl");
        }
    }

    /********************************************
     ********************************************
     ********************************************
                Banners Functions
     ********************************************
     ********************************************
     ********************************************/

    function delete_banner ($params_values_get, $params_values_post) {

        $dades_banners = new \webtortosa\banner();
        $params = array(
            "ID" => $params_values_get["id"]
        );

        $items = $dades_banners->getBannersData($params);
        $directorioFitxer = FOLDER_PUBLIC_FILES . "/banners"; // directori a om es guardaran els fitxers adjunts

        if(file_exists($directorioFitxer.'/res_'.$dades_banners->nombreFoto($params_values_get["id"]))){
            unlink($directorioFitxer.'/res_'.$dades_banners->nombreFoto($params_values_get["id"]));
        }

        //Debugar eliminar fitxer
        if($dades_banners->nombreArchivo($params_values_get["id"])){
            unlink($directorioFitxer.'/'.$dades_banners->nombreArchivo($params_values_get["id"]));
        }
        
        $banner = $dades_banners->deleteRow("BAN_BANNER", "ID", $params_values_get["id"]);

        $this->list_banner();
        
    }

    function edit_banner($params_values_get, $params_values_post, $params_files) {
        if ($params_values_post) {
            $directorioFitxer = FOLDER_PUBLIC_FILES . "/banners"; // directori a om es guardaran els fitxers adjunts
            //Tractament de la imatge.
            if ($params_files["FOTO"]["name"]) {
                //Si havia foto abans i ara també s'ha d'esborrar l'anterior
                if (($params_values_post["FOTO_AUX"] != "") && (file_exists($directorioFitxer . '/' . $params_values_post["FOTO_AUX"]))) {
                    unlink($directorioFitxer . '/' . $params_values_post["FOTO_AUX"]);
                }

                $allowedExts = array("jpg", "jpeg", "gif", "png", "JPG", "GIF", "PNG");
                $extension = end(explode(".", $params_files["FOTO"]["name"]));

                if ((($params_files["FOTO"]["type"] == "image/gif")
                        || ($params_files["FOTO"]["type"] == "image/jpeg")
                        || ($params_files["FOTO"]["type"] == "image/png")
                        || ($params_files["FOTO"]["type"] == "image/pjpeg"))
                    && in_array($extension, $allowedExts)
                ) {

                    // el archivo es un JPG/GIF/PNG, entonces...
                    $foto = substr(md5(uniqid(rand())), 0, 10) . "." . $extension;
                    // almacenar imagen en el servidor
                    move_uploaded_file($params_files['FOTO']['tmp_name'], $directorioFitxer . '/' . $foto);
                    $resFoto = 'ban_' . $foto;
                    if ($params_values_post["TIPUS"] == 1) {
                        $this->resizeImagen($directorioFitxer . '/', $foto, 304, 895, $resFoto, $extension);
                    } else if ($params_values_post["TIPUS"] == 2) {
                        $this->resizeImagen($directorioFitxer . '/', $foto, 246, 146, $resFoto, $extension);
                    } else if ($params_values_post["TIPUS"] == 3) {
                        $this->resizeImagen($directorioFitxer . '/', $foto, 312, 740, $resFoto, $extension);
                    }
                    unlink($directorioFitxer . '/' . $foto);

                }
            }
            else {
                $resFoto = $params_values_post["FOTO_AUX"];
            }
            //Tractament fitxer adjunt.
            if ($params_files["FITXER_LINK"]["name"]) {

                if (($params_values_post["FITXER_LINK_AUX"] != "") && (file_exists($directorioFitxer . '/' . $params_values_post["FITXER_LINK_AUX"]))) {
                    unlink($directorioFitxer . '/' . $params_values_post["FITXER_LINK_AUX"]);
                }
                // El camp adjunt conté un fitxer...
                $extension = end(explode(".", $params_files["FITXER_LINK"]["name"]));
                $adjunt = substr(md5(uniqid(rand())), 0, 10) . "." . $extension;

                //Posem nou al directori
                move_uploaded_file($params_files['FITXER_LINK']['tmp_name'], $directorioFitxer . '/' . $adjunt);

            }
            else {
                $adjunt = $params_values_post["FITXER_LINK_AUX"];
            }
            //Tractem parametres que ens arriben
            $params_values_post["DATA"] = $this->normaldate_to_mysql($params_values_post["DATA"]);
            $params_values_post["DATA_INICI"] = $this->normaldate_to_mysql($params_values_post["DATA_INICI"]);
            $params_values_post["DATA_FI"] = $this->normaldate_to_mysql($params_values_post["DATA_FI"]);
            $params_values_post["ACTIU"] = $params_values_post["ACTIU"] == "on" ? "1" : "0";
            $objmodel_banner = new \webtortosa\banner();
            if ($params_values_post["ID"]) {
                $objmodel_banner->updateBanner(
                    $params_values_post["ID"],
                    $params_values_post["TITOL"],
                    $params_values_post["LINK"],
                    $adjunt,
                    $params_values_post["DATA"],
                    $params_values_post["DATA_INICI"],
                    $params_values_post["DATA_FI"],
                    $params_values_post["TIPUS"],
                    $params_values_post["ACTIU"],
                    $resFoto
                );
            } else {
                $params_banner = array(
                    'TITOL' => $params_values_post["TITOL"],
                    'LINK' => $params_values_post["LINK"],
                    'FITXER_LINK' => $adjunt,
                    'DATA' => $params_values_post["DATA"],
                    'DATA_INICI' => $params_values_post["DATA_INICI"],
                    'DATA_FI' => $params_values_post["DATA_FI"],
                    'TIPUS' => $params_values_post["TIPUS"],
                    'ACTIU' => $params_values_post["ACTIU"],
                    'IMATGE' => $resFoto,
                );
                $objmodel_banner->addRow("BAN_BANNER", $params_banner);
            }
            $this->list_banner();
            exit();
        }
        $smarty = new \webtortosa\Smarty_admin();

        $smarty->display("../templates/head.tpl");
        $smarty->assign('info_usuari', $_SESSION['info_usuari']);
        $smarty->display("../templates/header.tpl");
        $smarty->assign('info_usuari', $_SESSION['info_usuari']);
        $smarty->display("../templates/menu.tpl");

        $dades_banner = new \webtortosa\banner();

        if($params_values_get["id"]!="") {
            $item = $dades_banner->getBanner($params_values_get["id"]);
            $smarty->assign('item', $item);
        }
        $smarty->display("banners/edit_banner.tpl");
        $smarty->display("../templates/footer.tpl");
    }

    function list_banner($params_values_get = null)
    {
        $smarty = new \webtortosa\Smarty_admin();
        $smarty->display("head.tpl");
        $smarty->assign('info_usuari', $_SESSION['info_usuari']);
        $smarty->display("header.tpl");
        $smarty->assign('info_usuari', $_SESSION['info_usuari']);
        $smarty->display("menu.tpl");

        $dades_banners = new \webtortosa\banner();

        $params = null;

        $items_banners = $dades_banners->getBannersData($params);
        /* Pagination */
        $total = count($items_banners);
        $page = $params_values_get['page'];
        $start = $page ? ($page - 1) * ITEMS_LIMIT : 0;
        //echo $start ." - ". ITEMS_LIMIT;
        if ($page == 0)
            $page = 1;
        $prev = $page - 1; //previous page is current page - 1
        $next = $page + 1; //next page is current page + 1
        $lastpage = ceil($total / ITEMS_LIMIT); //lastpage.
        unset($items_banners);
        $items_banners = $dades_banners->getBannersData($params, $start, ITEMS_LIMIT);
        $smarty->assign('items',$items_banners);
        $smarty->assign("lastpage",$lastpage);
        $smarty->assign("page",$page);
        $smarty->display("banners/list_banners.tpl");
        $smarty->display("footer.tpl");
        
    }

    /********************************************
     ********************************************
     ********************************************
                Telefons Functions
     ********************************************
     ********************************************
     ********************************************/

    function delete_telefon ($params_values_get, $params_values_post) {

        $dades_telefon = new \webtortosa\telefon();

        $dades_telefon->deleteRow("TEL_TELEFON", "ID", $params_values_get["id"]);

        $this->list_telefon();
        
    }

    function edit_telefon($params_values_get, $params_values_post) {

        //Creem el nou tel que ens ve
        if($params_values_post){

            $objmodel_telefon = new \webtortosa\telefon();
            $params_values_post["SERVEI"] = str_replace("'", "''", $params_values_post["SERVEI"]);

            if($params_values_post["ID"]){
                //Editem
                $objmodel_telefon->updateTelefon($params_values_post["ID"], 
                                                $params_values_post["SERVEI"],
                                                $params_values_post["CORREU"],
                                                $params_values_post["TELEFON"]
                                                );

            }else{
                //Creem
                $params_tel = array (
                    'SERVEI'=>$params_values_post["SERVEI"],
                    'CORREU'=>$params_values_post["CORREU"],
                    'TELEFON'=>$params_values_post["TELEFON"],
                );

                $objmodel_telefon->addRow("TEL_TELEFON", $params_tel);

            }

            $this->list_telefon();

        }

        //Si no, Editem el telefon que ens ve
        else if ($params_values_get["id"] != null) {

            $smarty = new \webtortosa\Smarty_admin();

            $smarty->display("../templates/head.tpl");
            $smarty->assign('info_usuari', $_SESSION['info_usuari']);
            $smarty->display("../templates/header.tpl");
            $smarty->assign('info_usuari', $_SESSION['info_usuari']);
            $smarty->display("../templates/menu.tpl");

            $dades_telefon = new \webtortosa\telefon();
            $item = $dades_telefon->getTelefon($params_values_get["id"]);

            //var_dump($item);

            $smarty->assign('item',$item);
            $smarty->display("telefons/edit_tel.tpl");
            $smarty->display("../templates/footer.tpl");

        }

        
        //Si no, mostramos formulario para crear nuevo
        else if (!$params_values_get["id"] && !$params_values_get["page"]) {

            $smarty = new \webtortosa\Smarty_admin();

            $smarty->display("../templates/head.tpl");
            $smarty->assign('info_usuari', $_SESSION['info_usuari']);
            $smarty->display("../templates/header.tpl");
            $smarty->assign('info_usuari', $_SESSION['info_usuari']);
            $smarty->display("../templates/menu.tpl");

            $smarty->display("telefons/edit_tel.tpl");
            $smarty->display("../templates/footer.tpl");

        }
        
    }

    function list_telefon($params_values_get=1)
    {

        $smarty = new \webtortosa\Smarty_admin();
        $smarty->display("head.tpl");
        $smarty->assign('info_usuari', $_SESSION['info_usuari']);
        $smarty->display("header.tpl");
        $smarty->assign('info_usuari', $_SESSION['info_usuari']);
        $smarty->display("menu.tpl");

        $dades_telefons = new \webtortosa\telefon();
        if ($_SESSION['info_usuari']['rol'] != "Superadministrador") {
            $params = array(
                "LOGIN" => $_SESSION['info_usuari']['login']
            );
        }
        else {
            $params = null;
        }
        $items_telefons = $dades_telefons->getTelefonsData($params);
        /* Pagination */
        $total = count($items_telefons);
        $page = $params_values_get['page'];
        $start = $page ? ($page - 1) * ITEMS_LIMIT : 0;

        if ($page == 0)
            $page = 1;
        $prev = $page - 1; //previous page is current page - 1
        $next = $page + 1; //next page is current page + 1
        $lastpage = ceil($total / ITEMS_LIMIT); //lastpage.

        $items_telefons = $dades_telefons->getTelefonsData($params, $start, ITEMS_LIMIT);
        $smarty->assign('items',$items_telefons);
        $smarty->assign("lastpage",$lastpage);
        $smarty->assign("page",$page);
        $smarty->display("telefons/list_tel.tpl");
        $smarty->display("footer.tpl");
        
    }

    /********************************************
     ********************************************
     ********************************************
                Headlines Functions
     ********************************************
     ********************************************
     ********************************************/

    function delete_headline ($params_values_get, $params_values_post) {
        $dades_headlines = new \webtortosa\headline();
        $params = array(
            "ID" => $params_values_get["id"]
        );
        $items = $dades_headlines->getHeadlinesData($params);
        if(count($items)>0) {
            $directorio = FOLDER_IMAGES . "/headlines"; // directori a on es guardaran les imatges
            if(file_exists($directorio.'/res_'.$items[0]->FOTO))
                unlink($directorio.'/res_'.$items[0]->FOTO);
            if(file_exists($directorio.'/min_'.$items[0]->FOTO))
                unlink($directorio.'/min_'.$items[0]->FOTO);

            $directorio = FOLDER_PUBLIC_FILES . "/headlines"; // directori a om es guardaran els fitxers adjunts
            if(file_exists($directorio.'/'.$items[0]->ADJUNT))
                unlink($directorio.'/'.$items[0]->ADJUNT);

            $headline = $dades_headlines->deleteRow("HEAD_HEADLINE", "ID", $params_values_get["id"]);
            $headline_tematica = $dades_headlines->deleteRow("HEAD_TEMATICA", "ID_HEADLINE", $params_values_get["id"]);
        }
        $this->list_headline();
    }

    function edit_headline($params_values_get, $params_values_post, $params_files) {

        if($params_values_post) {
            $dades_tematica = $params_values_post["ID_TEMATICA"];
            unset($params_values_post["ID_TEMATICA"]);
            unset($params_values_post["accio"]);
            $params_values_post["DATA"] = $this->normaldate_to_mysql($params_values_post["DATA"]);
            $params_values_post["DIV"] = $this->normaldate_to_mysql($params_values_post["DIV"]);
            $params_values_post["DFV"] = $this->normaldate_to_mysql($params_values_post["DFV"]);
            $params_values_post["ACTIVA"] = $params_values_post["ACTIVA"]=="on" ? "1" : "0";

            if($params_files['FOTO']['name'] != ""){ // El campo foto contiene una imagen...
                // Primer, hi ha que validar que es tracta d'un JPG/GIF/PNG
                $allowedExts = array("jpg", "jpeg", "gif", "png", "JPG", "GIF", "PNG");
                $extension = end(explode(".", $params_files["FOTO"]["name"]));
                if ((($params_files["FOTO"]["type"] == "image/gif")
                        || ($params_files["FOTO"]["type"] == "image/jpeg")
                        || ($params_files["FOTO"]["type"] == "image/png")
                        || ($params_files["FOTO"]["type"] == "image/pjpeg"))
                    && in_array($extension, $allowedExts)) {
                    // el archivo es un JPG/GIF/PNG, entonces...

                    $foto = substr(md5(uniqid(rand())),0,10).".".$extension;
                    $directorio = FOLDER_IMAGES . "/headlines"; // directori a on es guardaran les imatges
                    // almacenar imagen en el servidor
                    move_uploaded_file($params_files['FOTO']['tmp_name'], $directorio.'/'.$foto);
                    $minFoto = 'min_'.$foto;
                    $resFoto = 'res_'.$foto;
                    $this->resizeImagen($directorio.'/', $foto, 220, 220,$minFoto,$extension);
                    $this->resizeImagen($directorio.'/', $foto, 600, 800,$resFoto,$extension);
                    unlink($directorio.'/'.$foto);
                    $params_values_post["FOTO"] = $foto;
                    //Esborro la imatge antiga si hi havia
                    if($params_values_post["FOTO_AUX"]!="") {
                        unlink($directorio.'/res_'.$params_values_post["FOTO_AUX"]);
                        unlink($directorio.'/min_'.$params_values_post["FOTO_AUX"]);
                    }
                }

            }
            else {
                $params_values_post["FOTO"] = $params_values_post["FOTO_AUX"];
            }

            if($params_values_post['delete_foto']=="on") {
                $directorio = FOLDER_IMAGES . "/headlines"; // directori a on es guardaran les imatges
                $params_values_post["FOTO"] = "";
                unlink($directorio.'/res_'.$params_values_post["FOTO_AUX"]);
                unlink($directorio.'/min_'.$params_values_post["FOTO_AUX"]);
            }

            if($params_files['ADJUNT']['name'] != ""){ // El camp adjunt conté un fitxer...
                $extension = end(explode(".", $params_files["ADJUNT"]["name"]));
                $adjunt = substr(md5(uniqid(rand())),0,10).".".$extension;
                $directorio = FOLDER_PUBLIC_FILES . "/headlines"; // directori a om es guardaran els fitxers adjunts
                move_uploaded_file($params_files['ADJUNT']['tmp_name'], $directorio.'/'.$adjunt);
                $params_values_post["ADJUNT"] = $adjunt;
                //Esborro el fitxer si hi havia
                if($params_values_post["ADJUNT_AUX"]!="") {
                    unlink($directorio.'/'.$params_values_post["ADJUNT_AUX"]);
                }
            }
            else {
                $params_values_post["ADJUNT"] = $params_values_post["ADJUNT_AUX"];
            }
            if($params_values_post['delete_adjunt']=="on") {
                $directorio = FOLDER_PUBLIC_FILES . "/headlines"; // directori a om es guardaran els fitxers adjunts
                $params_values_post["ADJUNT"] = "";
                unlink($directorio.'/'.$params_values_post["ADJUNT_AUX"]);
            }
            unset($params_values_post["FOTO_AUX"]);
            unset($params_values_post["ADJUNT_AUX"]);
            unset($params_values_post["delete_adjunt"]);
            unset($params_values_post["delete_foto"]);

            $params_values_post["LOGIN"] = $_SESSION['info_usuari']['login'];
            $dades_headlines = new \webtortosa\headline();
            if($params_values_post["ID"]=="") {
                $headline = $dades_headlines->addRow("HEAD_HEADLINE", $this->apostrofFilter($params_values_post));
                //$headline = $dades_headlines->addRow("HEAD_HEADLINE", $params_values_post);
                $id_headline = $headline["mysql_insert_id"];
            }
            else {
                $id_headline = $params_values_post["ID"];
                unset($params_values_post["ID"]);
                $params_values_post["ID"] = $id_headline;
                $headline = $dades_headlines->updateRow("HEAD_HEADLINE", $this->apostrofFilter($params_values_post));
                //$headline = $dades_headlines->updateRow("HEAD_HEADLINE", $params_values_post);
            }

            if(count($dades_tematica)) {
                $objmodel_tematica = new \webtortosa\tematica();
                $objmodel_tematica->deleteRow("HEAD_TEMATICA", "ID_HEADLINE", $params_values_post["ID"]);
                foreach ($dades_tematica as $key => $value){
                    $params_tematica = array (
                        'ID_HEADLINE'=>$id_headline,
                        'ID_TEMATICA'=>$value,
                    );
                    $objmodel_tematica->addRow("HEAD_TEMATICA", $params_tematica);
                }
            }
            $this->list_headline();
        }
        else {
            $smarty = new \webtortosa\Smarty_admin();
            $smarty->display("head.tpl");
            $smarty->assign('info_usuari', $_SESSION['info_usuari']);
            $smarty->display("header.tpl");
            $smarty->assign('info_usuari', $_SESSION['info_usuari']);
            $smarty->display("menu.tpl");
            //Carreguem les tipologies
            $dades_tipologia = new \webtortosa\tipologia();
            if($_SESSION['info_usuari'][unitat]=="GP")
                $params = array(
                    'ID' => 2
                );
            else
                $params = null;
            $items_tipologia = $dades_tipologia->getTipologiaData($params);
            $smarty->assign('items_tipologia', $items_tipologia);
            //Carreguem les tematiques
            $dades_tematica = new \webtortosa\tematica();
            $items_tematica = $dades_tematica->getTematicaData(null);
            $items_tematica = $this->utf8_converter($items_tematica);
            $smarty->assign('items_tematica', $items_tematica);

            if($params_values_get["id"]) {
                $dades_headlines = new \webtortosa\headline();
                $params = array(
                    "ID"=>$params_values_get["id"]
                );
                $items = $dades_headlines->getHeadlinesData($params);
                if (count($items)>0) {
                    $items[0]->FOTO = str_replace("'", "·", $items[0]->FOTO);
                    $smarty->assign('item', $items[0]);
                }

                $params = array(
                    "ID_HEADLINE" => $params_values_get["id"]
                );
                $items_tematica_headlines = $dades_headlines->getTematiquesHeadlinesData($params);
                if (count($items_tematica_headlines)>0)
                    $smarty->assign('items_tematica_headlines', $items_tematica_headlines);
            }
            $smarty->display("edit_headline.tpl");
            $smarty->display("footer.tpl");
        }
    }

    function list_headline($params_values_get=1)
    {
        $smarty = new \webtortosa\Smarty_admin();
        $smarty->display("head.tpl");
        $smarty->assign('info_usuari', $_SESSION['info_usuari']);
        $smarty->display("header.tpl");
        $smarty->assign('info_usuari', $_SESSION['info_usuari']);
        $smarty->display("menu.tpl");

        $dades_headlines = new \webtortosa\headline();
        if ($_SESSION['info_usuari']['rol'] != "Superadministrador") {
            $params = array(
                "LOGIN" => $_SESSION['info_usuari']['login']
            );
        }
        else {
            $params = null;
        }
        $items_headlines = $dades_headlines->getHeadlinesData($params);
        /* Pagination */
        $total = count($items_headlines);
        $page = $params_values_get['page'];
        $start = $page ? ($page - 1) * ITEMS_LIMIT : 0;

        if ($page == 0)
            $page = 1;
        $prev = $page - 1; //previous page is current page - 1
        $next = $page + 1; //next page is current page + 1
        $lastpage = ceil($total / ITEMS_LIMIT); //lastpage.

        $items_headlines = $dades_headlines->getHeadlinesData($params, $start, ITEMS_LIMIT);
        $smarty->assign('items',$items_headlines);
        $smarty->assign("lastpage",$lastpage);
        $smarty->assign("page",$page);
        $smarty->display("list_headline.tpl");
        $smarty->display("footer.tpl");
    }


    /********************************************
     ********************************************
     ********************************************
                Eleccions Functions
     ********************************************
     ********************************************
     ********************************************/

    function carregaMesa($params_values_get) {
        $RefEleccio = $params_values_get['IdEleccio'];
        $RefColegi = $params_values_get['IdColegi'];
        $idMesa = $params_values_get['IdMesa'];

        $dades_meses = new \webtortosa\eleccions();
        $items_meses = $dades_meses->getMesaData($RefEleccio,$RefColegi,$idMesa);

        $strDadesMesa = "";

        if (count($items_meses)==1) { 
            foreach($items_meses as $item_mesa) {
                $strDadesMesa = $item_mesa->IdMesa. "#" 
                            .$item_mesa->RefEleccio. "#"
                            .$item_mesa->RefColegi. "#"
                            .$item_mesa->Districte. "#"
                            .$item_mesa->Seccio. "#"
                            .$item_mesa->Lletra. "#"
                            .$item_mesa->RangAlfabetic. "#"
                            .$item_mesa->CensVotants. "#"
                            .$item_mesa->CensVotantsInicial. "#"
                            .$item_mesa->CensVotantsFinal. "#"
                            .$item_mesa->RepresentantAdmMesa. "#"
                            .$item_mesa->TelRepresentantAdmMesa. "#"
                            .$item_mesa->RepresentantTransmissioDadesMesa. "#"
                            .$item_mesa->TelRepresentantTransmissioDadesMesa. "#"
                            .$item_mesa->President. "#"
                            .$item_mesa->TelefonPresident. "#"
                            .$item_mesa->Observacions. "#"
                            .$item_mesa->Vots1Participacio. "#"
                            .$item_mesa->Vots2Participacio. "#"
                            .$item_mesa->Vots3Participacio. "#"
                            .$item_mesa->VotsFinParticipacio;
            }
        }

        echo  $strDadesMesa;
    }

   function carregaCandidaturesResultatsMesa($params_values_get) {
        $RefEleccio = $params_values_get['IdEleccio'];
        $RefColegi = $params_values_get['IdColegi'];
        $idMesa = $params_values_get['IdMesa'];

        $dades_meses = new \webtortosa\eleccions();
        $items_meses = $dades_meses->getMesaData($RefEleccio,$RefColegi,$idMesa);

        $strDadesMesa = "";

        if (count($items_meses)==1) { 
            foreach($items_meses as $item_mesa) {
                $strDadesMesa = $item_mesa->IdMesa. "#" 
                            .$item_mesa->RefEleccio. "#"
                            .$item_mesa->RefColegi. "#"
                            .$item_mesa->Districte. "#"
                            .$item_mesa->Seccio. "#"
                            .$item_mesa->Lletra. "#"
                            .$item_mesa->RangAlfabetic. "#"
                            .$item_mesa->CensVotants. "#"
                            .$item_mesa->CensVotantsInicial. "#"
                            .$item_mesa->CensVotantsFinal. "#"
                            .$item_mesa->RepresentantAdmMesa. "#"
                            .$item_mesa->TelRepresentantAdmMesa. "#"
                            .$item_mesa->RepresentantTransmissioDadesMesa. "#"
                            .$item_mesa->TelRepresentantTransmissioDadesMesa. "#"
                            .$item_mesa->President. "#"
                            .$item_mesa->TelefonPresident. "#"
                            .$item_mesa->Observacions. "#"
                            .$item_mesa->Vots1Participacio. "#"
                            .$item_mesa->Vots2Participacio. "#"
                            .$item_mesa->Vots3Participacio. "#"
                            .$item_mesa->VotsFinParticipacio;
            }
        }

        $items_resultats_candidatures = $dades_meses->getMesaResultatsCandidatures($RefEleccio,$RefColegi,$idMesa);

        $strDadesMesa .= "#" .count($items_resultats_candidatures). "#";

        foreach($items_resultats_candidatures as $item_rc) {
            $strDadesMesa .= $item_rc->IdCandidatura. "#" 
                            .$item_rc->Sigles. "#" 
                            .utf8_encode($item_rc->Denominacio). "#" 
                            .$item_rc->Vots. "#";
        }

        echo  $strDadesMesa;
   }

   function carregaMeses($params_values_get) {
        $idE = $params_values_get['IdEleccio'];
        $idC = $params_values_get['IdColegi'];        

        $dades_meses = new \webtortosa\eleccions();
        $items_meses = $dades_meses->getMesesColegisEleccionsData($idE,$idC);
        $strCombo = "<option value=\"\">-- Selecciona una mesa --</option>";

        foreach($items_meses as $item_mesa) {
            $strCombo .= "<option value=\"" .$item_mesa->IdMesa. "\">" .utf8_encode($item_mesa->IdMesa). "</option>";
        }
        echo  $strCombo;
   }


   function carregaColegis($params_values_get) {
        $id = $params_values_get['IdEleccio'];

        $dades_colegis = new \webtortosa\eleccions();
        $items_colegis = $dades_colegis->getColegisEleccionsData($id);
        
        //$strCombo = "<select name=\"RefColegi\" id=\"RefColegi\">";
        $strCombo = "<option value=\"\">-- Selecciona un col·legi --</option>";

        foreach($items_colegis as $item_colegi) {
            $strCombo .= "<option value=\"" .$item_colegi->IdColegi. "\">" .utf8_encode($item_colegi->NomColegi). 
                         "</option>";
        }

        //$strCombo .= "</select>";

        echo  $strCombo;
   }

    function edit_participacio($params_values_get, $params_values_post) {
        $smarty = new \webtortosa\Smarty_admin();
        $smarty->display("head.tpl");
        $smarty->assign('info_usuari', $_SESSION['info_usuari']);
        $smarty->display("header.tpl");
        $smarty->assign('info_usuari', $_SESSION['info_usuari']);
        $smarty->display("menu.tpl");

        //Carreguem les eleccions
        $dades_eleccions = new \webtortosa\eleccions();
        $params = array(
            'Finalitzada' => 0
        );
        $items_eleccions = $dades_eleccions->getEleccionsData($params);
        foreach ($items_eleccions as $key => &$item_elec) {
            $item_elec->Denominacio = utf8_encode($item_elec->Denominacio);
        }
        $smarty->assign('items_eleccions', $items_eleccions);

        if($params_values_post) {
            $dades_mesa = new \webtortosa\eleccions();
            $result_update = $dades_mesa->updateRowParticipacio($params_values_post);
            $smarty->assign('result_update', $result_update);
        }

        $smarty->display("edit_participacio.tpl");
        $smarty->display("footer.tpl");
    }

    function edit_resultat($params_values_get, $params_values_post) {
        $smarty = new \webtortosa\Smarty_admin();
        $smarty->display("head.tpl");
        $smarty->assign('info_usuari', $_SESSION['info_usuari']);
        $smarty->display("header.tpl");
        $smarty->assign('info_usuari', $_SESSION['info_usuari']);
        $smarty->display("menu.tpl");

        //Carreguem les eleccions
        $dades_eleccions = new \webtortosa\eleccions();
        $params = array(
            'Finalitzada' => 0
        );
        $items_eleccions = $dades_eleccions->getEleccionsData($params);
        foreach ($items_eleccions as $key => &$item_elec) {
            $item_elec->Denominacio = utf8_encode($item_elec->Denominacio);
        }
        $smarty->assign('items_eleccions', $items_eleccions);

        if($params_values_post) {
            $RefEleccio = "";
            $RefColegi = "";
            $RefMesa = "";
            $Vots1Participacio = 0;
            $Vots2Participacio = 0;
            $Vots3Participacio = 0;
            $Districte = "";
            $Seccio = "";
            $Lletra = "";

            foreach ($_POST as $clave=>$valor) {
                if (strcmp("RefEleccio",$clave) == 0) { 
                    $RefEleccio = $valor;
                } else if (strcmp("RefColegi",$clave) == 0) { 
                    $RefColegi = $valor;
                } else if (strcmp("IdMesa",$clave) == 0) { 
                    $RefMesa = $valor;
                } else if (strcmp("Vots1Participacio",$clave) == 0) { 
                    $Vots1Participacio = $valor;
                } else if (strcmp("Vots2Participacio",$clave) == 0) { 
                    $Vots2Participacio = $valor;
                } else if (strcmp("Vots3Participacio",$clave) == 0) { 
                    $Vots3Participacio = $valor;
                } else if (strcmp("Districte",$clave) == 0) { 
                    $Districte = $valor;
                } else if (strcmp("Seccio",$clave) == 0) { 
                    $Seccio = $valor;
                } else if (strcmp("Lletra",$clave) == 0) { 
                    $Lletra = $valor;
                } else if (strcmp("Acceptar",$clave) !== 0) { 
                    $IdCandidatura = $clave;
                    $Vots = $valor;

                    $dades_eleccions->updateResultatsCandidaturaMesa($RefEleccio,$RefColegi,$RefMesa,$IdCandidatura,$Vots,$Districte,$Seccio);
                }
            }

            $result_update = $dades_eleccions->updateRowParticipacioCurt($RefEleccio,$RefColegi,$RefMesa,$Vots1Participacio,$Vots2Participacio,$Vots3Participacio);        
            $smarty->assign('result_update', $result_update);

        }

        $smarty->display("edit_resultat.tpl");
        $smarty->display("footer.tpl");
    }

    function edit_participacio_historic($params_values_get, $params_values_post) {
        $smarty = new \webtortosa\Smarty_admin();
        $smarty->display("head.tpl");
        $smarty->assign('info_usuari', $_SESSION['info_usuari']);
        $smarty->display("header.tpl");
        $smarty->assign('info_usuari', $_SESSION['info_usuari']);
        $smarty->display("menu.tpl");

        //Carreguem les eleccions
        $dades_eleccions = new \webtortosa\eleccions();
        $params = array(
            'Finalitzada' => 1
        );
        $items_eleccions = $dades_eleccions->getEleccionsData($params);
        foreach ($items_eleccions as $key => &$item_elec) {
            $item_elec->Denominacio = utf8_encode($item_elec->Denominacio);
        }
        $smarty->assign('items_eleccions', $items_eleccions);        

        if($params_values_post) {
            $dades_mesa = new \webtortosa\eleccions();
            $result_update = $dades_mesa->updateRowParticipacio($params_values_post);
            $smarty->assign('result_update', $result_update);
        }

        $smarty->display("edit_participacio_historic.tpl");
        $smarty->display("footer.tpl");
    }

    function edit_resultat_historic($params_values_get, $params_values_post) {
        $smarty = new \webtortosa\Smarty_admin();
        $smarty->display("head.tpl");
        $smarty->assign('info_usuari', $_SESSION['info_usuari']);
        $smarty->display("header.tpl");
        $smarty->assign('info_usuari', $_SESSION['info_usuari']);
        $smarty->display("menu.tpl");

        //Carreguem les eleccions
        $dades_eleccions = new \webtortosa\eleccions();
        $params = array(
            'Finalitzada' => 1
        );
        $items_eleccions = $dades_eleccions->getEleccionsData($params);
        foreach ($items_eleccions as $key => &$item_elec) {
            $item_elec->Denominacio = utf8_encode($item_elec->Denominacio);
        }
        $smarty->assign('items_eleccions', $items_eleccions);

        if($params_values_post) {
            $RefEleccio = "";
            $RefColegi = "";
            $RefMesa = "";
            $Vots1Participacio = 0;
            $Vots2Participacio = 0;
            $Vots3Participacio = 0;
            $Districte = "";
            $Seccio = "";
            $Lletra = "";

            foreach ($_POST as $clave=>$valor) {
                if (strcmp("RefEleccio",$clave) == 0) { 
                    $RefEleccio = $valor;
                } else if (strcmp("RefColegi",$clave) == 0) { 
                    $RefColegi = $valor;
                } else if (strcmp("IdMesa",$clave) == 0) { 
                    $RefMesa = $valor;
                } else if (strcmp("Vots1Participacio",$clave) == 0) { 
                    $Vots1Participacio = $valor;
                } else if (strcmp("Vots2Participacio",$clave) == 0) { 
                    $Vots2Participacio = $valor;
                } else if (strcmp("Vots3Participacio",$clave) == 0) { 
                    $Vots3Participacio = $valor;
                } else if (strcmp("Districte",$clave) == 0) { 
                    $Districte = $valor;
                } else if (strcmp("Seccio",$clave) == 0) { 
                    $Seccio = $valor;
                } else if (strcmp("Lletra",$clave) == 0) { 
                    $Lletra = $valor;
                } else if (strcmp("Acceptar",$clave) !== 0) { 
                    $IdCandidatura = $clave;
                    $Vots = $valor;

                    $dades_eleccions->updateResultatsCandidaturaMesa($RefEleccio,$RefColegi,$RefMesa,$IdCandidatura,$Vots,$Districte,$Seccio);
                }
            }

            $result_update = $dades_eleccions->updateRowParticipacioCurt($RefEleccio,$RefColegi,$RefMesa,$Vots1Participacio,$Vots2Participacio,$Vots3Participacio);        
            $smarty->assign('result_update', $result_update);

        }

        $smarty->display("edit_resultat_historic.tpl");
        $smarty->display("footer.tpl");
    }

    /* Pressupostos participatius */

    function estadistica_pressupostos_participatius($params_get, $params_post) {
        $smarty = new \webtortosa\Smarty_admin();
        if(isset($params_post['lloc_votacio'])) {
            require_once(FOLDER_MODEL . "/pressupostos-participatius.class.php");
            $fc = new \webtortosa\functions_controller();
            $ppdb = new \webtortosa\pressupostos_participatius();
            switch($params_post['lloc_votacio']) {
                case "1":
                    $lloc_query = 'ajtortosa';
                    $lloc = 'Ajuntament de Tortosa';
                    break;
                case "2":
                    $lloc_query = 'fira';
                    $lloc = 'Fira Expo Ebre';
                    break;
                case "3":
                    $lloc_query = 'reguers';
                    $lloc = 'Oficina dels Reguers';
                    break;
                case "4":
                    $lloc_query = 'vinallop';
                    $lloc = 'Oficina de Vinallop';
                    break;
                case "5":
                    $lloc_query = 'bitem';
                    $lloc = 'Ajuntament de Bítem';
                    break;
                case "6":
                    $lloc_query = 'campredo';
                    $lloc = 'Ajuntament de Campredó';
                    break;
                case "7":
                    $lloc_query = 'jesus';
                    $lloc = 'Ajuntament de Jesús';
                    break;
                case "8":
                    $lloc_query = 'tots';
                    $lloc = 'tots';
                    break;
            }
            if($params_post['lloc_votacio']<8) {
                $fc->escribir_log_file(FOLDER_LOGS . "/" . date('d-m-Y') . ".txt", "Consulta estadistica: ". $lloc);
                $lst_data = $ppdb->getEstPP($fc->test_input($lloc_query));
                $smarty->assign('action', 'list');
                $smarty->assign('lloc', $lloc);
                $smarty->assign('numero_vots', $lst_data[0]->total_vots);
            }
            else {
                $fc->escribir_log_file(FOLDER_LOGS . "/" . date('d-m-Y') . ".txt", "Consulta estadistica totes les seus ");
                $lst_data = $ppdb->getEstPPTotals();
                $cens = $ppdb->getCensPP();
                $smarty->assign('action', 'list_totals');
                $smarty->assign('total_vots', $cens[0]->total_vots);
                foreach($lst_data as &$item){
                    switch($item->LLOC_VOTACIO) {
                        case "ajtortosa":
                            $item->LLOC_VOTACIO = "Ajuntament de Tortosa";
                            break;
                        case "bitem":
                            $item->LLOC_VOTACIO = "EMD Bítem";
                            break;
                        case "campredo":
                            $item->LLOC_VOTACIO = "EMD Campredó";
                            break;
                        case "fira":
                            $item->LLOC_VOTACIO = "Fira ExpoEbre";
                            break;
                        case "jesus":
                            $item->LLOC_VOTACIO = "EMD Jesús";
                            break;
                        case "reguers":
                            $item->LLOC_VOTACIO = "Els Reguers";
                            break;
                        case "vinallop":
                            $item->LLOC_VOTACIO = "Vinallop";
                            break;
                    }
                }
                $smarty->assign('vots', $lst_data);
            }

        }



        $smarty->display("head.tpl");
        $smarty->assign('info_usuari', $_SESSION['info_usuari']);
        $smarty->display("header.tpl");
        $smarty->assign('info_usuari', $_SESSION['info_usuari']);
        $smarty->display("menu.tpl");

        $smarty->display("pressupostos-participatius/estadistica.tpl");
        $smarty->display("footer.tpl");
    }

    function pressupostos_participatius($params_get, $params_post){
        $fc = new \webtortosa\functions_controller();
        // Log per a control de 0 al davant
        //$fc->escribir_log_file(FOLDER_LOGS."/".date('d-m-Y').".txt",'DNI a consultar:'.$params_post['dni_number']);

        //$nom = isset($params_post['nom']) ? str_replace("Ñ","/",strtoupper($params_post['nom'])) : null;
        //$cognoms1 = $params_post['cognoms1'] ? str_replace("Ñ","/",strtoupper($params_post['cognoms1'])) : null;
        $nom = $params_post['nom'] ? utf8_decode($params_post['nom']) : null;
        $cognoms1 = $params_post['cognoms1'] ? utf8_decode($params_post['cognoms1']) : null;
        $cognoms2 = $params_post['cognoms2'] ? utf8_decode($params_post['cognoms2']) : null;
        $dni_number = $params_post['dni_number'] ? utf8_decode($params_post['dni_number']) : null;
        $dni_letter = $params_post['dni_letter'] ? utf8_decode($params_post['dni_letter']) : null;
        $option2_passport_number = $params_post['option2_passport_number'] ? utf8_decode($params_post['option2_passport_number']) : null;
        $tr_letter_1 = $params_post['tr_letter_1'] ? utf8_decode($params_post['tr_letter_1']) : null;
        $tr_number = $params_post['tr_number'] ? utf8_decode($params_post['tr_number']) : null;
        $tr_letter_2 = $params_post['tr_letter_2'] ? utf8_decode($params_post['tr_letter_2']) : null;
        $id = $params_post['id'] ? $params_post['id'] : null;
        $tipus_document = $params_post['tdocument'] ? $params_post['tdocument'] : null;

        $content1 = $params_post['action'] . " - " . $nom . " - ". $cognoms1 . " - " . $cognoms2;
        $content2 = $params_post['action'] . " - " . $dni_number . " - ". $dni_letter;
        $content3 = $params_post['action'] . " - " . $option2_passport_number;
        $content4 = $params_post['action'] . " - " . $tr_letter_1 . " - " . $tr_number . " - " . $tr_letter_2;
        //echo $content;


        if($params_post['action']=="consult"){ //Acció de consultar persones a la bbdd segons els paràmetres de cerca.
            require_once (FOLDER_MODEL."/pressupostos-participatius.class.php");
            $fc->escribir_log_file(FOLDER_LOGS."/".date('d-m-Y').".txt","Procés de consulta: ".$content1.' '.$content2.' '.$content3.' '.$content4);

            $ppdb = new \webtortosa\pressupostos_participatius();
            //Filtre dades per a query
            $nom = $fc->test_input($nom);
            $cognoms1 = $fc->test_input($cognoms1);
            $cognoms2 = $fc->test_input($cognoms2);
            $dni_number = $fc->test_input($dni_number);
            $dni_letter = $fc->test_input($dni_letter);
            $option2_passport_number = $fc->test_input($option2_passport_number);
            $tr_letter_1 = $fc->test_input($tr_letter_1);
            $tr_number = $fc->test_input($tr_number);
            $tr_letter_2 = $fc->test_input($tr_letter_2);

            $lst_people = $ppdb->getDataPP($nom, $cognoms1, $cognoms2, $dni_number, $dni_letter, $option2_passport_number, $tr_letter_1, $tr_number, $tr_letter_2);
            if(count($lst_people)>0) {
                $hi_ha_per_votar = false;
                $content = '<ul class="list-people">';
                foreach($lst_people as $person) {
                    // Comprovació DNI
                    if($person->DNI!=="")
                        $dni = $person->DNI;
                    else
                        $dni = "No DNI";

                    // Comprovació DNI
                    if($person->pinudo!=="")
                        $passaport = $person->pinudo;
                    else
                        $passaport = "No passaport";

                    $date = date_create($person->HORA_VOTACIO);
                    $data_naixement = date_create($person->ppfena);
                    //Comprovació votat
                    if(($person->LLOC_VOTACIO!="")&&($person->HORA_VOTACIO)&&($person->HA_VOTAT==1)) {
                        switch($person->LLOC_VOTACIO) {
                            case "ajtortosa":
                                $lloc = 'Ajuntament de Tortosa';
                                break;
                            case "fira":
                                $lloc = 'Fira Expo Ebre';
                                break;
                            case "jesus":
                                $lloc = 'Ajuntament de Jesús';
                                break;
                            case "campredo":
                                $lloc = 'Ajuntament de Campredó';
                                break;
                            case "bitem":
                                $lloc = 'Ajuntament de Bítem';
                                break;
                            case "reguers":
                                $lloc = 'Oficina dels Reguers';
                                break;
                            case "vinallop":
                                $lloc = 'Oficina de Vinallop';
                                break;
                            default:
                                $lloc = $person->LLOC_VOTACIO;
                                break;

                        }
                        $votacio = 'Ha votat a '.$lloc . ' el dia '. date_format($date, 'd-m-Y H:i:s');
                        $class_color_pp = "red_pp";
                    }
                    else {
                        $votacio = '<span class="selector-pp"><input type="checkbox" name="cb_pp[]" value="'.$person->pinume.'"></span>';
                        $class_color_pp = "green_pp";
                        $hi_ha_per_votar = true;
                    }
                    $fc->escribir_log_file(FOLDER_LOGS."/".date('d-m-Y').".txt",$person->piname.' '.$person->piape1.' '.$person->piape2.' | ' . $dni . ' | ' . $passaport . ' | '.date_format($data_naixement, 'd-m-Y').' | '.$person->catipo.' '.$person->cadesc.' '.$person->phnume.', '.$person->phesca.' '.$person->phpiso.' - '.$person->phpuer.' '.strip_tags ($votacio));
                    $content .= '<li class="'.$class_color_pp.'">'.utf8_encode($person->piname).' '.utf8_encode($person->piape1).' '.utf8_encode($person->piape2).' | ' . $dni . ' | ' .$passaport . ' | ' .date_format($data_naixement, 'd-m-Y').' | '.$person->catipo.' '.$person->cadesc.' '.$person->phnume.', '.$person->phesca.' '.$person->phpiso.' - '.$person->phpuer.' '.$votacio.'</li>';
                }
                $content .= '</ul>';
                if($hi_ha_per_votar) {
                    $content .= '<label id="tdocument-error-votar" class="error" for="tdocument-error-votar" style="display: none;"></label>';
                    $content .= '<input class="seleccionar" type="submit" name="seleccionar" value="Seleccionar">';
                }
                echo $content;
            }
            else {
                echo "Dades no trobades";
            }
            exit;
        }
        if($params_post['action']=="select"){ //Acció quan seleccionen una persona i ha de mostrar totes les seves dades abans de votar.
            require_once (FOLDER_MODEL."/pressupostos-participatius.class.php");
            $fc->escribir_log_file(FOLDER_LOGS."/".date('d-m-Y').".txt","Procés de seleccó de fitxa, id: ".$id);

            $ppdb = new \webtortosa\pressupostos_participatius();

            $lst_people = $ppdb->getDataPP($nom, $cognoms1, $cognoms2, $dni_number, $dni_letter, $option2_passport_number, $tr_letter_1, $tr_number, $tr_letter_2, $id);
            if(count($lst_people)>0) {
                $hi_ha_per_votar = false;
                $content = '<div class="title-file-people">Dades persona</div>';
                $content .= '<ul class="file-people">';
                foreach($lst_people as $person) {
                    $data_naixement = date_create($person->ppfena);

                    // Comprovació DNI
                    if($person->DNI!=="")
                        $dni = $person->DNI;
                    else
                        $dni = "No DNI";

                    // Comprovació DNI
                    if($person->pinudo!=="")
                        $passaport = $person->pinudo;
                    else
                        $passaport = "No passaport";

                    $content .= '<li>Nom: <span>'.utf8_encode($person->piname).'</span></li>';
                    $content .= '<li>Cognoms: <span>'.utf8_encode($person->piape1).' '.utf8_encode($person->piape2).'</span></li>';
                    $content .= '<li>DNI: <span>'.$dni.'</span></li>';
                    $content .= '<li>Passaport: <span>'.$passaport.'</span></li>';
                    $content .= '<li>Data naixement: <span>'.date_format($data_naixement, 'd-m-Y').'</span></li>';
                    $content .= '<li>Adreça: <span>'.$person->catipo.' '.$person->cadesc.' '.$person->phnume.', '.$person->phesca.' '.$person->phpiso.' - '.$person->phpuer.'</span></li>';
                }
                $content .= '</ul>';

                $content .= '<label id="tdocument-error-votar" class="error" for="tdocument-error-votar" style="display: none;"></label>';
                $content .= '<input id="id" type="hidden" name="id" value="'.$id.'">';
                $content .= '<input class="votar" type="submit" name="votar" value="Votar">';
                $content .= '<input class="novotar" type="button" name="novotar" value="Cancel·lar">';
                echo $content;
            }
            else {
                echo "Dades no trobades";
            }
            exit;
        }
        if($params_post['action']=="registry") {
            require_once (FOLDER_MODEL."/pressupostos-participatius.class.php");

            $ppdb = new \webtortosa\pressupostos_participatius();
            if($_SESSION['info_usuari']['login']=="admin")
                $seu = "fira";
            else
                $seu = $_SESSION['info_usuari']['login'];

            if($ppdb->UpdateVotacio($params_post['id'], $seu)) {

                $fc->escribir_log_file(FOLDER_LOGS . "/vots.log", $params_post['id'] . ":::" . $seu);
                echo "......... Votació realitzada .........";
            }
            else {
                echo "......... Error en la Votació .........";
            }
            exit;
        }
        $smarty = new \webtortosa\Smarty_admin();
        $smarty->display("head.tpl");
        $smarty->assign('info_usuari', $_SESSION['info_usuari']);
        $smarty->display("header.tpl");
        $smarty->assign('info_usuari', $_SESSION['info_usuari']);
        $smarty->display("menu.tpl");
        $hora_ini = strtotime( "08:50:00" );
        $hora_fi = strtotime( "21:30:00" );
        if(((strtotime(date('H:i:s'))>$hora_ini)&&(strtotime(date('H:i:s'))<=$hora_fi))||($_SESSION['info_usuari']['login']=="admin")) {
            $smarty->display("pressupostos-participatius/pressupostos_participatius.tpl");
        }
        else {
            echo '<div style="text-align:center; font-size: 17px;">Votació fora d\'horari</div>';
        }


        $smarty->display("footer.tpl");
    }


    /********************************************
     ********************************************
     ********************************************
        Actuacions Pressupostos Participatius
     ********************************************
     ********************************************
     ********************************************/

    /*
    function edit_proposta($params_values_get, $params_values_post, $params_files) {
        if($params_values_post) {
            // $dades_tematica = $params_values_post["ID_TEMATICA"];
            // unset($params_values_post["ID_TEMATICA"]);
            unset($params_values_post["accio"]);

            if($params_files['IMATGE1']["name"] != ""){ // El campo foto contiene una imagen...
                // Primer, hi ha que validar que es tracta d'un JPG/GIF/PNG
                $allowedExts = array("jpg", "jpeg", "gif", "png", "JPG", "GIF", "PNG");
                $extension = end(explode(".", $params_files["IMATGE1"]["name"]));
                if ((($params_files["IMATGE1"]["type"] == "image/gif")
                        || ($params_files["IMATGE1"]["type"] == "image/jpeg")
                        || ($params_files["IMATGE1"]["type"] == "image/png")
                        || ($params_files["IMATGE1"]["type"] == "image/pjpeg"))
                    && in_array($extension, $allowedExts)) {
                    // el archivo es un JPG/GIF/PNG, entonces...

                    $foto = substr(md5(uniqid(rand())),0,10).".".$extension;
                    $directorio = FOLDER_IMAGES . "/pciutadana/pressupostosp"; // directori a on es guardaran les imatges
                    // almacenar imagen en el servidor
                    move_uploaded_file($params_files['IMATGE1']['tmp_name'], $directorio.'/'.$foto);
                    $minFoto = 'min_'.$foto;
                    $resFoto = 'res_'.$foto;
                    $this->resizeImagen($directorio.'/', $foto, 220, 220,$minFoto,$extension);
                    $this->resizeImagen($directorio.'/', $foto, 500, 500,$resFoto,$extension);
                    unlink($directorio.'/'.$foto);
                    $params_values_post["IMATGE1"] = $foto;
                    //Esborro la imatge antiga si hi havia
                    if($params_values_post["FOTO_AUX"]!="") {
                        unlink($directorio.'/res_'.$params_values_post["FOTO_AUX"]);
                        unlink($directorio.'/min_'.$params_values_post["FOTO_AUX"]);
                    }
                }

            }
            else {
                $params_values_post["IMATGE1"] = $params_values_post["FOTO_AUX"];
            }

            if($params_values_post['delete_foto1']=="on") {
                $directorio = FOLDER_IMAGES . "/pciutadana/pressupostosp"; // directori a on es guardaran les imatges
                $params_values_post["IMATGE1"] = "";
                unlink($directorio.'/res_'.$params_values_post["FOTO_AUX"]);
                unlink($directorio.'/min_'.$params_values_post["FOTO_AUX"]);
            }

            if($params_files['IMATGE2']["name"] != ""){ // El campo foto contiene una imagen...
                // Primer, hi ha que validar que es tracta d'un JPG/GIF/PNG
                $allowedExts = array("jpg", "jpeg", "gif", "png", "JPG", "GIF", "PNG");
                $extension = end(explode(".", $params_files["IMATGE2"]["name"]));
                if ((($params_files["IMATGE2"]["type"] == "image/gif")
                        || ($params_files["IMATGE2"]["type"] == "image/jpeg")
                        || ($params_files["IMATGE2"]["type"] == "image/png")
                        || ($params_files["IMATGE2"]["type"] == "image/pjpeg"))
                    && in_array($extension, $allowedExts)) {
                    // el archivo es un JPG/GIF/PNG, entonces...

                    $foto = substr(md5(uniqid(rand())),0,10).".".$extension;
                    $directorio = FOLDER_IMAGES . "/pciutadana/pressupostosp"; // directori a on es guardaran les imatges
                    // almacenar imagen en el servidor
                    move_uploaded_file($params_files['IMATGE2']['tmp_name'], $directorio.'/'.$foto);
                    $minFoto = 'min_'.$foto;
                    $resFoto = 'res_'.$foto;
                    $this->resizeImagen($directorio.'/', $foto, 220, 220,$minFoto,$extension);
                    $this->resizeImagen($directorio.'/', $foto, 500, 500,$resFoto,$extension);
                    unlink($directorio.'/'.$foto);
                    $params_values_post["IMATGE2"] = $foto;
                    //Esborro la imatge antiga si hi havia
                    if($params_values_post["FOTO_AUX"]!="") {
                        unlink($directorio.'/res_'.$params_values_post["FOTO_AUX"]);
                        unlink($directorio.'/min_'.$params_values_post["FOTO_AUX"]);
                    }
                }

            }
            else {
                $params_values_post["IMATGE2"] = $params_values_post["FOTO_AUX"];
            }

            if($params_values_post['delete_foto2']=="on") {
                $directorio = FOLDER_IMAGES . "/pciutadana/pressupostosp"; // directori a on es guardaran les imatges
                $params_values_post["IMATGE2"] = "";
                unlink($directorio.'/res_'.$params_values_post["FOTO_AUX"]);
                unlink($directorio.'/min_'.$params_values_post["FOTO_AUX"]);
            }


            if($params_files['IMATGE3']["name"] != ""){ // El campo foto contiene una imagen...
                // Primer, hi ha que validar que es tracta d'un JPG/GIF/PNG
                $allowedExts = array("jpg", "jpeg", "gif", "png", "JPG", "GIF", "PNG");
                $extension = end(explode(".", $params_files["IMATGE3"]["name"]));
                if ((($params_files["IMATGE3"]["type"] == "image/gif")
                        || ($params_files["IMATGE3"]["type"] == "image/jpeg")
                        || ($params_files["IMATGE3"]["type"] == "image/png")
                        || ($params_files["IMATGE3"]["type"] == "image/pjpeg"))
                    && in_array($extension, $allowedExts)) {
                    // el archivo es un JPG/GIF/PNG, entonces...

                    $foto = substr(md5(uniqid(rand())),0,10).".".$extension;
                    $directorio = FOLDER_IMAGES . "/pciutadana/pressupostosp"; // directori a on es guardaran les imatges
                    // almacenar imagen en el servidor
                    move_uploaded_file($params_files['IMATGE3']['tmp_name'], $directorio.'/'.$foto);
                    $minFoto = 'min_'.$foto;
                    $resFoto = 'res_'.$foto;
                    $this->resizeImagen($directorio.'/', $foto, 220, 220,$minFoto,$extension);
                    $this->resizeImagen($directorio.'/', $foto, 500, 500,$resFoto,$extension);
                    unlink($directorio.'/'.$foto);
                    $params_values_post["IMATGE3"] = $foto;
                    //Esborro la imatge antiga si hi havia
                    if($params_values_post["FOTO_AUX"]!="") {
                        unlink($directorio.'/res_'.$params_values_post["FOTO_AUX"]);
                        unlink($directorio.'/min_'.$params_values_post["FOTO_AUX"]);
                    }
                }

            }
            else {
                $params_values_post["IMATGE3"] = $params_values_post["FOTO_AUX"];
            }

            if($params_values_post['delete_foto3']=="on") {
                $directorio = FOLDER_IMAGES . "/pciutadana/pressupostosp"; // directori a on es guardaran les imatges
                $params_values_post["IMATGE3"] = "";
                unlink($directorio.'/res_'.$params_values_post["FOTO_AUX"]);
                unlink($directorio.'/min_'.$params_values_post["FOTO_AUX"]);
            }

            
            unset($params_values_post["FOTO_AUX"]);
            unset($params_values_post["delete_foto1"]);
            unset($params_values_post["delete_foto2"]);
            unset($params_values_post["delete_foto3"]);

            $params_values_post["LOGIN"] = $_SESSION['info_usuari']['login'];
            $dades_propostes = new \webtortosa\execucio();
            if($params_values_post["ID_PROPOSTA"]=="") {
                $proposta = $dades_propostes->addRow("PPART", $this->apostrofFilter($params_values_post));
                //$headline = $dades_headlines->addRow("HEAD_HEADLINE", $params_values_post);
                $id_proposta = $proposta["mysql_insert_id"];
            }
            else {
                $id_proposta = $params_values_post["ID_PROPOSTA"];
                unset($params_values_post["ID_PROPOSTA"]);
                $params_values_post["ID_PROPOSTA"] = $id_proposta;
                $proposta = $dades_propostes->updateRow("PPART", $this->apostrofFilter($params_values_post));
                //$headline = $dades_headlines->updateRow("HEAD_HEADLINE", $params_values_post);
            }

            $this->list_proposta();
        }
        else {
            $smarty = new \webtortosa\Smarty_admin();
            $smarty->display("head.tpl");
            $smarty->assign('info_usuari', $_SESSION['info_usuari']);
            $smarty->display("header.tpl");
            $smarty->assign('info_usuari', $_SESSION['info_usuari']);
            $smarty->display("menu.tpl");
            
            if($params_values_get["id"]) {
                $dades_propostes = new \webtortosa\execucio();
                $params = array(
                    "ID_PROPOSTA"=>$params_values_get["id"]
                );
                $items = $dades_propostes->getPropostesData($params);
                if (count($items)>0) {
                    $items[0]->IMATGE1 = str_replace("'", "·", $items[0]->IMATGE1);
                    $items[0]->IMATGE2 = str_replace("'", "·", $items[0]->IMATGE2);
                    $items[0]->IMATGE3 = str_replace("'", "·", $items[0]->IMATGE3);
                    $smarty->assign('item', $items[0]);
                }
            }
            $smarty->display("pressupostos-participatius/propostes/edit_proposta.tpl");
            $smarty->display("footer.tpl");
        }
    }

    function list_proposta($params_values_get=1)
    {
        $smarty = new \webtortosa\Smarty_admin();
        $smarty->display("head.tpl");
        $smarty->assign('info_usuari', $_SESSION['info_usuari']);
        $smarty->display("header.tpl");
        $smarty->assign('info_usuari', $_SESSION['info_usuari']);
        $smarty->display("menu.tpl");

        $dades_propostes = new \webtortosa\execucio();
        if ($_SESSION['info_usuari']['rol'] != "Superadministrador") {
            $params = array(
                "LOGIN" => $_SESSION['info_usuari']['login']
            );
        }
        else {
            $params = null;
        }
        $items_propostes = $dades_propostes->getPropostesData($params);

        $total = count($items_propostes);
        $page = $params_values_get['page'];
        $start = $page ? ($page - 1) * ITEMS_LIMIT : 0;

        if ($page == 0)
            $page = 1;
        $prev = $page - 1; //previous page is current page - 1
        $next = $page + 1; //next page is current page + 1
        $lastpage = ceil($total / ITEMS_LIMIT); //lastpage.

        $items_propostes = $dades_propostes->getPropostesData($params, $start, ITEMS_LIMIT);
        $smarty->assign('items',$items_propostes);
        $smarty->assign("lastpage",$lastpage);
        $smarty->assign("page",$page);
        $smarty->display("pressupostos-participatius/propostes/list_propostes.tpl");
        $smarty->display("footer.tpl");
    }
    */


    /********************************************
     ********************************************
     ********************************************
                Commons Funcions
     ********************************************
     ********************************************
     ********************************************/

    /* Crea Slugs a partir de un string */
    function slug($string) {
        $characters = array(
            "Á" => "A", "Ç" => "c", "É" => "e", "Í" => "i", "Ñ" => "n", "Ó" => "o", "Ú" => "u",
            "á" => "a", "ç" => "c", "é" => "e", "í" => "i", "ñ" => "n", "ó" => "o", "ú" => "u",
            "à" => "a", "è" => "e", "ì" => "i", "ò" => "o", "ù" => "u"
        );

        $string = strtr($string, $characters);
        $string = strtolower(trim($string));
        $string = preg_replace("/[^a-z0-9-]/", "-", $string);
        $string = preg_replace("/-+/", "-", $string);

        if(substr($string, strlen($string) - 1, strlen($string)) === "-") {
            $string = substr($string, 0, strlen($string) - 1);
        }

        return $string;
    }

    function prepareTranslationsForSmarty($data_translations) {
        $a_translations = array();
        $control_id_pais_comercio = "";
        if(count($data_translations)) {
            foreach($data_translations as $data_translation) {
                if($control_id_pais_comercio!=$data_translation->ID_PAIS_COMERCIO) {
                    $a_fields = array();
                    $a_translations[$data_translation->ID_PAIS_COMERCIO] = $a_fields;
                    $control_id_pais_comercio = $data_translation->ID_PAIS_COMERCIO;
                }
                $a_fields[$data_translation->CAMPO] = $data_translation->VALOR;
            }
            $extract = array_shift($a_translations); //Quito el primer elemento que estaba vacio
            $a_translations[$data_translation->ID_PAIS_COMERCIO] = $a_fields;
        }
        return $a_translations;
    }

    //Controla el time out de la sessión del gestor
    function timeout_users ($tiempo, $redirect=true){
        if (!isset($_COOKIE['AJT'])) {
            if($redirect)
                header ("Location: /admin/index.php");
        }
        /*
        else{
            $_SESSION['tiempo'] = time();
            //$tiempo_caducidad = time()+3600+24; //1 dia
            $tiempo_caducidad = time()+60; //1 minuto

        }
        */
    }

    //Filtrador d'apòstrofs
    function apostrofFilter($input) {
        foreach ($input as $key => &$item_text) {
            $item_text = str_replace("\'", "'", $item_text);
            $item_text = str_replace("'", "\'", $item_text);
        }
        return $input;
    }

    //Filtrador dels valors d'una array a utf8
    function filtraArrayUtf8Encode($text_input) {
        foreach ($text_input as $key => &$item_text) {
            foreach($item_text as &$field)
                $field = utf8_encode($field);
        }
        return $text_input;
    }
}