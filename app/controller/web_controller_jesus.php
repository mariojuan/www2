<?php
/**
 * Created by Ajuntament de Tortosa.
 * User: Jesús Rodrí
 * Date: 19/01/14
 * Time: 23:18
 * To change this template use File | Settings | File Templates.
 */

namespace webtortosa;

session_start();
require_once (FOLDER_CONTROLLER."/functions_controller.php");
require_once (FOLDER_LIBS."/smarty/Smarty_web.class.php");
require_once (FOLDER_CHARTS."/fusioncharts.php");
require_once (FOLDER_MODEL."/db.class.php");
require_once (FOLDER_MODEL."/eleccions.class.php");
require_once (FOLDER_MODEL."/memoria-democratica.class.php");

class web_controller {

    /******************************************
     ********************************************
     ********************************************
        Funcions per a WEB
     ********************************************
     ********************************************
     ********************************************/





/****/
/****/
/****/





 function atencio_ciutadana ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

/*Cal definir 'type" perque s'usa a xxx_home.tpl*/
        $smarty->assign('type',$n);

        
        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
/*Carrega el menu*/
        require_once (FOLDER_LANGUAGES."/atencio_ciutadana/".$lang."_atencio_ciutadana.cfg");
/*Carrega el contingut de la opcio del menu*/
        require_once (FOLDER_LANGUAGES."/atencio_ciutadana/".$lang."_atencio_ciutadana".$n.".cfg");


        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");
      
        switch ($n) {
            case 1:
                $smarty->assign('url_ca','/atencio_ciutadana/index.php');
                $smarty->assign('url_es','/atencio_ciutadana/index.php?lang=es');
                break;
            case 2:
                $smarty->assign('url_ca','/atencio_ciutadana/atencio_ciutadana_horari.php');
                $smarty->assign('url_es','/atencio_ciutadana/atencio_ciutadana_horari.php?lang=es');
                break;
            case 3:
                $smarty->assign('url_ca','/atencio_ciutadana/atencio_ciutadana_seu.php');
                $smarty->assign('url_es','/atencio_ciutadana/atencio_ciutadana_seu.php?lang=es');
                break;
            case 4:
                $smarty->assign('url_ca','/atencio_ciutadana/atencio_ciutadana_contacta.php');
                $smarty->assign('url_es','/atencio_ciutadana/atencio_ciutadana_contacta.php?lang=es');
                break;
        }
        $smarty->display("atencio_ciutadana/atencio_ciutadana.tpl");
    }

/****/
/****/
/****/

 

    function home ($input_lang) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        $smarty->assign('url_ca','/');
        $smarty->assign('url_es','/es/');
        $smarty->assign('url_en','/en/');
        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_home.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->assign('LABEL_HDL', $this->headline($input_lang));
        $smarty->display("index.tpl");

    }

    /***function transparencia ($input_lang) {
        $smarty = new \webtortosa\Smarty_web();

        require_once (FOLDER_LANGUAGES."/".$this->languages($input_lang)."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$this->languages($input_lang)."_transparencia.cfg");
        require_once (FOLDER_LANGUAGES."/".$this->languages($input_lang)."_footer.cfg");

        $smarty->display("transparencia.tpl");
    }*///

    function pic ($input_lang) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/ajunta/pic/".$lang."_pic.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("ajunta/pic/pic.tpl");
    }

    function arxiuadm ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/arxiuadm/".$lang."_arxiuadm.cfg");
        require_once (FOLDER_LANGUAGES."/arxiuadm/".$lang."_arxiuadm".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("arxiuadm.tpl");
    }

    function alcalde ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");
        require_once (FOLDER_LANGUAGES."/alcalde/".$lang."_alcalde.cfg");
        require_once (FOLDER_LANGUAGES."/alcalde/".$lang."_alcalde".$n.".cfg");
        if ($n<5){
            $smarty->display("alcalde/alcalde.tpl");
        }
        else{
            $smarty->assign('ntpl', $n);
            $smarty->assign('ntitol', 5);
            $smarty->display("alcalde/alcAf.tpl");
        }
    }

    function pam ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('ntpl', $n);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/pam/".$lang."_pam.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

       
        $smarty->display("pam/pam.tpl");
    }

    function transpa ($input_lang) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/transparencia/".$lang."_transpa.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("transparencia/transpa.tpl");
    }

    function infpub ($input_lang) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/infpub/".$lang."_infpub.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("infpub/informacio_publica.tpl");
    }


    function infeco ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/infeco/".$lang."_infeco.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        if ($n==0){
            $smarty->display("infeco/infeco.tpl");
        }
        else{
            if ($n==1){
               $smarty->display("infeco/infeco_ep.tpl"); 
            }
        }
    }

    function infeco_expre ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/infeco/".$lang."_infeco.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        if ($n==117){
            $smarty->assign('image', "/images/infeco/tira_ep17.jpg");
            $smarty->display("infeco/ep17_infeco.tpl"); 
        }
    }

    function infeco_pressupost ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/infeco/".$lang."_infeco_presgen.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        if ($n==2017){
            $smarty->assign('image', "/images/infeco/tira_ep17.jpg");
            $smarty->display("infeco/ep17_pressupost.tpl"); 
        }
    }

    function infeco_execajt ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/infeco/".$lang."_infeco_execajt.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        if ($n==2017){
            $smarty->assign('image', "/images/infeco/tira_ep17.jpg");
            $smarty->display("infeco/ep17_execAjt.tpl"); 
        }
    }

    function infeco_execajtmodpre ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/infeco/".$lang."_infeco_execajtmodpre.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        if ($n==2017){
            $smarty->assign('image', "/images/infeco/tira_ep17.jpg");
            $smarty->display("infeco/ep17_execAjtmodpre.tpl"); 
        }
    }

    function infeco_evolucio ($input_lang, $n) {
        $smarty = new \webtortosa\Smarty_web();

        require_once (FOLDER_LANGUAGES."/".$this->languages($input_lang)."_header.cfg");
        require_once (FOLDER_LANGUAGES."/infeco/".$this->languages($input_lang)."_infeco_evolucio.cfg");
        require_once (FOLDER_LANGUAGES."/".$this->languages($input_lang)."_footer.cfg");

        $smarty->assign('image', "/images/infeco/tira_ev.jpg");
        if ($n==2017){
            $smarty->display("infeco/evolucio.tpl"); 
        }
        else{
            $smarty->assign('item', $n);
            $smarty->assign('graph', "/images/infeco/graph/evolucio".$n.".jpg");
            $smarty->display("infeco/evolucioN.tpl"); 
        }
    }

     function infeco_iplanif ($input_lang, $n) {
        $smarty = new \webtortosa\Smarty_web();

        require_once (FOLDER_LANGUAGES."/".$this->languages($input_lang)."_header.cfg");
        require_once (FOLDER_LANGUAGES."/infeco/".$this->languages($input_lang)."_infeco_iplanif.cfg");
        require_once (FOLDER_LANGUAGES."/".$this->languages($input_lang)."_footer.cfg");

        $smarty->assign('image', "/images/infeco/tira_ip.jpg");
        if ($n==2017){
            $smarty->display("infeco/iplanif.tpl"); 
        }
    }

    function estadistica_habitants($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('url_ca','/ciutat/estadistica-habitants/');

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/estadistica-habitants/".$lang."_estadistica-habitants.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");
        $smarty->display("estadistica-habitants/estadistica-habitants.tpl");
    }

    function turisme_oci ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/turisme-oci/".$lang."_turisme-oci".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("turisme-oci/turisme-oci.tpl");

    }

    function festes ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        if($n==1)
            require_once (FOLDER_LANGUAGES."/festes/".$lang."_festes".$n.".cfg");
        else
            require_once (FOLDER_LANGUAGES."/festes/".$lang."_festes2.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");
        $smarty->assign('type',$n);
        $smarty->display("festes/festes.tpl");

    }

    function transport ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('url_ca','/ciutat/transport/');

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/transport/".$lang."_transport".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("transport/transport.tpl");

    }

    function tortosabus ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/transport/".$lang."_tortosabus.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("transport/tortosabus.tpl");

    }

    function salut ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('url_ca','/ciutat/salut/');

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/salut/".$lang."_salut".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("salut/salut.tpl");

    }

    function cultura ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('url_ca','/ciutat/cultura/');

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/cultura/".$lang."_cultura".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("cultura/cultura.tpl");

    }

    function defensorc ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/defensor/".$lang."_defensor".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("defensorc.tpl");
    }


    function subvencions ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        
        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/subvencions/".$lang."_subvencions.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("subvencions/subvencions.tpl");
    }

    function exercicis ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('item',$n);
        
        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/subvencions/".$lang."_exercicis.cfg");
        require_once (FOLDER_LANGUAGES."/subvencions/".$lang."_exercicis1.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("subvencions/exercicis.tpl");
        
    }

    function nexercicis ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('item',$n);
        
        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/subvencions/".$lang."_nexercicis.cfg");
        require_once (FOLDER_LANGUAGES."/subvencions/".$lang."_exercicis".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("subvencions/nexercicis.tpl");
        
    }

    function pciutadana ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('item',$n);
        
        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/pciutadana/".$lang."_pciutadana.cfg");
        require_once (FOLDER_LANGUAGES."/pciutadana/".$lang."_pciutadana".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        if ($n==1){
            $smarty->display("pciutadana/pciutadana.tpl");
        }
        elseif ($n==2){
            $smarty->display("pciutadana/audiencies.tpl");
        }
        elseif ($n==3){
            $smarty->display("pciutadana/consulta.tpl");
        }
    }

    function presparticipa ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('item',$n);
        
        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/pciutadana/".$lang."_presparticipa.cfg");
        require_once (FOLDER_LANGUAGES."/pciutadana/".$lang."_presparticipa".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        if ($n==1){
            $smarty->display("pciutadana/presparticipa.tpl");
        }
        elseif ($n==2){
            $smarty->display("pciutadana/prespar2017.tpl");
        }
        
    }

    function organsparticipa ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('item',$n);
        
        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/pciutadana/".$lang."_organsparticipa.cfg");
        require_once (FOLDER_LANGUAGES."/pciutadana/".$lang."_organsparticipa".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("pciutadana/organsparticipa.tpl");
        
    }

    function sistemesparticipa ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('item',$n);
        
        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/pciutadana/".$lang."_sistemesparticipa.cfg");
        require_once (FOLDER_LANGUAGES."/pciutadana/".$lang."_sistemesparticipa".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("pciutadana/sistemes.tpl");
        
    }

    function serequi ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('item',$n);
        
        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/serequi/".$lang."_serequi.cfg");
        require_once (FOLDER_LANGUAGES."/serequi/".$lang."_serequi".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("serequi/serequi.tpl");
    }

    function serveis ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('item',$n);
        
        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/serequi/".$lang."_serveis.cfg");
        require_once (FOLDER_LANGUAGES."/serequi/".$lang."_serveis".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("serequi/serveis.tpl");
    }

    function equipa ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('item',$n);
        
        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/serequi/".$lang."_equipa.cfg");
        require_once (FOLDER_LANGUAGES."/serequi/".$lang."_equipa".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("serequi/equipa.tpl");
    }

   function educacio ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('url_ca','/ciutat/educacio/');

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/educacio/".$lang."_educacio0.cfg");
        if ($n>0){
            require_once (FOLDER_LANGUAGES."/educacio/".$lang."_educacio".$n.".cfg");
        }
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->assign('item', $n);
        $smarty->display("educacio/educacio.tpl");

    }

    function perfilc ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('item',$n);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
         require_once (FOLDER_LANGUAGES."/perfilc/".$lang."_perfilc.cfg");
        require_once (FOLDER_LANGUAGES."/perfilc/".$lang."_perfilc".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("perfilc/perfilc.tpl");
    }

    function laciutat ($input_lang) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('url_ca','/ciutat/');
        $smarty->assign('url_es','/es/ciudad/');

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/ciutat/".$lang."_ciutat.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("ciutat/laciutat.tpl");
    }

        function ciutat_imatges ($input_lang, $n) {
            $lang = $this->languages($input_lang);
            $smarty = new \webtortosa\Smarty_web();
            $smarty->assign('lang',$lang);
            $smarty->assign('url_ca','/ciutat/imatges/');

            $smarty->assign('ntpl', $n);

            require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
            require_once (FOLDER_LANGUAGES."/ciutat/".$lang."_imatges.cfg");
            require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

            $smarty->display("ciutat/ciutat_imatges.tpl");
        }

         function ciutat_municipi ($input_lang) {
             $lang = $this->languages($input_lang);
             $smarty = new \webtortosa\Smarty_web();
             $smarty->assign('lang',$lang);
             $smarty->assign('url_ca','/ciutat/municipi/');
             $smarty->assign('url_es','/es/ciudad/municipio/');

             require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
             require_once (FOLDER_LANGUAGES."/ciutat/".$lang."_municipi.cfg");
             require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

             $smarty->display("ciutat/ciutat_municipi.tpl");
        }

    function ciutat_territori ($input_lang) {
             $lang = $this->languages($input_lang);
             $smarty = new \webtortosa\Smarty_web();
             $smarty->assign('lang',$lang);
             $smarty->assign('url_ca','/ciutat/territori/');

             require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
             require_once (FOLDER_LANGUAGES."/ciutat/".$lang."_territori.cfg");
             require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

             $smarty->display("ciutat/ciutat_territori.tpl");
    }

    function ciutat_planols ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        $smarty->assign('ntitol', $n);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/ciutat/".$lang."_planols.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("ciutat/ciutat_planols.tpl");
    }

    function agermana ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        $smarty->assign('url_ca','/agermana/index.php');
        $smarty->assign('url_es','/agermana/index.php?lang=es');
        $smarty->assign('url_en','/agermana/index.php?lang=en');

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/agermana/".$lang."_agermana.cfg");
        require_once (FOLDER_LANGUAGES."/agermana/".$lang."_agermana".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("agermana/agermana.tpl");
    }

    function cuniversitaria ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/ciutat/".$lang."_cuniversitaria.cfg");
        require_once (FOLDER_LANGUAGES."/ciutat/".$lang."_cuniversitaria".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("ciutat/cuniversitaria.tpl");
    }

    function iviapublica ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('ntpl', $n);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/iviapublica/".$lang."_iviapublica.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("iviapublica/iviapublica.tpl");
    }

    function formivp ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('ntpl', $n);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/iviapublica/".$lang."_formivp.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("iviapublica/formivp.tpl");
    }


    function acciosocial ($input_lang, $m, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");
        if ($m==0){
            require_once (FOLDER_LANGUAGES."/acciosocial/".$lang."_as.cfg");
            require_once (FOLDER_LANGUAGES."/acciosocial/".$lang."_as0.cfg");
        }
        else{
            require_once (FOLDER_LANGUAGES."/acciosocial/".$lang."_as".$m.".cfg");
            require_once (FOLDER_LANGUAGES."/acciosocial/".$lang."_as".$m.$n.".cfg");
        }


        $smarty->display("acciosocial/as.tpl");
    }

    function mediambient ($input_lang, $m, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");
        if ($m==0){
            require_once (FOLDER_LANGUAGES."/mediambient/".$lang."_ma.cfg");
            require_once (FOLDER_LANGUAGES."/mediambient/".$lang."_ma0.cfg");
        }
        else{
            require_once (FOLDER_LANGUAGES."/mediambient/".$lang."_ma".$m.".cfg");
            require_once (FOLDER_LANGUAGES."/mediambient/".$lang."_ma".$m.$n.".cfg");
        }


        $smarty->display("mediambient/ma.tpl");
    }

    function divcul ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/divcul/".$lang."_divcul.cfg");
        require_once (FOLDER_LANGUAGES."/divcul/".$lang."_divcul".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("divcul/divcul.tpl");
    }

    function ensenya ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/ensenyament/".$lang."_ensenya.cfg");
        require_once (FOLDER_LANGUAGES."/ensenyament/".$lang."_ensenya".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("ensenyament/ensenya.tpl");
    }

    function pee616 ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/ensenyament/616/".$lang."_616.cfg");
        require_once (FOLDER_LANGUAGES."/ensenyament/616/".$lang."_616".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("ensenyament/616/616.tpl");
    }

    function ajunta ($input_lang) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/ajunta/".$lang."_ajunta.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("ajunta/ajunta.tpl");
    }


    function negocis ($input_lang) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('url_ca','/negocis/index.php');
        $smarty->assign('url_es','/negocis/index.php?lang=es');
        $smarty->assign('url_en','/negocis/index.php?lang=en');

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/negocis/".$lang."_negocis.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("negocis/negocis.tpl");
    }


    function negocis_i ($input_lang, $n, $nt) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('url_ca','/negocis/negocis'.$n.'.php');
        $smarty->assign('url_es','/negocis/negocis'.$n.'.php?lang=es');
        $smarty->assign('url_en','/negocis/negocis'.$n.'.php?lang=en');

        $smarty->assign('ntpl', $n);
        $smarty->assign('ntitol', $nt);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/negocis/".$lang."_negocis_i.cfg");
        require_once (FOLDER_LANGUAGES."/negocis/".$lang."_negocis_i".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("negocis/negocis_i.tpl");
    }


    // m: lloc que ocupa al menú    n: lloc que ocupa al submenú   nt: num elems que te el submenú
    function om ($input_lang, $m, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        $smarty->assign('ntpl', $n);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");
        if ($m==0){
            require_once (FOLDER_LANGUAGES."/ajunta/om/".$lang."_om.cfg");
            $smarty->display("ajunta/om/om.tpl");
        }
        else{
            if ($m==2 & $n>5)
                require_once (FOLDER_LANGUAGES."/ajunta/om/".$lang."_om".$m."b.cfg");
            else
                require_once (FOLDER_LANGUAGES."/ajunta/om/".$lang."_om".$m.".cfg");
            require_once (FOLDER_LANGUAGES."/ajunta/om/".$lang."_om".$m.$n.".cfg");
        }
        if ($m==1){
            if ($n==5)
                $smarty->display("ajunta/om/omAf.tpl");
            else
                $smarty->display("ajunta/om/omA.tpl");
        }
        elseif ($m==2){
            if ($n==3 | $n==4)
                $smarty->display("ajunta/om/omBf.tpl");
            else
                $smarty->display("ajunta/om/omB.tpl");
        }
        elseif ($m==3){
            if ($n==4)
                $smarty->display("ajunta/om/omBf.tpl");
            else
                $smarty->display("ajunta/om/omB.tpl");
        }
        elseif ($m==4){
            $smarty->display("ajunta/om/omB.tpl");
        }
        elseif ($m==5){
            $smarty->display("ajunta/om/omB.tpl");
        }
    }

    function infecon ($input_lang, $n, $nt, $nm, $nms) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/ajunta/infecon/".$lang."_infecon.cfg");
        require_once (FOLDER_LANGUAGES."/ajunta/infecon/".$lang."_infecon".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->assign('ntpl', $n);
        $smarty->assign('ntitol', $nt);
        $smarty->assign('nmi', $nm);
        $smarty->assign('nmisub', $nms);
        $smarty->assign('icon2', 'icon-angle-double-right');
        if ($n==1 | $n==5){ // Pressupost o Històric
            $smarty->assign('icon', 'icon-chart-bar');
        }
        elseif ($n==3){ // Execució
            $smarty->assign('icon', 'icon-chart-pie');
        }
        elseif ($n==4){ // PMP
            $smarty->assign('icon', 'icon-chart-area');
        }

        $smarty->display("ajunta/infecon/infecon.tpl");
    }

    function butlletins ($input_lang, $n, $nt) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        $smarty->assign('ntpl', $n);
        $smarty->assign('ntitol', $nt);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/butlletins/".$lang."_butlletins".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("butlletins/butlletins.tpl");
    }

    function deixalleria ($input_lang, $n, $nt) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        $smarty->assign('ntpl', $n);
        $smarty->assign('ntitol', $nt);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/deixalleria/".$lang."_deixalleria.cfg");
        require_once (FOLDER_LANGUAGES."/deixalleria/".$lang."_deixalleria".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("deixalleria/deixalleria.tpl");
    }

    function correus ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        $smarty->assign('ntpl', $n);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/correus/".$lang."_correus.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("correus/correus.tpl");
    }

    function telfurgen ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        $smarty->assign('ntpl', $n);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/telfurgen/".$lang."_telfurgen.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("telfurgen/telfurgen.tpl");
    }

    function telecentre ($input_lang, $n, $nt) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        $smarty->assign('ntpl', $n);
        $smarty->assign('ntitol', $nt);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/telecentre/".$lang."_telecentre.cfg");
        require_once (FOLDER_LANGUAGES."/telecentre/".$lang."_telecentre".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("telecentre/telecentre.tpl");
    }


    function headline ($input_lang) {
        require_once (FOLDER_MODEL."/headline.class.php");
        require_once (FOLDER_MODEL."/tematica.class.php");
        $headline = new \webtortosa\headline();
        $tematicadb = new \webtortosa\tematica();

        $elements = $headline->getHeadlinesDataOrdF();
        
        if($input_lang==""){
                $input_lang="ca";
        }
        $cont=1;
        foreach($elements as $itemElement) {
            $tematica = $tematicadb->getTematiquesForHeadlines($itemElement->ID);
            if($cont==1) {
                $content_text .= "<div class='caixaheadlinep'>"; 
            }
            else if($cont==2){
                $content_text .= "<div class='caixaheadlinep' id='caixaheadlinep2'>"; 
            }
            else if($cont==3){
                $content_text .= "<div class='caixaheadlinep' id='caixaheadlinep3'>"; 
            }
            else if($cont==4){
                $content_text .= "<div class='caixaheadlinep' id='caixaheadlinep4'>"; 
            }
            else if($cont==5){
                $content_text .= "<div class='caixaheadlinep' id='caixaheadlinep5'>"; 
            }
            else if($cont==6){
                $content_text .= "<div class='caixaheadlinep' id='caixaheadlinep6'>"; 
            }
            if($itemElement->FOTO==""){
                $content_text .= "<div><img src='/images/headlines/inotimage.jpg' width='220' height='125'></div>";
            }
            else{
                if($itemElement->IMPORT=="0")
                    $content_text .= "<div><img src='/images/headlines/min_".$itemElement->FOTO."' width='220' height='125'></div>";
                else
                    $content_text .= "<div><img src='http://www.tortosa.cat/webajt/gestiointerna/headlines/fotos/".$itemElement->FOTO."' width='220' height='125'></div>";
            }
            $content_text .= "<div class='caixaheadlinep_cos'>";
            $content_text .= "<div id='headlinep_data'>".strftime('%d/%m/%Y', strtotime($itemElement->DATA))."</div>";
            $content_text .= "<div id='headlinep_titol'>"."<a href='noticies/noticia.php?lang=".$input_lang."&id=".$itemElement->ID."'>".str_replace("\'", "'", $itemElement->TITOL)."</a></div>";
            $content_text .= "<div id='headlinep_subtitol'><strong>".str_replace("\'", "'", $tematica[0]->TEMATICA).". </strong>".str_replace("\'", "'", $itemElement->SUBTITOL)."</div>";
            $content_text .= "</div>";
            $content_text .= "</div>";
            $cont += 1;
        }
        return $content_text;
    }


    function noticies ($input_lang, $n, $nt, $params_values_get, $params_values_post) {
        require_once (FOLDER_MODEL."/headline.class.php");
        require_once (FOLDER_MODEL."/tematica.class.php");
        $headline = new \webtortosa\headline();
        $tematicadb = new \webtortosa\tematica();
        $utils = new \webtortosa\functions_controller();

        $input_lang = $input_lang=="" ? "ca" : "";
        $data_inici = $params_values_post['data_inici']!="" ? $params_values_post['data_inici'] : $params_values_get['data_inici'];
        $data_fi = $params_values_post['data_fi']!="" ? $params_values_post['data_fi'] : $params_values_get['data_fi'];
        $text = $params_values_post['text']!="" ? $params_values_post['text'] : $params_values_get['text'];
        //echo $data_inici . ' - ' . $data_fi . ' - ' . $text;

        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        $smarty->assign('ntpl', $n);
        $smarty->assign('ntitol', $nt);

        $file_logs = FOLDER_LOGS . '/'.date('d-m-Y').'.log';
        $this->escribir_log($file_logs, 'Entra a noticies');

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/noticies/".$lang."_noticies".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $params = array(
            'data_inici'    => $utils->normaldate_to_mysql($utils->test_input($data_inici)),
            'data_fi'       => $utils->normaldate_to_mysql($utils->test_input($data_fi)),
            'text'          => $utils->test_input($text)
        );


        /* Pagination */
        $total = $headline->getHeadlinesDataForListMaxium($params);
        $page = $params_values_get['page'];
        $start = $page ? ($page - 1) * ITEMS_LIMIT : 0;

        if($page==0)
            $page = 1;
        $prev = $page - 1; //previous page is current page - 1
        $next = $page + 1; //next page is current page + 1
        $lastpage = ceil($total/ITEMS_LIMIT); //lastpage.

        $elements = $headline->getHeadlinesDataForList($params, $start, ITEMS_LIMIT);

        $tematiques = array();
        foreach($elements as &$element) {
            $element->FOTO = str_replace("'","·", $element->FOTO);
            $tematica = $tematicadb->getTematiquesForHeadlines($element->ID);
            $tematiques[$element->ID] = $tematica[0]->TEMATICA;
        }
        $smarty->assign("data_inici",$data_inici);
        $smarty->assign("data_fi",$data_fi);
        $smarty->assign("text",$text);

        $smarty->assign("lastpage",$lastpage);
        $smarty->assign("page",$page);
        $smarty->assign("items",$elements);
        $smarty->assign("tematiques",$tematiques);

        //var_dump($elements);
        $smarty->assign('lang', $input_lang);
        $smarty->assign('destination_form', htmlspecialchars($_SERVER["PHP_SELF"]));
        $smarty->display("noticies/llistat_noticies.tpl");
    }


    function noticia ($input_lang, $id) {
        require_once (FOLDER_MODEL."/headline.class.php");
        require_once (FOLDER_MODEL."/tematica.class.php");
        require_once (FOLDER_MODEL."/tipologia.class.php");

        $headline = new \webtortosa\headline();
        $tematicadb = new \webtortosa\tematica();
        $tipologiadb = new \webtortosa\tipologia();

        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/noticies/".$lang."_noticia".".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $params = array(
            ID=>$id
        );
        $elements = $headline->getHeadlinesData($params);
        $content_text = "";
        foreach($elements as $itemElement) {
            $tematiques = $tematicadb->getTematiquesForHeadlines($itemElement->ID);
            $smarty->assign('date_publish', strftime('%d/%m/%Y', strtotime($itemElement->DATA)));
            $smarty->assign('time_publish', strftime('%R', strtotime($itemElement->DATA)));

            $smarty->assign('TITOL', str_replace("\'", "'", $itemElement->TITOL));
            $smarty->assign('SUBTITOL', str_replace("\'", "'", $itemElement->SUBTITOL));
            $smarty->assign('COS', str_replace("\'", "'", str_replace("\'", "'", str_replace("\r\n", "<br />", $itemElement->DESCRIPCIO))));

             if($itemElement->FOTO==""){
                 $smarty->assign("IMAGE", "<img src='/images/headlines/inotimage.jpg' width='220' height='125'>");
            }
            else{
                if($itemElement->IMPORT=="0")
                    $smarty->assign("IMAGE", "<img src='/images/headlines/res_".$itemElement->FOTO."' width='220' height='125'>");
                else
                    $smarty->assign("IMAGE", "<img src='http://www.tortosa.cat/webajt/gestiointerna/headlines/fotos/".$itemElement->FOTO."' width='220' height='125'>");
            }
            if(count($tematiques)>0) {
                $content_tematica = array();
                foreach($tematiques as $tematica) {
                    $content_tematica[] =  $tematica->TEMATICA;
                }
            }
            $params_tipologia = array(
                "ID"   => $itemElement->ID_TIPOLOGIA
            );
            $tipologies = $tipologiadb->getTipologiaData($params_tipologia);
            if(count($tipologies)>0) {
                $content_tipologia = array();
                foreach($tipologies as $tipologia) {
                    $content_tipologia[] = $tipologia->TIPOLOGIA;
                }
            }
            var_dump($content_tipologia);
        }


        //$smarty->assign('LABEL_TC1', $content_text);
        $smarty->assign('LABEL_TC_TEMATICA', $content_tematica);
        $smarty->assign('LABEL_TC_TIPOLOGIA', $content_tipologia);

        $smarty->display("noticies/noticia.tpl");
    }


    function urbanisme ($input_lang, $m, $n, $slug) {
        require_once (FOLDER_MODEL."/urbanisme.class.php");
        $urbanisme = new \webtortosa\urbanisme();
        $params['NIVELL'] = 1;
        $menu = $urbanisme->getMenuUrbanismeData($params);
        $listMenu = array();
        foreach($menu as $itemMenu) {
            $submenu = $urbanisme->getMenuUrbanismeData(array('ID_NIVELL_SUPERIOR'=>$itemMenu->ID));
            if(count($submenu)>0) {
                $listMenu[$itemMenu->TITOL] = array();
                foreach ($submenu as $subitemMenu) {
                    array_push($listMenu[$itemMenu->TITOL], array($subitemMenu->TITOL, $subitemMenu->LINK));
                }
            }
            else {
                $listMenu[$itemMenu->TITOL] = $itemMenu->LINK;
            }
        }

        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");
        if ($m==0){
            require_once (FOLDER_LANGUAGES."/urbanisme/".$lang."_urbanisme.cfg");
        }
        else{
            require_once (FOLDER_LANGUAGES."/urbanisme/".$lang."_urbanisme".$m.".cfg");
            require_once (FOLDER_LANGUAGES."/urbanisme/".$lang."_urbanisme".$m.$n.".cfg");
        }

        if (($m==0)&&($slug=="")){

            $smarty->assign('menu_dinamic', $listMenu);
            $smarty->display("urbanisme/urbanisme.tpl");
        }
        else{
            if($slug!="") {
                $titles = $urbanisme->getTreeMenuFromSlug($slug);
                $content_title = "";
                if(count($titles)>0) {
                    $content_title .= "<div id='title_page_parent'>".$titles[0]->TITOL_MENU_PARENT."</div>";
                    $content_title .= "<div id='title_page'>".$titles[0]->TITOL_MENU."</div>";
                }

                $elements = $urbanisme->getElementUrbanismeFromElementMenu($slug);
                $content_text = "";
                foreach($elements as $itemElement) {
                    $content_text .= "<div class='element_container'>
                    <div class='title_element'>".$itemElement->TITOL."</div>";
                    $params = array('ID_ELEMENT'=>$itemElement->ID);
                    $links = $urbanisme->getLinksUrbanismeData($params);
                    foreach($links as $key => $itemLink) {
                        if($key==0)
                            $content_text .= "<ul class='llista'>";
                        $content_text .= "<li class='item_link_container'>";
                        if(strpos($itemLink->LINK, ".pdf")>0)
                            $content_text .= "<i class='icon-file-pdf'></i>";
                        else
                            $content_text .= "<i class='icon-folder-open-empty'></i>";

                        $content_text .= "<div class='item_title_link'><a href='".$itemLink->LINK."' title='".$itemLink->TITOL."' target='_blank'>".$itemLink->TITOL."</a>";
                        if($itemLink->TIPUS!="altres")    $content_text .= "<span class='item_type_link'>".$itemLink->TIPUS."</span>";
                        $content_text .= "  </div>
                                         </li>";
                    }
                    if($key>0)
                        $content_text .= "</ul>";

                    $content_text .= "</div>";
                }
                $smarty->assign('LABEL_TC1', $content_title . $content_text);
            }
            $smarty->display("urbanisme/urbanismeA.tpl");
        }

    }



    function electes ($input_lang) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/eleccions/".$lang."_electes.cfg");

        $smarty->display("eleccions/electes.tpl");
    }


    /******************************************
     ********************************************
     ********************************************
        FI Funcions per a WEB
     ********************************************
     ********************************************************************************************************************
     ********************************************/


    /******************************************
    ********************************************
    ********************************************
       Funcions per a l'apartat de ELECCIONS
    ********************************************
    ********************************************
    ********************************************/

    function home_eleccions ($input_lang) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

		require_once (FOLDER_LANGUAGES."/".$this->languages($input_lang)."_ajunta.cfg");
		
        $smarty->display("index_eleccions.tpl");
    }

    function eleccionsElectes2015 ($params_values_get) {
    	$_SESSION['id_eleccions'] = $params_values_get["eleccions"] ? $params_values_get["eleccions"] : $_SESSION['id_eleccions'];
        $_SESSION['id_eleccions'] = $_SESSION['id_eleccions'] ? $_SESSION['id_eleccions'] : '2015MUN';

		$smarty = new \webtortosa\Smarty_web();
		$smarty->assign("path_eleccions","http://www2.tortosa.cat/templates");
        $smarty->assign("apartat","eleccions");
		$smarty->assign("path_charts",URL_CHARTS);
		$smarty->display("head.tpl");

        require_once (FOLDER_LANGUAGES."/".$this->languages($params_values_get["lang"])."_header.cfg");
        require_once (FOLDER_LANGUAGES."/eleccions/".$this->languages($params_values_get["lang"])."_eleccions.cfg");
        require_once (FOLDER_LANGUAGES."/".$this->languages($input_lang)."_footer.cfg");

		$dades_eleccions = new \webtortosa\eleccions();
		$eleccions = $dades_eleccions->getEleccionsData();
		$smarty->assign('eleccions',$this->filtraArrayUtf8Encode($eleccions));
		$smarty->assign('id_eleccio',$_SESSION['id_eleccions']);
		$smarty->display("eleccions/electes.tpl");
    }

    function eleccionsResultatsMeses ($params_values_get) {
    	$_SESSION['id_eleccions'] = $params_values_get["eleccions"] ? $params_values_get["eleccions"] : $_SESSION['id_eleccions'];
        $_SESSION['id_eleccions'] = $_SESSION['id_eleccions'] ? $_SESSION['id_eleccions'] : $this->getUltimaEleccio();

    	$seccions = $params_values_get["meses"] ? $params_values_get["meses"] : '01|001|01001U';

    	$seccions = explode("|", $seccions);
    	$districte = $seccions[0];
    	$seccio = $seccions[1];
    	$mesa = $seccions[2];

		$smarty = new \webtortosa\Smarty_web();
		$smarty->assign("path_eleccions","http://www2.tortosa.cat/templates");
        $smarty->assign("apartat","eleccions");
		$smarty->assign("path_charts",URL_CHARTS);
		$smarty->display("head.tpl");

        require_once (FOLDER_LANGUAGES."/".$this->languages($params_values_get["lang"])."_header.cfg");
        require_once (FOLDER_LANGUAGES."/eleccions/".$this->languages($params_values_get["lang"])."_eleccions.cfg");
        require_once (FOLDER_LANGUAGES."/".$this->languages($input_lang)."_footer.cfg");

		$dades_eleccions = new \webtortosa\eleccions();
		$params = array($_SESSION['id_eleccions']);

		$meses = $dades_eleccions->getMeses($params);
		foreach ($meses as $key => &$item_mesa) {
			$item_mesa->NomColegi = utf8_encode($item_mesa->NomColegi);
		}
		$smarty->assign('num_mesa',$districte."|".$seccio."|".$mesa);
		$smarty->assign('data_seccions',$meses);
		$params = array($_SESSION['id_eleccions'], $districte, $seccio, $mesa);
		$resultats = $dades_eleccions->getResultatsMeses($params);

		if(count($resultats)) {
			//Busco el numero de Vots nuls per poder restar alhora de fer el percentatge
			foreach ($resultats as $key => $element) {
				if($element->Sigles=="Nuls")
					$VotsNuls = $element->Vots;

				$denominacio = $element->NomEleccio;
                $logo_eleccions = $element->LogoEleccio;
				$VotsTotals = $element->VotsTotals;
				$AnyEleccio = $element->Any;
				$MaxDistricte = $element->MaxDistricte;
				$CensMesa = $element->CensMesa;
			}

			$actualData = array();
			$preparacio_resultats = array(
			 array('Mesa '.$mesa)
			);

			$primeraVegada = true;
			foreach ($resultats as $key => $element) {
				if ($primeraVegada) {
					$fila = array('Cens', $element->CensMesa, ' ');
					array_push($preparacio_resultats,$fila);
					$fila = array('Vots emesos', $element->VotsTotals, number_format($element->VotsTotals/$element->CensMesa*100,2) ." %");
					array_push($preparacio_resultats,$fila);
					$fila = array('Vots nuls', $VotsNuls, number_format( $VotsNuls/$element->VotsTotals*100,2) ." %");
					array_push($preparacio_resultats,$fila);
					$fila = array('Vots vàlids', $element->VotsTotals - $VotsNuls, number_format( ($element->VotsTotals - $VotsNuls)/$element->VotsTotals*100,2) ." %");
					array_push($preparacio_resultats,$fila);

					array_push($preparacio_resultats,array('Partit', 'Vots', '% s/ mesa'));
					$primeraVegada = false;
				}

				if (!$element->esVotNul) {
					$percentatgeResultats = number_format($element->Vots/($VotsTotals - $VotsNuls)*100,2)." %";
					$fila = array(utf8_encode($element->Sigles), $element->Vots, $percentatgeResultats);
					array_push($preparacio_resultats,$fila);
					$actualData[$element->Sigles] = $element->Vots;
					$colorsArr[$element->Sigles] = $element->Color;
				}
			}
			$smarty->assign('taula1',$preparacio_resultats);

			$colors = implode(",", $colorsArr);
			$chart = $this->setChartSettings("Resultats Mesa ".$mesa, "Total vots", "column3d", $colors, $actualData, "chart-1", "420", "300");
			$smarty->assign('chart1',$chart);

		}
		$eleccions = $dades_eleccions->getEleccionsData();
		$smarty->assign('title','Resultats per mesa '.utf8_encode($denominacio));
        $smarty->assign('subtitle','Dades informatives');
        $smarty->assign('logo_eleccions',$logo_eleccions);
		$smarty->assign('eleccions',$this->filtraArrayUtf8Encode($eleccions));
		$smarty->assign('id_eleccio',$_SESSION['id_eleccions']);

		$smarty->display("eleccions/resultats_meses.tpl");
    }

    function eleccionsResultatsSeccions ($params_values_get) {
    	$_SESSION['id_eleccions'] = $params_values_get["eleccions"] ? $params_values_get["eleccions"] : $_SESSION['id_eleccions'];
        $_SESSION['id_eleccions'] = $_SESSION['id_eleccions'] ? $_SESSION['id_eleccions'] : $this->getUltimaEleccio();
    	$seccions = $params_values_get["seccions"] ? $params_values_get["seccions"] : '01|001';

    	$seccions = explode("|", $seccions);
    	$districte = $seccions[0];
    	$seccio = $seccions[1];

		$smarty = new \webtortosa\Smarty_web();
		$smarty->assign("path_eleccions","http://www2.tortosa.cat/templates");
        $smarty->assign("apartat","eleccions");
		$smarty->assign("path_charts",URL_CHARTS);
		$smarty->display("head.tpl");

        require_once (FOLDER_LANGUAGES."/".$this->languages($params_values_get["lang"])."_header.cfg");
        require_once (FOLDER_LANGUAGES."/eleccions/".$this->languages($params_values_get["lang"])."_eleccions.cfg");
        require_once (FOLDER_LANGUAGES."/".$this->languages($input_lang)."_footer.cfg");

		$dades_eleccions = new \webtortosa\eleccions();
		$params = array($_SESSION['id_eleccions'], $districte, $seccio);

		$seccions = $dades_eleccions->getSeccions($params);
		$smarty->assign('num_seccio',$districte."|".$seccio);
		$smarty->assign('data_seccions',$seccions);
		$resultats = $dades_eleccions->getResultatsSeccions($params);

		if(count($resultats)) {
			//Busco el numero de Vots nuls per poder restar alhora de fer el percentatge
			foreach ($resultats as $key => $element) {
				if(strtoupper($element->Sigles)=="NULS")
					$VotsNuls = $element->Vots;
				$denominacio = $element->NomEleccio;
                $logo_eleccions = $element->LogoEleccio;
				$VotsTotals = $element->VotsTotals;
				$AnyEleccio = $element->Any;
			}

			$actualData = array();
			$preparacio_resultats = array(
			 array('Districte '.$districte.' - Secció '.$seccio)
			);

			$primeraVegada = true;
			foreach ($resultats as $key => $element) {
				if ($primeraVegada) {
					$fila = array('Cens', $element->CensSeccio, ' ');
					array_push($preparacio_resultats,$fila);
					$fila = array('Meses', $element->NumMesas, ' ');
					array_push($preparacio_resultats,$fila);
					$fila = array('Vots emesos', $element->VotsTotals, number_format($element->VotsTotals/$element->CensSeccio*100,2) ." %");
					array_push($preparacio_resultats,$fila);
					$fila = array('Vots nuls', $VotsNuls, number_format( $VotsNuls/$element->VotsTotals*100,2) ." %");
					array_push($preparacio_resultats,$fila);
					$fila = array('Vots vàlids', $element->VotsTotals - $VotsNuls, number_format( ($element->VotsTotals - $VotsNuls)/$element->VotsTotals*100,2) ." %");
					array_push($preparacio_resultats,$fila);

					array_push($preparacio_resultats,array('Partit', 'Vots', '% s/ secció'));
					$primeraVegada = false;
				}

				if (!$element->esVotNul) {
					$percentatgeResultats = number_format($element->Vots/($VotsTotals - $VotsNuls)*100,2)." %";
					$fila = array(utf8_encode($element->Sigles), $element->Vots, $percentatgeResultats);
					array_push($preparacio_resultats,$fila);
					$actualData[$element->Sigles] = $element->Vots;
					$colorsArr[$element->Sigles] = $element->Color;
				}
			}
			$smarty->assign('taula1',$preparacio_resultats);

			$colors = implode(",", $colorsArr);
			$chart = $this->setChartSettings("Resultats Districte ".$districte." - Secció ".$seccio, "Total vots", "column3d", $colors, $actualData, "chart-1", "420", "300");
			$smarty->assign('chart1',$chart);

		}
		$eleccions = $dades_eleccions->getEleccionsData();
		$smarty->assign('title','Resultats per secció '.utf8_encode($denominacio));
        $smarty->assign('subtitle','Dades informatives');
        $smarty->assign('logo_eleccions',$logo_eleccions);
		$smarty->assign('eleccions',$this->filtraArrayUtf8Encode($eleccions));
		$smarty->assign('id_eleccio',$_SESSION['id_eleccions']);

		$smarty->display("eleccions/resultats_seccions.tpl");
    }

    function eleccionsResultatsDistrictes ($params_values_get) {
    	$_SESSION['id_eleccions'] = $params_values_get["eleccions"] ? $params_values_get["eleccions"] : $_SESSION['id_eleccions'];
        $_SESSION['id_eleccions'] = $_SESSION['id_eleccions'] ? $_SESSION['id_eleccions'] : $this->getUltimaEleccio();

		$smarty = new \webtortosa\Smarty_web();
		$smarty->assign("path_eleccions","http://www2.tortosa.cat/templates");
        $smarty->assign("apartat","eleccions");
		$smarty->assign("path_charts",URL_CHARTS);
		$smarty->display("head.tpl");

        require_once (FOLDER_LANGUAGES."/".$this->languages($params_values_get["lang"])."_header.cfg");
        require_once (FOLDER_LANGUAGES."/eleccions/".$this->languages($params_values_get["lang"])."_eleccions.cfg");
        require_once (FOLDER_LANGUAGES."/".$this->languages($input_lang)."_footer.cfg");

		$dades_eleccions = new \webtortosa\eleccions();
		$params = array($_SESSION['id_eleccions']);
		// Consulta resultats districtes
		$resultats = $dades_eleccions->getResultatsDistrictes($params);
		if(count($resultats)) {
			//Busco el numero de Vots nuls per poder restar alhora de fer el percentatge
			$VotsNuls = array();
			foreach ($resultats as $key => $element) {
				if(strtoupper($element->Sigles)=="NULS")
					$VotsNuls[$element->Districte] = $element->Vots;

				$denominacio = $element->NomEleccio;
                $logo_eleccions = $element->LogoEleccio;
				$AnyEleccio = $element->Any;
				$MaxDistricte = $element->MaxDistricte;
			}

			for($districte==1;$districte<=$MaxDistricte;$districte++) {
				$actualData = array();
				$colorsArr = array();
 				$preparacio_resultats = array(
				 array('Districte '.$districte)
				);

				$posaDades = false;
				$currentDistricte = false;

				foreach ($resultats as $key => $element) {
					if((integer)$element->Districte==$districte) {

						if (!$currentDistricte) {
							$fila = array('Cens', $element->CensDistricte, ' ');
							array_push($preparacio_resultats,$fila);
							$fila = array('Meses', $element->NumMesas, ' ');
							array_push($preparacio_resultats,$fila);
							$fila = array('Vots emesos', $element->VotsTotals, number_format($element->VotsTotals/$element->CensDistricte*100,2) ." %");
							array_push($preparacio_resultats,$fila);
							$fila = array('Vots nuls', $VotsNuls[$element->Districte], number_format( $VotsNuls[$element->Districte]/$element->VotsTotals*100,2) ." %");
							array_push($preparacio_resultats,$fila);
							$fila = array('Vots vàlids', $element->VotsTotals - $VotsNuls[$element->Districte], number_format( ($element->VotsTotals - $VotsNuls[$element->Districte])/$element->VotsTotals*100,2) ." %");
							array_push($preparacio_resultats,$fila);

							array_push($preparacio_resultats,array('Partit', 'Vots', '% s/ districte'));
							$currentDistricte = true;
						}


						if (!$element->esVotNul) {
							$percentatgeResultats = number_format($element->Vots/($element->VotsTotals - $VotsNuls[$element->Districte])*100,2)." %";
							$fila = array(utf8_encode($element->Sigles), $element->Vots, $percentatgeResultats);
							array_push($preparacio_resultats,$fila);
							$actualData[$element->Sigles] = $element->Vots;
							$colorsArr[$element->Sigles] = $element->Color;
							$posaDades = true;
						}
					}
				}

				if($posaDades) {
					$smarty->assign('taula'.$districte,$preparacio_resultats);
					$colors = implode(",", $colorsArr);
					$chart = $this->setChartSettings("Resultats Districte ".$districte, "Total vots", "column3d", $colors, $actualData, "chart-".$districte, "420", "300");
					$smarty->assign('chart'.$districte,$chart);
				}
			}

		}
		// Fi Consulta resultats districtes
		$eleccions = $dades_eleccions->getEleccionsData();
		$smarty->assign('title','Resultats per districte '.utf8_encode($denominacio));
        $smarty->assign('subtitle','Dades informatives');
        $smarty->assign('logo_eleccions',$logo_eleccions);
		$smarty->assign('eleccions',$this->filtraArrayUtf8Encode($eleccions));
		$smarty->assign('id_eleccio',$_SESSION['id_eleccions']);

		$smarty->display("eleccions/resultats_districtes.tpl");
    }

    function eleccionsResultatsTotals ($params_values_get) {
    	$_SESSION['id_eleccions'] = $params_values_get["eleccions"] ? $params_values_get["eleccions"] : $_SESSION['id_eleccions'];
        $_SESSION['id_eleccions'] = $_SESSION['id_eleccions'] ? $_SESSION['id_eleccions'] : $this->getUltimaEleccio();

		$smarty = new \webtortosa\Smarty_web();
		$smarty->assign("path_eleccions","http://www2.tortosa.cat/templates");
        $smarty->assign("apartat","eleccions");
		$smarty->assign("path_charts",URL_CHARTS);
		$smarty->display("head.tpl");

        require_once (FOLDER_LANGUAGES."/".$this->languages($params_values_get["lang"])."_header.cfg");
        require_once (FOLDER_LANGUAGES."/eleccions/".$this->languages($params_values_get["lang"])."_eleccions.cfg");
        require_once (FOLDER_LANGUAGES."/".$this->languages($input_lang)."_footer.cfg");

		$dades_eleccions = new \webtortosa\eleccions();

		/* Consulta vots totals */
		$params = array('IdEleccio'=>$_SESSION['id_eleccions']);
		$vots = $dades_eleccions->getEleccionsData($params);
		if(count($vots)>0) {
			$preparacio_vots = array(
				array('', $vots[0]->Any)
			);
			$fila = array('Meses: ',$this->passa_milers(count($dades_eleccions->getMesesData(array('RefEleccio'=>$_SESSION['id_eleccions'])))));
			array_push($preparacio_vots,$fila);
			$fila = array('Cens: ',$this->passa_milers($vots[0]->CensVotants));
			array_push($preparacio_vots,$fila);
			$fila = array('','Vots','% s/Cens', '% s/vots Emesos', '%s/vots Vàlids');
			array_push($preparacio_vots,$fila);

			//Abstenció
			$abstencio_percent_s_cens = number_format(($vots[0]->CensVotants - $vots[0]->VotsEmesos) / $vots[0]->CensVotants * 100, 2);
			$fila = array('Abstenció: ',$this->passa_milers($vots[0]->CensVotants - $vots[0]->VotsEmesos), $abstencio_percent_s_cens . " %", '', '');
			array_push($preparacio_vots,$fila);

			// Vots emesos
			$votants_percent_s_cens = number_format($vots[0]->VotsEmesos / $vots[0]->CensVotants * 100, 2);
			$fila = array('Vots emesos: ',$this->passa_milers($vots[0]->VotsEmesos), $votants_percent_s_cens . " %", '', '');
			array_push($preparacio_vots,$fila);

			//Separació
			$fila = array('','', '', '', '');
			array_push($preparacio_vots,$fila);

			//Vots nuls
			$vots_nuls_percent_s_cens = number_format($vots[0]->VotsNuls / $vots[0]->CensVotants * 100, 2);
			$vots_nuls_percent_s_vemesos = number_format($vots[0]->VotsNuls / $vots[0]->VotsEmesos * 100, 2);
			$fila = array('Vots nuls: ',$this->passa_milers($vots[0]->VotsNuls), $vots_nuls_percent_s_cens . " %", $vots_nuls_percent_s_vemesos . " %", '');
			array_push($preparacio_vots,$fila);

			//Vots vàlids
			$vots_valids_percent_s_cens = number_format($vots[0]->VotsValids / $vots[0]->CensVotants * 100, 2);
			$vots_valids_percent_s_vemesos = number_format($vots[0]->VotsValids / $vots[0]->VotsEmesos * 100, 2);
			$fila = array('Vots vàlids: ',$this->passa_milers($vots[0]->VotsValids), $vots_valids_percent_s_cens . " %", $vots_valids_percent_s_vemesos . " %",'');
			array_push($preparacio_vots,$fila);

			//Separació
			$fila = array('','', '', '', '');
			array_push($preparacio_vots,$fila);

			//Vots en blanc
			$vots_blanc_percent_s_cens = number_format($vots[0]->VotsBlancs / $vots[0]->CensVotants * 100, 2);
			$vots_blanc_percent_s_vemesos = number_format($vots[0]->VotsBlancs / $vots[0]->VotsEmesos * 100, 2);
			$vots_blanc_percent_s_vvalids = number_format($vots[0]->VotsBlancs / $vots[0]->VotsValids * 100, 2);
			$fila = array('Vots en blanc: ',$this->passa_milers($vots[0]->VotsBlancs), $vots_blanc_percent_s_cens . " %", $vots_blanc_percent_s_vemesos . " %", $vots_blanc_percent_s_vvalids . " %");
			array_push($preparacio_vots,$fila);

			//Vots a candidatures
			$vots_candidatures_percent_s_cens = number_format($vots[0]->VotsCandidatures / $vots[0]->CensVotants * 100, 2);
			$vots_candidatures_percent_s_vemesos = number_format($vots[0]->VotsCandidatures / $vots[0]->VotsEmesos * 100, 2);
			$vots_candidatures_percent_s_vvalids = number_format($vots[0]->VotsCandidatures / $vots[0]->VotsValids * 100, 2);
			$fila = array('Vots a candidatures: ',$this->passa_milers($vots[0]->VotsCandidatures), $vots_candidatures_percent_s_cens . " %", $vots_candidatures_percent_s_vemesos . " %", $vots_candidatures_percent_s_vvalids . " %");
			array_push($preparacio_vots,$fila);

			$smarty->assign('taula1',$preparacio_vots);
		}

		// Consulta resultats totals per partit
		$params = array($_SESSION['id_eleccions']);
		$resultats = $dades_eleccions->getResultatsTotals($params);

		if(count($resultats)) {
			$preparacio_participacio = array(
				 array('Candidatura', 'Vots', '% s/Vots Vàlids')
				);
			//Busco el numero de Vots nuls per poder restar alhora de fer el percentatge
			foreach ($resultats as $key => $element) {
				if($element->esVotNul)
					$VotsNuls = $element->Vots;
				$denominacio = $element->NomEleccio;
                $logo_eleccions = $element->LogoEleccio;
				$regidors = $element->NumRegidors;
				$VotsTotals = $element->VotsTotals;
				$RefEleccioComparada = $element->RefEleccioComparada;
				$AnyEleccio = $element->Any;
			}
			$actualData = array();
			$colorsArr = array();
			$HontArr = array();
			foreach ($resultats as $key => $element) {
				//echo $element->Sigles . "->" .$element->esVotNul. " - ". $element->esVotBlanc . "<br>";
				if ((!$element->esVotNul)&&(!$element->esVotBlanc)) {
					$percentatgeResultats = number_format($element->Vots/($VotsTotals - $VotsNuls)*100,2)." %";
					$fila = array(utf8_encode($element->Sigles), $this->passa_milers($element->Vots), $percentatgeResultats);
					array_push($preparacio_participacio,$fila);
					//Dades per al gràfic
					$actualData[strtoupper($element->Sigles) . " - " . $AnyEleccio] = $element->Vots;
					$colorsArr[strtoupper($element->Sigles) . " - " . $AnyEleccio] = $element->Color;
					$HontArr[strtoupper($element->Sigles) . " - " . $AnyEleccio] = $element->Vots;
					$HontArrSenseAny[strtoupper($element->Sigles)] = $element->Vots;
				}
			}
			//echo var_dump($preparacio_participacio);
			$smarty->assign('taula2',$preparacio_participacio);

			$colors = implode(",", $colorsArr);
			$chart = $this->setChartSettings("Resultats ".utf8_encode($denominacio), "Total vots", "column3d", $colors, $actualData, "chart-1", "500", "600");

			$smarty->assign('chart1',$chart);

			//Resultats regidors, en el cas de municipals
			if(($regidors!=NULL)&&($regidors>0)) {
				$actualDataHont = array();
				$resultHont = $this->Hont($HontArrSenseAny, $regidors, $VotsTotals, $VotsNuls);
				foreach ($resultHont as $key => $itemHont) {
					$actualDataHont[utf8_decode($key) . " - " . $AnyEleccio] = utf8_decode($itemHont);
				}

				$chart = $this->setChartSettings("Llei d'Hont", "", "Pie3D", $colors, $actualDataHont, "chart-2", "500", "300", " reg.");
			}
			$smarty->assign('chart2',$chart);

			//Comparativa resultats amb eleccions anteriors
		    if($RefEleccioComparada!="") {
		    	$params = array($RefEleccioComparada);
				// Consulta resultats totals per partit
				$resultats_comparativa = $dades_eleccions->getResultatsTotals($params);
				if(count($resultats_comparativa)) {
					foreach ($resultats_comparativa as $key => $element) {
						if($resultats_comparativa->Sigles=="Nuls")
							$VotsNuls_comparativa = $element->Vots;
						$denominacio_comparativa = $element->NomEleccio;
						$regidors_comparativa = $element->NumRegidors;
						$VotsTotals_comparativa = $element->VotsTotals;
						$AnyEleccio_comparativa = $element->Any;
					}
					$actualData_comparativa = array();
					$colorsArr_comparativa = array();
					$HontArr_comparativa = array();
					foreach ($resultats_comparativa as $key => $element) {
						$percentatgeResultats_comparativa = number_format($element->Vots/($VotsTotals_comparativa - $VotsNuls_comparativa)*100,2)." %";
						$actualData_comparativa[strtoupper($element->Sigles) . " - " . $AnyEleccio_comparativa] = $element->Vots;
						$colorsArr_comparativa[strtoupper($element->Sigles) . " - " . $AnyEleccio_comparativa] = $element->Color;
						$HontArr_comparativa[strtoupper($element->Sigles)] = $element->Vots;
					}
					$actualData = array_merge($actualData, $actualData_comparativa);
					$colorsArrResultComp = array_merge($colorsArr, $colorsArr_comparativa);
					ksort($colorsArrResultComp);
					$colors = implode(",", $colorsArrResultComp);
					ksort($actualData);
				}

				$chart = $this->setChartSettings("Resultats ".utf8_encode($denominacio)." - comparativa ".$denominacio_comparativa, "Total vots", "column3d", $colors, $actualData, "chart-3", "500", "600");
		    }
		    $smarty->assign('chart3',$chart);

		    //Resultats regidors, en el cas de municipals
			if(($regidors_comparativa!=NULL)&&($regidors_comparativa>0)) {
				$actualDataHont_comparativa = array();
				$resultHont_comparativa = $this->Hont($HontArr_comparativa, $regidors_comparativa, $VotsTotals_comparativa, $VotsNuls_comparativa);
				foreach ($resultHont_comparativa as $key => $itemHont) {
					$actualDataHont_comparativa[utf8_decode($key . " - " . $AnyEleccio_comparativa)] = utf8_decode($itemHont);
				}
				$actualDataHont = array_merge($actualDataHont, $actualDataHont_comparativa);
				Ksort($actualDataHont);
				//Tractament del colors
				$colorsHont = array();

				foreach ($actualDataHont as $datakey => $hontData) {
					foreach ($colorsArrResultComp as $colorkey => $colorData) {
						//echo $colorkey . " - " . $datakey . "<br>";
						if($colorkey==$datakey) {
							$colorsHont[$datakey] = $colorData;
						}
					}
				}

				$colors = implode(",", $colorsHont);
				$chart = $this->setChartSettings("Llei d'Hont ".utf8_encode($denominacio)." <br> comparativa ".$denominacio_comparativa, "", "column3d", $colors, $actualDataHont, "chart-4", "500", "300");


			}
			$smarty->assign('chart4',$chart);


		}
		// Fi Consulta resultats totals per partit
		$eleccions = $dades_eleccions->getEleccionsData();
		$smarty->assign('eleccions',$this->filtraArrayUtf8Encode($eleccions));
		$smarty->assign('id_eleccio',$_SESSION['id_eleccions']);
		$smarty->assign('title','Resultats totals '.utf8_encode($denominacio));
        $smarty->assign('subtitle','Dades informatives');
        $smarty->assign('logo_eleccions',$logo_eleccions);
		$smarty->display("eleccions/resultats_totals.tpl");
    }

    function eleccionsParticipacioComparativa ($params_values_get) {
        $_SESSION['id_eleccions'] = $params_values_get["eleccions"] ? $params_values_get["eleccions"] : $_SESSION['id_eleccions'];
        $_SESSION['id_eleccions'] = $_SESSION['id_eleccions'] ? $_SESSION['id_eleccions'] : $this->getUltimaEleccio();

        $smarty = new \webtortosa\Smarty_web();

        require_once (FOLDER_LANGUAGES."/".$this->languages($params_values_get["lang"])."_header.cfg");
        require_once (FOLDER_LANGUAGES."/eleccions/".$this->languages($params_values_get["lang"])."_eleccions.cfg");
        require_once (FOLDER_LANGUAGES."/".$this->languages($input_lang)."_footer.cfg");

        $smarty->assign("path_apartat","http://www2.tortosa.cat/templates");
        $smarty->assign("apartat","eleccions");
        $smarty->display("head.tpl");

        $dades_eleccions = new \webtortosa\eleccions();

        $params = array('IdEleccio'=>$_SESSION['id_eleccions']);

        $eleccions = $dades_eleccions->getEleccionsData($params);

        if(count($eleccions)) {
            if($params_values_get["eleccions_a_comparar"])
                $EleccioComparativa = $params_values_get["eleccions_a_comparar"];
            else
                $EleccioComparativa = $eleccions[0]->RefEleccioComparada;
            $Any = $eleccions[0]->Any;
            $denominacio = $eleccions[0]->Denominacio;
            $Hora1participacio = $eleccions[0]->Hora1Participacio;
            $Hora2participacio = $eleccions[0]->Hora2Participacio;
            $Hora3participacio = $eleccions[0]->Hora3Participacio;
            $logo_eleccions = $eleccions[0]->LogoEleccio;
            $params = array('IdEleccio'=>$EleccioComparativa);
            $eleccionsComparades = $dades_eleccions->getEleccionsData($params);
            if(count($eleccionsComparades)>0) {
                $DenominacioComparativa = $eleccionsComparades[0]->Denominacio;
            }
        }


        $params = array($_SESSION['id_eleccions'], $EleccioComparativa);
        $comparativa = $dades_eleccions->getComparativaParticipacioMeses($params);

        $actualData1Participacio = array();
        $actualData2Participacio = array();
        $actualData3Participacio = array();
        $Meses = array();
        $actualData1ParticipacioComparativa = array();
        $actualData2ParticipacioComparativa = array();
        $actualData3ParticipacioComparativa = array();
        $actualDataPercent = array();
        foreach ($comparativa as $key => $element) {
            $Meses[$key] = $element->IdMesa;
            if($element->RefEleccio == $_SESSION['id_eleccions']) {
                $nameSerie = $element->Any;
                $actualData1Participacio[$element->IdMesa] = $element->Vots1Participacio;
                $actualData2Participacio[$element->IdMesa] = $element->Vots2Participacio;
                $actualData3Participacio[$element->IdMesa] = $element->Vots3Participacio;
                //$actualDataPercent[$element->IdMesa] = number_format($element->VotsFinParticipacio / $element->CensVotants * 100,2);
            }

            if($element->RefEleccio == $EleccioComparativa) {
                $nameSerieComparativa = $element->Any;
                $actualData1ParticipacioComparativa[$element->IdMesa] = $element->Vots1Participacio;
                $actualData2ParticipacioComparativa[$element->IdMesa] = $element->Vots2Participacio;
                $actualData3ParticipacioComparativa[$element->IdMesa] = $element->Vots3Participacio;
                //$actualDataComparativaPercent[$element->IdMesa] = number_format($element->VotsFinParticipacio / $element->CensVotants * 100,2);
            }
        }
        $actualDataData1Participacio = array();
        $actualDataData2Participacio = array();
        $actualDataData3Participacio = array();
        //$actualDataDataPercent = array();
        foreach($Meses as $mesa) {
            $actualDataData1Participacio[$mesa][0] = $actualData1Participacio[$mesa];
            $actualDataData1Participacio[$mesa][1] = $actualData1ParticipacioComparativa[$mesa];
            $actualDataData2Participacio[$mesa][0] = $actualData2Participacio[$mesa];
            $actualDataData2Participacio[$mesa][1] = $actualData2ParticipacioComparativa[$mesa];
            $actualDataData3Participacio[$mesa][0] = $actualData3Participacio[$mesa];
            $actualDataData3Participacio[$mesa][1] = $actualData3ParticipacioComparativa[$mesa];
            //$actualDataDataPercent[$mesa][0] = $actualDataPercent[$mesa];
            //$actualDataDataPercent[$mesa][1] = $actualDataComparativaPercent[$mesa];
        }
        ksort($actualDataData1Participacio);
        ksort($actualDataData2Participacio);
        ksort($actualDataData3Participacio);
        //ksort($actualDataDataPercent);
        if((strpos($_SESSION['id_eleccions'], "JES"))||(strpos($_SESSION['id_eleccions'], "CAM"))||(strpos($_SESSION['id_eleccions'], "BIT"))||(strpos($_SESSION['id_eleccions'], "XBI"))||(strpos($_SESSION['id_eleccions'], "XCA"))||(strpos($_SESSION['id_eleccions'], "XJE"))) {
            $height = 400;
        }
        else {
            $height = 1300;
        }
        $chart = $this->setChartSettingsObjectSeries($actualDataData1Participacio, $nameSerie, $nameSerieComparativa, "MSBar2D", "", "", "400", $height, "");
        // Títols per al Chart1
        $smarty->assign('CaptionChart1',"Participació ".utf8_encode($denominacio));
        $smarty->assign('SubCaptionChart1',"Comparativa de participació ".$Hora1participacio." h");
        // Chart1
        $smarty->assign('chart1',$chart);

        $chart = $this->setChartSettingsObjectSeries($actualDataData2Participacio, $nameSerie, $nameSerieComparativa, "MSBar2D", "", "", "400", $height, "");
        // Títols per al Chart2
        $smarty->assign('CaptionChart2',"Participació ".utf8_encode($denominacio));
        $smarty->assign('SubCaptionChart2',"Comparativa de participació ".$Hora2participacio." h");
        // Chart2
        $smarty->assign('chart2',$chart);

        $chart = $this->setChartSettingsObjectSeries($actualDataData3Participacio, $nameSerie, $nameSerieComparativa, "MSBar2D", "", "", "400", $height, "");
        // Títols per al Chart3
        $smarty->assign('CaptionChart3',"Participació ".utf8_encode($denominacio));
        $smarty->assign('SubCaptionChart3',"Comparativa de participació ".$Hora3participacio." h");
        // Chart2
        $smarty->assign('chart3',$chart);

        $eleccions = $dades_eleccions->getEleccionsData();
        $smarty->assign('eleccions',$this->filtraArrayUtf8Encode($eleccions));
        $smarty->assign('id_eleccio',$_SESSION['id_eleccions']);
        $smarty->assign('title','Comparativa '.utf8_encode($denominacio) . " - " . utf8_encode($DenominacioComparativa));
        $smarty->assign('subtitle','Dades informatives');
        $smarty->assign('logo_eleccions',$logo_eleccions);
        $smarty->assign('idelecciocomparativa',$EleccioComparativa);
        $smarty->display("eleccions/participacio_comparativa.tpl");
    }

    function eleccionsParticipacioMeses ($params_values_get) {
     	$_SESSION['id_eleccions'] = $params_values_get["eleccions"] ? $params_values_get["eleccions"] : $_SESSION['id_eleccions'];
        $_SESSION['id_eleccions'] = $_SESSION['id_eleccions'] ? $_SESSION['id_eleccions'] : $this->getUltimaEleccio();

    	$smarty = new \webtortosa\Smarty_web();
		$smarty->assign("path_eleccions","http://www2.tortosa.cat/templates");
        $smarty->assign("apartat","eleccions");
		$smarty->assign("path_charts",URL_CHARTS);
		$smarty->display("head.tpl");

        require_once (FOLDER_LANGUAGES."/".$this->languages($params_values_get["lang"])."_header.cfg");
        require_once (FOLDER_LANGUAGES."/eleccions/".$this->languages($params_values_get["lang"])."_eleccions.cfg");
        require_once (FOLDER_LANGUAGES."/".$this->languages($input_lang)."_footer.cfg");

		$dades_eleccions = new \webtortosa\eleccions();

		$params = array('IdEleccio'=>$_SESSION['id_eleccions']);
		$dataEleccions = $dades_eleccions->getEleccionsData($params);
		if(count($dataEleccions)>0) {
			$denominacio = $dataEleccions[0]->Denominacio;
            $logo_eleccions = $dataEleccions[0]->LogoEleccio;
		}

		/* Consulta participació per meses */
		$params = array($_SESSION['id_eleccions']);
		$participacio = $dades_eleccions->getParticipacioMesesFront($params);

		if(count($participacio)>0) {

			$preparacio_participacio = array(
				 array(
				 	'Col·legi',
				 	'Núm. Mesa',
				 	'Cens Mesa',
				 	'Part.'.$participacio[0]->Hora1Participacio."h",
				 	'Part.'.$participacio[0]->Hora2Participacio."h",
				 	'Part.'.$participacio[0]->Hora3Participacio."h",
				 	'Part.'.$participacio[0]->Hora1Participacio."h % s/ Cens",
				 	'Part.'.$participacio[0]->Hora2Participacio."h % s/ Cens",
				 	'Part.'.$participacio[0]->Hora3Participacio."h % s/ Cens")
				);

			foreach ($participacio as $key => $element) {
				$percent1Participacio = number_format($element->Vots1Participacio/$element->CensVotants*100,2)." %";
				$percent2Participacio = number_format($element->Vots2Participacio/$element->CensVotants*100,2)." %";
				$percent3Participacio = number_format($element->Vots3Participacio/$element->CensVotants*100,2)." %";
				$fila = array(
					utf8_encode($element->NomColegi),
					$element->IdMesa,
					$element->CensVotants,
					$element->Vots1Participacio,
					$element->Vots2Participacio,
					$element->VotsFinParticipacio,
					$percent1Participacio,
					$percent2Participacio,
					$percent3Participacio);
				array_push($preparacio_participacio,$fila);
			}
			//echo var_dump($preparacio_participacio);
			$smarty->assign('taula1',$preparacio_participacio);

		}

		$eleccions = $dades_eleccions->getEleccionsData();
		$smarty->assign('title','Participació per mesa '.utf8_encode($denominacio));
        $smarty->assign('subtitle','Dades informatives');
        $smarty->assign('logo_eleccions',$logo_eleccions);
		$smarty->assign('eleccions',$this->filtraArrayUtf8Encode($eleccions));
		$smarty->assign('id_eleccio',$_SESSION['id_eleccions']);

		$smarty->display("eleccions/participacio_meses.tpl");
    }

    function eleccionsParticipacioSeccions ($params_values_get) {
     	$_SESSION['id_eleccions'] = $params_values_get["eleccions"] ? $params_values_get["eleccions"] : $_SESSION['id_eleccions'];
        $_SESSION['id_eleccions'] = $_SESSION['id_eleccions'] ? $_SESSION['id_eleccions'] : $this->getUltimaEleccio();

    	$smarty = new \webtortosa\Smarty_web();
		$smarty->assign("path_eleccions","http://www2.tortosa.cat/templates");
        $smarty->assign("apartat","eleccions");
		$smarty->assign("path_charts",URL_CHARTS);
		$smarty->display("head.tpl");

        require_once (FOLDER_LANGUAGES."/".$this->languages($params_values_get["lang"])."_header.cfg");
        require_once (FOLDER_LANGUAGES."/eleccions/".$this->languages($params_values_get["lang"])."_eleccions.cfg");
        require_once (FOLDER_LANGUAGES."/".$this->languages($input_lang)."_footer.cfg");

		$dades_eleccions = new \webtortosa\eleccions();

		$params = array('IdEleccio'=>$_SESSION['id_eleccions']);
		$dataEleccions = $dades_eleccions->getEleccionsData($params);
		if(count($dataEleccions)>0) {
			$denominacio = $dataEleccions[0]->Denominacio;
            $logo_eleccions = $dataEleccions[0]->LogoEleccio;
		}

		/* Consulta participació per seccions */
		$params = array($_SESSION['id_eleccions']);
		$participacio = $dades_eleccions->getParticipacioSeccions($params);
		if(count($participacio)>0) {
			$preparacio_participacio = array(
				 array(
				 	'Districte',
				 	'Secció',
				 	'Cens',
				 	'Part.'.$participacio[0]->Hora1Participacio."h",
				 	'Part.'.$participacio[0]->Hora2Participacio."h",
				 	'Part.'.$participacio[0]->Hora3Participacio."h",
				 	'Part.'.$participacio[0]->Hora1Participacio."h % s/ Cens",
				 	'Part.'.$participacio[0]->Hora2Participacio."h % s/ Cens",
				 	'Part.'.$participacio[0]->Hora3Participacio."h % s/ Cens"
				 	)
				);
			foreach ($participacio as $key => $element) {
				$fila = array(
					$element->Districte,
					$element->Seccio,
					$element->CensVotants,
					$element->Vots1Participacio,
					$element->Vots2Participacio,
					$element->Vots3Participacio,
					number_format($element->Vots1Participacio / $element->CensVotants * 100,2). " %",
					number_format($element->Vots2Participacio / $element->CensVotants * 100,2). " %",
					number_format($element->Vots3Participacio / $element->CensVotants * 100,2). " %"
				);
				array_push($preparacio_participacio,$fila);
			}
			//echo var_dump($preparacio_participacio);
			$smarty->assign('taula1',$preparacio_participacio);
		}
		$eleccions = $dades_eleccions->getEleccionsData();
		$smarty->assign('title','Participació per secció '.utf8_encode($denominacio));
        $smarty->assign('subtitle','Dades informatives');
        $smarty->assign('logo_eleccions',$logo_eleccions);
        $smarty->assign('eleccions',$this->filtraArrayUtf8Encode($eleccions));
		$smarty->assign('id_eleccio',$_SESSION['id_eleccions']);

		$smarty->display("eleccions/participacio_seccions.tpl");
    }

    function eleccionsParticipacioDistrictes ($params_values_get) {
    	$_SESSION['id_eleccions'] = $params_values_get["eleccions"] ? $params_values_get["eleccions"] : $_SESSION['id_eleccions'];
        $_SESSION['id_eleccions'] = $_SESSION['id_eleccions'] ? $_SESSION['id_eleccions'] : $this->getUltimaEleccio();

    	$smarty = new \webtortosa\Smarty_web();
		$smarty->assign("path_eleccions","http://www2.tortosa.cat/templates");
        $smarty->assign("apartat","eleccions");
		$smarty->assign("path_charts",URL_CHARTS);
		$smarty->display("head.tpl");

        require_once (FOLDER_LANGUAGES."/".$this->languages($params_values_get["lang"])."_header.cfg");
        require_once (FOLDER_LANGUAGES."/eleccions/".$this->languages($params_values_get["lang"])."_eleccions.cfg");
        require_once (FOLDER_LANGUAGES."/".$this->languages($input_lang)."_footer.cfg");

		$dades_eleccions = new \webtortosa\eleccions();


		$params = array('IdEleccio'=>$_SESSION['id_eleccions']);
		$dataEleccions = $dades_eleccions->getEleccionsData($params);
		if(count($dataEleccions)>0) {
			$denominacio = $dataEleccions[0]->Denominacio;
            $logo_eleccions = $dataEleccions[0]->LogoEleccio;
		}

		/* Consulta participació per districtes */
		$params = array($_SESSION['id_eleccions']);
		$participacio = $dades_eleccions->getParticipacioDistrictes($params);
		if(count($participacio)>0) {
			//Inserció del gràfic
			/* Gràfic columnes per a participació per districtes */
			$actualData = array();
			$actualDataPercent = array();
			foreach ($participacio as $key => $element) {
				$actualData['Dist '.$element->Districte] = $element->VotsFinParticipacio;
				$actualDataPercent['Dist '.$element->Districte] = number_format($element->VotsFinParticipacio / $element->CensVotants*100, 2);
				//echo $element->IdMesa. " - " .$element->Any. " - " .$element->NomColegi." -> " . $element->VotsFinParticipacio . "<br>";
			}

		    $chart = $this->setChartSettings("Participació ".utf8_encode($denominacio), "Participació per districtes", "column2d", "", $actualData, "chart-1", "500", "300");
		    $smarty->assign('chart1',$chart);

		    $chart = $this->setChartSettings("Participació ".utf8_encode($denominacio), "Participació per districtes en %", "column2d", "", $actualDataPercent, "chart-2", "500", "300", " %");
		    $smarty->assign('chart2',$chart);
		    /* Fi Gràfic columnes per a participació per districtes */


			foreach ($participacio as $key => $element) {
				$preparacio_participacio = array(
				 array('Districte '.$element->Districte)
				);

				$fila = array('Meses', $element->NumMeses);
				array_push($preparacio_participacio,$fila);
				$fila = array('Cens', $this->passa_milers($element->CensVotants));
				array_push($preparacio_participacio,$fila);
				$fila = array('Vots Emesos', $element->Hora1Participacio."h", $this->passa_milers($element->Vots1Participacio), number_format($element->Vots1Participacio/$element->CensVotants*100, 2). " % s/ Cens");
				array_push($preparacio_participacio,$fila);
				$fila = array('Vots Emesos', $element->Hora2Participacio."h", $this->passa_milers($element->Vots2Participacio), number_format($element->Vots2Participacio/$element->CensVotants*100, 2). " % s/ Cens");
				array_push($preparacio_participacio,$fila);
				$fila = array('Vots Emesos', $element->Hora3Participacio."h", $this->passa_milers($element->Vots3Participacio), number_format($element->Vots3Participacio/$element->CensVotants*100, 2). " % s/ Cens");
				array_push($preparacio_participacio,$fila);

				$smarty->assign('taula'.(integer)$element->Districte,$preparacio_participacio);
			}
		}
		$eleccions = $dades_eleccions->getEleccionsData();
		$smarty->assign('title','Participació per districte '.utf8_encode($denominacio));
        $smarty->assign('subtitle','Dades informatives');
        $smarty->assign('logo_eleccions',$logo_eleccions);
		$smarty->assign('eleccions',$this->filtraArrayUtf8Encode($eleccions));
		$smarty->assign('id_eleccio',$_SESSION['id_eleccions']);
		$smarty->display("eleccions/participacio_districtes.tpl");
    }

    function eleccionsParticipacioTotals ($params_values_get) {
        $_SESSION['id_eleccions'] = $params_values_get["eleccions"] ? $params_values_get["eleccions"] : $_SESSION['id_eleccions'];
        $_SESSION['id_eleccions'] = $_SESSION['id_eleccions'] ? $_SESSION['id_eleccions'] : $this->getUltimaEleccio();

        $smarty = new \webtortosa\Smarty_web();

        require_once (FOLDER_LANGUAGES."/".$this->languages($params_values_get["lang"])."_header.cfg");
        require_once (FOLDER_LANGUAGES."/eleccions/".$this->languages($params_values_get["lang"])."_eleccions.cfg");
        require_once (FOLDER_LANGUAGES."/".$this->languages($input_lang)."_footer.cfg");
        $smarty->assign("path_apartat","http://www2.tortosa.cat/templates");
        $smarty->assign("apartat","eleccions");
        $smarty->display("head.tpl");

        $dades_eleccions = new \webtortosa\eleccions();

        $params = array('RefEleccio'=>$_SESSION['id_eleccions']);


        /* Consulta participació per hores */
        $participacio = $dades_eleccions->getParticipacio($params);



        if(count($participacio)>0) {
            $preparacio_participacio = array(
                array('Hora', 'Votants', 'Cens', '% s/Cens', 'Número Meses')
            );
            $participacio_percent = number_format($participacio[0]->Totals1participacio/$participacio[0]->CensVotants*100,2) . " %";
            $fila = array($participacio[0]->Hora1Participacio, $this->passa_milers($participacio[0]->Totals1participacio), $this->passa_milers($participacio[0]->CensVotants), $participacio_percent, $participacio[0]->NumMeses);
            array_push($preparacio_participacio,$fila);
            $participacio_percent = number_format($participacio[0]->Totals2participacio/$participacio[0]->CensVotants*100, 2) . " %";
            $fila = array($participacio[0]->Hora2Participacio, $this->passa_milers($participacio[0]->Totals2participacio), $this->passa_milers($participacio[0]->CensVotants), $participacio_percent, $participacio[0]->NumMeses);
            array_push($preparacio_participacio,$fila);
            $participacio_percent = number_format($participacio[0]->Totals3participacio/$participacio[0]->CensVotants*100, 2) . " %";
            $fila = array($participacio[0]->Hora3Participacio, $this->passa_milers($participacio[0]->Totals3participacio), $this->passa_milers($participacio[0]->CensVotants), $participacio_percent, $participacio[0]->NumMeses);
            array_push($preparacio_participacio,$fila);
            $NumMeses = $participacio[0]->NumMeses;
            $smarty->assign('taula1',$preparacio_participacio);
        }

        /* Fi Consulta participació per hores */

        /* Consulta vots totals */

        $params = array('IdEleccio'=>$_SESSION['id_eleccions']);
        $vots = $dades_eleccions->getEleccionsData($params);
        if(count($vots)>0) {
            $vots_valids_chart = $vots[0]->VotsValids;
            $abstencio_chart = $vots[0]->CensVotants - $vots[0]->VotsValids;
            $denominacio = $vots[0]->Denominacio;
            $logo_eleccions = $vots[0]->LogoEleccio;
            $EleccioComparativa = $vots[0]->RefEleccioComparada;

            $actualData = array(
                "Vots" => $vots_valids_chart,
                "Abstenció" => $abstencio_chart
            );
            $chart = $this->setChartSettingsObject($actualData, "Pie3D", "", "", "300", "300");
            // Títols per al Chart1
            $smarty->assign('CaptionChart1',"Participació ".utf8_encode($denominacio));
            $smarty->assign('SubCaptionChart1',"");
            // Chart1
            $smarty->assign('chart1',$chart);
        }

        $params = array($_SESSION['id_eleccions'], $EleccioComparativa);
        $comparativa = $dades_eleccions->getComparativaParticipacioMeses($params);
        $actualData = array();
        $Meses = array();
        $actualDataComparativa = array();
        $actualDataPercent = array();
        foreach ($comparativa as $key => $element) {
            $Meses[$key] = $element->IdMesa;
            if($element->RefEleccio == $_SESSION['id_eleccions']) {
                $nameSerie = $element->Any;
                $actualData[$element->IdMesa] = $element->VotsFinParticipacio;
                $actualDataPercent[$element->IdMesa] = number_format($element->VotsFinParticipacio / $element->CensVotants * 100,2);
            }

            if($element->RefEleccio == $EleccioComparativa) {
                $nameSerieComparativa = $element->Any;
                $actualDataComparativa[$element->IdMesa] = $element->VotsFinParticipacio;
                $actualDataComparativaPercent[$element->IdMesa] = number_format($element->VotsFinParticipacio / $element->CensVotants * 100,2);
            }
        }
        $actualDataData = array();
        $actualDataDataPercent = array();
        foreach($Meses as $mesa) {
            $actualDataData[$mesa][0] = $actualData[$mesa];
            $actualDataData[$mesa][1] = $actualDataComparativa[$mesa];
            $actualDataDataPercent[$mesa][0] = $actualDataPercent[$mesa];
            $actualDataDataPercent[$mesa][1] = $actualDataComparativaPercent[$mesa];
        }
        ksort($actualDataData);
        ksort($actualDataDataPercent);
        if((strpos($_SESSION['id_eleccions'], "JES"))||(strpos($_SESSION['id_eleccions'], "CAM"))||(strpos($_SESSION['id_eleccions'], "BIT"))||(strpos($_SESSION['id_eleccions'], "XBI"))||(strpos($_SESSION['id_eleccions'], "XCA"))||(strpos($_SESSION['id_eleccions'], "XJE"))) {
            $height = 400;
        }
        else {
            $height = 1300;
        }
        $chart = $this->setChartSettingsObjectSeries($actualDataData, $nameSerie, $nameSerieComparativa, "MSBar2D", "", "", "400", $height, "");
        // Títols per al Chart2
        $smarty->assign('CaptionChart2',"Participació ".utf8_encode($denominacio));
        $smarty->assign('SubCaptionChart2',"Comparativa de participació");
        // Chart2
        $smarty->assign('chart2',$chart);

        $chart = $this->setChartSettingsObjectSeries($actualDataDataPercent, $nameSerie, $nameSerieComparativa, "MSBar2D", "", "", "400", $height, " %");
        // Títols per al Chart3
        $smarty->assign('CaptionChart3',"Participació ".utf8_encode($denominacio). " en %");
        $smarty->assign('SubCaptionChart3',"Comparativa de participació");
        // Chart3
        $smarty->assign('chart3',$chart);


        /* Fi Consulta vots totals */
        $eleccions = $dades_eleccions->getEleccionsData();
        $smarty->assign('title','Participació '.utf8_encode($denominacio));
        $smarty->assign('subtitle','Dades informatives');
        $smarty->assign('logo_eleccions',$logo_eleccions);
        $smarty->assign('eleccions',$this->filtraArrayUtf8Encode($eleccions));
        $smarty->assign('id_eleccio',$_SESSION['id_eleccions']);
        $smarty->display("eleccions/participacio_totals2.tpl");
    }

    function Hont($resultats, $regidors, $VotsTotals, $VotsNuls) {
    	$HontArr = array();
    	$siglesHont = array();
    	$dataHont = array();
    	$resultHont = array();
    	foreach ($resultats as $key => $resultat) {
    		if(round($resultat/($VotsTotals - $VotsNuls)*100,2)>=5) {
    			for($i=1;$i<=$regidors;$i++) {
		    		array_push($HontArr,array($key, $resultat/$i));
		    	}
    		}
    	}

    	//Recorro per a fer l'ordre
    	foreach ($HontArr as $key => $hont) {
    		$aux[$key] = $hont[1];
    	}
    	array_multisort($aux, SORT_DESC, $HontArr);
    	foreach ($HontArr as $key => $hont) {
    		if($key<$regidors) {
    			$siglesHont[$hont[0]] = $hont[1];
				$dataHont[$key] = array($hont[0], $hont[1]);
    		}
    	}
    	$keyArr = array_keys($siglesHont);
    	$keyArr = array_unique($keyArr);

    	foreach ($siglesHont as $key => $siglaHont) {
    		$resultHont[$key] = 0;
    		foreach ($dataHont as $dataintem) {
    			if($key==$dataintem[0]) {
    				$resultHont[$key] = $resultHont[$key] + 1;
    			}
    		}
    	}
    	return $resultHont;
    }

    function getUltimaEleccio() {
        $dades_eleccions = new \webtortosa\eleccions();
        $items = $dades_eleccions->getLastEleccio();
        if(count($items)>0)
            return $items[0]->IdEleccio;
        else
            return false;
    }

    function setChartSettingsObjectSeries($dataData = NULL, $nameCategory1 = NULL,  $nameCategory2 = NULL, $type="MSBar2D", $caption, $subcaption, $width, $height, $sufix="" ) {
        require_once ($_SERVER['DOCUMENT_ROOT']."/chart/Includes/FusionCharts_Gen.php");
        # Create column 3d chart object
        $FC = new \FusionCharts($type, $width, $height);

        # Set Relative Path of swf file.
        $FC->setSwfPath("/chart/Charts/");

        # Store Chart attributes in a variable
        $strParam="caption=".$caption.";subcaption=".$subcaption.";xAxisName=Meses;yAxisName=Anys;numberPrefix=  ;numberSuffix=".$sufix.";decimalPrecision=0;formatNumberScale=0;valuePadding=5";

        #  Set Chart attributes
        $FC->setChartParams($strParam);

        # Add category names
        foreach($dataData as $key=>$itemCategory) {
            $FC->addCategory($key);
        }

        if(count($dataData)>0) {
            # Create a new dataset
            $FC->addDataset($nameCategory1,"color=#1EC436");
            # Add chart values for the above dataset
            foreach($dataData as $key=>$itemSerie) {
                $FC->addChartData($itemSerie[0]);
            }
        }

        if(count($dataData)>0) {
            # Create a new dataset
            $FC->addDataset($nameCategory2, "color=#DD4D22");
            # Add chart values for the above dataset
            foreach($dataData as $key=>$itemSerie) {
                $FC->addChartData($itemSerie[1]);
            }
        }

        # Render  Chart
        return $FC->renderChart(false, false);
    }

    function setChartSettingsObject($dataData = NULL, $type="MSBar2D", $caption, $subcaption, $width, $height ) {
        require_once ($_SERVER['DOCUMENT_ROOT']."/chart/Includes/FusionCharts_Gen.php");
        # Create column 3d chart object
        $FC = new \FusionCharts($type,$width,$height);

        # Set Relative Path of swf file.
        $FC->setSwfPath("/chart/Charts/");

        # Store Chart attributes in a variable
        $strParam="caption=".$caption.";subcaption=".$subcaption.";xAxisName=Meses;yAxisName=Anys;numberPrefix=;decimalPrecision=0;paletteColors=#1EC436,#DD4D22,#289AF7,#D9E639,#EF3030;formatNumberScale=0; thousandSeparator=.; showNames=1; animation=0";

        #  Set Chart attributes
        $FC->setChartParams($strParam);

        # Add category names
        foreach($dataData as $key=>$itemCategory) {
            $FC->addCategory($key);
        }

        if(count($dataData)>0) {
            # Add chart values for the above dataset
            foreach($dataData as $key=>$item) {
                $FC->addChartData($item,"name=".$key);
            }
        }

        # Render  Chart
        return $FC->renderChart(false, false);
    }

    function setChartSettings($title = "", $subtitle = "", $type = "", $colours = "#1EC436,#DD4D22,#289AF7,#D9E639,#EF3030", $actualData = NULL, $container = "", $width, $height, $sufix) {
    	$arrData = array(
	        "chart" => array(
	            "caption" => $title,
	            "subCaption" => $subtitle,
	            "paletteColors" => $colours,
	            "bgColor" => "#ffffff",
	            "showBorder" => "0",
	            "use3DLighting" => "0",
	            "showShadow" => "0",
	            "enableSmartLabels" => "0",
	            "startingAngle" => "0",
	            "showPercentValues" => "0",
	            "showPercentInTooltip" => "0",
	            "formatNumber" => "1",
	            "decimals" => "2",
	            "captionFontSize" => "14",
	            "subcaptionFontSize" => "14",
	            "subcaptionFontBold" => "0",
	            "toolTipColor" => "#000000",
	            "toolTipBorderThickness" => "0",
	            "toolTipBgColor" => "#ffffff",
	            "toolTipBgAlpha" => "80",
	            "toolTipBorderRadius" => "2",
	            "toolTipPadding" => "5",
	            "showHoverEffect" => "1",
	            "showLegend" => "1",
	            "legendBgColor" => "#ffffff",
	            "legendBorderAlpha" => "0",
	            "legendShadow" => "0",
	            "legendItemFontSize" => "10",
	            "legendItemFontColor" => "#666666",
	            "useDataPlotColorForLabels" => "1",
	            "labelFontSize" => "8",
	            "labelFontColor" => "#000000",
	            "formatNumberScale" =>"0",
	            "thousandSeparator" => ".",
	            "numberSuffix" => $sufix
	        )
	    );

		// Now we need to convert it to FusionCharts consumable form.
	    // Our data will be an array. So, let's declare it.
	    $arrData['data'] = array();

	    // Loop over actual data and insert it in data array.
	    foreach ($actualData as $key => $value) {
	        array_push($arrData['data'], array(
	                'label' => utf8_encode($key),
	                'value' => utf8_encode($value)
	            )
	        );
	    }

	    // JSON Encode the arrData
	    $jsonEncodedData = json_encode($arrData);

	    // Create pieChart object
	    $pieChart = new \webtortosa\FusionCharts($type, $container."_chart" , $width, $height, $container, "json", $jsonEncodedData);
	    // Render the chart
	    return $pieChart->render();
    }

    /******************************************
    ********************************************
    ********************************************
      FI Funcions per a l'apartat de ELECCIONS
    ********************************************
    ********************************************************************************************************************
    ********************************************/


    /******************************************
     ********************************************
     ********************************************
        Funcions Memòria Democràtica
     ********************************************
     ********************************************
     ********************************************/

    function getMemoriaData($params_values_get, $params_values_post) {
        $smarty = new \webtortosa\Smarty_web();
        require_once (FOLDER_LANGUAGES."/".$this->languages($params_values_get["lang"])."_memoria-democratica.cfg");
        $smarty->assign("path_apartat","http://www2.tortosa.cat/templates");
        $smarty->assign("apartat","memoria-democratica");
        $smarty->display("head_web_antiga2.tpl");
        $md = new \webtortosa\memoria_democratica_database();
        $params = array();
        $categories = $this->filtraArrayUtf8Encode($md->getDataCategories($params));
        $categories = $md->getDataCategories($params);
        $smarty->assign("categories",$categories);
        $smarty->display("memoria-democratica/principal.tpl");
    }

    function getListMemoriaData($params_values_get, $params_values_post) {
        $smarty = new \webtortosa\Smarty_web();
        require_once (FOLDER_LANGUAGES."/".$this->languages($params_values_get["lang"])."_memoria-democratica.cfg");
        $smarty->assign("path_apartat","http://www2.tortosa.cat/templates");
        $smarty->assign("apartat","memoria-democratica");
        $smarty->display("head_web_antiga2.tpl");
        $md = new \webtortosa\memoria_democratica_database();

        $params = array();

        if($params_values_post) {
            $_SESSION['params_values_post'] = $params_values_post;
        }
        else {
            $params_values_post = $_SESSION['params_values_post'];
        }
        $params['NOM'] = $this->treure_accents($params_values_post['nom']);
        $params['CATEGORIA'] = $this->treure_accents($params_values_post['categoria']);

        //$items = $this->filtraArrayUtf8Encode($md->getDataMemorial($params));
        $items = $md->getDataMemorial($params);
        // Setup Pagination vars
        $total = count($items);
        $page = $params_values_get['page'];
        $start = $page ? ($page - 1) * ITEMS_LIMIT : 0;

        if($page==0)
            $page = 1;
        $prev = $page - 1; //previous page is current page - 1
        $next = $page + 1; //next page is current page + 1
        $lastpage = ceil($total/ITEMS_LIMIT); //lastpage.

        $items = $md->getDataMemorial($params, $start, ITEMS_LIMIT);
        $params = array();
        $categories = $md->getDataCategories($params);

        $smarty->assign("lastpage",$lastpage);
        $smarty->assign("page",$page);
        $smarty->assign("items",$items);
        $smarty->assign("categories",$categories);
        $smarty->display("memoria-democratica/list.tpl");
    }

    function getFileItem($params_values_get, $params_values_post) {
        $smarty = new \webtortosa\Smarty_web();
        require_once (FOLDER_LANGUAGES."/".$this->languages($params_values_get["lang"])."_memoria-democratica.cfg");
        $smarty->assign("path_apartat","http://www2.tortosa.cat/templates");
        $smarty->assign("apartat","memoria-democratica");
        $smarty->display("head_web_antiga2.tpl");
        $md = new \webtortosa\memoria_democratica_database();
        $params = array();
        $params['ID'] = $params_values_get['id'];
        //$items = $this->filtraArrayUtf8Encode($md->getDataMemorial($params));
        $items = $md->getDataMemorial($params);
        if(count($items)>0) {
            $smarty->assign("item",$items[0]);
        }
        $params = array();
        //$categories = $this->filtraArrayUtf8Encode($md->getDataCategories($params));
        $categories = $md->getDataCategories($params);
        $smarty->assign("categories",$categories);
        $smarty->assign("page",$params_values_get['page']);
        $smarty->display("memoria-democratica/item.tpl");

    }

    /*******************************************
     ********************************************
     ********************************************
            Fi Funcions Memòria Democràtica
     ********************************************
     *******************************************************************************************************************
     ********************************************/


    /******************************************
     ********************************************
     ********************************************
            Funcions Consulta
     ********************************************
     ********************************************
     ********************************************/

    function consultaCiutadania() {
        $smarty = new \webtortosa\Smarty_web();
        require_once (FOLDER_LANGUAGES."/consulta-ciutadania/".$this->languages($params_values_get["lang"])."_consulta-ciutadania.cfg");

        $smarty->assign("path_apartat","http://www2.tortosa.cat/templates");
        $smarty->assign("apartat","consulta-ciutadania");
        $smarty->assign("path_templates","../");
        $smarty->display("head_web_antiga2.tpl");

        require_once (FOLDER_LIBS . '/recaptchalib.php');
        $publickey = "6Lfdwh4TAAAAAKajPw-1Rj8msRd8YVZx14Vwova-";
        $smarty->assign("recaptcha",recaptcha_get_html($publickey));
        $smarty->display("consulta-ciutadania/principal.tpl");
    }

    function searchConsultaCiutadania($params_values_post) {
        require_once (FOLDER_MODEL."/consulta-ciutadania.class.php");
        $cconsulta = new \webtortosa\consulta_ciutadania();
        $file_logs = FOLDER_LOGS . '/consulta-ciutadania.log';
        $text_log = date('d/m/Y H:i:s');
        if($params_values_post["TDOCUMENT"]!="") {
            switch ($params_values_post["TDOCUMENT"]) {
                case "1":
                    $text_log .= ' --> Consulta: '.$params_values_post["DNI_NUMBER"].$params_values_post["DNI_LETTER"];
                    $items = $cconsulta->getElementDNI_test(strip_tags($params_values_post["DNI_NUMBER"]), strip_tags($params_values_post["DNI_LETTER"]));
                    break;
                case "2":
                    $text_log .= ' --> Consulta: '.$params_values_post["OPTION2_PASSPORT_NUMBER"];
                    $items = $cconsulta->getElementPassaport_test(strip_tags($params_values_post["OPTION2_PASSPORT_NUMBER"]));
                    break;
                case "3":
                    $text_log .= ' --> Consulta: '.$params_values_post["TR_LETTER_1"].$params_values_post["TR_NUMBER"].$params_values_post["TR_LETTER_2"];
                    $items = $cconsulta->getElementTarjetaResidencia_test(strip_tags($params_values_post["TR_LETTER_1"]), strip_tags($params_values_post["TR_NUMBER"]), strip_tags($params_values_post["TR_LETTER_2"]));
                    break;
                default;
                    break;
            }
        }


        if(count($items)>0){
            echo $items[0]->LOCAL_CONSULTA;
            $text_log .= ' - Resposta: '.$items[0]->LOCAL_CONSULTA;
        }
        else {
            echo "error";
            $text_log .= ' - Resposta: no trobat';
        }
        $this->escribir_log($file_logs,$text_log);
    }

    /******************************************
     ********************************************
     ********************************************
            Fi Funcions Consulta
     ********************************************
     ********************************************
     ********************************************/


    /******************************************
    ********************************************
    ********************************************
                 Funcions COMUNES
    ********************************************
    ********************************************
    ********************************************/

    function passa_milers($input) {
    	return number_format($input, 0, ".", ".");
    }

    function filtraArrayUtf8Encode($text_input) {
    	foreach ($text_input as $key => &$item_text) {
            foreach($item_text as &$field)
            $field = utf8_encode($field);
		}
		return $text_input;
    }

    function languages($input_lang="") {
        if(isset($input_lang)){
            $lang = $input_lang;

            //registra sesion
            $_SESSION['lang'] = $lang;
            //define cookie
            //setcookie('lang', $lang, time() + (3600 * 24 * 30));

            //busca en variables cookie y session
        /*
        }else if(isset($_SESSION['lang'])){
            $lang = $_SESSION['lang'];
        }else if(isset($_COOKIE['lang'])){
            $lang = $_COOKIE['lang'];
        */
        }else{
            return ('ca');
        }
        return $lang;
    }

    function treure_accents ($input=null) {
        if($input) {
            $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
            $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
            $input = utf8_decode($input);
            $input = strtr($input, utf8_decode($originales), $modificadas);
            $input = strtolower($input);
            return utf8_encode($input);
        }
    }

    function escribir_log($archivo, $contenido){

        $output = date('d/m/Y G:i:s').':::: '.$contenido.PHP_EOL;
        file_put_contents($archivo, $output, FILE_APPEND);
    }

    /*******************************************
    ********************************************
    ********************************************
               Fi Funcions COMUNES
    ********************************************
    ********************************************
    ********************************************/
}