<?php
/**
 * Created by Ajuntament de Tortosa.
 * User: Mario Juan
 * Date: 19/01/14
 * Time: 23:18
 * To change this template use File | Settings | File Templates.
 */

namespace webtortosa;

session_start();
require_once (FOLDER_CONTROLLER."/functions_controller.php");
require_once (FOLDER_LIBS."/smarty/Smarty_web.class.php");
require_once (FOLDER_CHARTS."/fusioncharts.php");
require_once (FOLDER_MODEL."/db.class.php");
require_once (FOLDER_MODEL."/eleccions.class.php");
require_once (FOLDER_MODEL."/memoria-democratica.class.php");
require_once (FOLDER_MODEL."/informacio-publica.class.php");
require_once (FOLDER_MODEL."/ordenances.class.php");
require_once (FOLDER_MODEL."/banners/banner.class.php");
require_once (FOLDER_MODEL."/telefons/telefon.class.php");
require_once (FOLDER_MODEL."/plens.class.php");
require_once (FOLDER_MODEL."/juntes-govern.class.php");
require_once (FOLDER_MODEL."/GPDocuments.class.php");
require_once (FOLDER_MODEL."/presparticipa.class.php");


class web_controller {

    /******************************************
     ********************************************
     ********************************************
        Funcions per a WEB
     ********************************************
     ********************************************
     ********************************************/


    function home ($input_lang, $test) {
        require_once (FOLDER_MODEL."/headline.class.php");
        require_once (FOLDER_MODEL."/banners/banner.class.php");
        require_once (FOLDER_MODEL."/tematica.class.php");
        $headline = new \webtortosa\headline();
        $tematicadb = new \webtortosa\tematica();
        $bannerdb = new \webtortosa\banner();

        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        $smarty->assign('url_ca','/');
        $smarty->assign('url_es','/es/');
        $smarty->assign('url_en','/en/');

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_home.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $bannerslst_home = $bannerdb->getBannersDataFrontEnd(null, null, 1);
        $smarty->assign('bannerslst_home', $bannerslst_home);

        $bannerslst_camp = $bannerdb->getBannersDataFrontEnd(null, null, 2);
        $smarty->assign('bannerslst_camp', $bannerslst_camp);

        $smarty->assign('LABEL_HDL', $this->headline($input_lang));
        //variable per saber quina pàgina se mostra i carregar les llibreries que només fan falta a home
        $smarty->assign('apartat','home');

        $smarty->assign('test',$test);

        $smarty->display("index.tpl");

    }

    function page_404 ($input_lang) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('url_ca','/404/');

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/404/".$lang."_404.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");
        $smarty->display("404/404.tpl");
    }

    /*
     * Indicadors de transparècia
     */
    function transparencia($input_lang) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        $smarty->assign('url_ca','/indicadors-transparencia/index.php');
        $smarty->assign('url_es','/indicadors-transparencia/index.php?lang=es');

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");
        require_once (FOLDER_LANGUAGES."/indicadors-transparencia/".$lang."_transparencia.cfg");

        $smarty->display("indicadors-transparencia/transparencia.tpl");
    }

    function indicadors_transparencia($input_lang) {
        require_once (FOLDER_MODEL."/indicadors-transparencia/indicador.class.php");

        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $dades_indicadors = new \webtortosa\indicador();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");
        require_once (FOLDER_LANGUAGES."/indicadors-transparencia/".$lang."_indicadors.cfg");

        $grups      = $dades_indicadors->getIndicadorGrup(null, null, null, null, null, 1, 0);
        $subgrups   = $dades_indicadors->getIndicadorSubGrup(null, null, null, null, null, 1, 0);
        $indicadors = $dades_indicadors->getIndicador(null, null, null, null, null, 1, 0, "ID", "ASC");
        $indicadors_links = $dades_indicadors->getIndicadorLink(null, $lang);

        $smarty->assign('grups',$grups);
        $smarty->assign('subgrups',$subgrups);
        //var_dump($indicadors);
        $smarty->assign('indicadors',$indicadors);
        $smarty->assign('indicadors_links',$indicadors_links);

        $smarty->display("indicadors-transparencia/indicadors.tpl");
    }

    function agermana ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/agermana/".$lang."_agermana.cfg");
        require_once (FOLDER_LANGUAGES."/agermana/".$lang."_agermana".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        switch($n) {
            case 1:
                $smarty->assign('url_ca','/agermana/index.php');
                $smarty->assign('url_es','/agermana/index.php?lang=es');
                $smarty->assign('url_en','/agermana/index.php?lang=en');
                $smarty->assign('url_fr','/agermana/index.php?lang=fr');

                $smarty->display("agermana/agermana_presentacio.tpl");
                break;
            case 2:
                $smarty->assign('url_ca','/agermana/agavinyo.php');
                $smarty->assign('url_es','/agermana/agavinyo.php?lang=es');
                $smarty->assign('url_en','/agermana/agavinyo.php?lang=en');
                $smarty->assign('url_fr','/agermana/agavinyo.php?lang=fr');

                $smarty->display("agermana/agermana_fitxa.tpl");
                break;
            case 3:
                $smarty->assign('url_ca','/agermana/agalcanyis.php');
                $smarty->assign('url_es','/agermana/agalcanyis.php?lang=es');
                $smarty->assign('url_en','/agermana/agalcanyis.php?lang=en');
                $smarty->assign('url_fr','/agermana/agalcanyis.php?lang=fr');

                $smarty->display("agermana/agermana_fitxa.tpl");
                break;
            case 4:
                $smarty->assign('url_ca','/agermana/agvercelli.php');
                $smarty->assign('url_es','/agermana/agvercelli.php?lang=es');
                $smarty->assign('url_en','/agermana/agvercelli.php?lang=en');
                $smarty->assign('url_fr','/agermana/agvercelli.php?lang=fr');

                $smarty->display("agermana/agermana_fitxa.tpl");
                break;
            case 5:
                $smarty->assign('url_ca','/agermana/aglepuy.php');
                $smarty->assign('url_es','/agermana/aglepuy.php?lang=es');
                $smarty->assign('url_en','/agermana/aglepuy.php?lang=en');
                $smarty->assign('url_fr','/agermana/aglepuy.php?lang=fr');

                $smarty->display("agermana/agermana_fitxa.tpl");
                break;
            case 6:
                $smarty->assign('url_ca','/agermana/agtartous.php');
                $smarty->assign('url_es','/agermana/agtartous.php?lang=es');
                $smarty->assign('url_en','/agermana/agtartous.php?lang=en');
                $smarty->assign('url_fr','/agermana/agtartous.php?lang=fr');

                $smarty->display("agermana/agermana_fitxa.tpl");
                break;
            case 7:
                $smarty->assign('url_ca','/agermana/agjocs.php');
                $smarty->assign('url_es','/agermana/agjocs.php?lang=es');
                $smarty->assign('url_en','/agermana/agjocs.php?lang=en');
                $smarty->assign('url_fr','/agermana/agjocs.php?lang=fr');

                $smarty->display("agermana/agermana_fitxa.tpl");
                break;
        }
    }

    /***function transparencia ($input_lang) {
        $smarty = new \webtortosa\Smarty_web();

        require_once (FOLDER_LANGUAGES."/".$this->languages($input_lang)."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$this->languages($input_lang)."_transparencia.cfg");
        require_once (FOLDER_LANGUAGES."/".$this->languages($input_lang)."_footer.cfg");

        $smarty->display("transparencia.tpl");
    }*///

    

    function arxiuadm ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/arxiuadm/".$lang."_arxiuadm.cfg");
        require_once (FOLDER_LANGUAGES."/arxiuadm/".$lang."_arxiuadm".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("arxiuadm.tpl");
    }

    function alcalde ($input_lang, $n, $params = null, $params_get = null) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $source = $params_get['source'];

        require_once (FOLDER_LIBS . '/recaptchalib.php');
        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");
        require_once (FOLDER_LANGUAGES."/alcalde/".$lang."_alcalde.cfg");
        require_once (FOLDER_LANGUAGES."/alcalde/".$lang."_alcalde".$n.".cfg");
        switch ($n) {
            case 5:
                if($params) {
                    //var_dump($params);
                    $utils = new \webtortosa\functions_controller();
                    if(($params['nom']=="Luciu Constantin")||($utils->get_client_ip()=="62.32.213.201")||($params['assumpte']=="Buena Suerte")||(strpos($params['comentari'],'ANONYMOUS ')>0)) {
                        $content = '
                        Nom: Luciu Constantin
                        IP: '.$utils->get_client_ip().'
                        ';
                        $utils->sendEmail_phpMailer('Bloqueig contacte escriu a l\'Alcalde', $content, EMAIL_AVERIA );
                    }
                    else {
                        require_once(FOLDER_MODEL . "/alcalde/alcalde.class.php");
                        $alcaldedb = new \webtortosa\alcalde();


                        $alcaldedb->AddEscriuAlcalde($params['nom'], $params['email'], $params['poblacio'], $params['comarca'], $params['pais'], $params['assumpte'], $params['comentari'], $utils->get_client_ip());

                        $content = '
                            <html>
                            <head>
                            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                            </head>
                            <body>
                            <div>Un usuari sol·licita informació des de l\'apartat web d\'escriu a l\'Alcaldessa</div>
                            <p>Nom: ' . $params['nom'] . '</p>
                            <p>Email: ' . $params['email'] . '</p>
                            <p>Població: ' . $params['poblacio'] . '</p>
                            <p>Comarca / Província: ' . $params['comarca'] . '</p>
                            <p>Pais: ' . $params['pais'] . '</p>
                            <p>Assumpte: ' . $params['assumpte'] . '</p>
                            <p>Comentari: ' . $params['comentari'] . '</p>
                            </body>
                            </html>
                        ';

                        if ($utils->sendEmail_phpMailer('Contacte escriu a l\'Alcaldessa', $content, EMAIL_ESCRIU_ALCALDE, EMAIL_ESCRIU_ALCALDE_CC, EMAIL_AVERIA))
                            //if($utils->sendEmail('Contacte escriu a l\'Alcalde', $content, EMAIL_AVERIA))
                            $smarty->assign('request', 1);
                        else
                            $smarty->assign('request', 0);
                    }
                }
                $smarty->assign('url_ca','/alcalde/alccontac.php?source='.$source);
                $smarty->assign('url_es','/alcalde/alccontac.php?lang=es&source='.$source);
                $smarty->assign('url_en','/alcalde/alccontac.php?lang=en&source='.$source);
                //$smarty->assign('url_fr','/alcalde/alccontac.php?lang=fr&source='.$source);
                $smarty->assign('ntpl', $n);
                $smarty->assign('ntitol', 5);
                $publickey = "6Lfdwh4TAAAAAKajPw-1Rj8msRd8YVZx14Vwova-";
                $smarty->assign("recaptcha",recaptcha_get_html($publickey));
                $smarty->display("alcalde/escriu_alcalde.tpl");
                break;
            case 6:
                //Competències
                $smarty->assign('url_ca','/alcalde/competencies.php?source='.$source);
                $smarty->assign('url_es','/alcalde/competencies.php?lang=es&source='.$source);
                $smarty->assign('url_en','/alcalde/competencies.php?lang=en&source='.$source);
                //$smarty->assign('url_fr','/alcalde/competencies.php?lang=fr');
                $smarty->display("alcalde/alcalde_competencies.tpl");
                break;
            case 7:
                //Agenda
                $smarty->assign('url_ca','/alcalde/agenda.php?source='.$source);
                $smarty->assign('url_es','/alcalde/agenda.php?lang=es&source='.$source);
                $smarty->assign('url_en','/alcalde/agenda.php?lang=en&source='.$source);
                //$smarty->assign('url_fr','/alcalde/agenda.php?lang=fr&source='.$source);
                $smarty->display("alcalde/alcalde_agenda.tpl");
                break;
            case 1:
                $smarty->assign('url_ca','/alcalde/?source='.$source);
                $smarty->assign('url_es','/alcalde/index.php?lang=es&source='.$source);
                $smarty->assign('url_en','/alcalde/index.php?lang=en&source='.$source);
                //$smarty->assign('url_fr','/alcalde/index.php?lang=fr&source='.$source);
                $smarty->display("alcalde/alcalde.tpl");
                break;
            case 2:
                $smarty->assign('url_ca','/alcalde/alcbio.php?source='.$source);
                $smarty->assign('url_es','/alcalde/alcbio.php?lang=es&source='.$source);
                $smarty->assign('url_en','/alcalde/alcbio.php?lang=en&source='.$source);
                //$smarty->assign('url_fr','/alcalde/alcbio.php?lang=fr&source='.$source);
                $smarty->display("alcalde/alcalde.tpl");
                break;
            case 3:
                $smarty->assign('url_ca','/alcalde/alccurric.php?source='.$source);
                $smarty->assign('url_es','/alcalde/alccurric.php?lang=es&source='.$source);
                $smarty->assign('url_en','/alcalde/alccurric.php?lang=en&source='.$source);
                //$smarty->assign('url_fr','/alcalde/alccurric.php?lang=fr&source='.$source);
                $smarty->display("alcalde/alcalde.tpl");
                break;
            case 4:
                $smarty->assign('url_ca','/alcalde/alcantec.php?source='.$source);
                $smarty->assign('url_es','/alcalde/alcantec.php?lang=es&source='.$source);
                $smarty->assign('url_en','/alcalde/alcantec.php?lang=en&source='.$source);
                //$smarty->assign('url_fr','/alcalde/alcantec.php?lang=fr&source='.$source);
                $smarty->display("alcalde/alcalde.tpl");
                break;

            default:
                $smarty->assign('url_ca','/alcalde/?source='.$source);
                $smarty->assign('url_es','/alcalde/index.php?lang=es&source='.$source);
                $smarty->assign('url_en','/alcalde/index.php?lang=en&source='.$source);
                //$smarty->assign('url_fr','/alcalde/index.php?lang=fr&source='.$source);
                $smarty->display("alcalde/alcalde.tpl");
                break;
        }
    }

    function pam ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('ntpl', $n);

        $smarty->assign('url_ca','/pam/');
        $smarty->assign('url_es','/pam/index.php?lang=es');

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/pam/".$lang."_pam.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

       
        $smarty->display("pam/pam.tpl");
    }

    function transpa ($input_lang) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/transparencia/".$lang."_transpa.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("transparencia/transpa.tpl");
    }

    function infeco ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/infeco/".$lang."_infeco.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        switch($n) {
            case 0:
                $smarty->assign('url_ca','http://www2.tortosa.cat/infeco/index.php');
                $smarty->assign('url_es','http://www2.tortosa.cat/infeco/index.php?lang=es');
                $smarty->display("infeco/infeco.tpl");
                break;
            case 1:
                $smarty->assign('url_ca','http://www2.tortosa.cat/infeco/infeco_ep.php');
                $smarty->assign('url_es','http://www2.tortosa.cat/infeco/infeco_ep.php?lang=es');
                $smarty->display("infeco/infeco_ep.tpl");
                break;

        }
    }

    function infeco_expre ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/infeco/".$lang."_infeco.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        if ($n==117){
            $smarty->assign('image', "/images/infeco/tira_ep17.jpg");
            $smarty->assign('url_ca','http://www2.tortosa.cat/infeco/infeco_ep17.php');
            $smarty->assign('url_es','http://www2.tortosa.cat/infeco/infeco_ep17.php?lang=es');
            $smarty->display("infeco/ep17_infeco.tpl"); 
        }
        elseif ($n==118){
            $smarty->assign('image', "/images/infeco/tira_ep18.jpg");
            $smarty->assign('url_ca','http://www2.tortosa.cat/infeco/2018/infeco_ep18.php');
            $smarty->assign('url_es','http://www2.tortosa.cat/infeco/2018/infeco_ep18.php?lang=es');
            $smarty->display("infeco/2018/ep18_infeco.tpl"); 
        }
    }

    function infeco_tancament ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/infeco/".$lang."_infecotanca.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        if ($n==117){
            $smarty->assign('image', "/images/infeco/tira_ep17.jpg");
            $smarty->assign('url_ca','http://www2.tortosa.cat/infeco/infeco_tanca17.php');
            $smarty->assign('url_es','http://www2.tortosa.cat/infeco/infeco_tanca17.php?lang=es');
            $smarty->display("infeco/ep17_tancament.tpl"); 
        }
        elseif ($n==118){
            $smarty->assign('image', "/images/infeco/tira_ep18.jpg");
            $smarty->assign('url_ca','http://www2.tortosa.cat/infeco/2018/infeco_tanca18.php');
            $smarty->assign('url_es','http://www2.tortosa.cat/infeco/2018/infeco_tanca18.php?lang=es');
            $smarty->display("infeco/2018/ep18_tancament.tpl"); 
        }
    }

    function infeco_pressupost ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/infeco/".$lang."_infeco_presgen.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        if ($n==2017){
            $smarty->assign('image', "/images/infeco/tira_ep17.jpg");
            $smarty->assign('url_ca','http://www2.tortosa.cat/infeco/infeco_presup17.php');
            $smarty->assign('url_es','http://www2.tortosa.cat/infeco/infeco_presup17.php?lang=es');
            $smarty->display("infeco/ep17_pressupost.tpl"); 
        }
        elseif ($n==2018){
            $smarty->assign('image', "/images/infeco/tira_ep18.jpg");
            $smarty->assign('url_ca','http://www2.tortosa.cat/infeco/2018/infeco_presup18.php');
            $smarty->assign('url_es','http://www2.tortosa.cat/infeco/2018/infeco_presup18.php?lang=es');
            $smarty->display("infeco/2018/ep18_pressupost.tpl");
        }
    }

    function infeco_execajt ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/infeco/".$lang."_infeco_execajt.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        if ($n==2017){
            $smarty->assign('image', "/images/infeco/tira_ep17.jpg");
            $smarty->assign('url_ca','http://www2.tortosa.cat/infeco/infeco_execajt17.php');
            $smarty->assign('url_es','http://www2.tortosa.cat/infeco/infeco_execajt17.php?lang=es');
            $smarty->display("infeco/ep17_execAjt.tpl"); 
        }
        else if ($n==2018){
            $smarty->assign('image', "/images/infeco/tira_ep18.jpg");
            $smarty->assign('url_ca','http://www2.tortosa.cat/infeco/2018/infeco_execajt18.php');
            $smarty->assign('url_es','http://www2.tortosa.cat/infeco/2018/infeco_execajt18.php?lang=es');
            $smarty->display("infeco/2018/ep18_execAjt.tpl"); 
        }
    }

    function infeco_execajtmodpre ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);



        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/infeco/".$lang."_infeco_execajtmodpre.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        if ($n==2017){
            $smarty->assign('image', "/images/infeco/tira_ep17.jpg");
            $smarty->display("infeco/ep17_execAjtmodpre.tpl"); 
        }
        else if ($n==2018){

            $smarty->assign('image', "/images/infeco/tira_ep18.jpg");
            $smarty->display("infeco/2018/ep18_execAjtmodpre.tpl"); 
        }
    }

    function infeco_execgrup ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/infeco/".$lang."_infeco_execgrup.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        if ($n==2017){
            $smarty->assign('image', "/images/infeco/tira_ep17.jpg");
            $smarty->assign('url_ca','http://www2.tortosa.cat/infeco/infeco_execgrup17.php');
            $smarty->assign('url_es','http://www2.tortosa.cat/infeco/infeco_execgrup17.php?lang=es');
            $smarty->display("infeco/ep17_execGrup.tpl"); 
        }
        else if ($n==2018){
            $smarty->assign('image', "/images/infeco/tira_ep18.jpg");
            $smarty->assign('url_ca','http://www2.tortosa.cat/infeco/2018/infeco_execgrup18.php');
            $smarty->assign('url_es','http://www2.tortosa.cat/infeco/2018/infeco_execgrup18.php?lang=es');
            $smarty->display("infeco/2018/ep18_execGrup.tpl");
        }
    }

    function infeco_evolucio ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
		$smarty->assign('url_ca','http://www2.tortosa.cat/infeco/infeco_evolucio.php');
		$smarty->assign('url_es','http://www2.tortosa.cat/infeco/infeco_evolucio.php?lang=es');
		
        require_once (FOLDER_LANGUAGES."/".$this->languages($input_lang)."_header.cfg");
        require_once (FOLDER_LANGUAGES."/infeco/".$this->languages($input_lang)."_infeco_evolucio.cfg");
        require_once (FOLDER_LANGUAGES."/".$this->languages($input_lang)."_footer.cfg");

        $smarty->assign('image', "/images/infeco/tira_ev.jpg");
        if ($n==2017){
            $smarty->display("infeco/evolucio.tpl"); 
        }
        else{
            $smarty->assign('item', $n);
            $smarty->assign('graph', "/images/infeco/graph/evolucio".$n.".jpg");
            $smarty->display("infeco/evolucioN.tpl"); 
        }
    }

     function infeco_iplanif ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        require_once (FOLDER_LANGUAGES."/".$this->languages($input_lang)."_header.cfg");
        require_once (FOLDER_LANGUAGES."/infeco/".$this->languages($input_lang)."_infeco_iplanif.cfg");
        require_once (FOLDER_LANGUAGES."/".$this->languages($input_lang)."_footer.cfg");

         $smarty->assign('lang',$lang);
         $smarty->assign('url_ca','http://www2.tortosa.cat/infeco/infeco_iplanif.php');
         $smarty->assign('url_es','http://www2.tortosa.cat/infeco/infeco_iplanif.php?lang=es');

        $smarty->assign('image', "/images/infeco/tira_ip.jpg");
        if ($n==2017){
            $smarty->display("infeco/iplanif.tpl"); 
        }
    }

    function estadistica_habitants($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('url_ca','/ciutat/estadistica-habitants/');

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/estadistica-habitants/".$lang."_estadistica-habitants.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");
        $smarty->display("estadistica-habitants/estadistica-habitants.tpl");
    }
    /*
    function estadistica_habitants_din($input_tipus, $input_lang) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('url_ca','/ciutat/estadistica-habitants/');		
		
        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/estadistica-habitants/".$lang."_estadistica-habitants.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");
		
        $ehabitants= new \webtortosa\estadistiques-habitants();

        $elements = $ehabitants->getEstadistiquesHabitants($input_tipus);

        // Setup Pagination vars
        $total = count($elements);
        $page = $input_page;
        $start = $page ? ($page - 1) * ITEMS_LIMIT : 0;

        if($page==0)
            $page = 1;
        $prev = $page - 1; //previous page is current page - 1
        $next = $page + 1; //next page is current page + 1
        $lastpage = ceil($total/ITEMS_LIMIT); //lastpage.

        $elements = $ehabitants->getEstadistiquesHabitants($input_tipus, $start, ITEMS_LIMIT);

        $smarty->assign("lastpage",$lastpage);
        $smarty->assign("page",$page);
        $smarty->assign('elements', $elements);
        $smarty->assign('tipus', $input_tipus);

        //echo ("TOTAL->".$total);				
		
		$smarty->assign('tipus', $input_tipus);	
        $smarty->display("estadistica-habitants/estadistica-habitants-din.tpl");
    }
    */
	
    function turisme_oci ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        $smarty->assign('url_ca','/turisme-oci/');
        $smarty->assign('url_es','/turisme-oci/index.php?lang=es');
        $smarty->assign('url_en','/turisme-oci/index.php?lang=en');


        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/turisme-oci/".$lang."_turisme-oci".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("turisme-oci/turisme-oci.tpl");

    }

    function festes ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        switch($n) {
            case 1:
                $smarty->assign('url_ca','/festes/');
                $smarty->assign('url_es','/festes/index.php?lang=es');
                $smarty->assign('url_en','/festes/index.php?lang=en');
                //$smarty->assign('url_fr','/festes/index.php?lang=fr');
                break;
            case 2:
                $smarty->assign('url_ca','/festes/cinta_custom.php');
                $smarty->assign('url_es','/festes/cinta_custom.php?lang=es');
                $smarty->assign('url_en','/festes/cinta_custom.php?lang=en');
                //$smarty->assign('url_fr','/festes/cinta_custom.php?lang=fr');
                break;
            case 3:
                $smarty->assign('url_ca','/festes/pubilla-pubilletes.php');
                $smarty->assign('url_es','/festes/pubilla-pubilletes.php?lang=es');
                $smarty->assign('url_en','/festes/pubilla-pubilletes.php?lang=en');
                //$smarty->assign('url_fr','/festes/pubilla-pubilletes.php?lang=fr');
                break;
            case 4:
                $smarty->assign('url_ca','/festes/cinta.php');
                $smarty->assign('url_es','/festes/cinta.php?lang=es');
                $smarty->assign('url_en','/festes/cinta.php?lang=en');
                //$smarty->assign('url_fr','/festes/cinta.php?lang=fr');
                break;
            case 5:
                $smarty->assign('url_ca','/festes/ofrena.php');
                $smarty->assign('url_es','/festes/ofrena.php?lang=es');
                $smarty->assign('url_en','/festes/ofrena.php?lang=en');
                //$smarty->assign('url_fr','/festes/ofrena.php?lang=fr');
                break;
            case 6:
                $smarty->assign('url_ca','/festes/contacta.php');
                $smarty->assign('url_es','/festes/contacta.php?lang=es');
                $smarty->assign('url_en','/festes/contacta.php?lang=en');
                //$smarty->assign('url_fr','/festes/contacta.php?lang=fr');
                break;
            case 7:
                $smarty->assign('url_ca','/festes/altres.php');
                $smarty->assign('url_es','/festes/altres.php?lang=es');
                $smarty->assign('url_en','/festes/altres.php?lang=en');
                //$smarty->assign('url_fr','/festes/altres.php?lang=fr');
                break;
            case 8:
                $smarty->assign('url_ca','/festes/fpobles.php');
                $smarty->assign('url_es','/festes/fpobles.php?lang=es');
                $smarty->assign('url_en','/festes/fpobles.php?lang=en');
                //$smarty->assign('url_fr','/festes/fpobles.php?lang=fr');
                break;
            case 9:
                $smarty->assign('url_ca','/festes/anteriors.php');
                $smarty->assign('url_es','/festes/anteriors.php?lang=es');
                $smarty->assign('url_en','/festes/anteriors.php?lang=en');
                //$smarty->assign('url_fr','/festes/anteriors.php?lang=fr');
                break;
            case 10:
                $smarty->assign('url_ca','/festes/vestint-pubilla.php');
                $smarty->assign('url_es','/festes/vestint-pubilla.php?lang=es');
                $smarty->assign('url_en','/festes/vestint-pubilla.php?lang=en');
                //$smarty->assign('url_fr','/festes/pubilla-pubilletes.php?lang=fr');
                break;
            case 11:
                $smarty->assign('url_ca','/festes/pasdoble.php');
                $smarty->assign('url_es','/festes/pasdoble.php?lang=es');
                $smarty->assign('url_en','/festes/pasdoble.php?lang=en');
                //$smarty->assign('url_fr','/festes/pubilla-pubilletes.php?lang=fr');
                break;
        }

        /*
        $smarty->assign('url_es','/festes/index.php?lang=es');
        $smarty->assign('url_en','/festes/index.php?lang=en');
        */
        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        if ($n==1 or $n==6 or $n==7 or $n==8){
            require_once (FOLDER_LANGUAGES."/festes/".$lang."_festes1.cfg");
        }
        else{
            require_once (FOLDER_LANGUAGES."/festes/".$lang."_festes2.cfg");
        }
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");
        $smarty->assign('type',$n);
        $smarty->display("festes/festes.tpl");
    }

    function cinta17 ($input_lang,  $input_opcio) {
        $lang = $this->languages($input_lang);
        $opcio = $this->languages($input_opcio);

        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('opcio',$opcio);

        $smarty->assign('url_ca','/festes/cinta17/');
        $smarty->assign('url_es','/festes/cinta17/cinta17.php?lang=es');
        $smarty->assign('url_en','/festes/cinta17/cinta17.php?lang=en');

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/festes/cinta17/".$lang."_cinta17.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("festes/cinta17/cinta17.tpl");
    }

    function elementsfesta ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('item',$n);

        switch($n) {
            case 1:
                $smarty->assign('url_ca','/festes/elements/index.php');
                $smarty->assign('url_es','/festes/elements/index.php?lang=es');
                $smarty->assign('url_en','/festes/elements/index.php?lang=en');
                //$smarty->assign('url_fr','/festes/elements/index.php?lang=fr');
                break;
            case 2:
                $smarty->assign('url_ca','/festes/elements/cucafera.php');
                $smarty->assign('url_es','/festes/elements/cucafera.php?lang=es');
                $smarty->assign('url_en','/festes/elements/cucafera.php?lang=en');
                //$smarty->assign('url_fr','/festes/elements/cucafera.php?lang=fr');
                break;
            case 3:
                $smarty->assign('url_ca','/festes/elements/gegants.php');
                $smarty->assign('url_es','/festes/elements/gegants.php?lang=es');
                $smarty->assign('url_en','/festes/elements/gegants.php?lang=en');
                //$smarty->assign('url_fr','/festes/elements/gegants.php?lang=fr');
                break;
            case 4:
                $smarty->assign('url_ca','/festes/elements/aliga.php');
                $smarty->assign('url_es','/festes/elements/aliga.php?lang=es');
                $smarty->assign('url_en','/festes/elements/aliga.php?lang=en');
                //$smarty->assign('url_fr','/festes/elements/aliga.php?lang=fr');
                break;
        }

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/festes/elements/".$lang."_elements.cfg");
        require_once (FOLDER_LANGUAGES."/festes/elements/".$lang."_elements".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");
        
        $smarty->display("festes/elements/elements.tpl");

    }

     function ssanta ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        switch($n) {
            case 1:
                $smarty->assign('url_ca','/ssanta/index.php');
                $smarty->assign('url_es','/ssanta/index.php?lang=es');
                $smarty->assign('url_en','/ssanta/index.php?lang=en');
                //$smarty->assign('url_fr','/ssanta/index.php?lang=fr');
                break;
        }

        $smarty->assign('item',$n);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/ssanta/".$lang."_ssanta.cfg");
        require_once (FOLDER_LANGUAGES."/ssanta/".$lang."_ssanta".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");
        
        $smarty->display("ssanta/ssanta.tpl");

    }

    function ssantah ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('item',$n);

        switch($n) {
            case 1:
                $smarty->assign('url_ca','/ssanta/ssantah.php');
                $smarty->assign('url_es','/ssanta/ssantah.php?lang=es');
                $smarty->assign('url_en','/ssanta/ssantah.php?lang=en');
                //$smarty->assign('url_fr','/ssanta/ssantah.php?lang=fr');
                break;
            case 2:
                $smarty->assign('url_ca','/ssanta/misteris36.php');
                $smarty->assign('url_es','/ssanta/misteris36.php?lang=es');
                $smarty->assign('url_en','/ssanta/misteris36.php?lang=en');
                //$smarty->assign('url_fr','/ssanta/misteris36.php?lang=fr');
                break;
            case 3:
                $smarty->assign('url_ca','/ssanta/misteris.php');
                $smarty->assign('url_es','/ssanta/misteris.php?lang=es');
                $smarty->assign('url_en','/ssanta/misteris.php?lang=en');
                //$smarty->assign('url_fr','/ssanta/misteris.php?lang=fr');
                break;
            case 4:
                $smarty->assign('url_ca','/ssanta/germandats.php');
                $smarty->assign('url_es','/ssanta/germandats.php?lang=es');
                $smarty->assign('url_en','/ssanta/germandats.php?lang=en');
                //$smarty->assign('url_fr','/ssanta/germandats.php?lang=fr');
                break;
            case 5:
                $smarty->assign('url_ca','/ssanta/silenci.php');
                $smarty->assign('url_es','/ssanta/silenci.php?lang=es');
                $smarty->assign('url_en','/ssanta/silenci.php?lang=en');
                //$smarty->assign('url_fr','/ssanta/silenci.php?lang=fr');
                break;
            case 6:
                $smarty->assign('url_ca','/ssanta/enterra.php');
                $smarty->assign('url_es','/ssanta/enterra.php?lang=es');
                $smarty->assign('url_en','/ssanta/enterra.php?lang=en');
                //$smarty->assign('url_fr','/ssanta/enterra.php?lang=fr');
                break;
            case 7:
                $smarty->assign('url_ca','/ssanta/costums.php');
                $smarty->assign('url_es','/ssanta/costums.php?lang=es');
                $smarty->assign('url_en','/ssanta/costums.php?lang=en');
                //$smarty->assign('url_fr','/ssanta/costums.php?lang=fr');
                break;
            case 8:
                $smarty->assign('url_ca','/ssanta/altres.php');
                $smarty->assign('url_es','/ssanta/altres.php?lang=es');
                $smarty->assign('url_en','/ssanta/altres.php?lang=en');
                //$smarty->assign('url_fr','/ssanta/altres.php?lang=fr');
                break;
        }
        
        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/ssanta/".$lang."_ssantah.cfg");
        require_once (FOLDER_LANGUAGES."/ssanta/".$lang."_ssantah".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("ssanta/ssantah.tpl");
        
    }

    function centrei ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('item',$n);
        
        switch($n) {
            case 1:
                $smarty->assign('url_ca','/ssanta/centrei.php');
                $smarty->assign('url_es','/ssanta/centrei.php?lang=es');
                $smarty->assign('url_en','/ssanta/centrei.php?lang=en');
                //$smarty->assign('url_fr','/ssanta/centrei.php?lang=fr');
                break;
            case 2:
                $smarty->assign('url_ca','/ssanta/esglesia.php');
                $smarty->assign('url_es','/ssanta/esglesia.php?lang=es');
                $smarty->assign('url_en','/ssanta/esglesia.php?lang=en');
                //$smarty->assign('url_fr','/ssanta/esglesia.php?lang=fr');
                break;
            case 3:
                $smarty->assign('url_ca','/ssanta/passos.php');
                $smarty->assign('url_es','/ssanta/passos.php?lang=es');
                $smarty->assign('url_en','/ssanta/passos.php?lang=en');
                //$smarty->assign('url_fr','/ssanta/passos.php?lang=fr');
                break;
            case 4:
                $smarty->assign('url_ca','/ssanta/horari.php');
                $smarty->assign('url_es','/ssanta/horari.php?lang=es');
                $smarty->assign('url_en','/ssanta/horari.php?lang=en');
                //$smarty->assign('url_fr','/ssanta/horari.php?lang=fr');
                break;
            case 5:
                $smarty->assign('url_ca','/ssanta/contacte.php');
                $smarty->assign('url_es','/ssanta/contacte.php?lang=es');
                $smarty->assign('url_en','/ssanta/contacte.php?lang=en');
                //$smarty->assign('url_fr','/ssanta/contacte.php?lang=fr');
                break;
        }

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/ssanta/".$lang."_centrei.cfg");
        require_once (FOLDER_LANGUAGES."/ssanta/".$lang."_centrei".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("ssanta/centrei.tpl");
        
    }

    function transport ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('item',$n);

        switch($n) {
            case 1:
                $smarty->assign('url_ca','/transport/index.php?lang=ca');
                $smarty->assign('url_es','/transport/index.php?lang=es');
                $smarty->assign('url_en','/transport/index.php?lang=en');
                //$smarty->assign('url_fr','/ssanta/centrei.php?lang=fr');
                break;
            case 2:
                $smarty->assign('url_ca','/transport/com.php?lang=ca');
                $smarty->assign('url_es','/transport/com.php?lang=es');
                $smarty->assign('url_en','/transport/com.php?lang=en');
                //$smarty->assign('url_fr','/ssanta/esglesia.php?lang=fr');
                break;
            case 3:
                $smarty->assign('url_ca','/transport/bus.php?lang=ca');
                $smarty->assign('url_es','/transport/bus.php?lang=es');
                $smarty->assign('url_en','/transport/bus.php?lang=en');
                //$smarty->assign('url_fr','/ssanta/passos.php?lang=fr');
                break;
            case 4:
                $smarty->assign('url_ca','/transport/tren.php?lang=ca');
                $smarty->assign('url_es','/transport/tren.php?lang=es');
                $smarty->assign('url_en','/transport/tren.php?lang=en');
                //$smarty->assign('url_fr','/ssanta/horari.php?lang=fr');
                break;
            case 5:
                $smarty->assign('url_ca','/transport/aparca.php?lang=ca');
                $smarty->assign('url_es','/transport/aparca.php?lang=es');
                $smarty->assign('url_en','/transport/aparca.php?lang=en');
                //$smarty->assign('url_fr','/ssanta/contacte.php?lang=fr');
                break;
            case 6:
                $smarty->assign('url_ca','/transport/tbus.php?lang=ca');
                $smarty->assign('url_es','/transport/tbus.php?lang=es');
                $smarty->assign('url_en','/transport/tbus.php?lang=en');
                //$smarty->assign('url_fr','/ssanta/passos.php?lang=fr');
                break;
        }

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/transport/".$lang."_transport.cfg");
        require_once (FOLDER_LANGUAGES."/transport/".$lang."_transport".$n.".cfg");
        if ($n==2){
            require_once (FOLDER_LANGUAGES."/negocis/".$lang."_negocis_i1.cfg");
        }
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("transport/transport.tpl");

    }

    function tortosabus ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        $smarty->assign('url_ca','/transport/tbus.php');
        $smarty->assign('url_es','/transport/tbus.php?lang=es');

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/transport/".$lang."_tortosabus.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("transport/tortosabus.tpl");

    }

    function salut ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('url_ca','/ciutat/salut/');
        $smarty->assign('url_es','/es/ciudad/salud/');
        $smarty->assign('url_en','/salut/index.php?lang=en');


        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/salut/".$lang."_salut".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("salut/salut.tpl");

    }

    function cultura ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('url_ca','/ciutat/cultura/');
        $smarty->assign('url_es','/es/ciudad/cultura/');
        $smarty->assign('url_en','/en/city/cultura/');

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/cultura/".$lang."_cultura".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("cultura/cultura.tpl");

    }

    function defensorc ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();

        switch ($n) {
            case 1:
                $smarty->assign('url_ca','/defensorc/');
                $smarty->assign('url_es','/defensorc/index.php?lang=es');
                $smarty->assign('url_en','/defensorc/index.php?lang=en');
                break;
            case 2:
                $smarty->assign('url_ca','/defensorc/defcontacta.php');
                $smarty->assign('url_es','/defensorc/defcontacta.php?lang=es');
                $smarty->assign('url_en','/defensorc/defcontacta.php?lang=en');
                break;
        }
        //$smarty->assign('url_fr','/defensorc/index.php?lang=fr');
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/defensor/".$lang."_defensor".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("defensorc.tpl");
    }


    function subvencions ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('item',$n);
        
        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/subvencions/".$lang."_subvencions.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        switch ($n){
            case 1:
                $smarty->assign('url_ca','/subvencions/index.php');
                $smarty->assign('url_es','/subvencions/index.php?lang=es');
                break;
            case 2:
                $smarty->assign('url_ca','/subvencions/exercici17.php');
                $smarty->assign('url_es','/subvencions/exercici17.php?lang=es');
                break;
            case 3:
                $smarty->assign('url_ca','/subvencions/exercicisant.php');
                $smarty->assign('url_es','/subvencions/exercicisant.php?lang=es');
                break;
            case 4:
                $smarty->assign('url_ca','/subvencions/convenis.php');
                $smarty->assign('url_es','/subvencions/convenis.php?lang=es');
                break;
        }

        $smarty->display("subvencions/subvencions.tpl");
    }

    function exercicis ($input_lang, $n, $m) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('item',$n);
        $smarty->assign('item2',$m);
        
        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/subvencions/".$lang."_exercicis".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        switch ($n){
            case 1:
                require_once (FOLDER_LANGUAGES."/subvencions/".$lang."_exercicis".$n.$m.".cfg");
                $smarty->assign('url_ca','/subvencions/exercici17.php');
                $smarty->assign('url_es','/subvencions/exercici17.php?lang=es');
                switch ($m) {
                    case 1:
                        $smarty->assign('url_ca','/subvencions/exercici17.php');
                        $smarty->assign('url_es','/subvencions/exercici17.php?lang=es');
                        break;
                    case 2:
                        $smarty->assign('url_ca','/subvencions/infgeneral17.php');
                        $smarty->assign('url_es','/subvencions/infgeneral17.php?lang=es');
                        break;
                    case 3:
                        $smarty->assign('url_ca','/subvencions/convocatoria1.php');
                        $smarty->assign('url_es','/subvencions/convocatoria1.php?lang=es');
                        break;
                    case 4:
                        $smarty->assign('url_ca','/subvencions/convocatoria2.php');
                        $smarty->assign('url_es','/subvencions/convocatoria2.php?lang=es');
                        break;
                    case 5:
                        $smarty->assign('url_ca','/subvencions/subvencionsaavv.php');
                        $smarty->assign('url_es','/subvencions/subvencionsaavv.php?lang=es');
                        break;
                }
                break;
            case 2:
                $smarty->assign('url_ca','/subvencions/exercici17.php');
                $smarty->assign('url_es','/subvencions/exercici17.php?lang=es');
                switch ($m) {
                    case 1:
                        require_once (FOLDER_LANGUAGES."/subvencions/".$lang."_exercicis21.cfg");
                        $smarty->assign('url_ca', '/subvencions/exercici18.php');
                        $smarty->assign('url_es', '/subvencions/exercici18.php?lang=es');
                        break;
                    case 3:
                        require_once (FOLDER_LANGUAGES."/subvencions/".$lang."_exercicis13.cfg");
                        $smarty->assign('url_ca','/subvencions/convocatoria3.php');
                        $smarty->assign('url_es','/subvencions/convocatoria3.php?lang=es');
                        break;
                    case 4:
                        require_once (FOLDER_LANGUAGES."/subvencions/".$lang."_exercicis24.cfg");
                        $smarty->assign('url_ca','/subvencions/convocatoria4.php');
                        $smarty->assign('url_es','/subvencions/convocatoria4.php?lang=es');
                        break;
                }
                break;

        }

        $smarty->display("subvencions/exercicis.tpl");
        
    }

    function exercicisant ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        
        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/subvencions/".$lang."_exercicisant.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("subvencions/exercicisant.tpl");
    }

    /*
    / Paràmetre $n: apartat de menú de l'esquerra.
    */
    function pciutadana ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('item',$n);
        
        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/pciutadana/".$lang."_pciutadana.cfg");
        require_once (FOLDER_LANGUAGES."/pciutadana/".$lang."_pciutadana".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

         switch ($n){
            case 0:
                $smarty->assign('url_ca','/pciutadana/index.php');
                $smarty->assign('url_es','/pciutadana/index.php?lang=es');
                $smarty->display("pciutadana/presentacio.tpl");
                break;
            case 1:
                $smarty->assign('url_ca','/pciutadana/presentacio.php');
                $smarty->assign('url_es','/pciutadana/presentacio.php?lang=es');
                $smarty->display("pciutadana/pciutadana.tpl");
                break;
            case 2:
                $smarty->assign('url_ca','/pciutadana/audiencies.php');
                $smarty->assign('url_es','/pciutadana/audiencies.php?lang=es');
                $smarty->display("pciutadana/audiencies.tpl");
                break;
            case 3:
                $smarty->assign('url_ca','/pciutadana/consulta.php');
                $smarty->assign('url_es','/pciutadana/consulta.php?lang=es');
                $smarty->display("pciutadana/consulta.tpl");
                break;
            case 4:
                $smarty->assign('url_ca','/pciutadana/presparticipa.php');
                $smarty->assign('url_es','/pciutadana/presparticipa.php?lang=es');
                break;
            case 5:
                $smarty->assign('url_ca','/pciutadana/organspar.php');
                $smarty->assign('url_es','/pciutadana/organspar.php?lang=es');
                break;
            case 6:
                $smarty->assign('url_ca','/pciutadana/sistemes.php');
                $smarty->assign('url_es','/pciutadana/sistemes.php?lang=es');
                break;
        }
    }

    /*
    /   Consulta participació ciutadana monument del riu.
    */
    function consultamonument ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        
        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/pciutadana/".$lang."_pciutadana.cfg");
        require_once (FOLDER_LANGUAGES."/pciutadana/".$lang."_pciutadana".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

                $smarty->assign('url_ca','/2016/pciutadana/consultamonument.php');
                $smarty->assign('url_es','/2016/pciutadana/consultamonument.php?lang=es');
                $smarty->display("pciutadana/consultamonument.tpl");
    }


    function presparticipa ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('item',$n);
        
        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/pciutadana/".$lang."_presparticipa.cfg");
        require_once (FOLDER_LANGUAGES."/pciutadana/".$lang."_presparticipa".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        
     switch ($n){
            case 1:
                $smarty->assign('url_ca','/pciutadana/presparticipa.php');
                $smarty->assign('url_es','/pciutadana/presparticipa.php?lang=es');
                $smarty->display("pciutadana/presparticipa.tpl");
                break;
            case 2:
                $smarty->assign('url_ca','/pciutadana/prespar2017.php');
                $smarty->assign('url_es','/pciutadana/prespar2017.php?lang=es');
                 $smarty->display("pciutadana/prespar2017.tpl");
                break;
            case 3:
                $smarty->assign('url_ca','/pciutadana/index.php');
                $smarty->assign('url_es','/pciutadana/index.php?lang=es');
                break;
            
        }


        
    }

    function presparticipa2017 ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('item',$n);
        
        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/pciutadana/2017/".$lang."_presparticipa.cfg");
        require_once (FOLDER_LANGUAGES."/pciutadana/2017/".$lang."_presparticipa".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        if ($n<8){
            $smarty->assign('url_ca','/pciutadana/2017/prespar.php');
            $smarty->assign('url_es','/pciutadana/2017/prespar.php?lang=es');
        }
        else{
            $execucio = new \webtortosa\execucio();
            $elements = $execucio->getExecucioT();

            //  Tortosa
            $elements = $execucio->getExecucioT();
            $smarty->assign('elements', $elements);

            // Reguers
            $elementsR = $execucio->getExecucioR();
            $smarty->assign('elementsR', $elementsR);

            // Vinallop
            $elementsV = $execucio->getExecucioV();
            $smarty->assign('elementsV', $elementsV);
        }
        $smarty->display("pciutadana/2017/prespar.tpl");
    }

    function presparticipa2018 ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('item',$n);
        
        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/pciutadana/2018/".$lang."_presparticipa.cfg");
        require_once (FOLDER_LANGUAGES."/pciutadana/2018/".$lang."_presparticipa".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        if ($n!=8){
            $smarty->assign('url_ca','/pciutadana/2018/prespar.php');
            $smarty->assign('url_es','/pciutadana/2018/prespar.php?lang=es');
        }
        else{
            $execucio = new \webtortosa\execucio();
            $elements = $execucio->getExecucioT();

            //  Tortosa
            $elements = $execucio->getExecucioT();
            $smarty->assign('elements', $elements);

            // Reguers
            $elementsR = $execucio->getExecucioR();
            $smarty->assign('elementsR', $elementsR);

            // Vinallop
            $elementsV = $execucio->getExecucioV();
            $smarty->assign('elementsV', $elementsV);
        }
        $smarty->display("pciutadana/2018/prespar.tpl");
    }

    function organsparticipa ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('item',$n);
        
        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/pciutadana/".$lang."_organsparticipa.cfg");
        require_once (FOLDER_LANGUAGES."/pciutadana/".$lang."_organsparticipa".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        switch ($n){
            case 1:
                $smarty->assign('url_ca','/pciutadana/organspar.php');
                $smarty->assign('url_es','/pciutadana/organspar.php?lang=es');
                break;
            case 2:
                $smarty->assign('url_ca','/pciutadana/relacioorgans.php');
                $smarty->assign('url_es','/pciutadana/relacioorgans.php?lang=es');
                break;
            case 3:
                $smarty->assign('url_ca','/pciutadana/actesorgans.php');
                $smarty->assign('url_es','/pciutadana/actesorgans.php?lang=es');
                break;
            case 4:
                $smarty->assign('url_ca','/pciutadana/index.php');
                $smarty->assign('url_es','/pciutadana/index.php?lang=es');
                break;
        }


        $smarty->display("pciutadana/organsparticipa.tpl");
        
    }

    /*
    / Paràmetre $n: tipus informació o apartat a presentar.
    */
    function sistemesparticipa ($input_lang, $n, $params = null) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('item',$n);
        
        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/pciutadana/".$lang."_sistemesparticipa.cfg");
        require_once (FOLDER_LANGUAGES."/pciutadana/".$lang."_sistemesparticipa".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        switch ($n){
            case 1:
                $smarty->assign('url_ca','/pciutadana/sistemes.php');
                $smarty->assign('url_es','/pciutadana/sistemes.php?lang=es');
                break;
            case 2:
                $smarty->assign('url_ca','/pciutadana/digueu.php');
                $smarty->assign('url_es','/pciutadana/digueu.php?lang=es');
                break;
            case 3:
                $smarty->assign('url_ca','/pciutadana/aldia.php');
                $smarty->assign('url_es','/pciutadana/aldia.php?lang=es');
                break;
            case 4:
                $smarty->assign('url_ca','/pciutadana/index.php');
                $smarty->assign('url_es','/pciutadana/index.php?lang=es');
                break;
            case 5:
                if($params) {
                    require_once (FOLDER_MODEL."/ciutat/ciutat.class.php");
                    $ciutatdb = new \webtortosa\ciutat();
                    //var_dump($params);
                    $utils = new \webtortosa\functions_controller();

                    $params_query = array(
                        'NOM'                       => $params['nom_sollicitant'],
                        'PRIMER_COGNOM'             => $params['cognom1_sollicitant'],
                        'SEGON_COGNOM'              => $params['cognom2_sollicitant'],
                        'RAO_SOCIAL'                => $params['rao_social_sollicitant'],
                        'TIPUS_DOCUMENT'            => $params['tipus_document_sollicitant'],
                        'NUMERO_DOCUMENT'           => $params['numero_document_sollicitant'],
                        'TIPUS_VIA'                 => $params['tipus_via_sollicitant'],
                        'NOM_VIA'                   => $params['nom_via_sollicitant'],
                        'NUMERO'                    => $params['numero_sollicitant'],
                        'LLETRA'                    => $params['lletra_sollicitant'],
                        'KM'                        => $params['km_sollicitant'],
                        'BLOC'                      => $params['bloc_sollicitant'],
                        'ESCALA'                    => $params['escala_sollicitant'],
                        'PIS'                       => $params['pis_sollicitant'],
                        'PORTA'                     => $params['porta_sollicitant'],
                        'NUCLI_BARRI'               => $params['nucli_barri_sollicitant'],
                        'PROVINCIA'                 => $params['provincia_sollicitant'],
                        'MUNICIPI'                  => $params['municipi_sollicitant'],
                        'CODI_POSTAL'               => $params['codi_postal_sollicitant'],
                        'TELEFON_FIX'               => $params['telefon_fix_sollicitant'],
                        'TELEFON_MOBIL'             => $params['telefon_mobil_sollicitant'],
                        'EMAIL'                     => $params['email_sollicitant'],
                        'TIPUS_PETICIO'             => $params['tipus_peticio_sollicitant'],
                        'AFECTA_A'                  => $params['afecta_a_sollicitant'],
                        'DESCRIPCIO'                => $params['descripcio_sollicitant'],
                        'IP_CONNEXIO'               => $utils->get_client_ip()
                    );
                    $ciutatdb->AddQueixesSuggeriments($params_query);

                    //var_dump($params);
                    $content = '
                        <html>
                        <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                        </head>
                        <body>
                        <div>Enviament informació queixes i suggeriments</div>
                        <p>Nom: '.$params['nom_sollicitant'].'</p>
                        <p>Primer cognom: '.$params['cognom1_sollicitant'].'</p>
                        <p>Segon cognom: '.$params['cognom2_sollicitant'].'</p>
                        <p>Raó social: '.$params['rao_social_sollicitant'].'</p>
                        <p>Tipus de document: '.$params['tipus_document_sollicitant'].'</p>
                        <p>Número de document: '.$params['numero_document_sollicitant'].'</p>
                        <p>Tipus de via: '.$params['tipus_via_sollicitant'].'</p>
                        <p>Nom de la via: '.$params['nom_via_sollicitant'].'</p>
                        <p>Número: '.$params['numero_sollicitant'].'</p>
                        <p>Lletra: '.$params['lletra_sollicitant'].'</p>
                        <p>Km: '.$params['km_sollicitant'].'</p>
                        <p>Bloc: '.$params['bloc_sollicitant'].'</p>
                        <p>Escala: '.$params['escala_sollicitant'].'</p>
                        <p>Pis: '.$params['pis_sollicitant'].'</p>
                        <p>Porta: '.$params['porta_sollicitant'].'</p>
                        <p>Nucli o Barri: '.$params['nucli_barri_sollicitant'].'</p>
                        <p>Província: '.$params['provincia_sollicitant'].'</p>
                        <p>Municipi: '.$params['municipi_sollicitant'].'</p>
                        <p>Codi postal: '.$params['codi_postal_sollicitant'].'</p>
                        <p>Telèfon fix: '.$params['telefon_fix_sollicitant'].'</p>
                        <p>Telèfon mòbil: '.$params['telefon_mobil_sollicitant'].'</p>
                        <p>Email: '.$params['email_sollicitant'].'</p>
                        <p>Tipus de petició: '.$params['tipus_peticio_sollicitant'].'</p>
                        <p>Afecta a: '.$params['afecta_a_sollicitant'].'</p>
                        <p>Descripció: '.$params['descripcio_sollicitant'].'</p>
                        </body>
                        </html>
                    ';

                    if($utils->sendEmail_phpMailer('Contacte queixes i suggeriments', $content, EMAIL_QUEIXES_SUGGERIMENTS, $params['email_sollicitant'], EMAIL_AVERIA))
                    //if($utils->sendEmail_phpMailer('Contacte queixes i suggeriments', $content, EMAIL_AVERIA, $params['email_sollicitant']))
                        $smarty->assign('request', 1);
                    else
                        $smarty->assign('request', 0);
                    unset($params);
                }
                require_once (FOLDER_LIBS . '/recaptchalib.php');
                $smarty->assign('url_ca','/pciutadana/queixes_suggeriments.php');
                $smarty->assign('url_es','/pciutadana/queixes_suggeriments.php?lang=es');
                $publickey = "6Lfdwh4TAAAAAKajPw-1Rj8msRd8YVZx14Vwova-";
                $smarty->assign("recaptcha",recaptcha_get_html($publickey));
                break;    
        }
        $smarty->assign('apartat', $n);
        $smarty->display("pciutadana/sistemes.tpl");
    }

    function serequi ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('item',$n);
        
        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/serequi/".$lang."_serequi.cfg");
        require_once (FOLDER_LANGUAGES."/serequi/".$lang."_serequi".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("serequi/serequi.tpl");
    }

    function serveis ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('item',$n);
        
        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/serequi/".$lang."_serveis.cfg");
        require_once (FOLDER_LANGUAGES."/serequi/".$lang."_serveis".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("serequi/serveis.tpl");
    }

    function equipa ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('item',$n);
        
        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/serequi/".$lang."_equipa.cfg");
        require_once (FOLDER_LANGUAGES."/serequi/".$lang."_equipa".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("serequi/equipa.tpl");
    }

   function educacio ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('url_ca','/ciutat/educacio/');
        $smarty->assign('url_es','/ciutat/educacio?lang=es');
        $smarty->assign('url_en','/ciutat/educacio?lang=en');

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/educacio/".$lang."_educacio0.cfg");
        if ($n>0){
            require_once (FOLDER_LANGUAGES."/educacio/".$lang."_educacio".$n.".cfg");
        }
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->assign('item', $n);
        $smarty->display("educacio/educacio.tpl");

    }

    function entitats ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();

        switch ($n) {
            case 1:
                $smarty->assign('url_ca','/entitats/');
                $smarty->assign('url_es','/entitats/index.php?lang=es');
                
                break;
            case 2:
                $smarty->assign('url_ca','/entitats/contacta.php');
                $smarty->assign('url_es','/entitats/contacta.php?lang=es');
                
                break;
        }
        $smarty->assign('lang',$lang);
        $smarty->assign('item',$n);
        
        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/entitats/".$lang."_entitats".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");
        
        $smarty->display("entitats/entitats.tpl");
    }

    function perfilc ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        switch ($n){
        case 1:
            $smarty->assign('url_ca','/perfilc/index.php/');
            $smarty->assign('url_es','/perfilc/index.php?lang=es');
            break;
        case 2:
            $smarty->assign('url_ca','/perfilc/perfils.php/');
            $smarty->assign('url_es','/perfilc/perfils.php?lang=es');
            break;
        }
     
        $smarty->assign('item',$n);
        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/perfilc/".$lang."_perfilc.cfg");
        require_once (FOLDER_LANGUAGES."/perfilc/".$lang."_perfilc".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("perfilc/perfilc.tpl");
    }

    function laciutat ($input_lang) {
        $bannerdb = new \webtortosa\banner();
        $lang = $this->languages($input_lang);

        $bannerslst = $bannerdb->getBannersDataFrontEnd();

        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('url_ca','/ciutat/');
        $smarty->assign('url_es','/es/ciudad/');
        $smarty->assign('url_en','/en/city/');
        $smarty->assign('bannerslst', $bannerslst);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/ciutat/".$lang."_ciutat.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        //variable per saber quina pàgina se mostra i carregar les llibreries que només fan falta a ciutat
        $smarty->assign('apartat','ciutat');
        $smarty->display("ciutat/laciutat.tpl");

        
    }

        function ciutat_imatges ($input_lang, $n) {
            $lang = $this->languages($input_lang);
            $smarty = new \webtortosa\Smarty_web();
            $smarty->assign('lang',$lang);
            $smarty->assign('url_ca','/ciutat/imatges/');
            $smarty->assign('url_es','/es/ciudad/imagenes/');

            $smarty->assign('ntpl', $n);

            require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
            require_once (FOLDER_LANGUAGES."/ciutat/".$lang."_imatges.cfg");
            require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

            $smarty->display("ciutat/ciutat_imatges.tpl");
        }

        function ciutat_municipi ($input_lang) {
             $lang = $this->languages($input_lang);
             $smarty = new \webtortosa\Smarty_web();
             $smarty->assign('lang',$lang);
             $smarty->assign('url_ca','/ciutat/municipi/');
             $smarty->assign('url_es','/es/ciudad/municipio/');
             $smarty->assign('url_en','/ciutat/municipi.php?lang=en');
             //$smarty->assign('url_fr','/ciutat/municipi.php?lang=fr');

             require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
             require_once (FOLDER_LANGUAGES."/ciutat/".$lang."_municipi.cfg");
             require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

             $smarty->display("ciutat/ciutat_municipi.tpl");
        }

        function ciutat_historia ($input_lang) {
             $lang = $this->languages($input_lang);
             $smarty = new \webtortosa\Smarty_web();
             $smarty->assign('lang',$lang);
             $smarty->assign('url_ca','/ciutat/historia.php/index.php');
             $smarty->assign('url_es','/ciutat/historia.php?lang=es');
             $smarty->assign('url_en','/ciutat/historia.php?lang=en');
             //$smarty->assign('url_fr','/ciutat/historia.php?lang=fr');

             require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
             require_once (FOLDER_LANGUAGES."/ciutat/".$lang."_historia.cfg");
             require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

             $smarty->display("ciutat/ciutat_historia.tpl");
        }

        function gtributaria ($input_lang, $n) {
             $lang = $this->languages($input_lang);
             $smarty = new \webtortosa\Smarty_web();
             $smarty->assign('lang',$lang);
             $smarty->assign('url_ca','/gtributaria/index.php');
             $smarty->assign('url_es','/gtributaria/index.php?lang=es');
             $smarty->assign('item',$n);

             switch ($n){
                case 1:
                        $smarty->assign('url_ca','/gtributaria/index.php');
                        $smarty->assign('url_es','/gtributaria/index.php?lang=es');
                        break;
                case 3:
                        $smarty->assign('url_ca','/gtributaria/contacta.php');
                        $smarty->assign('url_es','/gtributaria/contacta.php?lang=es');
                        break;
                case 4:
                        $smarty->assign('url_ca','/gtributaria/normativa.php');
                        $smarty->assign('url_es','/gtributaria/normativa.php?lang=es');
                        break;
                case 5:
                        $smarty->assign('url_ca','/gtributaria/tributs.php');
                        $smarty->assign('url_es','/gtributaria/tributs.php?lang=es');
                        break;
                case 2:
                        $smarty->assign('url_ca','/gtributaria/pic.php');
                        $smarty->assign('url_es','/gtributaria/pic.php?lang=es');
                        break;
        }

             require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
             require_once (FOLDER_LANGUAGES."/gtributaria/".$lang."_gtributaria.cfg");
             require_once (FOLDER_LANGUAGES."/gtributaria/".$lang."_gtributaria".$n.".cfg");
             require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

             $smarty->display("gtributaria/gtributaria.tpl");
        }


   function pic ($input_lang) {
         $lang = $this->languages($input_lang);
         $smarty = new \webtortosa\Smarty_web();
         $smarty->assign('lang',$lang);

         require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
         require_once (FOLDER_LANGUAGES."/pic/".$lang."_pic.cfg");
         require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

         $smarty->display("pic/pic.tpl");
   }

    /*
     * Incidències a la via pública
     */
   function ciutat_ivp ($input_lang, $params_post) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('url_ca','/ciutat/incidencies_via_publica.php');
        $smarty->assign('url_es','/ciutat/incidencies_via_publica.php?lang=es');
        //$smarty->assign('url_en','/ciutat/territori.php?lang=en');
        //$smarty->assign('url_fr','/ciutat/territori.php?lang=fr');

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/ciutat/".$lang."_incidencies_via_publica.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");
        require_once (FOLDER_LIBS . '/recaptchalib.php');
        require_once (FOLDER_MODEL."/ciutat/ciutat.class.php");
        $ciutatdb = new \webtortosa\ciutat();

        if($params_post) {
            //var_dump($params);
            $utils = new \webtortosa\functions_controller();

            $params = array(
                'DESCRIPCIO'                => $params_post['descripcio'],
                'LATITUD'                   => $params_post['latitud_value'],
                'LONGITUD'                  => $params_post['longitud_value'],
                'ID_NUCLI'                  => $params_post['nucli']!="" ? $params_post['nucli'] : 0,
                'ID_BARRI'                  => $params_post['barri']!="" ? $params_post['barri'] : 0,
                'CARRER'                    => $params_post['carrer'],
                'NUMERO'                    => $params_post['numero']!="" ? $params_post['numero'] : "",
                'DESCRIPCIO_LOCALITZACIO'   => $params_post['descripcio_localitzacio'],
                'NOM'                       => $params_post['nom'],
                'COGNOMS'                   => $params_post['cognoms'],
                'EMAIL'                     => $params_post['email'],
                'TELEFON'                   => $params_post['telefon'],
                'ADRECA'                    => $params_post['adreca'],
                'POBLACIO'                  => $params_post['poblacio'],
                'CODI_POSTAL'               => $params_post['codi_postal'],
                'IP_CONNEXIO'               => $utils->get_client_ip()
            );
            $id_incidencia = $ciutatdb->AddIncidenciaViaPublica($params);
            //var_dump($params);
            $content = '
                        <html>
                        <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                        </head>
                        <body>
                        <div>Enviament informació incidències a la via pública</div>
                        <p>Descripció: '.$params['DESCRIPCIO'].'</p>
                        <p>Latitud: '.$params['LATITUD'].'</p>
                        <p>Longitud: '.$params['LONGITUD'].'</p>
                        <p>Nucli: '.$params_post['nucli_name'].'</p>
                        <p>Barri: '.$params_post['barri_name'].'</p>
                        <p>Carrer: '.$params['CARRER'].'</p>
                        <p>Número: '.$params['NUMERO'].'</p>
                        <p>Descripció localització: '.$params['DESCRIPCIO_LOCALITZACIO'].'</p>
                        <p>Nom: '.$params['NOM'].'</p>
                        <p>Cognoms: '.$params['COGNOMS'].'</p>
                        <p>Email: '.$params['EMAIL'].'</p>
                        <p>Telèfon: '.$params['TELEFON'].'</p>
                        <p>Adreça: '.$params['ADRECA'].'</p>
                        <p>Població: '.$params['POBLACIO'].'</p>
                        <p>Codi potal: '.$params['CODI_POSTAL'].'</p>
                        </body>
                        </html>
                    ';

            if($utils->sendEmail_phpMailer('Incidencia via publica numero ' . $id_incidencia, $content, EMAIL_INCIDENCIES_VIA_PUBLICA, $params['EMAIL'])) {
                $smarty->assign('request', 1);
                $smarty->assign('num_incidencia', $id_incidencia);
            }
            else {
                $smarty->assign('request', 0);
            }

        }

        $elements = $ciutatdb->getNucli();
        $smarty->assign("nuclis", $elements);

        $publickey = "6Lfdwh4TAAAAAKajPw-1Rj8msRd8YVZx14Vwova-";
        $smarty->assign("recaptcha",recaptcha_get_html($publickey));
        $smarty->display("ciutat/ciutat_incidencies_via_publica.tpl");
   }

    /*
     * Obté llistat de barris.
     * Se li passa per paràmetre el id de nucli per fer un filtre.
     */
    function getbarris($nucli_id) {
        require_once (FOLDER_MODEL."/ciutat/ciutat.class.php");

        $ciutatdb = new \webtortosa\ciutat();
        $barris = $ciutatdb->getBarri($nucli_id);
        if(count($barris)>0) {
            $output = "";
            foreach ($barris as $key=>$barri) {
                if($key==0)
                    $output .= $barri->ID_BARRI . "|" . $barri->DESCRIPCIO;
                else
                    $output .= "||" . $barri->ID_BARRI . "|" . $barri->DESCRIPCIO;
            }
            return $output;
        }
        return null;
    }

    /*
     * Obté llistat de carrers.
     * Se li passa per paràmetre string del carrer o part del nom del carrer.
     */
    function getcarrers($carrer_str, $nucli, $text_plain = null) {
        require_once (FOLDER_MODEL."/ciutat/ciutat.class.php");

        $ciutatdb = new \webtortosa\ciutat();
        $carrers = $ciutatdb->getCarrer($carrer_str, $nucli);
        if(count($carrers)>0) {
            $output = "";
            foreach ($carrers as $key=>$carrer) {
                if(!$text_plain) {
                    $output .= '<div class="suggest-element"><a data="' . $carrer->TIPVIA . ' ' . $carrer->DESCRIPCIO . '" id="service' . $carrer->ID . '">' . $carrer->TIPVIA . ' ' . $carrer->DESCRIPCIO . '</a></div>';
                }
                else {
                    if($key==0)
                        $output .= $carrer->DESCRIPCIO . "|" . $carrer->DESCRIPCIO . " " . $carrer->TIPVIA;
                    else
                        $output .= "||" . $carrer->DESCRIPCIO . "|" . $carrer->DESCRIPCIO . " " . $carrer->TIPVIA;
                }
            }
            return $output;
        }
        return null;
    }

    function ciutat_territori ($input_lang) {
             $lang = $this->languages($input_lang);
             $smarty = new \webtortosa\Smarty_web();
             $smarty->assign('lang',$lang);
             $smarty->assign('url_ca','/ciutat/territori/');
             $smarty->assign('url_es','/es/ciudad/territorio/');
             $smarty->assign('url_en','/ciutat/territori.php?lang=en');
             //$smarty->assign('url_fr','/ciutat/territori.php?lang=fr');

             require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
             require_once (FOLDER_LANGUAGES."/ciutat/".$lang."_territori.cfg");
             require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

             $smarty->display("ciutat/ciutat_territori.tpl");
    }

    function ciutat_planols ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        $smarty->assign('url_ca','/ciutat/planols.php');
        $smarty->assign('url_es','/ciutat/planols.php?lang=es');

        $smarty->assign('ntitol', $n);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/ciutat/".$lang."_planols.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("ciutat/ciutat_planols.tpl");
    }

    function cuniversitaria ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/ciutat/".$lang."_cuniversitaria.cfg");
        require_once (FOLDER_LANGUAGES."/ciutat/".$lang."_cuniversitaria".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("ciutat/cuniversitaria.tpl");
    }

    function iviapublica ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('url_ca','/iviapublica');
        $smarty->assign('url_es','/iviapublica/index.php?lang=es');
        $smarty->assign('url_en','/iviapublica/index.php?lang=en');
        //$smarty->assign('url_fr','/iviapublica/index.php?lang=fr');
        $smarty->assign('ntpl', $n);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/iviapublica/".$lang."_iviapublica.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("iviapublica/iviapublica.tpl");
    }

    function formivp ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('ntpl', $n);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/iviapublica/".$lang."_formivp.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("iviapublica/formivp.tpl");
    }


    function acciosocial ($input_lang, $m, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('url_ca','/acciosocial');
        $smarty->assign('url_es','/acciosocial/index.php?lang=es');

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");
        if ($m==0){
            require_once (FOLDER_LANGUAGES."/acciosocial/".$lang."_as.cfg");
            require_once (FOLDER_LANGUAGES."/acciosocial/".$lang."_as0.cfg");
        }
        else{
            require_once (FOLDER_LANGUAGES."/acciosocial/".$lang."_as".$m.".cfg");
            require_once (FOLDER_LANGUAGES."/acciosocial/".$lang."_as".$m.$n.".cfg");
        }


        $smarty->display("acciosocial/as.tpl");
    }

    function mediambient ($input_lang, $m, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");
        if ($m==0){
            require_once (FOLDER_LANGUAGES."/mediambient/".$lang."_ma.cfg");
            require_once (FOLDER_LANGUAGES."/mediambient/".$lang."_ma0.cfg");
        }
        else{
            require_once (FOLDER_LANGUAGES."/mediambient/".$lang."_ma".$m.".cfg");
            require_once (FOLDER_LANGUAGES."/mediambient/".$lang."_ma".$m.$n.".cfg");
        }


        $smarty->display("mediambient/ma.tpl");
    }

    function divcul ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('url_ca','/divcul');
        $smarty->assign('url_es','/divcul/index.php?lang=es');

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/divcul/".$lang."_divcul.cfg");
        require_once (FOLDER_LANGUAGES."/divcul/".$lang."_divcul".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        switch ($n) {
            case 1:
                $smarty->assign('url_ca','/divcul/index.php');
                $smarty->assign('url_es','/divcul/index.php?lang=es');
                break;
            case 2:
                $smarty->assign('url_ca','/divcul/divcul2.php');
                $smarty->assign('url_es','/divcul/divcul2.php?lang=es');
                break;
            case 3:
                $smarty->assign('url_ca','/divcul/divcul3.php');
                $smarty->assign('url_es','/divcul/divcul3.php?lang=es');
                break;
            case 4:
                $smarty->assign('url_ca','/divcul/divcul4.php');
                $smarty->assign('url_es','/divcul/divcul4.php?lang=es');
                break;
            case 5:
                $smarty->assign('url_ca','/divcul/divcul5.php');
                $smarty->assign('url_es','/divcul/divcul5.php?lang=es');
                break;
            case 6:
                $smarty->assign('url_ca','/divcul/divcul6.php');
                $smarty->assign('url_es','/divcul/divcul6.php?lang=es');
                break;
        }

        $smarty->display("divcul/divcul.tpl");
    }

    function vinallop ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        if($n>1) {
            $smarty->assign('url_ca', '/vinallop/vinallop'.$n.'.php');
            $smarty->assign('url_es', '/vinallop/vinallop'.$n.'.php?lang=es');
            $smarty->assign('url_en', '/vinallop/vinallop'.$n.'.php?lang=en');
        }
        else {
            $smarty->assign('url_ca', '/vinallop/');
            $smarty->assign('url_es', '/vinallop/index.php?lang=es');
            $smarty->assign('url_en', '/vinallop/index.php?lang=en');
            //$smarty->assign('url_fr','/vinallop/index.php?lang=fr');
        }

        $smarty->assign('item',$n);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/vinallop/".$lang."_vinallop.cfg");
        require_once (FOLDER_LANGUAGES."/vinallop/".$lang."_vinallop".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("vinallop/vinallop.tpl");
    }

    function reguers ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        if($n>1) {
            $smarty->assign('url_ca', '/reguers/reguers'.$n.'.php');
            $smarty->assign('url_es', '/reguers/reguers'.$n.'.php?lang=es');
            $smarty->assign('url_en', '/reguers/reguers'.$n.'.php?lang=en');
        }
        else {
            $smarty->assign('url_ca', '/reguers/');
            $smarty->assign('url_es', '/reguers/index.php?lang=es');
            $smarty->assign('url_en', '/reguers/index.php?lang=en');
            //$smarty->assign('url_fr','/reguers/index.php?lang=fr');
        }

        $smarty->assign('item',$n);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/reguers/".$lang."_reguers.cfg");
        require_once (FOLDER_LANGUAGES."/reguers/".$lang."_reguers".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("reguers/reguers.tpl");
    }

    function protocols ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('item',$n);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/protocols/".$lang."_protocols.cfg");
        require_once (FOLDER_LANGUAGES."/protocols/".$lang."_protocols".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");
          switch ($n){
            case 1:
                $smarty->assign('url_ca','/protocols/index.php');
                $smarty->assign('url_es','/protocols/index.php?lang=es');
                break;
            case 4:
                $smarty->assign('url_ca','/protocols/protocols4.php');
                $smarty->assign('url_es','/protocols/protocols4.php?lang=es');
                break;
            case 5:
                $smarty->assign('url_ca','/protocols/protocols5.php');
                $smarty->assign('url_es','/protocols/protocols5.php?lang=es');
                break;
        }

        $smarty->display("protocols/protocols.tpl");
    }

    function ensenya ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/ensenyament/".$lang."_ensenya.cfg");
        require_once (FOLDER_LANGUAGES."/ensenyament/".$lang."_ensenya".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("ensenyament/ensenya.tpl");
    }

    function pee616 ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/ensenyament/616/".$lang."_616.cfg");
        require_once (FOLDER_LANGUAGES."/ensenyament/616/".$lang."_616".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("ensenyament/616/616.tpl");
    }

    function ajunta ($input_lang, $responsive) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        $smarty->assign('url_ca','/ajunta/index.php');
        $smarty->assign('url_es','/ajunta/index.php?lang=es');

        //Variable per a la programació responsive en mode developer.
        $smarty->assign('responsive',$responsive);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/ajunta/".$lang."_ajunta.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        if(isset($responsive)){
            $smarty->display("ajunta/ajunta2.tpl");    
        }else{
            $smarty->display("ajunta/ajunta.tpl");
        }

    }

    function organs ($input_lang) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        $smarty->assign('url_ca','/organs/index.php');
        $smarty->assign('url_es','/organs/index.php?lang=es');

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/organs/".$lang."_organs.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("organs/organs.tpl");
    }

    function omunicipal ($input_lang) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        $smarty->assign('url_ca','/omunicipal/index.php');
        $smarty->assign('url_es','/omunicipal/index.php?lang=es');

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/omunicipal/".$lang."_omunicipal.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("omunicipal/omunicipal.tpl");
    }


    function negocis ($input_lang, $responsive) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('url_ca','/negocis/index.php');
        $smarty->assign('url_es','/negocis/index.php?lang=es');
        $smarty->assign('url_en','/negocis/index.php?lang=en');
        $smarty->assign('url_fr','/negocis/index.php?lang=fr');

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/negocis/".$lang."_negocis.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        //Variable per a la programació responsive en mode developer.
        $smarty->assign('responsive',$responsive);

        if(isset($responsive)){
            $smarty->display("negocis/negocis2.tpl");    
        }else{
            $smarty->display("negocis/negocis.tpl");    
        }
        
    }


    function negocis_i ($input_lang, $n, $nt) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('url_ca','/negocis/negocis'.$n.'.php');
        $smarty->assign('url_es','/negocis/negocis'.$n.'.php?lang=es');
        $smarty->assign('url_en','/negocis/negocis'.$n.'.php?lang=en');
        $smarty->assign('url_fr','/negocis/negocis'.$n.'.php?lang=fr');

        $smarty->assign('ntpl', $n);
        $smarty->assign('ntitol', $nt);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/negocis/".$lang."_negocis_i.cfg");
        require_once (FOLDER_LANGUAGES."/negocis/".$lang."_negocis_i".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("negocis/negocis_i.tpl");
    }


    // m: lloc que ocupa al menú    n: lloc que ocupa al submenú   nt: num elems que te el submenú
    function om ($input_lang, $m, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        $smarty->assign('ntpl', $n);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");
        if ($m==0){
            require_once (FOLDER_LANGUAGES."/ajunta/om/".$lang."_om.cfg");
            $smarty->display("ajunta/om/om.tpl");
        }
        else{
            if ($m==2 & $n>5)
                require_once (FOLDER_LANGUAGES."/ajunta/om/".$lang."_om".$m."b.cfg");
            else
                require_once (FOLDER_LANGUAGES."/ajunta/om/".$lang."_om".$m.".cfg");
            require_once (FOLDER_LANGUAGES."/ajunta/om/".$lang."_om".$m.$n.".cfg");
        }
        if ($m==1){
            if ($n==5)
                $smarty->display("ajunta/om/omAf.tpl");
            else
                $smarty->display("ajunta/om/omA.tpl");
        }
        elseif ($m==2){
            if ($n==3 | $n==4)
                $smarty->display("ajunta/om/omBf.tpl");
            else
                $smarty->display("ajunta/om/omB.tpl");
        }
        elseif ($m==3){
            if ($n==4)
                $smarty->display("ajunta/om/omBf.tpl");
            else
                $smarty->display("ajunta/om/omB.tpl");
        }
        elseif ($m==4){
            $smarty->display("ajunta/om/omB.tpl");
        }
        elseif ($m==5){
            $smarty->display("ajunta/om/omB.tpl");
        }
    }

    function infecon ($input_lang, $n, $nt, $nm, $nms) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/ajunta/infecon/".$lang."_infecon.cfg");
        require_once (FOLDER_LANGUAGES."/ajunta/infecon/".$lang."_infecon".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->assign('ntpl', $n);
        $smarty->assign('ntitol', $nt);
        $smarty->assign('nmi', $nm);
        $smarty->assign('nmisub', $nms);
        $smarty->assign('icon2', 'icon-angle-double-right');
        if ($n==1 | $n==5){ // Pressupost o Històric
            $smarty->assign('icon', 'icon-chart-bar');
        }
        elseif ($n==3){ // Execució
            $smarty->assign('icon', 'icon-chart-pie');
        }
        elseif ($n==4){ // PMP
            $smarty->assign('icon', 'icon-chart-area');
        }

        $smarty->display("ajunta/infecon/infecon.tpl");
    }

    function butlletins ($input_lang, $n, $nt) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        $smarty->assign('url_ca','/noticies/butlleti.php');
        $smarty->assign('url_es','/noticies/butlleti.php?lang=es');

        $smarty->assign('ntpl', $n);
        $smarty->assign('ntitol', $nt);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/butlletins/".$lang."_butlletins".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("butlletins/butlletins.tpl");
    }

    function deixalleria ($input_lang, $n, $nt) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        $smarty->assign('ntpl', $n);
        $smarty->assign('ntitol', $nt);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/deixalleria/".$lang."_deixalleria.cfg");
        require_once (FOLDER_LANGUAGES."/deixalleria/".$lang."_deixalleria".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        switch($n) {
            case 1:
                $smarty->assign('url_ca','/deixalleria/index.php');
                $smarty->assign('url_es','/deixalleria/index.php?lang=es');
                break;
            case 2:
                $smarty->assign('url_ca','/deixalleria/deixahorari.php');
                $smarty->assign('url_es','/deixalleria/deixahorari.php?lang=es');
                break;
            case 3:
                $smarty->assign('url_ca','/deixalleria/deixacontacta.php');
                $smarty->assign('url_es','/deixalleria/deixacontacta.php?lang=es');
                break;
            case 4:
                $smarty->assign('url_ca','/deixalleria/deixaandromines.php');
                $smarty->assign('url_es','/deixalleria/deixaandromines.php?lang=es');
                break;
        }

        $smarty->display("deixalleria/deixalleria.tpl");
    }


    function cementiri ($input_lang, $n, $nt) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        $smarty->assign('ntpl', $n);
        $smarty->assign('ntitol', $nt);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/cementiri/".$lang."_cementiri.cfg");
        require_once (FOLDER_LANGUAGES."/cementiri/".$lang."_cementiri".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        switch($n) {
            case 1:
                $smarty->assign('url_ca','/cementiri/index.php');
                $smarty->assign('url_es','/cementiri/index.php?lang=es');
                break;
            case 2:
                $smarty->assign('url_ca','/cementiri/cemenhorari.php');
                $smarty->assign('url_es','/cementiri/cemenhorari.php?lang=es');
                break;
            case 3:
                $smarty->assign('url_ca','/cementiri/cemencontacta.php');
                $smarty->assign('url_es','/cementiri/cemencontacta.php?lang=es');
                break;
        }

        $smarty->display("cementiri/cementiri.tpl");
    }


    function correus ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('url_ca','/correus/');
        $smarty->assign('url_es','/correus/index.php?lang=es');

        $dades_telefons = new \webtortosa\telefon();

        $items_telefons = $dades_telefons->getTelefonsData($params);
        $smarty->assign('itemsTel',$items_telefons);

        $smarty->assign('ntpl', $n);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/correus/".$lang."_correus.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("correus/correus.tpl");
    }

    function telfurgen ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        $smarty->assign('url_ca','/telfurgen/index.php');
        $smarty->assign('url_es','/telfurgen/index.php?lang=es');
        $smarty->assign('url_en','/telfurgen/index.php?lang=en');
        //$smarty->assign('url_fr','/telfurgen/index.php?lang=fr');

        $smarty->assign('ntpl', $n);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/telfurgen/".$lang."_telfurgen.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("telfurgen/telfurgen.tpl");
    }

    function telecentre ($input_lang, $n, $nt) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        $smarty->assign('ntpl', $n);
        $smarty->assign('ntitol', $nt);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/telecentre/".$lang."_telecentre.cfg");
        require_once (FOLDER_LANGUAGES."/telecentre/".$lang."_telecentre".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("telecentre/telecentre.tpl");
    }


    function headline ($input_lang, $test = null) {
        $lang = $this->languages($input_lang);
        require_once (FOLDER_MODEL."/headline.class.php");
        require_once (FOLDER_MODEL."/tematica.class.php");
        $headline = new \webtortosa\headline();
        $tematicadb = new \webtortosa\tematica();

        $elements = $headline->getHeadlinesDataOrdF();
        
        if($input_lang==""){
                $input_lang="ca";
        }
        $cont=1;
        foreach($elements as $itemElement) {
            $tematica = $tematicadb->getTematiquesForHeadlines($itemElement->ID);
            if($cont==1) {
                $content_text .= "<div class='caixaheadlinep'>"; 
            }
            else if($cont==2){
                $content_text .= "<div class='caixaheadlinep'>";
            }
            else if($cont==3){
                $content_text .= "<div class='caixaheadlinep'>";
            }
            else if($cont==4){
                $content_text .= "<div class='caixaheadlinep'>";
            }
            else if($cont==5){
                break;//A la 4 sortim per deixar lloc a widget de twitter, en developer.
                $content_text .= "<div class='caixaheadlinep' id='caixaheadlinep5'>";
            }
            else if($cont==6){
                break; //A la 5 sortim per deixar lloc a widget de twitter.
                $content_text .= "<div class='caixaheadlinep' id='caixaheadlinep6'>";
            }
            if($itemElement->FOTO==""){
                $content_text .= "<div><img src='/images/headlines/inotimage.jpg' width='220' height='125'></div>";
            }
            else{
                if($itemElement->IMPORT=="0")
                    $content_text .= "<div><img src='/images/headlines/min_".$itemElement->FOTO."' width='220' height='125'></div>";
                else
                    $content_text .= "<div><img src='http://www.tortosa.cat/webajt/gestiointerna/headlines/fotos/".$itemElement->FOTO."' width='220' height='125'></div>";
            }
            $content_text .= "<div class='caixaheadlinep_cos'>";
            $content_text .= "<div id='headlinep_data'>".strftime('%d/%m/%Y', strtotime($itemElement->DATA))."</div>";
            $content_text .= "<div id='headlinep_titol'>"."<a href='/noticies/noticia.php?lang=".$input_lang."&id=".$itemElement->ID."'>".str_replace("\'", "'", $itemElement->TITOL)."</a></div>";
            $content_text .= "<div id='headlinep_subtitol'><strong>".str_replace("\'", "'", $tematica[0]->TEMATICA).". </strong>".str_replace("\'", "'", $itemElement->SUBTITOL)."</div>";
            $content_text .= "</div>";
            $content_text .= "</div>";
            $cont += 1;
        }
        return $content_text;
    }


    function noticies ($input_lang, $n, $nt, $params_values_get=null, $params_values_post=null) {
        $lang = $this->languages($input_lang);
        require_once (FOLDER_MODEL."/headline.class.php");
        require_once (FOLDER_MODEL."/tematica.class.php");
        $headline = new \webtortosa\headline();
        $tematicadb = new \webtortosa\tematica();
        $utils = new \webtortosa\functions_controller();

        $data_inici = $params_values_post['data_inici']!="" ? $params_values_post['data_inici'] : $params_values_get['data_inici'];
        $data_fi = $params_values_post['data_fi']!="" ? $params_values_post['data_fi'] : $params_values_get['data_fi'];
        $text = $params_values_post['text']!="" ? $params_values_post['text'] : $params_values_get['text'];
        //echo $data_inici . ' - ' . $data_fi . ' - ' . $text;

        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        $smarty->assign('url_ca','/noticies/index.php');
        $smarty->assign('url_es','/noticies/index.php?lang=es');

        $smarty->assign('ntpl', $n);
        $smarty->assign('ntitol', $nt);

        $file_logs = FOLDER_LOGS . '/'.date('d-m-Y').'.log';
        $this->escribir_log($file_logs, 'Entra a noticies');

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/noticies/".$lang."_noticies".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $params = array(
            'data_inici'    => $utils->normaldate_to_mysql($utils->test_input($data_inici)),
            'data_fi'       => $utils->normaldate_to_mysql($utils->test_input($data_fi)),
            'text'          => $utils->test_input($text),
            'login'         => 'premsa'
        );


        /* Pagination */
        $total = $headline->getHeadlinesDataForListMaxium($params);
        $page = $params_values_get['page'];
        $start = $page ? ($page - 1) * ITEMS_LIMIT : 0;

        if($page==0)
            $page = 1;
        $prev = $page - 1; //previous page is current page - 1
        $next = $page + 1; //next page is current page + 1
        $lastpage = ceil($total/ITEMS_LIMIT); //lastpage.

        $elements = $headline->getHeadlinesDataForList($params, $start, ITEMS_LIMIT);

        $tematiques = array();
        foreach($elements as &$element) {
            $element->FOTO = str_replace("'","·", $element->FOTO);
            $tematica = $tematicadb->getTematiquesForHeadlines($element->ID);
            $tematiques[$element->ID] = $tematica[0]->TEMATICA;
        }
        $smarty->assign("data_inici",$data_inici);
        $smarty->assign("data_fi",$data_fi);
        $smarty->assign("text",$text);

        $smarty->assign("lastpage",$lastpage);
        $smarty->assign("page",$page);
        $smarty->assign("items",$elements);
        $smarty->assign("tematiques",$tematiques);

        //var_dump($elements);
        $smarty->assign('destination_form', htmlspecialchars($_SERVER["PHP_SELF"]).'?lang='.$lang);
        $smarty->display("noticies/llistat_noticies.tpl");
    }


    function noticia ($input_lang, $id) {
        $lang = $this->languages($input_lang);

        require_once (FOLDER_MODEL."/headline.class.php");
        require_once (FOLDER_MODEL."/tematica.class.php");
        require_once (FOLDER_MODEL."/tipologia.class.php");

        $headline = new \webtortosa\headline();
        $tematicadb = new \webtortosa\tematica();
        $tipologiadb = new \webtortosa\tipologia();

        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('url_ca','/noticies/noticia.php?id='.$id);
        $smarty->assign('url_es','/noticies/noticia.php?lang=es&id='.$id);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/noticies/".$lang."_noticia".".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $params = array(
            ID=>$id
        );
        $elements = $headline->getHeadlinesData($params);
        $content_text = "";
        foreach($elements as $itemElement) {
            $tematiques = $tematicadb->getTematiquesForHeadlines($itemElement->ID);
            $smarty->assign('date_publish', strftime('%d/%m/%Y', strtotime($itemElement->DATA)));
            $smarty->assign('time_publish', strftime('%R', strtotime($itemElement->DATA)));

            $smarty->assign('TITOL', str_replace("\'", "'", $itemElement->TITOL));
            $smarty->assign('SUBTITOL', str_replace("\'", "'", $itemElement->SUBTITOL));
            $smarty->assign('COS', str_replace("\'", "'", str_replace("\'", "'", str_replace("\r\n", "<br />", $itemElement->DESCRIPCIO))));
            $smarty->assign('LINK', $itemElement->LINK);
            $smarty->assign('ADJUNT', $itemElement->ADJUNT);
            
             if($itemElement->FOTO==""){
                 $smarty->assign("IMAGE", "<img src='/images/headlines/inotimage.jpg' width='220' height='125'>");
            }
            else{
                if($itemElement->IMPORT=="0")
                    $smarty->assign("IMAGE", "<img src='/images/headlines/res_".$itemElement->FOTO."' width='220' height='125'>");
                else
                    $smarty->assign("IMAGE", "<img src='http://www.tortosa.cat/webajt/gestiointerna/headlines/fotos/".$itemElement->FOTO."' width='220' height='125'>");
            }
            if(count($tematiques)>0) {
                $content_tematica = array();
                foreach($tematiques as $tematica) {
                    $content_tematica[] =  $tematica->TEMATICA;
                }
            }
            $params_tipologia = array(
                "ID"   => $itemElement->ID_TIPOLOGIA
            );
            $tipologies = $tipologiadb->getTipologiaData($params_tipologia);
            if(count($tipologies)>0) {
                $content_tipologia = array();
                foreach($tipologies as $tipologia) {
                    $content_tipologia[] = $tipologia->TIPOLOGIA;
                }
            }
            //var_dump($content_tipologia);
        }


        //$smarty->assign('LABEL_TC1', $content_text);
        $smarty->assign('IMG_NAME_ON_DB', $itemElement->FOTO);
        $smarty->assign('LABEL_TC_TEMATICA', $content_tematica);
        $smarty->assign('LABEL_TC_TIPOLOGIA', $content_tipologia);

        $smarty->display("noticies/noticia.tpl");
    }


    function urbanisme ($input_lang, $m, $n, $slug) {
        require_once (FOLDER_MODEL."/urbanisme.class.php");
        $urbanisme = new \webtortosa\urbanisme();
        $utils = new \webtortosa\functions_controller();
        $params['NIVELL'] = 1;
        $menu = $urbanisme->getMenuUrbanismeData($params);
        $listMenu = array();
        foreach($menu as $itemMenu) {
            $submenu = $urbanisme->getMenuUrbanismeData(array('ID_NIVELL_SUPERIOR'=>$itemMenu->ID));
            if(count($submenu)>0) {
                $listMenu[$itemMenu->TITOL] = array();
                foreach ($submenu as $subitemMenu) {
                    array_push($listMenu[$itemMenu->TITOL], array($subitemMenu->TITOL, $subitemMenu->LINK));
                }
            }
            else {
                $listMenu[$itemMenu->TITOL] = $itemMenu->LINK;
            }
        }

        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        $smarty->assign('url_ca','/urbanisme/index.php');
        $smarty->assign('url_es','/urbanisme/index.php?lang=es');

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");
        if ($m==0){
            require_once (FOLDER_LANGUAGES."/urbanisme/".$lang."_urbanisme.cfg");
        }
        else{
            require_once (FOLDER_LANGUAGES."/urbanisme/".$lang."_urbanisme".$m.".cfg");
            require_once (FOLDER_LANGUAGES."/urbanisme/".$lang."_urbanisme".$m.$n.".cfg");
        }

        if (($m==0)&&($slug=="")){

            $smarty->assign('menu_dinamic', $listMenu);
            $smarty->display("urbanisme/urbanisme.tpl");
        }
        else{
            if($slug!="") {
                $titles = $urbanisme->getTreeMenuFromSlug($slug);
                $content_title = "";
                if(count($titles)>0) {
                    $content_title .= "<div id='title_page_parent'>".$titles[0]->TITOL_MENU_PARENT."</div>";
                    $content_title .= "<div id='title_page'>".$titles[0]->TITOL_MENU."</div>";
                }

                $elements = $urbanisme->getElementUrbanismeFromElementMenu($slug);
                $content_text = "";
                if(count($elements )>0) {
                    foreach ($elements as $itemElement) {
                        if ($itemElement->LINK != "")
                            $link_titol = "<a href='" . $itemElement->LINK . "' target='_blank'>" . $itemElement->TITOL . "</a>";
                        else
                            $link_titol = $itemElement->TITOL;

                        if($utils->normaldate_to_spanish($itemElement->DATA))
                            $fecha = $utils->normaldate_to_spanish($itemElement->DATA) . " - ";

                        $content_text .= "<div class='element_container' style=\"border-bottom: #cccccc 1px solid; padding-top: 15px;\">
                        <div class='title_element'>".$fecha . $link_titol . "</div>";
                        $params = array('ID_ELEMENT' => $itemElement->ID);
                        $links = $urbanisme->getLinksUrbanismeData($params);
                        foreach ($links as $key => $itemLink) {
                            if ($key == 0)
                                $content_text .= "<ul class='llista'>";
                            $content_text .= "<li class='item_link_container'>";
                            if (strpos($itemLink->LINK, ".pdf") > 0)
                                $content_text .= "<i class='icon-file-pdf'></i>";
                            else
                                $content_text .= "<i class='icon-folder-open-empty'></i>";

                            $content_text .= "<div class='item_title_link'><a href='" . $itemLink->LINK . "' title='" . $itemLink->TITOL . "' target='_blank'>" . $itemLink->TITOL . "</a>";
                            if ($itemLink->TIPUS != "altres") $content_text .= "<span class='item_type_link'>" . $itemLink->TIPUS . "</span>";
                            $content_text .= "  </div>
                                             </li>";
                        }
                        if ($key > 0)
                            $content_text .= "</ul>";

                        $content_text .= "</div>";
                    }
                }
                $smarty->assign('LABEL_TC1', $content_title . $content_text);
            }
            $smarty->display("urbanisme/urbanismeA.tpl");
        }

    }


    
 function lgtbi ($input_lang) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        
        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/lgtbi/".$lang."_lgtbi.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");
        $smarty->assign('url_ca','/lgtbi/index.php');
        $smarty->assign('url_es','/lgtbi/index.php?lang=es');
        $smarty->display("lgtbi/lgtbi.tpl");
}


function ebop ($input_lang) {
    $lang = $this->languages($input_lang);
    $smarty = new \webtortosa\Smarty_web();
    $smarty->assign('lang',$lang);
    
    require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
    require_once (FOLDER_LANGUAGES."/ebop/".$lang."_ebop.cfg");
    require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");
    $smarty->assign('url_ca','/ebop/index.php');
    $smarty->assign('url_es','/ebop/index.php?lang=es');
    $smarty->display("ebop/ebop.tpl");
}


 function atencio_ciutadana ($input_lang, $n) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

/*Cal definir 'type" perque s'usa a xxx_home.tpl*/
        $smarty->assign('type',$n);

        
        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
/*Carrega el menu*/
        require_once (FOLDER_LANGUAGES."/atencio_ciutadana/".$lang."_atencio_ciutadana.cfg");
/*Carrega el contingut de la opcio del menu*/
        require_once (FOLDER_LANGUAGES."/atencio_ciutadana/".$lang."_atencio_ciutadana".$n.".cfg");


        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");
      
        switch ($n) {
            case 1:
                $smarty->assign('url_ca','/atencio_ciutadana/index.php');
                $smarty->assign('url_es','/atencio_ciutadana/index.php?lang=es');
                break;
            case 2:
                $smarty->assign('url_ca','/atencio_ciutadana/atencio_ciutadana_horari.php');
                $smarty->assign('url_es','/atencio_ciutadana/atencio_ciutadana_horari.php?lang=es');
                break;
            case 3:
                $smarty->assign('url_ca','/atencio_ciutadana/atencio_ciutadana_seu.php');
                $smarty->assign('url_es','/atencio_ciutadana/atencio_ciutadana_seu.php?lang=es');
                break;
            case 4:
                $smarty->assign('url_ca','/atencio_ciutadana/atencio_ciutadana_contacta.php');
                $smarty->assign('url_es','/atencio_ciutadana/atencio_ciutadana_contacta.php?lang=es');
                break;
        }
        $smarty->display("atencio_ciutadana/atencio_ciutadana.tpl");
    }



   function arxiu ($input_lang, $n) {
        /*Funcio amb menu*/
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        /* switch per als llenguatges */
        switch ($n) {
            case '1':
                $smarty->assign('url_ca','/arxiu/index.php');
                $smarty->assign('url_es','/arxiu/index.php?lang=es');
                break;
            case '2':
                $smarty->assign('url_ca','/arxiu/arxiu_tramits.php');
                $smarty->assign('url_es','/arxiu/arxiu_tramits.php?lang=es');
                break;
            case '3':
                $smarty->assign('url_ca','/arxiu/arxiu_doc_consultables.php');
                $smarty->assign('url_es','/arxiu/arxiu_doc_consultables.php?lang=es');
                break;
            case '4':
                $smarty->assign('url_ca','/arxiu/arxiu_diposit.php');
                $smarty->assign('url_es','/arxiu/arxiu_diposit.php?lang=es');
                break;
            case '5':
                $smarty->assign('url_ca','/arxiu/arxiu_repositori.php');
                $smarty->assign('url_es','/arxiu/arxiu_repositori.php?lang=es');
                break;
        }
        

        /*la var type es per al if's del tpl*/
        $smarty->assign('type',$n);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/arxiu/".$lang."_arxiu.cfg");
        require_once (FOLDER_LANGUAGES."/arxiu/".$lang."_arxiu".$n.".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $smarty->display("arxiu/arxiu.tpl");
   }

   function fitxa_ofertes_ocupacio($input_lang, $codi, $ens) {
        require_once (FOLDER_MODEL."/ofertes-ocupacio/oferta-ocupacio.class.php");
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('url_ca','/ofertes-ocupacio/fitxa.php?id=$codi');
        $smarty->assign('url_es','/ofertes-ocupacio/fitxa.php?id=$codi&lang=es');

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");



        require_once (FOLDER_LANGUAGES."/ofertes-ocupacio/".$lang."_oferta_ocupacio.cfg");

        $ofocdb = new \webtortosa\oferta();
        $fc = new \webtortosa\functions_controller();


        $elements = $ofocdb->getOfertaOcupacio($codi, null, null, null, null, 1, 0, $ens);
        if(count($elements)>0) {
            //Aquí consulta els fitxers associats a l'Oferta d'Ocupació.
            require_once (FOLDER_MODEL."/fitxers.class.php");
            $ofocfiles = new \webtortosa\fitxers();
            $params = array(
                'TAULA'     => 'OO_OFERTA_OCUPACIO',
                'ID_TAULA'  => $elements[0]->ID
            );
            $fitxers = $ofocfiles->getFitxers($params);
            $smarty->assign('fitxers', $fitxers);
        }
        $smarty->assign('elements', $elements);
        $smarty->assign('tipus', $ens);
        $smarty->display("ofertes-ocupacio/fitxa.tpl");
   }

   function ofertes_ocupacio($input_lang, $ens, $input_page) {
        require_once (FOLDER_MODEL."/ofertes-ocupacio/oferta-ocupacio.class.php");

        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('url_ca','/borsa/');
        $smarty->assign('url_es','/es/borsa/');

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        require_once (FOLDER_LANGUAGES."/ofertes-ocupacio/".$lang."_oferta_ocupacio.cfg");

        $ofocdb = new \webtortosa\oferta();
        $fc = new \webtortosa\functions_controller();

        $elements = $ofocdb->getOfertaOcupacio(null, null,  null,  null,  null,  1,  0, $ens);

        // Setup Pagination vars
        $total = count($elements);
        $page = $input_page;
        $start = $page ? ($page - 1) * ITEMS_LIMIT : 0;

        if($page==0)
            $page = 1;
        $prev = $page - 1; //previous page is current page - 1
        $next = $page + 1; //next page is current page + 1
        $lastpage = ceil($total/ITEMS_LIMIT); //lastpage.

        $elements = $ofocdb->getOfertaOcupacio(null, null, null, $start, ITEMS_LIMIT, 1, 0, $ens);

        $smarty->assign("lastpage",$lastpage);
        $smarty->assign("page",$page);
        $smarty->assign('elements', $elements);
        $smarty->assign('tipus', $ens);

        $smarty->display("ofertes-ocupacio/llistat_ofertes_ocupacio.tpl");
   }

   function borsa($input_lang, $ens, $input_page) {
        require_once (FOLDER_MODEL."/borsa.class.php");

        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('url_ca','/borsa/');
        $smarty->assign('url_es','/es/borsa/');

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        require_once (FOLDER_LANGUAGES."/borsa/".$lang."_borsa.cfg");

        $borsadb = new \webtortosa\borsa();
        $fc = new \webtortosa\functions_controller();

        $elements = $borsadb->getBorsa($ens);

        // Setup Pagination vars
        $total = count($elements);
        $page = $input_page;
        $start = $page ? ($page - 1) * ITEMS_LIMIT : 0;

        if($page==0)
            $page = 1;
        $prev = $page - 1; //previous page is current page - 1
        $next = $page + 1; //next page is current page + 1
        $lastpage = ceil($total/ITEMS_LIMIT); //lastpage.

        $elements = $borsadb->getBorsa($tipus_pagina, $start, ITEMS_LIMIT);

        if(count($elements)) {
            foreach($elements as &$item) {
                $item->descripcio_curta = preg_replace( "/\r|\n/", "", $item->descripcio_curta);
            }
        }
        $smarty->assign("lastpage",$lastpage);
        $smarty->assign("page",$page);
        $smarty->assign('elements', $elements);
        $smarty->assign('tipus', $ens);

        $smarty->display("borsa/borsa.tpl");
   }

    function fitxa_borsa($input_lang, $codi, $ens) {
        require_once (FOLDER_MODEL."/borsa.class.php");

        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('url_ca','/borsa/fitxa.php?id=$codi');
        $smarty->assign('url_es','/borsa/fitxa.php?id=$codi&lang=es');

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        require_once (FOLDER_LANGUAGES."/borsa/".$lang."_borsa.cfg");

        $borsadb = new \webtortosa\borsa();
        $fc = new \webtortosa\functions_controller();

        $elements = $borsadb->getBorsa($ens, null, null, $codi);
        if(count($elements)) {
            foreach ($elements as &$item) {

                $item->anunci = str_replace("\'", "'", $fc->filtrar_mssql($item->anunci));
                $item->objecte = str_replace("\'", "'", $fc->filtrar_mssql($item->objecte));
                $item->denominacio = str_replace("\'", "'", $fc->filtrar_mssql($item->denominacio));
                $item->caracter = str_replace("\'", "'", $fc->filtrar_mssql($item->caracter));
                $item->escala = str_replace("\'", "'", $fc->filtrar_mssql($item->escala));
                $item->subescala = str_replace("\'", "'", $fc->filtrar_mssql($item->subescala));
                $item->classe = str_replace("\'", "'", $fc->filtrar_mssql($item->classe));
                $item->grup = str_replace("\'", "'", $fc->filtrar_mssql($item->grup));
                $item->seleccio = str_replace("\'", "'", $fc->filtrar_mssql($item->seleccio));
                $item->requisits = str_replace("\'", "'", $fc->filtrar_mssql($item->requisits));
                $item->presen_solicituds = str_replace("\'", "'", $fc->filtrar_mssql($item->presen_solicituds));



            }
        }
        $smarty->assign('elements', $elements);
        $smarty->assign('tipus', $ens);

        $smarty->display("borsa/fitxa.tpl");
    }

    /***************************************************************************************************************************************
     ***************************************************************************************************************************************
    Funcions INFORMACIO PUBLICA
     ***************************************************************************************************************************************
     ***************************************************************************************************************************************/

    function infpub ($input_lang, $input_accio, $input_page) {
        $smarty = new \webtortosa\Smarty_web();
        $lang = $this->languages($input_lang);
        $smarty->assign('lang',$lang);

        $smarty->assign('url_ca','/infpub/index.php');
        $smarty->assign('url_es','/infpub/index.php?lang=es');

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/infpub/".$lang."_infpub.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $informacio_publica = new \webtortosa\informacio_publica();

        $elements = $informacio_publica->getInformacioPublica();

        // Setup Pagination vars
        $total = count($elements);
        $page = $input_page;
        $start = $page ? ($page - 1) * ITEMS_LIMIT : 0;

        if($page==0)
            $page = 1;
        $prev = $page - 1; //previous page is current page - 1
        $next = $page + 1; //next page is current page + 1
        $lastpage = ceil($total/ITEMS_LIMIT); //lastpage.

        $elements = $informacio_publica->getInformacioPublica($start, ITEMS_LIMIT);

        $smarty->assign("lastpage",$lastpage);
        $smarty->assign("page",$page);
        $smarty->assign('elements', $elements);
        $smarty->display("infpub/informacio_publica.tpl");
    }


    /***************************************************************************************************************************************
     ***************************************************************************************************************************************
    Funcions ORDENANCES
     ***************************************************************************************************************************************
     ***************************************************************************************************************************************/

    function ordenances ($input_tipus, $input_lang, $input_accio, $input_page) {
        $smarty = new \webtortosa\Smarty_web();
        $lang = $this->languages($input_lang);
        $smarty->assign('lang',$lang);

        $smarty->assign('url_ca','/ordenances/index.php');
        $smarty->assign('url_es','/ordenances/index.php?lang=es');

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/ordenances/".$lang."_ordenances.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $ordenances = new \webtortosa\ordenances();

        $elements = $ordenances->getOrdenances($input_tipus);

        // Setup Pagination vars
        $total = count($elements);
        $page = $input_page;
        $start = $page ? ($page - 1) * ITEMS_LIMIT : 0;

        if($page==0)
            $page = 1;
        $prev = $page - 1; //previous page is current page - 1
        $next = $page + 1; //next page is current page + 1
        $lastpage = ceil($total/ITEMS_LIMIT); //lastpage.

        $elements = $ordenances->getOrdenances($input_tipus, $start, ITEMS_LIMIT);

        $smarty->assign("lastpage",$lastpage);
        $smarty->assign("page",$page);
        $smarty->assign('elements', $elements);
        $smarty->assign('tipus', $input_tipus);

        //echo ("TOTAL->".$total);
        $smarty->display("ordenances/ordenances.tpl");
    }

    function getOrdenansaItem($params_values_get, $params_values_post, $input_lang) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        $smarty->assign('url_ca','/ordenances/index.php');
        $smarty->assign('url_es','/ordenances/index.php?lang=es');

        require_once (FOLDER_LANGUAGES."/ordenances/".$lang."_ordenances.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");
        $md = new \webtortosa\ordenances();
        $params = array();
        $params['Codi'] = $params_values_get['codi'];
        //$items = $this->filtraArrayUtf8Encode($md->getDataMemorial($params));
        $items = $md->getDataOrdenansa($params);
        if(count($items)>0) {
            $smarty->assign("item",$items[0]);
        }
        $params = array();

        $smarty->assign("page",$params_values_get['page']);
        $smarty->assign("accio",$params_values_get['accio']);
        $smarty->assign("tipus",$params_values_get['tipus']);
        $smarty->display("ordenances/ordenances.tpl");

    }

    function ordenancesOrIRe ($input_tipus, $input_lang, $input_accio, $input_page) {
        $smarty = new \webtortosa\Smarty_web();
        $lang = $this->languages($input_lang);
        $smarty->assign('lang',$lang);

        $smarty->assign('url_ca','/ordenances/indexorire.php');
        $smarty->assign('url_es','/ordenances/indexorire.php?lang=es');

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/ordenances/".$lang."_ordenances.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $ordenances = new \webtortosa\ordenances();

        $elements = $ordenances->getOrdenancesOrIRe($input_tipus);

        // Setup Pagination vars
        $total = count($elements);
        $page = $input_page;
        $start = $page ? ($page - 1) * ITEMS_LIMIT : 0;

        if($page==0)
            $page = 1;
        $prev = $page - 1; //previous page is current page - 1
        $next = $page + 1; //next page is current page + 1
        $lastpage = ceil($total/ITEMS_LIMIT); //lastpage.

        $elements = $ordenances->getOrdenancesOrIRe($input_tipus, $start, ITEMS_LIMIT);

        $smarty->assign("lastpage",$lastpage);
        $smarty->assign("page",$page);
        $smarty->assign('elements', $elements);
        $smarty->assign('tipus', $input_tipus);

        //echo ("TOTAL->".$total);
        $smarty->display("ordenances/ordenances_orire.tpl");
    }

    function getOrdenansaOrIReItem($params_values_get, $params_values_post, $input_lang) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        $smarty->assign('url_ca','/ordenances/indexorire.php');
        $smarty->assign('url_es','/ordenances/indexorire.php?lang=es');

        require_once (FOLDER_LANGUAGES."/ordenances/".$lang."_ordenances.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");
        $md = new \webtortosa\ordenances();
        $params = array();
        $params['Codi'] = $params_values_get['codi'];
        //$items = $this->filtraArrayUtf8Encode($md->getDataMemorial($params));
        $items = $md->getDataOrdenansa($params);
        if(count($items)>0) {
            $smarty->assign("item",$items[0]);
        }
        $params = array();

        $smarty->assign("page",$params_values_get['page']);
        $smarty->assign("accio",$params_values_get['accio']);
        $smarty->assign("tipus",$params_values_get['tipus']);
        $smarty->display("ordenances/ordenances_orire.tpl");
    }

    /***************************************************************************************************************************************
     ***************************************************************************************************************************************
    Funcions PLENS
     ***************************************************************************************************************************************
     ***************************************************************************************************************************************/

    function plens ($input_year, $input_lang, $input_page) {
        $smarty = new \webtortosa\Smarty_web();
        $lang = $this->languages($input_lang);
        $smarty->assign('lang',$lang);

        $smarty->assign('url_ca','/plens/index.php');
        $smarty->assign('url_es','/plens/index.php?lang=es');

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/plens/".$lang."_plens.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        //Filtrar camps per treure caràcters utilitzats en sqlinjection
        $utils = new \webtortosa\functions_controller();
        $utils->test_input($input_year);

        $plens = new \webtortosa\plens();
        $elements = $plens->getPlens($input_year);

        // Setup Pagination vars
        $total = count($elements);
        $page = $input_page;
        $start = $page ? ($page - 1) * ITEMS_LIMIT : 0;

        if($page==0)
            $page = 1;
        $prev = $page - 1; //previous page is current page - 1
        $next = $page + 1; //next page is current page + 1
        $lastpage = ceil($total/ITEMS_LIMIT); //lastpage.

        $elements = $plens->getPlens($input_year, $start, ITEMS_LIMIT);

        $smarty->assign("lastpage",$lastpage);
        $smarty->assign("page",$page);
        $smarty->assign('elements', $elements);
        $smarty->assign('year', $input_year);

        //echo ("TOTAL->".$total);
        $smarty->display("plens/plens.tpl");
    }

    /***************************************************************************************************************************************
     ***************************************************************************************************************************************
    Funcions JUNTES GOVERN
     ***************************************************************************************************************************************
     ***************************************************************************************************************************************/

    function juntesGovern ($input_year, $input_lang, $input_page) {
        /*
        if($input_year=="")
            $input_year = 2017;
        */    
        $smarty = new \webtortosa\Smarty_web();
        $lang = $this->languages($input_lang);
        $smarty->assign('lang',$lang);

        $smarty->assign('url_ca','/jungov/index.php');
        $smarty->assign('url_es','/jungov/index.php?lang=es');

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/jungov/".$lang."_juntesGovern.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        //Filtrar camps per treure caràcters utilitzats en sqlinjection
        $utils = new \webtortosa\functions_controller();
        $utils->test_input($input_year);

        $juntesGovern = new \webtortosa\juntes_govern();
        $elements = $juntesGovern->getJuntesGovern($input_year);

        // Setup Pagination vars
        $total = count($elements);
        $page = $input_page;
        $start = $page ? ($page - 1) * ITEMS_LIMIT : 0;

        if($page==0)
            $page = 1;
        $prev = $page - 1; //previous page is current page - 1
        $next = $page + 1; //next page is current page + 1
        $lastpage = ceil($total/ITEMS_LIMIT); //lastpage.

        $elements = $juntesGovern->getJuntesGovern($input_year, $start, ITEMS_LIMIT);

        $smarty->assign("lastpage",$lastpage);
        $smarty->assign("page",$page);
        $smarty->assign('elements', $elements);
        $smarty->assign('year', $input_year);

        //echo ("TOTAL->".$total);
        $smarty->display("jungov/juntesGovern.tpl");
    }


    /***************************************************************************************************************************************
     ***************************************************************************************************************************************
    Funcions GRUPS POLITICS - DOCUMENTS
     ***************************************************************************************************************************************
     ***************************************************************************************************************************************/

    function grupPoliticDocuments ($input_lang, $grup, $input_page) {
        $smarty = new \webtortosa\Smarty_web();
        $lang = $this->languages($input_lang);
        $smarty->assign('lang',$lang);

        $smarty->assign('url_ca','/grups-politics/index.php');
        $smarty->assign('url_es','/grups-politics/index.php?lang=es');

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");
        require_once (FOLDER_LANGUAGES."/grups-politics/".$lang."_documents.cfg");

        //Filtrar camps per treure caràcters utilitzats en sqlinjection
        $utils = new \webtortosa\functions_controller();
        $utils->test_input($grup);

        $idEns = 1;
        $idServei = 0;
        $nomCarpetaGrup = $grup;

        if ($grup=='PDECAT') {
            $smarty->assign("image", "http://www2.tortosa.cat/images/grups-politics/pdecat/tira.jpg");
            $smarty->assign("LABEL_TITOL1b", "PdeCat");
            $idServei = 20;
            $nomCarpetaGrup = 'pdecat';
            require_once (FOLDER_LANGUAGES."/grups-politics/".$nomCarpetaGrup."/".$lang."_menu.cfg");
            if ($lang=='es') {
                $smarty->assign("LABEL_TITOL1b", "Partido Demócrata Catalán");
            } else {
                $smarty->assign("LABEL_TITOL1b", "Partit Demòcrata Català");
            }
        }
        else if ($grup=='ERC')  {
            $smarty->assign("image", "http://www2.tortosa.cat/images/grups-politics/erc/tira.jpg");
            $idServei = 22;
            $nomCarpetaGrup = 'erc';
            require_once (FOLDER_LANGUAGES."/grups-politics/".$nomCarpetaGrup."/".$lang."_menu.cfg");
            if ($lang=='es') {
                $smarty->assign("LABEL_TITOL1b", "Izquierda Republicana de Cataluña");
            } else {
                $smarty->assign("LABEL_TITOL1b", "Esquerra Republicana de Catalunya");
            }
        }
        else if ($grup=='CUP')  {
            $smarty->assign("image", "http://www2.tortosa.cat/images/grups-politics/cup/tira.jpg");
            $idServei = 41;
            $nomCarpetaGrup = 'cup';
            require_once (FOLDER_LANGUAGES."/grups-politics/".$nomCarpetaGrup."/".$lang."_menu.cfg");
            if ($lang=='es') {
                $smarty->assign("LABEL_TITOL1b", "Candidatura d'Unitat Popular");
            } else {
                $smarty->assign("LABEL_TITOL1b", "Candidatura de Unidad Popular");
            }
        }
        else if ($grup=='MTE')  {
            $smarty->assign("image", "http://www2.tortosa.cat/images/grups-politics/mt-e/tira.jpg");
            $idServei = 40;
            $nomCarpetaGrup = 'mt-e';
            require_once (FOLDER_LANGUAGES."/grups-politics/".$nomCarpetaGrup."/".$lang."_menu.cfg");
            if ($lang=='es') {
                $smarty->assign("LABEL_TITOL1b", "Movem Tortosa - Entesa");
            } else {
                $smarty->assign("LABEL_TITOL1b", "Movem Tortosa - Entesa");
            }
        }
        else if ($grup=='PSC')  {
            $smarty->assign("image", "http://www2.tortosa.cat/images/grups-politics/psc/tira.jpg");
            $idServei = 21;
            $nomCarpetaGrup = 'psc';
            require_once (FOLDER_LANGUAGES."/grups-politics/".$nomCarpetaGrup."/".$lang."_menu.cfg");
            if ($lang=='es') {
                $smarty->assign("LABEL_TITOL1b", "Partido de los Socialistas de Cataluña");
            } else {
                $smarty->assign("LABEL_TITOL1b", "Partit dels Socialistes de Catalunya");
            }
        }
        else if ($grup=='PP')  {
            $smarty->assign("image", "http://www2.tortosa.cat/images/grups-politics/pp/tira.jpg");
            $idServei = 24;
            $nomCarpetaGrup = 'pp';
            require_once (FOLDER_LANGUAGES."/grups-politics/".$nomCarpetaGrup."/".$lang."_menu.cfg");
            if ($lang=='es') {
                $smarty->assign("LABEL_TITOL1b", "Partido Popular");
            } else {
                $smarty->assign("LABEL_TITOL1b", "Partit Popular");
            }
        }

        $documents = new \webtortosa\GPDocuments();
        $elements = $documents->getGPDocuments($idEns, $idServei);

        // Setup Pagination vars
        $total = count($elements);
        $page = $input_page;
        $start = $page ? ($page - 1) * ITEMS_LIMIT : 0;

        if($page==0)
            $page = 1;
        $prev = $page - 1; //previous page is current page - 1
        $next = $page + 1; //next page is current page + 1
        $lastpage = ceil($total/ITEMS_LIMIT); //lastpage.

        $elements = $documents->getGPDocuments($idEns, $idServei, $start, ITEMS_LIMIT);

        $smarty->assign("lastpage",$lastpage);
        $smarty->assign("page",$page);
        $smarty->assign('elements', $elements);
        $smarty->assign('grup', $grup);
        $smarty->assign('idEns', $idEns);
        $smarty->assign('idServei', $idServei);

        $smarty->assign('destination_form', htmlspecialchars($_SERVER["PHP_SELF"]));

        //echo ("LANG->   ".$input_lang. "<br>");
        //echo ("GRUP->   ".$grup. "<br>");
        //echo ("PAGE->   ".$input_page. "<br>");
        //echo ("ENS->   ".$idEns. "<br>");
        //echo ("SERVEI->".$idServei. "<br>");
        //echo ("TOTAL-> ".$total. "<br>");
        $smarty->display("grups-politics/grup-politic/documents.tpl");
    }




    function electes ($input_lang) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/eleccions/".$lang."_electes.cfg");

        $smarty->display("eleccions/electes.tpl");
    }


    /******************************************
     ********************************************
     ********************************************
        FI Funcions per a WEB
     ********************************************
     ********************************************************************************************************************
     ********************************************/


    /******************************************
    ********************************************
    ********************************************
       Funcions per a l'apartat de ELECCIONS
    ********************************************
    ********************************************
    ********************************************/

    function home_eleccions ($input_lang) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

		require_once (FOLDER_LANGUAGES."/".$this->languages($input_lang)."_ajunta.cfg");
		
        $smarty->display("index_eleccions.tpl");
    }

    function eleccionsElectes2015 ($params_values_get) {
    	$_SESSION['id_eleccions'] = $params_values_get["eleccions"] ? $params_values_get["eleccions"] : $_SESSION['id_eleccions'];
        $_SESSION['id_eleccions'] = $_SESSION['id_eleccions'] ? $_SESSION['id_eleccions'] : '2015MUN';

		$smarty = new \webtortosa\Smarty_web();
		$smarty->assign("path_eleccions","http://www2.tortosa.cat/templates");
        $smarty->assign("apartat","eleccions");
		$smarty->assign("path_charts",URL_CHARTS);
		$smarty->display("head.tpl");

        require_once (FOLDER_LANGUAGES."/".$this->languages($params_values_get["lang"])."_header.cfg");
        require_once (FOLDER_LANGUAGES."/eleccions/".$this->languages($params_values_get["lang"])."_eleccions.cfg");
        require_once (FOLDER_LANGUAGES."/".$this->languages($input_lang)."_footer.cfg");

		$dades_eleccions = new \webtortosa\eleccions();
		$eleccions = $dades_eleccions->getEleccionsData();
		$smarty->assign('eleccions',$this->filtraArrayUtf8Encode($eleccions));
		$smarty->assign('id_eleccio',$_SESSION['id_eleccions']);
		$smarty->display("eleccions/electes.tpl");
    }

    function eleccionsResultatsMeses ($params_values_get) {
    	$_SESSION['id_eleccions'] = $params_values_get["eleccions"] ? $params_values_get["eleccions"] : $_SESSION['id_eleccions'];
        $_SESSION['id_eleccions'] = $_SESSION['id_eleccions'] ? $_SESSION['id_eleccions'] : $this->getUltimaEleccio();

    	$seccions = $params_values_get["meses"] ? $params_values_get["meses"] : '01|001|01001U';

    	$seccions = explode("|", $seccions);
    	$districte = $seccions[0];
    	$seccio = $seccions[1];
    	$mesa = $seccions[2];

		$smarty = new \webtortosa\Smarty_web();
		$smarty->assign("path_eleccions","http://www2.tortosa.cat/templates");
        $smarty->assign("apartat","eleccions");
		$smarty->assign("path_charts",URL_CHARTS);
		$smarty->display("head.tpl");

        require_once (FOLDER_LANGUAGES."/".$this->languages($params_values_get["lang"])."_header.cfg");
        require_once (FOLDER_LANGUAGES."/eleccions/".$this->languages($params_values_get["lang"])."_eleccions.cfg");
        require_once (FOLDER_LANGUAGES."/".$this->languages($input_lang)."_footer.cfg");

		$dades_eleccions = new \webtortosa\eleccions();
		$params = array($_SESSION['id_eleccions']);

		$meses = $dades_eleccions->getMeses($params);
		foreach ($meses as $key => &$item_mesa) {
			$item_mesa->NomColegi = utf8_encode($item_mesa->NomColegi);
		}
		$smarty->assign('num_mesa',$districte."|".$seccio."|".$mesa);
		$smarty->assign('data_seccions',$meses);
		$params = array($_SESSION['id_eleccions'], $districte, $seccio, $mesa);
		$resultats = $dades_eleccions->getResultatsMeses($params);

		if(count($resultats)) {
			//Busco el numero de Vots nuls per poder restar alhora de fer el percentatge
			foreach ($resultats as $key => $element) {
				if($element->Sigles=="Nuls")
					$VotsNuls = $element->Vots;

				$denominacio = $element->NomEleccio;
                $logo_eleccions = $element->LogoEleccio;
				$VotsTotals = $element->VotsTotals;
				$AnyEleccio = $element->Any;
				$MaxDistricte = $element->MaxDistricte;
				$CensMesa = $element->CensMesa;
			}

			$actualData = array();
			$preparacio_resultats = array(
			 array('Mesa '.$mesa)
			);

			$primeraVegada = true;
			foreach ($resultats as $key => $element) {
				if ($primeraVegada) {
					$fila = array('Cens', $element->CensMesa, ' ');
					array_push($preparacio_resultats,$fila);
					$fila = array('Vots emesos', $element->VotsTotals, number_format($element->VotsTotals/$element->CensMesa*100,2) ." %");
					array_push($preparacio_resultats,$fila);
					$fila = array('Vots nuls', $VotsNuls, number_format( $VotsNuls/$element->VotsTotals*100,2) ." %");
					array_push($preparacio_resultats,$fila);
					$fila = array('Vots vàlids', $element->VotsTotals - $VotsNuls, number_format( ($element->VotsTotals - $VotsNuls)/$element->VotsTotals*100,2) ." %");
					array_push($preparacio_resultats,$fila);

					array_push($preparacio_resultats,array('Partit', 'Vots', '% s/ mesa'));
					$primeraVegada = false;
				}

				if (!$element->esVotNul) {
					$percentatgeResultats = number_format($element->Vots/($VotsTotals - $VotsNuls)*100,2)." %";
					$fila = array(utf8_encode($element->Sigles), $element->Vots, $percentatgeResultats);
					array_push($preparacio_resultats,$fila);
					$actualData[$element->Sigles] = $element->Vots;
					$colorsArr[$element->Sigles] = $element->Color;
				}
			}
			$smarty->assign('taula1',$preparacio_resultats);

			$colors = implode(",", $colorsArr);
			$chart = $this->setChartSettings("Resultats Mesa ".$mesa, "Total vots", "column3d", $colors, $actualData, "chart-1", "420", "300");
			$smarty->assign('chart1',$chart);

		}
		$eleccions = $dades_eleccions->getEleccionsData();
		$smarty->assign('title','Resultats per mesa '.utf8_encode($denominacio));
        $smarty->assign('subtitle','Dades informatives');
        $smarty->assign('logo_eleccions',$logo_eleccions);
		$smarty->assign('eleccions',$this->filtraArrayUtf8Encode($eleccions));
		$smarty->assign('id_eleccio',$_SESSION['id_eleccions']);

		$smarty->display("eleccions/resultats_meses.tpl");
    }

    function eleccionsResultatsSeccions ($params_values_get) {
    	$_SESSION['id_eleccions'] = $params_values_get["eleccions"] ? $params_values_get["eleccions"] : $_SESSION['id_eleccions'];
        $_SESSION['id_eleccions'] = $_SESSION['id_eleccions'] ? $_SESSION['id_eleccions'] : $this->getUltimaEleccio();
    	$seccions = $params_values_get["seccions"] ? $params_values_get["seccions"] : '01|001';

    	$seccions = explode("|", $seccions);
    	$districte = $seccions[0];
    	$seccio = $seccions[1];

		$smarty = new \webtortosa\Smarty_web();
		$smarty->assign("path_eleccions","http://www2.tortosa.cat/templates");
        $smarty->assign("apartat","eleccions");
		$smarty->assign("path_charts",URL_CHARTS);
		$smarty->display("head.tpl");

        require_once (FOLDER_LANGUAGES."/".$this->languages($params_values_get["lang"])."_header.cfg");
        require_once (FOLDER_LANGUAGES."/eleccions/".$this->languages($params_values_get["lang"])."_eleccions.cfg");
        require_once (FOLDER_LANGUAGES."/".$this->languages($input_lang)."_footer.cfg");

		$dades_eleccions = new \webtortosa\eleccions();
		$params = array($_SESSION['id_eleccions'], $districte, $seccio);

		$seccions = $dades_eleccions->getSeccions($params);
		$smarty->assign('num_seccio',$districte."|".$seccio);
		$smarty->assign('data_seccions',$seccions);
		$resultats = $dades_eleccions->getResultatsSeccions($params);

		if(count($resultats)) {
			//Busco el numero de Vots nuls per poder restar alhora de fer el percentatge
			foreach ($resultats as $key => $element) {
				if(strtoupper($element->Sigles)=="NULS")
					$VotsNuls = $element->Vots;
				$denominacio = $element->NomEleccio;
                $logo_eleccions = $element->LogoEleccio;
				$VotsTotals = $element->VotsTotals;
				$AnyEleccio = $element->Any;
			}

			$actualData = array();
			$preparacio_resultats = array(
			 array('Districte '.$districte.' - Secció '.$seccio)
			);

			$primeraVegada = true;
			foreach ($resultats as $key => $element) {
				if ($primeraVegada) {
					$fila = array('Cens', $element->CensSeccio, ' ');
					array_push($preparacio_resultats,$fila);
					$fila = array('Meses', $element->NumMesas, ' ');
					array_push($preparacio_resultats,$fila);
					$fila = array('Vots emesos', $element->VotsTotals, number_format($element->VotsTotals/$element->CensSeccio*100,2) ." %");
					array_push($preparacio_resultats,$fila);
					$fila = array('Vots nuls', $VotsNuls, number_format( $VotsNuls/$element->VotsTotals*100,2) ." %");
					array_push($preparacio_resultats,$fila);
					$fila = array('Vots vàlids', $element->VotsTotals - $VotsNuls, number_format( ($element->VotsTotals - $VotsNuls)/$element->VotsTotals*100,2) ." %");
					array_push($preparacio_resultats,$fila);

					array_push($preparacio_resultats,array('Partit', 'Vots', '% s/ secció'));
					$primeraVegada = false;
				}

				if (!$element->esVotNul) {
					$percentatgeResultats = number_format($element->Vots/($VotsTotals - $VotsNuls)*100,2)." %";
					$fila = array(utf8_encode($element->Sigles), $element->Vots, $percentatgeResultats);
					array_push($preparacio_resultats,$fila);
					$actualData[$element->Sigles] = $element->Vots;
					$colorsArr[$element->Sigles] = $element->Color;
				}
			}
			$smarty->assign('taula1',$preparacio_resultats);

			$colors = implode(",", $colorsArr);
			$chart = $this->setChartSettings("Resultats Districte ".$districte." - Secció ".$seccio, "Total vots", "column3d", $colors, $actualData, "chart-1", "420", "300");
			$smarty->assign('chart1',$chart);

		}
		$eleccions = $dades_eleccions->getEleccionsData();
		$smarty->assign('title','Resultats per secció '.utf8_encode($denominacio));
        $smarty->assign('subtitle','Dades informatives');
        $smarty->assign('logo_eleccions',$logo_eleccions);
		$smarty->assign('eleccions',$this->filtraArrayUtf8Encode($eleccions));
		$smarty->assign('id_eleccio',$_SESSION['id_eleccions']);

		$smarty->display("eleccions/resultats_seccions.tpl");
    }

    function eleccionsResultatsDistrictes ($params_values_get) {
    	$_SESSION['id_eleccions'] = $params_values_get["eleccions"] ? $params_values_get["eleccions"] : $_SESSION['id_eleccions'];
        $_SESSION['id_eleccions'] = $_SESSION['id_eleccions'] ? $_SESSION['id_eleccions'] : $this->getUltimaEleccio();

		$smarty = new \webtortosa\Smarty_web();
		$smarty->assign("path_eleccions","http://www2.tortosa.cat/templates");
        $smarty->assign("apartat","eleccions");
		$smarty->assign("path_charts",URL_CHARTS);
		$smarty->display("head.tpl");

        require_once (FOLDER_LANGUAGES."/".$this->languages($params_values_get["lang"])."_header.cfg");
        require_once (FOLDER_LANGUAGES."/eleccions/".$this->languages($params_values_get["lang"])."_eleccions.cfg");
        require_once (FOLDER_LANGUAGES."/".$this->languages($input_lang)."_footer.cfg");

		$dades_eleccions = new \webtortosa\eleccions();
		$params = array($_SESSION['id_eleccions']);
		// Consulta resultats districtes
		$resultats = $dades_eleccions->getResultatsDistrictes($params);
		if(count($resultats)) {
			//Busco el numero de Vots nuls per poder restar alhora de fer el percentatge
			$VotsNuls = array();
			foreach ($resultats as $key => $element) {
				if(strtoupper($element->Sigles)=="NULS")
					$VotsNuls[$element->Districte] = $element->Vots;

				$denominacio = $element->NomEleccio;
                $logo_eleccions = $element->LogoEleccio;
				$AnyEleccio = $element->Any;
				$MaxDistricte = $element->MaxDistricte;
			}

			for($districte==1;$districte<=$MaxDistricte;$districte++) {
				$actualData = array();
				$colorsArr = array();
 				$preparacio_resultats = array(
				 array('Districte '.$districte)
				);

				$posaDades = false;
				$currentDistricte = false;

				foreach ($resultats as $key => $element) {
					if((integer)$element->Districte==$districte) {

						if (!$currentDistricte) {
							$fila = array('Cens', $element->CensDistricte, ' ');
							array_push($preparacio_resultats,$fila);
							$fila = array('Meses', $element->NumMesas, ' ');
							array_push($preparacio_resultats,$fila);
							$fila = array('Vots emesos', $element->VotsTotals, number_format($element->VotsTotals/$element->CensDistricte*100,2) ." %");
							array_push($preparacio_resultats,$fila);
							$fila = array('Vots nuls', $VotsNuls[$element->Districte], number_format( $VotsNuls[$element->Districte]/$element->VotsTotals*100,2) ." %");
							array_push($preparacio_resultats,$fila);
							$fila = array('Vots vàlids', $element->VotsTotals - $VotsNuls[$element->Districte], number_format( ($element->VotsTotals - $VotsNuls[$element->Districte])/$element->VotsTotals*100,2) ." %");
							array_push($preparacio_resultats,$fila);

							array_push($preparacio_resultats,array('Partit', 'Vots', '% s/ districte'));
							$currentDistricte = true;
						}


						if (!$element->esVotNul) {
							$percentatgeResultats = number_format($element->Vots/($element->VotsTotals - $VotsNuls[$element->Districte])*100,2)." %";
							$fila = array(utf8_encode($element->Sigles), $element->Vots, $percentatgeResultats);
							array_push($preparacio_resultats,$fila);
							$actualData[$element->Sigles] = $element->Vots;
							$colorsArr[$element->Sigles] = $element->Color;
							$posaDades = true;
						}
					}
				}

				if($posaDades) {
					$smarty->assign('taula'.$districte,$preparacio_resultats);
					$colors = implode(",", $colorsArr);
					$chart = $this->setChartSettings("Resultats Districte ".$districte, "Total vots", "column3d", $colors, $actualData, "chart-".$districte, "420", "300");
					$smarty->assign('chart'.$districte,$chart);
				}
			}

		}
		// Fi Consulta resultats districtes
		$eleccions = $dades_eleccions->getEleccionsData();
		$smarty->assign('title','Resultats per districte '.utf8_encode($denominacio));
        $smarty->assign('subtitle','Dades informatives');
        $smarty->assign('logo_eleccions',$logo_eleccions);
		$smarty->assign('eleccions',$this->filtraArrayUtf8Encode($eleccions));
		$smarty->assign('id_eleccio',$_SESSION['id_eleccions']);

		$smarty->display("eleccions/resultats_districtes.tpl");
    }

    function eleccionsResultatsTotals ($params_values_get) {
    	$_SESSION['id_eleccions'] = $params_values_get["eleccions"] ? $params_values_get["eleccions"] : $_SESSION['id_eleccions'];
        $_SESSION['id_eleccions'] = $_SESSION['id_eleccions'] ? $_SESSION['id_eleccions'] : $this->getUltimaEleccio();

		$smarty = new \webtortosa\Smarty_web();
		$smarty->assign("path_eleccions","http://www2.tortosa.cat/templates");
        $smarty->assign("apartat","eleccions");
		$smarty->assign("path_charts",URL_CHARTS);
		$smarty->display("head.tpl");

        require_once (FOLDER_LANGUAGES."/".$this->languages($params_values_get["lang"])."_header.cfg");
        require_once (FOLDER_LANGUAGES."/eleccions/".$this->languages($params_values_get["lang"])."_eleccions.cfg");
        require_once (FOLDER_LANGUAGES."/".$this->languages($input_lang)."_footer.cfg");

		$dades_eleccions = new \webtortosa\eleccions();

		/* Consulta vots totals */
		$params = array('IdEleccio'=>$_SESSION['id_eleccions']);
		$vots = $dades_eleccions->getEleccionsData($params);
		if(count($vots)>0) {
			$preparacio_vots = array(
				array('', $vots[0]->Any)
			);
			$fila = array('Meses: ',$this->passa_milers(count($dades_eleccions->getMesesData(array('RefEleccio'=>$_SESSION['id_eleccions'])))));
			array_push($preparacio_vots,$fila);
			$fila = array('Cens: ',$this->passa_milers($vots[0]->CensVotants));
			array_push($preparacio_vots,$fila);
			$fila = array('','Vots','% s/Cens', '% s/vots Emesos', '%s/vots Vàlids');
			array_push($preparacio_vots,$fila);

			//Abstenció
			$abstencio_percent_s_cens = number_format(($vots[0]->CensVotants - $vots[0]->VotsEmesos) / $vots[0]->CensVotants * 100, 2);
			$fila = array('Abstenció: ',$this->passa_milers($vots[0]->CensVotants - $vots[0]->VotsEmesos), $abstencio_percent_s_cens . " %", '', '');
			array_push($preparacio_vots,$fila);

			// Vots emesos
			$votants_percent_s_cens = number_format($vots[0]->VotsEmesos / $vots[0]->CensVotants * 100, 2);
			$fila = array('Vots emesos: ',$this->passa_milers($vots[0]->VotsEmesos), $votants_percent_s_cens . " %", '', '');
			array_push($preparacio_vots,$fila);

			//Separació
			$fila = array('','', '', '', '');
			array_push($preparacio_vots,$fila);

			//Vots nuls
			$vots_nuls_percent_s_cens = number_format($vots[0]->VotsNuls / $vots[0]->CensVotants * 100, 2);
			$vots_nuls_percent_s_vemesos = number_format($vots[0]->VotsNuls / $vots[0]->VotsEmesos * 100, 2);
			$fila = array('Vots nuls: ',$this->passa_milers($vots[0]->VotsNuls), $vots_nuls_percent_s_cens . " %", $vots_nuls_percent_s_vemesos . " %", '');
			array_push($preparacio_vots,$fila);

			//Vots vàlids
			$vots_valids_percent_s_cens = number_format($vots[0]->VotsValids / $vots[0]->CensVotants * 100, 2);
			$vots_valids_percent_s_vemesos = number_format($vots[0]->VotsValids / $vots[0]->VotsEmesos * 100, 2);
			$fila = array('Vots vàlids: ',$this->passa_milers($vots[0]->VotsValids), $vots_valids_percent_s_cens . " %", $vots_valids_percent_s_vemesos . " %",'');
			array_push($preparacio_vots,$fila);

			//Separació
			$fila = array('','', '', '', '');
			array_push($preparacio_vots,$fila);

			//Vots en blanc
			$vots_blanc_percent_s_cens = number_format($vots[0]->VotsBlancs / $vots[0]->CensVotants * 100, 2);
			$vots_blanc_percent_s_vemesos = number_format($vots[0]->VotsBlancs / $vots[0]->VotsEmesos * 100, 2);
			$vots_blanc_percent_s_vvalids = number_format($vots[0]->VotsBlancs / $vots[0]->VotsValids * 100, 2);
			$fila = array('Vots en blanc: ',$this->passa_milers($vots[0]->VotsBlancs), $vots_blanc_percent_s_cens . " %", $vots_blanc_percent_s_vemesos . " %", $vots_blanc_percent_s_vvalids . " %");
			array_push($preparacio_vots,$fila);

			//Vots a candidatures
			$vots_candidatures_percent_s_cens = number_format($vots[0]->VotsCandidatures / $vots[0]->CensVotants * 100, 2);
			$vots_candidatures_percent_s_vemesos = number_format($vots[0]->VotsCandidatures / $vots[0]->VotsEmesos * 100, 2);
			$vots_candidatures_percent_s_vvalids = number_format($vots[0]->VotsCandidatures / $vots[0]->VotsValids * 100, 2);
			$fila = array('Vots a candidatures: ',$this->passa_milers($vots[0]->VotsCandidatures), $vots_candidatures_percent_s_cens . " %", $vots_candidatures_percent_s_vemesos . " %", $vots_candidatures_percent_s_vvalids . " %");
			array_push($preparacio_vots,$fila);

			$smarty->assign('taula1',$preparacio_vots);
		}

		// Consulta resultats totals per partit
		$params = array($_SESSION['id_eleccions']);
		$resultats = $dades_eleccions->getResultatsTotals($params);

		if(count($resultats)) {
			$preparacio_participacio = array(
				 array('Candidatura', 'Vots', '% s/Vots Vàlids')
				);
			//Busco el numero de Vots nuls per poder restar alhora de fer el percentatge
			foreach ($resultats as $key => $element) {
				if($element->esVotNul)
					$VotsNuls = $element->Vots;
				$denominacio = $element->NomEleccio;
                $logo_eleccions = $element->LogoEleccio;
				$regidors = $element->NumRegidors;
				$VotsTotals = $element->VotsTotals;
				$RefEleccioComparada = $element->RefEleccioComparada;
				$AnyEleccio = $element->Any;
			}
			$actualData = array();
			$colorsArr = array();
			$HontArr = array();
			foreach ($resultats as $key => $element) {
				//echo $element->Sigles . "->" .$element->esVotNul. " - ". $element->esVotBlanc . "<br>";
				if ((!$element->esVotNul)&&(!$element->esVotBlanc)) {
					$percentatgeResultats = number_format($element->Vots/($VotsTotals - $VotsNuls)*100,2)." %";
					$fila = array(utf8_encode($element->Sigles), $this->passa_milers($element->Vots), $percentatgeResultats);
					array_push($preparacio_participacio,$fila);
					//Dades per al gràfic
					$actualData[strtoupper($element->Sigles) . " - " . $AnyEleccio] = $element->Vots;
					$colorsArr[strtoupper($element->Sigles) . " - " . $AnyEleccio] = $element->Color;
					$HontArr[strtoupper($element->Sigles) . " - " . $AnyEleccio] = $element->Vots;
					$HontArrSenseAny[strtoupper($element->Sigles)] = $element->Vots;
				}
			}
			//echo var_dump($preparacio_participacio);
			$smarty->assign('taula2',$preparacio_participacio);

			$colors = implode(",", $colorsArr);
			$chart = $this->setChartSettings("Resultats ".utf8_encode($denominacio), "Total vots", "column3d", $colors, $actualData, "chart-1", "500", "600");

			$smarty->assign('chart1',$chart);

			//Resultats regidors, en el cas de municipals
			if(($regidors!=NULL)&&($regidors>0)) {
				$actualDataHont = array();
				$resultHont = $this->Hont($HontArrSenseAny, $regidors, $VotsTotals, $VotsNuls);
				foreach ($resultHont as $key => $itemHont) {
					$actualDataHont[utf8_decode($key) . " - " . $AnyEleccio] = utf8_decode($itemHont);
				}

				$chart = $this->setChartSettings("Llei d'Hont", "", "Pie3D", $colors, $actualDataHont, "chart-2", "500", "300", " reg.");
			}
			$smarty->assign('chart2',$chart);

			//Comparativa resultats amb eleccions anteriors
		    if($RefEleccioComparada!="") {
		    	$params = array($RefEleccioComparada);
				// Consulta resultats totals per partit
				$resultats_comparativa = $dades_eleccions->getResultatsTotals($params);
				if(count($resultats_comparativa)) {
					foreach ($resultats_comparativa as $key => $element) {
						if($resultats_comparativa->Sigles=="Nuls")
							$VotsNuls_comparativa = $element->Vots;
						$denominacio_comparativa = $element->NomEleccio;
						$regidors_comparativa = $element->NumRegidors;
						$VotsTotals_comparativa = $element->VotsTotals;
						$AnyEleccio_comparativa = $element->Any;
					}
					$actualData_comparativa = array();
					$colorsArr_comparativa = array();
					$HontArr_comparativa = array();
					foreach ($resultats_comparativa as $key => $element) {
						$percentatgeResultats_comparativa = number_format($element->Vots/($VotsTotals_comparativa - $VotsNuls_comparativa)*100,2)." %";
						$actualData_comparativa[strtoupper($element->Sigles) . " - " . $AnyEleccio_comparativa] = $element->Vots;
						$colorsArr_comparativa[strtoupper($element->Sigles) . " - " . $AnyEleccio_comparativa] = $element->Color;
						$HontArr_comparativa[strtoupper($element->Sigles)] = $element->Vots;
					}
					$actualData = array_merge($actualData, $actualData_comparativa);
					$colorsArrResultComp = array_merge($colorsArr, $colorsArr_comparativa);
					ksort($colorsArrResultComp);
					$colors = implode(",", $colorsArrResultComp);
					ksort($actualData);
				}

				$chart = $this->setChartSettings("Resultats ".utf8_encode($denominacio)." - comparativa ".$denominacio_comparativa, "Total vots", "column3d", $colors, $actualData, "chart-3", "500", "600");
		    }
		    $smarty->assign('chart3',$chart);

		    //Resultats regidors, en el cas de municipals
			if(($regidors_comparativa!=NULL)&&($regidors_comparativa>0)) {
				$actualDataHont_comparativa = array();
				$resultHont_comparativa = $this->Hont($HontArr_comparativa, $regidors_comparativa, $VotsTotals_comparativa, $VotsNuls_comparativa);
				foreach ($resultHont_comparativa as $key => $itemHont) {
					$actualDataHont_comparativa[utf8_decode($key . " - " . $AnyEleccio_comparativa)] = utf8_decode($itemHont);
				}
				$actualDataHont = array_merge($actualDataHont, $actualDataHont_comparativa);
				Ksort($actualDataHont);
				//Tractament del colors
				$colorsHont = array();

				foreach ($actualDataHont as $datakey => $hontData) {
					foreach ($colorsArrResultComp as $colorkey => $colorData) {
						//echo $colorkey . " - " . $datakey . "<br>";
						if($colorkey==$datakey) {
							$colorsHont[$datakey] = $colorData;
						}
					}
				}

				$colors = implode(",", $colorsHont);
				$chart = $this->setChartSettings("Llei d'Hont ".utf8_encode($denominacio)." <br> comparativa ".$denominacio_comparativa, "", "column3d", $colors, $actualDataHont, "chart-4", "500", "300");


			}
			$smarty->assign('chart4',$chart);


		}
		// Fi Consulta resultats totals per partit
		$eleccions = $dades_eleccions->getEleccionsData();
		$smarty->assign('eleccions',$this->filtraArrayUtf8Encode($eleccions));
		$smarty->assign('id_eleccio',$_SESSION['id_eleccions']);
		$smarty->assign('title','Resultats totals '.utf8_encode($denominacio));
        $smarty->assign('subtitle','Dades informatives');
        $smarty->assign('logo_eleccions',$logo_eleccions);
		$smarty->display("eleccions/resultats_totals.tpl");
    }

    function eleccionsParticipacioComparativa ($params_values_get) {
        $_SESSION['id_eleccions'] = $params_values_get["eleccions"] ? $params_values_get["eleccions"] : $_SESSION['id_eleccions'];
        $_SESSION['id_eleccions'] = $_SESSION['id_eleccions'] ? $_SESSION['id_eleccions'] : $this->getUltimaEleccio();

        $smarty = new \webtortosa\Smarty_web();

        require_once (FOLDER_LANGUAGES."/".$this->languages($params_values_get["lang"])."_header.cfg");
        require_once (FOLDER_LANGUAGES."/eleccions/".$this->languages($params_values_get["lang"])."_eleccions.cfg");
        require_once (FOLDER_LANGUAGES."/".$this->languages($input_lang)."_footer.cfg");

        $smarty->assign("path_apartat","http://www2.tortosa.cat/templates");
        $smarty->assign("apartat","eleccions");
        $smarty->display("head.tpl");

        $dades_eleccions = new \webtortosa\eleccions();

        $params = array('IdEleccio'=>$_SESSION['id_eleccions']);

        $eleccions = $dades_eleccions->getEleccionsData($params);

        if(count($eleccions)) {
            if($params_values_get["eleccions_a_comparar"])
                $EleccioComparativa = $params_values_get["eleccions_a_comparar"];
            else
                $EleccioComparativa = $eleccions[0]->RefEleccioComparada;
            $Any = $eleccions[0]->Any;
            $denominacio = $eleccions[0]->Denominacio;
            $Hora1participacio = $eleccions[0]->Hora1Participacio;
            $Hora2participacio = $eleccions[0]->Hora2Participacio;
            $Hora3participacio = $eleccions[0]->Hora3Participacio;
            $logo_eleccions = $eleccions[0]->LogoEleccio;
            $params = array('IdEleccio'=>$EleccioComparativa);
            $eleccionsComparades = $dades_eleccions->getEleccionsData($params);
            if(count($eleccionsComparades)>0) {
                $DenominacioComparativa = $eleccionsComparades[0]->Denominacio;
            }
        }


        $params = array($_SESSION['id_eleccions'], $EleccioComparativa);
        $comparativa = $dades_eleccions->getComparativaParticipacioMeses($params);

        $actualData1Participacio = array();
        $actualData2Participacio = array();
        $actualData3Participacio = array();
        $Meses = array();
        $actualData1ParticipacioComparativa = array();
        $actualData2ParticipacioComparativa = array();
        $actualData3ParticipacioComparativa = array();
        $actualDataPercent = array();
        foreach ($comparativa as $key => $element) {
            $Meses[$key] = $element->IdMesa;
            if($element->RefEleccio == $_SESSION['id_eleccions']) {
                $nameSerie = $element->Any;
                $actualData1Participacio[$element->IdMesa] = $element->Vots1Participacio;
                $actualData2Participacio[$element->IdMesa] = $element->Vots2Participacio;
                $actualData3Participacio[$element->IdMesa] = $element->Vots3Participacio;
                //$actualDataPercent[$element->IdMesa] = number_format($element->VotsFinParticipacio / $element->CensVotants * 100,2);
            }

            if($element->RefEleccio == $EleccioComparativa) {
                $nameSerieComparativa = $element->Any;
                $actualData1ParticipacioComparativa[$element->IdMesa] = $element->Vots1Participacio;
                $actualData2ParticipacioComparativa[$element->IdMesa] = $element->Vots2Participacio;
                $actualData3ParticipacioComparativa[$element->IdMesa] = $element->Vots3Participacio;
                //$actualDataComparativaPercent[$element->IdMesa] = number_format($element->VotsFinParticipacio / $element->CensVotants * 100,2);
            }
        }
        $actualDataData1Participacio = array();
        $actualDataData2Participacio = array();
        $actualDataData3Participacio = array();
        //$actualDataDataPercent = array();
        foreach($Meses as $mesa) {
            $actualDataData1Participacio[$mesa][0] = $actualData1Participacio[$mesa];
            $actualDataData1Participacio[$mesa][1] = $actualData1ParticipacioComparativa[$mesa];
            $actualDataData2Participacio[$mesa][0] = $actualData2Participacio[$mesa];
            $actualDataData2Participacio[$mesa][1] = $actualData2ParticipacioComparativa[$mesa];
            $actualDataData3Participacio[$mesa][0] = $actualData3Participacio[$mesa];
            $actualDataData3Participacio[$mesa][1] = $actualData3ParticipacioComparativa[$mesa];
            //$actualDataDataPercent[$mesa][0] = $actualDataPercent[$mesa];
            //$actualDataDataPercent[$mesa][1] = $actualDataComparativaPercent[$mesa];
        }
        ksort($actualDataData1Participacio);
        ksort($actualDataData2Participacio);
        ksort($actualDataData3Participacio);
        //ksort($actualDataDataPercent);
        if((strpos($_SESSION['id_eleccions'], "JES"))||(strpos($_SESSION['id_eleccions'], "CAM"))||(strpos($_SESSION['id_eleccions'], "BIT"))||(strpos($_SESSION['id_eleccions'], "XBI"))||(strpos($_SESSION['id_eleccions'], "XCA"))||(strpos($_SESSION['id_eleccions'], "XJE"))) {
            $height = 400;
        }
        else {
            $height = 1300;
        }
        $chart = $this->setChartSettingsObjectSeries($actualDataData1Participacio, $nameSerie, $nameSerieComparativa, "MSBar2D", "", "", "400", $height, "");
        // Títols per al Chart1
        $smarty->assign('CaptionChart1',"Participació ".utf8_encode($denominacio));
        $smarty->assign('SubCaptionChart1',"Comparativa de participació ".$Hora1participacio." h");
        // Chart1
        $smarty->assign('chart1',$chart);

        $chart = $this->setChartSettingsObjectSeries($actualDataData2Participacio, $nameSerie, $nameSerieComparativa, "MSBar2D", "", "", "400", $height, "");
        // Títols per al Chart2
        $smarty->assign('CaptionChart2',"Participació ".utf8_encode($denominacio));
        $smarty->assign('SubCaptionChart2',"Comparativa de participació ".$Hora2participacio." h");
        // Chart2
        $smarty->assign('chart2',$chart);

        $chart = $this->setChartSettingsObjectSeries($actualDataData3Participacio, $nameSerie, $nameSerieComparativa, "MSBar2D", "", "", "400", $height, "");
        // Títols per al Chart3
        $smarty->assign('CaptionChart3',"Participació ".utf8_encode($denominacio));
        $smarty->assign('SubCaptionChart3',"Comparativa de participació ".$Hora3participacio." h");
        // Chart2
        $smarty->assign('chart3',$chart);

        $eleccions = $dades_eleccions->getEleccionsData();
        $smarty->assign('eleccions',$this->filtraArrayUtf8Encode($eleccions));
        $smarty->assign('id_eleccio',$_SESSION['id_eleccions']);
        $smarty->assign('title','Comparativa '.utf8_encode($denominacio) . " - " . utf8_encode($DenominacioComparativa));
        $smarty->assign('subtitle','Dades informatives');
        $smarty->assign('logo_eleccions',$logo_eleccions);
        $smarty->assign('idelecciocomparativa',$EleccioComparativa);
        $smarty->display("eleccions/participacio_comparativa.tpl");
    }

    function eleccionsParticipacioMeses ($params_values_get) {
     	$_SESSION['id_eleccions'] = $params_values_get["eleccions"] ? $params_values_get["eleccions"] : $_SESSION['id_eleccions'];
        $_SESSION['id_eleccions'] = $_SESSION['id_eleccions'] ? $_SESSION['id_eleccions'] : $this->getUltimaEleccio();

    	$smarty = new \webtortosa\Smarty_web();
		$smarty->assign("path_eleccions","http://www2.tortosa.cat/templates");
        $smarty->assign("apartat","eleccions");
		$smarty->assign("path_charts",URL_CHARTS);
		$smarty->display("head.tpl");

        require_once (FOLDER_LANGUAGES."/".$this->languages($params_values_get["lang"])."_header.cfg");
        require_once (FOLDER_LANGUAGES."/eleccions/".$this->languages($params_values_get["lang"])."_eleccions.cfg");
        require_once (FOLDER_LANGUAGES."/".$this->languages($input_lang)."_footer.cfg");

		$dades_eleccions = new \webtortosa\eleccions();

		$params = array('IdEleccio'=>$_SESSION['id_eleccions']);
		$dataEleccions = $dades_eleccions->getEleccionsData($params);
		if(count($dataEleccions)>0) {
			$denominacio = $dataEleccions[0]->Denominacio;
            $logo_eleccions = $dataEleccions[0]->LogoEleccio;
		}

		/* Consulta participació per meses */
		$params = array($_SESSION['id_eleccions']);
		$participacio = $dades_eleccions->getParticipacioMesesFront($params);

		if(count($participacio)>0) {

			$preparacio_participacio = array(
				 array(
				 	'Col·legi',
				 	'Núm. Mesa',
				 	'Cens Mesa',
				 	'Part.'.$participacio[0]->Hora1Participacio."h",
				 	'Part.'.$participacio[0]->Hora2Participacio."h",
				 	'Part.'.$participacio[0]->Hora3Participacio."h",
				 	'Part.'.$participacio[0]->Hora1Participacio."h % s/ Cens",
				 	'Part.'.$participacio[0]->Hora2Participacio."h % s/ Cens",
				 	'Part.'.$participacio[0]->Hora3Participacio."h % s/ Cens")
				);

			foreach ($participacio as $key => $element) {
				$percent1Participacio = number_format($element->Vots1Participacio/$element->CensVotants*100,2)." %";
				$percent2Participacio = number_format($element->Vots2Participacio/$element->CensVotants*100,2)." %";
				$percent3Participacio = number_format($element->Vots3Participacio/$element->CensVotants*100,2)." %";
				$fila = array(
					utf8_encode($element->NomColegi),
					$element->IdMesa,
					$element->CensVotants,
					$element->Vots1Participacio,
					$element->Vots2Participacio,
					$element->VotsFinParticipacio,
					$percent1Participacio,
					$percent2Participacio,
					$percent3Participacio);
				array_push($preparacio_participacio,$fila);
			}
			//echo var_dump($preparacio_participacio);
			$smarty->assign('taula1',$preparacio_participacio);

		}

		$eleccions = $dades_eleccions->getEleccionsData();
		$smarty->assign('title','Participació per mesa '.utf8_encode($denominacio));
        $smarty->assign('subtitle','Dades informatives');
        $smarty->assign('logo_eleccions',$logo_eleccions);
		$smarty->assign('eleccions',$this->filtraArrayUtf8Encode($eleccions));
		$smarty->assign('id_eleccio',$_SESSION['id_eleccions']);

		$smarty->display("eleccions/participacio_meses.tpl");
    }

    function eleccionsParticipacioSeccions ($params_values_get) {
     	$_SESSION['id_eleccions'] = $params_values_get["eleccions"] ? $params_values_get["eleccions"] : $_SESSION['id_eleccions'];
        $_SESSION['id_eleccions'] = $_SESSION['id_eleccions'] ? $_SESSION['id_eleccions'] : $this->getUltimaEleccio();

    	$smarty = new \webtortosa\Smarty_web();
		$smarty->assign("path_eleccions","http://www2.tortosa.cat/templates");
        $smarty->assign("apartat","eleccions");
		$smarty->assign("path_charts",URL_CHARTS);
		$smarty->display("head.tpl");

        require_once (FOLDER_LANGUAGES."/".$this->languages($params_values_get["lang"])."_header.cfg");
        require_once (FOLDER_LANGUAGES."/eleccions/".$this->languages($params_values_get["lang"])."_eleccions.cfg");
        require_once (FOLDER_LANGUAGES."/".$this->languages($input_lang)."_footer.cfg");

		$dades_eleccions = new \webtortosa\eleccions();

		$params = array('IdEleccio'=>$_SESSION['id_eleccions']);
		$dataEleccions = $dades_eleccions->getEleccionsData($params);
		if(count($dataEleccions)>0) {
			$denominacio = $dataEleccions[0]->Denominacio;
            $logo_eleccions = $dataEleccions[0]->LogoEleccio;
		}

		/* Consulta participació per seccions */
		$params = array($_SESSION['id_eleccions']);
		$participacio = $dades_eleccions->getParticipacioSeccions($params);
		if(count($participacio)>0) {
			$preparacio_participacio = array(
				 array(
				 	'Districte',
				 	'Secció',
				 	'Cens',
				 	'Part.'.$participacio[0]->Hora1Participacio."h",
				 	'Part.'.$participacio[0]->Hora2Participacio."h",
				 	'Part.'.$participacio[0]->Hora3Participacio."h",
				 	'Part.'.$participacio[0]->Hora1Participacio."h % s/ Cens",
				 	'Part.'.$participacio[0]->Hora2Participacio."h % s/ Cens",
				 	'Part.'.$participacio[0]->Hora3Participacio."h % s/ Cens"
				 	)
				);
			foreach ($participacio as $key => $element) {
				$fila = array(
					$element->Districte,
					$element->Seccio,
					$element->CensVotants,
					$element->Vots1Participacio,
					$element->Vots2Participacio,
					$element->Vots3Participacio,
					number_format($element->Vots1Participacio / $element->CensVotants * 100,2). " %",
					number_format($element->Vots2Participacio / $element->CensVotants * 100,2). " %",
					number_format($element->Vots3Participacio / $element->CensVotants * 100,2). " %"
				);
				array_push($preparacio_participacio,$fila);
			}
			//echo var_dump($preparacio_participacio);
			$smarty->assign('taula1',$preparacio_participacio);
		}
		$eleccions = $dades_eleccions->getEleccionsData();
		$smarty->assign('title','Participació per secció '.utf8_encode($denominacio));
        $smarty->assign('subtitle','Dades informatives');
        $smarty->assign('logo_eleccions',$logo_eleccions);
        $smarty->assign('eleccions',$this->filtraArrayUtf8Encode($eleccions));
		$smarty->assign('id_eleccio',$_SESSION['id_eleccions']);

		$smarty->display("eleccions/participacio_seccions.tpl");
    }

    function eleccionsParticipacioDistrictes ($params_values_get) {
    	$_SESSION['id_eleccions'] = $params_values_get["eleccions"] ? $params_values_get["eleccions"] : $_SESSION['id_eleccions'];
        $_SESSION['id_eleccions'] = $_SESSION['id_eleccions'] ? $_SESSION['id_eleccions'] : $this->getUltimaEleccio();

    	$smarty = new \webtortosa\Smarty_web();
		$smarty->assign("path_eleccions","http://www2.tortosa.cat/templates");
        $smarty->assign("apartat","eleccions");
		$smarty->assign("path_charts",URL_CHARTS);
		$smarty->display("head.tpl");

        require_once (FOLDER_LANGUAGES."/".$this->languages($params_values_get["lang"])."_header.cfg");
        require_once (FOLDER_LANGUAGES."/eleccions/".$this->languages($params_values_get["lang"])."_eleccions.cfg");
        require_once (FOLDER_LANGUAGES."/".$this->languages($input_lang)."_footer.cfg");

		$dades_eleccions = new \webtortosa\eleccions();


		$params = array('IdEleccio'=>$_SESSION['id_eleccions']);
		$dataEleccions = $dades_eleccions->getEleccionsData($params);
		if(count($dataEleccions)>0) {
			$denominacio = $dataEleccions[0]->Denominacio;
            $logo_eleccions = $dataEleccions[0]->LogoEleccio;
		}

		/* Consulta participació per districtes */
		$params = array($_SESSION['id_eleccions']);
		$participacio = $dades_eleccions->getParticipacioDistrictes($params);
		if(count($participacio)>0) {
			//Inserció del gràfic
			/* Gràfic columnes per a participació per districtes */
			$actualData = array();
			$actualDataPercent = array();
			foreach ($participacio as $key => $element) {
				$actualData['Dist '.$element->Districte] = $element->VotsFinParticipacio;
				$actualDataPercent['Dist '.$element->Districte] = number_format($element->VotsFinParticipacio / $element->CensVotants*100, 2);
				//echo $element->IdMesa. " - " .$element->Any. " - " .$element->NomColegi." -> " . $element->VotsFinParticipacio . "<br>";
			}

		    $chart = $this->setChartSettings("Participació ".utf8_encode($denominacio), "Participació per districtes", "column2d", "", $actualData, "chart-1", "500", "300");
		    $smarty->assign('chart1',$chart);

		    $chart = $this->setChartSettings("Participació ".utf8_encode($denominacio), "Participació per districtes en %", "column2d", "", $actualDataPercent, "chart-2", "500", "300", " %");
		    $smarty->assign('chart2',$chart);
		    /* Fi Gràfic columnes per a participació per districtes */


			foreach ($participacio as $key => $element) {
				$preparacio_participacio = array(
				 array('Districte '.$element->Districte)
				);

				$fila = array('Meses', $element->NumMeses);
				array_push($preparacio_participacio,$fila);
				$fila = array('Cens', $this->passa_milers($element->CensVotants));
				array_push($preparacio_participacio,$fila);
				$fila = array('Vots Emesos', $element->Hora1Participacio."h", $this->passa_milers($element->Vots1Participacio), number_format($element->Vots1Participacio/$element->CensVotants*100, 2). " % s/ Cens");
				array_push($preparacio_participacio,$fila);
				$fila = array('Vots Emesos', $element->Hora2Participacio."h", $this->passa_milers($element->Vots2Participacio), number_format($element->Vots2Participacio/$element->CensVotants*100, 2). " % s/ Cens");
				array_push($preparacio_participacio,$fila);
				$fila = array('Vots Emesos', $element->Hora3Participacio."h", $this->passa_milers($element->Vots3Participacio), number_format($element->Vots3Participacio/$element->CensVotants*100, 2). " % s/ Cens");
				array_push($preparacio_participacio,$fila);

				$smarty->assign('taula'.(integer)$element->Districte,$preparacio_participacio);
			}
		}
		$eleccions = $dades_eleccions->getEleccionsData();
		$smarty->assign('title','Participació per districte '.utf8_encode($denominacio));
        $smarty->assign('subtitle','Dades informatives');
        $smarty->assign('logo_eleccions',$logo_eleccions);
		$smarty->assign('eleccions',$this->filtraArrayUtf8Encode($eleccions));
		$smarty->assign('id_eleccio',$_SESSION['id_eleccions']);
		$smarty->display("eleccions/participacio_districtes.tpl");
    }

    function eleccionsParticipacioTotals ($params_values_get) {
        $_SESSION['id_eleccions'] = $params_values_get["eleccions"] ? $params_values_get["eleccions"] : $_SESSION['id_eleccions'];
        $_SESSION['id_eleccions'] = $_SESSION['id_eleccions'] ? $_SESSION['id_eleccions'] : $this->getUltimaEleccio();

        $smarty = new \webtortosa\Smarty_web();

        require_once (FOLDER_LANGUAGES."/".$this->languages($params_values_get["lang"])."_header.cfg");
        require_once (FOLDER_LANGUAGES."/eleccions/".$this->languages($params_values_get["lang"])."_eleccions.cfg");
        require_once (FOLDER_LANGUAGES."/".$this->languages($input_lang)."_footer.cfg");
        $smarty->assign("path_apartat","http://www2.tortosa.cat/templates");
        $smarty->assign("apartat","eleccions");
        $smarty->display("head.tpl");

        $dades_eleccions = new \webtortosa\eleccions();

        $params = array('RefEleccio'=>$_SESSION['id_eleccions']);


        /* Consulta participació per hores */
        $participacio = $dades_eleccions->getParticipacio($params);



        if(count($participacio)>0) {
            $preparacio_participacio = array(
                array('Hora', 'Votants', 'Cens', '% s/Cens', 'Número Meses')
            );
            $participacio_percent = number_format($participacio[0]->Totals1participacio/$participacio[0]->CensVotants*100,2) . " %";
            $fila = array($participacio[0]->Hora1Participacio, $this->passa_milers($participacio[0]->Totals1participacio), $this->passa_milers($participacio[0]->CensVotants), $participacio_percent, $participacio[0]->NumMeses);
            array_push($preparacio_participacio,$fila);
            $participacio_percent = number_format($participacio[0]->Totals2participacio/$participacio[0]->CensVotants*100, 2) . " %";
            $fila = array($participacio[0]->Hora2Participacio, $this->passa_milers($participacio[0]->Totals2participacio), $this->passa_milers($participacio[0]->CensVotants), $participacio_percent, $participacio[0]->NumMeses);
            array_push($preparacio_participacio,$fila);
            $participacio_percent = number_format($participacio[0]->Totals3participacio/$participacio[0]->CensVotants*100, 2) . " %";
            $fila = array($participacio[0]->Hora3Participacio, $this->passa_milers($participacio[0]->Totals3participacio), $this->passa_milers($participacio[0]->CensVotants), $participacio_percent, $participacio[0]->NumMeses);
            array_push($preparacio_participacio,$fila);
            $NumMeses = $participacio[0]->NumMeses;
            $smarty->assign('taula1',$preparacio_participacio);
        }

        /* Fi Consulta participació per hores */

        /* Consulta vots totals */

        $params = array('IdEleccio'=>$_SESSION['id_eleccions']);
        $vots = $dades_eleccions->getEleccionsData($params);
        if(count($vots)>0) {
            $vots_valids_chart = $vots[0]->VotsValids;
            $abstencio_chart = $vots[0]->CensVotants - $vots[0]->VotsValids;
            $denominacio = $vots[0]->Denominacio;
            $logo_eleccions = $vots[0]->LogoEleccio;
            $EleccioComparativa = $vots[0]->RefEleccioComparada;

            $actualData = array(
                "Vots" => $vots_valids_chart,
                "Abstenció" => $abstencio_chart
            );
            $chart = $this->setChartSettingsObject($actualData, "Pie3D", "", "", "300", "300");
            // Títols per al Chart1
            $smarty->assign('CaptionChart1',"Participació ".utf8_encode($denominacio));
            $smarty->assign('SubCaptionChart1',"");
            // Chart1
            $smarty->assign('chart1',$chart);
        }

        $params = array($_SESSION['id_eleccions'], $EleccioComparativa);
        $comparativa = $dades_eleccions->getComparativaParticipacioMeses($params);
        $actualData = array();
        $Meses = array();
        $actualDataComparativa = array();
        $actualDataPercent = array();
        foreach ($comparativa as $key => $element) {
            $Meses[$key] = $element->IdMesa;
            if($element->RefEleccio == $_SESSION['id_eleccions']) {
                $nameSerie = $element->Any;
                $actualData[$element->IdMesa] = $element->VotsFinParticipacio;
                $actualDataPercent[$element->IdMesa] = number_format($element->VotsFinParticipacio / $element->CensVotants * 100,2);
            }

            if($element->RefEleccio == $EleccioComparativa) {
                $nameSerieComparativa = $element->Any;
                $actualDataComparativa[$element->IdMesa] = $element->VotsFinParticipacio;
                $actualDataComparativaPercent[$element->IdMesa] = number_format($element->VotsFinParticipacio / $element->CensVotants * 100,2);
            }
        }
        $actualDataData = array();
        $actualDataDataPercent = array();
        foreach($Meses as $mesa) {
            $actualDataData[$mesa][0] = $actualData[$mesa];
            $actualDataData[$mesa][1] = $actualDataComparativa[$mesa];
            $actualDataDataPercent[$mesa][0] = $actualDataPercent[$mesa];
            $actualDataDataPercent[$mesa][1] = $actualDataComparativaPercent[$mesa];
        }
        ksort($actualDataData);
        ksort($actualDataDataPercent);
        if((strpos($_SESSION['id_eleccions'], "JES"))||(strpos($_SESSION['id_eleccions'], "CAM"))||(strpos($_SESSION['id_eleccions'], "BIT"))||(strpos($_SESSION['id_eleccions'], "XBI"))||(strpos($_SESSION['id_eleccions'], "XCA"))||(strpos($_SESSION['id_eleccions'], "XJE"))) {
            $height = 400;
        }
        else {
            $height = 1300;
        }
        $chart = $this->setChartSettingsObjectSeries($actualDataData, $nameSerie, $nameSerieComparativa, "MSBar2D", "", "", "400", $height, "");
        // Títols per al Chart2
        $smarty->assign('CaptionChart2',"Participació ".utf8_encode($denominacio));
        $smarty->assign('SubCaptionChart2',"Comparativa de participació");
        // Chart2
        $smarty->assign('chart2',$chart);

        $chart = $this->setChartSettingsObjectSeries($actualDataDataPercent, $nameSerie, $nameSerieComparativa, "MSBar2D", "", "", "400", $height, " %");
        // Títols per al Chart3
        $smarty->assign('CaptionChart3',"Participació ".utf8_encode($denominacio). " en %");
        $smarty->assign('SubCaptionChart3',"Comparativa de participació");
        // Chart3
        $smarty->assign('chart3',$chart);


        /* Fi Consulta vots totals */
        $eleccions = $dades_eleccions->getEleccionsData();
        $smarty->assign('title','Participació '.utf8_encode($denominacio));
        $smarty->assign('subtitle','Dades informatives');
        $smarty->assign('logo_eleccions',$logo_eleccions);
        $smarty->assign('eleccions',$this->filtraArrayUtf8Encode($eleccions));
        $smarty->assign('id_eleccio',$_SESSION['id_eleccions']);
        $smarty->display("eleccions/participacio_totals2.tpl");
    }

    function Hont($resultats, $regidors, $VotsTotals, $VotsNuls) {
    	$HontArr = array();
    	$siglesHont = array();
    	$dataHont = array();
    	$resultHont = array();
    	foreach ($resultats as $key => $resultat) {
    		if(round($resultat/($VotsTotals - $VotsNuls)*100,2)>=5) {
    			for($i=1;$i<=$regidors;$i++) {
		    		array_push($HontArr,array($key, $resultat/$i));
		    	}
    		}
    	}

    	//Recorro per a fer l'ordre
    	foreach ($HontArr as $key => $hont) {
    		$aux[$key] = $hont[1];
    	}
    	array_multisort($aux, SORT_DESC, $HontArr);
    	foreach ($HontArr as $key => $hont) {
    		if($key<$regidors) {
    			$siglesHont[$hont[0]] = $hont[1];
				$dataHont[$key] = array($hont[0], $hont[1]);
    		}
    	}
    	$keyArr = array_keys($siglesHont);
    	$keyArr = array_unique($keyArr);

    	foreach ($siglesHont as $key => $siglaHont) {
    		$resultHont[$key] = 0;
    		foreach ($dataHont as $dataintem) {
    			if($key==$dataintem[0]) {
    				$resultHont[$key] = $resultHont[$key] + 1;
    			}
    		}
    	}
    	return $resultHont;
    }

    function getUltimaEleccio() {
        $dades_eleccions = new \webtortosa\eleccions();
        $items = $dades_eleccions->getLastEleccio();
        if(count($items)>0)
            return $items[0]->IdEleccio;
        else
            return false;
    }

    function setChartSettingsObjectSeries($dataData = NULL, $nameCategory1 = NULL,  $nameCategory2 = NULL, $type="MSBar2D", $caption, $subcaption, $width, $height, $sufix="" ) {
        require_once ($_SERVER['DOCUMENT_ROOT']."/chart/Includes/FusionCharts_Gen.php");
        # Create column 3d chart object
        $FC = new \FusionCharts($type, $width, $height);

        # Set Relative Path of swf file.
        $FC->setSwfPath("/chart/Charts/");

        # Store Chart attributes in a variable
        $strParam="caption=".$caption.";subcaption=".$subcaption.";xAxisName=Meses;yAxisName=Anys;numberPrefix=  ;numberSuffix=".$sufix.";decimalPrecision=0;formatNumberScale=0;valuePadding=5";

        #  Set Chart attributes
        $FC->setChartParams($strParam);

        # Add category names
        foreach($dataData as $key=>$itemCategory) {
            $FC->addCategory($key);
        }

        if(count($dataData)>0) {
            # Create a new dataset
            $FC->addDataset($nameCategory1,"color=#1EC436");
            # Add chart values for the above dataset
            foreach($dataData as $key=>$itemSerie) {
                $FC->addChartData($itemSerie[0]);
            }
        }

        if(count($dataData)>0) {
            # Create a new dataset
            $FC->addDataset($nameCategory2, "color=#DD4D22");
            # Add chart values for the above dataset
            foreach($dataData as $key=>$itemSerie) {
                $FC->addChartData($itemSerie[1]);
            }
        }

        # Render  Chart
        return $FC->renderChart(false, false);
    }

    function setChartSettingsObject($dataData = NULL, $type="MSBar2D", $caption, $subcaption, $width, $height ) {
        require_once ($_SERVER['DOCUMENT_ROOT']."/chart/Includes/FusionCharts_Gen.php");
        # Create column 3d chart object
        $FC = new \FusionCharts($type,$width,$height);

        # Set Relative Path of swf file.
        $FC->setSwfPath("/chart/Charts/");

        # Store Chart attributes in a variable
        $strParam="caption=".$caption.";subcaption=".$subcaption.";xAxisName=Meses;yAxisName=Anys;numberPrefix=;decimalPrecision=0;paletteColors=#1EC436,#DD4D22,#289AF7,#D9E639,#EF3030;formatNumberScale=0; thousandSeparator=.; showNames=1; animation=0";

        #  Set Chart attributes
        $FC->setChartParams($strParam);

        # Add category names
        foreach($dataData as $key=>$itemCategory) {
            $FC->addCategory($key);
        }

        if(count($dataData)>0) {
            # Add chart values for the above dataset
            foreach($dataData as $key=>$item) {
                $FC->addChartData($item,"name=".$key);
            }
        }

        # Render  Chart
        return $FC->renderChart(false, false);
    }

    function setChartSettings($title = "", $subtitle = "", $type = "", $colours = "#1EC436,#DD4D22,#289AF7,#D9E639,#EF3030", $actualData = NULL, $container = "", $width, $height, $sufix = null) {
    	$arrData = array(
	        "chart" => array(
	            "caption" => $title,
	            "subCaption" => $subtitle,
	            "paletteColors" => $colours,
	            "bgColor" => "#ffffff",
	            "showBorder" => "0",
	            "use3DLighting" => "0",
	            "showShadow" => "0",
	            "enableSmartLabels" => "0",
	            "startingAngle" => "0",
	            "showPercentValues" => "0",
	            "showPercentInTooltip" => "0",
	            "formatNumber" => "1",
	            "decimals" => "2",
	            "captionFontSize" => "14",
	            "subcaptionFontSize" => "14",
	            "subcaptionFontBold" => "0",
	            "toolTipColor" => "#000000",
	            "toolTipBorderThickness" => "0",
	            "toolTipBgColor" => "#ffffff",
	            "toolTipBgAlpha" => "80",
	            "toolTipBorderRadius" => "2",
	            "toolTipPadding" => "5",
	            "showHoverEffect" => "1",
	            "showLegend" => "1",
	            "legendBgColor" => "#ffffff",
	            "legendBorderAlpha" => "0",
	            "legendShadow" => "0",
	            "legendItemFontSize" => "10",
	            "legendItemFontColor" => "#666666",
	            "useDataPlotColorForLabels" => "1",
	            "labelFontSize" => "8",
	            "labelFontColor" => "#000000",
	            "formatNumberScale" =>"0",
	            "thousandSeparator" => ".",
	            "numberSuffix" => $sufix
	        )
	    );

		// Now we need to convert it to FusionCharts consumable form.
	    // Our data will be an array. So, let's declare it.
	    $arrData['data'] = array();

	    // Loop over actual data and insert it in data array.
	    foreach ($actualData as $key => $value) {
	        array_push($arrData['data'], array(
	                'label' => utf8_encode($key),
	                'value' => utf8_encode($value)
	            )
	        );
	    }

	    // JSON Encode the arrData
	    $jsonEncodedData = json_encode($arrData);

	    // Create pieChart object
	    $pieChart = new \webtortosa\FusionCharts($type, $container."_chart" , $width, $height, $container, "json", $jsonEncodedData);
	    // Render the chart
	    return $pieChart->render();
    }

    function memoria_democratica2($input_lang, $num_pag) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        $smarty->assign('url_ca','/memoria-democratica/');
        $smarty->assign('url_es','/memoria-democratica/index.php?lang=es');

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");
        require_once (FOLDER_LANGUAGES."/memoria-democratica/".$lang."_memoria_democratica.cfg");

        $smarty->display("memoria-democratica2/memoria_democratica.tpl");
    }

    function getListMemoriaData2($params_values_get, $params_values_post, $input_lang) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();


        $smarty->assign('lang',$lang);
        $smarty->assign('url_ca','/memoria-democratica/base-dades.php');
        $smarty->assign('url_es','/memoria-democratica/base-dades.php?lang=es');

        require_once (FOLDER_LANGUAGES."/memoria-democratica/".$lang."_memoria_democratica.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $md = new \webtortosa\memoria_democratica_database();

        $params = array();

        if($params_values_post) {
            $_SESSION['params_values_post'] = $params_values_post;
        }
        else {
            $params_values_post = $_SESSION['params_values_post'];
        }
        $params['NOM'] = $this->treure_accents($params_values_post['nom']);
        $params['CATEGORIA'] = $this->treure_accents($params_values_post['categoria']);

        //$items = $this->filtraArrayUtf8Encode($md->getDataMemorial($params));
        $items = $md->getDataMemorial($params);
        // Setup Pagination vars
        $total = count($items);
        $page = $params_values_get['page'];
        $start = $page ? ($page - 1) * ITEMS_LIMIT : 0;

        if($page==0)
            $page = 1;
        $prev = $page - 1; //previous page is current page - 1
        $next = $page + 1; //next page is current page + 1
        $lastpage = ceil($total/ITEMS_LIMIT); //lastpage.

        $items = $md->getDataMemorial($params, $start, ITEMS_LIMIT);
        $params = array();
        $categories = $md->getDataCategories($params);

        $smarty->assign("lastpage",$lastpage);
        $smarty->assign("page",$page);
        $smarty->assign("items",$items);
        $smarty->assign("categories",$categories);
        $smarty->display("memoria-democratica2/list.tpl");
    }

    function getFileItem2($params_values_get, $params_values_post, $input_lang) {
        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/memoria-democratica/".$lang."_memoria_democratica.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");
        $md = new \webtortosa\memoria_democratica_database();
        $params = array();
        $params['ID'] = $params_values_get['id'];
        //$items = $this->filtraArrayUtf8Encode($md->getDataMemorial($params));
        $items = $md->getDataMemorial($params);
        if(count($items)>0) {
            $smarty->assign("item",$items[0]);
        }
        $params = array();
        //$categories = $this->filtraArrayUtf8Encode($md->getDataCategories($params));
        $categories = $md->getDataCategories($params);
        $smarty->assign("categories",$categories);
        $smarty->assign("page",$params_values_get['page']);
        $smarty->display("memoria-democratica2/item.tpl");

    }

    /******************************************
    ********************************************
    ********************************************
      FI Funcions per a l'apartat de ELECCIONS
    ********************************************
    ********************************************************************************************************************
    ********************************************/


    /******************************************
     ********************************************
     ********************************************
        Funcions Memòria Democràtica
     ********************************************
     ********************************************
     ********************************************/

    function getMemoriaData($params_values_get, $params_values_post) {
        $smarty = new \webtortosa\Smarty_web();
        require_once (FOLDER_LANGUAGES."/".$this->languages($params_values_get["lang"])."_memoria-democratica.cfg");
        $smarty->assign("path_apartat","http://www2.tortosa.cat/templates");
        $smarty->assign("apartat","memoria-democratica");
        $smarty->display("head_web_antiga2.tpl");
        $md = new \webtortosa\memoria_democratica_database();
        $params = array();
        $categories = $this->filtraArrayUtf8Encode($md->getDataCategories($params));
        $categories = $md->getDataCategories($params);
        $smarty->assign("categories",$categories);
        $smarty->display("memoria-democratica/principal.tpl");
    }

    function getListMemoriaData($params_values_get, $params_values_post) {
        $smarty = new \webtortosa\Smarty_web();
        require_once (FOLDER_LANGUAGES."/".$this->languages($params_values_get["lang"])."_memoria-democratica.cfg");
        $smarty->assign("path_apartat","http://www2.tortosa.cat/templates");
        $smarty->assign("apartat","memoria-democratica");
        $smarty->display("head_web_antiga2.tpl");
        $md = new \webtortosa\memoria_democratica_database();

        $params = array();

        if($params_values_post) {
            $_SESSION['params_values_post'] = $params_values_post;
        }
        else {
            $params_values_post = $_SESSION['params_values_post'];
        }
        $params['NOM'] = $this->treure_accents($params_values_post['nom']);
        $params['CATEGORIA'] = $this->treure_accents($params_values_post['categoria']);

        //$items = $this->filtraArrayUtf8Encode($md->getDataMemorial($params));
        $items = $md->getDataMemorial($params);
        // Setup Pagination vars
        $total = count($items);
        $page = $params_values_get['page'];
        $start = $page ? ($page - 1) * ITEMS_LIMIT : 0;

        if($page==0)
            $page = 1;
        $prev = $page - 1; //previous page is current page - 1
        $next = $page + 1; //next page is current page + 1
        $lastpage = ceil($total/ITEMS_LIMIT); //lastpage.

        $items = $md->getDataMemorial($params, $start, ITEMS_LIMIT);
        $params = array();
        $categories = $md->getDataCategories($params);

        $smarty->assign("lastpage",$lastpage);
        $smarty->assign("page",$page);
        $smarty->assign("items",$items);
        $smarty->assign("categories",$categories);
        $smarty->display("memoria-democratica/list.tpl");
    }

    function getFileItem($params_values_get, $params_values_post) {
        $smarty = new \webtortosa\Smarty_web();
        require_once (FOLDER_LANGUAGES."/".$this->languages($params_values_get["lang"])."_memoria-democratica.cfg");
        $smarty->assign("path_apartat","http://www2.tortosa.cat/templates");
        $smarty->assign("apartat","memoria-democratica");
        $smarty->display("head_web_antiga2.tpl");
        $md = new \webtortosa\memoria_democratica_database();
        $params = array();
        $params['ID'] = $params_values_get['id'];
        //$items = $this->filtraArrayUtf8Encode($md->getDataMemorial($params));
        $items = $md->getDataMemorial($params);
        if(count($items)>0) {
            $smarty->assign("item",$items[0]);
        }
        $params = array();
        //$categories = $this->filtraArrayUtf8Encode($md->getDataCategories($params));
        $categories = $md->getDataCategories($params);
        $smarty->assign("categories",$categories);
        $smarty->assign("page",$params_values_get['page']);
        $smarty->display("memoria-democratica/item.tpl");

    }

    /*******************************************
     ********************************************
     ********************************************
            Fi Funcions Memòria Democràtica
     ********************************************
     *******************************************************************************************************************
     ********************************************/


    /******************************************
     ********************************************
     ********************************************
            Funcions Consulta
     ********************************************
     ********************************************
     ********************************************/

    function consultaCiutadania() {
        $smarty = new \webtortosa\Smarty_web();
        require_once (FOLDER_LANGUAGES."/consulta-ciutadania/".$this->languages($params_values_get["lang"])."_consulta-ciutadania.cfg");

        $smarty->assign("path_apartat","http://www2.tortosa.cat/templates");
        $smarty->assign("apartat","consulta-ciutadania");
        $smarty->assign("path_templates","../");
        $smarty->display("head_web_antiga2.tpl");

        require_once (FOLDER_LIBS . '/recaptchalib.php');
        $publickey = "6Lfdwh4TAAAAAKajPw-1Rj8msRd8YVZx14Vwova-";
        $smarty->assign("recaptcha",recaptcha_get_html($publickey));
        $smarty->display("consulta-ciutadania/principal.tpl");
    }

    function searchConsultaCiutadania($params_values_post) {
        require_once (FOLDER_MODEL."/consulta-ciutadania.class.php");
        $cconsulta = new \webtortosa\consulta_ciutadania();
        $file_logs = FOLDER_LOGS . '/consulta-ciutadania.log';
        $text_log = date('d/m/Y H:i:s');
        if($params_values_post["TDOCUMENT"]!="") {
            switch ($params_values_post["TDOCUMENT"]) {
                case "1":
                    $text_log .= ' --> Consulta: '.$params_values_post["DNI_NUMBER"].$params_values_post["DNI_LETTER"];
                    $items = $cconsulta->getElementDNI_test(strip_tags($params_values_post["DNI_NUMBER"]), strip_tags($params_values_post["DNI_LETTER"]));
                    break;
                case "2":
                    $text_log .= ' --> Consulta: '.$params_values_post["OPTION2_PASSPORT_NUMBER"];
                    $items = $cconsulta->getElementPassaport_test(strip_tags($params_values_post["OPTION2_PASSPORT_NUMBER"]));
                    break;
                case "3":
                    $text_log .= ' --> Consulta: '.$params_values_post["TR_LETTER_1"].$params_values_post["TR_NUMBER"].$params_values_post["TR_LETTER_2"];
                    $items = $cconsulta->getElementTarjetaResidencia_test(strip_tags($params_values_post["TR_LETTER_1"]), strip_tags($params_values_post["TR_NUMBER"]), strip_tags($params_values_post["TR_LETTER_2"]));
                    break;
                default;
                    break;
            }
        }


        if(count($items)>0){
            echo $items[0]->LOCAL_CONSULTA;
            $text_log .= ' - Resposta: '.$items[0]->LOCAL_CONSULTA;
        }
        else {
            echo "error";
            $text_log .= ' - Resposta: no trobat';
        }
        $this->escribir_log($file_logs,$text_log);
    }

    /******************************************
     ********************************************
     ********************************************
            Fi Funcions Consulta
     ********************************************
     ********************************************
     ********************************************/


    /******************************************
    ********************************************
    ********************************************
                 Funcions COMUNES
    ********************************************
    ********************************************
    ********************************************/

    function passa_milers($input) {
    	return number_format($input, 0, ".", ".");
    }

    function filtraArrayUtf8Encode($text_input) {
        if($text_input) {
            foreach ($text_input as $key => &$item_text) {
                foreach ($item_text as &$field)
                    $field = utf8_encode($field);
            }
        }
        return $text_input;
    }

    function languages($input_lang="") {
        if(isset($input_lang)){
            $lang = $input_lang;

            //registra sesion
            $_SESSION['lang'] = $lang;
            //define cookie
            //setcookie('lang', $lang, time() + (3600 * 24 * 30));

            //busca en variables cookie y session
        /*
        }else if(isset($_SESSION['lang'])){
            $lang = $_SESSION['lang'];
        }else if(isset($_COOKIE['lang'])){
            $lang = $_COOKIE['lang'];
        */
        }else{
            return ('ca');
        }
        return $lang;
    }

    function treure_accents ($input=null) {
        if($input) {
            $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
            $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
            $input = utf8_decode($input);
            $input = strtr($input, utf8_decode($originales), $modificadas);
            $input = strtolower($input);
            return utf8_encode($input);
        }
    }

    function escribir_log($archivo, $contenido){

        $output = date('d/m/Y G:i:s').':::: '.$contenido.PHP_EOL;
        file_put_contents($archivo, $output, FILE_APPEND);
    }

    /*******************************************
    ********************************************
    ********************************************
               Fi Funcions COMUNES
    ********************************************
    ********************************************
    ********************************************/

    //INI Yves Navarro 22/05/2017
    function ple ($input_lang, $n) {

        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        /*
        $smarty->assign('url_es','/festes/index.php?lang=es');
        $smarty->assign('url_en','/festes/index.php?lang=en');
        */

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");


        if ($n==0){

            $smarty->assign('url_ca','/ple/');
            $smarty->assign('url_es','/ple/index.php?lang=es');
            require_once (FOLDER_LANGUAGES."/ple/".$lang."_menu.cfg");
            require_once (FOLDER_LANGUAGES."/ple/".$lang."_ple.cfg");
            $smarty->display("ple/ple.tpl");

        } else if($n==1){

            $smarty->assign('url_ca','/ple/competencies.php');
            $smarty->assign('url_es','/ple/competencies.php?lang=es');
            require_once (FOLDER_LANGUAGES."/ple/".$lang."_menu.cfg");
            require_once (FOLDER_LANGUAGES."/ple/".$lang."_ple1.cfg");
            $smarty->display("ple/competencies.tpl");

        }
        else if($n==2){

            $smarty->assign('url_ca','/ple/actes_convocatories.php');
            $smarty->assign('url_es','/ple/actes_convocatories.php?lang=es');
            require_once (FOLDER_LANGUAGES."/ple/".$lang."_menu.cfg");
            require_once (FOLDER_LANGUAGES."/ple/".$lang."_ple2.cfg");
            $smarty->display("ple/actes_convocatories.tpl");

        }
        else if($n==3){

            $smarty->assign('url_ca','/ple/actes_resum.php');
            $smarty->assign('url_es','/ple/actes_resum.php?lang=es');
            require_once (FOLDER_LANGUAGES."/ple/".$lang."_menu.cfg");
            require_once (FOLDER_LANGUAGES."/ple/".$lang."_ple3.cfg");
            $smarty->display("ple/actes_resum.tpl");

        }
        else if($n==4){

            $smarty->assign('url_ca','/ple/actes_mocions.php');
            $smarty->assign('url_es','/ple/actes_mocions.php?lang=es');
            require_once (FOLDER_LANGUAGES."/ple/".$lang."_menu.cfg");
            require_once (FOLDER_LANGUAGES."/ple/".$lang."_ple4.cfg");
            $smarty->display("ple/actes_mocions.tpl");

        }
        else if($n==5){

            $smarty->assign('url_ca','/ple/composicio_actual.php');
            $smarty->assign('url_es','/ple/composicio_actual.php?lang=es');
            require_once (FOLDER_LANGUAGES."/ple/".$lang."_menu.cfg");
            require_once (FOLDER_LANGUAGES."/ple/".$lang."_ple5.cfg");
            $smarty->display("ple/composicio_actual.tpl");

        }
        else if($n==6){

            $smarty->assign('url_ca','/ple/retribucions.php');
            $smarty->assign('url_es','/ple/retribucions.php?lang=es');
            require_once (FOLDER_LANGUAGES."/ple/".$lang."_menu.cfg");
            require_once (FOLDER_LANGUAGES."/ple/".$lang."_ple6.cfg");
            $smarty->display("ple/retribucions.tpl");

        }
        else if($n==7){

            $smarty->assign('url_ca','/ple/registre_bens_interessos.php');
            $smarty->assign('url_es','/ple/registre_bens_interessos.php?lang=es');
            require_once (FOLDER_LANGUAGES."/ple/".$lang."_menu.cfg");
            require_once (FOLDER_LANGUAGES."/ple/".$lang."_ple7.cfg");
            $smarty->display("ple/registre_bens_interessos.tpl");

        }
        // else if($n==8){

        //     $smarty->assign('url_ca','/grups_politics/');
        //     $smarty->assign('url_es','/grups_politics/index.php?lang=es');
        //     require_once (FOLDER_LANGUAGES."/grups_politics/".$lang."_main.cfg");
        //     $smarty->display("grups_politics/grups_politics.tpl");

        // }
        else if($n==9){

            $smarty->assign('url_ca','/ple/junta_portaveus.php');
            $smarty->assign('url_es','/ple/junta_portaveus.php?lang=es');
            require_once (FOLDER_LANGUAGES."/ple/".$lang."_menu.cfg");
            require_once (FOLDER_LANGUAGES."/ple/".$lang."_ple9.cfg");
            $smarty->display("ple/junta_portaveus.tpl");

        }
        else if($n==10){

            $smarty->assign('url_ca','/ple/comissions_informatives.php');
            $smarty->assign('url_es','/ple/comissions_informatives.php?lang=es');
            require_once (FOLDER_LANGUAGES."/ple/".$lang."_menu.cfg");
            require_once (FOLDER_LANGUAGES."/ple/".$lang."_ple10.cfg");
            $smarty->display("ple/comissions_informatives.tpl");

        }
        else if($n==11){

            $smarty->assign('url_ca','/ple/c_especial_de_comptes.php');
            $smarty->assign('url_es','/ple/c_especial_de_comptes.php?lang=es');
            require_once (FOLDER_LANGUAGES."/ple/".$lang."_menu.cfg");
            require_once (FOLDER_LANGUAGES."/ple/".$lang."_ple11.cfg");
            $smarty->display("ple/c_especial_de_comptes.tpl");

        }

        // de 100  a 122 són tot Regidors

        else if($n==100){

            $smarty->assign('url_ca','/ple/fitxes_personal/100.php');
            $smarty->assign('url_es','/ple/fitxes_personal/100.php?lang=es');
            require_once (FOLDER_LANGUAGES."/ple/fitxes_personal/".$lang."_100.cfg");
            $smarty->display("ple/fitxa_personal.tpl");

        }
        else if($n==101){

            $smarty->assign('url_ca','/ple/fitxes_personal/101.php');
            $smarty->assign('url_es','/ple/fitxes_personal/101.php?lang=es');
            require_once (FOLDER_LANGUAGES."/ple/fitxes_personal/".$lang."_101.cfg");
            $smarty->display("ple/fitxa_personal.tpl");

        }
        else if($n==102){

            $smarty->assign('url_ca','/ple/fitxes_personal/102.php');
            $smarty->assign('url_es','/ple/fitxes_personal/102.php?lang=es');
            require_once (FOLDER_LANGUAGES."/ple/fitxes_personal/".$lang."_102.cfg");
            $smarty->display("ple/fitxa_personal.tpl");

        }
        else if($n==103){

            $smarty->assign('url_ca','/ple/fitxes_personal/103.php');
            $smarty->assign('url_es','/ple/fitxes_personal/103.php?lang=es');
            require_once (FOLDER_LANGUAGES."/ple/fitxes_personal/".$lang."_103.cfg");
            $smarty->display("ple/fitxa_personal.tpl");

        }
        else if($n==104){

            $smarty->assign('url_ca','/ple/fitxes_personal/104.php');
            $smarty->assign('url_es','/ple/fitxes_personal/104.php?lang=es');
            require_once (FOLDER_LANGUAGES."/ple/fitxes_personal/".$lang."_104.cfg");
            $smarty->display("ple/fitxa_personal.tpl");

        }
        else if($n==105){

            $smarty->assign('url_ca','/ple/fitxes_personal/105.php');
            $smarty->assign('url_es','/ple/fitxes_personal/105.php?lang=es');
            require_once (FOLDER_LANGUAGES."/ple/fitxes_personal/".$lang."_105.cfg");
            $smarty->display("ple/fitxa_personal.tpl");

        }
        else if($n==106){

            $smarty->assign('url_ca','/ple/fitxes_personal/106.php');
            $smarty->assign('url_es','/ple/fitxes_personal/106.php?lang=es');
            require_once (FOLDER_LANGUAGES."/ple/fitxes_personal/".$lang."_106.cfg");
            $smarty->display("ple/fitxa_personal.tpl");

        }
        else if($n==107){

            $smarty->assign('url_ca','/ple/fitxes_personal/107.php');
            $smarty->assign('url_es','/ple/fitxes_personal/107.php?lang=es');
            require_once (FOLDER_LANGUAGES."/ple/fitxes_personal/".$lang."_107.cfg");
            $smarty->display("ple/fitxa_personal.tpl");

        }
        else if($n==108){

            $smarty->assign('url_ca','/ple/fitxes_personal/108.php');
            $smarty->assign('url_es','/ple/fitxes_personal/108.php?lang=es');
            require_once (FOLDER_LANGUAGES."/ple/fitxes_personal/".$lang."_108.cfg");
            $smarty->display("ple/fitxa_personal.tpl");

        }
        else if($n==109){

            $smarty->assign('url_ca','/ple/fitxes_personal/109.php');
            $smarty->assign('url_es','/ple/fitxes_personal/109.php?lang=es');
            require_once (FOLDER_LANGUAGES."/ple/fitxes_personal/".$lang."_109.cfg");
            $smarty->display("ple/fitxa_personal.tpl");

        }
        else if($n==110){

            $smarty->assign('url_ca','/ple/fitxes_personal/110.php');
            $smarty->assign('url_es','/ple/fitxes_personal/110.php?lang=es');
            require_once (FOLDER_LANGUAGES."/ple/fitxes_personal/".$lang."_110.cfg");
            $smarty->display("ple/fitxa_personal.tpl");

        }
        else if($n==111){

            $smarty->assign('url_ca','/ple/fitxes_personal/111.php');
            $smarty->assign('url_es','/ple/fitxes_personal/111.php?lang=es');
            require_once (FOLDER_LANGUAGES."/ple/fitxes_personal/".$lang."_111.cfg");
            $smarty->display("ple/fitxa_personal.tpl");

        }
        else if($n==112){

            $smarty->assign('url_ca','/ple/fitxes_personal/112.php');
            $smarty->assign('url_es','/ple/fitxes_personal/112.php?lang=es');
            require_once (FOLDER_LANGUAGES."/ple/fitxes_personal/".$lang."_112.cfg");
            $smarty->display("ple/fitxa_personal.tpl");

        }
        else if($n==113){

            $smarty->assign('url_ca','/ple/fitxes_personal/113.php');
            $smarty->assign('url_es','/ple/fitxes_personal/113.php?lang=es');
            require_once (FOLDER_LANGUAGES."/ple/fitxes_personal/".$lang."_113.cfg");
            $smarty->display("ple/fitxa_personal.tpl");

        }
        else if($n==114){

            $smarty->assign('url_ca','/ple/fitxes_personal/114.php');
            $smarty->assign('url_es','/ple/fitxes_personal/114.php?lang=es');
            require_once (FOLDER_LANGUAGES."/ple/fitxes_personal/".$lang."_114.cfg");
            $smarty->display("ple/fitxa_personal.tpl");

        }
        else if($n==115){

            $smarty->assign('url_ca','/ple/fitxes_personal/115.php');
            $smarty->assign('url_es','/ple/fitxes_personal/115.php?lang=es');
            require_once (FOLDER_LANGUAGES."/ple/fitxes_personal/".$lang."_115.cfg");
            $smarty->display("ple/fitxa_personal.tpl");

        }
        else if($n==116){

            $smarty->assign('url_ca','/ple/fitxes_personal/116.php');
            $smarty->assign('url_es','/ple/fitxes_personal/116.php?lang=es');
            require_once (FOLDER_LANGUAGES."/ple/fitxes_personal/".$lang."_116.cfg");
            $smarty->display("ple/fitxa_personal.tpl");

        }
        else if($n==117){

            $smarty->assign('url_ca','/ple/fitxes_personal/117.php');
            $smarty->assign('url_es','/ple/fitxes_personal/117.php?lang=es');
            require_once (FOLDER_LANGUAGES."/ple/fitxes_personal/".$lang."_117.cfg");
            $smarty->display("ple/fitxa_personal.tpl");

        }
        else if($n==118){

            $smarty->assign('url_ca','/ple/fitxes_personal/118.php');
            $smarty->assign('url_es','/ple/fitxes_personal/118.php?lang=es');
            require_once (FOLDER_LANGUAGES."/ple/fitxes_personal/".$lang."_118.cfg");
            $smarty->display("ple/fitxa_personal.tpl");

        }
        else if($n==119){

            $smarty->assign('url_ca','/ple/fitxes_personal/119.php');
            $smarty->assign('url_es','/ple/fitxes_personal/119.php?lang=es');
            require_once (FOLDER_LANGUAGES."/ple/fitxes_personal/".$lang."_119.cfg");
            $smarty->display("ple/fitxa_personal.tpl");

        }
        else if($n==120){

            $smarty->assign('url_ca','/ple/fitxes_personal/120.php');
            $smarty->assign('url_es','/ple/fitxes_personal/120.php?lang=es');
            require_once (FOLDER_LANGUAGES."/ple/fitxes_personal/".$lang."_120.cfg");
            $smarty->display("ple/fitxa_personal.tpl");

        }
        else if($n==121){

            $smarty->assign('url_ca','/ple/fitxes_personal/121.php');
            $smarty->assign('url_es','/ple/fitxes_personal/121.php?lang=es');
            require_once (FOLDER_LANGUAGES."/ple/fitxes_personal/".$lang."_121.cfg");
            $smarty->display("ple/fitxa_personal.tpl");

        }
        else if($n==122){

            $smarty->assign('url_ca','/ple/fitxes_personal/122.php');
            $smarty->assign('url_es','/ple/fitxes_personal/122.php?lang=es');
            require_once (FOLDER_LANGUAGES."/ple/fitxes_personal/".$lang."_122.cfg");
            $smarty->display("ple/fitxa_personal.tpl");

        }
        else{

            require_once (FOLDER_LANGUAGES."/ple/".$lang."_ple.cfg");

        }

        $smarty->assign('type',$n);

    }

    function grupsPolitics ($input_lang, $n) {

        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        /*
        $smarty->assign('url_es','/festes/index.php?lang=es');
        $smarty->assign('url_en','/festes/index.php?lang=en');
        */

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        if ($n==0){

            $smarty->assign('url_ca','/grups-politics/');
            $smarty->assign('url_es','/grups-politics/index.php?lang=es');
            require_once (FOLDER_LANGUAGES."/grups-politics/".$lang."_main.cfg");
            $smarty->display("grups-politics/grups_politics.tpl");

        }else{

            $smarty->assign('url_ca','/grups-politics/');
            $smarty->assign('url_es','/grups-politics/index.php?lang=es');
            require_once (FOLDER_LANGUAGES."/grups-politics/".$lang."_main.cfg");
            $smarty->display("grups-politics/grups_politics.tpl");

        }

        $smarty->assign('type',$n);

    }

    function grupPolitic ($input_lang, $grup, $n, $page=null) {

        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        $smarty->assign('tiraPartit', 'http://www2.tortosa.cat/images/grups-politics/'.$grup.'/tira.jpg');

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/grups-politics/".$grup."/".$lang."_menu.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        if ($n==0){

            $smarty->assign('url_ca','/grups-politics/'.$grup.'/');
            $smarty->assign('url_es','/grups-politics/'.$grup.'/index.php?lang=es');
            require_once (FOLDER_LANGUAGES."/grups-politics/".$grup."/".$lang."_inici.cfg");
            $smarty->display("grups-politics/grup-politic/inici.tpl");

        } else if($n==1){



            require_once (FOLDER_MODEL."/headline.class.php");
            require_once (FOLDER_MODEL."/tematica.class.php");
            $headline = new \webtortosa\headline();
            $tematicadb = new \webtortosa\tematica();
            $utils = new \webtortosa\functions_controller();

            $smarty->assign('ntpl', $n);
            $smarty->assign('ntitol', $nt);
            if($utils->test_input($grup) == "mt-e")
                $grup_sigles = "mte";
            else
                $grup_sigles = $utils->test_input($grup);

            $params = array(
                'LOGIN'         => $grup_sigles,
                'ID_TIPOLOGIA'  => 2,
                'ACTIVA'        => 1
            );

            /* Pagination */
            $total = $headline->getHeadlinesDataArticlesOfPoliticGroupForListMaxium($params);
            $start = $page ? ($page - 1) * ITEMS_LIMIT : 0;

            if($page==0 || $page==null)
                $page = 1;
            $prev = $page - 1; //previous page is current page - 1
            $next = $page + 1; //next page is current page + 1
            $lastpage = ceil($total/ITEMS_LIMIT); //lastpage.

            $elements = $headline->getHeadlinesDataArticlesOfPoliticGroupForList($params, $start, ITEMS_LIMIT);

            $tematiques = array();

            if(isset($elements)){
                foreach($elements as &$element) {
                    $element->FOTO = str_replace("'","·", $element->FOTO);
                    $tematica = $tematicadb->getTematiquesForHeadlines($element->ID);
                    $tematiques[$element->ID] = $tematica[0]->TEMATICA;
                }
            }else{
                $smarty->assign("LABEL_noArticles", "No s'ha trobat articles per a mostrar.");
            }

            $smarty->assign("lblSeg", "Seg");
            $smarty->assign("lblAnt", "Ant");

            $smarty->assign("lastpage", $lastpage);
            $smarty->assign("page", $page);
            $smarty->assign("items", $elements);
            $smarty->assign("tematiques", $tematiques);

            $smarty->assign("GRUP_POLITIC", $grup);

            $smarty->assign('destination_form', htmlspecialchars($_SERVER["PHP_SELF"]));

            $smarty->assign('url_ca','/grups-politics/'.$grup.'/');
            $smarty->assign('url_es','/grups-politics/'.$grup.'/index.php?lang=es');
            require_once (FOLDER_LANGUAGES."/grups-politics/".$grup."/".$lang."_articles.cfg");
            $smarty->display("grups-politics/grup-politic/articles.tpl");

        }
        else if($n==2){



            $smarty->assign('url_ca','/grups-politics/'.$grup.'/');
            $smarty->assign('url_es','/grups-politics/'.$grup.'/index.php?lang=es');
            require_once (FOLDER_LANGUAGES."/grups-politics/".$grup."/".$lang."_documents.cfg");
            $smarty->display("grups-politics/grup-politic/inici.tpl");


        }

        $smarty->assign('type',$n);


    }

    //Un article d'opinió
    function articleOpinio ($input_lang, $id, $grup) {

        require_once (FOLDER_MODEL."/headline.class.php");
        require_once (FOLDER_MODEL."/tematica.class.php");
        require_once (FOLDER_MODEL."/tipologia.class.php");

        $headline = new \webtortosa\headline();
        $tematicadb = new \webtortosa\tematica();
        $tipologiadb = new \webtortosa\tipologia();

        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/grups-politics/articles-opinio/".$lang."_article".".cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        $params = array(
            ID=>$id
        );

        $elements = $headline->getHeadlinesData($params);
        $content_text = "";
        foreach($elements as $itemElement) {
            $tematiques = $tematicadb->getTematiquesForHeadlines($itemElement->ID);
            $smarty->assign('date_publish', strftime('%d/%m/%Y', strtotime($itemElement->DATA)));
            $smarty->assign('time_publish', strftime('%R', strtotime($itemElement->DATA)));

            $smarty->assign('TITOL', str_replace("\'", "'", $itemElement->TITOL));
            $smarty->assign('SUBTITOL', str_replace("\'", "'", $itemElement->SUBTITOL));
            $smarty->assign('COS', str_replace("\'", "'", str_replace("\'", "'", str_replace("\r\n", "<br />", $itemElement->DESCRIPCIO))));

            if($itemElement->FOTO==""){
                $smarty->assign("IMAGE", "<img src='/images/headlines/inotimage.jpg' width='220' height='125'>");
            }
            else{
                if($itemElement->IMPORT=="0")
                    $smarty->assign("IMAGE", "<img src='/images/headlines/res_".$itemElement->FOTO."' width='220' height='125'>");
                else
                    $smarty->assign("IMAGE", "<img src='http://www.tortosa.cat/webajt/gestiointerna/grups_municipals/documentacio/".$itemElement->FOTO."' width='220' height='125'>");
            }
            if(count($tematiques)>0) {
                $content_tematica = array();
                foreach($tematiques as $tematica) {
                    $content_tematica[] =  $tematica->TEMATICA;
                }
            }
            $params_tipologia = array(
                "ID"   => $itemElement->ID_TIPOLOGIA
            );
            $tipologies = $tipologiadb->getTipologiaData($params_tipologia);
            if(count($tipologies)>0) {
                $content_tipologia = array();
                foreach($tipologies as $tipologia) {
                    $content_tipologia[] = $tipologia->TIPOLOGIA;
                }
            }
        }

        $smarty->assign('LABEL_TC_TEMATICA', $content_tematica);
        $smarty->assign('LABEL_TC_TIPOLOGIA', $content_tipologia);

        $smarty->assign('GRUP_POLITIC', $grup);

        $smarty->display("grups-politics/grup-politic/article.tpl");

    }

    function comissioInformativa ($input_lang, $comissio) {

        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/ple/comissions/".$lang."_menu.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        if ($comissio == "serveis territori"){

            $smarty->assign('url_ca','/ple/comissions/comissio-info-serveis-territori/');
            $smarty->assign('url_es','/ple/comissions/comissio-info-serveis-territori/index.php?lang=es');
            require_once (FOLDER_LANGUAGES."/ple/comissions/comissio-info-serveis-territori/".$lang."_inici.cfg");
            $smarty->display("ple/comissio-informativa/inici.tpl");

        } else if ($comissio == "serveis persones") {

            $smarty->assign('url_ca','/ple/comissions/comissio-info-serveis-persones/');
            $smarty->assign('url_es','/ple/comissions/comissio-info-serveis-persones/index.php?lang=es');
            require_once (FOLDER_LANGUAGES."/ple/comissions/comissio-info-serveis-persones/".$lang."_inici.cfg");
            $smarty->display("ple/comissio-informativa/inici.tpl");

        }
        else if ($comissio == "promocio ciutat") {

            $smarty->assign('url_ca','/ple/comissions/comissio-info-promocio-ciutat/');
            $smarty->assign('url_es','/ple/comissions/comissio-info-promocio-ciutat/index.php?lang=es');
            require_once (FOLDER_LANGUAGES."/ple/comissions/comissio-info-promocio-ciutat/".$lang."_inici.cfg");
            $smarty->display("ple/comissio-informativa/inici.tpl");

        }
        else if ($comissio == "serveis centrals") {

            $smarty->assign('url_ca','/ple/comissions/comissio-info-serveis-centrals/');
            $smarty->assign('url_es','/ple/comissions/comissio-info-serveis-centrals/index.php?lang=es');
            require_once (FOLDER_LANGUAGES."/ple/comissions/comissio-info-serveis-centrals/".$lang."_inici.cfg");
            $smarty->display("ple/comissio-informativa/inici.tpl");

        }
        else if ($comissio == "espai public") {

            $smarty->assign('url_ca','/ple/comissions/comissio-info-espai-public/');
            $smarty->assign('url_es','/ple/comissions/comissio-info-espai-public/index.php?lang=es');
            require_once (FOLDER_LANGUAGES."/ple/comissions/comissio-info-espai-public/".$lang."_inici.cfg");
            $smarty->display("ple/comissio-informativa/inici.tpl");

        }

        $smarty->assign('type',$n);

    }

    //Junta de govern local
    function juntaGovernLocal ($input_lang, $n) {

        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");
        require_once (FOLDER_LANGUAGES."/junta-govern-local/".$lang."_menu.cfg");

        if ($n==0){

            $smarty->assign('url_ca','/junta-govern-local/');
            $smarty->assign('url_es','/junta-govern-local/index.php?lang=es');
            require_once (FOLDER_LANGUAGES."/junta-govern-local/".$lang."_juntaGovernLocal.cfg");
            $smarty->display("junta-govern-local/juntaGovernLocal.tpl");

        } else if($n==1){

            $smarty->assign('url_ca','/junta-govern-local/composicio.php');
            $smarty->assign('url_es','/junta-govern-local/composicio.php?lang=es');
            require_once (FOLDER_LANGUAGES."/junta-govern-local/".$lang."_composicio.cfg");
            $smarty->display("junta-govern-local/composicio.tpl");

        }
        else if($n==2){

            $smarty->assign('url_ca','/junta-govern-local/competencies.php');
            $smarty->assign('url_es','/junta-govern-local/competencies.php?lang=es');
            require_once (FOLDER_LANGUAGES."/junta-govern-local/".$lang."_competencies.cfg");
            $smarty->display("junta-govern-local/competencies.tpl");

        }
        else if($n==3){

            $smarty->assign('url_ca','/junta-govern-local/sessions.php');
            $smarty->assign('url_es','/junta-govern-local/sessions.php?lang=es');
            require_once (FOLDER_LANGUAGES."/junta-govern-local/".$lang."_sessions.cfg");
            $smarty->display("junta-govern-local/sessions.tpl");

        }

        $smarty->assign('type',$n);

    }

    //Altres òrgans municipals
    function altresOrgansMunicipals ($input_lang, $n) {

        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");
        require_once (FOLDER_LANGUAGES."/altres-organs-municipals/".$lang."_menu.cfg");

        if ($n==0){

            $smarty->assign('url_ca','/altres-organs-municipals/');
            $smarty->assign('url_es','/altres-organs-municipals/index.php?lang=es');
            require_once (FOLDER_LANGUAGES."/altres-organs-municipals/".$lang."_altres-organs-municipals.cfg");
            $smarty->display("altres-organs-municipals/altres-organs-municipals.tpl");

        } else if($n==1){

            $smarty->assign('url_ca','/altres-organs-municipals/alcaldes-pedanis.php');
            $smarty->assign('url_es','/altres-organs-municipals/alcaldes-pedanis.php?lang=es');
            require_once (FOLDER_LANGUAGES."/altres-organs-municipals/".$lang."_alcaldes-pedanis.cfg");
            $smarty->display("altres-organs-municipals/alcaldes-pedanis.tpl");

        }
        else if($n==2){

            $smarty->assign('url_ca','/altres-organs-municipals/consells-assessors.php');
            $smarty->assign('url_es','/altres-organs-municipals/consells-assessors.php?lang=es');
            require_once (FOLDER_LANGUAGES."/altres-organs-municipals/".$lang."_consells-assessors.cfg");
            $smarty->display("altres-organs-municipals/consells-assessors.tpl");

        }
        else if($n==3){

            $smarty->assign('url_ca','/altres-organs-municipals/juntes-coordinacio.php');
            $smarty->assign('url_es','/altres-organs-municipals/juntes-coordinacio.php?lang=es');
            require_once (FOLDER_LANGUAGES."/altres-organs-municipals/".$lang."_juntes-coordinacio.cfg");
            $smarty->display("altres-organs-municipals/juntes-coordinacio.tpl");

        }
        else if($n==4){

            $smarty->assign('url_ca','/altres-organs-municipals/junta-coordinacio-barris.php');
            $smarty->assign('url_es','/altres-organs-municipals/junta-coordinacio-barris.php?lang=es');
            require_once (FOLDER_LANGUAGES."/altres-organs-municipals/".$lang."_junta-coordinacio-barris.cfg");
            $smarty->display("altres-organs-municipals/junta-coordinacio-barris.tpl");

        }
        else if($n==5){

            $smarty->assign('url_ca','/altres-organs-municipals/junta-coordinacio-pobles.php');
            $smarty->assign('url_es','/altres-organs-municipals/junta-coordinacio-pobles.php?lang=es');
            require_once (FOLDER_LANGUAGES."/altres-organs-municipals/".$lang."_junta-coordinacio-pobles.cfg");
            $smarty->display("altres-organs-municipals/junta-coordinacio-pobles.tpl");

        }

        $smarty->assign('type',$n);

    }

    function equipGovern ($input_lang, $n) {

        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");
        require_once (FOLDER_LANGUAGES."/equip-govern/".$lang."_menu.cfg");

        if ($n==0){

            $smarty->assign('url_ca','/equip-govern/');
            $smarty->assign('url_es','/equip-govern/index.php?lang=es');
            require_once (FOLDER_LANGUAGES."/equip-govern/".$lang."_equip-govern.cfg");
            $smarty->display("equip-govern/equip-govern.tpl");

        } else if($n==1){

            $smarty->assign('url_ca','/equip-govern/tinences-alcaldia.php');
            $smarty->assign('url_es','/equip-govern/tinences-alcaldia.php?lang=es');
            require_once (FOLDER_LANGUAGES."/equip-govern/".$lang."_tinences-alcaldia.cfg");
            $smarty->display("equip-govern/tinences-alcaldia.tpl");

        }

        // else if($n==2){

        //     $smarty->assign('url_ca','/equip-govern/organitzacio.php');
        //     $smarty->assign('url_es','/aequip-govern/organitzacio.php?lang=es');
        //     require_once (FOLDER_LANGUAGES."/equip-govern/".$lang."_organitzacio.cfg");
        //     $smarty->display("equip-govern/organitzacio.tpl");

        // }

        $smarty->assign('type',$n);

    }

    function organitzacioGovernMunicipal ($input_lang, $n) {

        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);

        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        if ($n==0){

            $smarty->assign('url_ca','/organitzacio-govern-municipal/index.php');
            $smarty->assign('url_es','/organitzacio-govern-municipal/index.php?lang=es');
            require_once (FOLDER_LANGUAGES."/organitzacio-govern-municipal/".$lang."_organitzacio-govern-municipal.cfg");
            $smarty->display("organitzacio-govern-municipal/organitzacio-govern-municipal.tpl");

        }

        $smarty->assign('type',$n);

    }

    function informacioEmpleatsPublics ($input_lang, $n) {

        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);
        require_once (FOLDER_LANGUAGES."/informacio-empleats-publics/".$lang."_menu.cfg");


        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        if ($n==0){

            $smarty->assign('url_ca','/informacio-empleats-publics/');
            $smarty->assign('url_es','/informacio-empleats-publics/index.php?lang=es');
            require_once (FOLDER_LANGUAGES."/informacio-empleats-publics/".$lang."_informacio-empleats-publics.cfg");
            $smarty->display("informacio-empleats-publics/informacio-empleats-publics.tpl");

        } else if ($n==1){

            $smarty->assign('url_ca','/informacio-empleats-publics/personal-eventual.php');
            $smarty->assign('url_es','/informacio-empleats-publics/personal-eventual.php?lang=es');
            require_once (FOLDER_LANGUAGES."/informacio-empleats-publics/".$lang."_personal-eventual.cfg");
            $smarty->display("informacio-empleats-publics/personal-eventual.tpl");

        } else if ($n==2){

            $smarty->assign('url_ca','/informacio-empleats-publics/alliberats.php');
            $smarty->assign('url_es','/informacio-empleats-publics/alliberats.php?lang=es');
            require_once (FOLDER_LANGUAGES."/informacio-empleats-publics/".$lang."_alliberats.cfg");
            $smarty->display("informacio-empleats-publics/alliberats.tpl");

        } else if ($n==3){

            $smarty->assign('url_ca','/informacio-empleats-publics/personal-plantilla.php');
            $smarty->assign('url_es','/informacio-empleats-publics/personal-plantilla.php?lang=es');
            require_once (FOLDER_LANGUAGES."/informacio-empleats-publics/".$lang."_personal-plantilla.cfg");
            $smarty->display("informacio-empleats-publics/personal-plantilla.tpl");

            //Fitxes personal eventual

        } else if ($n==200){

            $smarty->assign('url_ca','/informacio-empleats-publics/200.php');
            $smarty->assign('url_es','/informacio-empleats-publics/200.php?lang=es');
            require_once (FOLDER_LANGUAGES."/informacio-empleats-publics/empleats/".$lang."_200.cfg");
            $smarty->display("informacio-empleats-publics/fitxa_personal.tpl");


        } else if ($n==201){

            $smarty->assign('url_ca','/informacio-empleats-publics/201.php');
            $smarty->assign('url_es','/informacio-empleats-publics/201.php?lang=es');
            require_once (FOLDER_LANGUAGES."/informacio-empleats-publics/empleats/".$lang."_201.cfg");
            $smarty->display("informacio-empleats-publics/fitxa_personal.tpl");

        } else if ($n==202){

            $smarty->assign('url_ca','/informacio-empleats-publics/202.php');
            $smarty->assign('url_es','/informacio-empleats-publics/202.php?lang=es');
            require_once (FOLDER_LANGUAGES."/informacio-empleats-publics/empleats/".$lang."_202.cfg");
            $smarty->display("informacio-empleats-publics/fitxa_personal.tpl");

        } else if ($n==203){

            $smarty->assign('url_ca','/informacio-empleats-publics/203.php');
            $smarty->assign('url_es','/informacio-empleats-publics/203.php?lang=es');
            require_once (FOLDER_LANGUAGES."/informacio-empleats-publics/empleats/".$lang."_203.cfg");
            $smarty->display("informacio-empleats-publics/fitxa_personal.tpl");

        } else if ($n==204){

            $smarty->assign('url_ca','/informacio-empleats-publics/204.php');
            $smarty->assign('url_es','/informacio-empleats-publics/204.php?lang=es');
            require_once (FOLDER_LANGUAGES."/informacio-empleats-publics/empleats/".$lang."_204.cfg");
            $smarty->display("informacio-empleats-publics/fitxa_personal.tpl");

        }

        $smarty->assign('type',$n);

    }

    function organitzacioMunicipal ($input_lang, $n) {

        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);


        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        if ($n==0){

            $smarty->assign('url_ca','/organitzacio-municipal/');
            $smarty->assign('url_es','/organitzacio-municipal/index.php?lang=es');
            require_once (FOLDER_LANGUAGES."/organitzacio-municipal/".$lang."_organitzacio-municipal.cfg");
            $smarty->display("organitzacio-municipal/organitzacio-municipal.tpl");

        }

        $smarty->assign('type',$n);

    }

    function organitzacioMunicipalAjuntament ($input_lang, $n) {

        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);


        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        if ($n==0){

            $smarty->assign('url_ca','/organitzacio-municipal-ajuntament/');
            $smarty->assign('url_es','/organitzacio-municipal-ajuntament/index.php?lang=es');
            require_once (FOLDER_LANGUAGES."/organitzacio-municipal-ajuntament/".$lang."_organitzacio-municipal-ajuntament.cfg");
            $smarty->display("organitzacio-municipal-ajuntament/organitzacio-municipal-ajuntament.tpl");

        }

        $smarty->assign('type',$n);

    }

    function organitzacioMunicipalEntitatsMunicipalsDescentralitzades ($input_lang, $n) {

        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);


        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        if ($n==0){

            $smarty->assign('url_ca','/organitzacio-municipal-entitats-municipals-descentralitzades/');
            $smarty->assign('url_es','/organitzacio-municipal-entitats-municipals-descentralitzades/index.php?lang=es');
            require_once (FOLDER_LANGUAGES."/organitzacio-municipal-entitats-municipals-descentralitzades/".$lang."_organitzacio-municipal-entitats-municipals-descentralitzades.cfg");
            $smarty->display("organitzacio-municipal-entitats-municipals-descentralitzades/organitzacio-municipal-entitats-municipals-descentralitzades.tpl");

        }

        $smarty->assign('type',$n);

    }

    function organitzacioMunicipalParticipacioAltresEns ($input_lang, $n) {

        $lang = $this->languages($input_lang);
        $smarty = new \webtortosa\Smarty_web();
        $smarty->assign('lang',$lang);


        require_once (FOLDER_LANGUAGES."/".$lang."_header.cfg");
        require_once (FOLDER_LANGUAGES."/".$lang."_footer.cfg");

        if ($n==0){

            $smarty->assign('url_ca','/organitzacio-municipal-participacio-altres-ens/');
            $smarty->assign('url_es','/organitzacio-municipal-participacio-altres-ens/index.php?lang=es');
            require_once (FOLDER_LANGUAGES."/organitzacio-municipal-participacio-altres-ens/".$lang."_organitzacio-municipal-participacio-altres-ens.cfg");
            $smarty->display("organitzacio-municipal-participacio-altres-ens/organitzacio-municipal-participacio-altres-ens.tpl");

        }

        $smarty->assign('type',$n);

    }

    //FIN Yves Navarro 22/05/2017
}