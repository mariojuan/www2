<?php
/**
 * Created by Ajuntament de Tortosa.
 * User: Mario Juan
 * Date: 19/01/14
 * Time: 23:18
 * To change this template use File | Settings | File Templates.
 */

namespace webtortosa;

session_start();
require_once (FOLDER_CONTROLLER."/functions_controller.php");
require_once (FOLDER_LIBS."/smarty/Smarty_web.class.php");
require_once (FOLDER_CHARTS."/fusioncharts.php");
require_once (FOLDER_MODEL."/db.class.php");
require_once (FOLDER_MODEL."/eleccions.class.php");
require_once (FOLDER_MODEL."/memoria-democratica.class.php");
require_once (FOLDER_MODEL."/informacio-publica.class.php");
require_once (FOLDER_MODEL."/ordenances.class.php");
require_once (FOLDER_MODEL."/banners/banner.class.php");
require_once (FOLDER_MODEL."/telefons/telefon.class.php");
require_once (FOLDER_MODEL."/plens.class.php");
require_once (FOLDER_MODEL."/juntes-govern.class.php");
require_once (FOLDER_MODEL."/GPDocuments.class.php");
require_once (FOLDER_MODEL."/presparticipa.class.php");


class test_controller {
    public function time_stamp() {
        $url = "https://freetsa.org/screenshot.php";
        $fichero = "test.pdf";
        $data = array(
            'screenshot'=>'http://www2.tortosa.cat/ofertes-ocupacio/fitxa.php?id=6&lang=ca',
            'delay'     => n

        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        //curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/timestamp-query'));
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0");
        $binary_response_string = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        file_put_contents($fichero, $binary_response_string);

        echo "Resultado: ".var_dump($binary_response_string);
        echo "<br>Estado: ".$status;
    }
}