<?php
/**
 * Created by JetBrains PhpStorm.
 * User: CARMA
 * Date: 15/05/17
 * Time: 07:53
 * To change this template use File | Settings | File Templates.
 */

namespace webtortosa;

class informacio_publica extends database {

    /* CARMA's functions */

    function getInformacioPublica ($start = NULL, $limit = NULL) {
        $conn = $this->connect_bbddd_sql_server();
        $strSQL = 'SELECT * FROM INFP_EDICTE WHERE ACTIU=1 AND DATA_INICI_VISUALITZACIO <=\''.date('Y/m/d').'\' AND DATA_FI_VISUALITZACIO>=\''.date('Y/m/d').'\' ';

        //echo $strSQL."<br>";
        $query = mssql_query($strSQL, $conn);
        $this->disconnect_sql_server($conn);
    
        if(mssql_num_rows($query) > 0) {
            //echo "Hi ha registres<br>";

            $items = array();
            $conta = 0;

            while($item = mssql_fetch_object($query)) {
                $conta++;

                if (isset($start) && isset($limit)) {
                    if (($conta>$start) && ($conta<=($start+$limit))) { 
                        $items[] = $item;
                        //echo $item;
                    }
                } else { 
                    $items[] = $item;
                }
            }
            return $items;
        } else { 
            //echo "NOOO hi ha registres<br>";
        }
    }
  
}