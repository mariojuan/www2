<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Mario Juan
 * Date: 13/04/15
 * Time: 08:00
 * To change this template use File | Settings | File Templates.
 */

namespace webtortosa;

class traduccio extends database {

    function addTraduccio($params) {
        $conn = $this->connect_mysqlcli();

        $strSQL = 'INSERT INTO TRA_TRADUCCIO
                   (IDIOMA, TAULA, CAMP, ID_REGISTRE, VALOR)
                   VALUES
                   (?, ?, ?, ?, ?)';

        if ($stmt = $conn->prepare($strSQL)) {
            $stmt->bind_param("sssis", $params['IDIOMA'], $params['TAULA'], $params['CAMP'], $params['ID_REGISTRE'], $params['VALOR']);
            if(!$stmt->execute()) {
                die("Error" . $stmt->error);
            }
            $id = mysqli_stmt_insert_id($stmt);
            mysqli_close($conn);
            return $id;
        }
        mysqli_close($conn);
        return null;

    }

    function getTraduccio($params = null) {
        $first_parameter = false;
        $bind_param_value = array();
        $myConnection = $this->connect_mysqlcli();

        $strSQL = "SELECT IDIOMA, TAULA, CAMP, ID_REGISTRE, VALOR
                   FROM TRA_TRADUCCIO
                   WHERE ID_REGISTRE = ? AND IDIOMA = ? AND TAULA = ? AND CAMP = ?";

        $stmt = $myConnection->prepare($strSQL);
        //echo $strSQL;
        //var_dump($a_params);
        $stmt->bind_param("isss", $params['ID_REGISTRE'], $params['IDIOMA'], $params['TAULA'], $params['CAMP']);
        $stmt->execute();
        $result = $stmt->get_result();
        $items = array();
        while ($item = $result->fetch_object()) {
            $items[] = $item;
        }
        $stmt->close();
        $myConnection->close();
        return $items;
    }

    function deleteTraduccio($params) {
        $conn = $this->connect_mysqlcli();

        $strSQL = 'DELETE FROM TRA_TRADUCCIO
                   WHERE ID_REGISTRE = ? AND IDIOMA = ? AND TAULA = ? AND CAMP = ?';

        if ($stmt = $conn->prepare($strSQL)) {
            $stmt->bind_param("isss", $params['ID_REGISTRE'], $params['IDIOMA'], $params['TAULA'], $params['CAMP']);
            if(!$stmt->execute()) {
                die("Error" . $stmt->error);
            }
            $id = mysqli_stmt_insert_id($stmt);
            mysqli_close($conn);
            return $id;
        }
        mysqli_close($conn);
        return null;
    }
}