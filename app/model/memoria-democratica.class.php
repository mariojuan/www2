<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Mario Juan
 * Date: 10/08/15
 * Time: 08:00
 *
 */

namespace webtortosa;

class memoria_democratica_database extends database {

    /* Mario's functions */

    function getDataMemorial($params, $start = NULL, $limit = NULL) {
        $this->connect();
        $strSQL = "SELECT * FROM MEM_DEMOCRATICA ";
        foreach($params as $key => $value) {
            if($first_time)
                $strSQL .= " AND " . $key . " LIKE '%" . $value . "%'";
            else {
                $strSQL .= " WHERE " . $key . " LIKE '%" . $value . "%'";
                $first_time = true;
            }
        }

        if(isset($start) && isset($limit))
            $strSQL .= " LIMIT ".$start.", ".$limit;

        //echo $strSQL . "<br>";
        $query = $this->consulta($strSQL, $this->conexion);
        $this->disconnect($this->conexion);

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }

    function getDataCategories($params) {
        $this->connect();
        $strSQL = "SELECT DISTINCT CATEGORIA FROM MEM_DEMOCRATICA ";
        foreach($params as $key => $value) {
            if($first_time)
                $strSQL .= " AND " . $key . " = '" . $value . "'";
            else {
                $strSQL .= " WHERE " . $key . "  = '" . $value . "'";
                $first_time = true;
            }
        }

        //echo $strSQL . "<br>";
        $query = $this->consulta($strSQL, $this->conexion);
        $this->disconnect($this->conexion);

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }

}