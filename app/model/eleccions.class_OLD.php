<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Mario Juan
 * Date: 13/04/15
 * Time: 08:00
 * To change this template use File | Settings | File Templates.
 */

namespace webtortosa;

class eleccions extends database {
    function getParticipacioMeses($params) {
        $this->connect_bbdd_elecc();
        
        $strSQL = "SELECT SUM(Vots14h), SUM(Vots18h), SUM(Vots20h) FROM meses";

        foreach($params as $key => $value) {
            if($a)  
                $strSQL .= " AND " . $key . " = '" . $value . "'";
            else {
                $strSQL .= " WHERE " . $key . " = '" . $value . "'";
                $a = true;
            }
        }
        $query = $this->consulta($strSQL, $this->conexion_elecc);
        $this->disconnect();

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }

    function getMesesData($params) { 
        $this->connect_bbdd_elecc();

        return $this->getData($params, "meses");        
    }

    function getEleccionsData($params) { 
        $this->connect_bbdd_elecc();
        return $this->getData($params, "eleccions");        
    }

    function getColegisEleccionsData($IdC) { 
        $strSQL = " SELECT * FROM colegis where RefEleccio='" .$IdC. "' ";
        $strSQL .= " ORDER BY NomColegi";

        $this->connect_bbdd_elecc();
        $query = $this->consulta($strSQL);
        $this->disconnect();
                        
        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }        
    }

    function getMesesColegisEleccionsData($IdComici, $IdColegi) { 
        $strSQL = " SELECT * FROM meses where RefEleccio='" .$IdComici. "' and RefColegi='" .$IdColegi. "' ";
        $strSQL .= " ORDER BY Districte, Seccio, Lletra";

        $this->connect_bbdd_elecc();
        $query = $this->consulta($strSQL);
        $this->disconnect();
                        
        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }        
    }

    function getMesaData($IdComici, $IdColegi, $IdMesa) { 
        $strSQL = " SELECT * FROM meses where RefEleccio='" .$IdComici. 
                                       "' and RefColegi='" .$IdColegi. 
                                       "' and IdMesa='" .$IdMesa. "' ";

        $this->connect_bbdd_elecc();
        $query = $this->consulta($strSQL);
        $this->disconnect();
                        
        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }        
    }

    function getMesaResultatsCandidatures($IdComici, $IdColegi, $IdMesa) { 
        $strSQL = " SELECT candidatures.IdCandidatura, candidatures.Denominacio, candidatures.Sigles, resultats.Vots " . 
                  " FROM candidatures left join resultats on ( " . 
                  " candidatures.RefEleccio = resultats.RefEleccio " . 
                  " and candidatures.IdCandidatura = resultats.RefCandidatura " . 
                  " and resultats.RefColegi = '" .$IdColegi. "' " .
                  " and resultats.RefMesa = '" .$IdMesa. "' ) " .
                  " WHERE candidatures.RefEleccio ='" .$IdComici. "' " . 
                  " ORDER BY candidatures.Denominacio ";      
        
        $this->connect_bbdd_elecc();
        $query = $this->consulta($strSQL);
        $this->disconnect();
                        
        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }        

    }

    function updateRowParticipacio($variables = array()) { 
        if (($variables["IdMesa"] == null) || ($variables["IdMesa"] == '')) { 
            echo "Error! Heu de seleccionar una mesa per modificar-la.";
        } 
        else { 

            $strUpdateSQL = " UPDATE meses SET RangAlfabetic='" .str_replace("'","\'",$variables["RangAlfabetic"]). "'," .
                                              "CensVotants='" .str_replace("'","",$variables["CensVotants"]). "'," .
                                              "CensVotantsInicial='" .str_replace("'","",$variables["CensVotantsInicial"]). "'," .
                                              "CensVotantsFinal='" .str_replace("'","",$variables["CensVotantsFinal"]). "'," .
                                              "RepresentantAdmMesa='" .str_replace("'","\'",$variables["RepresentantAdmMesa"]). "'," .
                                              "TelRepresentantAdmMesa='" .str_replace("'","\'",$variables["TelRepresentantAdmMesa"]). "'," .
                                              "RepresentantTransmissioDadesMesa='" .str_replace("'","\'",$variables["RepresentantTransmissioDadesMesa"]). "'," .
                                              "TelRepresentantTransmissioDadesMesa='" .str_replace("'","\'",$variables["TelRepresentantTransmissioDadesMesa"]). "'," .
                                              "President='" .str_replace("'","\'",$variables["President"]). "'," .
                                              "TelefonPresident='" .str_replace("'","\'",$variables["TelefonPresident"]). "'," .
                                              "Vots1Participacio='" .str_replace("'","",$variables["Vots1Participacio"]). "'," .
                                              "Vots2Participacio='" .str_replace("'","",$variables["Vots2Participacio"]). "'," .
                                              "Vots3Participacio='" .str_replace("'","",$variables["Vots3Participacio"]). "'," .
                                              "Observacions='" .str_replace("'","\'",$variables["Observacions"]). "'" .
                                       " WHERE RefEleccio='" .$variables["RefEleccio"]. "'" .
                                       " AND RefColegi='" .$variables["RefColegi"]. "'" .
                                       " AND IdMesa='" .$variables["IdMesa"]. "'";

            $this->connect_bbdd_elecc();
            $query = $this->consulta($strUpdateSQL);
            $this->disconnect();

            if( !$query ) {
                $this->notifica_suceso("Error en updateRow", $strUpdateSQL);
            } 
            else { 
                return 1;
            }
        }
    }


    function updateRowParticipacioCurt($RefEleccio,$RefColegi,$IdMesa,$Vots1Participacio,$Vots2Participacio,$Vots3Participacio) { 
        if (($IdMesa == null) || ($IdMesa == '')) { 
            echo "Error! Heu de seleccionar una mesa per modificar-la.";
        } 
        else { 
            $strUpdateSQL = "UPDATE meses SET Vots1Participacio='" .str_replace("'","",$Vots1Participacio). "'," .
                                             " Vots2Participacio='" .str_replace("'","",$Vots2Participacio). "'," .
                                             " Vots3Participacio='" .str_replace("'","",$Vots3Participacio). "'" .
                                       " WHERE RefEleccio='" .$RefEleccio. "'" .
                                       " AND RefColegi='" .$RefColegi. "'" .
                                       " AND IdMesa='" .$IdMesa. "'";

            $this->connect_bbdd_elecc();
            $query = $this->consulta($strUpdateSQL);
            $this->disconnect();

            if( !$query ) {
                $this->notifica_suceso("Error en updateRow", $strUpdateSQL);
            } 
            else { 
                return 1;
            }
        }
    }

    function updateResultatsCandidaturaMesa($RefEleccio,$RefColegi,$IdMesa,$IdCandidatura,$Vots,$Districte,$Seccio) { 
        if (($IdMesa == null) || ($IdMesa == '')) { 
            echo "Error! Heu de seleccionar una mesa per modificar els resultats.";
        } 
        else { 

            $strUpdateSQL = " SELECT COUNT(*) as NumExist FROM resultats " .
                                       " WHERE RefEleccio='" .$RefEleccio. "'" .
                                       " AND RefColegi='" .$RefColegi. "'" .
                                       " AND RefMesa='" .$IdMesa. "'" . 
                                       " AND RefCandidatura='" .$IdCandidatura. "'";

            $this->connect_bbdd_elecc();
            $query = $this->consulta($strUpdateSQL);
            $this->disconnect();

            $afectedRows = 0;

             if($this->numero_de_filas($query)>0) {
                $items = array();
                if ($item = mysql_fetch_object($query)) {
                    $afectedRows = $item->NumExist;
                }
            }            

            if ($afectedRows==0) { 
                // INSERT
                $strUpdateSQL = " INSERT INTO resultats (RefEleccio,RefColegi,RefMesa,RefCandidatura,Vots,Districte,Seccio) VALUES ( ".
                                "'" .$RefEleccio. "'," . "'" .$RefColegi. "'," . "'" .$IdMesa. "'," . "'" .$IdCandidatura. "'," .
                                "'" .str_replace("'","",$Vots). "'," . "'" .$Districte. "'," . "'" .$Seccio. "' )";

                $this->connect_bbdd_elecc();
                $query = $this->consulta($strUpdateSQL);
                $this->disconnect();

                if( !$query ) {
                    $this->notifica_suceso("Error en updateRow", $strUpdateSQL);
                } 
            } else { 
                // UPDATE
                $strUpdateSQL = " UPDATE resultats SET Vots='" .str_replace("'","",$Vots). "'" .
                                           " WHERE RefEleccio='" .$RefEleccio. "'" .
                                           " AND RefColegi='" .$RefColegi. "'" .
                                           " AND RefMesa='" .$IdMesa. "'" . 
                                           " AND RefCandidatura='" .$IdCandidatura. "'";

                $this->connect_bbdd_elecc();
                $query = $this->consulta($strUpdateSQL);
                $this->disconnect();

                if( !$query ) {
                    $this->notifica_suceso("Error en updateRow", $strUpdateSQL);
                } 
            }
        }
    }

}