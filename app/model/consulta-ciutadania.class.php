<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Mario Juan
 * Date: 29/04/16
 * Time: 08:00
 * To change this template use File | Settings | File Templates.
 */

namespace webtortosa;

class consulta_ciutadania extends database {

    /* Mario's functions */
    function getElementDNI($number, $letter) {
        $this->connect();
        $document = addslashes($number . $letter);
        $strSQL = "SELECT DISTRICTE, SECCIO, LOCAL_CONSULTA FROM CONS_MONUMENT
            WHERE CONCAT_WS('',IDENTIFICADOR, LLETRA2) = '".$document."'";
        //echo $strSQL;
        $query = $this->consulta($strSQL, $this->conexion);
        $this->disconnect();

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }

    function getElementPassaport($number) {
        $this->connect();
        $strSQL = "SELECT DISTRICTE, SECCIO, LOCAL_CONSULTA FROM CONS_MONUMENT
            WHERE DOCUMENT = '".$number."'";
        //echo $strSQL;
        $query = $this->consulta($strSQL, $this->conexion);
        $this->disconnect();

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }

    function getElementTarjetaResidencia($letter1, $number, $letter2) {
        $this->connect();
        $strSQL = "SELECT DISTRICTE, SECCIO, LOCAL_CONSULTA FROM CONS_MONUMENT
            WHERE CONCAT_WS('',LLETRA1, IDENTIFICADOR, LLETRA2) = '".$letter1 . $number . $letter2."'";
        //echo $strSQL;
        $query = $this->consulta($strSQL, $this->conexion);
        $this->disconnect();

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }

    /* Mario's functions */
    function getElementDNI_test($number, $letter) {
        $myConnection = $this->connect_mysqlcli();
        $document = $number . $letter;

        $strSQL = "SELECT DISTRICTE, SECCIO, LOCAL_CONSULTA FROM CONS_MONUMENT
            WHERE CONCAT_WS('',IDENTIFICADOR, LLETRA2) = ?";
        $stmt = $myConnection->prepare($strSQL);
        $stmt->bind_param("s",$document);
        //echo $strSQL;
        $stmt->execute();
        $result = $stmt->get_result();
        $items = array();
        while ($item = $result->fetch_object()) {
            $items[] = $item;
        }
        $stmt->close();
        $myConnection->close();
        return $items;
    }

    function getElementTarjetaResidencia_test($letter1, $number, $letter2) {
        $myConnection = $this->connect_mysqlcli();
        $document = $letter1 . $number . $letter2;

        $strSQL = "SELECT DISTRICTE, SECCIO, LOCAL_CONSULTA FROM CONS_MONUMENT
            WHERE CONCAT_WS('',LLETRA1, IDENTIFICADOR, LLETRA2) = ?";
        $stmt = $myConnection->prepare($strSQL);
        $stmt->bind_param("s",$document);
        //echo $strSQL;
        $stmt->execute();
        $result = $stmt->get_result();
        $items = array();
        while ($item = $result->fetch_object()) {
            $items[] = $item;
        }
        $stmt->close();
        $myConnection->close();
        return $items;
    }

    function getElementPassaport_test($number) {
        $myConnection = $this->connect_mysqlcli();
        $document = $number;

        $strSQL = "SELECT DISTRICTE, SECCIO, LOCAL_CONSULTA FROM CONS_MONUMENT
            WHERE DOCUMENT = ?";
        $stmt = $myConnection->prepare($strSQL);
        $stmt->bind_param("s",$document);
        //echo $strSQL;
        $stmt->execute();
        $result = $stmt->get_result();
        $items = array();
        while ($item = $result->fetch_object()) {
            $items[] = $item;
        }
        $stmt->close();
        $myConnection->close();
        return $items;
    }
}