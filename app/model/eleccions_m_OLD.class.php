<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Mario Juan
 * Date: 13/04/15
 * Time: 08:00
 * To change this template use File | Settings | File Templates.
 */

namespace webtortosa;

class eleccions extends database {

    function getResultatsMeses($params) {
        $this->connect_bbdd_elecc();

        $strSQL = 'SELECT eleccions.Denominacio AS NomEleccio, eleccions.Any, SUM( resultats.Vots ) AS Vots, candidatures.Sigles, candidatures.Denominacio, candidatures.Color, meses.Districte,
            (SELECT SUM(resultats.Vots) FROM resultats WHERE resultats.RefEleccio=\''.$params[0].'\' AND resultats.Districte = \''.$params[1].'\'
            AND resultats.Seccio = \''.$params[2].'\' AND resultats.refMesa = \''.$params[3].'\') as VotsTotals, 
            (SELECT MAX(resultats.Districte) FROM resultats WHERE resultats.RefEleccio=\''.$params[0].'\') as MaxDistricte
            FROM resultats
            INNER JOIN candidatures ON candidatures.IdCandidatura = resultats.RefCandidatura
            AND candidatures.RefEleccio = resultats.RefEleccio
            INNER JOIN eleccions ON eleccions.IdEleccio = resultats.RefEleccio
            INNER JOIN meses ON meses.idMesa = resultats.refMesa
            AND meses.RefEleccio = resultats.RefEleccio
            WHERE resultats.RefEleccio = \''.$params[0].'\' 
            AND resultats.Districte = \''.$params[1].'\'
            AND resultats.Seccio = \''.$params[2].'\'
            AND resultats.refMesa = \''.$params[3].'\'
            GROUP BY resultats.Districte, resultats.RefCandidatura
            ORDER BY resultats.Districte ASC,resultats.Vots DESC ';
        
        //echo $strSQL."<br>";
        $query = $this->consulta($strSQL, $this->conexion);
        $this->disconnect();

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }

    function getResultatsSeccions($params) {
        $this->connect_bbdd_elecc();

        $strSQL = 'SELECT eleccions.Denominacio AS NomEleccio, eleccions.Any, SUM( R1.Vots ) AS Vots, candidatures.Sigles, candidatures.Denominacio, candidatures.Color, meses.Districte, meses.Seccio,
            (SELECT SUM(R2.Vots) FROM resultats R2 WHERE R2.RefEleccio=\''.$params[0].'\' AND R2.Districte = \''.$params[1].'\'
            AND R2.Seccio = \''.$params[2].'\' AND R2.Districte = R1.Districte) as VotsTotals
            FROM resultats R1
            INNER JOIN candidatures ON candidatures.IdCandidatura = R1.RefCandidatura
            AND candidatures.RefEleccio = R1.RefEleccio
            INNER JOIN eleccions ON eleccions.IdEleccio = R1.RefEleccio
            INNER JOIN meses ON meses.idMesa = R1.refMesa
            AND meses.RefEleccio = R1.RefEleccio
            WHERE R1.RefEleccio = \''.$params[0].'\' 
            AND R1.Districte = \''.$params[1].'\'
            AND R1.Seccio = \''.$params[2].'\'
            GROUP BY R1.Districte, R1.RefCandidatura
            ORDER BY R1.Districte ASC,R1.Vots DESC ';
        
        //echo $strSQL."<br>";
        $query = $this->consulta($strSQL, $this->conexion);
        $this->disconnect();

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }

    function getResultatsDistrictes($params) {
        $this->connect_bbdd_elecc();

        $strSQL = '
            SELECT eleccions.Denominacio AS NomEleccio, eleccions.Any, SUM( R1.Vots ) AS Vots, candidatures.Sigles, candidatures.Denominacio, candidatures.Color, meses.Districte, 
            (SELECT SUM(R2.Vots) FROM resultats R2 WHERE R2.RefEleccio=\''.$params[0].'\' AND R2.Districte = R1.Districte GROUP BY R2.Districte) as VotsTotals, 
            (SELECT MAX(resultats.Districte) FROM resultats WHERE resultats.RefEleccio=\''.$params[0].'\') as MaxDistricte 
            FROM resultats R1 
            INNER JOIN candidatures ON candidatures.IdCandidatura = R1.RefCandidatura AND candidatures.RefEleccio = R1.RefEleccio 
            INNER JOIN eleccions ON eleccions.IdEleccio = R1.RefEleccio INNER JOIN meses ON meses.idMesa = R1.refMesa AND meses.RefEleccio = R1.RefEleccio 
            WHERE R1.RefEleccio = \''.$params[0].'\' 
            GROUP BY R1.Districte, R1.RefCandidatura ORDER BY R1.Districte ASC,R1.Vots DESC'; 
        
        //echo $strSQL."<br>";
        $query = $this->consulta($strSQL, $this->conexion);
        $this->disconnect();

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }

    function getResultatsTotals($params) {
        $this->connect_bbdd_elecc();
        
        $strSQL = 'SELECT eleccions.Denominacio AS NomEleccio, eleccions.Any, eleccions.RefEleccioComparada, eleccions.Regidors AS NumRegidors, SUM(resultats.Vots) as Vots, candidatures.Sigles, candidatures.Denominacio, candidatures.Color,
            (SELECT SUM(resultats.Vots) FROM resultats WHERE resultats.RefEleccio=\''.$params[0].'\') as VotsTotals
            FROM resultats 
            INNER JOIN candidatures ON candidatures.IdCandidatura = resultats.RefCandidatura AND candidatures.RefEleccio = resultats.RefEleccio 
            INNER JOIN eleccions ON eleccions.IdEleccio = resultats.RefEleccio
            WHERE resultats.RefEleccio=\''.$params[0].'\'
            GROUP BY resultats.RefCandidatura ORDER BY Vots DESC';
        
        //echo $strSQL."<br>";
        $query = $this->consulta($strSQL, $this->conexion);
        $this->disconnect();

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }

	function getParticipacio($params) {
		$this->connect_bbdd_elecc();
        
		$strSQL = 'SELECT SUM(Vots1participacio) as Totals1participacio, SUM(Vots2Participacio) as Totals2participacio, SUM(Vots3Participacio) as Totals3participacio, COUNT(IDMesa) as NumMeses, eleccions.CensVotants, eleccions.Hora1Participacio, eleccions.Hora2Participacio, eleccions.Hora3Participacio  
            FROM meses 
            INNER JOIN eleccions ON meses.RefEleccio = eleccions.IdEleccio';

		foreach($params as $key => $value) {
			if($a)	
            	$strSQL .= " AND " . $key . " = '" . $value . "'";
            else {
            	$strSQL .= " WHERE " . $key . " = '" . $value . "'";
            	$a = true;
            }
        }
        //echo $strSQL."<br>";
        $query = $this->consulta($strSQL, $this->conexion);
        $this->disconnect();

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }

    function getMeses($params) {
        $this->connect_bbdd_elecc();
        $strSQL = 'select DISTINCT resultats.Districte, resultats.Seccio, resultats.RefMesa as Mesa, colegis.NomColegi 
            FROM resultats 
            INNER JOIN colegis ON colegis.IdColegi = resultats.RefColegi
            AND colegis.RefEleccio = \''.$params[0].'\'
            WHERE resultats.RefEleccio = \''.$params[0].'\' 
            ORDER BY resultats.Districte, resultats.Seccio ASC';

        //echo $strSQL."<br>";
        $query = $this->consulta($strSQL, $this->conexion);
        $this->disconnect();

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }

    function getSeccions($params) {
        $this->connect_bbdd_elecc();
        $strSQL = 'select DISTINCT Districte, Seccio 
            From resultats 
            WHERE RefEleccio = \''.$params[0].'\' 
            order by Districte, Seccio ASC';

        //echo $strSQL."<br>";
        $query = $this->consulta($strSQL, $this->conexion);
        $this->disconnect();

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }

    function getEleccionsData($params) { 
        $this->connect_bbdd_elecc();
        return $this->getData($params, "eleccions");        
    }

    function getComparativaParticipacioMeses($params) {
        $this->connect_bbdd_elecc();
        $strSQL = 'SELECT meses.IdMesa, meses.VotsFinParticipacio, colegis.NomColegi, eleccions.Any 
            FROM meses 
            INNER JOIN eleccions ON eleccions.IdEleccio = meses.RefEleccio 
            INNER JOIN colegis ON colegis.IdColegi = meses.RefColegi
            WHERE meses.RefEleccio=\''.$params[0].'\' group by meses.IdMesa
            UNION 
            SELECT meses.IdMesa, meses.VotsFinParticipacio, colegis.NomColegi, eleccions.Any 
            FROM meses 
            INNER JOIN eleccions ON eleccions.IdEleccio = meses.RefEleccio 
            INNER JOIN colegis ON colegis.IdColegi = meses.RefColegi
            WHERE eleccions.IdEleccio=\''.$params[1].'\' group by meses.IdMesa';

        //echo $strSQL."<br>";
        $query = $this->consulta($strSQL, $this->conexion);
        $this->disconnect();

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }

    function getParticipacioDistrictes($params) {

        $this->connect_bbdd_elecc();
        $strSQL = 'SELECT meses.Districte, 
                    SUM(meses.Vots1Participacio) as Vots1Participacio, 
                    SUM(meses.Vots2Participacio) as Vots2Participacio, 
                    SUM(meses.Vots3Participacio) as Vots3Participacio, 
                    SUM(meses.VotsFinParticipacio) as VotsFinParticipacio, 
                    COUNT(meses.IdMesa) as NumMeses, SUM(meses.CensVotants) as CensVotants,
                    eleccions.Hora1Participacio,
                    eleccions.Hora2Participacio,
                    eleccions.Hora3Participacio
            FROM meses 
            INNER JOIN eleccions ON eleccions.IdEleccio = meses.RefEleccio
            WHERE meses.RefEleccio=\''.$params[0].'\' group by meses.Districte';

        //echo $strSQL."<br>";
        $query = $this->consulta($strSQL, $this->conexion);
        $this->disconnect();

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }

    function getParticipacioSeccions($params) {

        $this->connect_bbdd_elecc();
        $strSQL = 'SELECT meses.Districte, meses.Seccio,
                    SUM(meses.Vots1Participacio) as Vots1Participacio, 
                    SUM(meses.Vots2Participacio) as Vots2Participacio, 
                    SUM(meses.Vots3Participacio) as Vots3Participacio, 
                    SUM(meses.VotsFinParticipacio) as VotsFinParticipacio, 
                    COUNT(meses.IdMesa) as NumMeses, SUM(meses.CensVotants) as CensVotants,
                    eleccions.Hora1Participacio,
                    eleccions.Hora2Participacio,
                    eleccions.Hora3Participacio
            FROM meses 
            INNER JOIN eleccions ON eleccions.IdEleccio = meses.RefEleccio
            WHERE meses.RefEleccio=\''.$params[0].'\' group by meses.Districte, meses.Seccio';

        //echo $strSQL."<br>";
        $query = $this->consulta($strSQL, $this->conexion);
        $this->disconnect();

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }

    function getParticipacioMeses($params) {
        $this->connect_bbdd_elecc();
        $strSQL = 'SELECT DISTINCT(meses.IdMesa), meses.Districte, meses.Seccio,
                    meses.Vots1Participacio, 
                    meses.Vots2Participacio, 
                    meses.Vots3Participacio, 
                    meses.VotsFinParticipacio, 
                    meses.CensVotants,
                    eleccions.Hora1Participacio,
                    eleccions.Hora2Participacio,
                    eleccions.Hora3Participacio,
                    colegis.NomColegi
            FROM meses 
            INNER JOIN eleccions ON eleccions.IdEleccio = meses.RefEleccio
            INNER JOIN colegis ON colegis.IdColegi = meses.RefColegi AND colegis.RefEleccio = \''.$params[0].'\'
            WHERE meses.RefEleccio=\''.$params[0].'\'';

        //echo $strSQL."<br>";
        $query = $this->consulta($strSQL, $this->conexion);
        $this->disconnect();

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }
}