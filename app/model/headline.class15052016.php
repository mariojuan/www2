<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Mario Juan
 * Date: 13/04/15
 * Time: 08:00
 * To change this template use File | Settings | File Templates.
 */

namespace webtortosa;

class headline extends database {
		function getHeadlinesData($params) {
            $this->connect();
            return $this->getData($params, "HEAD_HEADLINE");
        }

        function getTematiquesHeadlinesData($params) {
            $this->connect();
            return $this->getData($params, "HEAD_TEMATICA");
        }
}