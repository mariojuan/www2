<?php
/**
 * Created by a mano.
 * User: Carlos Bardi
 * Date: 25/05/16
 * Time: 13:30
 */

namespace webtortosa;

class consultes extends database {

    /* Mario's functions */

    function getLastEleccio () {
        $this->connect_bbdd_elecc();
        $strSQL = 'SELECT eleccions.IdEleccio FROM eleccions WHERE TipusEleccio=\''.'CON'.'\' ORDER BY eleccions.Fecha DESC LIMIT 0, 1';

        //echo $strSQL."<br>";
        $query = $this->consulta($strSQL, $this->conexion);
        $this->disconnect();

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }

    function getResultatsMeses($params) {
        $this->connect_bbdd_elecc();

/*
        $strSQL = 'SELECT eleccions.Denominacio AS NomEleccio, eleccions.Any, SUM( resultats.Vots ) AS Vots, candidatures.Sigles, candidatures.Denominacio, candidatures.Color, meses.Districte,
            (SELECT SUM(resultats.Vots) FROM resultats WHERE resultats.RefEleccio=\''.$params[0].'\' AND resultats.Districte = \''.$params[1].'\'
            AND resultats.Seccio = \''.$params[2].'\' AND resultats.refMesa = \''.$params[3].'\') as VotsTotals, 
            (SELECT MAX(resultats.Districte) FROM resultats WHERE resultats.RefEleccio=\''.$params[0].'\') as MaxDistricte
            FROM resultats
            INNER JOIN candidatures ON candidatures.IdCandidatura = resultats.RefCandidatura
            AND candidatures.RefEleccio = resultats.RefEleccio
            INNER JOIN eleccions ON eleccions.IdEleccio = resultats.RefEleccio
            INNER JOIN meses ON meses.idMesa = resultats.refMesa
            AND meses.RefEleccio = resultats.RefEleccio
            WHERE resultats.RefEleccio = \''.$params[0].'\' 
            AND resultats.Districte = \''.$params[1].'\'
            AND resultats.Seccio = \''.$params[2].'\'
            AND resultats.refMesa = \''.$params[3].'\'
            GROUP BY resultats.Districte, resultats.RefCandidatura
            ORDER BY resultats.Districte ASC,resultats.Vots DESC ';
*/    


        $strSQL = 'SELECT eleccions.Denominacio AS NomEleccio, eleccions.Any, SUM( R0.Vots ) AS Vots, 
            candidatures.Sigles, candidatures.Denominacio, candidatures.Color, meses.Districte,
            candidatures.esVotNul,
            (SELECT SUM(R1.Vots) FROM resultats R1 WHERE R1.RefEleccio=R0.RefEleccio AND R1.Districte = R0.Districte
            AND R1.Seccio = R0.Seccio AND R1.refMesa = R0.refMesa) as VotsTotals, 
            (SELECT MAX(R2.Districte) FROM resultats R2 WHERE R2.RefEleccio=R0.RefEleccio) as MaxDistricte,
            (SELECT SUM(C1.CensVotants) as CensSeccio FROM meses C1 
             WHERE C1.RefEleccio = R0.RefEleccio 
             AND C1.Districte = R0.Districte 
             and C1.Seccio=R0.Seccio
             and C1.IdMesa=R0.RefMesa
             GROUP BY C1.Districte,C1.Seccio,C1.IdMesa) as CensMesa

            FROM resultats R0
            INNER JOIN candidatures ON candidatures.IdCandidatura = R0.RefCandidatura
            AND candidatures.RefEleccio = R0.RefEleccio
            INNER JOIN eleccions ON eleccions.IdEleccio = R0.RefEleccio
            INNER JOIN meses ON meses.idMesa = R0.refMesa
            AND meses.RefEleccio = R0.RefEleccio
            WHERE R0.RefEleccio = \''.$params[0].'\' 
            AND R0.Districte = \''.$params[1].'\'
            AND R0.Seccio = \''.$params[2].'\'
            AND R0.refMesa = \''.$params[3].'\'
            GROUP BY R0.Districte, R0.RefCandidatura
            ORDER BY R0.Districte ASC,R0.Vots DESC ';

        //echo $strSQL."<br>";
        $query = $this->consulta($strSQL, $this->conexion);
        $this->disconnect();

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }

    function getResultatsSeccions($params) {
        $this->connect_bbdd_elecc();

/*
        $strSQL = 'SELECT eleccions.Denominacio AS NomEleccio, eleccions.Any, SUM( R1.Vots ) AS Vots, candidatures.Sigles, candidatures.Denominacio, candidatures.Color, meses.Districte, meses.Seccio,
            (SELECT SUM(R2.Vots) FROM resultats R2 WHERE R2.RefEleccio=R1.RefEleccio AND R2.Districte = R1.Districte
            AND R2.Seccio = R1.Seccio) as VotsTotals
            FROM resultats R1
            INNER JOIN candidatures ON candidatures.IdCandidatura = R1.RefCandidatura
            AND candidatures.RefEleccio = R1.RefEleccio
            INNER JOIN eleccions ON eleccions.IdEleccio = R1.RefEleccio
            INNER JOIN meses ON meses.idMesa = R1.refMesa
            AND meses.RefEleccio = R1.RefEleccio
            WHERE R1.RefEleccio = \''.$params[0].'\' 
            AND R1.Districte = \''.$params[1].'\'
            AND R1.Seccio = \''.$params[2].'\'
            GROUP BY R1.Districte, R1.RefCandidatura
            ORDER BY R1.Districte ASC,R1.Vots DESC ';
*/
        $strSQL = 'SELECT eleccions.Denominacio AS NomEleccio, eleccions.Any, SUM( R1.Vots ) AS Vots, 
            candidatures.Sigles, candidatures.Denominacio, candidatures.Color, 
            meses.Districte, meses.Seccio, candidatures.esVotNul,
            (SELECT SUM(R2.Vots) FROM resultats R2 
                WHERE R2.RefEleccio=R1.RefEleccio 
                 AND R2.Districte = R1.Districte
                 AND R2.Seccio = R1.Seccio) as VotsTotals,
            (SELECT COUNT(DISTINCT(R3.RefMesa)) as num_mesas FROM resultats R3 
             WHERE R3.RefEleccio=R1.RefEleccio 
             and R3.Districte=R1.Districte 
             and R3.Seccio=R1.Seccio
             GROUP BY R3.Districte,R3.Seccio) as NumMesas,
            (SELECT SUM(C1.CensVotants) as CensSeccio FROM meses C1 
             WHERE C1.RefEleccio = R1.RefEleccio 
             AND C1.Districte = R1.Districte 
             and C1.Seccio=R1.Seccio
             GROUP BY C1.Districte,C1.Seccio) as CensSeccio
                        FROM resultats R1
                        INNER JOIN candidatures ON candidatures.IdCandidatura = R1.RefCandidatura
                        AND candidatures.RefEleccio = R1.RefEleccio
                        INNER JOIN eleccions ON eleccions.IdEleccio = R1.RefEleccio
                        INNER JOIN meses ON meses.idMesa = R1.refMesa
                        AND meses.RefEleccio = R1.RefEleccio
                        WHERE R1.RefEleccio = \''.$params[0].'\' 
                        AND R1.Districte = \''.$params[1].'\'
                        AND R1.Seccio = \''.$params[2].'\'
                        GROUP BY R1.Districte, R1.RefCandidatura
                        ORDER BY R1.Districte ASC,R1.Vots DESC ';

        //echo $strSQL."<br>";
        $query = $this->consulta($strSQL, $this->conexion);
        $this->disconnect();

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }

    function getResultatsDistrictes($params) {
        $this->connect_bbdd_elecc();

/*
        $strSQL = '
            SELECT eleccions.Denominacio AS NomEleccio, eleccions.Any, SUM( R1.Vots ) AS Vots, candidatures.Sigles, candidatures.Denominacio, candidatures.Color, meses.Districte, 
            (SELECT SUM(R2.Vots) FROM resultats R2 WHERE R2.RefEleccio=\''.$params[0].'\' AND R2.Districte = R1.Districte GROUP BY R2.Districte) as VotsTotals, 
            (SELECT MAX(resultats.Districte) FROM resultats WHERE resultats.RefEleccio=\''.$params[0].'\') as MaxDistricte 
            FROM resultats R1 
            INNER JOIN candidatures ON candidatures.IdCandidatura = R1.RefCandidatura AND candidatures.RefEleccio = R1.RefEleccio 
            INNER JOIN eleccions ON eleccions.IdEleccio = R1.RefEleccio INNER JOIN meses ON meses.idMesa = R1.refMesa AND meses.RefEleccio = R1.RefEleccio 
            WHERE R1.RefEleccio = \''.$params[0].'\' 
            GROUP BY R1.Districte, R1.RefCandidatura ORDER BY R1.Districte ASC,R1.Vots DESC'; 
*/

            $strSQL = 'SELECT eleccions.Denominacio AS NomEleccio, 
            eleccions.Any, 
            SUM( R1.Vots ) AS Vots, 
            candidatures.Sigles, 
            candidatures.Denominacio, 
            candidatures.Color, 
            meses.Districte, 
            candidatures.esVotNul,
            (SELECT SUM(R2.Vots) FROM resultats R2 
            WHERE R2.RefEleccio=R1.RefEleccio AND R2.Districte = R1.Districte 
            GROUP BY R2.Districte) as VotsTotals, 
            (SELECT SUM(C1.CensVotants) as CensDistricte FROM meses C1 
            WHERE C1.RefEleccio = R1.RefEleccio AND C1.Districte = R1.Districte 
            GROUP BY R1.Districte) as CensDistricte,
            (SELECT MAX(resultats.Districte) FROM resultats 
            WHERE resultats.RefEleccio=R1.RefEleccio) as MaxDistricte,
            (SELECT COUNT(DISTINCT(R3.RefMesa)) as num_mesas FROM resultats R3 
            WHERE R3.RefEleccio=R1.RefEleccio and R3.Districte=R1.Districte 
            GROUP BY R3.Districte) as NumMesas

            FROM resultats R1 
                       INNER JOIN candidatures ON candidatures.IdCandidatura = R1.RefCandidatura AND candidatures.RefEleccio = R1.RefEleccio 
                        INNER JOIN eleccions ON eleccions.IdEleccio = R1.RefEleccio 
                        INNER JOIN meses ON meses.idMesa = R1.refMesa AND meses.RefEleccio = R1.RefEleccio 
            WHERE R1.RefEleccio = \''.$params[0].'\' 
            GROUP BY R1.Districte, R1.RefCandidatura 
            ORDER BY R1.Districte ASC,R1.Vots DESC';

        //echo $strSQL."<br>";
        $query = $this->consulta($strSQL, $this->conexion);
        $this->disconnect();

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }

    function getResultatsTotals($params) {
        $this->connect_bbdd_elecc();
        
        $strSQL = 'SELECT eleccions.Denominacio AS NomEleccio, eleccions.Any, eleccions.RefEleccioComparada, eleccions.Regidors AS NumRegidors, SUM(resultats.Vots) as Vots, candidatures.SiglaNormal as Sigles, candidatures.Denominacio, candidatures.Color, candidatures.esVotNul, candidatures.esVotBlanc,
            (SELECT SUM(resultats.Vots) FROM resultats WHERE resultats.RefEleccio=\''.$params[0].'\') as VotsTotals
            FROM resultats 
            INNER JOIN candidatures ON candidatures.IdCandidatura = resultats.RefCandidatura AND candidatures.RefEleccio = resultats.RefEleccio 
            INNER JOIN eleccions ON eleccions.IdEleccio = resultats.RefEleccio
            WHERE resultats.RefEleccio=\''.$params[0].'\'
            GROUP BY resultats.RefCandidatura ORDER BY Vots DESC';
        
        //echo $strSQL."<br>";
        $query = $this->consulta($strSQL, $this->conexion);
        $this->disconnect();

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }

  function getParticipacio($params) {
    $this->connect_bbdd_elecc();
        
    $strSQL = 'SELECT SUM(Vots1participacio) as Totals1participacio, SUM(Vots2Participacio) as Totals2participacio, SUM(Vots3Participacio) as Totals3participacio, COUNT(IDMesa) as NumMeses, eleccions.CensVotants, eleccions.Hora1Participacio, eleccions.Hora2Participacio, eleccions.Hora3Participacio  
            FROM meses 
            INNER JOIN eleccions ON meses.RefEleccio = eleccions.IdEleccio';

    foreach($params as $key => $value) {
      if($a)  
              $strSQL .= " AND " . $key . " = '" . $value . "'";
            else {
              $strSQL .= " WHERE " . $key . " = '" . $value . "'";
              $a = true;
            }
        }
        //echo $strSQL."<br>";
        $query = $this->consulta($strSQL, $this->conexion);
        $this->disconnect();

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }

    function getMeses($params) {
        $this->connect_bbdd_elecc();
        $strSQL = 'select DISTINCT resultats.Districte, resultats.Seccio, resultats.RefMesa as Mesa, colegis.NomColegi 
            FROM resultats 
            INNER JOIN colegis ON colegis.IdColegi = resultats.RefColegi
            AND colegis.RefEleccio = \''.$params[0].'\'
            WHERE resultats.RefEleccio = \''.$params[0].'\' 
            ORDER BY colegis.NomColegi ASC';

        //echo $strSQL."<br>";
        $query = $this->consulta($strSQL, $this->conexion);
        $this->disconnect();

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }

    function getSeccions($params) {
        $this->connect_bbdd_elecc();
        $strSQL = 'select DISTINCT Districte, Seccio 
            From resultats 
            WHERE RefEleccio = \''.$params[0].'\' 
            order by Districte, Seccio ASC';

        //echo $strSQL."<br>";
        $query = $this->consulta($strSQL, $this->conexion);
        $this->disconnect();

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }

    function getComparativaParticipacioMeses($params) {
        $this->connect_bbdd_elecc();
        $strSQL = 'SELECT meses.IdMesa, meses.CensVotants, meses.VotsFinParticipacio, colegis.NomColegi, eleccions.Any,
            meses.Vots1Participacio, meses.Vots2Participacio, meses.Vots3Participacio,
            eleccions.Hora1Participacio, eleccions.Hora2Participacio, eleccions.Hora3Participacio, meses.RefEleccio
            FROM meses 
            INNER JOIN eleccions ON eleccions.IdEleccio = meses.RefEleccio 
            INNER JOIN colegis ON colegis.IdColegi = meses.RefColegi
            WHERE meses.RefEleccio=\''.$params[0].'\' group by meses.IdMesa
            UNION 
            SELECT meses.IdMesa, meses.CensVotants, meses.VotsFinParticipacio, colegis.NomColegi, eleccions.Any,
            meses.Vots1Participacio, meses.Vots2Participacio, meses.Vots3Participacio,
            eleccions.Hora1Participacio, eleccions.Hora2Participacio, eleccions.Hora3Participacio, meses.RefEleccio   
            FROM meses 
            INNER JOIN eleccions ON eleccions.IdEleccio = meses.RefEleccio 
            INNER JOIN colegis ON colegis.IdColegi = meses.RefColegi
            WHERE eleccions.IdEleccio=\''.$params[1].'\' group by meses.IdMesa';

        //echo $strSQL."<br>";
        $query = $this->consulta($strSQL, $this->conexion);
        $this->disconnect();

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }

    function getParticipacioDistrictes($params) {

        $this->connect_bbdd_elecc();
        $strSQL = 'SELECT meses.Districte, 
                    SUM(meses.Vots1Participacio) as Vots1Participacio, 
                    SUM(meses.Vots2Participacio) as Vots2Participacio, 
                    SUM(meses.Vots3Participacio) as Vots3Participacio, 
                    SUM(meses.VotsFinParticipacio) as VotsFinParticipacio, 
                    COUNT(meses.IdMesa) as NumMeses, SUM(meses.CensVotants) as CensVotants,
                    eleccions.Hora1Participacio,
                    eleccions.Hora2Participacio,
                    eleccions.Hora3Participacio
            FROM meses 
            INNER JOIN eleccions ON eleccions.IdEleccio = meses.RefEleccio
            WHERE meses.RefEleccio=\''.$params[0].'\' group by meses.Districte';

        //echo $strSQL."<br>";
        $query = $this->consulta($strSQL, $this->conexion);
        $this->disconnect();

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }

    function getParticipacioSeccions($params) {

        $this->connect_bbdd_elecc();
        $strSQL = 'SELECT meses.Districte, meses.Seccio,
                    SUM(meses.Vots1Participacio) as Vots1Participacio, 
                    SUM(meses.Vots2Participacio) as Vots2Participacio, 
                    SUM(meses.Vots3Participacio) as Vots3Participacio, 
                    SUM(meses.VotsFinParticipacio) as VotsFinParticipacio, 
                    COUNT(meses.IdMesa) as NumMeses, SUM(meses.CensVotants) as CensVotants,
                    eleccions.Hora1Participacio,
                    eleccions.Hora2Participacio,
                    eleccions.Hora3Participacio
            FROM meses 
            INNER JOIN eleccions ON eleccions.IdEleccio = meses.RefEleccio
            WHERE meses.RefEleccio=\''.$params[0].'\' group by meses.Districte, meses.Seccio';

        //echo $strSQL."<br>";
        $query = $this->consulta($strSQL, $this->conexion);
        $this->disconnect();

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }

    function getParticipacioMesesFront($params) {
        $this->connect_bbdd_elecc();
        $strSQL = 'SELECT DISTINCT(meses.IdMesa), meses.Districte, meses.Seccio,
                    meses.Vots1Participacio, 
                    meses.Vots2Participacio, 
                    meses.Vots3Participacio, 
                    meses.VotsFinParticipacio, 
                    meses.CensVotants,
                    eleccions.Hora1Participacio,
                    eleccions.Hora2Participacio,
                    eleccions.Hora3Participacio,
                    colegis.NomColegi
            FROM meses 
            INNER JOIN eleccions ON eleccions.IdEleccio = meses.RefEleccio
            INNER JOIN colegis ON colegis.IdColegi = meses.RefColegi AND colegis.RefEleccio = \''.$params[0].'\'
            WHERE meses.RefEleccio=\''.$params[0].'\'';

        //echo $strSQL."<br>";
        $query = $this->consulta($strSQL, $this->conexion);
        $this->disconnect();

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }

    /* Carlos's functions */

    function getParticipacioMeses($params) {
        $this->connect_bbdd_elecc();
        
        $strSQL = "SELECT SUM(Vots14h), SUM(Vots18h), SUM(Vots20h) FROM meses";

        foreach($params as $key => $value) {
            if($a)  
                $strSQL .= " AND " . $key . " = '" . $value . "'";
            else {
                $strSQL .= " WHERE " . $key . " = '" . $value . "'";
                $a = true;
            }
        }
        $query = $this->consulta($strSQL, $this->conexion_elecc);
        $this->disconnect();

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }

    function getMesesData($params) { 
        $this->connect_bbdd_elecc();

        return $this->getData($params, "meses");        
    }

    function getEleccionsData($params) { 
        $this->connect_bbdd_elecc();
        return $this->getData($params, "eleccions","Denominacio", "DESC");        
    }

    function getColegisEleccionsData($IdC) { 
        $strSQL = " SELECT * FROM colegis where RefEleccio='" .$IdC. "' ";
        $strSQL .= " ORDER BY NomColegi";

        $this->connect_bbdd_elecc();
        $query = $this->consulta($strSQL);
        $this->disconnect();
                        
        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }        
    }

    function getMesesColegisEleccionsData($IdComici, $IdColegi) { 
        $strSQL = " SELECT * FROM meses where RefEleccio='" .$IdComici. "' and RefColegi='" .$IdColegi. "' ";
        $strSQL .= " ORDER BY Districte, Seccio, Lletra";

        $this->connect_bbdd_elecc();
        $query = $this->consulta($strSQL);
        $this->disconnect();
                        
        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }        
    }

    function getMesaData($IdComici, $IdColegi, $IdMesa) { 
        $strSQL = " SELECT * FROM meses where RefEleccio='" .$IdComici. 
                                       "' and RefColegi='" .$IdColegi. 
                                       "' and IdMesa='" .$IdMesa. "' ";

        $this->connect_bbdd_elecc();
        $query = $this->consulta($strSQL);
        $this->disconnect();
                        
        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }        
    }

    function getMesaResultatsCandidatures($IdComici, $IdColegi, $IdMesa) { 
        $strSQL = " SELECT candidatures.IdCandidatura, candidatures.Denominacio, candidatures.Sigles, resultats.Vots " . 
                  " FROM candidatures left join resultats on ( " . 
                  " candidatures.RefEleccio = resultats.RefEleccio " . 
                  " and candidatures.IdCandidatura = resultats.RefCandidatura " . 
                  " and resultats.RefColegi = '" .$IdColegi. "' " .
                  " and resultats.RefMesa = '" .$IdMesa. "' ) " .
                  " WHERE candidatures.RefEleccio ='" .$IdComici. "' " . 
                  " ORDER BY candidatures.Denominacio ";      
        
        $this->connect_bbdd_elecc();
        $query = $this->consulta($strSQL);
        $this->disconnect();
                        
        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }        

    }

    function updateRowParticipacio($variables = array()) { 
        if (($variables["IdMesa"] == null) || ($variables["IdMesa"] == '')) { 
            echo "Error! Heu de seleccionar una mesa per modificar-la.";
        } 
        else { 

            $strUpdateSQL = " UPDATE meses SET RangAlfabetic='" .str_replace("'","\'",$variables["RangAlfabetic"]). "'," .
                                              "CensVotants='" .str_replace("'","",$variables["CensVotants"]). "'," .
                                              "CensVotantsInicial='" .str_replace("'","",$variables["CensVotantsInicial"]). "'," .
                                              "CensVotantsFinal='" .str_replace("'","",$variables["CensVotantsFinal"]). "'," .
                                              "RepresentantAdmMesa='" .str_replace("'","\'",$variables["RepresentantAdmMesa"]). "'," .
                                              "TelRepresentantAdmMesa='" .str_replace("'","\'",$variables["TelRepresentantAdmMesa"]). "'," .
                                              "RepresentantTransmissioDadesMesa='" .str_replace("'","\'",$variables["RepresentantTransmissioDadesMesa"]). "'," .
                                              "TelRepresentantTransmissioDadesMesa='" .str_replace("'","\'",$variables["TelRepresentantTransmissioDadesMesa"]). "'," .
                                              "President='" .str_replace("'","\'",$variables["President"]). "'," .
                                              "TelefonPresident='" .str_replace("'","\'",$variables["TelefonPresident"]). "'," .
                                              "Vots1Participacio='" .str_replace("'","",$variables["Vots1Participacio"]). "'," .
                                              "Vots2Participacio='" .str_replace("'","",$variables["Vots2Participacio"]). "'," .
                                              "Vots3Participacio='" .str_replace("'","",$variables["Vots3Participacio"]). "'," .
                                              "Observacions='" .str_replace("'","\'",$variables["Observacions"]). "'" .
                                       " WHERE RefEleccio='" .$variables["RefEleccio"]. "'" .
                                       " AND RefColegi='" .$variables["RefColegi"]. "'" .
                                       " AND IdMesa='" .$variables["IdMesa"]. "'";

            $this->connect_bbdd_elecc();
            $query = $this->consulta($strUpdateSQL);
            $this->disconnect();

            if( !$query ) {
                $this->notifica_suceso("Error en updateRow", $strUpdateSQL);
            } 
            else { 
                return 1;
            }
        }
    }


    function updateRowParticipacioCurt($RefEleccio,$RefColegi,$IdMesa,$Vots1Participacio,$Vots2Participacio,$Vots3Participacio) { 
        if (($IdMesa == null) || ($IdMesa == '')) { 
            echo "Error! Heu de seleccionar una mesa per modificar-la.";
        } 
        else { 
            $strUpdateSQL = "UPDATE meses SET Vots1Participacio='" .str_replace("'","",$Vots1Participacio). "'," .
                                             " Vots2Participacio='" .str_replace("'","",$Vots2Participacio). "'," .
                                             " Vots3Participacio='" .str_replace("'","",$Vots3Participacio). "'" .
                                       " WHERE RefEleccio='" .$RefEleccio. "'" .
                                       " AND RefColegi='" .$RefColegi. "'" .
                                       " AND IdMesa='" .$IdMesa. "'";

            $this->connect_bbdd_elecc();
            $query = $this->consulta($strUpdateSQL);
            $this->disconnect();

            if( !$query ) {
                $this->notifica_suceso("Error en updateRow", $strUpdateSQL);
            } 
            else { 
                return 1;
            }
        }
    }

    function updateResultatsCandidaturaMesa($RefEleccio,$RefColegi,$IdMesa,$IdCandidatura,$Vots,$Districte,$Seccio) { 
        if (($IdMesa == null) || ($IdMesa == '')) { 
            echo "Error! Heu de seleccionar una mesa per modificar els resultats.";
        } 
        else { 

            $strUpdateSQL = " SELECT COUNT(*) as NumExist FROM resultats " .
                                       " WHERE RefEleccio='" .$RefEleccio. "'" .
                                       " AND RefColegi='" .$RefColegi. "'" .
                                       " AND RefMesa='" .$IdMesa. "'" . 
                                       " AND RefCandidatura='" .$IdCandidatura. "'";

            $this->connect_bbdd_elecc();
            $query = $this->consulta($strUpdateSQL);
            $this->disconnect();

            $afectedRows = 0;

             if($this->numero_de_filas($query)>0) {
                $items = array();
                if ($item = mysql_fetch_object($query)) {
                    $afectedRows = $item->NumExist;
                }
            }            

            if ($afectedRows==0) { 
                // INSERT
                $strUpdateSQL = " INSERT INTO resultats (RefEleccio,RefColegi,RefMesa,RefCandidatura,Vots,Districte,Seccio) VALUES ( ".
                                "'" .$RefEleccio. "'," . "'" .$RefColegi. "'," . "'" .$IdMesa. "'," . "'" .$IdCandidatura. "'," .
                                "'" .str_replace("'","",$Vots). "'," . "'" .$Districte. "'," . "'" .$Seccio. "' )";

                $this->connect_bbdd_elecc();
                $query = $this->consulta($strUpdateSQL);
                $this->disconnect();

                if( !$query ) {
                    $this->notifica_suceso("Error en updateRow", $strUpdateSQL);
                } 
            } else { 
                // UPDATE
                $strUpdateSQL = " UPDATE resultats SET Vots='" .str_replace("'","",$Vots). "'" .
                                           " WHERE RefEleccio='" .$RefEleccio. "'" .
                                           " AND RefColegi='" .$RefColegi. "'" .
                                           " AND RefMesa='" .$IdMesa. "'" . 
                                           " AND RefCandidatura='" .$IdCandidatura. "'";

                $this->connect_bbdd_elecc();
                $query = $this->consulta($strUpdateSQL);
                $this->disconnect();

                if( !$query ) {
                    $this->notifica_suceso("Error en updateRow", $strUpdateSQL);
                } 
            }
        }
    }
}