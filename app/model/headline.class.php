<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Mario Juan
 * Date: 13/04/15
 * Time: 08:00
 * To change this template use File | Settings | File Templates.
 */

namespace webtortosa;

class headline extends database {
    function getHeadlinesData($params, $start = NULL, $limit = NULL) {
        $this->connect();
        $strSQL = 'SELECT HEAD_HEADLINE.* FROM HEAD_HEADLINE ';
        $first_time = false;
        if($params) {
            foreach ($params as $key => $value) {
                if ($first_time)
                    $strSQL .= " AND " . $key . " LIKE '%" . $value . "%'";
                else {
                    $strSQL .= " WHERE " . $key . " LIKE '%" . $value . "%'";
                    $first_time = true;
                }
            }
        }

        $strSQL .= " ORDER BY HEAD_HEADLINE.DATA DESC";

        if(isset($start) && isset($limit))
            $strSQL .= " LIMIT ".$start.", ".$limit;

        //echo $strSQL . "<br>";
        $query = $this->consulta($strSQL, $this->conexion);
        $this->disconnect($this->conexion);

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }

    function getHeadlinesDataOrdF($params=null, $start = 0, $limit = 6) {
        $this->connect();
        $strSQL = 'SELECT DISTINCT HH.*
        FROM HEAD_HEADLINE AS HH
        WHERE HH.ACTIVA = 1
        ORDER BY HH.DATA DESC';

        if(isset($start) && isset($limit))
            $strSQL .= ' LIMIT '.$start.', '.$limit;

        //echo $strSQL."<br>";
        $query = $this->consulta($strSQL, $this->conexion);
        $this->disconnect($this->conexion);

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }

    /*
     * Consulta dades de les notícies per al llistat de l'apartat de notícies de la part pública.
     */
    function getHeadlinesDataForList($params=null, $start = NULL, $limit = NULL) {
        $first_time = true;
        $conn = $this->connect();
        $strSQL = 'SELECT HH.*
        FROM HEAD_HEADLINE AS HH
        ';

        if($params!=null) {
            if($params['data_inici']!="" && $params['data_fi']!="") {
                $strSQL .= ' WHERE HH.DATA BETWEEN "'.$params['data_inici'].'" AND "'.$params['data_fi'].'" ';
                $first_time = false;
            } else { 
                if ( (($params['data_inici'])!=NULL) && (trim($params['data_inici'])!="") ) {
                    $strSQL .= ' WHERE HH.DATA >= "'.$params['data_inici'].'" ';
                    $first_time = false;
                }  else { 
                    if ( (($params['data_fi'])!=NULL) && (trim($params['data_fi'])!="") ) {
                        $strSQL .= ' WHERE HH.DATA <= "'.$params['data_fi'].'" ';
                        $first_time = false;
                    }
                } 
            }

            if($params['text']!="") {
                if($first_time)
                    $strSQL .= ' WHERE HH.TITOL LIKE "%'.$params['text'].'%"';
                else
                    $strSQL .= ' AND HH.TITOL LIKE "%'.$params['text'].'%"';

                $first_time = false;
            }

            if($first_time)
                $strSQL .= ' WHERE HH.LOGIN  = "'.$params['login'].'"';
            else
                $strSQL .= ' AND HH.LOGIN  = "'.$params['login'].'"';
        }


        $strSQL .= ' ORDER BY HH.DATA DESC';

        if(isset($start) && isset($limit))
            $strSQL .= ' LIMIT '.$start.', '.$limit;

        //echo $strSQL."<br>";
        $query = $this->consulta($strSQL, $conn);
        $this->disconnect($conn);

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }

    /*
     * Consulta número de les notícies per al llistat de l'apartat de notícies de la part pública.
     */
    function getHeadlinesDataForListMaxium($params=null) {
        $this->connect();
        $strSQL = 'SELECT COUNT(*) as maxium
        FROM HEAD_HEADLINE AS HH
        WHERE HH.ACTIVA = 1 
        ';

        if($params!=null) {
            if($params['data_inici']!="" && $params['data_fi']!="") {
                $strSQL .= ' AND HH.DATA BETWEEN "'.$params['data_inici'].'" AND "'.$params['data_fi'].'" ';
            } else { 
                if ( (($params['data_inici'])!=NULL) && (trim($params['data_inici'])!="") ) {
                    $strSQL .= ' AND HH.DATA >= "'.$params['data_inici'].'" ';
                    $first_time = false;
                }  else { 
                    if ( (($params['data_fi'])!=NULL) && (trim($params['data_fi'])!="") ) {
                        $strSQL .= ' AND HH.DATA <= "'.$params['data_fi'].'" ';
                        $first_time = false;
                    }
                } 
            }

            if($params['text']!="") {
                $strSQL .= ' AND HH.TITOL LIKE "%'.$params['text'].'%"';
            }
        }

        //echo $strSQL."<br>";
        $query = $this->consulta($strSQL);

        $item = mysql_fetch_object($query);

        $this->disconnect($this->conexion);
        return $item->maxium;
    }

    //Paginació en articles d'opinio en grup polític
    function getHeadlinesDataArticlesOfPoliticGroupForListMaxium($params) {
        
       $this->connect();
        $strSQL = 'SELECT COUNT(*) as maxium
        FROM HEAD_HEADLINE AS HH
        ';

        if($params!=null) {
                $strSQL .= ' WHERE HH.LOGIN = "'.$params['LOGIN'].'"';
                if($params['LOGIN'] == 'pdecat'){
                    $strSQL .= ' OR HH.LOGIN = "ciu"';
                }
                $strSQL .= ' AND HH.ID_TIPOLOGIA = '.$params['ID_TIPOLOGIA'];
                $strSQL .= ' AND HH.ACTIVA = '.$params['ACTIVA'];
        }
        //echo $strSQL;

        $query = $this->consulta($strSQL);

        $item = mysql_fetch_object($query);

        $this->disconnect($this->conexion);
        return $item->maxium;

    }
    
    function getTematiquesHeadlinesData($params) {
        $this->connect();
        return $this->getData($params, "HEAD_TEMATICA");
    }

    /*********************************************INI YVES*/

    //Totes les entrades d'article d'opinió segons grup polític
    function getHeadlinesDataArticlesOfPoliticGroupForList($params=null, $start = NULL, $limit = NULL) {
        $first_time = true;
        $this->connect();
        $strSQL = 'SELECT HH.*
        FROM HEAD_HEADLINE AS HH
        ';

        if($params!=null) {
                $strSQL .= ' WHERE HH.LOGIN = "'.$params['LOGIN'].'"';
                if($params['LOGIN'] == 'pdecat'){
                    $strSQL .= ' OR HH.LOGIN = "ciu"';
                }
                $strSQL .= ' AND HH.ID_TIPOLOGIA = '.$params['ID_TIPOLOGIA'];
                $strSQL .= ' AND HH.ACTIVA = '.$params['ACTIVA'];
        }

        $strSQL .= ' ORDER BY HH.DATA DESC';

        if(isset($start) && isset($limit))
            $strSQL .= ' LIMIT '.$start.', '.$limit;

        //echo $strSQL."<br>";
        $query = $this->consulta($strSQL, $this->conexion);
        $this->disconnect($this->conexion);

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }

    

    /***********************************************FI YVES*/

}