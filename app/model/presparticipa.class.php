<?php
/**
 * Created by JetBrains PhpStorm.
 * User: CARMA
 * Date: 22/05/17
 * Time: 07:53
 * To change this template use File | Settings | File Templates.
 */

namespace webtortosa;


class execucio extends database {
    function getExecucioT () {
        $conn = $this->connect_bbdd_elecc();

        $strSQL = 'SELECT *
                          FROM PPART 
                          WHERE NUCLI = "Tortosa"
                          ORDER BY ID_PROPOSTA ASC';

        //echo $strSQL."<br>";
        $query = $this->consulta($strSQL, $conn);
        $this->disconnect($conn);

        if($this->numero_de_filas($query)>0) {
            //echo "Hi ha registres<br>";
            $items = array();
            $conta = 0;
            while ($item = mysql_fetch_object($query)) {
                        $items[] = $item;
                        //echo $item;
            }
            return $items;
        }
    }

   function getExecucioR () {
        $conn = $this->connect_bbdd_elecc();

        $strSQL = 'SELECT *
                          FROM PPART 
                          WHERE NUCLI = "Els Reguers"
                          ORDER BY ID_PROPOSTA ASC';

        //echo $strSQL."<br>";
        $query = $this->consulta($strSQL, $conn);
        $this->disconnect($conn);

        if($this->numero_de_filas($query)>0) {
            //echo "Hi ha registres<br>";
            $items = array();
            $conta = 0;

            while ($item = mysql_fetch_object($query)) {
                        $items[] = $item;
                        //echo $item;
            }
            
            return $items;
        }
    }

    function getExecucioV () {
        $conn = $this->connect_bbdd_elecc();

        $strSQL = 'SELECT *
                          FROM PPART 
                          WHERE NUCLI = "Vinallop"
                          ORDER BY ID_PROPOSTA ASC';

        //echo $strSQL."<br>";
        $query = $this->consulta($strSQL, $conn);
        $this->disconnect($conn);

        if($this->numero_de_filas($query)>0) {
            //echo "Hi ha registres<br>";
            $items = array();
            $conta = 0;

            while ($item = mysql_fetch_object($query)) {
                        $items[] = $item;
                        //echo $item;
            }
            return $items;
        }
    }

    



}
