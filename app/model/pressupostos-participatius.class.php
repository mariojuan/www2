<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Mario Juan
 * Date: 13/04/15
 * Time: 08:00
 * To change this template use File | Settings | File Templates.
 */

namespace webtortosa;

class pressupostos_participatius extends database {

    /* Mario's functions */

    // Realitza Backup de diferents taules
    function realitzaBackup($sufix) {
        $conn = $this->connect_bbdd_elecc_cli();
        $strSQL = '
            CREATE TABLE qtl153.PPART_PROPOSTA'.$sufix.' SELECT * FROM qtl153.PPART_PROPOSTA
        ';
        if ($stmt = $conn->prepare($strSQL)) {
            $stmt->execute();
        }

        $strSQL1 = '
            CREATE TABLE qtl153.PPART_URNA'.$sufix.' SELECT * FROM qtl153.PPART_URNA
        ';
        if ($stmt1 = $conn->prepare($strSQL1)) {
            $stmt1->execute();
        }

        $strSQL2 = '
            CREATE TABLE qtl153.LLISTA_VOTANTS'.$sufix.' SELECT * FROM qtl153.PPART_LLISTA_VOTANTS
        ';
        if ($stmt2 = $conn->prepare($strSQL2)) {
            $stmt2->execute();
        }

        mysqli_close($conn);
        return null;
    }

    //Consulta les dades de la taula del Cens.
    function getDataCens ($params) {
        $conn = $this->connect_bbdd_elecc_cli();
        $bind_param_value = array();

        //var_dump($params);
        $strSQL = 'SELECT PPART_CENS.*, PPART_PROCESP.DESCRIPCIO AS PROCESP FROM PPART_CENS
                    INNER JOIN PPART_PROCESP ON PPART_CENS.ID_PROCESP = PPART_PROCESP.ID AND PPART_PROCESP.ACTIU = 1';

        $primer_parametre = true;
        if(($params['dni_number'])&&($params['dni_letter'])) {
            $strSQL .= ' WHERE PPART_CENS.piiden = ? AND PPART_CENS.pilide = ?';
            $bind_param = "ss";
            $bind_param_value[] = $params['dni_number'];
            $bind_param_value[] = $params['dni_letter'];
        }
        if($params['passaport']) {
            if($primer_parametre) {
                $strSQL .= ' WHERE PPART_CENS.pinudo = ?';
                $primer_parametre = false;
            }
            else
                $strSQL .= ' AND PPART_CENS.pinudo = ?';
            $bind_param .= "s";
            $bind_param_value[] = $params['passaport'];
        }

        if(($params['tr_letter1'])&&($params['tr_number'])&&($params['tr_letter2'])) {
            if($primer_parametre) {
                $strSQL .= ' WHERE PPART_CENS.pilede = ? AND PPART_CENS.piiden = ? AND PPART_CENS.pilide = ?';
                $primer_parametre = false;
            }
            else {
                $strSQL .= ' AND PPART_CENS.pilede = ? AND PPART_CENS.piiden = ? AND PPART_CENS.pilide = ?';
            }
            $bind_param .= "sss";
            $bind_param_value[] = $params['tr_letter1'];
            $bind_param_value[] = $params['tr_number'];
            $bind_param_value[] = $params['tr_letter2'];
        }

        if($params['data_naixement']) {
            $strSQL .= ' AND PPART_CENS.ppfena = ?';
            $bind_param .= "s";
            $bind_param_value[] = $params['data_naixement'];
        }

        if($params['primer_cognom']) {
            $strSQL .= ' AND PPART_CENS.piape1 = ?';
            $bind_param .= "s";
            $bind_param_value[] = $params['primer_cognom'];
        }

        /*
        if($params['nom_carrer']) {
            $strSQL .= ' AND PPART_CENS.cadesc = ?';
            $bind_param .= "s";
            $bind_param_value[] = $params['nom_carrer'];
        }
        */
        //echo $strSQL;
        $a_params[] = & $bind_param;

        for($i=0; $i<count($bind_param_value); $i++) {
            $a_params[] = &$bind_param_value[$i];
        }

        $stmt = $conn->prepare($strSQL);

        if(count($bind_param_value)>0)
            call_user_func_array(array($stmt, 'bind_param'), $a_params);

        if(!$stmt->execute()) {
            die("Error" . $stmt->error);
        }
        $result = $stmt->get_result();
        $items = array();
        while ($item = $result->fetch_object()) {
            $items[] = $item;
        }

        $stmt->close();
        $conn->close();
        return $items;
    }

    //Consulta les dades de la taula de Registrats.
    function getRegistrat ($params) {
        $conn = $this->connect_bbdd_elecc_cli();
        $bind_param_value = array();

        //var_dump($params);

        $strSQL = 'SELECT PPART_REGISTRATS.*, PPART_PROCESP.DESCRIPCIO AS PROCESP FROM PPART_REGISTRATS
                    INNER JOIN PPART_PROCESP ON PPART_REGISTRATS.ID_PROCESP = PPART_PROCESP.ID AND PPART_PROCESP.ACTIU = 1';

        $primer_parametre = true;

        if(($params['dni_number'])&&($params['dni_letter'])) {
            $strSQL .= ' WHERE PPART_REGISTRATS.DOCUMENT = ? AND PPART_REGISTRATS.LLETRA2 = ?';
            $bind_param = "ss";
            $bind_param_value[] = $params['dni_number'];
            $bind_param_value[] = $params['dni_letter'];
            $primer_parametre = false;
        }
        if($params['passaport']) {
            if($primer_parametre) {
                $strSQL .= ' WHERE PPART_REGISTRATS.PASSAPORT = ?';
                $primer_parametre = false;
            }
            else
                $strSQL .= ' AND PPART_REGISTRATS.PASSAPORT = ?';
            $bind_param .= "s";
            $bind_param_value[] = $params['passaport'];
        }

        if(($params['tr_letter1'])&&($params['tr_number'])&&($params['tr_letter2'])) {
            if($primer_parametre) {
                $strSQL .= ' WHERE PPART_REGISTRATS.LLETRA1 = ? AND PPART_REGISTRATS.DOCUMENT = ? AND PPART_REGISTRATS.LLETRA2 = ?';
                $primer_parametre = false;
            }
            else {
                $strSQL .= ' AND PPART_REGISTRATS.LLETRA1 = ? AND PPART_REGISTRATS.DOCUMENT = ? AND PPART_REGISTRATS.LLETRA2 = ?';
            }
            $bind_param .= "sss";
            $bind_param_value[] = $params['tr_letter1'];
            $bind_param_value[] = $params['tr_number'];
            $bind_param_value[] = $params['tr_letter2'];
        }

        if($params['ID_CENS']) {
            if($primer_parametre) {
                $strSQL .= ' WHERE PPART_REGISTRATS.ID_CENS = ?';
                $primer_parametre = false;
            }
            else {
                $strSQL .= ' AND PPART_REGISTRATS.ID_CENS = ?';
            }
            $bind_param .= "i";
            $bind_param_value[] = $params['ID_CENS'];
        }

        if($params['EMAIL']) {
            if($primer_parametre) {
                $strSQL .= ' WHERE PPART_REGISTRATS.EMAIL = ?';
                $primer_parametre = false;
            }
            else {
                $strSQL .= ' AND PPART_REGISTRATS.EMAIL = ?';
            }
            $bind_param .= "s";
            $bind_param_value[] = $params['EMAIL'];
        }

        if($params['VALIDAT']) {
            if($primer_parametre) {
                $strSQL .= ' WHERE PPART_REGISTRATS.VALIDAT = ?';
                $primer_parametre = false;
            }
            else {
                $strSQL .= ' AND PPART_REGISTRATS.VALIDAT = ?';
            }
            $bind_param .= "i";
            $bind_param_value[] = $params['VALIDAT'];
        }

        if($params['PIN']) {
            if($primer_parametre) {
                $strSQL .= ' WHERE PPART_REGISTRATS.PIN = ?';
                $primer_parametre = false;
            }
            else {
                $strSQL .= ' AND PPART_REGISTRATS.PIN = ?';
            }
            $bind_param .= "s";
            $bind_param_value[] = $params['PIN'];
        }

        if($params['ID']) {
            if($primer_parametre) {
                $strSQL .= ' WHERE PPART_REGISTRATS.ID = ?';
                $primer_parametre = false;
            }
            else {
                $strSQL .= ' AND PPART_REGISTRATS.ID = ?';
            }
            $bind_param .= "i";
            $bind_param_value[] = $params['ID'];
        }

        //echo "<br>".$strSQL."<br>";
        $a_params[] = & $bind_param;

        for($i=0; $i<count($bind_param_value); $i++) {
            $a_params[] = &$bind_param_value[$i];
        }

        $stmt = $conn->prepare($strSQL);

        if(count($bind_param_value)>0)
            call_user_func_array(array($stmt, 'bind_param'), $a_params);

        if(!$stmt->execute()) {
            die("Error" . $stmt->error);
        }

        $result = $stmt->get_result();
        $items = array();
        while ($item = $result->fetch_object()) {
            $items[] = $item;
        }

        $stmt->close();
        $conn->close();
        return $items;
    }

    function addRegistrat($params) {
        $conn = $this->connect_bbdd_elecc_cli();
        $strSQL = 'INSERT INTO PPART_REGISTRATS
                   (LLETRA1, DOCUMENT, LLETRA2, PASSAPORT, EMAIL, TELEFON, PIN, ID_CENS, DATA_REGISTRE, HORA_REGISTRE, IP_DISPOSITIU, TIPUS_DISPOSITIU, VALIDAT, VOTACIO_EFECTUADA, ID_PROCESP)
                   VALUES
                   (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';

        if ($stmt = $conn->prepare($strSQL)) {
            $stmt->bind_param("sssssssissssiii", $params['tr_letter1'], $params['tr_number'], $params['tr_letter2'], $params['passaport'], $params['email'], $params['telefon'], $params['PIN'], $params['ID_CENS'], $params['DATA_REGISTRE'], $params['HORA_REGISTRE'], $params['IP_DISPOSITIU'], $params['TIPUS_DISPOSITIU'], $params['VALIDAT'], $params['VOTACIO_EFECTUADA'], $params['ID_PROCESP']);
            if (!$stmt->execute()) {
                die("Error" . $stmt->error);
            }
            $id = mysqli_stmt_insert_id($stmt);

            /* --- LOGS --- */
            $usuari = $_SESSION['info_usuari'];
            $params_input = array(
                'LLETRA1' => $params['tr_letter1'],
                'DOCUMENT' => $params['tr_number'],
                'LLETRA2' => $params['tr_letter2'],
                'PASSAPORT' => $params['passaport'],
                'EMAIL' => $params['email'],
                'TELEFON' => $params['telefon'],
                'EMAIL' => $params['email'],
                'PIN' => $params['pin'],
                'ID_CENS' => $params['ID_CENS'],
                'DATA_REGISTRE' => $params['DATA_REGISTRE'],
                'HORA_REGISTRE' => $params['HORA_REGISTRE'],
                'IP_DISPOSITIU' => $params['IP_DISPOSITIU'],
                'TIPUS_DISPOSITIU' => $params['TIPUS_DISPOSITIU'],
                'VALIDAT' => $params['VALIDAT'],
                'VOTACIO_EFECTUADA' => $params['VOTACIO_EFECTUADA'],
                'ID_PROCESP' => $params['ID_PROCESP']
            );
            $dades_logs = new \webtortosa\logs();
            foreach ($params_input as $key => $item) {
                $params_log = array(
                    "ACCIO" => "insert",
                    "USUARI" => 'Usuari Registrat',
                    "TAULA" => "PPART_REGISTRATS",
                    "CAMP" => $key,
                    "ID_REGISTRE" => $id,
                    "VALOR_ANTERIOR" => "",
                    "VALOR_NOU" => $item
                );
                $dades_logs->addLog($params_log);
            }
        }
        else {
            echo "Prepare failed: (" . $conn->errno . ") " . $conn->error;
        }
        /* --- FI LOGS --- */
        mysqli_close($conn);
        return $id;
    }

    function updateRegistrat($params) {

        $conn = $this->connect_bbdd_elecc_cli();

        $primer_parametre   = true;
        $params_old         = array();
        $params_new         = array();

        // Get data for logs
        $params_update = array(
            'ID'    => $params['ID']
        );
        $data_registrat = $this->getRegistrat($params_update);

        $strSQL = 'UPDATE PPART_REGISTRATS SET';

        if($params['VALIDAT']) {
            $strSQL .= ' VALIDAT = ?';
            $bind_param = "i";
            $bind_param_value[] = $params['VALIDAT'];
            $params_old['VALIDAT'] = $data_registrat[0]->VALIDAT;
            $params_new['VALIDAT'] = $params['VALIDAT'];
            $primer_parametre = false;
        }

        if($params['VOTACIO_EFECTUADA']) {
            if($primer_parametre) {
                $strSQL .= ' VOTACIO_EFECTUADA = ?';
                $bind_param = "i";
            }
            else {
                $strSQL .= ', VOTACIO_EFECTUADA = ?';
                $bind_param .= "i";
            }
            $bind_param_value[] = $params['VOTACIO_EFECTUADA'];
            $params_old['VOTACIO_EFECTUADA'] = $data_registrat[0]->VOTACIO_EFECTUADA;
            $params_new['VOTACIO_EFECTUADA'] = $params['VOTACIO_EFECTUADA'];
        }

        if($params['PIN']) {
            if($primer_parametre) {
                $strSQL .= ' PIN = ?';
                $bind_param = "s";
            }
            else {
                $strSQL .= ', PIN = ?';
                $bind_param .= "s";
            }
            $bind_param_value[] = $params['PIN'];
            $params_old['PIN'] = $data_registrat[0]->PIN;
            $params_new['PIN'] = $params['PIN'];
        }

        $strSQL .= ' WHERE ID = ?';
        $bind_param .= "i";
        $bind_param_value[] = $params['ID'];

        //echo $strSQL;
        $a_params[] = & $bind_param;

        for($i=0; $i<count($bind_param_value); $i++) {
            $a_params[] = &$bind_param_value[$i];
        }

        if ($stmt = $conn->prepare($strSQL)) {
            if (count($bind_param_value) > 0)
                call_user_func_array(array($stmt, 'bind_param'), $a_params);

            if (!$stmt->execute()) {
                mysqli_close($conn);
                die("Error" . $stmt->error);
            }
            /* --- LOGS --- */
            $dades_logs = new \webtortosa\logs();
            foreach($params_old as $key => $item) {
                $params_log = array(
                    "ACCIO"             => "update",
                    "USUARI"            => 'Usuari Registrat',
                    "TAULA"             => "PPART_REGISTRATS",
                    "CAMP"              => $key,
                    "ID_REGISTRE"       => $params['ID'],
                    "VALOR_ANTERIOR"    => $item,
                    "VALOR_NOU"         => $params_new[$key]
                );
                $dades_logs->addLog($params_log);
            }
            /* --- FI LOGS --- */
        }
        else {
            echo "Prepare failed: (" . $conn->errno . ") " . $conn->error;
        }
        $stmt->close();
        $conn->close();
        return null;
    }

    function getDataPP ($nom = null, $cognom1 = null, $cognom2 = null, $dni_number = null, $dni_letter = null, $passaport = null, $tr_letter1 = null, $tr_number = null, $tr_letter2 = null, $id=null) {
        $conn = $this->connect_bbdd_elecc_cli();
        if($nom && $cognom1) {
            $strSQL = "SELECT * FROM PPART_CENS WHERE piname LIKE ? AND piape1 LIKE ?";
        }

        if($nom && $cognom1 && $cognom2) {
            $strSQL = "SELECT * FROM PPART_CENS WHERE piname LIKE ? AND piape1 LIKE ? AND piape2 LIKE ?";
        }

        if(($dni_number)&&($dni_letter)) {
            $strSQL = 'SELECT *
                   FROM PPART_CENS
                   WHERE piiden = ? AND pilide = ?
                  ';
        }

        if($passaport) {
            $strSQL = 'SELECT *
                   FROM PPART_CENS
                   WHERE pinudo LIKE ?
                  ';
        }

        if(($tr_letter1)&&($tr_number)&&($tr_letter2)) {
            $strSQL = 'SELECT *
                   FROM PPART_CENS
                   WHERE pilede = ? AND piiden = ? AND pilide = ?
                  ';
        }
        //echo $strSQL;

        if($id) {
            $strSQL = 'SELECT *
                   FROM PPART_CENS
                   WHERE pinume = ?
                  ';
        }

        if ($stmt = $conn->prepare($strSQL)) {

            if($nom && $cognom1) {
                $nom = '%'.$nom.'%';
                $cognom1 = '%'.$cognom1.'%';
                $stmt->bind_param("ss", $nom, $cognom1);
            }
            if($nom && $cognom1 && $cognom2) {
                $nom = '%'.$nom.'%';
                $cognom1 = '%'.$cognom1.'%';
                $cognom2 = '%'.$cognom2.'%';
                $stmt->bind_param("sss", $nom, $cognom1, $cognom2);
            }
            if(($dni_number)&&($dni_letter)) {
                $stmt->bind_param("ss", $dni_number, $dni_letter);
            }
            if($passaport) {
                $passaport = '%'.$passaport.'%';
                $stmt->bind_param("s", $passaport);
            }

            if(($tr_letter1)&&($tr_number)&&($tr_letter2)) {
                $stmt->bind_param("sis", $tr_letter1, $tr_number, $tr_letter2);
            }

            if($id) {
                $stmt->bind_param("i", $id);
            }


            $stmt->execute();

            // Get the result
            $result = $stmt->get_result();

            $num_of_rows = $result->num_rows;

            $items = array();

            while ($row = $result->fetch_object()) {
                $items[] = $row;
            }
            $stmt->close();
            $conn->close();
            return $items;

        }
        $conn->close();
    }

    function getEstPP ($lloc = null) {
        $conn = $this->connect_bbdd_elecc_cli();

        $strSQL = 'SELECT COUNT(*) as total_vots
                   FROM PPART_CENS
                   WHERE LLOC_VOTACIO = ?
                  ';

        //echo $strSQL."<br>";
        if ($stmt = $conn->prepare($strSQL)) {
            $stmt->bind_param("s", $lloc);
            $stmt->execute();

            /* Get the result */
            $result = $stmt->get_result();

            $num_of_rows = $result->num_rows;

            $items = array();
            while ($row = $result->fetch_object()) {
                $items[] = $row;
            }
            mysqli_close($conn);
            return $items;
        }
        mysqli_close($conn);
        return null;
    }

    function getEstPPTotals() {
        $conn = $this->connect_bbdd_elecc_cli();
        $strSQL = '
            SELECT COUNT(*) as total_vots, LLOC_VOTACIO FROM `PPART_CENS` WHERE HA_VOTAT=1 GROUP BY LLOC_VOTACIO
        ';
        //echo $strSQL."<br>";
        if ($stmt = $conn->prepare($strSQL)) {
            $stmt->execute();

            /* Get the result */
            $result = $stmt->get_result();

            $num_of_rows = $result->num_rows;

            $items = array();
            while ($row = $result->fetch_object()) {
                $items[] = $row;
            }
            mysqli_close($conn);
            return $items;
        }
        mysqli_close($conn);
        return null;
    }

    function getCensPP() {
        $conn = $this->connect_bbdd_elecc_cli();
        $strSQL = '
            SELECT COUNT(*) as total_vots FROM `PPART_CENS`
        ';
        //echo $strSQL."<br>";
        if ($stmt = $conn->prepare($strSQL)) {
            $stmt->execute();

            /* Get the result */
            $result = $stmt->get_result();

            $num_of_rows = $result->num_rows;

            $items = array();
            while ($row = $result->fetch_object()) {
                $items[] = $row;
            }
            mysqli_close($conn);
            return $items;
        }
        mysqli_close($conn);
        return null;
    }

    function UpdateVotacio($id, $seu) {
        $conn = $this->connect_bbdd_elecc_cli();
        /*
        $strSQL = 'UPDATE PPART2017 SET
                   HORA_VOTACIO = "'.date('Y-m-d H:i:s').'",
                   LLOC_VOTACIO = "'.$_SESSION['info_usuari']['login'].'"
                   WHERE pinume = '.$id;
        */
        $strSQL = '
                   UPDATE PPART_CENS SET
                   HORA_VOTACIO = ?, LLOC_VOTACIO = ?, HA_VOTAT = 1
                   WHERE pinume = ?
                   ';

        //echo $strSQL."<br>";
        //echo " - " . $id . " - " . $seu ."<br>" ;

        if ($stmt = $conn->prepare($strSQL)) {
            $data = date('Y-m-d H:i:s');
            $lloc = $seu;
            $stmt->bind_param("ssi", $data, $lloc, $id);
            $stmt->execute();
            mysqli_close($conn);
            return true;
        }
        mysqli_close($conn);
        return null;
    }

   function UpdateRegistratVotacioEfectuada($id) {
        $conn = $this->connect_bbdd_elecc_cli();
        $strSQL = '
                   UPDATE PPART_REGISTRATS SET
                   VOTACIO_EFECTUADA = 1
                   WHERE ID_CENS = ?
                   ';

        if ($stmt = $conn->prepare($strSQL)) {
            $data = date('Y-m-d H:i:s');
            //$lloc = $seu;
            $stmt->bind_param("i", $id);
            $stmt->execute();
            mysqli_close($conn);
            return true;
        }
        mysqli_close($conn);
        return null;
    }

    function haVotatPPartCens($ID_CENS) {
        $haVotat = false;
        $conn = $this->connect_bbdd_elecc_cli();
        $strSQL = '
            SELECT HA_VOTAT FROM PPART_CENS WHERE  pinume = ?
        ';
        
        //echo $strSQL."<br>";
        //echo $ID_CENS;

        if ($stmt = $conn->prepare($strSQL)) {           
            $stmt->bind_param("i", $ID_CENS);
            $stmt->execute();
            
            
            // Get the result 
            $result = $stmt->get_result();
            $row = $result->fetch_object();

            if ($row->HA_VOTAT == 1) {
                    $haVotat = true;
            }

            mysqli_close($conn);
            return $haVotat;
            
        }
        

        mysqli_close($conn);
        return $haVotat;
    }

    function getNextIdPapereta($idProcesP) {
        $conn = $this->connect_bbdd_elecc_cli();
        $strSQL = '
                   SELECT MAX(ID_PAPERETA) AS MAX_ID_PAPERETA FROM PPART_VOT WHERE ID_PROCESP=?
                   ';

        if ($stmt = $conn->prepare($strSQL)) {
            $stmt->bind_param("i", $idProcesP);
            $stmt->execute();
            $result = $stmt->get_result();

            $value=0;

            if ($row = $result->fetch_object()) {
                $value = $row->MAX_ID_PAPERETA;
            }

            $value = $value + 1;

            mysqli_close($conn);
            return $value;
        }

        mysqli_close($conn);
        return null;
    }


    function addVot($idProcesP, $idPapereta, $idVot, $numProposta, $data, $hora, $hash) {
        $conn = $this->connect_bbdd_elecc_cli();
        $strSQL = 'INSERT INTO PPART_VOT
                   (ID_PROCESP,ID_PAPERETA,ID_VOT,NUM_PROPOSTA,DATA,HORA,HASH)
                   VALUES
                   (?, ?, ?, ?, ?, ?, ?)';

        if ($stmt = $conn->prepare($strSQL)) {
            $stmt->bind_param("iiissss", $idProcesP, $idPapereta, $idVot, $numProposta, $data, $hora, $hash);
            if (!$stmt->execute()) {
                die("Error" . $stmt->error);
            }
            $id = mysqli_stmt_insert_id($stmt);

            /* --- LOGS --- */
            $usuari = $_SESSION['info_usuari'];
            $params_input = array(
                'ID_PROCESP' => $idProcesP,
                'ID_PAPERETA' => $idPapereta,
                'ID_VOT' => $idVot,
                'NUM_PROPOSTA' => $numProposta,
                'DATA' => $data,
                'HORA' => $hora,
                'HASH' => $hash
            );
            $dades_logs = new \webtortosa\logs();
            foreach ($params_input as $key => $item) {
                $params_log = array(
                    "ACCIO" => "insert",
                    "USUARI" => 'Usuari Registrat',
                    "TAULA" => "PPART_VOT",
                    "CAMP" => $key,
                    "ID_REGISTRE" => $id,
                    "VALOR_ANTERIOR" => "",
                    "VALOR_NOU" => $item
                );
                $dades_logs->addLog($params_log);
            }
            
        }
        else {
            echo "Prepare failed: (" . $conn->errno . ") " . $conn->error;
        }
        /* --- FI LOGS --- */

        mysqli_close($conn);
        return $id;
    }

    function addLlistaVotants($idProcesP, $idCens, $data, $hora, $idDispositiu, $tipusDispositiu) {
        $conn = $this->connect_bbdd_elecc_cli();
        $strSQL = 'INSERT INTO PPART_LLISTA_VOTANTS
                   (ID_PROCESP,ID_CENS,DATA,HORA,ID_DISPOSITIU,TIPUS_DISPOSITIU)
                   VALUES
                   (?, ?, ?, ?, ?, ?)';

        if ($stmt = $conn->prepare($strSQL)) {
            $stmt->bind_param("iissss", $idProcesP, $idCens, $data, $hora, $idDispositiu, $tipusDispositiu);
            if (!$stmt->execute()) {
                die("Error" . $stmt->error);
            }
            $id = mysqli_stmt_insert_id($stmt);

            /* --- LOGS --- */
            $usuari = $_SESSION['info_usuari'];
            $params_input = array(
                'ID_PROCESP' => $idProcesP,
                'ID_CENS' => $idCens,
                'DATA' => $data,
                'HORA' => $hora,
                'ID_DISPOSITIU' => $idDispositiu,
                'TIPUS_DISPOSITIU' => $tipusDispositiu
            );
            $dades_logs = new \webtortosa\logs();
            foreach ($params_input as $key => $item) {
                $params_log = array(
                    "ACCIO" => "insert",
                    "USUARI" => 'Usuari Registrat',
                    "TAULA" => "PPART_LLISTA_VOTANTS",
                    "CAMP" => $key,
                    "ID_REGISTRE" => $id,
                    "VALOR_ANTERIOR" => "",
                    "VALOR_NOU" => $item
                );
                $dades_logs->addLog($params_log);
            }
            
        }
        else {
            echo "Prepare failed: (" . $conn->errno . ") " . $conn->error;
        }
        /* --- FI LOGS --- */

        mysqli_close($conn);
        return $id;
    }

    
    public function existeixIEsPropostaValida($proposta) {
        $existeixiEsValida = false;

        $conn = $this->connect_bbdd_elecc_cli();

        $strSQL = 'SELECT PPART_PROPOSTA.NUM_PROPOSTA  FROM PPART_PROPOSTA
                    INNER JOIN PPART_PROCESP ON ( PPART_PROPOSTA.ID_PROCESP = PPART_PROCESP.ID AND PPART_PROCESP.ACTIU = 1 ) 
                    WHERE PPART_PROPOSTA.NUM_PROPOSTA=?';

        if ($stmt = $conn->prepare($strSQL)) {
            $stmt->bind_param("i", $proposta);
            $stmt->execute();
            $result = $stmt->get_result();

            $value=0;

            if ($row = $result->fetch_object()) {
                $value = $row->NUM_PROPOSTA;
            }

            if ($value > 0) { 
                $existeixiEsValida = true;
            }

            mysqli_close($conn);
            return $existeixiEsValida;
        }

        mysqli_close($conn);

        return $existeixiEsValida;        
    }

    // Consulta el llistat de Propostes del Pressupost Participatiu Vigent
    public function getPropostes() {
        $existeixiEsValida = false;

        $conn = $this->connect_bbdd_elecc_cli();

        $strSQL = 'SELECT PPART_PROPOSTA.NUM_PROPOSTA, PPART_PROPOSTA.DESC_CURTA, PPART_PROPOSTA.IMPORT  FROM PPART_PROPOSTA
                    INNER JOIN PPART_PROCESP ON ( PPART_PROPOSTA.ID_PROCESP = PPART_PROCESP.ID AND PPART_PROCESP.ACTIU = 1 )';

        if ($stmt = $conn->prepare($strSQL)) {
            $stmt->execute();

            /* Get the result */
            $result = $stmt->get_result();

            $num_of_rows = $result->num_rows;

            $items = array();
            while ($row = $result->fetch_object()) {
                $items[] = $row;
            }
            mysqli_close($conn);
            return $items;
        }

        mysqli_close($conn);
        return null;
    }


    function UpdateSumaVotProposta($idProces, $numProposta, $hash) {
        $conn = $this->connect_bbdd_elecc_cli();
        $strSQL = '
                   UPDATE PPART_PROPOSTA SET VOTS=VOTS+1, HASH=?
                   WHERE ID_PROCESP=? AND NUM_PROPOSTA=?
                   ';

        if ($stmt = $conn->prepare($strSQL)) {
            $stmt->bind_param("sii", $hash, $idProces, $numProposta);
            $stmt->execute();
            mysqli_close($conn);
            return true;
        }
        mysqli_close($conn);
        return null;
    }

    function addVotUrna($idProcesP, $numProposta, $hash) {
        $conn = $this->connect_bbdd_elecc_cli();
        $strSQL = 'INSERT INTO PPART_URNA
                   (ID_PROCESP,NUM_PROPOSTA,HASH)
                   VALUES
                   (?, ?, ?)';

        if ($stmt = $conn->prepare($strSQL)) {
            $stmt->bind_param("iss", $idProcesP, $numProposta, $hash);
            if (!$stmt->execute()) {
                die("Error" . $stmt->error);
            }
            $id = mysqli_stmt_insert_id($stmt);

            /* --- LOGS --- */
            $usuari = $_SESSION['info_usuari'];
            $params_input = array(
                'ID_PROCESP' => $idProcesP,
                'NUM_PROPOSTA' => $numProposta,
                'HASH' => $hash
            );
            $dades_logs = new \webtortosa\logs();
            foreach ($params_input as $key => $item) {
                $params_log = array(
                    "ACCIO" => "insert",
                    "USUARI" => 'Usuari Registrat',
                    "TAULA" => "PPART_URNA",
                    "CAMP" => $key,
                    "ID_REGISTRE" => $id,
                    "VALOR_ANTERIOR" => "",
                    "VALOR_NOU" => $item
                );
                $dades_logs->addLog($params_log);
            }
            
        }
        else {
            echo "Prepare failed: (" . $conn->errno . ") " . $conn->error;
        }
        /* --- FI LOGS --- */

        mysqli_close($conn);
        return $id;
    }

    function getNumVotsProposta($numProposta) { 
        $conn = $this->connect_bbdd_elecc_cli();

        $strSQL = 'SELECT PPART_PROPOSTA.VOTS  FROM PPART_PROPOSTA
                    INNER JOIN PPART_PROCESP ON ( PPART_PROPOSTA.ID_PROCESP = PPART_PROCESP.ID AND PPART_PROCESP.ACTIU = 1 ) 
                    WHERE PPART_PROPOSTA.NUM_PROPOSTA=?';

        if ($stmt = $conn->prepare($strSQL)) {
            $stmt->bind_param("i", $numProposta);
            $stmt->execute();
            $result = $stmt->get_result();

            $value=0;

            if ($row = $result->fetch_object()) {
                $value = $row->VOTS;
            }

            mysqli_close($conn);
            return $value;
        }

        mysqli_close($conn);
        return null;
       }


}