<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Mario Juan
 * Date: 13/04/15
 * Time: 08:00
 * To change this template use File | Settings | File Templates.
 */

namespace webtortosa;

class tematica extends database {
	function getTematicaData($params) {
		$this->connect();
		return $this->getData($params, "COM_TEMATICA");
	}
    function getTematiquesForHeadlines($id_headline) {
        $this->connect();
        $strSQL = '
          SELECT *
          FROM COM_TEMATICA
          INNER JOIN HEAD_TEMATICA ON HEAD_TEMATICA.ID_TEMATICA = COM_TEMATICA.ID
          WHERE HEAD_TEMATICA.ID_HEADLINE = '.$id_headline;

        $query = $this->consulta($strSQL, $this->conexion);
        $this->disconnect($this->conexion);

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }
}