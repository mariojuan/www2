<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Mario Juan
 * Date: 18/01/14
 * Time: 22:39
 * To change this template use File | Settings | File Templates.
 */
/*
CLASE PARA LA CONEXION Y LA GESTION DE LA BASE DE DATOS Y LA PAGINA WEB
*/
namespace webtortosa;

class database {

    private $conexion;
    /*
    Metodo para conectar con la base de datos
    */
    public function connect()
    {
        if(!isset($this->conexion))
        {
            $this->conexion = (mysql_connect("217.76.143.20","qte872","Tortosa2015")) or die(mysql_error());
            mysql_select_db("qte872",$this->conexion) or die(mysql_error());
            mysql_set_charset('utf8', $this->conexion);
        }
    }

    public function connect_bbdd_elecc()
    {
        if(!isset($this->conexion))
        {   
            /*
            $this->conexion_elecc = (mysql_connect("217.76.143.24","qia239","062227Xxx")) or die(mysql_error());
            mysql_select_db("qia239",$this->conexion_elecc) or die(mysql_error());
            */
            $this->conexion = (mysql_connect("217.76.143.23","qtl153","A123456b")) or die(mysql_error());
            mysql_select_db("qtl153",$this->conexion) or die(mysql_error());
            return $this->conexion;
        }
    }

    public function connect_bbddd_sql_server() {
        $serverName = "lwdd131.servidoresdns.net"; //serverName\instanceName
        $conn = mssql_connect( $serverName, 'qdt399', 'Tortosa4321');

        if( $conn ) {
            //echo "Conexión establecida.<br />";
            return $conn;
        }else{
            //echo "Conexión no se pudo establecer.<br />";
            die( print_r( sqlsrv_errors(), true));
        }
    }

    public function connect_mysqlcli() {
        $myConnection = mysqli_connect("217.76.143.20", "qte872", "Tortosa2015", "qte872") or die ("could not connect to mysql");
        $myConnection->set_charset("utf8");
        return $myConnection;
    }
    
    public function connect_bbdd_elecc_cli()
    {
        $myConnection = mysqli_connect("217.76.143.23", "qtl153", "A123456b", "qtl153") or die ("could not connect to mysql");
        if( $myConnection ) {
            return $myConnection;
        }else{
            echo "Conexión no se pudo establecer.<br />";
            die( print_r( sqlsrv_errors(), true));
        }
    }

    /*
    Metodo para realizar una consulta
    Input:  $sql | codigo sql para ejecutar la consulta
    Output: $result
    */
    public function consulta($sql)
    {
        $resultado = mysql_query($sql,$this->conexion);
        if(!$resultado){
            echo 'MySQL Error: ' . mysql_error();
            $this->notifica_suceso("Error en consulta", $sql);
            exit;
        }
        return $resultado;
    }

    /*
    Metodo para realizar una consulta a partir de parámetros y tabla
    Input:  $params parámetros de la consulta | $table tabla a consultar
    Output: $result
    */
    function getData($params, $table, $order_field="", $sort="", $start = NULL, $limit = NULL) {

        $strSQL = "SELECT * FROM ".$table." WHERE 1 = 1";

        foreach($params as $key => $value) {
            $strSQL .= " AND `" . $key . "` = '" . $value . "'";
        }

        if(($order_field!="")&&($sort!=""))
            $strSQL .= " ORDER BY $order_field $sort";

        if(isset($start) && isset($limit))
            $strSQL .= " LIMIT ".$start.", ".$limit;

        //echo $strSQL . "<br>";
        $query = $this->consulta($strSQL);
        $this->disconnect($this->conexion);

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }

    /*
    Metodo para contar el numero de resultados
    input:  $result
    Output: cantidad de registros encontrados
    */
    function numero_de_filas($result){
        if(!is_resource($result)) return false;
        return mysql_num_rows($result);
    }

    /*
    Metodo para crear array desde una consulta
    INPUT: $result
    OUTPUT: array con los resultados de una consulta
    */
    function fetch_assoc($result){
        if(!is_resource($result)) return false;
        return mysql_fetch_assoc($result);
    }

    /*
    Metodo para insertar un array a una fila de la tabla
    INPUT: $table (nombre de la tabla) | $data (array con los datos a insertar) | $exclude (array de datos a excluir)
    OUTPUT: array con los resultados de la inserción
    */
    function addRow($table, $data, $exclude = array()) {
        $this->connect();
        $fields = $values = array();

        if( !is_array($exclude) ) $exclude = array($exclude);

        foreach( array_keys($data) as $key ) {
            if( !in_array($key, $exclude) ) {
                if($data[$key]) {
                    $fields[] = "`$key`";
                    $values[] = "'" . $data[$key] . "'";    
                }
                
            }
        }

        $fields = implode(",", $fields);
        $values = implode(",", $values);
        $strSQL = "INSERT INTO $table ($fields) VALUES ($values)";
        //echo $strSQL."<br>";
        if( mysql_query($strSQL, $this->conexion) ) {
            $query_results = array( "mysql_error" => false,
                "mysql_insert_id" => mysql_insert_id($this->conexion),
                "mysql_affected_rows" => mysql_affected_rows($this->conexion),
                "mysql_info" => mysql_info()
            );
            $this->disconnect($this->conexion);
            return $query_results;
        } else {
            $this->disconnect($this->conexion);
            $this->notifica_suceso("Error en addRow", $strSQL ."-->".mysql_error());
            return array( "mysql_error" => mysql_error() );
        }
    }

    /*
    Metodo para modificar una fila de la tabla a partir de un array
    INPUT: $table (nombre de la tabla) | $data (array con los datos a modificar)
    OUTPUT:
    */
    function updateRow($table, $data, $exclude = array()) {
        $this->connect();
        $fields_values = array();

        if( !is_array($exclude) ) $exclude = array($exclude);

        foreach( array_keys($data) as $key ) {
            $fields_values[] = "`$key` = '" . mysql_real_escape_string($data[$key]) . "'";
        }
        $conditional = array_pop($fields_values);
        $fields_values = implode(",", $fields_values);

        $strSQL = "UPDATE $table SET $fields_values WHERE $conditional";
        //echo $strSQL."<br>";
        if( !mysql_query($strSQL) ) {
            $this->notifica_suceso("Error en updateRow", $strSQL);
            return array( "mysql_error" => mysql_error() );
        }
    }

    /*
     Metodo para eliminar una fila de la tabla a partir de un campo
     INPUT: $table (nombre de la tabla) | $key (nombre del campo) | $value (valor del campo)
     OUTPUT: array con los errores posibles de la consulta
    */
    public function deleteRow($table, $key, $value) {
        $this->connect();
        $strSQL = "DELETE FROM $table WHERE $key = '".$value."'";
        //echo $strSQL."<br>";
        if( !mysql_query($strSQL, $this->conexion) ) {
            $this->notifica_suceso("Error en deleteRow", $strSQL);
            return array( "mysql_error" => mysql_error() );
        }
    }

    /*
    Metodo para cerrar la conexion a la base de datos
    */
    public function disconnect($con) {
        if(!is_null($con))
            mysql_close($con);
    }

    public function disconnect_sql_server($con) {
        mssql_close($con);
    }

    //envia un email informando de algún error o realizando alguna notificación
    function notifica_suceso($textofuncion, $strSQL){
        $texto_email = $textofuncion."<br>".$strSQL;
        mail(EMAIL_AVERIA,"Gestor www2.tortosa.cat - suceso -- ".date("d-m-Y H:i:s"),$texto_email,"Content-type: text/html\r\nFrom:www2.tortosa.cat <no-reply@tortosa.cat>");
    }

}
?>