<?php
/**
 * Created by JetBrains PhpStorm.
 * User: CARMA
 * Date: 22/05/17
 * Time: 07:53
 * To change this template use File | Settings | File Templates.
 */

namespace webtortosa;

class borsa extends database {
   /* CARMA's functions */

    function getBorsa ($tipus = NULL, $start = NULL, $limit = NULL, $codi = null) {
        $conn = $this->connect_bbddd_sql_server();

        // En el cas d'una fitxa
        if($codi) {
            $strSQL = 'SELECT Borsa.Codi,
                       Borsa.anunci,
                       CAST(Borsa.objecte AS TEXT) as objecte,
                       Borsa.denominacio,
                       Borsa.regim_juridic,
                       Borsa.caracter,
                       Borsa.escala,
                       Borsa.subescala,
                       Borsa.descripcio_curta,
                       Borsa.classe,
                       Borsa.grup,
                       Borsa.num_places,
                       Borsa.seleccio,
                       CAST(Borsa.requisits AS TEXT) as requisits,
                       Borsa.presen_solicituds,
                       Borsa.data_inici_presen,
                       Borsa.data_final_presen,
                       Borsa.drets_examen,
                       Borsa.bases,
                       Borsa.solicitud,
                       Borsa.destinacio,
                       Borsa.llista_admesos_exclosos,
                       Borsa.llista_admesos_exclosos_definitiva,
                       Borsa.doc_annexa,
                       Borsa.desc_doc_annexa,
                       Borsa.resultats1,
                       Borsa.borsa_provisional,
                       Borsa.borsa_definitiva
                       FROM Borsa
                       WHERE
                       Borsa.activa = 1
                       AND Borsa.baixa = 0
                       AND Borsa.codi = '.$codi;

            //echo $strSQL."<br>";
            $query = mssql_query($strSQL, $conn);
            $this->disconnect_sql_server($conn);

            if (mssql_num_rows($query) > 0) {
                //echo "Hi ha registres<br>";


                while ($item = mssql_fetch_object($query)) {
                        $items[] = $item;
                }
                //var_dump($items);
                return $items;
            } else {
                //echo "NOOO hi ha registres<br>";
            }
        }
        else {
            $strSQL = 'SELECT Borsa.Codi,
                       Borsa.descripcio_curta
                       FROM Borsa
                       WHERE Borsa.activa = 1
                       AND Borsa.baixa = 0
                       AND Borsa.div <= \'' . date('Y/m/d') . '\'
                       AND Borsa.dfv >= \'' . date('Y/m/d') . '\'
                       AND Borsa.destinacio = 2
                       ORDER BY Borsa.codi DESC';


            //echo $strSQL."<br>";
            $query = mssql_query($strSQL, $conn);
            $this->disconnect_sql_server($conn);

            if (mssql_num_rows($query) > 0) {
                //echo "Hi ha registres<br>";

                $items = array();
                $conta = 0;

                while ($item = mssql_fetch_object($query)) {
                    $conta++;

                    if (isset($start) && isset($limit)) {
                        if (($conta > $start) && ($conta <= ($start + $limit))) {
                            $items[] = $item;
                            //echo $item;
                        }
                    } else {
                        $items[] = $item;
                    }
                }
                return $items;
            } else {
                //echo "NOOO hi ha registres<br>";
            }
        }
    }
  
}