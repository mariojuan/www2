<?php
/**
 * Created by JetBrains PhpStorm.
 * User: CARMA
 * Date: 22/05/17
 * Time: 07:53
 * To change this template use File | Settings | File Templates.
 */

namespace webtortosa;

class juntes_govern extends database {

    /* CARMA's functions */

    function getJuntesGovern ($year = NULL, $start = NULL, $limit = NULL) {
        $conn = $this->connect_bbddd_sql_server();

        if (!isset($year)) { 
           $year = date("Y"); 
        }

        $strSQL = 'SELECT year(Os.data_inici) as currentYear, month(Os.data_inici) as currentMonth,
                          OS.id,OS.data_inici,OS.nom_arxiu, OS.tamany_arxiu,OS.tipus_sessio,OS.actiu,
                          OS.tipus_document,OS.descripcio as desc_lliure,OO.descripcio 
                          FROM OGO_SESSIO_SIMPLE OS 
                             INNER JOIN OGO_ORGAN OO ON OS.ID_ORGAN=OO.ID 
                          WHERE OS.Actiu=1 
                            and year(Os.data_inici)='.$year.  
                           ' and OO.descripcio=\'Juntes de Govern Local\' 
                          order by year(Os.data_inici) desc, Os.data_inici asc';

        //echo $strSQL."<br>";
        $query = mssql_query($strSQL, $conn);
        $this->disconnect_sql_server($conn);
    
        if(mssql_num_rows($query) > 0) {
            //echo "Hi ha registres<br>";

            $items = array();
            $conta = 0;

            while($item = mssql_fetch_object($query)) {
                $conta++;

                //if (isset($start) && isset($limit)) {
                //    if (($conta>$start) && ($conta<=($start+$limit))) { 
                //        $items[] = $item;
                        //echo $item;
                //    }
                //} else { 
                    $items[] = $item;
                //}
            }
            return $items;
        } else { 
            //echo "NOOO hi ha registres<br>";
        }
    }



}
