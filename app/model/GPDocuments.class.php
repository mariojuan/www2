<?php
/**
 * Created by JetBrains PhpStorm.
 * User: CARMA
 * Date: 22/05/17
 * Time: 07:53
 * To change this template use File | Settings | File Templates.
 */

namespace webtortosa;

class GPDocuments extends database {

    /* CARMA's functions */

    function getGPDocuments ($idEns = NULL, $idServei = NULL, $start = NULL, $limit = NULL) {
        $conn = $this->connect_bbddd_sql_server();
      
        $strSQL = 'SELECT ID_DOCUMENT, DESCRIPCIOCURTA, FITXER, ACTIU, DATA, CODIIDIOMA, SERVEI 
                     FROM GM_DOCUMENT 
                    WHERE ENS=' .$idEns.
                    ' AND SERVEI=' .$idServei. 
                    ' AND ACTIU = 1 
                    ORDER BY DATA DESC';

        //echo $strSQL."<br>";
        $query = mssql_query($strSQL, $conn);
        $this->disconnect_sql_server($conn);
    
        if(mssql_num_rows($query) > 0) {
            //echo "Hi ha registres<br>";

            $items = array();
            $conta = 0;

            while($item = mssql_fetch_object($query)) {
                $conta++;

                if (isset($start) && isset($limit)) {
                    if (($conta>$start) && ($conta<=($start+$limit))) { 
                        $items[] = $item;
                        //echo $item;
                    }
                } else { 
                    $items[] = $item;
                }
            }
            return $items;
        } else { 
            //echo "NOOO hi ha registres<br>";
        }
    }



}
