<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Mario Juan
 * Date: 13/04/15
 * Time: 08:00
 * To change this template use File | Settings | File Templates.
 */

namespace webtortosa;

class indicador extends database {

    /* Grups */
    function addIndicadorGrup($params)
    {
        //var_dump($params);
        $conn = $this->connect_mysqlcli();

        $strSQL = 'INSERT INTO IND_INDICADOR_GRUP
                   (CODI_GRUP_INDICADOR, TIPUS_INDICADOR, EXERCICI, GRUP_INDICADOR, ID_USUARI, ID_SERVEI, BAIXA, VISIBLE)
                   VALUES
                   (?, ?, ?, ?, ?, ?, ?, ?)';

        if ($stmt = $conn->prepare($strSQL)) {
            $stmt->bind_param("ssisiiii", $params['CODI_GRUP_INDICADOR'], $params['TIPUS_INDICADOR'], $params['EXERCICI'], $params['GRUP_INDICADOR'], $params['ID_USUARI'], $params['ID_SERVEI'], $params['BAIXA'], $params['VISIBLE']);
            if(!$stmt->execute()) {
                die("Error" . $stmt->error);
            }

            $id = mysqli_stmt_insert_id($stmt);

            /* --- LOGS --- */
            $usuari = $_SESSION['info_usuari'];
            $params_input = array(
                'CODI_GRUP_INDICADOR'   => $params['CODI_GRUP_INDICADOR'],
                'TIPUS_INDICADOR'       => $params['TIPUS_INDICADOR'],
                'EXERCICI'              => $params['EXERCICI'],
                'GRUP_INDICADOR'        => $params['GRUP_INDICADOR'],
                'ID_USUARI'             => $params['ID_USUARI'],
                'ID_SERVEI'             => $params['ID_SERVEI'],
                'BAIXA'                 => $params['BAIXA'],
                'VISIBLE'               => $params['VISIBLE']
            );
            $dades_logs = new \webtortosa\logs();
            foreach($params_input as $key => $item) {
                $params_log = array(
                    "ACCIO"             => "insert",
                    "USUARI"            => $usuari["login"],
                    "TAULA"             => "IND_INDICADOR_GRUP",
                    "CAMP"              => $key,
                    "ID_REGISTRE"       => $id,
                    "VALOR_ANTERIOR"    => "",
                    "VALOR_NOU"         => $item
                );
                $dades_logs->addLog($params_log);
            }
            /* --- FI LOGS --- */

            mysqli_close($conn);
            return $id;
        }
        mysqli_close($conn);
        return null;

    }

    function getIndicadorGrup($id = null, $id_usuari = null, $id_servei = null, $start = null, $limit = null, $visible = null, $baixa = null)
    {
        $first_parameter = false;
        $bind_param_value = array();
        $myConnection = $this->connect_mysqlcli();

        $strSQL = "SELECT ID, CODI_GRUP_INDICADOR, TIPUS_INDICADOR, EXERCICI, GRUP_INDICADOR, ID_USUARI, ID_SERVEI, BAIXA, VISIBLE
                   FROM IND_INDICADOR_GRUP";

        if (isset($id)) {
            $first_parameter = true;
            $strSQL .= " WHERE ID = ?";
            $bind_param = "i";
            $bind_param_value[] = (int)$id;
        }
        if (isset($id_usuari)) {
            if($first_parameter) {
                $strSQL .= " AND ID_USUARI = ?";
            }
            else {
                $strSQL .= " WHERE ID_USUARI = ?";
                $first_parameter = true;
            }
            $bind_param .= "i";
            $bind_param_value[] = $id_usuari;
        }
        if (isset($id_servei)) {
            if($first_parameter) {
                $strSQL .= " AND ID_SERVEI = ?";
            }
            else {
                $strSQL .= " WHERE ID_SERVEI = ?";
                $first_parameter = true;
            }
            $bind_param .= "i";
            $bind_param_value[] = (int)$id_servei;
        }
        if (isset($visible)) {
            if($first_parameter) {
                $strSQL .= " AND VISIBLE = ?";
            }
            else {
                $strSQL .= " WHERE VISIBLE = ?";
                $first_parameter = true;
            }
            $bind_param .= "i";
            $bind_param_value[] = (int)$visible;
        }
        if (isset($baixa)) {
            if($first_parameter) {
                $strSQL .= " AND BAIXA = ?";
            }
            else {
                $strSQL .= " WHERE BAIXA = ?";
                $first_parameter = true;
            }
            $bind_param .= "i";
            $bind_param_value[] = (int)$baixa;
        }
        if (isset($start) && isset($limit)) {
            $strSQL .= " LIMIT ?, ?";
            $bind_param .= "ii";
            $bind_param_value[] = (int)$start;
            $bind_param_value[] = (int)$limit;
        }
        $a_params[] = & $bind_param;

        for($i=0; $i<count($bind_param_value); $i++) {
            $a_params[] = &$bind_param_value[$i];
        }
        $stmt = $myConnection->prepare($strSQL);
        //echo $strSQL;
        //var_dump($a_params);

        if(count($bind_param_value)>0)
            call_user_func_array(array($stmt, 'bind_param'), $a_params);
        $stmt->execute();
        $result = $stmt->get_result();
        $items = array();
        while ($item = $result->fetch_object()) {
            $items[] = $item;
        }
        $stmt->close();
        $myConnection->close();
        return $items;
    }

    function updateIndicadorGrup($params) {

        $conn = $this->connect_mysqlcli();

        // Get data for logs
        $data_indicador_grup = $this->getIndicadorGrup($params['ID']);

        $strSQL = 'UPDATE IND_INDICADOR_GRUP SET
                   CODI_GRUP_INDICADOR = ?, TIPUS_INDICADOR = ? , EXERCICI = ?, GRUP_INDICADOR = ?, BAIXA = ? , VISIBLE = ?
                   WHERE ID = ?';


        if ($stmt = $conn->prepare($strSQL)) {
            //var_dump($params);
            $stmt->bind_param("ssisiii", $params['CODI_GRUP_INDICADOR'], $params['TIPUS_INDICADOR'], $params['EXERCICI'], $params['GRUP_INDICADOR'], $params['BAIXA'], $params['VISIBLE'], $params['ID']);
            if(!$stmt->execute()) {
                mysqli_close($conn);
                die("Error" . $stmt->error);
            }
            /* --- LOGS --- */
            $params_old = array(
                'CODI_GRUP_INDICADOR'   => $data_indicador_grup[0]->CODI_GRUP_INDICADOR,
                'TIPUS_INDICADOR'       => $data_indicador_grup[0]->TIPUS_INDICADOR,
                'EXERCICI'              => $data_indicador_grup[0]->EXERCICI,
                'GRUP_INDICADOR'        => $data_indicador_grup[0]->GRUP_INDICADOR,
                'ID_USUARI'             => $data_indicador_grup[0]->ID_USUARI,
                'ID_SERVEI'             => $data_indicador_grup[0]->ID_SERVEI,
                'BAIXA'                 => $data_indicador_grup[0]->BAIXA,
                'VISIBLE'               => $data_indicador_grup[0]->VISIBLE
            );
            $params_new = array(
                'CODI_GRUP_INDICADOR'   => $params['CODI_GRUP_INDICADOR'],
                'TIPUS_INDICADOR'       => $params['TIPUS_INDICADOR'],
                'EXERCICI'              => $params['EXERCICI'],
                'GRUP_INDICADOR'        => $params['GRUP_INDICADOR'],
                'ID_USUARI'             => $params['ID_USUARI'],
                'ID_SERVEI'             => $params['ID_SERVEI'],
                'BAIXA'                 => $params['BAIXA'],
                'VISIBLE'               => $params['VISIBLE']
            );

            $usuari = $_SESSION['info_usuari'];
            $dades_logs = new \webtortosa\logs();
            foreach($params_old as $key => $item) {
                $params_log = array(
                    "ACCIO"             => "update",
                    "USUARI"            => $usuari["login"],
                    "TAULA"             => "IND_INDICADOR_GRUP",
                    "CAMP"              => $key,
                    "ID_REGISTRE"       => $params['ID'],
                    "VALOR_ANTERIOR"    => $item,
                    "VALOR_NOU"         => $params_new[$key]
                );
                $dades_logs->addLog($params_log);
            }

            /* --- FI LOGS --- */
        }
        else {
            die("Error en el sql");
        }
        mysqli_close($conn);
        return null;
    }

    function deleteIndicadorGrup($id) {
        $conn = $this->connect_mysqlcli();

        // Get data for logs
        $data_indicador_grup = $this->getIndicadorGrup($id);

        $strSQL = 'DELETE FROM IND_INDICADOR_GRUP
                   WHERE ID = ?';

        if ($stmt = $conn->prepare($strSQL)) {
            $stmt->bind_param("i", $id);
            if(!$stmt->execute()) {
                die("Error" . $stmt->error);
            }
            /* --- LOGS --- */
            $params_old = array(
                'CODI_GRUP_INDICADOR'   => $data_indicador_grup[0]->CODI_GRUP_INDICADOR,
                'TIPUS_INDICADOR'       => $data_indicador_grup[0]->TIPUS_INDICADOR,
                'EXERCICI'              => $data_indicador_grup[0]->EXERCICI,
                'GRUP_INDICADOR'        => $data_indicador_grup[0]->GRUP_INDICADOR,
                'ID_USUARI'             => $data_indicador_grup[0]->ID_USUARI,
                'ID_SERVEI'             => $data_indicador_grup[0]->ID_SERVEI,
                'BAIXA'                 => $data_indicador_grup[0]->BAIXA,
                'VISIBLE'               => $data_indicador_grup[0]->VISIBLE
            );
            $usuari = $_SESSION['info_usuari'];
            $dades_logs = new \webtortosa\logs();
            foreach($params_old as $key => $item) {
                $params_log = array(
                    "ACCIO"             => "delete",
                    "USUARI"            => $usuari["login"],
                    "TAULA"             => "IND_INDICADOR_GRUP",
                    "CAMP"              => $key,
                    "ID_REGISTRE"       => $id,
                    "VALOR_ANTERIOR"    => $item,
                    "VALOR_NOU"         => ""
                );
                $dades_logs->addLog($params_log);
            }

            /* --- FI LOGS --- */
            mysqli_close($conn);
            return $id;
        }
        mysqli_close($conn);
        return null;
    }

    /* SubGrups */
    function addIndicadorSubGrup($params)
    {
        //var_dump($params);
        $conn = $this->connect_mysqlcli();

        $strSQL = 'INSERT INTO IND_INDICADOR_SUBGRUP
                   (CODI_SUBGRUP_INDICADOR, TIPUS_INDICADOR, EXERCICI, SUBGRUP_INDICADOR, ID_GRUP, ID_USUARI, ID_SERVEI, BAIXA, VISIBLE)
                   VALUES
                   (?, ?, ?, ?, ?, ?, ?, ?, ?)';

        if ($stmt = $conn->prepare($strSQL)) {
            $stmt->bind_param("ssisiiiii", $params['CODI_SUBGRUP_INDICADOR'], $params['TIPUS_INDICADOR'], $params['EXERCICI'], $params['SUBGRUP_INDICADOR'], $params['ID_GRUP'], $params['ID_USUARI'], $params['ID_SERVEI'], $params['BAIXA'], $params['VISIBLE']);
            if(!$stmt->execute()) {
                die("Error" . $stmt->error);
            }
            $id = mysqli_stmt_insert_id($stmt);

            /* --- LOGS --- */
            $usuari = $_SESSION['info_usuari'];
            $params_input = array(
                'CODI_SUBGRUP_INDICADOR'    => $params['CODI_SUBGRUP_INDICADOR'],
                'TIPUS_INDICADOR'           => $params['TIPUS_INDICADOR'],
                'EXERCICI'                  => $params['EXERCICI'],
                'ID_GRUP'                   => $params['ID_GRUP'],
                'SUBGRUP_INDICADOR'         => $params['SUBGRUP_INDICADOR'],
                'ID_USUARI'                 => $params['ID_USUARI'],
                'ID_SERVEI'                 => $params['ID_SERVEI'],
                'BAIXA'                     => $params['BAIXA'],
                'VISIBLE'                   => $params['VISIBLE']
            );
            $dades_logs = new \webtortosa\logs();
            foreach($params_input as $key => $item) {
                $params_log = array(
                    "ACCIO"             => "insert",
                    "USUARI"            => $usuari["login"],
                    "TAULA"             => "IND_INDICADOR_SUBGRUP",
                    "CAMP"              => $key,
                    "ID_REGISTRE"       => $id,
                    "VALOR_ANTERIOR"    => "",
                    "VALOR_NOU"         => $item
                );
                $dades_logs->addLog($params_log);
            }
            /* --- FI LOGS --- */
            mysqli_close($conn);
            return $id;
        }
        mysqli_close($conn);
        return null;

    }

    function getIndicadorSubGrup($id = null, $id_usuari = null, $id_servei = null, $start = null, $limit = null, $visible = null, $baixa = null)
    {
        $first_parameter = false;
        $bind_param_value = array();
        $myConnection = $this->connect_mysqlcli();

        $strSQL = "SELECT IIS.ID, IIS.CODI_SUBGRUP_INDICADOR, IIS.TIPUS_INDICADOR, IIS.EXERCICI, IIS.SUBGRUP_INDICADOR, IIS.ID_GRUP, IIS.ID_USUARI, IIS.ID_SERVEI, IIS.BAIXA, IIS.VISIBLE, IG.GRUP_INDICADOR
                   FROM IND_INDICADOR_SUBGRUP IIS
                   LEFT JOIN IND_INDICADOR_GRUP IG ON IG.ID = IIS.ID_GRUP";

        if ($id) {
            $first_parameter = true;
            $strSQL .= " WHERE IIS.ID = ?";
            $bind_param = "i";
            $bind_param_value[] = (int)$id;
        }
        if ($id_usuari) {
            if($first_parameter) {
                $strSQL .= " AND IIS.ID_USUARI = ?";
            }
            else {
                $strSQL .= " WHERE IIS.ID_USUARI = ?";
                $first_parameter = true;
            }
            $bind_param .= "i";
            $bind_param_value[] = $id_usuari;
        }
        if ($id_servei) {
            if($first_parameter) {
                $strSQL .= " AND IIS.ID_SERVEI = ?";
            }
            else {
                $strSQL .= " WHERE IIS.ID_SERVEI = ?";
                $first_parameter = true;
            }
            $bind_param .= "i";
            $bind_param_value[] = (int)$id_servei;
        }
        if ($visible) {
            if($first_parameter) {
                $strSQL .= " AND IIS.VISIBLE = ?";
            }
            else {
                $strSQL .= " WHERE IIS.VISIBLE = ?";
                $first_parameter = true;
            }
            $bind_param .= "i";
            $bind_param_value[] = (int)$visible;
        }
        if ($baixa) {
            if($first_parameter) {
                $strSQL .= " AND IIS.BAIXA = ?";
            }
            else {
                $strSQL .= " WHERE IIS.BAIXA = ?";
                $first_parameter = true;
            }
            $bind_param .= "i";
            $bind_param_value[] = (int)$baixa;
        }
        if (isset($start) && isset($limit)) {
            $strSQL .= " LIMIT ?, ?";
            $bind_param .= "ii";
            $bind_param_value[] = (int)$start;
            $bind_param_value[] = (int)$limit;
        }
        $a_params[] = & $bind_param;
        for($i=0; $i<count($bind_param_value); $i++) {
            $a_params[] = &$bind_param_value[$i];
        }
        $stmt = $myConnection->prepare($strSQL);
        //echo $strSQL;
        //var_dump($a_params);
        if(count($bind_param_value)>0)
            call_user_func_array(array($stmt, 'bind_param'), $a_params);
        $stmt->execute();
        $result = $stmt->get_result();
        $items = array();
        while ($item = $result->fetch_object()) {
            $items[] = $item;
        }
        $stmt->close();
        $myConnection->close();
        return $items;
    }

    function updateIndicadorSubGrup($params) {

        $conn = $this->connect_mysqlcli();

        // Get data for logs
        $data_indicador_subgrup = $this->getIndicadorSubGrup($params['ID']);

        $strSQL = 'UPDATE IND_INDICADOR_SUBGRUP SET
                   CODI_SUBGRUP_INDICADOR = ?, TIPUS_INDICADOR = ? , EXERCICI = ?, SUBGRUP_INDICADOR = ?, ID_GRUP = ?, BAIXA = ? , VISIBLE = ?
                   WHERE ID = ?';


        if ($stmt = $conn->prepare($strSQL)) {
            //var_dump($params);
            $stmt->bind_param("ssisiiii", $params['CODI_SUBGRUP_INDICADOR'], $params['TIPUS_INDICADOR'], $params['EXERCICI'], $params['SUBGRUP_INDICADOR'], $params['ID_GRUP'], $params['BAIXA'], $params['VISIBLE'], $params['ID']);
            if(!$stmt->execute()) {
                mysqli_close($conn);
                die("Error" . $stmt->error);
            }
            /* --- LOGS --- */
            $params_old = array(
                'CODI_SUBGRUP_INDICADOR'    => $data_indicador_subgrup[0]->CODI_SUBGRUP_INDICADOR,
                'TIPUS_INDICADOR'           => $data_indicador_subgrup[0]->TIPUS_INDICADOR,
                'EXERCICI'                  => $data_indicador_subgrup[0]->EXERCICI,
                'SUBGRUP_INDICADOR'         => $data_indicador_subgrup[0]->SUBGRUP_INDICADOR,
                'ID_GRUP'                   => $data_indicador_subgrup[0]->ID_GRUP,
                'ID_USUARI'                 => $data_indicador_subgrup[0]->ID_USUARI,
                'ID_SERVEI'                 => $data_indicador_subgrup[0]->ID_SERVEI,
                'BAIXA'                     => $data_indicador_subgrup[0]->BAIXA,
                'VISIBLE'                   => $data_indicador_subgrup[0]->VISIBLE
            );
            $params_new = array(
                'CODI_SUBGRUP_INDICADOR'    => $params['CODI_SUBGRUP_INDICADOR'],
                'TIPUS_INDICADOR'           => $params['TIPUS_INDICADOR'],
                'EXERCICI'                  => $params['EXERCICI'],
                'SUBGRUP_INDICADOR'         => $params['SUBGRUP_INDICADOR'],
                'ID_GRUP'                   => $params['ID_GRUP'],
                'ID_USUARI'                 => $params['ID_USUARI'],
                'ID_SERVEI'                 => $params['ID_SERVEI'],
                'BAIXA'                     => $params['BAIXA'],
                'VISIBLE'                   => $params['VISIBLE']
            );
            $usuari = $_SESSION['info_usuari'];
            $dades_logs = new \webtortosa\logs();
            foreach($params_old as $key => $item) {
                $params_log = array(
                    "ACCIO"             => "update",
                    "USUARI"            => $usuari["login"],
                    "TAULA"             => "IND_INDICADOR_SUBGRUP",
                    "CAMP"              => $key,
                    "ID_REGISTRE"       => $params['ID'],
                    "VALOR_ANTERIOR"    => $item,
                    "VALOR_NOU"         => $params_new[$key]
                );
                $dades_logs->addLog($params_log);
            }

            /* --- FI LOGS --- */
        }
        else {
            die("Error en el sql");
        }
        mysqli_close($conn);
        return null;
    }

    function deleteIndicadorSubGrup($id) {
        $conn = $this->connect_mysqlcli();

        // Get data for logs
        $data_indicador_subgrup = $this->getIndicadorSubGrup($id);

        $strSQL = 'DELETE FROM IND_INDICADOR_SUBGRUP
                   WHERE ID = ?';

        if ($stmt = $conn->prepare($strSQL)) {
            $stmt->bind_param("i", $id);
            if(!$stmt->execute()) {
                die("Error" . $stmt->error);
            }
            /* --- LOGS --- */
            $params_old = array(
                'CODI_SUBGRUP_INDICADOR'    => $data_indicador_subgrup[0]->CODI_SUBGRUP_INDICADOR,
                'TIPUS_INDICADOR'           => $data_indicador_subgrup[0]->TIPUS_INDICADOR,
                'EXERCICI'                  => $data_indicador_subgrup[0]->EXERCICI,
                'SUBGRUP_INDICADOR'         => $data_indicador_subgrup[0]->SUBGRUP_INDICADOR,
                'ID_USUARI'                 => $data_indicador_subgrup[0]->ID_USUARI,
                'ID_SERVEI'                 => $data_indicador_subgrup[0]->ID_SERVEI,
                'BAIXA'                     => $data_indicador_subgrup[0]->BAIXA,
                'VISIBLE'                   => $data_indicador_subgrup[0]->VISIBLE
            );
            $usuari = $_SESSION['info_usuari'];
            $dades_logs = new \webtortosa\logs();
            foreach($params_old as $key => $item) {
                $params_log = array(
                    "ACCIO"             => "delete",
                    "USUARI"            => $usuari["login"],
                    "TAULA"             => "IND_INDICADOR_SUBGRUP",
                    "CAMP"              => $key,
                    "ID_REGISTRE"       => $id,
                    "VALOR_ANTERIOR"    => $item,
                    "VALOR_NOU"         => ""
                );
                $dades_logs->addLog($params_log);
            }

            /* --- FI LOGS --- */
            mysqli_close($conn);
            return $id;
        }
        mysqli_close($conn);
        return null;
    }

    /* Indicadors */
    function addIndicador($params)
    {
        //var_dump($params);
        $conn = $this->connect_mysqlcli();

        $strSQL = 'INSERT INTO IND_INDICADOR
                   (CODI_INDICADOR, TIPUS_INDICADOR, EXERCICI, INDICADOR, ESTAT, COMENTARIS, ID_GRUP, ID_SUBGRUP, ID_USUARI, ID_SERVEI, BAIXA, VISIBLE)
                   VALUES
                   (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';

        if ($stmt = $conn->prepare($strSQL)) {
            $stmt->bind_param("ssisssiiiiii", $params['CODI_INDICADOR'], $params['TIPUS_INDICADOR'], $params['EXERCICI'], $params['INDICADOR'], $params['ESTAT'], $params['COMENTARIS'], $params['ID_GRUP'], $params['ID_SUBGRUP'], $params['ID_USUARI'], $params['ID_SERVEI'], $params['BAIXA'], $params['VISIBLE']);
            if(!$stmt->execute()) {
                die("Error" . $stmt->error);
            }
            $id = mysqli_stmt_insert_id($stmt);
            /* --- LOGS --- */
            $usuari = $_SESSION['info_usuari'];
            $params_input = array(
                'TIPUS_INDICADOR'           => $params['TIPUS_INDICADOR'],
                'CODI_INDICADOR'            => $params['CODI_INDICADOR'],
                'EXERCICI'                  => $params['EXERCICI'],
                'INDICADOR'                 => $params['INDICADOR'],
                'ESTAT'                     => $params['ESTAT'],
                'COMENTARIS'                => $params['COMENTARIS'],
                'ID_GRUP'                   => $params['ID_GRUP'],
                'ID_SUBGRUP'                => $params['ID_SUBGRUP'],
                'ID_USUARI'                 => $params['ID_USUARI'],
                'ID_SERVEI'                 => $params['ID_SERVEI'],
                'BAIXA'                     => $params['BAIXA'],
                'VISIBLE'                   => $params['VISIBLE']
            );
            $dades_logs = new \webtortosa\logs();
            foreach($params_input as $key => $item) {
                $params_log = array(
                    "ACCIO"             => "insert",
                    "USUARI"            => $usuari["login"],
                    "TAULA"             => "IND_INDICADOR",
                    "CAMP"              => $key,
                    "ID_REGISTRE"       => $id,
                    "VALOR_ANTERIOR"    => "",
                    "VALOR_NOU"         => $item
                );
                $dades_logs->addLog($params_log);
            }
            /* --- FI LOGS --- */
            mysqli_close($conn);
            return $id;
        }
        mysqli_close($conn);
        return null;

    }

    function getIndicador($id = null, $id_usuari = null, $id_servei = null, $start = null, $limit = null, $visible = null, $baixa = null, $field_sort = null, $sort = null)
    {
        $first_parameter = false;
        $bind_param_value = array();
        $myConnection = $this->connect_mysqlcli();

        $strSQL = "SELECT IND.ID, IND.CODI_INDICADOR, IND.TIPUS_INDICADOR, IND.EXERCICI, IND.INDICADOR, IND.ID_GRUP, IND.ID_SUBGRUP, IND.ESTAT, IND.TIPUS_INDICADOR, IND.COMENTARIS, IND.ID_SERVEI, IND.ID_USUARI, IND.BAIXA, IND.VISIBLE, IG.GRUP_INDICADOR, IIS.SUBGRUP_INDICADOR
                   FROM IND_INDICADOR IND
                   LEFT JOIN IND_INDICADOR_GRUP IG ON IG.ID = IND.ID_GRUP
                   LEFT JOIN IND_INDICADOR_SUBGRUP IIS ON IIS.ID = IND.ID_SUBGRUP";

        if ($id) {
            $first_parameter = true;
            $strSQL .= " WHERE IND.ID = ?";
            $bind_param = "i";
            $bind_param_value[] = (int)$id;
        }
        if ($id_usuari) {
            if($first_parameter) {
                $strSQL .= " AND IND.ID_USUARI = ?";
            }
            else {
                $strSQL .= " WHERE IND.ID_USUARI = ?";
                $first_parameter = true;
            }
            $bind_param .= "i";
            $bind_param_value[] = $id_usuari;
        }
        if ($id_servei) {
            if($first_parameter) {
                $strSQL .= " AND IND.ID_SERVEI = ?";
            }
            else {
                $strSQL .= " WHERE IND.ID_SERVEI = ?";
                $first_parameter = true;
            }
            $bind_param .= "i";
            $bind_param_value[] = (int)$id_servei;
        }
        if ($visible) {
            if($first_parameter) {
                $strSQL .= " AND IND.VISIBLE = ?";
            }
            else {
                $strSQL .= " WHERE IND.VISIBLE = ?";
                $first_parameter = true;
            }
            $bind_param .= "i";
            $bind_param_value[] = (int)$visible;
        }
        if ($baixa) {
            if($first_parameter) {
                $strSQL .= " AND IND.BAIXA = ?";
            }
            else {
                $strSQL .= " WHERE IND.BAIXA = ?";
                $first_parameter = true;
            }
            $bind_param .= "i";
            $bind_param_value[] = (int)$baixa;
        }
        if(isset($field_sort) && (isset($sort))) {
            $strSQL .= " ORDER BY " . $field_sort . " " . $sort;
        }
        if (isset($start) && isset($limit)) {
            $strSQL .= " LIMIT ?, ?";
            $bind_param .= "ii";
            $bind_param_value[] = (int)$start;
            $bind_param_value[] = (int)$limit;
        }
        $a_params[] = & $bind_param;
        for($i=0; $i<count($bind_param_value); $i++) {
            $a_params[] = &$bind_param_value[$i];
        }
        $stmt = $myConnection->prepare($strSQL);
        //echo $strSQL;
        //var_dump($a_params);
        call_user_func_array(array($stmt, 'bind_param'), $a_params);
        $stmt->execute();
        $result = $stmt->get_result();
        $items = array();
        while ($item = $result->fetch_object()) {
            $items[] = $item;
        }
        $stmt->close();
        $myConnection->close();
        return $items;
    }

    function updateIndicador($params) {

        $conn = $this->connect_mysqlcli();

        // Get data for logs
        $data_indicador = $this->getIndicador($params['ID']);

        $strSQL = 'UPDATE IND_INDICADOR SET
                   CODI_INDICADOR = ?, TIPUS_INDICADOR = ? , EXERCICI = ?, INDICADOR = ?, ESTAT = ?, COMENTARIS = ?, ID_GRUP = ?, ID_SUBGRUP  = ?,  BAIXA = ? , VISIBLE = ?
                   WHERE ID = ?';


        if ($stmt = $conn->prepare($strSQL)) {
            //var_dump($params);
            $stmt->bind_param("ssisssiiiii", $params['CODI_INDICADOR'], $params['TIPUS_INDICADOR'], $params['EXERCICI'], $params['INDICADOR'], $params['ESTAT'], $params['COMENTARIS'], $params['ID_GRUP'], $params['ID_SUBGRUP'], $params['BAIXA'], $params['VISIBLE'], $params['ID']);
            if(!$stmt->execute()) {
                mysqli_close($conn);
                die("Error" . $stmt->error);
            }
            /* --- LOGS --- */
            $params_old = array(
                'TIPUS_INDICADOR'           => $data_indicador[0]->TIPUS_INDICADOR,
                'CODI_INDICADOR'            => $data_indicador[0]->CODI_INDICADOR,
                'EXERCICI'                  => $data_indicador[0]->EXERCICI,
                'INDICADOR'                 => $data_indicador[0]->INDICADOR,
                'ESTAT'                     => $data_indicador[0]->ESTAT,
                'COMENTARIS'                => $data_indicador[0]->COMENTARIS,
                'ID_GRUP'                   => $data_indicador[0]->ID_GRUP,
                'ID_SUBGRUP'                => $data_indicador[0]->ID_SUBGRUP,
                'ID_USUARI'                 => $data_indicador[0]->ID_USUARI,
                'ID_SERVEI'                 => $data_indicador[0]->ID_SERVEI,
                'BAIXA'                     => $data_indicador[0]->BAIXA,
                'VISIBLE'                   => $data_indicador[0]->VISIBLE
            );
            $params_new = array(
                'TIPUS_INDICADOR'   => $params['TIPUS_INDICADOR'],
                'CODI_INDICADOR'    => $params['CODI_INDICADOR'],
                'EXERCICI'          => $params['EXERCICI'],
                'INDICADOR'         => $params['INDICADOR'],
                'ESTAT'             => $params['ESTAT'],
                'COMENTARIS'        => $params['COMENTARIS'],
                'ID_GRUP'           => $params['ID_GRUP'],
                'ID_SUBGRUP'        => $params['ID_SUBGRUP'],
                'ID_USUARI'         => $params['ID_USUARI'],
                'ID_SERVEI'         => $params['ID_SERVEI'],
                'BAIXA'             => $params['BAIXA'],
                'VISIBLE'           => $params['VISIBLE']
            );
            $usuari = $_SESSION['info_usuari'];
            $dades_logs = new \webtortosa\logs();
            foreach($params_old as $key => $item) {
                $params_log = array(
                    "ACCIO"             => "update",
                    "USUARI"            => $usuari["login"],
                    "TAULA"             => "IND_INDICADOR",
                    "CAMP"              => $key,
                    "ID_REGISTRE"       => $params['ID'],
                    "VALOR_ANTERIOR"    => $item,
                    "VALOR_NOU"         => $params_new[$key]
                );
                $dades_logs->addLog($params_log);
            }
            /* --- FI LOGS --- */
        }
        else {
            die("Error en el sql");
        }
        mysqli_close($conn);
        return null;
    }

    function deleteIndicador($id) {
        $conn = $this->connect_mysqlcli();

        // Get data for logs
        $data_indicador = $this->getIndicador($id);

        $strSQL = 'DELETE FROM IND_INDICADOR
                   WHERE ID = ?';

        if ($stmt = $conn->prepare($strSQL)) {
            $stmt->bind_param("i", $id);
            if(!$stmt->execute()) {
                die("Error" . $stmt->error);
            }

            /* --- LOGS --- */
            $params_old = array(
                'TIPUS_INDICADOR'           => $data_indicador[0]->TIPUS_INDICADOR,
                'CODI_INDICADOR'            => $data_indicador[0]->CODI_INDICADOR,
                'EXERCICI'                  => $data_indicador[0]->EXERCICI,
                'INDICADOR'                 => $data_indicador[0]->INDICADOR,
                'ESTAT'                     => $data_indicador[0]->ESTAT,
                'COMENTARIS'                => $data_indicador[0]->COMENTARIS,
                'ID_GRUP'                   => $data_indicador[0]->ID_GRUP,
                'ID_SUBGRUP'                => $data_indicador[0]->ID_SUBGRUP,
                'ID_USUARI'                 => $data_indicador[0]->ID_USUARI,
                'ID_SERVEI'                 => $data_indicador[0]->ID_SERVEI,
                'BAIXA'                     => $data_indicador[0]->BAIXA,
                'VISIBLE'                   => $data_indicador[0]->VISIBLE
            );

            $usuari = $_SESSION['info_usuari'];
            $dades_logs = new \webtortosa\logs();
            foreach($params_old as $key => $item) {
                $params_log = array(
                    "ACCIO"             => "delete",
                    "USUARI"            => $usuari["login"],
                    "TAULA"             => "IND_INDICADOR",
                    "CAMP"              => $key,
                    "ID_REGISTRE"       => $id,
                    "VALOR_ANTERIOR"    => $item,
                    "VALOR_NOU"         => ""
                );
                $dades_logs->addLog($params_log);
            }
            /* --- FI LOGS --- */

            mysqli_close($conn);
            return $id;
        }
        mysqli_close($conn);
        return null;
    }

    /*
     * Consulta enllaços d'indicadors
     */
    function getIndicadorLink($id = null, $lang = null)
    {
        $bind_param_value = array();
        $myConnection = $this->connect_mysqlcli();

        $strSQL = "SELECT DESCRIPCIO, LINK, LLENGUATGE, ID_INDICADOR
                   FROM IND_INDICADOR_LINK";

        if (isset($id)) {
            $first_parameter = true;
            $strSQL .= " WHERE ID_INDICADOR = ?";
            $bind_param = "i";
            $bind_param_value[] = (int)$id;
        }
        if (isset($lang)) {
            if($first_parameter) {
                $strSQL .= " AND LLENGUATGE = ?";
            }
            else {
                $strSQL .= " WHERE LLENGUATGE = ?";
                $first_parameter = true;
            }
            $bind_param .= "s";
            $bind_param_value[] = $lang;
        }
        $a_params[] = & $bind_param;

        for($i=0; $i<count($bind_param_value); $i++) {
            $a_params[] = &$bind_param_value[$i];
        }
        $stmt = $myConnection->prepare($strSQL);
        //echo $strSQL;
        //var_dump($a_params);

        if(count($bind_param_value)>0)
            call_user_func_array(array($stmt, 'bind_param'), $a_params);
        $stmt->execute();
        $result = $stmt->get_result();
        $items = array();
        while ($item = $result->fetch_object()) {
            $items[] = $item;
        }
        $stmt->close();
        $myConnection->close();
        return $items;
    }

    /*
     * Inserta un enllaç d'Indicador.
     */
    function addIndicadorLink($params)
    {
        //var_dump($params);
        $conn = $this->connect_mysqlcli();
        $strSQL = 'INSERT INTO IND_INDICADOR_LINK
                   (ID_INDICADOR, DESCRIPCIO, LINK, LLENGUATGE)
                   VALUES
                   (?, ?, ?, ?)';

        if ($stmt = $conn->prepare($strSQL)) {
            $stmt->bind_param("isss", $params['ID_INDICADOR'], $params['DESCRIPCIO'], $params['LINK'], $params['LLENGUATGE']);
            if(!$stmt->execute()) {
                die("Error" . $stmt->error);
            }
            $id = mysqli_stmt_insert_id($stmt);

            /* --- LOGS --- */
            $usuari = $_SESSION['info_usuari'];
            $params_input = array(
                'ID_INDICADOR'      => $params['ID_INDICADOR'],
                'DESCRIPCIO'        => $params['DESCRIPCIO'],
                'LINK'              => $params['LINK'],
                'LLENGUATGE'        => $params['LLENGUATGE']
            );
            $dades_logs = new \webtortosa\logs();
            foreach($params_input as $key => $item) {
                $params_log = array(
                    "ACCIO"             => "insert",
                    "USUARI"            => $usuari["login"],
                    "TAULA"             => "IND_INDICADOR_LINK",
                    "CAMP"              => $key,
                    "ID_REGISTRE"       => $id,
                    "VALOR_ANTERIOR"    => "",
                    "VALOR_NOU"         => $item
                );
                $dades_logs->addLog($params_log);
            }
            /* --- FI LOGS --- */

            mysqli_close($conn);
            return $id;
        }
        mysqli_close($conn);
        return null;
    }

    /*
     * Esborra un enllaç d'Indocador.
     */
    function deleteIndicadorLink($id, $lang) {
        $conn = $this->connect_mysqlcli();

        // Get data for logs
        $data_indicador_link = $this->getIndicadorLink($id, $lang);

        $strSQL = 'DELETE FROM IND_INDICADOR_LINK
                   WHERE ID_INDICADOR = ? AND LLENGUATGE = ?';

        if ($stmt = $conn->prepare($strSQL)) {
            $stmt->bind_param("is", $id, $lang);
            if(!$stmt->execute()) {
                die("Error" . $stmt->error);
            }

            /* --- LOGS --- */
            if(count($data_indicador_link)>0) {
                foreach($data_indicador_link as $dil) {
                    $params_old = array(
                        'ID_INDICADOR'  => $dil->ID_INDICADOR,
                        'DESCRIPCIO'    => $dil->DESCRIPCIO,
                        'LINK'          => $dil->LINK,
                        'LLENGUATGE'    => $dil->LLENGUATGE
                    );

                    $usuari = $_SESSION['info_usuari'];
                    $dades_logs = new \webtortosa\logs();
                    foreach ($params_old as $key => $item) {
                        $params_log = array(
                            "ACCIO" => "delete",
                            "USUARI" => $usuari["login"],
                            "TAULA" => "IND_INDICADOR_LINK",
                            "CAMP" => $key,
                            "ID_REGISTRE" => $id,
                            "VALOR_ANTERIOR" => $item,
                            "VALOR_NOU" => ""
                        );
                        $dades_logs->addLog($params_log);
                    }
                }
            }
            /* --- FI LOGS --- */

            mysqli_close($conn);
            return $id;
        }
        mysqli_close($conn);
        return null;
    }
}