<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Mario Juan
 * Date: 13/04/15
 * Time: 08:00
 * To change this template use File | Settings | File Templates.
 */

namespace webtortosa;

class telefon extends database {

    function updateTelefon($id, $servei, $correu, $telefon)
    {

        str_replace("'", "a", $servei);
        
        $this->connect();

        $strSQL = "UPDATE TEL_TELEFON SET SERVEI = '".$servei."', CORREU = '".$correu."', TELEFON = '".$telefon."' WHERE ID = ".$id."";

        $query = $this->consulta($strSQL, $this->conexion);
        $this->disconnect($this->conexion);

    }

    function getTelefon($id) {

        $this->connect();

        $strSQL = "SELECT TEL_TELEFON.* FROM TEL_TELEFON WHERE ID = ".$id." ORDER BY TEL_TELEFON.SERVEI ASC";

        $query = $this->consulta($strSQL, $this->conexion);
        $this->disconnect($this->conexion);

        if($this->numero_de_filas($query)>0) {
            $item = mysql_fetch_object($query);

            return $item;

        }
    }

    function getTelefonsData($params, $start = NULL, $limit = NULL) {

        $this->connect();
        $strSQL = 'SELECT TEL_TELEFON.* FROM TEL_TELEFON ';
        $first_time = false;
        if($params) {
            foreach ($params as $key => $value) {
                if ($first_time)
                    $strSQL .= " AND " . $key . " LIKE '%" . $value . "%'";
                else {
                    $strSQL .= " WHERE " . $key . " LIKE '%" . $value . "%'";
                    $first_time = true;
                }
            }
        }

        $strSQL .= " ORDER BY TEL_TELEFON.SERVEI ASC";

        if(isset($start) && isset($limit))
            $strSQL .= " LIMIT ".$start.", ".$limit;

            //echo $strSQL . "<br>";
        $query = $this->consulta($strSQL, $this->conexion);
        $this->disconnect($this->conexion);

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }

    }

}