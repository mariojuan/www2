<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Mario Juan
 * Date: 13/04/15
 * Time: 08:00
 * To change this template use File | Settings | File Templates.
 */

namespace webtortosa;

class banner extends database {

    function nombreArchivo($id) {

        $this->connect();

       $strSQL = "SELECT FITXER_LINK FROM BAN_BANNER WHERE ID = ".$id." ";

       $query = $this->consulta($strSQL, $this->conexion);
       $this->disconnect($this->conexion);

       $item = mysql_fetch_object($query);

       return $item->FITXER_LINK;

   }

    function nombreFoto($id) {

        $this->connect();

       $strSQL = "SELECT IMATGE FROM BAN_BANNER WHERE ID = ".$id." ";

       $query = $this->consulta($strSQL, $this->conexion);
       $this->disconnect($this->conexion);

       $item = mysql_fetch_object($query);

       return $item->IMATGE;

   }    

    function updateBanner($id, $titol, $link, $fitxerLink, $data, $dataInici, $dataFi, $tipus, $actiu, $imatge) {

        //Tracto comilles
        //$servei = str_replace("'", "''", $servei);
        
        $conn = $this->connect();

        $strSQL =   "UPDATE BAN_BANNER SET TITOL = '".$titol."', LINK = '".$link."', FITXER_LINK = '".$fitxerLink."', DATA = '".$data."'
                    , DATA_INICI = '".$dataInici."' , DATA_FI = '".$dataFi."', TIPUS = '".$tipus."', ACTIU = '".$actiu."'
                    , DATA_FI = '".$dataFi."', IMATGE = '".$imatge."'
                    WHERE ID = ".$id."";

        //echo $strSQL;

        $query = $this->consulta($strSQL, $conn);
        $this->disconnect($conn);

    }

    function getBanner($id) {

        $conn = $this->connect();

        $strSQL = "SELECT BAN_BANNER.* FROM BAN_BANNER WHERE ID = ".$id." ";

        $query = $this->consulta($strSQL, $conn);
        //echo $strSQL;
        $this->disconnect($conn);

        if($this->numero_de_filas($query)>0) {
            $item = mysql_fetch_object($query);

            return $item;

        }
    }

    function getBannersData($params = null, $start = null, $limit = null) {

        $conn = $this->connect();
        $strSQL = 'SELECT BAN_BANNER.* FROM BAN_BANNER ';
        $first_time = false;
        if($params) {
            foreach ($params as $key => $value) {
                if ($first_time)
                    $strSQL .= " AND " . $key . " LIKE '%" . $value . "%'";
                else {
                    $strSQL .= " WHERE " . $key . " LIKE '%" . $value . "%'";
                    $first_time = true;
                }
            }
        }

        $strSQL .= " ORDER BY BAN_BANNER.DATA DESC, BAN_BANNER.ID ASC";

        if(isset($start) && isset($limit))
            $strSQL .= " LIMIT ".$start.", ".$limit;

        //echo $strSQL . "<br>";
        $query = $this->consulta($strSQL, $conn);
        $this->disconnect($conn);

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }

    }

    function getBannersDataFrontEnd($start = null, $limit = null, $tipus = null) {
        $conn = $this->connect();
        $strSQL = 'SELECT BAN_BANNER.* FROM BAN_BANNER WHERE BAN_BANNER.ACTIU = 1 AND BAN_BANNER.DATA_INICI <= "'.date('Y-m-d').'" AND BAN_BANNER.DATA_FI >= "'.date('Y-m-d').'" AND ACTIU = 1';

        if(isset($tipus))
            $strSQL .= " AND TIPUS = ". $tipus;

        $strSQL .= " ORDER BY BAN_BANNER.DATA DESC";

        if(isset($start) && isset($limit))
            $strSQL .= " LIMIT ".$start.", ".$limit;

        //echo $strSQL . "<br>";
        $query = $this->consulta($strSQL, $conn);
        $this->disconnect($conn);

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }

}