<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Mario Juan
 * Date: 13/04/15
 * Time: 08:00
 * To change this template use File | Settings | File Templates.
 */

namespace webtortosa;

class tipologia extends database {
	function getTipologiaData($params) {
		$this->connect();
		return $this->getData($params, "COM_TIPOLOGIA");
	}
}