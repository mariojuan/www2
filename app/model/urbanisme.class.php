<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Mario Juan
 * Date: 13/04/15
 * Time: 08:00
 * To change this template use File | Settings | File Templates.
 */

namespace webtortosa;

class urbanisme extends database {

    /* Mario's functions */
    function getMenuUrbanismeData($params) {
        $this->connect();
        $strSQL = "SELECT * FROM IMA_NAVEGACIO ";
        if(count($params)>0) {
            foreach ($params as $key => $value) {
                if ($first_time)
                    $strSQL .= " AND " . $key . " = '" . $value . "'";
                else {
                    $strSQL .= " WHERE " . $key . " = '" . $value . "'";
                    $first_time = true;
                }
            }
        }
        //echo $strSQL . "<br>";
        $query = $this->consulta($strSQL, $this->conexion);
        $this->disconnect($this->conexion);

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }
    function getLinksUrbanismeData($params) {
        /*
        $this->connect();
        return $this->getData($params, "IMA_ELEMENT_LINK");
        */
        $conn = $this->connect();
        $strSQL = "SELECT * FROM IMA_ELEMENT_LINK ";
        $first_time = false;
        foreach($params as $key => $value) {
            if($first_time)
                $strSQL .= " AND " . $key . "  = " . $value;
            else {
                $strSQL .= " WHERE " . $key . " = " . $value;
                $first_time = true;
            }
        }

        if(isset($start) && isset($limit))
            $strSQL .= " LIMIT ".$start.", ".$limit;

        $strSQL .= " ORDER BY ID ASC";

        //echo $strSQL . "<br>";
        $query = $this->consulta($strSQL, $this->conexion);
        $this->disconnect($conn);

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }


    function getElementUrbanismeData($params, $start = NULL, $limit = NULL) {
        $conn = $this->connect();
        $strSQL = "SELECT * FROM IMA_ELEMENT ";
        foreach($params as $key => $value) {
            if($first_time)
                $strSQL .= " AND " . $key . " LIKE '%" . $value . "%'";
            else {
                $strSQL .= " WHERE " . $key . " LIKE '%" . $value . "%'";
                $first_time = true;
            }
        }

        if(isset($start) && isset($limit))
            $strSQL .= " LIMIT ".$start.", ".$limit;

        //echo $strSQL . "<br>";
        $query = $this->consulta($strSQL, $this->conexion);
        $this->disconnect($conn);

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }
    function getElementUrbanismeFromElementMenu($slug_menu="") {
        $this->connect();
        $strSQL = "SELECT IE.* FROM IMA_ELEMENT IE
            INNER JOIN IMA_NAVEGACIO `IN` ON IE.ID_MENU_NAVEGACIO = `IN`.ID
            WHERE `IN`.SLUG = '".$slug_menu."' ORDER BY ID ASC";
        //echo $strSQL;
        $query = $this->consulta($strSQL, $this->conexion);
        $this->disconnect($this->conexion);

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }

    function getTreeMenuFromSlug($slug="") {
        $this->connect();
        $strSQL = "SELECT IN1.TITOL AS TITOL_MENU, IN2.TITOL AS TITOL_MENU_PARENT
                  FROM IMA_NAVEGACIO IN1
                  INNER JOIN IMA_NAVEGACIO IN2 ON IN1.ID_NIVELL_SUPERIOR = IN2.ID
                  WHERE IN1.SLUG = '".$slug."'";
        //echo $strSQL;
        $query = $this->consulta($strSQL, $this->conexion);
        $this->disconnect($this->conexion);

        if($this->numero_de_filas($query)>0) {
            $items = array();
            while($item = mysql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }
}