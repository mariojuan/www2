<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Mario Juan
 * Date: 19/01/14
 * Time: 0:00
 * To change this template use File | Settings | File Templates.
 */

namespace webtortosa;

class usuari extends database
{
    public function getDataENS($params = null) {
        $conn = $this->connect_mysqlcli();
        
        $strSQL = '
            SELECT ORG_ENS.ID, ORG_ENS.DESCRIPCIO, ORG_ENS.DESC_CURTA
            FROM ORG_ENS
            ';

        if ($stmt = $conn->prepare($strSQL)) {
            $stmt->execute();

            /* Get the result */
            $result = $stmt->get_result();

            $num_of_rows = $result->num_rows;

            $items = array();
            while ($row = $result->fetch_object()) {
                $items[] = $row;
            }
            mysqli_close($conn);
            return $items;
        }
        mysqli_close($con);
        return false;
    }

    public function getDataUser($params)
    {
        $conn = $this->connect_mysqlcli();

        $strSQL = '
            SELECT ORG_USUARI.NOM, ORG_USUARI.COGNOMS, ORG_USUARI.ROL, ORG_UAP.DESC_CURTA AS UNITAT, ORG_USUARI.LOGIN, ORG_USUARI.ID, ORG_USUARI.ID_SERVEI
            FROM ORG_USUARI
            INNER JOIN ORG_UAP ON ORG_UAP.ID = ORG_USUARI.ID_UNITAT WHERE ORG_USUARI.LOGIN = ? AND ORG_USUARI.PASSWORD = ? AND ORG_USUARI.ACTIU = ?';

        if ($stmt = $conn->prepare($strSQL)) {
            $stmt->bind_param("ssi", $params['ORG_USUARI.LOGIN'], $params['ORG_USUARI.PASSWORD'], $params['ORG_USUARI.ACTIU']);
            $stmt->execute();

            /* Get the result */
            $result = $stmt->get_result();

            $num_of_rows = $result->num_rows;

            $items = array();
            while ($row = $result->fetch_object()) {
                $items[] = $row;
            }
            mysqli_close($conn);
            return $items;
        }
        mysqli_close($con);
        return false;
    }

    public function getDataUser2($types, $params)
    {
        $conn = $this->connect_mysqlcli();
        $strSQL = "SELECT * FROM ORG_USUARI WHERE LOGIN = ? AND PASSWORD = ? AND ACTIU = ?";

        if ($stmt = $conn->prepare($strSQL)) {

            $id = 1;
            $user = "admin";
            $pwd = "#Tortosa2015%%";
            $actiu = 1;

            $stmt->bind_param("ssi", $user, $pwd, $actiu);

            $stmt->execute();

            /* Get the result */
            $result = $stmt->get_result();


            $num_of_rows = $result->num_rows;

            $items = array();
            while ($row = $result->fetch_object()) {
                $items[] = $row;
            }
            return $items;
        }
    }

}