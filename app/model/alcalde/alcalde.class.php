<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Mario Juan
 * Date: 13/04/15
 * Time: 08:00
 * To change this template use File | Settings | File Templates.
 */

namespace webtortosa;

class alcalde extends database {
    /* Mario's functions */
    function AddEscriuAlcalde($nom, $email, $poblacio, $comarca, $pais, $assumpte, $comentari, $ip) {
        $conn = $this->connect_mysqlcli();

        $strSQL = 'INSERT INTO ESCRIU_ALCALDE
                   (NOM, EMAIL, POBLACIO, COMARCA_PROVINCIA, PAIS, ASSUMPTE, COMENTARI, IP_CONNEXIO)
                   VALUES
                   (?, ?, ?, ?, ?, ?, ?, ?)';

        if ($stmt = $conn->prepare($strSQL)) {
            $stmt->bind_param("ssssssss", $nom, $email, $poblacio, $comarca, $pais, $assumpte, $comentari, $ip);
            if(!$stmt->execute())
                echo "Error".$stmt->error;
            mysqli_close($conn);
            return true;
        }
        mysqli_close($conn);
        return null;
    }
}