<?php
namespace webtortosa;

class fitxers extends database
{
    function getFitxers($params) {
        $conn = $this->connect_mysqlcli();

        $strSQL = "SELECT
                   ID_TAULA,
                   DESCRIPCIO,
                   NOM_FITXER,
                   DATA_PUBLICACIO,
                   HORA_PUBLICACIO,
                   TIPUS_FITXER,
                   TAMANY,
                   BAIXA
                   FROM COM_FITXERS
                   WHERE ID_TAULA = ? AND TAULA = ?";

        if ($stmt = $conn->prepare($strSQL)) {
            $stmt->bind_param("is", $params['ID_TAULA'], $params['TAULA']);
            $stmt->execute();

            // Get the result
            $result = $stmt->get_result();

            $num_of_rows = $result->num_rows;

            $items = array();

            while ($row = $result->fetch_object()) {
                $items[] = $row;
            }
            $stmt->close();
            $conn->close();
            return $items;
        }
    }

    function addFitxer($params) {
        $conn = $this->connect_mysqlcli();

        $strSQL = 'INSERT INTO COM_FITXERS
        (TAULA, ID_TAULA, DESCRIPCIO, NOM_FITXER, DATA_PUBLICACIO, HORA_PUBLICACIO, TIPUS_FITXER, TAMANY, BAIXA)
        VALUES
        (?, ?, ?, ?, ?, ?, ?, ?, ?)';

        if ($stmt = $conn->prepare($strSQL)) {
            $stmt->bind_param("sissssssi", $params['TAULA'], $params['ID_TAULA'], $params['DESCRIPCIO'], $params['NOM_FITXER'], date('Y-m-d'), date('H:i:s'), $params['TIPUS_FITXER'], $params['TAMANY'], $params['BAIXA']);
            if (!$stmt->execute()) {
                die("Error" . $stmt->error);
            }
            $id = mysqli_stmt_insert_id($stmt);
            mysqli_close($conn);
            return $id;
        }
        mysqli_close($conn);
        return null;
    }

    function deleteFitxer($params) {
        $conn = $this->connect_mysqlcli();

        $strSQL = 'DELETE FROM COM_FITXERS WHERE NOM_FITXER = ? AND TAULA = ?';

        if ($stmt = $conn->prepare($strSQL)) {
            $stmt->bind_param("ss", $params['NOM_FITXER'], $params['TAULA']);
            if (!$stmt->execute()) {
                die("Error" . $stmt->error);
            }
            mysqli_close($conn);
        }
        mysqli_close($conn);
        return null;
    }

    function disabledFitxer($params) {
        $conn = $this->connect_mysqlcli();

        $strSQL = 'UPDATE COM_FITXERS SET BAIXA = 1 WHERE NOM_FITXER = ? AND TAULA = ?';

        if ($stmt = $conn->prepare($strSQL)) {
            $stmt->bind_param("ss", $params['NOM_FITXER'], $params['TAULA']);
            if (!$stmt->execute()) {
                die("Error" . $stmt->error);
            }
            mysqli_close($conn);
        }
        mysqli_close($conn);
        return null;
    }

    function passaDeTemporalABoFitxer($params) {
        $conn = $this->connect_mysqlcli();

        $strSQL = 'UPDATE COM_FITXERS SET BAIXA = 0, ID_TAULA = ? WHERE NOM_FITXER = ? AND TAULA = ?';

        if ($stmt = $conn->prepare($strSQL)) {
            $stmt->bind_param("iss", $params['ID_TAULA'], $params['NOM_FITXER'], $params['TAULA']);
            if (!$stmt->execute()) {
                die("Error" . $stmt->error);
            }
            mysqli_close($conn);
        }
        mysqli_close($conn);
        return null;
    }
}
?>