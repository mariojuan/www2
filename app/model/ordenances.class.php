<?php
/**
 * Created by JetBrains PhpStorm.
 * User: CARMA
 * Date: 22/05/17
 * Time: 07:53
 * To change this template use File | Settings | File Templates.
 */

namespace webtortosa;

class ordenances extends database {

    /* CARMA's functions */

    function getOrdenances ($tipus = NULL, $start = NULL, $limit = NULL) {
        $conn = $this->connect_bbddd_sql_server();

        $strSQL = 'SELECT TOP 10 Ordenances.tipus_ord,Ordenances.codi_ord,Ordenances.Codi_ord1,
                          CAST(Ordenances.descripcio_curta as TEXT) as descripcio_curta,Ordenances.Codi,
                          Ordenances.document,Ordenances.Visites 
                          FROM Ordenances 
                          WHERE tipus_registre=\'vigent\' 
                          and (tipus_ord = \'I\' OR tipus_ord = \'P\' OR tipus_ord = \'T\' OR tipus_ord = \'TF\')
                          and Ordenances.baixa = 0 
                          and Ordenances.activa = 1 
                          ORDER BY Ordenances.Visites DESC';

        if ($tipus=='IM') { 
            $strSQL = 'SELECT Ordenances.tipus_ord,Ordenances.codi_ord,Ordenances.Codi_ord1,
                              CAST(Ordenances.descripcio_curta as TEXT) as descripcio_curta,Ordenances.Codi,
                              Ordenances.document,Ordenances.Visites 
                              FROM Ordenances 
                              WHERE tipus_registre=\'vigent\' 
                              and tipus_ord = \'I\'
                              and Ordenances.baixa = 0 
                              and Ordenances.activa = 1 
                              ORDER BY Ordenances.Codi_ord1 ASC, Ordenances.codi_ord ASC';
 
        } else if ($tipus=='PP') {
            $strSQL = 'SELECT Ordenances.tipus_ord,Ordenances.codi_ord,Ordenances.Codi_ord1,
                              CAST(Ordenances.descripcio_curta as TEXT) as descripcio_curta,Ordenances.Codi,
                              Ordenances.document,Ordenances.Visites 
                              FROM Ordenances 
                              WHERE tipus_registre=\'vigent\' 
                              and tipus_ord = \'PP\'
                              and Ordenances.baixa = 0 
                              and Ordenances.activa = 1 
                              ORDER BY Ordenances.Codi_ord1 ASC, Ordenances.codi_ord ASC';

        } else if ($tipus=='TX') {
            $strSQL = 'SELECT Ordenances.tipus_ord,Ordenances.codi_ord,Ordenances.Codi_ord1,
                              CAST(Ordenances.descripcio_curta as TEXT) as descripcio_curta,Ordenances.Codi,
                              Ordenances.document,Ordenances.Visites 
                              FROM Ordenances 
                              WHERE tipus_registre=\'vigent\' 
                              and tipus_ord  = \'T\'
                              and Ordenances.baixa = 0 
                              and Ordenances.activa = 1 
                              ORDER BY Ordenances.Codi_ord1 ASC, Ordenances.codi_ord ASC';

        } else if ($tipus=='TF') {
            $strSQL = 'SELECT Ordenances.tipus_ord,Ordenances.codi_ord,Ordenances.Codi_ord1,
                              CAST(Ordenances.descripcio_curta as TEXT) as descripcio_curta,Ordenances.Codi,
                              Ordenances.document,Ordenances.Visites 
                              FROM Ordenances 
                              WHERE tipus_registre=\'vigent\' 
                              and tipus_ord = \'TF\'
                              and Ordenances.baixa = 0 
                              and Ordenances.activa = 1 
                              ORDER BY Ordenances.Codi_ord1 ASC, Ordenances.codi_ord ASC';

        } else if ($tipus=='10OF') {
          // és la per defecte
        }  

        //echo $strSQL."<br>";
        $query = mssql_query($strSQL, $conn);
        $this->disconnect_sql_server($conn);
    
        if(mssql_num_rows($query) > 0) {
            //echo "Hi ha registres<br>";

            $items = array();
            $conta = 0;

            while($item = mssql_fetch_object($query)) {
                $conta++;

                if (isset($start) && isset($limit)) {
                    if (($conta>$start) && ($conta<=($start+$limit))) { 
                        $items[] = $item;
                        //echo $item;
                    }
                } else { 
                    $items[] = $item;
                }
            }
            return $items;
        } else { 
            //echo "NOOO hi ha registres<br>";
        }
    }

    // en faig un altra per la consulta per defecte (amb param $tipus = null  -> TOP 10), 
    // ja que és diferent en els dos casos. Es pordía unificar sabent d'on es ve, si  
    // ordenances fiscals o de ordenances i reglaments.
    function getOrdenancesOrIRe ($tipus = NULL, $start = NULL, $limit = NULL) {
        $conn = $this->connect_bbddd_sql_server();

        $strSQL = 'SELECT TOP 10 Ordenances.tipus_ord,Ordenances.codi_ord,Ordenances.Codi_ord1,
                          CAST(Ordenances.descripcio_curta as TEXT) as descripcio_curta,Ordenances.Codi,
                          Ordenances.document,Ordenances.Visites 
                          FROM Ordenances 
                          WHERE tipus_registre=\'vigent\' 
                          AND tipus_ord not LIKE \'I%\' and tipus_ord not LIKE \'T%\' and tipus_ord not LIKE \'P%\'
                          and Ordenances.baixa = 0 
                          and Ordenances.activa = 1 
                          ORDER BY Ordenances.Visites DESC';

        if ($tipus=='AL') { 
            $strSQL = 'SELECT Ordenances.tipus_ord,Ordenances.codi_ord,Ordenances.Codi_ord1,
                              CAST(Ordenances.descripcio_curta as TEXT) as descripcio_curta,Ordenances.Codi,
                              Ordenances.document,Ordenances.Visites 
                              FROM Ordenances 
                              WHERE tipus_registre=\'vigent\' 
                              AND tipus_ord not LIKE \'I%\' and tipus_ord not LIKE \'T%\' and tipus_ord not LIKE \'P%\'
                              and Ordenances.baixa = 0 
                              and Ordenances.activa = 1 
                              ORDER BY Ordenances.Codi_ord1 ASC, Ordenances.codi_ord ASC';
        } else if ($tipus=='10OR') {
          // és la per defecte
        }  


        //echo $strSQL."<br>";
        $query = mssql_query($strSQL, $conn);
        $this->disconnect_sql_server($conn);
    
        if(mssql_num_rows($query) > 0) {
            //echo "Hi ha registres<br>";

            $items = array();
            $conta = 0;

            while($item = mssql_fetch_object($query)) {
                $conta++;

                if (isset($start) && isset($limit)) {
                    if (($conta>$start) && ($conta<=($start+$limit))) { 
                        $items[] = $item;
                        //echo $item;
                    }
                } else { 
                    $items[] = $item;
                }
            }
            return $items;
        } else { 
            //echo "NOOO hi ha registres<br>";
        }
    }


    function getDataOrdenansa($params, $start = NULL, $limit = NULL) {
        $conn = $this->connect_bbddd_sql_server();
        $strSQL = " SELECT Ordenances.Codi_ord1,Ordenances.codi_ord,CAST(Ordenances.descripcio_curta as TEXT) as descripcio_curta,Ordenances.servei_OA,Ordenances.data_entrada_vigor,Ordenances.organ_aprovacio,Ordenances.data_versio_aprovacio,Ordenances.data_publicacio_acord_prov,Ordenances.butlleti_publicacio_acord_prov,Ordenances.data_publicacio_acord_def,Ordenances.butlleti_publicacio_acord_def FROM Ordenances ";
        foreach($params as $key => $value) {
            if($first_time)
                $strSQL .= " AND Ordenances." . $key . " LIKE '%" . $value . "%'";
            else {
                $strSQL .= " WHERE Ordenances." . $key . " = " . $value ;
                $first_time = true;
            }
        }

        if(isset($start) && isset($limit))
            $strSQL .= " LIMIT ".$start.", ".$limit;

        //echo $strSQL . "<br>";
        $query = mssql_query($strSQL, $conn);
        $this->disconnect_sql_server($conn);
    
        if(mssql_num_rows($query)>0) {
            $items = array();
            while($item = mssql_fetch_object($query)) {
                $items[] = $item;
            }
            return $items;
        }
    }



}
