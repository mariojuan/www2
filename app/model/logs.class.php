<?php
namespace webtortosa;

class logs extends database
{
    function addLog($params) {
        $conn = $this->connect_mysqlcli();

        $strSQL = 'INSERT INTO LOG_LOGS
        (ACCIO, USUARI, TAULA, CAMP, ID_REGISTRE, VALOR_ANTERIOR, VALOR_NOU)
        VALUES
        (?, ?, ?, ?, ?, ?, ?)';

        if ($stmt = $conn->prepare($strSQL)) {
            $stmt->bind_param("ssssiss", $params['ACCIO'], $params['USUARI'], $params['TAULA'], $params['CAMP'], $params['ID_REGISTRE'], $params['VALOR_ANTERIOR'], $params['VALOR_NOU']);
            if (!$stmt->execute()) {
                die("Error" . $stmt->error);
            }
            $id = mysqli_stmt_insert_id($stmt);
            mysqli_close($conn);
            return $id;
        }
        mysqli_close($conn);
        return null;
    }

    function getNumSequencia($tipus) {
        $conn = $this->connect_bbdd_elecc_cli();

        if($tipus=="PPART") {
            $field = "VOTACIONS";
        }
        $strSQL = '
                   SELECT '.$field.' FROM NUMERO_SEQUENCIA_LOG
                   ';

        //echo $strSQL."<br>";
        if ($stmt = $conn->prepare($strSQL)) {
            $stmt->execute();
            $result = $stmt->get_result();

            $value=0;

            if ($row = $result->fetch_object()) {
                $value = $row->$field;
            }

            $strSQL = '
                UPDATE NUMERO_SEQUENCIA_LOG SET '.$field.' = '.$field.'+1
            ';
            if ($stmt1 = $conn->prepare($strSQL)) {
                $stmt1->execute();
                mysqli_close($conn);
                return $value;
            }
            mysqli_close($conn);
            return null;
        }
        mysqli_close($conn);
        return null;
    }
}
?>