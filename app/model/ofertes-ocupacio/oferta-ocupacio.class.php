<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Mario Juan
 * Date: 13/04/15
 * Time: 08:00
 * To change this template use File | Settings | File Templates.
 */

namespace webtortosa;

class oferta extends database {

    /* Alta oferta ocupació */
    function addOfertaOcupacio($params)
    {
        $conn = $this->connect_mysqlcli();

        $strSQL = 'INSERT INTO OO_OFERTA_OCUPACIO
                   (ID_ENS,
                   NUMERO_EXPEDIENT,
                   TIPUS_OFERTA,
                   DESCRIPCIO_CURTA,
                   OBJECTE,
                   DENOMINACIO,
                   REGIM_JURIDIC,
                   CARACTER,
                   ESCALA,
                   SUBESCALA,
                   CLASSE,
                   GRUP,
                   NUM_PLACES,
                   SISTEMA_SELECCIO,
                   REQUISITS,
                   DATA_INICI_PRESENT_SOLLICITUDS,
                   DATA_FI_PRESENT_SOLLICITUDS,
                   DRETS_EXAMEN,
                   DATA_INICI_PRESENT_ALEGACIONS,
                   DATA_FI_PRESENT_ALEGACIONS,
                   ESTAT,
                   DATA_INICI_VIGENCIA_BORSA_TREBALL,
                   DURACIO_BORSA_TREBALL,
                   DATA_INICI_VISUALITZACIO,
                   DATA_FI_VISUALITZACIO,
                   VISIBLE,
                   DATA_PUBLICACIO,
                   HORA_PUBLICACIO,
                   ID_USUARI,
                   ID_SERVEI,
                   BAIXA)
                   VALUES
                   (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';

        if ($stmt = $conn->prepare($strSQL)) {
            $stmt->bind_param("isssssssssssisssssssssississiii", $params['ID_ENS'], $params['NUMERO_EXPEDIENT'], $params['TIPUS_OFERTA'], $params['DESCRIPCIO_CURTA'], $params['OBJECTE'], $params['DENOMINACIO'], $params['REGIM_JURIDIC'], $params['CARACTER'], $params['ESCALA'], $params['SUBESCALA'], $params['CLASSE'], $params['GRUP'], $params['NUM_PLACES'], $params['SISTEMA_SELECCIO'], $params['REQUISITS'], $params['DATA_INICI_PRESENT_SOLLICITUDS'], $params['DATA_FI_PRESENT_SOLLICITUDS'], $params['DRETS_EXAMEN'], $params['DATA_INICI_PRESENT_ALEGACIONS'], $params['DATA_FI_PRESENT_ALEGACIONS'], $params['ESTAT'], $params['DATA_INICI_VIGENCIA_BORSA_TREBALL'], $params['DURACIO_BORSA_TREBALL'], $params['DATA_INICI_VISUALITZACIO'], $params['DATA_FI_VISUALITZACIO'], $params['VISIBLE'], $params['DATA_PUBLICACIO'], $params['HORA_PUBLICACIO'], $params['ID_USUARI'], $params['ID_SERVEI'], $params['BAIXA']);
            var_dump($params);
            if(!$stmt->execute()) {
                die("Error" . $stmt->error);
            }

            $id = mysqli_stmt_insert_id($stmt);

            /* --- LOGS --- */
            $usuari = $_SESSION['info_usuari'];
            $params_input = array(
                'ID'                                => $id,
                'ID_ENS'                            => $params['ID_ENS'],
                'NUMERO_EXPEDIENT'                  => $params['NUMERO_EXPEDIENT'],
                'TIPUS_OFERTA'                      => $params['TIPUS_OFERTA'],
                'DESCRIPCIO_CURTA'                  => $params['DESCRIPCIO_CURTA'],
                'OBJECTE'                           => $params['OBJECTE'],
                'DENOMINACIO'                       => $params['DENOMINACIO'],
                'REGIM_JURIDIC'                     => $params['REGIM_JURIDIC'],
                'CARACTER'                          => $params['CARACTER'],
                'ESCALA'                            => $params['ESCALA'],
                'SUBESCALA'                         => $params['SUBESCALA'],
                'CLASSE'                            => $params['CLASSE'],
                'GRUP'                              => $params['GRUP'],
                'NUM_PLACES'                        => $params['NUM_PLACES'],
                'SISTEMA_SELECCIO'                  => $params['SISTEMA_SELECCIO'],
                'REQUISITS'                         => $params['REQUISITS'],
                'DATA_INICI_PRESENT_SOLLICITUDS'    => $params['DATA_INICI_PRESENT_SOLLICITUDS'],
                'DATA_FI_PRESENT_SOLLICITUDS'       => $params['DATA_FI_PRESENT_SOLLICITUDS'],
                'DRETS_EXAMEN'                      => $params['DRETS_EXAMEN'],
                'DATA_INICI_PRESENT_ALEGACIONS'     => $params['DATA_INICI_PRESENT_ALEGACIONS'],
                'DATA_FI_PRESENT_ALEGACIONS'        => $params['DATA_FI_PRESENT_ALEGACIONS'],
                'ESTAT'                             => $params['ESTAT'],
                'DATA_INICI_VIGENCIA_BORSA_TREBALL' => $params['DATA_INICI_VIGENCIA_BORSA_TREBALL'],
                'DURACIO_BORSA_TREBALL'             => $params['DURACIO_BORSA_TREBALL'],
                'DATA_INICI_VISUALITZACIO'          => $params['DATA_INICI_VISUALITZACIO'],
                'DATA_FI_VISUALITZACIO'             => $params['DATA_FI_VISUALITZACIO'],
                'DATA_PUBLICACIO'                   => $params['DATA_PUBLICACIO'],
                'HORA_PUBLICACIO'                   => $params['HORA_PUBLICACIO'],
                'ID_USUARI'                         => $params['ID_USUARI'],
                'ID_SERVEI'                         => $params['ID_SERVEI'],
                'BAIXA'                             => $params['BAIXA'],
                'VISIBLE'                           => $params['VISIBLE']
            );
            $dades_logs = new \webtortosa\logs();
            foreach($params_input as $key => $item) {
                $params_log = array(
                    "ACCIO"             => "insert",
                    "USUARI"            => $usuari["login"],
                    "TAULA"             => "OO_OFERTA_OCUPACIO",
                    "CAMP"              => $key,
                    "ID_REGISTRE"       => $id,
                    "VALOR_ANTERIOR"    => "",
                    "VALOR_NOU"         => $item
                );
                $dades_logs->addLog($params_log);
            }
            /* --- FI LOGS --- */

            mysqli_close($conn);
            return $id;
        }
        mysqli_close($conn);
        return null;

    }

    function getOfertaOcupacio($id = null, $id_usuari = null, $id_servei = null, $start = null, $limit = null, $visible = null, $baixa = null, $id_ens = null)
    {
        $first_parameter = false;
        $bind_param_value = array();
        $myConnection = $this->connect_mysqlcli();

        $strSQL = "SELECT
                   OO_OFERTA_OCUPACIO.ID,
                   ID_ENS,
                   NUMERO_EXPEDIENT,
                   TIPUS_OFERTA,
                   DESCRIPCIO_CURTA,
                   OBJECTE,
                   DENOMINACIO,
                   REGIM_JURIDIC,
                   CARACTER,
                   ESCALA,
                   SUBESCALA,
                   CLASSE,
                   GRUP,
                   NUM_PLACES,
                   SISTEMA_SELECCIO,
                   REQUISITS,
                   DATA_INICI_PRESENT_SOLLICITUDS,
                   DATA_FI_PRESENT_SOLLICITUDS,
                   DRETS_EXAMEN,
                   DATA_INICI_PRESENT_ALEGACIONS,
                   DATA_FI_PRESENT_ALEGACIONS,
                   ESTAT,
                   DATA_INICI_VIGENCIA_BORSA_TREBALL,
                   DATA_INICI_VISUALITZACIO,
                   DATA_FI_VISUALITZACIO,
                   DATA_PUBLICACIO,
                   HORA_PUBLICACIO,
                   ID_USUARI,
                   ID_SERVEI,
                   BAIXA,
                   VISIBLE,
                   DATA_PUBLICACIO_LLISTA_PROVISIONAL_ADM_EXCL,
                   HORA_PUBLICACIO_LLISTA_PROVISIONAL_ADM_EXCL,
                   ORG_ENS.DESCRIPCIO AS ENS
                   FROM OO_OFERTA_OCUPACIO
                   INNER JOIN ORG_ENS ON ORG_ENS.ID = OO_OFERTA_OCUPACIO.ID_ENS";

        if (isset($id)) {
            $first_parameter = true;
            $strSQL .= " WHERE OO_OFERTA_OCUPACIO.ID = ?";
            $bind_param = "i";
            $bind_param_value[] = (int)$id;
        }
        if (isset($id_usuari)) {
            if($first_parameter) {
                $strSQL .= " AND ID_USUARI = ?";
            }
            else {
                $strSQL .= " WHERE ID_USUARI = ?";
                $first_parameter = true;
            }
            $bind_param .= "i";
            $bind_param_value[] = $id_usuari;
        }
        if (isset($id_servei)) {
            if($first_parameter) {
                $strSQL .= " AND ID_SERVEI = ?";
            }
            else {
                $strSQL .= " WHERE ID_SERVEI = ?";
                $first_parameter = true;
            }
            $bind_param .= "i";
            $bind_param_value[] = (int)$id_servei;
        }
        if (isset($id_ens)) {
            if($first_parameter) {
                $strSQL .= " AND ID_ENS = ?";
            }
            else {
                $strSQL .= " WHERE ID_ENS = ?";
                $first_parameter = true;
            }
            $bind_param .= "i";
            $bind_param_value[] = (int)$id_ens;
        }
        if (isset($visible)) {
            if($first_parameter) {
                $strSQL .= " AND VISIBLE = ?";
            }
            else {
                $strSQL .= " WHERE VISIBLE = ?";
                $first_parameter = true;
            }
            $bind_param .= "i";
            $bind_param_value[] = (int)$visible;
        }
        if (isset($baixa)) {
            if($first_parameter) {
                $strSQL .= " AND BAIXA = ?";
            }
            else {
                $strSQL .= " WHERE BAIXA = ?";
                $first_parameter = true;
            }
            $bind_param .= "i";
            $bind_param_value[] = (int)$baixa;
        }
        if (isset($start) && isset($limit)) {
            $strSQL .= " LIMIT ?, ?";
            $bind_param .= "ii";
            $bind_param_value[] = (int)$start;
            $bind_param_value[] = (int)$limit;
        }
        $a_params[] = & $bind_param;

        for($i=0; $i<count($bind_param_value); $i++) {
            $a_params[] = &$bind_param_value[$i];
        }
        $stmt = $myConnection->prepare($strSQL);
        //echo $strSQL;
        //var_dump($a_params);

        if(count($bind_param_value)>0)
            call_user_func_array(array($stmt, 'bind_param'), $a_params);
        $stmt->execute();
        $result = $stmt->get_result();
        $items = array();
        while ($item = $result->fetch_object()) {
            $items[] = $item;
        }
        $stmt->close();
        $myConnection->close();
        return $items;
    }

    function updateOfertaOcupacio($params) {

        $conn = $this->connect_mysqlcli();

        // Get data for logs
        $data_oferta_ocupacio = $this->getOfertaOcupacio($params['ID']);

        $strSQL = 'UPDATE OO_OFERTA_OCUPACIO SET
                   ID_ENS = ?,
                   NUMERO_EXPEDIENT = ?,
                   TIPUS_OFERTA = ?,
                   DESCRIPCIO_CURTA = ?,
                   OBJECTE = ?,
                   DENOMINACIO = ?,
                   REGIM_JURIDIC = ?,
                   CARACTER = ?,
                   ESCALA = ?,
                   SUBESCALA = ?,
                   CLASSE = ?,
                   GRUP = ?,
                   NUM_PLACES = ?,
                   SISTEMA_SELECCIO = ?,
                   REQUISITS = ?,
                   DATA_INICI_PRESENT_SOLLICITUDS = ?,
                   DATA_FI_PRESENT_SOLLICITUDS = ?,
                   DRETS_EXAMEN = ?,
                   DATA_INICI_PRESENT_ALEGACIONS = ?,
                   DATA_FI_PRESENT_ALEGACIONS = ?,
                   ESTAT = ?,
                   DATA_INICI_VIGENCIA_BORSA_TREBALL = ?,
                   DURACIO_BORSA_TREBALL = ?,
                   DATA_INICI_VISUALITZACIO = ?,
                   DATA_FI_VISUALITZACIO = ?,
                   VISIBLE = ?,
                   DATA_PUBLICACIO = ?,
                   HORA_PUBLICACIO = ?,
                   ID_USUARI = ?,
                   ID_SERVEI = ?,
                   BAIXA = ?
                   WHERE ID = ?';


        if ($stmt = $conn->prepare($strSQL)) {
            //var_dump($params);
            $stmt->bind_param("isssssssssssisssssssssississiiii", $params['ID_ENS'], $params['NUMERO_EXPEDIENT'], $params['TIPUS_OFERTA'], $params['DESCRIPCIO_CURTA'], $params['OBJECTE'], $params['DENOMINACIO'], $params['REGIM_JURIDIC'], $params['CARACTER'], $params['ESCALA'], $params['SUBESCALA'], $params['CLASSE'], $params['GRUP'], $params['NUM_PLACES'], $params['SISTEMA_SELECCIO'], $params['REQUISITS'], $params['DATA_INICI_PRESENT_SOLLICITUDS'], $params['DATA_FI_PRESENT_SOLLICITUDS'], $params['DRETS_EXAMEN'], $params['DATA_INICI_PRESENT_ALEGACIONS'], $params['DATA_FI_PRESENT_ALEGACIONS'], $params['ESTAT'], $params['DATA_INICI_VIGENCIA_BORSA_TREBALL'], $params['DURACIO_BORSA_TREBALL'], $params['DATA_INICI_VISUALITZACIO'], $params['DATA_FI_VISUALITZACIO'], $params['VISIBLE'], $params['DATA_PUBLICACIO'], $params['HORA_PUBLICACIO'], $params['ID_USUARI'], $params['ID_SERVEI'], $params['BAIXA'], $params['ID']);
            if(!$stmt->execute()) {
                mysqli_close($conn);
                die("Error" . $stmt->error);
            }
            /* --- LOGS --- */
            $params_old = array(
                'ID'                                => $data_oferta_ocupacio[0]->ID,
                'ID_ENS'                            => $data_oferta_ocupacio[0]->ID_ENS,
                'NUMERO_EXPEDIENT'                  => $data_oferta_ocupacio[0]->NUMERO_EXPEDIENT,
                'TIPUS_OFERTA'                      => $data_oferta_ocupacio[0]->TIPUS_OFERTA,
                'DESCRIPCIO_CURTA'                  => $data_oferta_ocupacio[0]->DESCRIPCIO_CURTA,
                'OBJECTE'                           => $data_oferta_ocupacio[0]->OBJECTE,
                'DENOMINACIO'                       => $data_oferta_ocupacio[0]->DENOMINACIO,
                'REGIM_JURIDIC'                     => $data_oferta_ocupacio[0]->REGIM_JURIDIC,
                'CARACTER'                          => $data_oferta_ocupacio[0]->CARACTER,
                'ESCALA'                            => $data_oferta_ocupacio[0]->ESCALA,
                'SUBESCALA'                         => $data_oferta_ocupacio[0]->SUBESCALA,
                'CLASSE'                            => $data_oferta_ocupacio[0]->CLASSE,
                'GRUP'                              => $data_oferta_ocupacio[0]->GRUP,
                'NUM_PLACES'                        => $data_oferta_ocupacio[0]->NUM_PLACES,
                'SISTEMA_SELECCIO'                  => $data_oferta_ocupacio[0]->SISTEMA_SELECCIO,
                'REQUISITS'                         => $data_oferta_ocupacio[0]->REQUISITS,
                'DATA_INICI_PRESENT_SOLLICITUDS'    => $data_oferta_ocupacio[0]->DATA_INICI_PRESENT_SOLLICITUDS,
                'DATA_FI_PRESENT_SOLLICITUDS'       => $data_oferta_ocupacio[0]->DATA_FI_PRESENT_SOLLICITUDS,
                'DRETS_EXAMEN'                      => $data_oferta_ocupacio[0]->DRETS_EXAMEN,
                'DATA_INICI_PRESENT_ALEGACIONS'     => $data_oferta_ocupacio[0]->DATA_INICI_PRESENT_ALEGACIONS,
                'DATA_FI_PRESENT_ALEGACIONS'        => $data_oferta_ocupacio[0]->DATA_FI_PRESENT_ALEGACIONS,
                'ESTAT'                             => $data_oferta_ocupacio[0]->ESTAT,
                'DATA_INICI_VIGENCIA_BORSA_TREBALL' => $data_oferta_ocupacio[0]->DATA_INICI_VIGENCIA_BORSA_TREBALL,
                'DURACIO_BORSA_TREBALL'             => $data_oferta_ocupacio[0]->DURACIO_BORSA_TREBALL,
                'DATA_INICI_VISUALITZACIO'          => $data_oferta_ocupacio[0]->DATA_INICI_VISUALITZACIO,
                'DATA_FI_VISUALITZACIO'             => $data_oferta_ocupacio[0]->DATA_FI_VISUALITZACIO,
                'DATA_PUBLICACIO'                   => $data_oferta_ocupacio[0]->DATA_PUBLICACIO,
                'HORA_PUBLICACIO'                   => $data_oferta_ocupacio[0]->HORA_PUBLICACIO,
                'ID_USUARI'                         => $data_oferta_ocupacio[0]->ID_USUARI,
                'ID_SERVEI'                         => $data_oferta_ocupacio[0]->ID_SERVEI,
                'BAIXA'                             => $data_oferta_ocupacio[0]->BAIXA,
                'VISIBLE'                           => $data_oferta_ocupacio[0]->VISIBLE
            );
            $params_new = array(
                'ID'                                => $params['ID'],
                'ID_ENS'                            => $params['ID_ENS'],
                'NUMERO_EXPEDIENT'                  => $params['NUMERO_EXPEDIENT'],
                'TIPUS_OFERTA'                      => $params['TIPUS_OFERTA'],
                'DESCRIPCIO_CURTA'                  => $params['DESCRIPCIO_CURTA'],
                'OBJECTE'                           => $params['OBJECTE'],
                'DENOMINACIO'                       => $params['DENOMINACIO'],
                'REGIM_JURIDIC'                     => $params['REGIM_JURIDIC'],
                'CARACTER'                          => $params['CARACTER'],
                'ESCALA'                            => $params['ESCALA'],
                'SUBESCALA'                         => $params['SUBESCALA'],
                'CLASSE'                            => $params['CLASSE'],
                'GRUP'                              => $params['GRUP'],
                'NUM_PLACES'                        => $params['NUM_PLACES'],
                'SISTEMA_SELECCIO'                  => $params['SISTEMA_SELECCIO'],
                'REQUISITS'                         => $params['REQUISITS'],
                'DATA_INICI_PRESENT_SOLLICITUDS'    => $params['DATA_INICI_PRESENT_SOLLICITUDS'],
                'DATA_FI_PRESENT_SOLLICITUDS'       => $params['DATA_FI_PRESENT_SOLLICITUDS'],
                'DRETS_EXAMEN'                      => $params['DRETS_EXAMEN'],
                'DATA_INICI_PRESENT_ALEGACIONS'     => $params['DATA_INICI_PRESENT_ALEGACIONS'],
                'DATA_FI_PRESENT_ALEGACIONS'        => $params['DATA_FI_PRESENT_ALEGACIONS'],
                'ESTAT'                             => $params['ESTAT'],
                'DATA_INICI_VIGENCIA_BORSA_TREBALL' => $params['DATA_INICI_VIGENCIA_BORSA_TREBALL'],
                'DURACIO_BORSA_TREBALL'             => $params['DURACIO_BORSA_TREBALL'],
                'DATA_INICI_VISUALITZACIO'          => $params['DATA_INICI_VISUALITZACIO'],
                'DATA_FI_VISUALITZACIO'             => $params['DATA_FI_VISUALITZACIO'],
                'DATA_PUBLICACIO'                   => $params['DATA_PUBLICACIO'],
                'HORA_PUBLICACIO'                   => $params['HORA_PUBLICACIO'],
                'ID_USUARI'                         => $params['ID_USUARI'],
                'ID_SERVEI'                         => $params['ID_SERVEI'],
                'BAIXA'                             => $params['BAIXA'],
                'VISIBLE'                           => $params['VISIBLE']
            );

            $usuari = $_SESSION['info_usuari'];
            $dades_logs = new \webtortosa\logs();
            foreach($params_old as $key => $item) {
                $params_log = array(
                    "ACCIO"             => "update",
                    "USUARI"            => $usuari["login"],
                    "TAULA"             => "OO_OFERTA_OCUPACIO",
                    "CAMP"              => $key,
                    "ID_REGISTRE"       => $params['ID'],
                    "VALOR_ANTERIOR"    => $item,
                    "VALOR_NOU"         => $params_new[$key]
                );
                $dades_logs->addLog($params_log);
            }

            /* --- FI LOGS --- */
        }
        else {
            die("Error en el sql");
        }
        mysqli_close($conn);
        return null;
    }

    function deleteOfertaOcupacio($id) {
        $conn = $this->connect_mysqlcli();

        // Get data for logs
        $data_indicador_grup = $this->getIndicadorGrup($id);

        $strSQL = 'DELETE FROM IND_INDICADOR_GRUP
                   WHERE ID = ?';

        if ($stmt = $conn->prepare($strSQL)) {
            $stmt->bind_param("i", $id);
            if(!$stmt->execute()) {
                die("Error" . $stmt->error);
            }
            /* --- LOGS --- */
            $params_old = array(
                'CODI_GRUP_INDICADOR'   => $data_indicador_grup[0]->CODI_GRUP_INDICADOR,
                'TIPUS_INDICADOR'       => $data_indicador_grup[0]->TIPUS_INDICADOR,
                'EXERCICI'              => $data_indicador_grup[0]->EXERCICI,
                'GRUP_INDICADOR'        => $data_indicador_grup[0]->GRUP_INDICADOR,
                'ID_USUARI'             => $data_indicador_grup[0]->ID_USUARI,
                'ID_SERVEI'             => $data_indicador_grup[0]->ID_SERVEI,
                'BAIXA'                 => $data_indicador_grup[0]->BAIXA,
                'VISIBLE'               => $data_indicador_grup[0]->VISIBLE
            );
            $usuari = $_SESSION['info_usuari'];
            $dades_logs = new \webtortosa\logs();
            foreach($params_old as $key => $item) {
                $params_log = array(
                    "ACCIO"             => "delete",
                    "USUARI"            => $usuari["login"],
                    "TAULA"             => "IND_INDICADOR_GRUP",
                    "CAMP"              => $key,
                    "ID_REGISTRE"       => $id,
                    "VALOR_ANTERIOR"    => $item,
                    "VALOR_NOU"         => ""
                );
                $dades_logs->addLog($params_log);
            }

            /* --- FI LOGS --- */
            mysqli_close($conn);
            return $id;
        }
        mysqli_close($conn);
        return null;
    }
}