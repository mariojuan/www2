<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Mario Juan
 * Date: 13/04/15
 * Time: 08:00
 * To change this template use File | Settings | File Templates.
 */

namespace webtortosa;

class ciutat extends database {
    /* Mario's functions */

    /*
     * Guarda les dades d'una incidència a la via pública
     */
    function AddIncidenciaViaPublica($params) {
        $conn = $this->connect_mysqlcli();

        $strSQL = 'INSERT INTO INCIDENCIES_VIA_PUBLICA
                   (DESCRIPCIO, LATITUD, LONGITUD, ID_NUCLI, ID_BARRI, CARRER, NUMERO, DESCRIPCIO_LOCALITZACIO, NOM, COGNOMS, EMAIL, TELEFON, ADRECA, POBLACIO, CODI_POSTAL, IP_CONNEXIO)
                   VALUES
                   (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';

        if ($stmt = $conn->prepare($strSQL)) {
            $stmt->bind_param("sssiisssssssssss", $params['DESCRIPCIO'], $params['LATITUD'], $params['LONGITUD'], $params['ID_NUCLI'], $params['ID_BARRI'], $params['CARRER'], $params['NUMERO'], $params['DESCRIPCIO_LOCALITZACIO'], $params['NOM'], $params['COGNOMS'], $params['EMAIL'], $params['POBLACIO'], $params['TELEFON'], $params['ADRECA'], $params['CODI_POSTAL'], $params['IP_CONNEXIO']);
            if(!$stmt->execute())
                echo "Error".$stmt->error;
            $id = mysqli_stmt_insert_id($stmt);
            mysqli_close($conn);
            return $id;
        }
        mysqli_close($conn);
        return null;
    }

    /*
     * Guarda les dades d'un registre del formulari de Quixes i Suggeriments.
     */
    function AddQueixesSuggeriments($params) {
        $conn = $this->connect_mysqlcli();

        $strSQL = 'INSERT INTO QUEIXES_SUGGERIMENTS
                   (NOM, PRIMER_COGNOM, SEGON_COGNOM, RAO_SOCIAL, TIPUS_DOCUMENT, NUMERO_DOCUMENT, TIPUS_VIA, NOM_VIA, NUMERO, LLETRA, KM, BLOC, ESCALA, PIS, PORTA, NUCLI_BARRI, PROVINCIA, MUNICIPI, CODI_POSTAL, TELEFON_FIX, TELEFON_MOBIL, EMAIL, TIPUS_PETICIO, AFECTA_A, DESCRIPCIO, IP_CONNEXIO)
                   VALUES
                   (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';

        if ($stmt = $conn->prepare($strSQL)) {
            $stmt->bind_param("ssssssssisississssisssssss", $params['NOM'], $params['PRIMER_COGNOM'], $params['SEGON_COGNOM'], $params['RAO_SOCIAL'], $params['TIPUS_DOCUMENT'], $params['NUMERO_DOCUMENT'], $params['TIPUS_VIA'], $params['NOM_VIA'], $params['NUMERO'], $params['LLETRA'], $params['KM'], $params['BLOC'], $params['ESCALA'], $params['PIS'], $params['PORTA'], $params['NUCLI_BARRI'], $params['PROVINCIA'], $params['MUNICIPI'], $params['CODI_POSTAL'], $params['TELEFON_FIX'], $params['TELEFON_MOBIL'], $params['EMAIL'], $params['TIPUS_PETICIO'], $params['AFECTA_A'], $params['DESCRIPCIO'], $params['IP_CONNEXIO']);
            if(!$stmt->execute())
                echo "Error".$stmt->error;

            $id = mysqli_stmt_insert_id($stmt);
            mysqli_close($conn);
            return $id;
        }
        mysqli_close($conn);
        return null;
    }

    /*
     * Consulta els nuclis de la ciutat
     */
    function getNucli() {
        $myConnection = $this->connect_mysqlcli();

        $strSQL = "SELECT ID, DESCRIPCIO, ACTIU FROM TER_NUCLI
            WHERE ACTIU = ?";
        $stmt = $myConnection->prepare($strSQL);
        $value = 1;
        $stmt->bind_param("i", $value);
        //echo $strSQL;
        $stmt->execute();
        $result = $stmt->get_result();
        $items = array();
        while ($item = $result->fetch_object()) {
            $items[] = $item;
        }
        $stmt->close();
        $myConnection->close();
        return $items;
    }

    /*
     * Consulta els barris de la ciutat
     */
    function getBarri($nucli_id = null) {
        $myConnection = $this->connect_mysqlcli();

        if(!$nucli_id) {
            $strSQL = "SELECT ID_NUCLI, ID_BARRI, DESCRIPCIO FROM TER_BARRI";
            $stmt = $myConnection->prepare($strSQL);
        }
        else {
            $strSQL = "SELECT ID_NUCLI, ID_BARRI, DESCRIPCIO FROM TER_BARRI WHERE ID_NUCLI = ?";
            $stmt = $myConnection->prepare($strSQL);
            $stmt->bind_param("i", $nucli_id);
        }


        $stmt->execute();
        $result = $stmt->get_result();
        $items = array();
        while ($item = $result->fetch_object()) {
            $items[] = $item;
        }
        $stmt->close();
        $myConnection->close();
        return $items;
    }

    /*
     * Consulta els carrers la ciutat
     */
    function getCarrer($carrer_str = null, $nucli_id = null) {
        $myConnection = $this->connect_mysqlcli();

        $strSQL = "SELECT ID, TIPVIA, DESCRIPCIO FROM TER_CARRER
            WHERE DESCRIPCIO LIKE ? AND ID LIKE ? ORDER BY DESCRIPCIO ASC";

        $stmt = $myConnection->prepare($strSQL);
        $carrer_str = '%' . $carrer_str . '%';
        $nucli_str = $nucli_id . '%';
        $stmt->bind_param("ss", $carrer_str, $nucli_str);

        //echo $strSQL;
        $stmt->execute();
        $result = $stmt->get_result();
        $items = array();
        while ($item = $result->fetch_object()) {
            $items[] = $item;
        }
        $stmt->close();
        $myConnection->close();
        return $items;
    }

}