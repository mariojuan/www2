<?
namespace webtortosa;

require_once("config.cfg");
require_once(FOLDER_CONTROLLER . "/web_controller.php");


//se instancia al controlador
$controller = new web_controller();

switch ($_GET['accio']) {
    case 'participacio_totals':
        $controller->eleccionsParticipacioTotals($_GET);
        break;
    case 'participacio_districtes':
        $controller->eleccionsParticipacioDistrictes($_GET);
        break;
    case 'participacio_seccions':
        $controller->eleccionsParticipacioSeccions($_GET);
        break;
    case 'participacio_meses':
        $controller->eleccionsParticipacioMeses($_GET);
        break;
    case 'participacio_comparativa':
        $controller->eleccionsParticipacioComparativa($_GET);
        break;
    case 'resultats_totals':
        $controller->eleccionsResultatsTotals($_GET);
        break;
    case 'resultats_districtes':
        $controller->eleccionsResultatsDistrictes($_GET);
        break;
    case 'resultats_seccions':
        $controller->eleccionsResultatsSeccions($_GET);
        break;
    case 'resultats_meses':
        $controller->eleccionsResultatsMeses($_GET);
        break;
    case 'resultats_bitem':
        $controller->eleccionsResultatsTotals($_GET);
        break;
    case 'resultats_campredo':
        $controller->eleccionsResultatsTotals($_GET);
        break;
    case 'resultats_jesus':
        $controller->eleccionsResultatsTotals($_GET);
        break;
    case 'electes2015':
        $controller->eleccionsElectes2015($_GET);
        break;
    default:
        $controller->eleccionsParticipacioTotals($_GET);
        break;
}
?>