<html>
<head>
    <title>Registre Logs Sistemes Telemàtics</title>
    <meta charset="utf-8">
</head>
<style type="text/css">
    body {
        font-family: Arial, Helvetica, "Bitstream Vera Sans", sans-serif;
        font-size: 12px;
        color: #6E6E6E;
    }
    ul {
        list-style: none;
        padding-left: 0;
    }
    span {
        width: 150px;
        display: inline-block;
    }
    .a {
        background-color: #E6E6E6;
    }
    .b {
        background-color: #ffffff;
    }
    h1 {
        color: #ffffff;
        background-color: #ED1F33;
        padding: 5px;
    }
    a {
        color: #6E6E6E;
    }
    li {
        padding: 5px;
    }
</style>
<?php
$files = array();
if ($handle = opendir('.')) {
    while (false !== ($file = readdir($handle))) {
        if ($file != "." && $file != "..") {
            $files[filemtime($file)] = $file;
        }
    }
    closedir($handle);

    // sort
    krsort($files);
    // find the last modification
    $reallyLastModified = end($files);
    echo "<h1>Registre Logs Sistemes Telemàtics</h1>";
    if($files) {
        $cont = 0;
        echo "<ul>";
        foreach($files as $file) {
            $mod = $cont%2;
            $class = $cont%2==0 ? 'class="a"' : 'class="b"';
            $cont++;
            $lastModified = date('F d Y, H:i:s',filemtime($file));
            if ($file == $reallyLastModified) {
                // do stuff for the real last modified file
            }
            if($file!="index.php")
                echo "<li $class><span><a href=\"$file\" target=\"_blank\">$file</a></span> -- $lastModified</li>";
        }
        echo "</ul>";
    }

}
?>
</html>