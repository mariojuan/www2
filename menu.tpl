<aside id="sidebar" class="column">
    <!--
    <form class="quick_search">
        <input type="text" value="Quick Search">
    </form>
    <hr/>
    -->
    {if ($info_usuari['rol']=='Superadministrador') || ($info_usuari['unitat']=="CMK")}
    <h3>Notícies</h3>
    <ul class="toggle">
        <li class="icn_new_article"><i class="icon-doc-new"></i><a href="edit_headline.php">Nova notícia</a></li>
        <li class="icn_edit_article"><i class="icon-pencil"></i><a href="list_headline.php">Gestió notícies</a></li>
    </ul>
    {/if}

    {if ($info_usuari['rol']=='Superadministrador') || ($info_usuari['unitat']=='INF')}
    <h3>Eleccions</h3>
    <ul class="toggle">
        <li class="icn_new_article"><i class="icon-doc-new"></i><a href="edit_participacio.php">Edició mesa i participació</a></li>        
        <li class="icn_new_article"><i class="icon-doc-new"></i><a href="edit_resultat.php">Edició resultats comicis</a></li>        
    </ul>
    {/if}

    {if $info_usuari['rol']=='Superadministrador'}
    <h3>Usuaris</h3>
    <ul class="toggle">
        <li class="icn_add_user"><a href="edit_user.php">Nou usuari</a></li>
        <li class="icn_view_users"><a href="list_user.php">Gestió usuaris</a></li>
    </ul>
    {/if}
    <h3>Altres</h3>
    <ul class="toggle">
        <li class="icn_jump_back"><i class="icon-logout"></i><a href="logout.php">Logout</a></li>
    </ul>

    <footer>
        <hr />
        <p><strong>Copyright &copy; 2015 Website Ajuntament de Tortosa</strong></p>
    </footer>
</aside><!-- end of sidebar -->