<?php
namespace webtortosa;

require_once("config.cfg");
require_once (FOLDER_MODEL."/db.class.php");
require_once (FOLDER_MODEL."/headline.class.php");

$headlines_db = new \webtortosa\headline();

$serverName = "lwdd131.servidoresdns.net"; //serverName\instanceName
$conn = mssql_connect( $serverName, 'qdt399', 'Tortosa4321');
mssql_select_db('qdt399', $conn)
or die('Could not select to qdt399 database');

if(!$conn ) {
    echo "Conexión no se pudo establecer.<br />";
    die( print_r( sqlsrv_errors(), true));
}



$strSQL = 'SELECT
            Headlines.codi,
            CAST(Headlines.titol as TEXT) AS titol,
            CAST(Headlines.subtitol as TEXT) AS subtitol,
            CAST(Headlines.descripcio as TEXT) AS descripcio,
            Headlines.activa,
            Headlines.foto,
            Headlines.div,
            Headlines.dfv,
            Headlines.link,
            Headlines.data,
            Headlines.arxiu,
            Headlines.ure,
            Headlines.login
            FROM Headlines
            ORDER BY codi ASC
            ';

$query = mssql_query($strSQL, $conn);

if (mssql_num_rows($query) > 0){
    while ($row=mssql_fetch_array($query)) {
        $time = strtotime($row['div']);
        $div = date("Y/m/d h:i:s", $time);
        $time = strtotime($row['dfv']);
        $dfv = date("Y/m/d h:i:s", $time);
        $time = strtotime($row['data']);
        $data = date("Y/m/d h:i:s", $time);

        $data_transfer = array(
            'TITOL'         => filtrar($row["titol"]),
            'SUBTITOL'      => filtrar($row["subtitol"]),
            'DESCRIPCIO'    => filtrar($row["descripcio"]),
            'ACTIVA'        => $row["activa"],
            'FOTO'          => filtrar($row["foto"]),
            'DIV'           => $div,
            'DFV'           => $dfv,
            'LINK'          => $row["link"],
            'DATA'          => $data,
            'ADJUNT'        => filtrar($row["arxiu"]),
            'ID_URE'        => $row["ure"],
            'LOGIN'         => $row["login"],
            'ID_TIPOLOGIA'  => '1',
            'IMPORT'        => 1
        );
        $result = $headlines_db->addRow('HEAD_HEADLINE', $data_transfer);
        $id_headline = $result['mysql_insert_id'];

        $strSQL = 'SELECT Tematica.id_tematica FROM Tematica
                   INNER JOIN Headline_Tematica ON Headline_Tematica.id_tematica = Tematica.id_tematica
                   WHERE Headline_Tematica.id_headline = '.$row['codi'];


        $query2 = mssql_query($strSQL, $conn);


        if (mssql_num_rows($query2) > 0) {
            while ($row_tem = mssql_fetch_array($query2)) {
                if($row_tem['id_tematica']==61)
                    $tematica = 31;
                else
                    $tematica = $row_tem['id_tematica'];

                $tematica_arr = array(
                    'ID_HEADLINE'   => $id_headline,
                    'ID_TEMATICA'   => $tematica
                );
                $result = $headlines_db->addRow('HEAD_TEMATICA', $tematica_arr);
            }
        }

    }
} else {
    echo "No hay resultados";
}

echo '<!----------------  Data Finish -------------------------->';

function filtrar($input) {
    $text2_arr = str_split(utf8_encode($input));
    $text2_arr_def = array();
    foreach($text2_arr as &$item) {
        //echo $item." > ". ord($item) ."<br>";
        switch(ord($item)) {
            case 146:
                $item = "'";
                break;
            case 243:
                $item = "'";
                break;
            case 183:
                $item = "'";
                break;
            case 231:
                $item = "'";
                break;
            case 237:
                $item = "'";
                break;
            case 147:
                $item = "'";
                break;
            case 233:
                $item = "'";
                break;
            case 180:
                $item = "'";
                break;
            case 148:
                $item = "\"";
                break;
        }
        if(ord($item)!=194)
            $text2_arr_def[] = $item;
    }
    $output = addslashes(str_replace("''", "'", implode($text2_arr_def)));
    return $output;
}
?>