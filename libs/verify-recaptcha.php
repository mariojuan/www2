<?php

if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])):
    //your site secret key
    $secretKey = '6Lfdwh4TAAAAADWgTt5scfFMPEaJH2V7re-Oer0D';
    //get verify response data
    $ch = curl_init();

    curl_setopt_array($ch, [
        CURLOPT_URL => 'https://www.google.com/recaptcha/api/siteverify',
        CURLOPT_POST => true,
        CURLOPT_POSTFIELDS => [
            'secret' => $secretKey,
            'response' => $_POST['g-recaptcha-response'],
            'remoteip' => $_SERVER['REMOTE_ADDR']
        ],
        CURLOPT_RETURNTRANSFER => true
    ]);

    $output = curl_exec($ch);
    curl_close($ch);

    $responseData = json_decode($output);
    if($responseData->success):
        //captacha validated successfully.
        echo "1";
    else:
        echo "0";
    endif;
else:
    echo "0";
endif;
?>