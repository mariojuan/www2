<?php /* Smarty version Smarty-3.1.16, created on 2017-12-28 17:11:21
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/memoria-democratica2/item.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3119281315a4517a9318280-89623975%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a1e0d957fdff4099b0b48916d0e8206839d84df6' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/memoria-democratica2/item.tpl',
      1 => 1499252468,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3119281315a4517a9318280-89623975',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'LABEL_TITOL1' => 0,
    'MENU' => 0,
    'itemMenu' => 0,
    'lblName' => 0,
    'item' => 0,
    'lblCause' => 0,
    'lblPlace' => 0,
    'lblAge' => 0,
    'lblPlaceOfBirth' => 0,
    'lblFiliation' => 0,
    'lblProfession' => 0,
    'lblCivilStatus' => 0,
    'lblCivilRegistration' => 0,
    'lblOrigin' => 0,
    'lblObservations' => 0,
    'lblBack' => 0,
    'lang' => 0,
    'page' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a4517a9488734_24531216',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a4517a9488734_24531216')) {function content_5a4517a9488734_24531216($_smarty_tpl) {?><html>
<head>
    <?php echo $_smarty_tpl->getSubTemplate ("head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

</head>
<body>
<?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<div id="page-wrap">
    <div id="conttranstotalcos">
        <div id="conttranscos">
            <div id="btranspresen">
                <div id="transimatge">
                    <img src="/images/laciutat/municipi/tira.jpg" class="img-slider-territori"/>
                </div>
                <div id="transimatgeText">
                    <h1>
                        <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

                    </h1>
                </div>
            </div>
            <ul id='menu_planA'>
                <?php  $_smarty_tpl->tpl_vars['itemMenu'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['itemMenu']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['MENU']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['itemMenu']->key => $_smarty_tpl->tpl_vars['itemMenu']->value) {
$_smarty_tpl->tpl_vars['itemMenu']->_loop = true;
?>
                    <a href=<?php echo $_smarty_tpl->tpl_vars['itemMenu']->value;?>
>
                        <li id='menu_planA_item'>
                            <?php echo $_smarty_tpl->tpl_vars['itemMenu']->key;?>

                        </li>
                    </a>
                <?php } ?>
            </ul>
            <div id="tcentre_planA" class="memoria-democratica">
                <h2><?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>
</h2>
                <table id="item" name="item">
                    <tr>
                        <td><?php echo $_smarty_tpl->tpl_vars['lblName']->value;?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['item']->value->NOM;?>
</td>
                    </tr>
                    <tr>
                        <td><?php echo $_smarty_tpl->tpl_vars['lblCause']->value;?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['item']->value->CAUSA;?>
</td>
                    </tr>
                    <tr>
                        <td><?php echo $_smarty_tpl->tpl_vars['lblPlace']->value;?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['item']->value->LLOC;?>
</td>
                    </tr>
                    <tr>
                        <td><?php echo $_smarty_tpl->tpl_vars['lblAge']->value;?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['item']->value->EDAT;?>
</td>
                    </tr>
                    <tr>
                        <td><?php echo $_smarty_tpl->tpl_vars['lblPlaceOfBirth']->value;?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['item']->value->LLOC_NAIXEMENT;?>
</td>
                    </tr>
                    <tr>
                        <td><?php echo $_smarty_tpl->tpl_vars['lblFiliation']->value;?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['item']->value->FILIACIO;?>
</td>
                    </tr>
                    <tr>
                        <td><?php echo $_smarty_tpl->tpl_vars['lblProfession']->value;?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['item']->value->Professio;?>
</td>
                    </tr>
                    <tr>
                        <td><?php echo $_smarty_tpl->tpl_vars['lblCivilStatus']->value;?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['item']->value->ESTAT_CIVIL;?>
</td>
                    </tr>
                    <tr>
                        <td><?php echo $_smarty_tpl->tpl_vars['lblCivilRegistration']->value;?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['item']->value->REG_CIVIL;?>
</td>
                    </tr>
                    <tr>
                        <td><?php echo $_smarty_tpl->tpl_vars['lblOrigin']->value;?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['item']->value->ORIGEN_INFORMACIO;?>
</td>
                    </tr>
                    <tr>
                        <td><?php echo $_smarty_tpl->tpl_vars['lblObservations']->value;?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['item']->value->OBSERVACIONS;?>
</td>
                    </tr>
                </table>
                <div id="buttons-container">
                    <input type="button" name="enrere" id="enrere" value="<?php echo $_smarty_tpl->tpl_vars['lblBack']->value;?>
" onclick="JavaScript:window.location.href='/memoria-democratica/base-dades.php?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
&accio=list&page=<?php echo $_smarty_tpl->tpl_vars['page']->value;?>
';">
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

</body>
</html>


<?php }} ?>
