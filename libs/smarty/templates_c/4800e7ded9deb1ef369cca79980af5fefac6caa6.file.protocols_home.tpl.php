<?php /* Smarty version Smarty-3.1.16, created on 2017-12-12 16:10:22
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/protocols/protocols_home.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2673514435a2ff15e33d325-30143836%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4800e7ded9deb1ef369cca79980af5fefac6caa6' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/protocols/protocols_home.tpl',
      1 => 1499250586,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2673514435a2ff15e33d325-30143836',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'image' => 0,
    'LABEL_TITOL1' => 0,
    'MENU' => 0,
    'itemMenu' => 0,
    'LABEL_TITOL0' => 0,
    'item' => 0,
    'LABEL_TC1' => 0,
    'LABEL_TC2' => 0,
    'LABEL_TC3' => 0,
    'LABEL_TC4' => 0,
    'LABEL_TC5' => 0,
    'LABEL_TC6' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a2ff15e38a654_45284446',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a2ff15e38a654_45284446')) {function content_5a2ff15e38a654_45284446($_smarty_tpl) {?><!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->
<div id="page-wrap">
<div class="contenedor-responsive">
   <div id="conttranstotalcos">
      <div id="conttranscos">
         <div id="btranspresen">
            <div id="transimatge">          
               <img src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
" class="img-slider-territori"/>           
            </div>
            <div id="transimatgeText">
               <h1>
                  <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

                  <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
               </h1>
            </div>
         </div>
         <ul id='menu_planA'>
            <?php  $_smarty_tpl->tpl_vars['itemMenu'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['itemMenu']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['MENU']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['itemMenu']->key => $_smarty_tpl->tpl_vars['itemMenu']->value) {
$_smarty_tpl->tpl_vars['itemMenu']->_loop = true;
?>
            <a href=<?php echo $_smarty_tpl->tpl_vars['itemMenu']->value['link'];?>
 target="_<?php echo $_smarty_tpl->tpl_vars['itemMenu']->value['target'];?>
">
               <li id='menu_planA_item'>
                  <?php echo $_smarty_tpl->tpl_vars['itemMenu']->value['name'];?>

               </li>
            </a>
            <?php } ?>
         </ul>
         <div id="tcentre_planA">
            <font size=4 color=#666666><?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL0']->value;?>
</font>
            <?php if ($_smarty_tpl->tpl_vars['item']->value==1) {?>
            <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
</p>
            <?php } elseif ($_smarty_tpl->tpl_vars['item']->value==4) {?>
            <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
<strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
</strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC3']->value;?>
</p>
            <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC4']->value;?>
</p>
            <p><a href="http://www.tortosa.cat/webajt/decaleg.pdf" target="_blank"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC5']->value;?>
</a></p>

            <?php } elseif ($_smarty_tpl->tpl_vars['item']->value==5) {?>
            <div id="taulaicorpo" class="taulaBannersMod">
               <div id="filaicorpo">
                  <div class="colicorpo taulaBannersMod" id="col1icorpo">.TIF (6,97Mb) </div>
                  <div class="colicorpo taulaBannersMod"><a href="http://www.tortosa.cat/webajt/ajunta/imatge/horitzontal.tif" target="_blank"><img src="/images/protocols/logo1.jpg"></a></div>
                  <div class="colicorpo taulaBannersMod" id="col3icorpo"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC4']->value;?>
</div>
               </div>
               <div id="filaicorpo">
                  <div class="colicorpo taulaBannersMod" id="col1icorpo">.JPG (461Kb) </div>
                  <div class="colicorpo taulaBannersMod" id="col2icorpo"><a href="http://www.tortosa.cat/webajt/ajunta/imatge/horitzontal.jpg" target="_blank"><img src="/images/protocols/logo1.jpg"></a></div>
                  <div class="colicorpo taulaBannersMod" id="col3icorpo"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC4']->value;?>
</div>
               </div>
               <div id="filaicorpo">
                  <div class="colicorpo taulaBannersMod" id="col1icorpo">.TIF (5,61Mb) </div>
                  <div class="colicorpo taulaBannersMod" id="col2icorpo"><a href="http://www.tortosa.cat/webajt/ajunta/imatge/vertical.tif" target="_blank"><img src="/images/protocols/logo2.jpg"></a></div>
                  <div class="colicorpo taulaBannersMod" id="col3icorpo"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC5']->value;?>
</div>
               </div>
               <div id="filaicorpo">
                  <div class="colicorpo taulaBannersMod" id="col1icorpo">.JPG (455Kb) </div>
                  <div class="colicorpo taulaBannersMod" id="col2icorpo"><a href="http://www.tortosa.cat/webajt/ajunta/imatge/vertical.jpg" target="_blank"><img src="/images/protocols/logo2.jpg"></a></div>
                  <div class="colicorpo taulaBannersMod" id="col3icorpo"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC5']->value;?>
</div>
               </div>
               <div id="filaicorpo">
                  <div class="colicorpo taulaBannersMod" id="col1icorpo">.FH9 (121Kb) </div>
                  <div class="colicorpo taulaBannersMod" id="col2icorpo"><a href="http://www.tortosa.cat/webajt/ajunta/imatge/horitzontal.fh9" target="_blank"><img src="/images/protocols/logo3.jpg"></a></div>
                  <div class="colicorpo taulaBannersMod" id="col3icorpo"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC6']->value;?>
</div>
               </div>
               <div id="filaicorpo">
                  <div class="colicorpo taulaBannersMod" id="col1icorpo">.PDF (45,4Kb) </div>
                  <div class="colicorpo taulaBannersMod" id="col2icorpo"><a href="http://www.tortosa.cat/webajt/ajunta/imatge/escuts.pdf" target="_blank"><img src="/images/protocols/logo3.jpg"></a></div>
                  <div class="colicorpo taulaBannersMod" id="col3icorpo"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC6']->value;?>
</div>
               </div>
               <div id="filaicorpo">
                  <div class="colicorpo taulaBannersMod" id="col1icorpo">.JPG (125Kb) </div>
                  <div class="colicorpo taulaBannersMod" id="col2icorpo"><a href="http://www.tortosa.cat/webajt/ajunta/imatge/aiguestortosa.jpg" target="_blank"><img src="/images/protocols/logo4.jpg"></a></div>
                  <div class="colicorpo taulaBannersMod" id="col3icorpo">Aigües de Tortosa</div>
               </div>
               <div id="filaicorpo">
                  <div class="colicorpo taulaBannersMod" id="col1icorpo">.JPG (575Kb) </div>
                  <div class="colicorpo taulaBannersMod" id="col2icorpo"><a href="http://www.tortosa.cat/webajt/ajunta/imatge/tortosasport.jpg" target="_blank"><img src="/images/protocols/logo5.jpg"></a></div>
                  <div class="colicorpo taulaBannersMod" id="col3icorpo">Tortosasport</div>
               </div>
            </div>
            <?php }?>

         </div>
      </div>
   </div>
   </div>
</div><?php }} ?>
