<?php /* Smarty version Smarty-3.1.16, created on 2017-12-12 16:10:22
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/atencio_ciutadana/atencio_ciutadana_home.tpl" */ ?>
<?php /*%%SmartyHeaderCode:19942702235a2ff15e461303-84295259%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '15089aef41a7158e6a3ab52a26ab9063a1293ac2' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/atencio_ciutadana/atencio_ciutadana_home.tpl',
      1 => 1499249180,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '19942702235a2ff15e461303-84295259',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'image' => 0,
    'LABEL_TITOL1' => 0,
    'MENU' => 0,
    'itemMenu' => 0,
    'type' => 0,
    'LABEL_TC0' => 0,
    'LABEL_TC1' => 0,
    'LABEL_TC2' => 0,
    'LABEL_TC3' => 0,
    'LABEL_TC4' => 0,
    'LABEL_TC5' => 0,
    'LABEL_TC6' => 0,
    'LABEL_TC7' => 0,
    'LABEL_TC8' => 0,
    'LABEL_TC9' => 0,
    'LABEL_TC10' => 0,
    'LABEL_TC11' => 0,
    'LABEL_TC12' => 0,
    'LABEL_TC13' => 0,
    'LABEL_TC14' => 0,
    'LABEL_TC15' => 0,
    'LABEL_TC16' => 0,
    'LABEL_TC17' => 0,
    'LABEL_TC18' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a2ff15e5880c6_89005403',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a2ff15e5880c6_89005403')) {function content_5a2ff15e5880c6_89005403($_smarty_tpl) {?><!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->
<div id="page-wrap">

   <div class="contenedor-responsive">

      <div id="conttranstotalcos">
         <div id="conttranscos">

            <div id="btranspresen">

               <div id="transimatge">
                  <img src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
" class="img-slider-territori"/>
               </div>

               <div id="transimatgeText">
                  <h1>
                     <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

                     <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                  </h1>
               </div>

            </div>

            <ul id='menu_planA'>
               <?php  $_smarty_tpl->tpl_vars['itemMenu'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['itemMenu']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['MENU']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['itemMenu']->key => $_smarty_tpl->tpl_vars['itemMenu']->value) {
$_smarty_tpl->tpl_vars['itemMenu']->_loop = true;
?>
               <a href=<?php echo $_smarty_tpl->tpl_vars['itemMenu']->value['link'];?>
 target="_<?php echo $_smarty_tpl->tpl_vars['itemMenu']->value['target'];?>
">
                  <li id='menu_planA_item'>
                     <?php echo $_smarty_tpl->tpl_vars['itemMenu']->value['name'];?>

                  </li>
               </a>
               <?php } ?>
            </ul>

            <div id="tcentre_planA">
               <?php if ($_smarty_tpl->tpl_vars['type']->value==1) {?>
               <!-- Presentació Atenció Ciutadana-->
               <h2><?php echo $_smarty_tpl->tpl_vars['LABEL_TC0']->value;?>
</h2>
               <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
</p>
               <?php } elseif ($_smarty_tpl->tpl_vars['type']->value==2) {?>
               <!--Horaris d'atenció -->
               <h2><?php echo $_smarty_tpl->tpl_vars['LABEL_TC0']->value;?>
</h2>
               <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
</p>
               <ul class="subvencions1">
                  <li> <i class="icon-bank"> </i> &nbsp;
                     <?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
 <a href="http://www.seu-e.cat/web/tortosa" target="_blank"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC3']->value;?>
 </a>
                  </li>
                  <li >
                     <i class="icon-bank"> </i> &nbsp;
                     <?php echo $_smarty_tpl->tpl_vars['LABEL_TC4']->value;?>
 <b> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC5']->value;?>
 </b>
                     <ul>
                        <li> 
                           <?php echo $_smarty_tpl->tpl_vars['LABEL_TC6']->value;?>

                        </li>
                        <li> 
                           <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7']->value;?>

                        </li>
                     </ul>
                  </li>
                  <li><i class="icon-bank"> </i> &nbsp;
                     <?php echo $_smarty_tpl->tpl_vars['LABEL_TC8']->value;?>

                  </li>
                  <li>
                     <i class="icon-bank"> </i> &nbsp;
                     <?php echo $_smarty_tpl->tpl_vars['LABEL_TC9']->value;?>
 <b> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC10']->value;?>
 </b>
                     <ul>
                        <li> 
                           <?php echo $_smarty_tpl->tpl_vars['LABEL_TC11']->value;?>

                        </li>
                        <li> 
                           <?php echo $_smarty_tpl->tpl_vars['LABEL_TC12']->value;?>

                        </li>
                        <li> 
                           <?php echo $_smarty_tpl->tpl_vars['LABEL_TC13']->value;?>

                        </li>
                     </ul>
                  </li>
                  <li>
                     <i class="icon-bank"> </i> &nbsp;
                     <?php echo $_smarty_tpl->tpl_vars['LABEL_TC14']->value;?>
 <b> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC15']->value;?>
 </b>
                     <ul>
                        <li> 
                           <?php echo $_smarty_tpl->tpl_vars['LABEL_TC16']->value;?>

                        </li>
                        <li> 
                           <?php echo $_smarty_tpl->tpl_vars['LABEL_TC17']->value;?>

                        </li>
                        <li> 
                           <?php echo $_smarty_tpl->tpl_vars['LABEL_TC18']->value;?>

                        </li>
                     </ul>
                  </li>
               </ul>
               <?php } elseif ($_smarty_tpl->tpl_vars['type']->value==3) {?>
               <!--Seu Electrònica -->
               <h2><?php echo $_smarty_tpl->tpl_vars['LABEL_TC0']->value;?>
</h2>
               <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
  <a href="http://www.seu-e.cat/web/tortosa" target="_blank"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
 </a>
               </p>
               <?php } elseif ($_smarty_tpl->tpl_vars['type']->value==4) {?>
               <!-- Contacta-->
               <h2><?php echo $_smarty_tpl->tpl_vars['LABEL_TC0']->value;?>
</h2>
               <p><b><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
</b> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
</p>
               <p><b><?php echo $_smarty_tpl->tpl_vars['LABEL_TC3']->value;?>
</b> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC4']->value;?>
</p>
               <p><b><?php echo $_smarty_tpl->tpl_vars['LABEL_TC5']->value;?>
</b> <a href="mailto:<?php echo $_smarty_tpl->tpl_vars['LABEL_TC6']->value;?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC6']->value;?>
</a></p>
               <?php }?>
            </div>
            
            <div class="separator"></div>
         </div>
      </div>
   </div>
</div><?php }} ?>
