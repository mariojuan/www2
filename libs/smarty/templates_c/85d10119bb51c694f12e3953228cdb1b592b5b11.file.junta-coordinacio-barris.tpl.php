<?php /* Smarty version Smarty-3.1.16, created on 2017-12-18 12:21:28
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/altres-organs-municipals/junta-coordinacio-barris.tpl" */ ?>
<?php /*%%SmartyHeaderCode:6019956365a37a4b8574954-93397022%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '85d10119bb51c694f12e3953228cdb1b592b5b11' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/altres-organs-municipals/junta-coordinacio-barris.tpl',
      1 => 1499162000,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6019956365a37a4b8574954-93397022',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'image' => 0,
    'LABEL_TITOL1' => 0,
    'LABEL_TC0' => 0,
    'LABEL_TC2' => 0,
    'LABEL_TC3' => 0,
    'LABEL_T4' => 0,
    'LABEL_TC5' => 0,
    'LABEL_TC6' => 0,
    'LABEL_TC7' => 0,
    'DECRETS1' => 0,
    'item' => 0,
    'LABEL_TC9' => 0,
    'COMPOSICIO1' => 0,
    'LABEL_NORMATIVA_E' => 0,
    'LABEL_NORMATIVA' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a37a4b86b6255_31153867',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a37a4b86b6255_31153867')) {function content_5a37a4b86b6255_31153867($_smarty_tpl) {?><html>
	<head>

        <?php echo $_smarty_tpl->getSubTemplate ("head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	</head>
	<body>

		<?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


		<div id="page-wrap">

        <div class="contenedor-responsive">

    <div id="conttranstotalcos">
        <div id="conttranscos">
            <div id="btranspresen">
                <div id="transimatge">
                    <img src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
" class="img-slider-territori"/>
                </div>
                <div id="transimatgeText">
                    <h1>
                        <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

                        <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                    </h1>
                </div>
            </div>

            <!-- Menu -->
                <?php echo $_smarty_tpl->getSubTemplate ("altres-organs-municipals/menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            <!--// Menu -->

            <div id="tcentre_planA">
                <font size="4" color="#666666"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC0']->value;?>
</font>
                <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
</p>
                <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC3']->value;?>
</p>
                <p><?php echo $_smarty_tpl->tpl_vars['LABEL_T4']->value;?>
</p>
                <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC5']->value;?>
</p>
                <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC6']->value;?>
</p>
                <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC7']->value;?>
</p>

                <!-- Decrets -->
                <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['DECRETS1']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
                    <p><a href="<?php echo $_smarty_tpl->tpl_vars['item']->value['link'];?>
" target="<?php echo $_smarty_tpl->tpl_vars['item']->value['target'];?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
</a></p>
                <?php } ?>

                <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC9']->value;?>
</p>
                
                <ul>
                <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['COMPOSICIO1']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
                    <li><?php echo $_smarty_tpl->tpl_vars['item']->value['occupation'];?>
 - <?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
</li>
                <?php } ?>
                </ul>

                <p  class="normativa"><strong><?php echo $_smarty_tpl->tpl_vars['LABEL_NORMATIVA_E']->value;?>
</strong><?php echo $_smarty_tpl->tpl_vars['LABEL_NORMATIVA']->value;?>
</p>
                
            </div>
        </div>
    </div>
    </div>
</div>

		<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	</body>
</html><?php }} ?>
