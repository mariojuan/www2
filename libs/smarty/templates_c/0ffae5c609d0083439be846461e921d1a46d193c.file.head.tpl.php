<?php /* Smarty version Smarty-3.1.16, created on 2017-12-12 14:57:25
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/head.tpl" */ ?>
<?php /*%%SmartyHeaderCode:16099228085a2fe045a938f0-47578682%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0ffae5c609d0083439be846461e921d1a46d193c' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/head.tpl',
      1 => 1510320223,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '16099228085a2fe045a938f0-47578682',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'title_page' => 0,
    'meta_description' => 0,
    'url' => 0,
    'site_name' => 0,
    'responsive' => 0,
    'apartat' => 0,
    'path_eleccions' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a2fe045b25cf1_01916033',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a2fe045b25cf1_01916033')) {function content_5a2fe045b25cf1_01916033($_smarty_tpl) {?><title><?php echo $_smarty_tpl->tpl_vars['title_page']->value;?>
</title>
<meta name="description" content="<?php echo $_smarty_tpl->tpl_vars['meta_description']->value;?>
">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1.0">

<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@tortosa" />
<meta name="twitter:title" content="<?php echo $_smarty_tpl->tpl_vars['title_page']->value;?>
" />
<meta name="twitter:description" content="<?php echo $_smarty_tpl->tpl_vars['meta_description']->value;?>
" />
<meta name="twitter:image" content="https://pbs.twimg.com/profile_images/1152077486/logo_twitter_400x400.jpg" />

<!-- Open Graph data -->
<meta property="og:title" content="<?php echo $_smarty_tpl->tpl_vars['title_page']->value;?>
" />
<meta property="og:url" content="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
" />
<meta property="og:image" content="http://www2.tortosa.cat/logos/logo-vertical.jpg" />
<meta property="og:description" content="<?php echo $_smarty_tpl->tpl_vars['meta_description']->value;?>
" />
<meta property="og:site_name" content="<?php echo $_smarty_tpl->tpl_vars['site_name']->value;?>
" />

<!-- Styles Libraries -->
<link rel="shortcut icon" href="/ajt.ico">
<link href="/css/estils.css" rel="stylesheet" type="text/css">
<link href="/css/responsive.css" rel="stylesheet" type="text/css">
<?php if ($_smarty_tpl->tpl_vars['responsive']->value!='') {?>
    <!-- Estils en desenvolupament -->
    <!-- Versió tablet/phone, etc  -->
    <link href="/css/responsive2.css" rel="stylesheet" type="text/css">
    <!-- Versió escriptori  -->
<?php }?>
<link href="/css/estils_v2.css" rel="stylesheet" type="text/css">
<link href="/css/estils_yves.css" rel="stylesheet" type="text/css">
<link rel='stylesheet' href='/fonts/fontello-fafedd5f/css/search.css'>
<link rel='stylesheet' href='/fonts/fontello-fafedd5f/css/animation.css'>
<link rel="stylesheet" href="/admin/css/jquery-ui.css">
<link rel="stylesheet" href="/css/jquery.bxslider.css"/>


<!-- JS Libraries -->
<script src="http://www.tortosa.cat/js/google_analytics.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="/js/jquery.bxslider.min.js"></script>
<script type='text/javascript' src='/js/datanova.js' language="javascript"></script>

<script src="/admin/js/jquery-ui.js" type="text/javascript"></script>
<script src="/admin/js/jquery.validate.js" type="text/javascript"></script>


<?php if ($_smarty_tpl->tpl_vars['apartat']->value=="eleccions") {?>
    <SCRIPT LANGUAGE="Javascript" SRC="/chart/js/FusionCharts.js"></SCRIPT>
<?php }?>
<script src="http://www.tortosa.cat/js/google_analytics.js"></script>
<?php if ($_smarty_tpl->tpl_vars['apartat']->value=="consulta-ciutadania") {?>
    <script type="text/javascript" src="/js/consulta-ciutadana.js"></SCRIPT>
<?php }?>

<script type="text/javascript" src="/chart/js/FusionCharts.js"></script>
<script>
    $( document ).ready(function() {
        $('.bxslider').bxSlider({
            auto: true
        });

        $( "#eleccions" ).change(function() {
            window.location.href = 'eleccions.php?accio=participacio_totals&eleccions='+( $( "#eleccions" ).val() );
        });
        $(window).click(function() {
            //Hide the menus if visible
            $("#widget_translate").css("display", "none");
        });
        $("#button_translate").click(function(event) {
            event.stopPropagation();
            $("#widget_translate").css("display", "block");
        });
        $("#google_translate_element").click(function(event) {
            event.stopPropagation();
        });

    });
    function PrintElem(elem, web) {
        $('#logos').css('display','block');
        $('#printer').css('display','none');
        switch (web) {
            case "participacio_totals":
                $('#chart-1').css('display','none');
                $('#chart-2').css('display','none');
                break;
            default:
                break;
        }
        Popup(jQuery(elem).html());
    }

    function Popup(data) {
        var mywindow = window.open('', 'my div', 'height=400,width=600');
        mywindow.document.write('<html><head><title></title>');
        mywindow.document.write('<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['path_eleccions']->value;?>
/style.css" type="text/css" type="text/css" />');
        mywindow.document.write('<style type="text/css">.test { color:red; } </style></head><body>');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');
        mywindow.document.close();
        mywindow.print();
    }

    function displayMenu() {
        $( "#menu_planA" ).slideToggle( "slow", function() {
        
        });
    }

</script>

<?php }} ?>
