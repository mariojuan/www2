<?php /* Smarty version Smarty-3.1.16, created on 2017-12-12 16:04:11
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/negocis/home_negocis.tpl" */ ?>
<?php /*%%SmartyHeaderCode:18418336475a2fefebba6871-37918991%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c01286165aa9dd13c8488156e3082982c92527fe' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/negocis/home_negocis.tpl',
      1 => 1499252623,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18418336475a2fefebba6871-37918991',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'LABEL_TITOL' => 0,
    'LABEL_TEXTINTRO' => 0,
    'LABEL_TEXTINTRO1' => 0,
    'LABEL_TEXTINTRO2' => 0,
    'LABEL_TEXTINTRO3' => 0,
    'LABEL_TITOLS1B' => 0,
    'URL_HOME1' => 0,
    'LABEL_ADM1' => 0,
    'URL_HOME2' => 0,
    'LABEL_ADM2' => 0,
    'URL_HOME4' => 0,
    'LABEL_ADM4' => 0,
    'URL_HOME5' => 0,
    'LABEL_ADM5' => 0,
    'URL_HOME3' => 0,
    'LABEL_ADM3' => 0,
    'LABEL_TITOLS1' => 0,
    'LABEL_BOTOA4' => 0,
    'LABEL_BOTOA4B' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a2fefebc96130_26706824',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a2fefebc96130_26706824')) {function content_5a2fefebc96130_26706824($_smarty_tpl) {?><div id="page-wrap">



   <div id="contenidortotalcos">

      <!-- FOTOS SLIDER -->
      <div class="rowCartellera">
         <div id="bciutatpresen" class="bciutatpresen-rectificat">

            <div id="ciutatimatge ciutatimatge-rectificat">
               <img src="/images/negocis/cap.jpg" class="img-slider-negocis"/>
            </div>
            <div id="negocisimatgeText" class="imatgeCapText">
               <h1><?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL']->value;?>
&nbsp;&nbsp;</h1>
            </div>

         </div>
      </div>
      <!--/ FOTOS SLIDER -->

      <!-- TEXT -->
      <div class="row">

         <p class="paragraf-personalitzat"><?php echo $_smarty_tpl->tpl_vars['LABEL_TEXTINTRO']->value;?>
</p>
         
         <?php if ($_smarty_tpl->tpl_vars['LABEL_TEXTINTRO1']->value!=''||$_smarty_tpl->tpl_vars['LABEL_TEXTINTRO2']->value!=''||$_smarty_tpl->tpl_vars['LABEL_TEXTINTRO3']->value!='') {?>
            <p class="paragraf-personalitzat">&#9679; <?php echo $_smarty_tpl->tpl_vars['LABEL_TEXTINTRO1']->value;?>
</p>
            <p class="paragraf-personalitzat">&#9679; <?php echo $_smarty_tpl->tpl_vars['LABEL_TEXTINTRO2']->value;?>
</p>
            <p class="paragraf-personalitzat">&#9679; <?php echo $_smarty_tpl->tpl_vars['LABEL_TEXTINTRO3']->value;?>
</p>
         <?php }?>
         
      </div>
      <!--/ TEXT -->

      <!-- NEGOCIS I AMBITS -->
      <div class="row">

         <div class="column-left-negocis">

            <div class="titolsSeccionsHome titolBig marge-titol-contingut">
               <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOLS1B']->value;?>

            </div>

            <div class="marc-serveis-ciutat">
               <ul class="negocis-ajuntament-list">
                  <li>
                     <a href="<?php echo $_smarty_tpl->tpl_vars['URL_HOME1']->value;?>
">
                        <?php echo $_smarty_tpl->tpl_vars['LABEL_ADM1']->value;?>

                     </a>
                  </li>
                  <li>
                     <a href="<?php echo $_smarty_tpl->tpl_vars['URL_HOME2']->value;?>
">
                        <?php echo $_smarty_tpl->tpl_vars['LABEL_ADM2']->value;?>

                     </a>
                  </li>
                   
                  <li>
                     <a href="<?php echo $_smarty_tpl->tpl_vars['URL_HOME4']->value;?>
">
                        <?php echo $_smarty_tpl->tpl_vars['LABEL_ADM4']->value;?>

                     </a>
                  </li>
                  <li>
                     <a href="<?php echo $_smarty_tpl->tpl_vars['URL_HOME5']->value;?>
">
                        <?php echo $_smarty_tpl->tpl_vars['LABEL_ADM5']->value;?>

                     </a>
                  </li>
                  <li>
                     <a href="<?php echo $_smarty_tpl->tpl_vars['URL_HOME3']->value;?>
">
                        <?php echo $_smarty_tpl->tpl_vars['LABEL_ADM3']->value;?>

                     </a>
                  </li>
               </ul>
            </div>

         </div>

         <div class="column-right-negocis">

            <div class="titolsSeccionsHome titolBig marge-titol-contingut">
               <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOLS1']->value;?>

            </div>

            <a href="http://www.firatortosa.cat/">
                <div class="marcArees arees-negocis">
                    <img src="/images/negocis/bfira.jpg" border="0" />
                </div>
            </a>

            <a href="http://viverempreses.tortosa.cat/">
                <div class="marcArees arees-negocis arees-negocis-last">
                    <img src="/images/negocis/bviver.jpg" />
                </div>
            </a>

            <a href="http://www.tortosaturisme.cat/">
                <div class="marcArees arees-negocis">
                    <img src="/images/negocis/bturisme.jpg" />
                </div>
            </a>

            <a href="https://agencia.tortosa.cat/">
                <div class="marcArees arees-negocis arees-negocis-last">
                    <img src="/images/negocis/bcfo.jpg" />
                    <div id="textNboto2b" class="textbotoa">
                        <?php echo $_smarty_tpl->tpl_vars['LABEL_BOTOA4']->value;?>

                    </div>
                    <div id="textN2boto2b" class="textbotoa">
                        <?php echo $_smarty_tpl->tpl_vars['LABEL_BOTOA4B']->value;?>

                    </div>
                </div>
            </a>
            
         </div>
         
      </div>
      <!--/ NEGOCIS I AMBITS -->

   </div>
</div><?php }} ?>
