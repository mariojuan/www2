<?php /* Smarty version Smarty-3.1.16, created on 2018-02-02 12:04:02
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/estadistica-habitants/estadistica-habitants.tpl" */ ?>
<?php /*%%SmartyHeaderCode:18444483655a30153dc1b519-51225171%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a70f26b8157739aed755dd4d88d2fd4e1b0db67b' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/estadistica-habitants/estadistica-habitants.tpl',
      1 => 1517568540,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18444483655a30153dc1b519-51225171',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a30153dcadc33_07855588',
  'variables' => 
  array (
    'LABEL_TITOL1' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a30153dcadc33_07855588')) {function content_5a30153dcadc33_07855588($_smarty_tpl) {?><html>
	<head>
        <?php echo $_smarty_tpl->getSubTemplate ("head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

        <style>
            #tcentre_planA .graf {
                width: 100%;
            }
        </style>
	</head>
	<body>
		<?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

        <div id="page-wrap">

        <div class="contenedor-responsive">

            <div id="conttranstotalcos">
                <div id="conttranscos">
                    <div id="btranspresen">
                        <div id="transimatge">
                            <img src=/images/laciutat/tira.jpg / class="img-slider-territori">
                        </div>
                        <div id="transimatgeText">
                            <h1>
                                <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

                            </h1>
                        </div>
                    </div>
                    <div id="tcentre_planA" style="left: 0 !important;">
                        <!--<iframe src="http://43500.tortosa.cat:8081/WebAJT/EstadistiquesHabitantsServlet" width="900" height="600" frameborder="0" style="border:solid 0px; width: 900px;" scrolling="no"></iframe>-->
                        <h2>Població totals a data 18/12/2017</h2>
                        <div style="margin: 0 auto 25px auto">
                            <img src="/estadistica-habitants/taula-homes-dones-totals.jpg" alt="Taula Homes - Dones totals">
                        </div>
                        <div style="margin: 0 auto 25px auto">
                            <img src="/estadistica-habitants/homes-dones-totals.jpg" alt="Totals Homes - Dones" class="graf">
                        </div>
                        <h2>Població per anys</h2>
                        <div style="margin: 0 auto 25px auto">
                            <img src="/estadistica-habitants/taula-homes-dones-per-anys.jpg" alt="Taula Homes - Dones per anys">
                        </div>
                        <div style="margin: 0 auto 25px auto">
                            <img src="/estadistica-habitants/poblacio-total-per-anys.jpg" alt="Població total per anys" class="graf">
                        </div>
                        <div style="margin: 0 auto 25px auto">
                            <img src="/estadistica-habitants/homes-dones-per-anys.jpg" alt="Homes - Dones per anys" class="graf">
                        </div>
                        <h2>Població per nuclis a data 18/12/2017</h2>
                        <div style="margin: 0 auto 25px auto">
                            <img src="/estadistica-habitants/taula-poblacio-per-nuclis.jpg" alt="Taula Població per nuclis">
                        </div>
                        <div style="margin: 0 auto 25px auto">
                            <img src="/estadistica-habitants/poblacio-total-per-nuclis.jpg" alt="Població per nuclis" class="graf">
                        </div>
                        <h2>Població per pais de procedència a data 18/12/2017</h2>
                        <div style="margin: 0 auto 25px auto">
                            <img src="/estadistica-habitants/taula-poblacio-per-pais.jpg" alt="Taula Població per pais de procedència">
                        </div>
                    </div>
                    <div class="separator"></div>
                </div>
            </div>

        </div>            

        </div>
		<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

	</body>
</html><?php }} ?>
