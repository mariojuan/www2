<?php /* Smarty version Smarty-3.1.16, created on 2017-12-13 02:28:19
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/subvencions/subvencions_associacions_esportives.tpl" */ ?>
<?php /*%%SmartyHeaderCode:298667365a30823312ac45-18022372%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6e567384d9e2f438e5c410f5528dc310c06c1be8' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/subvencions/subvencions_associacions_esportives.tpl',
      1 => 1507283751,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '298667365a30823312ac45-18022372',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'image' => 0,
    'LABEL_TITOL1' => 0,
    'LABEL_TITOL1b' => 0,
    'url_exercici_back' => 0,
    'MENU' => 0,
    'itemMenu' => 0,
    'LABEL_TITOL0' => 0,
    'LABEL_TC2_1PUNT_0' => 0,
    'LABEL_TC2_1TXT_0' => 0,
    'LABEL_TC2_1PUNT_1' => 0,
    'LABEL_TC2_1TXT_1' => 0,
    'LABEL_TC2_1PUNT_2' => 0,
    'LABEL_PUNT_2LI_1' => 0,
    'LABEL_PUNT_2LI_2' => 0,
    'LABEL_TC2_1PUNT_3' => 0,
    'LABEL_PUNT_3LI_1' => 0,
    'LABEL_PUNT_3LI_2' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a308233226037_55606406',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a308233226037_55606406')) {function content_5a308233226037_55606406($_smarty_tpl) {?><!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->
<div id="page-wrap">
<div class="contenedor-responsive">
   <div id="conttranstotalcos">
      <div id="conttranscos">
         <div id="btranspresen">
            <div id="transimatge">			
               <img src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
" class="img-slider-territori"/>			
            </div>
            <div id="transimatgeText">
               <h1>
                  <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

                  <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
               </h1>
            </div>
         </div>
         <div class="subtitolinfeco" id="subtitolinfeco2">
            <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1b']->value;?>

         </div>
         <div class="marcback">
            <a href="<?php echo $_smarty_tpl->tpl_vars['url_exercici_back']->value;?>
">
            <i class="icon-angle-double-up"></i>
            </a>
         </div>
         <ul id='menu_planA'>
            <?php  $_smarty_tpl->tpl_vars['itemMenu'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['itemMenu']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['MENU']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['itemMenu']->key => $_smarty_tpl->tpl_vars['itemMenu']->value) {
$_smarty_tpl->tpl_vars['itemMenu']->_loop = true;
?>
            <a href=<?php echo $_smarty_tpl->tpl_vars['itemMenu']->value;?>
>
               <li id='menu_planA_item'>
                  <?php echo $_smarty_tpl->tpl_vars['itemMenu']->key;?>

               </li>
            </a>
            <?php } ?>
         </ul>
         <div id="tcentre_planA">
            <font size=4 color=#666666>
            <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL0']->value;?>

            </font>

            <ul class="subvencions1">
               <li>
                  <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2_1PUNT_0']->value;?>
</strong></p>
                  <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2_1TXT_0']->value;?>
</p>
               </li>
               <li>
                  <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2_1PUNT_1']->value;?>
</strong></p>
                  <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2_1TXT_1']->value;?>
</p>
               </li>
               <li>
                  <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2_1PUNT_2']->value;?>
</strong></p>
                  <ul>
                     <li><?php echo $_smarty_tpl->tpl_vars['LABEL_PUNT_2LI_1']->value;?>
</li>
                     <li><?php echo $_smarty_tpl->tpl_vars['LABEL_PUNT_2LI_2']->value;?>
</li>
                  </ul>
               </li>
               <li>
                  <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2_1PUNT_3']->value;?>
</strong></p>
                  <ul class="subvencions1">
                      <li><i class="icon-file-pdf"></i>&nbsp;<a href="/subvencions/2017/bases_sub_esp_17.pdf" target="_blank"><?php echo $_smarty_tpl->tpl_vars['LABEL_PUNT_3LI_1']->value;?>
</a></li>
                      <li><i class="icon-file-pdf"></i>&nbsp;<a href="/subvencions/2017/annex_sub_esp_17.pdf" target="_blank"><?php echo $_smarty_tpl->tpl_vars['LABEL_PUNT_3LI_2']->value;?>
</a></li>
                  </ul>
               </li>
            </ul>
            <br><br><br>
         </div>
      </div>
   </div>
</div>
</div><?php }} ?>
