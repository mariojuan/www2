<?php /* Smarty version Smarty-3.1.16, created on 2018-02-19 10:54:32
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/ple/actes_mocions.tpl" */ ?>
<?php /*%%SmartyHeaderCode:7561175775a2ff16140dc85-17016845%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '612005f7c2574ab115701081614ca43bdb621920' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/ple/actes_mocions.tpl',
      1 => 1519034059,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7561175775a2ff16140dc85-17016845',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a2ff1614aa577_20811668',
  'variables' => 
  array (
    'image' => 0,
    'LABEL_TITOL1' => 0,
    'LABEL_TITOLSECCIO' => 0,
    'LABEL_TC10' => 0,
    'LABEL_TC1' => 0,
    'LABEL_TC0' => 0,
    'LABEL_20152019' => 0,
    'LABEL_EXTRACTES' => 0,
    'LABEL_TC2015' => 0,
    'LABEL_EXTRACTEMOCIONS' => 0,
    'LABEL_TC2016' => 0,
    'LABEL_TC2017' => 0,
    'LABEL_20112015' => 0,
    'LABEL_TC2011' => 0,
    'LABEL_TC2012' => 0,
    'LABEL_TC2013' => 0,
    'LABEL_TC2014' => 0,
    'LABEL_TCTRACTAMENT_ESTADISTIC' => 0,
    'LABEL_TCINFORMACIO_TABULADA' => 0,
    'LABEL_TC2' => 0,
    'LABEL_TC3' => 0,
    'LABEL_TC4' => 0,
    'LABEL_TC5' => 0,
    'LABEL_TC6' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a2ff1614aa577_20811668')) {function content_5a2ff1614aa577_20811668($_smarty_tpl) {?><html>
<head>
	<?php echo $_smarty_tpl->getSubTemplate ("head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

</head>
<body>

	<?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<div id="page-wrap">

	<div class="contenedor-responsive">
	
		<div id="conttranstotalcos">
			<div id="conttranscos">
				<div id="btranspresen">
					<div id="transimatge">
						<img src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
" class="img-slider-territori"/>
					</div>
					<div id="transimatgeText">
						<h1>
							<?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

							<i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
						</h1>
					</div>
				</div>

				<!-- Menu -->
                <?php echo $_smarty_tpl->getSubTemplate ("ple/menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            	<!--// Menu -->

				<div id="tcentre_planA">

					<!-- Mocions Grups Polítics -->

					<font size="4" color="#666666"><?php echo $_smarty_tpl->tpl_vars['LABEL_TITOLSECCIO']->value;?>
</font>

					<p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC10']->value;?>
</p>

					<h3><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
</h3>

					<p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC0']->value;?>
</p>
					

					<h3><?php echo $_smarty_tpl->tpl_vars['LABEL_20152019']->value;?>
</h3>
					<p><i><?php echo $_smarty_tpl->tpl_vars['LABEL_EXTRACTES']->value;?>
</i></p>

					<ul class="listSenseBullets">
						<li><i class="icon-file-pdf"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC2015']->value;?>
 - <a href="http://www.tortosa.cat/webajt/ajunta/om/dades-plens/mocions-2015-2.pdf"><?php echo $_smarty_tpl->tpl_vars['LABEL_EXTRACTEMOCIONS']->value;?>
</a></li>
						<li><i class="icon-file-pdf"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC2016']->value;?>
 - <a href="http://www.tortosa.cat/webajt/ajunta/om/dades-plens/mocions-2016.pdf"><?php echo $_smarty_tpl->tpl_vars['LABEL_EXTRACTEMOCIONS']->value;?>
</a></li>
						<li><i class="icon-file-pdf"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC2017']->value;?>
 - <a href="http://www.tortosa.cat/webajt/ajunta/om/dades-plens/mocions-2017.pdf"><?php echo $_smarty_tpl->tpl_vars['LABEL_EXTRACTEMOCIONS']->value;?>
</a></li>
					</ul>

					<div class="separator"></div>

					<h3><?php echo $_smarty_tpl->tpl_vars['LABEL_20112015']->value;?>
</h3>
					<p><i><?php echo $_smarty_tpl->tpl_vars['LABEL_EXTRACTES']->value;?>
</i></p>
					<ul class="listSenseBullets">
						<li><i class="icon-file-pdf"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC2011']->value;?>
 - <a href="http://www.tortosa.cat/webajt/ajunta/om/dades-plens/mocions-2011.pdf"><?php echo $_smarty_tpl->tpl_vars['LABEL_EXTRACTEMOCIONS']->value;?>
</a></li>
						<li><i class="icon-file-pdf"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC2012']->value;?>
 - <a href="http://www.tortosa.cat/webajt/ajunta/om/dades-plens/mocions-2012.pdf"><?php echo $_smarty_tpl->tpl_vars['LABEL_EXTRACTEMOCIONS']->value;?>
</a></li>
						<li><i class="icon-file-pdf"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC2013']->value;?>
 - <a href="http://www.tortosa.cat/webajt/ajunta/om/dades-plens/mocions-2013.pdf"><?php echo $_smarty_tpl->tpl_vars['LABEL_EXTRACTEMOCIONS']->value;?>
</a></li>
						<li><i class="icon-file-pdf"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC2014']->value;?>
 - <a href="http://www.tortosa.cat/webajt/ajunta/om/dades-plens/mocions-2014.pdf"><?php echo $_smarty_tpl->tpl_vars['LABEL_EXTRACTEMOCIONS']->value;?>
</a></li>
						<li><i class="icon-file-pdf"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC2015']->value;?>
 - <a href="http://www.tortosa.cat/webajt/ajunta/om/dades-plens/mocions-2015-1.pdf"><?php echo $_smarty_tpl->tpl_vars['LABEL_EXTRACTEMOCIONS']->value;?>
</a></li>
					</ul>

					<p><?php echo $_smarty_tpl->tpl_vars['LABEL_TCTRACTAMENT_ESTADISTIC']->value;?>
</p>
					<p><i><?php echo $_smarty_tpl->tpl_vars['LABEL_TCINFORMACIO_TABULADA']->value;?>
</i></p>

					<a href="http://www.tortosa.cat/webajt/ajunta/om/dades-plens/estadistiques-grafiques-mocions-2011-2015.pdf">
						<img src="/templates/ple/img/grafic-mocions-mandat-2011-2015.jpg" height="130" width="300" class="marcterme img-responsive-95" />
					</a>

					<div class="separator"></div>

					<!-- Mocions per inicitiava popular -->

					<h3><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
</h3>
					<p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC3']->value;?>
</p>
					<ul>
						<li><?php echo $_smarty_tpl->tpl_vars['LABEL_TC4']->value;?>
</li>
						<li><?php echo $_smarty_tpl->tpl_vars['LABEL_TC5']->value;?>
</li>
						<li><?php echo $_smarty_tpl->tpl_vars['LABEL_TC6']->value;?>
</li>
					</ul>
				</div>
			</div>
			</div>
		</div>
	</div>


	<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

</body>
</html><?php }} ?>
