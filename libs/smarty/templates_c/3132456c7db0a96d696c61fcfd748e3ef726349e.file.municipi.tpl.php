<?php /* Smarty version Smarty-3.1.16, created on 2017-12-13 01:45:29
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/ciutat/municipi.tpl" */ ?>
<?php /*%%SmartyHeaderCode:14660821415a307829dc60b1-91009127%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3132456c7db0a96d696c61fcfd748e3ef726349e' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/ciutat/municipi.tpl',
      1 => 1498647471,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14660821415a307829dc60b1-91009127',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'image' => 0,
    'LABEL_TITOL1' => 0,
    'LABEL_PARAG1' => 0,
    'LABEL_PARAG2' => 0,
    'LABEL_MAP1' => 0,
    'LABEL_MAP2' => 0,
    'LABEL_TAULA1' => 0,
    'LABEL_TAULA2' => 0,
    'LABEL_TAULA3' => 0,
    'LABEL_TAULA4' => 0,
    'LABEL_TAULA5' => 0,
    'LABEL_PARAG3' => 0,
    'LABEL_PARAG4' => 0,
    'LABEL_PARAG5' => 0,
    'LABEL_PARAG6' => 0,
    'LABEL_PARAG7' => 0,
    'LABEL_PARAG8' => 0,
    'LABEL_PARAG9' => 0,
    'LABEL_PARAG10' => 0,
    'LABEL_PARAG11' => 0,
    'LABEL_PARAG12' => 0,
    'LABEL_PARAG13' => 0,
    'LABEL_PARAG14' => 0,
    'LABEL_PARAG15' => 0,
    'LABEL_LINK10' => 0,
    'LABEL_LINK12' => 0,
    'url_reguers' => 0,
    'LABEL_LINK14' => 0,
    'LABEL_LINK11' => 0,
    'url_vinallop' => 0,
    'LABEL_LINK13' => 0,
    'url_historia' => 0,
    'LABEL_LINK9' => 0,
    'LABEL_LINK1' => 0,
    'url_equipa' => 0,
    'LABEL_LINK2' => 0,
    'LABEL_LINK3' => 0,
    'LABEL_LINK5' => 0,
    'url_planols' => 0,
    'LABEL_LINK6' => 0,
    'LABEL_LINK7' => 0,
    'LABEL_LINK8' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a307829e2e860_24953481',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a307829e2e860_24953481')) {function content_5a307829e2e860_24953481($_smarty_tpl) {?><div id="page-wrap">

<div class="contenedor-responsive">

<div id="conttranstotalcos">
	<div id="conttranscos">
		<div id="btranspresen">
			<div id="transimatge">			
				<img src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
" class="img-slider-territori">			
			</div>
			<div id="transimatgeText">
				<h1>
				<?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

				</h1>
			</div>
		</div>
		<div id="tcentre_municipi">
			<?php echo $_smarty_tpl->tpl_vars['LABEL_PARAG1']->value;?>

			<br><br>
			<?php echo $_smarty_tpl->tpl_vars['LABEL_PARAG2']->value;?>

			<br><br>
			<div id="ciutatmap1">
					<img src="/images/laciutat/municipi/Imagen1.jpg" /><br><?php echo $_smarty_tpl->tpl_vars['LABEL_MAP1']->value;?>
								
			</div>
			<div id="ciutatmap2">
					<img src="/images/laciutat/municipi/Imagen2.jpg" /><br><?php echo $_smarty_tpl->tpl_vars['LABEL_MAP2']->value;?>
								
			</div>
			<br><br><br>
			<div id="taulaNMunicipi">
    			<div id="filaNXifres">
        			<div class="colNegocis" id="col1NXifres"><?php echo $_smarty_tpl->tpl_vars['LABEL_TAULA1']->value;?>
</div>
        			<div class="colNegocis" id="col2NXifres">33.743</div>
    			</div>
    			<div id="filaNXifres">
        			<div class="colNegocis" id="col1NXifres"><?php echo $_smarty_tpl->tpl_vars['LABEL_TAULA2']->value;?>
</div>
        			<div class="colNegocis" id="col2NXifres">218,5</div>
    			</div>
    			<div id="filaNXifres">
        			<div class="colNegocis" id="col1NXifres"><?php echo $_smarty_tpl->tpl_vars['LABEL_TAULA3']->value;?>
</div>
        			<div class="colNegocis" id="col2NXifres">12</div>
    			</div>
    			<div id="filaNXifres">
        			<div class="colNegocis" id="col1NXifres"><?php echo $_smarty_tpl->tpl_vars['LABEL_TAULA4']->value;?>
</div>
        			<div class="colNegocis" id="col2NXifres">0,523347</div>
    			</div>
    			<div id="filaNXifres">
        			<div class="colNegocis" id="col1NXifres"><?php echo $_smarty_tpl->tpl_vars['LABEL_TAULA5']->value;?>
</div>
        			<div class="colNegocis" id="col2NXifres"> 40,812064</div>
    			</div>
			</div>
			<br><br style="clear: both">
			<?php echo $_smarty_tpl->tpl_vars['LABEL_PARAG3']->value;?>

			<br><br>
			<?php echo $_smarty_tpl->tpl_vars['LABEL_PARAG4']->value;?>

			<br><br>
			<div id="ciutatmap3" >
					<img src="/images/laciutat/municipi/Imagen3.jpg" class="img-mapa-territori1"/>								
			</div>
			<br><br>
			<?php echo $_smarty_tpl->tpl_vars['LABEL_PARAG5']->value;?>

			<br><br>
			<?php echo $_smarty_tpl->tpl_vars['LABEL_PARAG6']->value;?>

			<br><br>
			<div id="ciutatmap1">
					<img src="/images/laciutat/municipi/Imagen4.jpg" />								
			</div>
			<div id="ciutatmap2">
					<img src="/images/laciutat/municipi/Imagen5.jpg" />								
			</div>
			<div style="margin-bottom: 25px">
			    <?php echo $_smarty_tpl->tpl_vars['LABEL_PARAG7']->value;?>

			</div>
            <div style="margin-bottom: 25px">
                <?php echo $_smarty_tpl->tpl_vars['LABEL_PARAG8']->value;?>

            </div>
            <div style="margin-bottom: 25px">
                <?php echo $_smarty_tpl->tpl_vars['LABEL_PARAG9']->value;?>

            </div>
            <div style="margin-bottom: 25px">
                <?php echo $_smarty_tpl->tpl_vars['LABEL_PARAG10']->value;?>

            </div>
            <div style="margin-bottom: 25px">
                <?php echo $_smarty_tpl->tpl_vars['LABEL_PARAG11']->value;?>

            </div>
            <div style="margin-bottom: 25px">
                <?php echo $_smarty_tpl->tpl_vars['LABEL_PARAG12']->value;?>

            </div>
            <div style="margin-bottom: 25px">
                <?php echo $_smarty_tpl->tpl_vars['LABEL_PARAG13']->value;?>

            </div>
            <div style="margin-bottom: 25px">
                <?php echo $_smarty_tpl->tpl_vars['LABEL_PARAG14']->value;?>

            </div>
            <div style="margin-bottom: 25px">
                <?php echo $_smarty_tpl->tpl_vars['LABEL_PARAG15']->value;?>

            </div>

			<ul class="municipi">
                <a href="http://www.bitem.altanet.org/" target="_blank">
                    <li><i class="icon-link"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_LINK10']->value;?>
</li>
                </a>
                <a href="http://www.campredo.cat" target="_blank">
                    <li><i class="icon-link"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_LINK12']->value;?>
</li>
                </a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['url_reguers']->value;?>
" target="_blank">
                    <li><i class="icon-link"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_LINK14']->value;?>
</li>
                </a>
                <a href="http://www.jesus.cat/" target="_blank">
                    <li><i class="icon-link"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_LINK11']->value;?>
</li>
                </a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['url_vinallop']->value;?>
" target="_self">
                    <li><i class="icon-link"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_LINK13']->value;?>
</li>
                </a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['url_historia']->value;?>
" target="_self">
				<li><i class="icon-link"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_LINK9']->value;?>
</li>
				</a>
				<a href="http://www2.tortosa.cat/ciutat/estadistica-habitants/" target="_blank">
				<li><i class="icon-link"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_LINK1']->value;?>
</li>
				</a>
				<a href="<?php echo $_smarty_tpl->tpl_vars['url_equipa']->value;?>
">
				<li><i class="icon-link"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_LINK2']->value;?>
</li>
				</a>
				<a href="http://serveisoberts.gencat.cat/equipaments#?adreca.municipi=Tortosa" target="_blank">
				<li><i class="icon-link-ext"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_LINK3']->value;?>
</li>
				</a>
				<a href="http://www.tortosa.cat/webajt/MunicipiIdescat.asp" target="_blank">
				<li><i class="icon-link"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_LINK5']->value;?>
</li>
				</a>
				<a href="<?php echo $_smarty_tpl->tpl_vars['url_planols']->value;?>
" target="_self">
				<li><i class="icon-link"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_LINK6']->value;?>
</li>
				</a>
				<!--<a href="http://www2.tortosa.cat/ciutat/planols.php" target="_self">
				<li><i class="icon-link"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_LINK7']->value;?>
</li>
				</a>
				<a href="http://www2.tortosa.cat/ciutat/planols.php" target="_self">
				<li><i class="icon-link"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_LINK8']->value;?>
</li>
				</a>-->
			</ul>
			<br><br><br>
		</div>
	
	</div>
</div>

</div>

</div>
<?php }} ?>
