<?php /* Smarty version Smarty-3.1.16, created on 2017-12-13 02:59:29
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/eleccions/participacio_meses.tpl" */ ?>
<?php /*%%SmartyHeaderCode:9406484255a3089816c7bc5-08371966%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5d9b345fb85207774f29ab39e8bf2e3b46b3a406' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/eleccions/participacio_meses.tpl',
      1 => 1490860747,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '9406484255a3089816c7bc5-08371966',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'LABEL_TITOL' => 0,
    'logo_eleccions' => 0,
    'title' => 0,
    'subtitle' => 0,
    'chart1' => 0,
    'chart2' => 0,
    'taula1' => 0,
    'titols' => 0,
    'titol' => 0,
    'valors' => 0,
    'valor' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a308981780bd1_82981066',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a308981780bd1_82981066')) {function content_5a308981780bd1_82981066($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("templates/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<div id="page-wrap" class="negocis eleccions">
    <div id="contajttotalcos">
        <div id="conttranscos">
            <div id="bciutatpresen">
                <div id="ciutatimatge">
                    <img src="/images/negocis/cap.jpg" />
                </div>
                <div id="negocisimatgeText" class="imatgeCapText">
                    <h1>
                        <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL']->value;?>

                        &nbsp;&nbsp;
                    </h1>
                </div>
            </div>

            <?php echo $_smarty_tpl->getSubTemplate ("templates/eleccions/menu_eleccions.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            <div id="tcentre_planA">
                <!--
                <div id="title">
                    <input type="button" name="imprimir" value="Imprimir" id="printer" class="print" onclick="PrintElem('#tcentre_planA', 'participacio_totals')">
                </div>
                <div id="logos">
                    <div>
                        <?php if ($_smarty_tpl->tpl_vars['logo_eleccions']->value!='') {?>
                            <img src="templates/eleccions/images/<?php echo $_smarty_tpl->tpl_vars['logo_eleccions']->value;?>
">
                        <?php } else { ?>
                            <?php echo $_smarty_tpl->tpl_vars['title']->value;?>

                        <?php }?>
                    </div>
                    <div>
                        <img src="templates/eleccions/images/logo-ajuntament.jpg">
                    </div>
                </div>
                -->
                <div id="section_title">
                    <?php echo $_smarty_tpl->tpl_vars['title']->value;?>

                    <?php if ($_smarty_tpl->tpl_vars['subtitle']->value) {?>
                        <br/><span id="text_dades_informatives"><?php echo $_smarty_tpl->tpl_vars['subtitle']->value;?>
</span>
                    <?php }?>
                </div>
                <?php echo $_smarty_tpl->tpl_vars['chart1']->value;?>

                <div id="chart-1">

                </div>
                <?php echo $_smarty_tpl->tpl_vars['chart2']->value;?>

                <div id="chart-2">

                </div>
                <?php if (isset($_smarty_tpl->tpl_vars['taula1']->value)) {?>
                <div>
                    <table class="data_table participacio_meses">
                        <tr>
                            <?php $_smarty_tpl->tpl_vars['titols'] = new Smarty_variable($_smarty_tpl->tpl_vars['taula1']->value['0'], null, 0);?>
                            <?php  $_smarty_tpl->tpl_vars['titol'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['titol']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['titols']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['titol']->key => $_smarty_tpl->tpl_vars['titol']->value) {
$_smarty_tpl->tpl_vars['titol']->_loop = true;
?>
                                <th><?php echo $_smarty_tpl->tpl_vars['titol']->value;?>
</th>
                            <?php } ?>
                        </tr>
                        <?php  $_smarty_tpl->tpl_vars['valors'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['valors']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['taula1']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['valors']->key => $_smarty_tpl->tpl_vars['valors']->value) {
$_smarty_tpl->tpl_vars['valors']->_loop = true;
?>
                            <tr>
                            <?php if ($_smarty_tpl->tpl_vars['valors']->key>0) {?>
                                <?php  $_smarty_tpl->tpl_vars['valor'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['valor']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['valors']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['valor']->key => $_smarty_tpl->tpl_vars['valor']->value) {
$_smarty_tpl->tpl_vars['valor']->_loop = true;
?>
                                    <td><?php echo $_smarty_tpl->tpl_vars['valor']->value;?>
</td>
                                <?php } ?>
                            <?php }?>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
                <?php }?>
            </div>
        </div>
	</div>
	<?php echo $_smarty_tpl->getSubTemplate ("templates/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

</div><?php }} ?>
