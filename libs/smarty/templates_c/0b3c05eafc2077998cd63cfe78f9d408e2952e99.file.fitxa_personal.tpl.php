<?php /* Smarty version Smarty-3.1.16, created on 2018-02-15 18:19:45
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/ple/fitxa_personal.tpl" */ ?>
<?php /*%%SmartyHeaderCode:428584125a30000e37d974-29890619%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0b3c05eafc2077998cd63cfe78f9d408e2952e99' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/ple/fitxa_personal.tpl',
      1 => 1518715183,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '428584125a30000e37d974-29890619',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a30000e44ae23_90314819',
  'variables' => 
  array (
    'LABEL_IMG' => 0,
    'LABEL_NOM' => 0,
    'LABEL_NAIXEMENT' => 0,
    'LABEL_PARTIT' => 0,
    'LABEL_TC2' => 0,
    'CARREC' => 0,
    'item' => 0,
    'ALTRESCARRECS' => 0,
    'LABEL_TC4' => 0,
    'DOCS' => 0,
    'LABEL_DOCS' => 0,
    'AREES' => 0,
    'LABEL_AMBITS' => 0,
    'LABEL_TC5' => 0,
    'CURRICULUM' => 0,
    'TRAJECTORIA' => 0,
    'LABEL_TC6' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a30000e44ae23_90314819')) {function content_5a30000e44ae23_90314819($_smarty_tpl) {?><html>

<head>

    <?php echo $_smarty_tpl->getSubTemplate ("head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


</head>

<body>

<?php echo $_smarty_tpl->getSubTemplate ("header_little.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


    <div id="page-wrap">
        <div id="conttranstotalcos">
            <div id="conttranscos">
                

                <div id="tcentre_municipi">

                    <div class="mainDetails">
                        
                        <div id="name">

                        <div class="quickFade delayTwo">
                            <img src="<?php echo $_smarty_tpl->tpl_vars['LABEL_IMG']->value;?>
" class="avatar2" />
                        </div>

                            <h1 class="quickFade delayTwo"><?php echo $_smarty_tpl->tpl_vars['LABEL_NOM']->value;?>
</h1>
                            <p class="quickFade delayTwo"><?php echo $_smarty_tpl->tpl_vars['LABEL_NAIXEMENT']->value;?>
</p>
                            <p class="quickFade delayTwo"><?php echo $_smarty_tpl->tpl_vars['LABEL_PARTIT']->value;?>
</p>

                            <h3 class="quickFade delayTwo"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
</h3>
                            <ul class="quickFade delayTwo">

                            <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['CARREC']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
                            <li><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
</li>
                            <?php } ?>
                            
                            </ul>

                            <?php if (isset($_smarty_tpl->tpl_vars['ALTRESCARRECS']->value)) {?>

                            <h3 class="quickFade delayThree"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC4']->value;?>
</h2>
                                <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['ALTRESCARRECS']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
                                <ul class="quickFade delayTwo">
                                    <li>
                                        <?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>

                                    </li>
                                </ul>
                                <?php } ?>

                                <?php }?>

                                <?php if (isset($_smarty_tpl->tpl_vars['DOCS']->value)) {?>

                            <h3 class="quickFade delayThree"><?php echo $_smarty_tpl->tpl_vars['LABEL_DOCS']->value;?>
</h2>
                                <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['DOCS']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
                                <ul class="quickFade delayTwo">
                                    <li>
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['item']->value['file'];?>
" target="<?php echo $_smarty_tpl->tpl_vars['item']->value['target'];?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
</a>
                                    </li>
                                </ul>
                                <?php } ?>

                                <?php }?>

                        </div>

                        <div class="clear"></div>
                    </div>

                    <div id="mainArea" class="quickFade delayFive">

                     <?php if (isset($_smarty_tpl->tpl_vars['AREES']->value)) {?>

                        <section>

                            <div class="sectionTitle">
                                <h1><?php echo $_smarty_tpl->tpl_vars['LABEL_AMBITS']->value;?>
</h1>
                            </div>

                            <div class="sectionContent">

                                <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['AREES']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
                                <article>
                                    <p><b><?php echo $_smarty_tpl->tpl_vars['item']->value['area'];?>
</b></br><?php echo $_smarty_tpl->tpl_vars['item']->value['nom'];?>
</p>
                                </article>
                                <?php } ?>

                            </div>

                            <div class="clear"></div>
                        </section>

                        <?php }?>

                        <section>
                            <article>
                                <div class="sectionTitle">
                                    <h1><?php echo $_smarty_tpl->tpl_vars['LABEL_TC5']->value;?>
</h1>
                                </div>

                                <div class="sectionContent">

                                    <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['CURRICULUM']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>

                                    <p><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
</p>

                                    <?php } ?>

                                </div>
                            </article>
                            <div class="clear"></div>
                        </section>

                        <?php if (isset($_smarty_tpl->tpl_vars['TRAJECTORIA']->value)) {?>
                        
                        <section>

                            <div class="sectionTitle">
                                <h1><?php echo $_smarty_tpl->tpl_vars['LABEL_TC6']->value;?>
</h1>
                            </div>

                            <div class="sectionContent">

                                <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['TRAJECTORIA']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
                                <article>
                                    <p><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
</br><?php echo $_smarty_tpl->tpl_vars['item']->value['year'];?>
</p>
                                </article>
                                <?php } ?>

                            </div>

                            <div class="clear"></div>
                        </section>
                        
                        <?php }?> 

                    </div>

                </div>
                <div class="separator"></div>
            </div>
        </div>
    </div>

    <?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

</body>

</html><?php }} ?>
