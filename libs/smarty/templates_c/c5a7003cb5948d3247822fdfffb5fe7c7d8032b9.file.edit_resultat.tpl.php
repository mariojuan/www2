<?php /* Smarty version Smarty-3.1.16, created on 2018-01-18 14:50:54
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/admin/templates/edit_resultat.tpl" */ ?>
<?php /*%%SmartyHeaderCode:13785563685a60a63ed78af4-47768826%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c5a7003cb5948d3247822fdfffb5fe7c7d8032b9' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/admin/templates/edit_resultat.tpl',
      1 => 1432536202,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13785563685a60a63ed78af4-47768826',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'result_update' => 0,
    'items_eleccions' => 0,
    'fila' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a60a63edd8b14_48984769',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a60a63edd8b14_48984769')) {function content_5a60a63edd8b14_48984769($_smarty_tpl) {?><section id="main" class="column">

<h4 class="alert_info"><i class="icon-pencil"></i>Edicció participacio i resultats</h4>
<?php if ($_smarty_tpl->tpl_vars['result_update']->value=="1") {?>
<div id="div_resultat">
    <h4 id="resultat_modificacio" class="alert_success">Modificacions realitzades</h4>
</div>
<?php }?>
<article class="module width_full">
    <div class="module_content">
        <form id="resultatForm" method="post" action="edit_resultat.php" novalidate="novalidate" >
            <br>
            <p>
                <label>Comici *</label>
                <select name="RefEleccio" id="RefEleccio">
                    <!--<option value="">-- Seleccionar un comici --</option>-->
                <?php  $_smarty_tpl->tpl_vars['fila'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['fila']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['items_eleccions']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['fila']->key => $_smarty_tpl->tpl_vars['fila']->value) {
$_smarty_tpl->tpl_vars['fila']->_loop = true;
?> 
                    <option value="<?php echo $_smarty_tpl->tpl_vars['fila']->value->IdEleccio;?>
"><?php echo $_smarty_tpl->tpl_vars['fila']->value->Denominacio;?>
</option>                   
                <?php } ?> 

                </select>
            </p>

            <p>
                <label>Col·legi *</label>
                <label id="colegis">
                    <select name="RefColegi" id="RefColegi">
                        <option value="">-- Selecciona un col·legi --</option>
                    </select>
                </label>
            </p>

            <p>
                <label>Mesa *</label>
                <label id="meses">
                    <select name="IdMesa" id="IdMesa">
                        <option value="">-- Selecciona un col·legi --</option>
                    </select>
                </label>
            </p>

            <p>
                &nbsp;
            </p>

            <p>
                <label>Districte</label>
                <input class="medio" type="text" name="Districte" id="Districte" readonly>
                <label class="center">Secció</label>
                <input class="medio" type="text" name="Seccio" id="Seccio" readonly>
                <label class="center">Lletra</label>
                <input class="corto" type="text" name="Lletra" id="Lletra" readonly>
            </p>

            <p>
                <label>Participació 14:00</label>
                <input class="medio" type="text" name="Vots1Participacio" id="Vots1Participacio">
                <label class="center">18:00</label>
                <input class="medio" type="text" name="Vots2Participacio" id="Vots2Participacio">
                <label class="center">20:00</label>
                <input class="medio" type="text" name="Vots3Participacio" id="Vots3Participacio">
            </p>

            <p>
                &nbsp;
            </p>

            <div id="resultats_partits" class="padding-right50">

            </div>

            <p>
                &nbsp;
            </p>

            <center>
                <p>
                    <input class="submit" type="submit" value="Acceptar" id="Acceptar" name="Acceptar">
                </p>

                <p>
                    <label>* Camps obligatoris</label>
                </p>
            </center>

        </form>
    </div>
</article>

<!-- end of styles article -->

<script>

    function resetFields() { 
        $('#Districte').val("");
        $('#Seccio').val("");
        $('#Lletra').val("");
        $('#Vots1Participacio').val("");
        $('#Vots2Participacio').val("");
        $('#Vots3Participacio').val("");
        $('#resultats_partits').html("");
    }

    function carregaColegis() { 
        $.ajax({ 
            url: 'carregaColegis.php',
            data: 'IdEleccio='+$('#RefEleccio option:selected').val(),                
            type: 'get',
            success: function(output) {
                $('#resultat_modificacio').css("display","none");

                $('#RefColegi').html(output);
                $('#IdMesa').html("<option value=\"\">-- Selecciona un col·legi --</option>");

                resetFields();
            }
        });
    }

    $().ready(function() {
        carregaColegis();

        $("#resultatForm").validate({
            rules: {
                IdMesa: "required"
            },
            messages: {
                IdMesa: "Atenció! Heu de seleccionar una mesa per editar-la."
            }
        });


        $('#RefEleccio').change(function() {
           carregaColegis();
        });    
            
        $('#RefColegi').change(function() {
            $.ajax({ 
                url: 'carregaMeses.php',
                data: 'IdEleccio='+$('#RefEleccio option:selected').val() + '&IdColegi='+$('#RefColegi option:selected').val(),
                type: 'get',
                success: function(output) {
                    $('#IdMesa').html(output);

                    resetFields();
                }
            });
        });        
        
        $('#IdMesa').change(function() {
            $.ajax({ 
                url: 'carregaCandidaturesResultatsMesa.php',
                data: 'IdEleccio='+$('#RefEleccio option:selected').val() + 
                     '&IdColegi=' + $('#RefColegi option:selected').val() + 
                     '&IdMesa='+$('#IdMesa option:selected').val(),
                type: 'get',
                success: function(output) {

                    if (output.indexOf('#')!=-1) {     

                        result = output.split('#');
                        $('#Districte').val(result[3]);
                        $('#Seccio').val(result[4]);
                        $('#Lletra').val(result[5]);
                        $('#Vots1Participacio').val(result[17]);
                        $('#Vots2Participacio').val(result[18]);
                        $('#Vots3Participacio').val(result[19]);

                        // 20 -> vots finals

                        $numCandidatures = result[21];
                        $posVector = 22;
                        $htmlCandidats = "<p><hr>";

                        for (x=1;x<=$numCandidatures;x++)  { 
                            $IdCandidatura = result[$posVector]; 
                            $posVector = $posVector + 1;
                            $Sigles = result[$posVector]; 
                            $posVector = $posVector + 1;
                            $Denominacio = result[$posVector]; 
                            $posVector = $posVector + 1;
                            $Vots = result[$posVector]; 
                            $posVector = $posVector + 1;                                                        

                            $htmlCandidats +=  "<label>" + $Denominacio +  "</label>" + 
                                                "<input class=\"medio_right\" type=\"text\" name=\"" + 
                                                 $IdCandidatura + "\" id=\""  + 
                                                 $IdCandidatura + "\" value=\"" + $Vots + "\"><br>"  + 
                                              "<hr style='clear:both'>";
                        }
                        $htmlCandidats += "</p>";

                        $('#resultats_partits').html($htmlCandidats);

                        $('input.medio_right').css('width','50px');                        
                        $('input.medio_right').css('float','right');                        
                    }
                }
            });
        });    

    });

</script>



<div class="spacer"></div>
</section>

<?php }} ?>
