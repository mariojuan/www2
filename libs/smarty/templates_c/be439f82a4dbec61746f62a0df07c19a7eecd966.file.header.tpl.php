<?php /* Smarty version Smarty-3.1.16, created on 2017-12-12 20:52:30
         compiled from "templates/header.tpl" */ ?>
<?php /*%%SmartyHeaderCode:10798330925a30337e25a878-33547008%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'be439f82a4dbec61746f62a0df07c19a7eecd966' => 
    array (
      0 => 'templates/header.tpl',
      1 => 1498114572,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '10798330925a30337e25a878-33547008',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'lang' => 0,
    'LABEL_MENUH1' => 0,
    'LABEL_MENUH2' => 0,
    'LABEL_MENUH4' => 0,
    'LABEL_MENUH3' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a30337e2c1313_77449557',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a30337e2c1313_77449557')) {function content_5a30337e2c1313_77449557($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("header_little.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>



<!-- BARRA SUBHEADER (MENU) -->
<div class="row row-header row-header-menu">

	<!-- TITOL -->
   <div class="titolcap">
      <h1>Tortosa</h1>
   </div>
   <!--/ TITOL -->

   <!-- BOTÓ HOME -->
   
   <div id="bhome">
      <?php if ($_smarty_tpl->tpl_vars['lang']->value=='es') {?>
      <a href="/es/">
      <?php } elseif ($_smarty_tpl->tpl_vars['lang']->value=='en') {?>
      <a href="/en/">
      <?php } else { ?>
      <a href="/">
         <?php }?>
         <div id="menuh_text"><i class='icon-home'></i></div>
      </a>
   </div>
   
   <!--/ BOTÓ HOME -->

   <!-- MENÚ -->

   <div class="row">
   		<p class="menu-responsive">
   			<a href="<?php if ($_smarty_tpl->tpl_vars['lang']->value=="ca") {?>/ciutat/<?php } elseif ($_smarty_tpl->tpl_vars['lang']->value=="es") {?> /<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
/ciudad/<?php } elseif ($_smarty_tpl->tpl_vars['lang']->value=="en") {?> /ciutat/index.php?lang=en<?php } elseif ($_smarty_tpl->tpl_vars['lang']->value=="fr") {?> /ciutat/index.php?lang=fr<?php } else { ?>/ciutat/index.php<?php }?>" hreflang="ca"><?php echo $_smarty_tpl->tpl_vars['LABEL_MENUH1']->value;?>

   			</a>
   		</p>
   		<p class="menu-responsive">
   			<a href="<?php if ($_smarty_tpl->tpl_vars['lang']->value=="ca") {?>/ajunta/index.php<?php } elseif ($_smarty_tpl->tpl_vars['lang']->value=="es") {?> /ajunta/index.php?lang=es<?php } elseif ($_smarty_tpl->tpl_vars['lang']->value=="en") {?> /ajunta/index.php?lang=en <?php } elseif ($_smarty_tpl->tpl_vars['lang']->value=="fr") {?>/ajunta/index.php?lang=fr<?php } else { ?>/ajunta/index.php<?php }?>"><?php echo $_smarty_tpl->tpl_vars['LABEL_MENUH2']->value;?>

   			</a>
   		</p>
   		<p class="menu-responsive">
   			<a href="<?php if ($_smarty_tpl->tpl_vars['lang']->value=="ca") {?>/negocis/index.php<?php } elseif ($_smarty_tpl->tpl_vars['lang']->value=="es") {?> /negocis/index.php?lang=es<?php } elseif ($_smarty_tpl->tpl_vars['lang']->value=="en") {?> /negocis/index.php?lang=en<?php } elseif ($_smarty_tpl->tpl_vars['lang']->value=="fr") {?>/negocis/index.php?lang=fr<?php } else { ?>/negocis/index.php<?php }?>" hreflang="ca"><?php echo $_smarty_tpl->tpl_vars['LABEL_MENUH4']->value;?>

   			</a>
   		</p>
   		<p class="menu-responsive">
   			<a href="<?php if ($_smarty_tpl->tpl_vars['lang']->value=="ca") {?>http://www.tortosaturisme.cat<?php } elseif ($_smarty_tpl->tpl_vars['lang']->value=="es") {?>http://www.tortosaturisme.cat/es/<?php } elseif ($_smarty_tpl->tpl_vars['lang']->value=="en") {?> http://www.tortosaturisme.cat/en/<?php } elseif ($_smarty_tpl->tpl_vars['lang']->value=="fr") {?> http://www.tortosaturisme.cat/fr/<?php } else { ?>http://www.tortosaturisme.cat<?php }?>" hreflang="ca" target="_blank"><?php echo $_smarty_tpl->tpl_vars['LABEL_MENUH3']->value;?>

   			</a>
   		</p>
   </div>

   <div id="menuh">
      <div id="bmenu">
         <a href="<?php if ($_smarty_tpl->tpl_vars['lang']->value=="ca") {?>/ciutat/<?php } elseif ($_smarty_tpl->tpl_vars['lang']->value=="es") {?> /<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
/ciudad/<?php } elseif ($_smarty_tpl->tpl_vars['lang']->value=="en") {?> /ciutat/index.php?lang=en<?php } elseif ($_smarty_tpl->tpl_vars['lang']->value=="fr") {?> /ciutat/index.php?lang=fr<?php } else { ?>/ciutat/index.php<?php }?>" hreflang="ca">
         <div id="menuh_text">
            <?php echo $_smarty_tpl->tpl_vars['LABEL_MENUH1']->value;?>

         </div>
         </a>
      </div>
      <div id="bmenu">
         <a href="<?php if ($_smarty_tpl->tpl_vars['lang']->value=="ca") {?>/ajunta/index.php<?php } elseif ($_smarty_tpl->tpl_vars['lang']->value=="es") {?> /ajunta/index.php?lang=es<?php } elseif ($_smarty_tpl->tpl_vars['lang']->value=="en") {?> /ajunta/index.php?lang=en <?php } elseif ($_smarty_tpl->tpl_vars['lang']->value=="fr") {?>/ajunta/index.php?lang=fr<?php } else { ?>/ajunta/index.php<?php }?>">
         <div id="menuh_text">
            <?php echo $_smarty_tpl->tpl_vars['LABEL_MENUH2']->value;?>

         </div>
         </a>
      </div>
      <div id="bmenu">
         <a href="<?php if ($_smarty_tpl->tpl_vars['lang']->value=="ca") {?>/negocis/index.php<?php } elseif ($_smarty_tpl->tpl_vars['lang']->value=="es") {?> /negocis/index.php?lang=es<?php } elseif ($_smarty_tpl->tpl_vars['lang']->value=="en") {?> /negocis/index.php?lang=en<?php } elseif ($_smarty_tpl->tpl_vars['lang']->value=="fr") {?>/negocis/index.php?lang=fr<?php } else { ?>/negocis/index.php<?php }?>" hreflang="ca">
         <div id="menuh_text">
            <?php echo $_smarty_tpl->tpl_vars['LABEL_MENUH4']->value;?>

         </div>
         </a>
      </div>
      <div id="bmenu">
         <a href="<?php if ($_smarty_tpl->tpl_vars['lang']->value=="ca") {?>http://www.tortosaturisme.cat<?php } elseif ($_smarty_tpl->tpl_vars['lang']->value=="es") {?>http://www.tortosaturisme.cat/es/<?php } elseif ($_smarty_tpl->tpl_vars['lang']->value=="en") {?> http://www.tortosaturisme.cat/en/<?php } elseif ($_smarty_tpl->tpl_vars['lang']->value=="fr") {?> http://www.tortosaturisme.cat/fr/<?php } else { ?>http://www.tortosaturisme.cat<?php }?>" hreflang="ca" target="_blank">
         <div id="menuh_text">
            <?php echo $_smarty_tpl->tpl_vars['LABEL_MENUH3']->value;?>

         </div>
         </a>
      </div>
   </div>
   
   <!--/ MENÚ -->

</div>
<!--/ BARRA SUBHEADER (MENU) --><?php }} ?>
