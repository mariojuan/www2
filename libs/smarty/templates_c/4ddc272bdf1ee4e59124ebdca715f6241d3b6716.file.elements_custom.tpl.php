<?php /* Smarty version Smarty-3.1.16, created on 2017-12-13 02:04:45
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/festes/elements/elements_custom.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12988030455a307cad200b11-18551379%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4ddc272bdf1ee4e59124ebdca715f6241d3b6716' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/festes/elements/elements_custom.tpl',
      1 => 1499073498,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12988030455a307cad200b11-18551379',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'image' => 0,
    'LABEL_TITOL1' => 0,
    'LABEL_TITOL1b' => 0,
    'MENU' => 0,
    'itemMenu' => 0,
    'item' => 0,
    'LABEL_TC0' => 0,
    'LABEL_TC1' => 0,
    'LABEL_TC2' => 0,
    'LABEL_TC3' => 0,
    'LABEL_TC4' => 0,
    'LABEL_TC5' => 0,
    'LABEL_TC6' => 0,
    'LABEL_TC7' => 0,
    'LABEL_TC8' => 0,
    'LABEL_TC9' => 0,
    'LABEL_TC10' => 0,
    'LABEL_TC11' => 0,
    'LABEL_TC12' => 0,
    'LABEL_TC13' => 0,
    'LABEL_TC14' => 0,
    'LABEL_TC15' => 0,
    'LABEL_TC16' => 0,
    'LABEL_TC17' => 0,
    'LABEL_TC18' => 0,
    'LABEL_TC19' => 0,
    'LABEL_TC20' => 0,
    'LABEL_TC21' => 0,
    'LABEL_TC22' => 0,
    'LABEL_TC23' => 0,
    'LABEL_TC24' => 0,
    'LABEL_TC25' => 0,
    'LABEL_TC26' => 0,
    'LABEL_TC27' => 0,
    'LABEL_TC28' => 0,
    'LABEL_TC29' => 0,
    'LABEL_TC30' => 0,
    'LABEL_TC31' => 0,
    'LABEL_TC32' => 0,
    'LABEL_TC33' => 0,
    'LABEL_TC34' => 0,
    'LABEL_TC35' => 0,
    'LABEL_TC36' => 0,
    'LABEL_TC37' => 0,
    'LABEL_TC38' => 0,
    'LABEL_TC39' => 0,
    'LABEL_TC40' => 0,
    'LABEL_TC41' => 0,
    'LABEL_TC42' => 0,
    'LABEL_TC43' => 0,
    'LABEL_TC44' => 0,
    'LABEL_TC45' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a307cad2f9306_92352806',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a307cad2f9306_92352806')) {function content_5a307cad2f9306_92352806($_smarty_tpl) {?><!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->

<div id="page-wrap">

<div class="contenedor-responsive">

    <div id="conttranstotalcos">

        <div id="conttranscos">

            <div id="btranspresen">

                <div id="transimatge">
                    <img src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
" class="img-slider-territori"/>
                </div>

                <div id="transimatgeText">
                    <h1>
                    <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

                    <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                    </h1>
                </div>

            </div>

            <div class="subtitolinfeco" id="subtitolinfeco2">
            <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1b']->value;?>

            </div>

            <div class="marcback">
                    <a href="index.php">
                    <i class="icon-angle-double-up"></i>
                    </a>
            </div>

            <ul id='menu_planA'>
            <?php  $_smarty_tpl->tpl_vars['itemMenu'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['itemMenu']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['MENU']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['itemMenu']->key => $_smarty_tpl->tpl_vars['itemMenu']->value) {
$_smarty_tpl->tpl_vars['itemMenu']->_loop = true;
?>
                <a href=<?php echo $_smarty_tpl->tpl_vars['itemMenu']->value['link'];?>
 target="_<?php echo $_smarty_tpl->tpl_vars['itemMenu']->value['target'];?>
">
                <li id='menu_planA_item'>
                    <?php echo $_smarty_tpl->tpl_vars['itemMenu']->value['name'];?>

                </li>
                </a>
            <?php } ?>
            </ul>

            <div id="tcentre_planA">
                <?php if ($_smarty_tpl->tpl_vars['item']->value==1) {?>
                    <h2><?php echo $_smarty_tpl->tpl_vars['LABEL_TC0']->value;?>
</h2>
                    <div class="marcterme">
                    <img src="/images/festes/elements.jpg" class="img-responsive">
                    </div>
                <?php } elseif ($_smarty_tpl->tpl_vars['item']->value==2) {?>
                    <h2><?php echo $_smarty_tpl->tpl_vars['LABEL_TC0']->value;?>
</h2>
                    <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
</strong></p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC3']->value;?>
</p>
                    <div class="marcterme" style="left:200px;">
                    <img src="/images/festes/cuca1.jpg">
                    </div>
                    <div class="separator"></div>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC4']->value;?>
</p>
                    <div class="separator"></div>
                    <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC5']->value;?>
</strong></p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC6']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC7']->value;?>
</p>
                    <div class="marcterme" style="left:200px;">
                    <img src="/images/festes/cuca2.jpg">
                    </div>
                    <div class="separator"></div>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC8']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC9']->value;?>
</p>
                    <div class="separator"></div>
                    <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC10']->value;?>
</strong></p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC11']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC12']->value;?>
</p>
                    <p align="center"><i><?php echo $_smarty_tpl->tpl_vars['LABEL_TC13']->value;?>
<br>
                    <?php echo $_smarty_tpl->tpl_vars['LABEL_TC14']->value;?>
<br>
                    <?php echo $_smarty_tpl->tpl_vars['LABEL_TC15']->value;?>
<br>
                    <?php echo $_smarty_tpl->tpl_vars['LABEL_TC16']->value;?>
</i></p>
                    <div class="marcterme rectificacio-marcTerme-img-ssanta2017" style="left:60px;">
                    <img src="/images/festes/cuca3.jpg" class="img-responsive">
                    </div>
                    <div class="separator"></div>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC17']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC18']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC19']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC20']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC21']->value;?>
</p>
                    <p align="center"><i><?php echo $_smarty_tpl->tpl_vars['LABEL_TC22']->value;?>
<br>
                    <?php echo $_smarty_tpl->tpl_vars['LABEL_TC23']->value;?>
<br>
                    <?php echo $_smarty_tpl->tpl_vars['LABEL_TC24']->value;?>
<br>
                    <?php echo $_smarty_tpl->tpl_vars['LABEL_TC25']->value;?>
</i></p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC26']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC27']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC28']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC29']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC30']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC31']->value;?>
</p>
                <?php } elseif ($_smarty_tpl->tpl_vars['item']->value==3) {?>
                    <h2><?php echo $_smarty_tpl->tpl_vars['LABEL_TC0']->value;?>
</h2>
                    <div class="separator"></div>
                    <div class="marcterme rectificacio-marcTerme-img-ssanta2017" style="left:10px;">
                    <img src="/images/festes/gegants.jpg" class="img-responsive">
                    </div>
                    <div class="separator"></div>
                    <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
</strong></p>
                    <p><strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
</strong></p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC3']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC4']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC5']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC6']->value;?>
</p>
                    <p><strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC7']->value;?>
</strong></p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC8']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC9']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC10']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC11']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC12']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC13']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC14']->value;?>
</p>
                    <div class="separator"></div>
                    <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC15']->value;?>
</strong></p>
                    <p><strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC16']->value;?>
</strong></p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC17']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC18']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC19']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC20']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC21']->value;?>
</p>
                    <p><strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC7']->value;?>
</strong></p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC22']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC23']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC24']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC25']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC26']->value;?>
</p>
                    <div class="separator"></div>
                    <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC27']->value;?>
</strong></p>
                    <p><strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC16']->value;?>
</strong></p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC28']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC29']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC30']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC31']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC32']->value;?>
</p>
                    <p><strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC7']->value;?>
</strong></p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC33']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC34']->value;?>
</p>
                    <div class="separator"></div>
                    <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC35']->value;?>
</strong></p>
                    <p><strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
</strong></p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC36']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC37']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC38']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC39']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC40']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC41']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC42']->value;?>
</p>
                    <p><strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC7']->value;?>
</strong></p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC43']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC44']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC45']->value;?>
</p>
                <?php } elseif ($_smarty_tpl->tpl_vars['item']->value==4) {?>
                    <h2><?php echo $_smarty_tpl->tpl_vars['LABEL_TC0']->value;?>
</h2>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
</p>
                    <div class="separator"></div>
                    <div class="marcterme rectificacio-marcTerme-img-ssanta2017" style="left:140px;">
                    <img src="/images/festes/aguila1.jpg" class="img-responsive">
                    </div>
                    <div class="separator"></div>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC3']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC4']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC5']->value;?>
</p>
                    <div class="separator"></div>
                    <div class="marcterme rectificacio-marcTerme-img-ssanta2017" style="left:160px;">
                    <img src="/images/festes/aguila2.jpg" class="img-responsive">
                    </div>
                    <div class="separator"></div>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC6']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC7']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC8']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC9']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC10']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC11']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC12']->value;?>
</p>
                    <div class="separator"></div>
                    <div class="marcterme rectificacio-marcTerme-img-ssanta2017" style="left:150px;">
                    <img src="/images/festes/aguila3.jpg" class="img-responsive">
                    </div>
                    <div class="separator"></div>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC13']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC14']->value;?>
</p>
                <?php }?>
            </div>

        </div>
    </div>
</div>
</div>


<?php }} ?>
