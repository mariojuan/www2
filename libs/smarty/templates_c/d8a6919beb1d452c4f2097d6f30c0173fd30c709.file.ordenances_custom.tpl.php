<?php /* Smarty version Smarty-3.1.16, created on 2017-12-14 08:21:15
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/ordenances/ordenances_custom.tpl" */ ?>
<?php /*%%SmartyHeaderCode:10348706515a32266bd68181-21085891%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd8a6919beb1d452c4f2097d6f30c0173fd30c709' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/ordenances/ordenances_custom.tpl',
      1 => 1498032050,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '10348706515a32266bd68181-21085891',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'image' => 0,
    'LABEL_TITOL_FISCAL' => 0,
    'MENU_FISCAL' => 0,
    'itemMenu' => 0,
    'lang' => 0,
    'tipus' => 0,
    'LABEL_TC2' => 0,
    'LABEL_TC3' => 0,
    'LABEL_TC4' => 0,
    'LABEL_TC5' => 0,
    'LABEL_TC1' => 0,
    'elements' => 0,
    'value' => 0,
    'page' => 0,
    'LABEL_TC7' => 0,
    'lastpage' => 0,
    'page_ant' => 0,
    'lblAnt' => 0,
    'desp' => 0,
    'firstpage' => 0,
    'counter' => 0,
    'page_seg' => 0,
    'lblSeg' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a32266c00a0c5_22014324',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a32266c00a0c5_22014324')) {function content_5a32266c00a0c5_22014324($_smarty_tpl) {?><!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->

<div id="page-wrap">
<div id="contordenantotalcos">
	<div id="conttranscos">
		<div id="btranspresen">
			<div id="transimatge">			
			<img src=<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
 />			
			</div>
			<div id="transimatgeText">
				<h1>
				<?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL_FISCAL']->value;?>

				</h1>
			</div>
		</div>
		<ul id='menu_planA'>
		<?php  $_smarty_tpl->tpl_vars['itemMenu'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['itemMenu']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['MENU_FISCAL']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['itemMenu']->key => $_smarty_tpl->tpl_vars['itemMenu']->value) {
$_smarty_tpl->tpl_vars['itemMenu']->_loop = true;
?>
			<a href=<?php echo $_smarty_tpl->tpl_vars['itemMenu']->value;?>
&lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
>
			<li id='menu_planA_item'>
				<?php echo $_smarty_tpl->tpl_vars['itemMenu']->key;?>

			</li>
			</a>
		<?php } ?>
		</ul>
		
		<div id="tcentre_planA">
			<div id="textintroinfeco" class="taulaicorpo">	
				    <?php if ($_smarty_tpl->tpl_vars['tipus']->value!=null) {?> 			    	
					    <?php if ($_smarty_tpl->tpl_vars['tipus']->value=='IM') {?> 
					    	<?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>

					    <?php } elseif ($_smarty_tpl->tpl_vars['tipus']->value=='PP') {?>
					    	<?php echo $_smarty_tpl->tpl_vars['LABEL_TC3']->value;?>

					    <?php } elseif ($_smarty_tpl->tpl_vars['tipus']->value=='TX') {?>
					    	<?php echo $_smarty_tpl->tpl_vars['LABEL_TC4']->value;?>

					    <?php } elseif ($_smarty_tpl->tpl_vars['tipus']->value=='TF') {?>
					    	<?php echo $_smarty_tpl->tpl_vars['LABEL_TC5']->value;?>

					    <?php } else { ?>
		                	<?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>

		                <?php }?>	
				    <?php } else { ?>
	                	<?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>

	                <?php }?>	
	                <br>
	                <br>
	                <br>

                    <div id="taulaicorpo">
	                <?php if (count($_smarty_tpl->tpl_vars['elements']->value)>0) {?>

		               <?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['value']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['elements']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value) {
$_smarty_tpl->tpl_vars['value']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['value']->key;
?>
		               <div id="filaicorpo">
			                <div class="colicorpo ord-col1">
			                    <a href="index.php?tipus=<?php echo $_smarty_tpl->tpl_vars['tipus']->value;?>
&lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
&accio=detall&codi=<?php echo utf8_encode($_smarty_tpl->tpl_vars['value']->value->Codi);?>
&page=<?php echo $_smarty_tpl->tpl_vars['page']->value;?>
" style="text-decoration:none; color:#000000">
			                    	<i class="icon-link"></i>
			                    </a>
		   		            </div>
		   		            <div class="colicorpo ord-col2">	
		    	                <a href="http://www.tortosa.cat/webajt/gestiointerna/ordenances/documents/<?php echo utf8_encode($_smarty_tpl->tpl_vars['value']->value->document);?>
" target="_blank"  style="text-decoration:none; color:#000000">
			                    	<i class="icon-file-pdf"></i>
				                </a>
		                    </div>
		                    <div class="colicorpo ord-col3">
			                    <a href="index.php?tipus=<?php echo $_smarty_tpl->tpl_vars['tipus']->value;?>
&lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
&accio=detall&codi=<?php echo utf8_encode($_smarty_tpl->tpl_vars['value']->value->Codi);?>
&page=<?php echo $_smarty_tpl->tpl_vars['page']->value;?>
" style="text-decoration:none; color:#000000">
			                        <?php echo utf8_encode($_smarty_tpl->tpl_vars['value']->value->Codi_ord1);?>
 - <?php echo utf8_encode($_smarty_tpl->tpl_vars['value']->value->codi_ord);?>

			                    </a>
		                  	</div>                        
		                    <div class="colicorpo ord-col4">
			                    <a href="index.php?tipus=<?php echo $_smarty_tpl->tpl_vars['tipus']->value;?>
&lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
&accio=detall&codi=<?php echo utf8_encode($_smarty_tpl->tpl_vars['value']->value->Codi);?>
&page=<?php echo $_smarty_tpl->tpl_vars['page']->value;?>
" style="text-decoration:none; color:#000000">
			                        <?php echo utf8_encode($_smarty_tpl->tpl_vars['value']->value->descripcio_curta);?>

			                    </a>
		                  	</div>                        
		                </div>
		                <?php } ?>
	                <?php } else { ?> 
	                  <div id="filaicorpo">
	                    <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7']->value;?>

	                  </div>    
	                <?php }?>
	            	</div>
	        </div>

	        <br>

	        <div id="pagination">
	            <ul>
	                <?php if ($_smarty_tpl->tpl_vars['lastpage']->value>1) {?>
	                    <?php if ($_smarty_tpl->tpl_vars['page']->value>1) {?>
	                        <?php $_smarty_tpl->tpl_vars["page_ant"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value-1, null, 0);?>
	                        <li><a href="index.php?tipus=<?php echo $_smarty_tpl->tpl_vars['tipus']->value;?>
&lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
&accio=list&page=<?php echo $_smarty_tpl->tpl_vars['page_ant']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['lblAnt']->value;?>
</a></li>
	                    <?php }?>
	                    <?php $_smarty_tpl->tpl_vars["desp"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value+4, null, 0);?>
	                    <?php $_smarty_tpl->tpl_vars["firstpage"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value-4, null, 0);?>

	                    <?php if ($_smarty_tpl->tpl_vars['desp']->value<$_smarty_tpl->tpl_vars['lastpage']->value) {?>
	                        <?php $_smarty_tpl->tpl_vars["lastpage"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value+4, null, 0);?>
	                    <?php }?>
	                    <?php if ($_smarty_tpl->tpl_vars['firstpage']->value<1) {?>
	                        <?php $_smarty_tpl->tpl_vars["firstpage"] = new Smarty_variable(1, null, 0);?>
	                    <?php }?>

	                    <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['counter']->step = 1;$_smarty_tpl->tpl_vars['counter']->total = (int) ceil(($_smarty_tpl->tpl_vars['counter']->step > 0 ? $_smarty_tpl->tpl_vars['lastpage']->value+1 - ($_smarty_tpl->tpl_vars['firstpage']->value) : $_smarty_tpl->tpl_vars['firstpage']->value-($_smarty_tpl->tpl_vars['lastpage']->value)+1)/abs($_smarty_tpl->tpl_vars['counter']->step));
if ($_smarty_tpl->tpl_vars['counter']->total > 0) {
for ($_smarty_tpl->tpl_vars['counter']->value = $_smarty_tpl->tpl_vars['firstpage']->value, $_smarty_tpl->tpl_vars['counter']->iteration = 1;$_smarty_tpl->tpl_vars['counter']->iteration <= $_smarty_tpl->tpl_vars['counter']->total;$_smarty_tpl->tpl_vars['counter']->value += $_smarty_tpl->tpl_vars['counter']->step, $_smarty_tpl->tpl_vars['counter']->iteration++) {
$_smarty_tpl->tpl_vars['counter']->first = $_smarty_tpl->tpl_vars['counter']->iteration == 1;$_smarty_tpl->tpl_vars['counter']->last = $_smarty_tpl->tpl_vars['counter']->iteration == $_smarty_tpl->tpl_vars['counter']->total;?>
	                        <?php if ($_smarty_tpl->tpl_vars['counter']->value==$_smarty_tpl->tpl_vars['page']->value) {?>
	                            <li><?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
</li>
	                        <?php } else { ?>
	                            <li><a href="index.php?tipus=<?php echo $_smarty_tpl->tpl_vars['tipus']->value;?>
&lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
&accio=list&page=<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
</a></li>
	                        <?php }?>
	                    <?php }} ?>
	                    <?php if ($_smarty_tpl->tpl_vars['page']->value<$_smarty_tpl->tpl_vars['lastpage']->value) {?>
	                        <?php $_smarty_tpl->tpl_vars["page_seg"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value+1, null, 0);?>
	                        <li><a href="index.php?tipus=<?php echo $_smarty_tpl->tpl_vars['tipus']->value;?>
&lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
&accio=list&page=<?php echo $_smarty_tpl->tpl_vars['page_seg']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['lblSeg']->value;?>
</a></li>
	                    <?php }?>
	                <?php } else { ?>
	                  <li>1</li>	
	                <?php }?>
	            </ul>
	        </div>

		</div>
  	</div>
</div>
</div>


<?php }} ?>
