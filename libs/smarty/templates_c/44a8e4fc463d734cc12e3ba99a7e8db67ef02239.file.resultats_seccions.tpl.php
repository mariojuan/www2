<?php /* Smarty version Smarty-3.1.16, created on 2017-12-13 02:59:04
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/eleccions/resultats_seccions.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12969828095a30896858de56-18053554%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '44a8e4fc463d734cc12e3ba99a7e8db67ef02239' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/eleccions/resultats_seccions.tpl',
      1 => 1490861671,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12969828095a30896858de56-18053554',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'LABEL_TITOL' => 0,
    'logo_eleccions' => 0,
    'title' => 0,
    'subtitle' => 0,
    'data_seccions' => 0,
    'item_seccio' => 0,
    'num_seccio' => 0,
    'districte_seccio' => 0,
    'taula1' => 0,
    'titols' => 0,
    'titol' => 0,
    'valors' => 0,
    'valor' => 0,
    'chart1' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a308968668a26_60508645',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a308968668a26_60508645')) {function content_5a308968668a26_60508645($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("templates/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<div id="page-wrap" class="negocis eleccions">
    <div id="contajttotalcos">
        <div id="conttranscos">
            <div id="bciutatpresen">
                <div id="ciutatimatge">
                    <img src="/images/negocis/cap.jpg" />
                </div>
                <div id="negocisimatgeText" class="imatgeCapText">
                    <h1>
                        <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL']->value;?>

                        &nbsp;&nbsp;
                    </h1>
                </div>
            </div>

            <?php echo $_smarty_tpl->getSubTemplate ("templates/eleccions/menu_eleccions.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            <div id="tcentre_planA">
                <!--
                <div id="title">
                    <input type="button" name="imprimir" value="Imprimir" id="printer" class="print" onclick="PrintElem('#tcentre_planA', 'participacio_districtes')">
                </div>
                <div id="logos">
                    <div>
                        <?php if ($_smarty_tpl->tpl_vars['logo_eleccions']->value!='') {?>
                            <img src="templates/eleccions/images/<?php echo $_smarty_tpl->tpl_vars['logo_eleccions']->value;?>
">
                        <?php } else { ?>
                            <?php echo $_smarty_tpl->tpl_vars['title']->value;?>

                        <?php }?>
                    </div>
                    <div>
                        <img src="templates/eleccions/images/logo-ajuntament.jpg">
                    </div>
                </div>
                -->
                <div id="section_title">
                    <?php echo $_smarty_tpl->tpl_vars['title']->value;?>

                    <?php if ($_smarty_tpl->tpl_vars['subtitle']->value) {?>
                        <br/><span id="text_dades_informatives"><?php echo $_smarty_tpl->tpl_vars['subtitle']->value;?>
</span>
                    <?php }?>
                </div>
                <div id="selector">
                    <select name="seccions" id="seccions" onchange="JavaScript:window.location.href='eleccions.php?accio=resultats_seccions&seccions='+this.options[this.selectedIndex].value;">
                        <option value="">-- Selecionar --</option>
                        <?php  $_smarty_tpl->tpl_vars['item_seccio'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item_seccio']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['data_seccions']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item_seccio']->key => $_smarty_tpl->tpl_vars['item_seccio']->value) {
$_smarty_tpl->tpl_vars['item_seccio']->_loop = true;
?>
                            <?php $_smarty_tpl->tpl_vars["districte_seccio"] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['item_seccio']->value->Districte)."|".((string)$_smarty_tpl->tpl_vars['item_seccio']->value->Seccio), null, 0);?>
                            <?php if ($_smarty_tpl->tpl_vars['num_seccio']->value==$_smarty_tpl->tpl_vars['districte_seccio']->value) {?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['item_seccio']->value->Districte;?>
|<?php echo $_smarty_tpl->tpl_vars['item_seccio']->value->Seccio;?>
" selected>Districte <?php echo $_smarty_tpl->tpl_vars['item_seccio']->value->Districte;?>
 - Seccio <?php echo $_smarty_tpl->tpl_vars['item_seccio']->value->Seccio;?>
</option>
                            <?php } else { ?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['item_seccio']->value->Districte;?>
|<?php echo $_smarty_tpl->tpl_vars['item_seccio']->value->Seccio;?>
">Districte <?php echo $_smarty_tpl->tpl_vars['item_seccio']->value->Districte;?>
 - Seccio <?php echo $_smarty_tpl->tpl_vars['item_seccio']->value->Seccio;?>
</option>
                            <?php }?>
                        <?php } ?>
                    </select>
                </div>
                <?php if (isset($_smarty_tpl->tpl_vars['taula1']->value)) {?>
                <div>
                    <table class="data_table">
                        <tr>
                            <?php $_smarty_tpl->tpl_vars['titols'] = new Smarty_variable($_smarty_tpl->tpl_vars['taula1']->value['0'], null, 0);?>
                            <?php  $_smarty_tpl->tpl_vars['titol'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['titol']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['titols']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['titol']->key => $_smarty_tpl->tpl_vars['titol']->value) {
$_smarty_tpl->tpl_vars['titol']->_loop = true;
?>
                            <th colspan="3"><?php echo $_smarty_tpl->tpl_vars['titol']->value;?>
</th>
                            <?php } ?>
                        </tr>
                        <?php  $_smarty_tpl->tpl_vars['valors'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['valors']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['taula1']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['valors']->key => $_smarty_tpl->tpl_vars['valors']->value) {
$_smarty_tpl->tpl_vars['valors']->_loop = true;
?>
                            <tr>
                            <?php if ($_smarty_tpl->tpl_vars['valors']->key>0) {?>
                                <?php  $_smarty_tpl->tpl_vars['valor'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['valor']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['valors']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['valor']->key => $_smarty_tpl->tpl_vars['valor']->value) {
$_smarty_tpl->tpl_vars['valor']->_loop = true;
?>
                                    <?php if ($_smarty_tpl->tpl_vars['valor']->value=="Partit"||$_smarty_tpl->tpl_vars['valor']->value=="Vots"||$_smarty_tpl->tpl_vars['valor']->value=="% s/ secció") {?>
                                        <td class="td_color"><?php echo $_smarty_tpl->tpl_vars['valor']->value;?>
</td>
                                    <?php } else { ?>
                                        <td><?php echo $_smarty_tpl->tpl_vars['valor']->value;?>
</td>
                                    <?php }?>
                                <?php } ?>
                            <?php }?>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
                <?php }?>

                <?php echo $_smarty_tpl->tpl_vars['chart1']->value;?>

                <div id="chart-1" class="chart">

                </div>
            </div>
        </div>
	</div>
	<?php echo $_smarty_tpl->getSubTemplate ("templates/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

</div><?php }} ?>
