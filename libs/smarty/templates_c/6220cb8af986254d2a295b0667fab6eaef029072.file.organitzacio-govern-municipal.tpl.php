<?php /* Smarty version Smarty-3.1.16, created on 2018-02-15 13:15:32
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/organitzacio-govern-municipal/organitzacio-govern-municipal.tpl" */ ?>
<?php /*%%SmartyHeaderCode:5617756685a2ff15fa3df59-83355658%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6220cb8af986254d2a295b0667fab6eaef029072' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/organitzacio-govern-municipal/organitzacio-govern-municipal.tpl',
      1 => 1518696928,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5617756685a2ff15fa3df59-83355658',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a2ff15fad02c4_59215678',
  'variables' => 
  array (
    'image' => 0,
    'LABEL_TITOL1' => 0,
    'LABEL_TC0' => 0,
    'LABEL_TC1' => 0,
    'LABEL_TC2' => 0,
    'LINK_DELEGACIONS' => 0,
    'item' => 0,
    'LABEL_TC3' => 0,
    'LABEL_TC4' => 0,
    'LABEL_TC5' => 0,
    'LABEL_TC51' => 0,
    'LABEL_TC6' => 0,
    'LABEL_TC7' => 0,
    'LINK_POBLES' => 0,
    'LABEL_TC8' => 0,
    'LABEL_TC9' => 0,
    'ARRAY_BARRIS' => 0,
    'LABEL_TC10' => 0,
    'LINK_BARRIS' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a2ff15fad02c4_59215678')) {function content_5a2ff15fad02c4_59215678($_smarty_tpl) {?><html>
   <head>
      <?php echo $_smarty_tpl->getSubTemplate ("head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

   </head>
   <body>
      <?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

      <div id="page-wrap">

      <div class="contenedor-responsive">

         <div id="conttranstotalcos">
            <div id="conttranscos">
               <div id="btranspresen">

                  <div id="transimatge">
                     <img src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
" class="img-slider-territori"/>
                  </div>

                  <div id="transimatgeText">
                     <h1>
                        <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

                     </h1>
                  </div>

               </div>

               <div id="tcentre_municipi">

                  <font size="4" color="#666666"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC0']->value;?>
</font>
                  <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
</p>
                  <h3><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
</h3>

                  <img src="http://www.tortosa.cat/webajt/ajunta/om/estructura10.jpg" class="img-responsive"/>

                  <div class="separator"></div>

                  <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['LINK_DELEGACIONS']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
                  <p><a href="<?php echo $_smarty_tpl->tpl_vars['item']->value['link'];?>
" target="<?php echo $_smarty_tpl->tpl_vars['item']->value['target'];?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
</a></p>
                  <?php } ?>

                  <div class="separator"></div>
                  <h3><?php echo $_smarty_tpl->tpl_vars['LABEL_TC3']->value;?>
</h3>
                  <p><b><?php echo $_smarty_tpl->tpl_vars['LABEL_TC4']->value;?>
</b></p>

                  <div class="ogm-row">
                     <div class="ogm-col-left">

                        <img src="http://www.tortosa.cat/webajt/ajunta/om/mappobles.jpg" class="marcterme img-pobles-barris"/>
                        
                     </div>
                     <!-- <div class="separator"></div> -->
                     <div class="ogm-col-right">

                     <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC5']->value;?>
:</p>
                        <ul>
                            <li><p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC51']->value;?>
</p></li>
                            <li><p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC6']->value;?>
</p></li>
                        </ul>
                     <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC7']->value;?>
</p>

                        <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['LINK_POBLES']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['item']->value['link'];?>
" target="<?php echo $_smarty_tpl->tpl_vars['item']->value['target'];?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
</a>
                        <?php } ?>

                     </div>
                  </div>

                  <div class="separator"></div>

                  <p><b><?php echo $_smarty_tpl->tpl_vars['LABEL_TC8']->value;?>
</b></p>

                  <div class="ogm-row">
                     <div class="ogm-col-left">

                     <img src="http://www.tortosa.cat/webajt/ajunta/om/mapbarris.jpg" class="marcterme img-pobles-barris"/>

                     </div>
                     <div class="ogm-col-right">

                        <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC9']->value;?>
</p>
                        <ul>
                           <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['ARRAY_BARRIS']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
                           <li><b><?php echo $_smarty_tpl->tpl_vars['item']->value['barri'];?>
: </b> <?php echo $_smarty_tpl->tpl_vars['item']->value['persona'];?>
</li>
                           <?php } ?>
                        </ul>
                        <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC10']->value;?>
</p>
                        <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['LINK_BARRIS']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
                        <p><a href="<?php echo $_smarty_tpl->tpl_vars['item']->value['link'];?>
" target="<?php echo $_smarty_tpl->tpl_vars['item']->value['target'];?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
</a></p>
                        <?php } ?>

                     </div>
                  </div>

               </div>
               <div class="separator"></div>
            </div>
         </div>
         </div>
      </div>
      <?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

   </body>
</html><?php }} ?>
