<?php /* Smarty version Smarty-3.1.16, created on 2018-03-02 11:52:34
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/noticies/noticia.tpl" */ ?>
<?php /*%%SmartyHeaderCode:6673241505a2fe808567b93-90955086%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'acbf2ae299bc27c80c738e2e13406bc39aabfc19' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/noticies/noticia.tpl',
      1 => 1519987952,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6673241505a2fe808567b93-90955086',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a2fe808702ab8_37376064',
  'variables' => 
  array (
    'LABEL_TITOL1' => 0,
    'LABEL_TC1' => 0,
    'date_publish' => 0,
    'TITOL' => 0,
    'SUBTITOL' => 0,
    'COS' => 0,
    'IMG_NAME_ON_DB' => 0,
    'IMAGE' => 0,
    'LABEL_TITOL3' => 0,
    'LABEL_TC_TIPOLOGIA' => 0,
    'tipologia_item' => 0,
    'LABEL_TITOL2' => 0,
    'LABEL_TC_TEMATICA' => 0,
    'tematica_item' => 0,
    'LINK' => 0,
    'LABEL_TITOL4' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a2fe808702ab8_37376064')) {function content_5a2fe808702ab8_37376064($_smarty_tpl) {?><html>
    <head>
        <?php echo $_smarty_tpl->getSubTemplate ("head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


        <!-- PrettyPhoto -->

        <link rel="stylesheet" href="/css/prettyPhoto.css">
        <script type="text/javascript" src="/js/imagesloaded.pkgd.min.js"></script>
        <script type="text/javascript" src="/js/jquery.prettyPhoto.js"></script>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function(){
                $("a[rel^='prettyPhoto']").prettyPhoto({
                    allow_resize
                });
        </script>
    </head>
	<body>
		<?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

        <div id="page-wrap">
            <div id="conttranstotalcos">
                <div id="conttranscos">
                    <div id="btranspresen">
                        <div id="transimatge">
                            <img src=/images/butlletins/tira.jpg />
                        </div>
                    </div>
                    <div id="transimatgeText">
                        <h1>
                            <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

                        </h1>
                    </div>
                    <div id='caixadatanoticia'>
                        <?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['date_publish']->value;?>

                    </div>
                    <div class='caixatextnoticia'>
                        <div id='noticia_titol'><?php echo $_smarty_tpl->tpl_vars['TITOL']->value;?>
</div>
                        <div id='noticia_subtitol'><?php echo $_smarty_tpl->tpl_vars['SUBTITOL']->value;?>
</div>
                        <div id='noticia_cos'><?php echo $_smarty_tpl->tpl_vars['COS']->value;?>
</div>
                    </div>

                    <div id='noticia_foto'>

                        <a href="http://www2.tortosa.cat/images/headlines/res_<?php echo $_smarty_tpl->tpl_vars['IMG_NAME_ON_DB']->value;?>
" rel="prettyPhoto"><?php echo $_smarty_tpl->tpl_vars['IMAGE']->value;?>
</a>

                    </div>

                    <div id='sidebar'>
                        <h3><?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL3']->value;?>
</h3>
                        <ul>
                            <?php  $_smarty_tpl->tpl_vars['tipologia_item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['tipologia_item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['LABEL_TC_TIPOLOGIA']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['tipologia_item']->key => $_smarty_tpl->tpl_vars['tipologia_item']->value) {
$_smarty_tpl->tpl_vars['tipologia_item']->_loop = true;
?>
                                <li><?php echo $_smarty_tpl->tpl_vars['tipologia_item']->value;?>
</li>
                            <?php } ?>
                        </ul>
                        <h3><?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL2']->value;?>
</h3>
                        <ul>
                        <?php  $_smarty_tpl->tpl_vars['tematica_item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['tematica_item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['LABEL_TC_TEMATICA']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['tematica_item']->key => $_smarty_tpl->tpl_vars['tematica_item']->value) {
$_smarty_tpl->tpl_vars['tematica_item']->_loop = true;
?>
                            <li><?php echo $_smarty_tpl->tpl_vars['tematica_item']->value;?>
</li>
                        <?php } ?>
                        </ul>
                        <?php if ($_smarty_tpl->tpl_vars['LINK']->value!='') {?>
                        <div>
                            <h3><?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL4']->value;?>
</h3>
                            <p><a href="<?php echo $_smarty_tpl->tpl_vars['LINK']->value;?>
" target="_blank"><i class="icon-link"></i></a></p>
                        </div>
                        <?php }?>
                    </div>

                    <div id='noticia_espaipeu'></div>
                </div>

            </div>
        </div>
		<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


        <script type="text/javascript" charset="utf-8">
            $(document).ready(function(){
                $("a[rel^='prettyPhoto']").prettyPhoto();
            });
        </script>

	</body>
</html><?php }} ?>
