<?php /* Smarty version Smarty-3.1.16, created on 2018-01-01 03:24:08
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/eleccions/participacio_comparativa.tpl" */ ?>
<?php /*%%SmartyHeaderCode:8688561575a499bc88efe01-60274361%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b81e16464d9bf60656558cd24ffafa6d9fa1dec6' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/eleccions/participacio_comparativa.tpl',
      1 => 1499332349,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8688561575a499bc88efe01-60274361',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'LABEL_TITOL' => 0,
    'logo_eleccions' => 0,
    'title' => 0,
    'apartat' => 0,
    'lblText01' => 0,
    'eleccions' => 0,
    'value' => 0,
    'idelecciocomparativa' => 0,
    'subtitle' => 0,
    'taula1' => 0,
    'titols' => 0,
    'titol' => 0,
    'valors' => 0,
    'valor' => 0,
    'taula2' => 0,
    'CaptionChart1' => 0,
    'SubCaptionChart1' => 0,
    'chart1' => 0,
    'CaptionChart2' => 0,
    'SubCaptionChart2' => 0,
    'chart2' => 0,
    'CaptionChart3' => 0,
    'SubCaptionChart3' => 0,
    'chart3' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a499bc8ab0e56_53143806',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a499bc8ab0e56_53143806')) {function content_5a499bc8ab0e56_53143806($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("templates/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<div id="page-wrap" class="negocis eleccions">

    <div id="contajttotalcos">
        <div id="conttranscos">
            <div id="bciutatpresen">
                <div id="ciutatimatge">
                    <img src="/images/negocis/cap.jpg" />
                </div>
                <div id="negocisimatgeText" class="imatgeCapText">
                    <h1>
                        <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL']->value;?>

                        &nbsp;&nbsp;
                    </h1>
                </div>
            </div>

            <?php echo $_smarty_tpl->getSubTemplate ("templates/eleccions/menu_eleccions.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            <div id="tcentre_planA">
                <!--
                <div id="title">
                    <input type="button" name="imprimir" value="Imprimir" id="printer" class="print" onclick="PrintElem('#tcentre_planA', 'participacio_comparativa')">
                </div>
                <div id="logos">
                    <div>
                        <?php if ($_smarty_tpl->tpl_vars['logo_eleccions']->value!='') {?>
                            <img src="templates/eleccions/images/<?php echo $_smarty_tpl->tpl_vars['logo_eleccions']->value;?>
">
                        <?php } else { ?>
                            <?php echo $_smarty_tpl->tpl_vars['title']->value;?>

                        <?php }?>
                    </div>
                    <div>
                        <img src="templates/eleccions/images/logo-ajuntament.jpg">
                    </div>
                </div>
                -->
                <div id="section_title">
                    <?php if ($_smarty_tpl->tpl_vars['apartat']->value=="eleccions") {?>
                        <div>
                        <?php echo $_smarty_tpl->tpl_vars['lblText01']->value;?>
:
                        <select name="eleccions_a_comparar" id="eleccions_a_comparar">
                            <?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['value']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['eleccions']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value) {
$_smarty_tpl->tpl_vars['value']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['value']->key;
?>
                                <?php if ($_smarty_tpl->tpl_vars['value']->value->IdEleccio==$_smarty_tpl->tpl_vars['idelecciocomparativa']->value) {?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['value']->value->IdEleccio;?>
" selected><?php echo $_smarty_tpl->tpl_vars['value']->value->Denominacio;?>
</option>
                                <?php } else { ?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['value']->value->IdEleccio;?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value->Denominacio;?>
</option>
                                <?php }?>
                            <?php } ?>
                        </select>
                        </div>
                    <?php }?>
                    <?php echo $_smarty_tpl->tpl_vars['title']->value;?>

                    <?php if ($_smarty_tpl->tpl_vars['subtitle']->value) {?>
                        <br/><span id="text_dades_informatives"><?php echo $_smarty_tpl->tpl_vars['subtitle']->value;?>
</span>
                    <?php }?>
                </div>
                <?php if (isset($_smarty_tpl->tpl_vars['taula1']->value)) {?>
                <div>
                    <table class="data_table">
                        <tr>
                            <?php $_smarty_tpl->tpl_vars['titols'] = new Smarty_variable($_smarty_tpl->tpl_vars['taula1']->value['0'], null, 0);?>
                            <?php  $_smarty_tpl->tpl_vars['titol'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['titol']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['titols']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['titol']->key => $_smarty_tpl->tpl_vars['titol']->value) {
$_smarty_tpl->tpl_vars['titol']->_loop = true;
?>
                            <th><?php echo $_smarty_tpl->tpl_vars['titol']->value;?>
</th>
                            <?php } ?>
                        </tr>
                        <?php  $_smarty_tpl->tpl_vars['valors'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['valors']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['taula1']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['valors']->key => $_smarty_tpl->tpl_vars['valors']->value) {
$_smarty_tpl->tpl_vars['valors']->_loop = true;
?>
                            <tr>
                            <?php if ($_smarty_tpl->tpl_vars['valors']->key>0) {?>
                                <?php  $_smarty_tpl->tpl_vars['valor'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['valor']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['valors']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['valor']->key => $_smarty_tpl->tpl_vars['valor']->value) {
$_smarty_tpl->tpl_vars['valor']->_loop = true;
?>
                                    <td><?php echo $_smarty_tpl->tpl_vars['valor']->value;?>
</td>
                                <?php } ?>
                            <?php }?>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
                <?php }?>

                <?php if (isset($_smarty_tpl->tpl_vars['taula2']->value)) {?>
                <div>
                    <table class="data_table">
                        <tr>
                            <?php $_smarty_tpl->tpl_vars['titols'] = new Smarty_variable($_smarty_tpl->tpl_vars['taula2']->value['0'], null, 0);?>
                            <?php  $_smarty_tpl->tpl_vars['titol'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['titol']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['titols']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['titol']->key => $_smarty_tpl->tpl_vars['titol']->value) {
$_smarty_tpl->tpl_vars['titol']->_loop = true;
?>
                                <?php if ($_smarty_tpl->tpl_vars['titol']->key==0) {?>
                                    <th><?php echo $_smarty_tpl->tpl_vars['titol']->value;?>
</th>
                                <?php } else { ?>
                                    <th colspan="2"><?php echo $_smarty_tpl->tpl_vars['titol']->value;?>
</th>
                                <?php }?>
                            <?php } ?>
                        </tr>
                        <?php  $_smarty_tpl->tpl_vars['valors'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['valors']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['taula2']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['valors']->key => $_smarty_tpl->tpl_vars['valors']->value) {
$_smarty_tpl->tpl_vars['valors']->_loop = true;
?>
                            <tr>
                            <?php if ($_smarty_tpl->tpl_vars['valors']->key>0) {?>
                                <?php  $_smarty_tpl->tpl_vars['valor'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['valor']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['valors']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['valor']->key => $_smarty_tpl->tpl_vars['valor']->value) {
$_smarty_tpl->tpl_vars['valor']->_loop = true;
?>
                                    <td><?php echo $_smarty_tpl->tpl_vars['valor']->value;?>
</td>
                                <?php } ?>
                            <?php }?>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
                <?php }?>

                <div id="chart-1" class="chart">
                    <div class="caption"><?php echo $_smarty_tpl->tpl_vars['CaptionChart1']->value;?>
</div>
                    <div class="subcaption"><?php echo $_smarty_tpl->tpl_vars['SubCaptionChart1']->value;?>
</div>
                    <?php echo $_smarty_tpl->tpl_vars['chart1']->value;?>

                </div>

                <div id="chart-2" class="chart">
                    <div class="caption"><?php echo $_smarty_tpl->tpl_vars['CaptionChart2']->value;?>
</div>
                    <div class="subcaption"><?php echo $_smarty_tpl->tpl_vars['SubCaptionChart2']->value;?>
</div>
                    <?php echo $_smarty_tpl->tpl_vars['chart2']->value;?>

                </div>

                <div id="chart-3" class="chart">
                    <div class="caption"><?php echo $_smarty_tpl->tpl_vars['CaptionChart3']->value;?>
</div>
                    <div class="subcaption"><?php echo $_smarty_tpl->tpl_vars['SubCaptionChart3']->value;?>
</div>
                    <?php echo $_smarty_tpl->tpl_vars['chart3']->value;?>

                </div>
            </div>
        </div>
	</div>
	<?php echo $_smarty_tpl->getSubTemplate ("templates/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

</div><?php }} ?>
