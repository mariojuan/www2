<?php /* Smarty version Smarty-3.1.16, created on 2018-02-01 16:17:14
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/admin/templates/menu.tpl" */ ?>
<?php /*%%SmartyHeaderCode:8874227465a30d589726458-33679028%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'cd009b8dfd718269fe0926bb9e0c134759de4e44' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/admin/templates/menu.tpl',
      1 => 1517498225,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8874227465a30d589726458-33679028',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a30d5897cd994_72484916',
  'variables' => 
  array (
    'info_usuari' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a30d5897cd994_72484916')) {function content_5a30d5897cd994_72484916($_smarty_tpl) {?><aside id="sidebar" class="column">
    <!--
    <form class="quick_search">
        <input type="text" value="Quick Search">
    </form>
    <hr/>
    -->
    <!--
    1.- Superadministrador
    2.- Unitat de Comunicació i Màrketing (CMK)
    3.- Grups Polítics (GP)
    -->

    <?php if (($_smarty_tpl->tpl_vars['info_usuari']->value['rol']=='Superadministrador')) {?>
        <h3>Ofertes d'Ocupació</h3>
        <ul class="toggle">
            <li class="icn_new_article"><i class="icon-doc-new"></i>
                <a href="/admin/ofertes_ocupacio/oferta.php?accio=edit_oferta">
                    Nova oferta
                </a>
            </li>
            <li class="icn_edit_article"><i class="icon-pencil"></i>
                <a href="/admin/ofertes_ocupacio/oferta.php">Gestió ofertes</a>
            </li>
        </ul>
    <?php }?>

    <?php if (($_smarty_tpl->tpl_vars['info_usuari']->value['rol']=='Superadministrador')) {?>
        <h3>Indicadors Transparència</h3>
        <ul class="toggle">
            <li class="icn_new_article"><i class="icon-doc-new"></i>
                <a href="/admin/indicadors_transparencia/indicadors_grup.php?accio=edit_indicador_grup">
                    Nou grup
                </a>
            </li>
            <li class="icn_edit_article"><i class="icon-pencil"></i>
                <a href="/admin/indicadors_transparencia/indicadors_grup.php">Gestió grups</a>
            </li>
            <li class="icn_new_article"><i class="icon-doc-new"></i>
                <a href="/admin/indicadors_transparencia/indicadors_subgrup.php?accio=edit_indicador_subgrup">
                    Nou subgrup
                </a>
            </li>
            <li class="icn_edit_article"><i class="icon-pencil"></i>
                <a href="/admin/indicadors_transparencia/indicadors_subgrup.php">Gestió subgrups</a>
            </li>
            <li class="icn_new_article"><i class="icon-doc-new"></i>
                <a href="/admin/indicadors_transparencia/indicadors.php?accio=edit_indicador">
                    Nou indicador
                </a>
            </li>
            <li class="icn_edit_article"><i class="icon-pencil"></i>
                <a href="/admin/indicadors_transparencia/indicadors.php">Gestió indicadors</a>
            </li>
        </ul>
    <?php }?>
    
    <?php if (($_smarty_tpl->tpl_vars['info_usuari']->value['rol']=='Superadministrador')||($_smarty_tpl->tpl_vars['info_usuari']->value['unitat']=="CMK")||($_smarty_tpl->tpl_vars['info_usuari']->value['unitat']=="GP")) {?>
    <h3>Notícies</h3>
    <ul class="toggle">
        <li class="icn_new_article"><i class="icon-doc-new"></i><a href="/admin/headlines.php?accio=edit_headlines">Nova notícia</a></li>
        <li class="icn_edit_article"><i class="icon-pencil"></i><a href="/admin/headlines.php">Gestió notícies</a></li>
    </ul>
    <?php }?>

    <?php if (($_smarty_tpl->tpl_vars['info_usuari']->value['rol']=='Superadministrador')) {?>
    <h3>Banners</h3>
    <ul class="toggle">
        <li class="icn_new_article"><i class="icon-doc-new"></i>
            <a href="/admin/banners.php?accio=edit_banners">
                Nou banner
            </a>
        </li>
        <li class="icn_edit_article"><i class="icon-pencil"></i>
            <a href="/admin/banners.php">Gestió banners</a>
        </li>
    </ul>
    <?php }?>

    <?php if (($_smarty_tpl->tpl_vars['info_usuari']->value['rol']=='Superadministrador')) {?>
    <h3>Telèfons</h3>
    <ul class="toggle">
        <li class="icn_new_article"><i class="icon-doc-new"></i>
            <a href="/admin/telefons.php?accio=edit_telefon">
                Nou telèfon
            </a>
        </li>
        <li class="icn_edit_article"><i class="icon-pencil"></i>
            <a href="/admin/telefons.php">Gestió telèfons</a>
        </li>
    </ul>
    <?php }?>

    <?php if (($_smarty_tpl->tpl_vars['info_usuari']->value['rol']=='Superadministrador')||($_smarty_tpl->tpl_vars['info_usuari']->value['unitat']=='INF')) {?>
    <h3>Eleccions</h3>
    <ul class="toggle">
        <li class="icn_edit_article"><i class="icon-pencil"></i><a href="/admin/edit_participacio.php">Edició mesa</a>
        </li>        
        <li class="icn_edit_article"><i class="icon-pencil"></i><a href="/admin/edit_resultat.php">Edició participació i resultats</a>
        </li>        
        <li class="icn_edit_article"><i class="icon-pencil"></i>&nbsp;&nbsp;&nbsp;Històric
            <ul>
                <li class="icn_edit_article"><i class="icon-pencil"></i><a href="/admin/edit_participacio_historic.php">Edició mesa</a>
                </li>        
                <li class="icn_edit_article"><i class="icon-pencil"></i><a href="/admin/edit_resultat_historic.php">Edició participació i resultats</a>
                </li>        
            </ul>
        </li>        
    </ul>
    <?php }?>
    <?php if (($_smarty_tpl->tpl_vars['info_usuari']->value['rol']=='Superadministrador')||($_smarty_tpl->tpl_vars['info_usuari']->value['unitat']=="URB")) {?>
        <h3>Urbanisme</h3>
        <ul class="toggle">
            <li class="icn_new_article"><i class="icon-doc-new"></i><a href="/admin/urbanisme.php?accio=edit_urbanisme_element">Nou Element</a></li>
            <li class="icn_edit_article"><i class="icon-pencil"></i><a href="/admin/urbanisme.php">Gestió elements</a></li>
        </ul>
    <?php }?>
    <?php if (($_smarty_tpl->tpl_vars['info_usuari']->value['rol']=='Superadministrador')||($_smarty_tpl->tpl_vars['info_usuari']->value['unitat']=="PP")) {?>
        <h3>Servei d'Atenció Ciutadana</h3>
        <ul class="toggle">
            <li class="icn_edit_article"><i class="icon-pencil"></i>&nbsp;&nbsp;&nbsp;Pressupostos Participatius
                <ul>
                    <li class="icn_edit_article"><i class="icon-pencil"></i><a href="/admin/pressupostos-participatius/pressupostos_participatius.php">Votació</a>
                    </li>
                    <li class="icn_edit_article"><i class="icon-pencil"></i><a href="/admin/pressupostos-participatius/estadistica.php">Estadística</a>
                    </li>
                </ul>
            </li>
        </ul>
    <?php }?>

    <?php if (($_smarty_tpl->tpl_vars['info_usuari']->value['rol']=='Superadministrador')||($_smarty_tpl->tpl_vars['info_usuari']->value['unitat']=='INF')) {?>
    <h3>Pressupostos participatius</h3>
    <ul class="toggle">
        <li class="icn_edit_article"><i class="icon-pencil"></i><a href="/admin/pressupostos-participatius/propostes/propostes.php?accio=edit_proposta">Nova actuació</a>
        </li>        
        <li class="icn_edit_article"><i class="icon-pencil"></i><a href="/admin/pressupostos-participatius/propostes/propostes.php">Gestió d'actuacions</a>
        </li>                
    </ul>
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['info_usuari']->value['rol']=='Superadministrador') {?>
    <!--
    <h3>Usuaris</h3>
    <ul class="toggle">
        <li class="icn_add_user"><a href="edit_user.php">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nou usuari</a></li>
        <li class="icn_view_users"><a href="list_user.php">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Gestió usuaris</a></li>
    </ul>
    -->
    <?php }?>
    <h3>Altres</h3>
    <ul class="toggle">
        <li class="icn_jump_back"><i class="icon-logout"></i><a href="/admin/logout.php">Sortir</a></li>
    </ul>

    <footer>
        <hr />
        <p><strong>Copyright &copy; 2017 Website Ajuntament de Tortosa</strong></p>
    </footer>
</aside><!-- end of sidebar --><?php }} ?>
