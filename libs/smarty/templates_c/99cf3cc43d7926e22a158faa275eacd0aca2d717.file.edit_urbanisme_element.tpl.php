<?php /* Smarty version Smarty-3.1.16, created on 2018-01-19 14:11:28
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/admin/templates/edit_urbanisme_element.tpl" */ ?>
<?php /*%%SmartyHeaderCode:5361526205a61ee80c8e570-73888771%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '99cf3cc43d7926e22a158faa275eacd0aca2d717' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/admin/templates/edit_urbanisme_element.tpl',
      1 => 1440576458,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5361526205a61ee80c8e570-73888771',
  'function' => 
  array (
    'links' => 
    array (
      'parameter' => 
      array (
        'num_link' => 0,
        'link_items' => NULL,
      ),
      'compiled' => '',
    ),
  ),
  'variables' => 
  array (
    'num_link' => 0,
    'link_items' => 0,
    'item' => 0,
    'items_menu' => 0,
    'fila' => 0,
    'i' => 0,
  ),
  'has_nocache_code' => 0,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a61ee80e8d547_10181417',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a61ee80e8d547_10181417')) {function content_5a61ee80e8d547_10181417($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/vhost/www2.tortosa.cat/home/html/libs/smarty/plugins/modifier.date_format.php';
?><?php if (!function_exists('smarty_template_function_links')) {
    function smarty_template_function_links($_smarty_tpl,$params) {
    $saved_tpl_vars = $_smarty_tpl->tpl_vars;
    foreach ($_smarty_tpl->smarty->template_functions['links']['parameter'] as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);};
    foreach ($params as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);}?>
    <p class="links" <?php if ($_smarty_tpl->tpl_vars['link_items']->value[$_smarty_tpl->tpl_vars['num_link']->value]->TITOL=='') {?>style="display:none"<?php }?> id="link<?php echo $_smarty_tpl->tpl_vars['num_link']->value;?>
">
        <label>Enllaç</label>
        <input type="text" name="TEXT_LINK_<?php echo $_smarty_tpl->tpl_vars['num_link']->value;?>
" id="TEXT_LINK_<?php echo $_smarty_tpl->tpl_vars['num_link']->value;?>
" value="<?php if ($_smarty_tpl->tpl_vars['link_items']->value[$_smarty_tpl->tpl_vars['num_link']->value]->TITOL!='') {?><?php echo $_smarty_tpl->tpl_vars['link_items']->value[$_smarty_tpl->tpl_vars['num_link']->value]->TITOL;?>
<?php } else { ?>títol<?php }?>">
        <input type="text" name="LINK_<?php echo $_smarty_tpl->tpl_vars['num_link']->value;?>
" class="link" id="LINK_<?php echo $_smarty_tpl->tpl_vars['num_link']->value;?>
" value="<?php if ($_smarty_tpl->tpl_vars['link_items']->value[$_smarty_tpl->tpl_vars['num_link']->value]->LINK!='') {?><?php echo $_smarty_tpl->tpl_vars['link_items']->value[$_smarty_tpl->tpl_vars['num_link']->value]->LINK;?>
<?php } else { ?>link<?php }?>">
        <select name="TIPUS_<?php echo $_smarty_tpl->tpl_vars['num_link']->value;?>
" id="TIPUS_<?php echo $_smarty_tpl->tpl_vars['num_link']->value;?>
">
            <option value="">-- seleccionar --</option>
            <option value="diari oficial" <?php if ($_smarty_tpl->tpl_vars['link_items']->value[$_smarty_tpl->tpl_vars['num_link']->value]->TIPUS=="diari oficial") {?>selected<?php }?>>Diari Oficial</option>
            <option value="altres" <?php if ($_smarty_tpl->tpl_vars['link_items']->value[$_smarty_tpl->tpl_vars['num_link']->value]->TIPUS=="altres") {?>selected<?php }?>>Altres</option>
        </select> <a href="JavaScript:delete_link('<?php echo $_smarty_tpl->tpl_vars['num_link']->value;?>
');"><i class="icon-trash-empty" title="Esborrar enllaç" alt="Esborrar enllaç"></i></a>
        <input type="hidden" name="link_active_<?php echo $_smarty_tpl->tpl_vars['num_link']->value;?>
" id="link_active_<?php echo $_smarty_tpl->tpl_vars['num_link']->value;?>
" value="<?php if ($_smarty_tpl->tpl_vars['link_items']->value[$_smarty_tpl->tpl_vars['num_link']->value]->TITOL!='') {?>1<?php } else { ?>0<?php }?>">
    </p>
<?php $_smarty_tpl->tpl_vars = $saved_tpl_vars;
foreach (Smarty::$global_tpl_vars as $key => $value) if(!isset($_smarty_tpl->tpl_vars[$key])) $_smarty_tpl->tpl_vars[$key] = $value;}}?>


<section id="main" class="column">

<h4 class="alert_info"><i class="icon-doc-new"></i>Alta element urbanisme</h4>
<article class="module width_full">
    <div class="module_content">
        <form id="urbanismeForm" method="post" action="urbanisme.php" novalidate="novalidate" enctype="multipart/form-data">
            <input type="hidden" name="accio" id="accio" value="edit_urbanisme_element">
            <p>
                <label>Títol *</label>
                <input type="text" name="TITOL" id="TITOL" value="<?php echo $_smarty_tpl->tpl_vars['item']->value->TITOL;?>
">
            </p>
            <p>
                <label>Enllaç</label>
                <input type="text" name="LINK" id="LINK" value="<?php echo $_smarty_tpl->tpl_vars['item']->value->LINK;?>
" class="link">
            </p>
            <p>
                <label>Data</label>
                <input type="text" name="DATA" id="DATA" value="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['item']->value->DATA,"%d/%m/%G");?>
">
            </p>
            <p>
                <label>Menú superior</label>
                <select name="ID_MENU_NAVEGACIO" id="ID_MENU_NAVEGACIO">
                    <option value="">-- Seleccionar --</option>
                    <?php  $_smarty_tpl->tpl_vars['fila'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['fila']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['items_menu']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['fila']->key => $_smarty_tpl->tpl_vars['fila']->value) {
$_smarty_tpl->tpl_vars['fila']->_loop = true;
?>
                        <?php if ($_smarty_tpl->tpl_vars['item']->value->ID_MENU_NAVEGACIO==$_smarty_tpl->tpl_vars['fila']->value->ID) {?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['fila']->value->ID;?>
" selected><?php echo $_smarty_tpl->tpl_vars['fila']->value->TITOL;?>
 - Nivell: <?php echo $_smarty_tpl->tpl_vars['fila']->value->NIVELL;?>
</option>
                        <?php } else { ?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['fila']->value->ID;?>
"><?php echo $_smarty_tpl->tpl_vars['fila']->value->TITOL;?>
 - Nivell: <?php echo $_smarty_tpl->tpl_vars['fila']->value->NIVELL;?>
</option>
                        <?php }?>
                    <?php } ?>
                </select>
            </p>
            <p>
                <label>Actiu</label>
                <input type="checkbox" name="ACTIVA" id="ACTIVA" <?php if ($_smarty_tpl->tpl_vars['item']->value->ACTIVA==1) {?> checked <?php }?>>
            </p>
            <p>
                <label>Enllaços</label>
            </p>
            <p><a href="JavaScript:new_link();">Nou enllaç</a></p>
            <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? 100+1 - (0) : 0-(100)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 0, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
                <?php smarty_template_function_links($_smarty_tpl,array('num_link'=>$_smarty_tpl->tpl_vars['i']->value,'link_items'=>$_smarty_tpl->tpl_vars['link_items']->value));?>

            <?php }} ?>
            <p>
                <input class="submit" type="submit" value="Desar">
            </p>
            <p>
                <label>* Camps obligatoris</label>
            </p>
            <input type="hidden" name="ID" id="ID" value="<?php echo $_smarty_tpl->tpl_vars['item']->value->ID;?>
">
        </form>
    </div>
</article>



<!-- end of styles article -->

<div class="spacer"></div>
</section>

<script>
    /*
    $.validator.setDefaults({
        submitHandler: function() {
            alert("submitted!");
        }
    });
    */
    var num_elements = <?php echo count($_smarty_tpl->tpl_vars['link_items']->value);?>
;
    function new_link() {
        $("#link"+num_elements).css("display","block");
        $("#link_active_"+num_elements).val('1');
        num_elements++;
    }

    function delete_link(num_item) {
        $("#link"+num_item).css("display","none");
        $("#link_active_"+num_item).val('0');
    }

    $().ready(function() {
        $( "#DATA" ).datepicker();

        // validate signup form on keyup and submit
        
        $("#urbanismeForm").validate({
            rules: {
                TITOL:  {
                    required: true,
                    maxlength: 200
                }
            },
            messages: {
                TITOL: {
                    required: "El camp de títol és obligatori",
                    maxlength: "El camp de títol és massa llarg"
                }
            }
        });

        <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? 100+1 - (0) : 0-(100)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 0, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
        $("#urbanismeForm input#TEXT_LINK_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
").focus(function() {
            if($(this).val()=='títol')
                $(this).val('');
        });
        $("#urbanismeForm input#LINK_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
").focus(function() {
            if($(this).val()=='link')
                $(this).val('');
        });
        <?php }} ?>
    });
</script><?php }} ?>
