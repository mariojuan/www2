<?php /* Smarty version Smarty-3.1.16, created on 2017-12-12 16:10:21
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/pciutadana/sistemes.tpl" */ ?>
<?php /*%%SmartyHeaderCode:18783727975a2ff15d427541-69735657%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'dbea525f1551d3bc94c263da5480f8e6edbbc1a1' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/pciutadana/sistemes.tpl',
      1 => 1511451177,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18783727975a2ff15d427541-69735657',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'apartat' => 0,
    'lang' => 0,
    'LABEL_ERROR_1' => 0,
    'LABEL_ERROR_2' => 0,
    'LABEL_ERROR_3' => 0,
    'LABEL_ERROR_4' => 0,
    'LABEL_ERROR_5' => 0,
    'LABEL_ERROR_6' => 0,
    'LABEL_ERROR_7' => 0,
    'LABEL_ERROR_INTEGER' => 0,
    'LABEL_ERROR_8' => 0,
    'LABEL_ERROR_9' => 0,
    'LABEL_ERROR_10' => 0,
    'LABEL_ERROR_11' => 0,
    'LABEL_ERROR_EMAIL' => 0,
    'LABEL_ERROR_12' => 0,
    'LABEL_ERROR_13' => 0,
    'LABEL_ERROR_14' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a2ff15d4c98d7_54427527',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a2ff15d4c98d7_54427527')) {function content_5a2ff15d4c98d7_54427527($_smarty_tpl) {?><html>
	<head>
        <?php echo $_smarty_tpl->getSubTemplate ("head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

        <?php if ($_smarty_tpl->tpl_vars['apartat']->value==5) {?>
            <script src='https://www.google.com/recaptcha/api.js?hl=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
'></script>
            <script type="text/javascript">
                var captcha_value = false;
                $().ready(function() {
                    $("#queixes_i_suggeriments_form").validate({
                        rules: {
                            nom_sollicitant:  {
                                required: true
                            },
                            cognom1_sollicitant: {
                                required: true
                            },
                            tipus_document_sollicitant: {
                                required: true
                            },
                            numero_document_sollicitant: {
                                required: true
                            },
                            tipus_via_sollicitant: {
                                required: true
                            },
                            tipus_nom_sollicitant: {
                                required: true
                            },
                            numero_sollicitant: {
                                required: true,
                                digits: true
                            },
                            lletra_sollicitant: {
                                required: true
                            },
                            km_sollicitant: {
                                digits: true
                            },
                            pis_sollicitant: {
                                digits: true
                            },
                            provincia_sollicitant: {
                                required: true
                            },
                            municipi_sollicitant: {
                                required: true
                            },
                            codi_postal_sollicitant: {
                                required: true,
                                digits: true
                            },
                            email_sollicitant: {
                                email:true
                            },
                            tipus_peticio_sollicitant: {
                                required: true
                            },
                            afecta_a_sollicitant: {
                                required: true
                            },
                            descripcio_sollicitant: {
                                required: true
                            }
                        },
                        messages: {
                            nom_sollicitant: {
                                required: "<?php echo $_smarty_tpl->tpl_vars['LABEL_ERROR_1']->value;?>
"
                            },
                            cognom1_sollicitant: {
                                required: "<?php echo $_smarty_tpl->tpl_vars['LABEL_ERROR_2']->value;?>
"
                            },
                            tipus_document_sollicitant: {
                                required: "<?php echo $_smarty_tpl->tpl_vars['LABEL_ERROR_3']->value;?>
"
                            },
                            numero_document_sollicitant: {
                                required: "<?php echo $_smarty_tpl->tpl_vars['LABEL_ERROR_4']->value;?>
"
                            },
                            tipus_via_sollicitant: {
                                required: "<?php echo $_smarty_tpl->tpl_vars['LABEL_ERROR_5']->value;?>
"
                            },
                            tipus_nom_sollicitant: {
                                required: "<?php echo $_smarty_tpl->tpl_vars['LABEL_ERROR_6']->value;?>
"
                            },
                            numero_sollicitant: {
                                required: "<?php echo $_smarty_tpl->tpl_vars['LABEL_ERROR_7']->value;?>
",
                                digits: "<?php echo $_smarty_tpl->tpl_vars['LABEL_ERROR_INTEGER']->value;?>
"
                            },
                            lletra_sollicitant: {
                                required: "<?php echo $_smarty_tpl->tpl_vars['LABEL_ERROR_8']->value;?>
"
                            },
                            km_sollicitant: {
                                digits: "<?php echo $_smarty_tpl->tpl_vars['LABEL_ERROR_INTEGER']->value;?>
"
                            },
                            pis_sollicitant: {
                                digits: "<?php echo $_smarty_tpl->tpl_vars['LABEL_ERROR_INTEGER']->value;?>
"
                            },
                            provincia_sollicitant: {
                                required: "<?php echo $_smarty_tpl->tpl_vars['LABEL_ERROR_9']->value;?>
"
                            },
                            municipi_sollicitant: {
                                required: "<?php echo $_smarty_tpl->tpl_vars['LABEL_ERROR_10']->value;?>
"
                            },
                            codi_postal_sollicitant: {
                                required: "<?php echo $_smarty_tpl->tpl_vars['LABEL_ERROR_11']->value;?>
",
                                digits: "<?php echo $_smarty_tpl->tpl_vars['LABEL_ERROR_INTEGER']->value;?>
"
                            },
                            email_sollicitant: {
                                email: "<?php echo $_smarty_tpl->tpl_vars['LABEL_ERROR_EMAIL']->value;?>
"
                            },
                            tipus_peticio_sollicitant: {
                                required: "<?php echo $_smarty_tpl->tpl_vars['LABEL_ERROR_12']->value;?>
"
                            },
                            afecta_a_sollicitant: {
                                required: "<?php echo $_smarty_tpl->tpl_vars['LABEL_ERROR_13']->value;?>
"
                            },
                            descripcio_sollicitant: {
                                required: "<?php echo $_smarty_tpl->tpl_vars['LABEL_ERROR_14']->value;?>
"
                            }
                        },
                        submitHandler: function(form) {
                            captcha()
                            if(captcha_value) {
                                form.submit();
                            }
                        }
                    });

                    $("#incidencies_via_publica_form").validate();
                });

                function captcha() {
                    var xmlhttp = new XMLHttpRequest();
                    xmlhttp.onreadystatechange = function() {
                        if (this.readyState == 4 && this.status == 200) {
                            var out = this.responseText;
                            if(out==1) {
                                captcha_value = true;
                            }
                            else {
                                $('#recaptcha-error').css('display', 'block');
                            }
                        }
                    };
                    xmlhttp.open("POST", "http://www2.tortosa.cat/libs/verify-recaptcha.php", false);
                    xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                    xmlhttp.send("g-recaptcha-response="+grecaptcha.getResponse());
                }
            </script>
        <?php }?>
	</head>
	<body>
		<?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

        <?php if ($_smarty_tpl->tpl_vars['apartat']->value==5) {?>
            <?php echo $_smarty_tpl->getSubTemplate ("pciutadana/queixes_suggeriments.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('image'=>"/images/pciutadana/tira.jpg",'ntitol'=>2), 0);?>

        <?php } else { ?>
		    <?php echo $_smarty_tpl->getSubTemplate ("pciutadana/sistemes_home.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('image'=>"/images/pciutadana/tira.jpg",'ntitol'=>2), 0);?>

        <?php }?>
		<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

	</body>
</html><?php }} ?>
