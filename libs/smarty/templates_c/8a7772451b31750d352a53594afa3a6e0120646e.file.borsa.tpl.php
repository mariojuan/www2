<?php /* Smarty version Smarty-3.1.16, created on 2017-12-12 14:58:54
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/borsa/borsa.tpl" */ ?>
<?php /*%%SmartyHeaderCode:16950601675a2fe09e91dd00-55768008%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8a7772451b31750d352a53594afa3a6e0120646e' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/borsa/borsa.tpl',
      1 => 1499248140,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '16950601675a2fe09e91dd00-55768008',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'LABEL_TITOL1' => 0,
    'MENU' => 0,
    'itemMenu' => 0,
    'LABEL_TITOL2' => 0,
    'LABEL_TC1' => 0,
    'elements' => 0,
    'value' => 0,
    'page' => 0,
    'lang' => 0,
    'lastpage' => 0,
    'tipus' => 0,
    'page_ant' => 0,
    'lblAnt' => 0,
    'desp' => 0,
    'firstpage' => 0,
    'counter' => 0,
    'page_seg' => 0,
    'lblSeg' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a2fe09ea58d41_03157122',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a2fe09ea58d41_03157122')) {function content_5a2fe09ea58d41_03157122($_smarty_tpl) {?><html>
   <head>
      <?php echo $_smarty_tpl->getSubTemplate ("head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

   </head>
   <body id="borsa">
      <?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

      <div id="page-wrap">
      <div class="contenedor-responsive">
         <div id="conttranstotalcos">
            <div id="conttranscos">
               <div id="btranspresen">
                  <div id="transimatge">
                     <img src="/images/laciutat/municipi/tira.jpg" class="img-slider-territori"/>
                  </div>
                  <div id="transimatgeText">
                     <h1>
                        <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

                        <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                     </h1>
                  </div>
               </div>
               <ul id='menu_planA'>
                  <?php  $_smarty_tpl->tpl_vars['itemMenu'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['itemMenu']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['MENU']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['itemMenu']->key => $_smarty_tpl->tpl_vars['itemMenu']->value) {
$_smarty_tpl->tpl_vars['itemMenu']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['itemMenu']->key;
?>
                  <a href=<?php echo $_smarty_tpl->tpl_vars['itemMenu']->value['link'];?>
 target="<?php echo $_smarty_tpl->tpl_vars['itemMenu']->value['target'];?>
">
                     <li id='menu_planA_item'>
                        <?php echo $_smarty_tpl->tpl_vars['itemMenu']->value['name'];?>

                     </li>
                  </a>
                  <?php } ?>
               </ul>
               <div id="tcentre_planA">
                  <h2><?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL2']->value;?>
</h2>
                  <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
</p>
                  <ul style="list-style-type: none; width: 80%">
                     <?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['value']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['elements']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value) {
$_smarty_tpl->tpl_vars['value']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['value']->key;
?>
                        <li>
                           &#8226; <?php echo trim(utf8_encode($_smarty_tpl->tpl_vars['value']->value->descripcio_curta));?>
 - <a href="/borsa/fitxa.php?id=<?php echo $_smarty_tpl->tpl_vars['value']->value->Codi;?>
&page=<?php echo $_smarty_tpl->tpl_vars['page']->value;?>
&lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
"><i class="icon-link" title="<?php echo trim(utf8_encode($_smarty_tpl->tpl_vars['value']->value->descripcio_curta));?>
"></i></a>
                        <li>
                     <?php } ?>
                  </ul>
                  </div>
                  <div id="pagination">
                     <ul>
                        <?php if ($_smarty_tpl->tpl_vars['lastpage']->value>1) {?>
                        <?php if ($_smarty_tpl->tpl_vars['page']->value>1) {?>
                        <?php $_smarty_tpl->tpl_vars["page_ant"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value-1, null, 0);?>
                        <li><a href="index.php?tipus=<?php echo $_smarty_tpl->tpl_vars['tipus']->value;?>
&lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
&accio=list&page=<?php echo $_smarty_tpl->tpl_vars['page_ant']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['lblAnt']->value;?>
</a></li>
                        <?php }?>
                        <?php $_smarty_tpl->tpl_vars["desp"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value+4, null, 0);?>
                        <?php $_smarty_tpl->tpl_vars["firstpage"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value-4, null, 0);?>
                        <?php if ($_smarty_tpl->tpl_vars['desp']->value<$_smarty_tpl->tpl_vars['lastpage']->value) {?>
                        <?php $_smarty_tpl->tpl_vars["lastpage"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value+4, null, 0);?>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['firstpage']->value<1) {?>
                        <?php $_smarty_tpl->tpl_vars["firstpage"] = new Smarty_variable(1, null, 0);?>
                        <?php }?>
                        <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['counter']->step = 1;$_smarty_tpl->tpl_vars['counter']->total = (int) ceil(($_smarty_tpl->tpl_vars['counter']->step > 0 ? $_smarty_tpl->tpl_vars['lastpage']->value+1 - ($_smarty_tpl->tpl_vars['firstpage']->value) : $_smarty_tpl->tpl_vars['firstpage']->value-($_smarty_tpl->tpl_vars['lastpage']->value)+1)/abs($_smarty_tpl->tpl_vars['counter']->step));
if ($_smarty_tpl->tpl_vars['counter']->total > 0) {
for ($_smarty_tpl->tpl_vars['counter']->value = $_smarty_tpl->tpl_vars['firstpage']->value, $_smarty_tpl->tpl_vars['counter']->iteration = 1;$_smarty_tpl->tpl_vars['counter']->iteration <= $_smarty_tpl->tpl_vars['counter']->total;$_smarty_tpl->tpl_vars['counter']->value += $_smarty_tpl->tpl_vars['counter']->step, $_smarty_tpl->tpl_vars['counter']->iteration++) {
$_smarty_tpl->tpl_vars['counter']->first = $_smarty_tpl->tpl_vars['counter']->iteration == 1;$_smarty_tpl->tpl_vars['counter']->last = $_smarty_tpl->tpl_vars['counter']->iteration == $_smarty_tpl->tpl_vars['counter']->total;?>
                        <?php if ($_smarty_tpl->tpl_vars['counter']->value==$_smarty_tpl->tpl_vars['page']->value) {?>
                        <li><?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
</li>
                        <?php } else { ?>
                        <li><a href="index.php?tipus=<?php echo $_smarty_tpl->tpl_vars['tipus']->value;?>
&lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
&accio=list&page=<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
</a></li>
                        <?php }?>
                        <?php }} ?>
                        <?php if ($_smarty_tpl->tpl_vars['page']->value<$_smarty_tpl->tpl_vars['lastpage']->value) {?>
                        <?php $_smarty_tpl->tpl_vars["page_seg"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value+1, null, 0);?>
                        <li><a href="index.php?tipus=<?php echo $_smarty_tpl->tpl_vars['tipus']->value;?>
&lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
&accio=list&page=<?php echo $_smarty_tpl->tpl_vars['page_seg']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['lblSeg']->value;?>
</a></li>
                        <?php }?>
                        <?php } else { ?>
                        <li>1</li>
                        <?php }?>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

   </body>
</html><?php }} ?>
