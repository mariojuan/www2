<?php /* Smarty version Smarty-3.1.16, created on 2017-12-13 02:46:09
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/festes/cinta17/salutacions.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3675634165a3086619fb874-06291260%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '586fd5827f85080cbc4262d107c8bfa30fc46f87' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/festes/cinta17/salutacions.tpl',
      1 => 1503491417,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3675634165a3086619fb874-06291260',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'image' => 0,
    'LABEL_TITOL1' => 0,
    'MENU' => 0,
    'itemMenu' => 0,
    'LABEL01' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a308661a96e52_89099265',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a308661a96e52_89099265')) {function content_5a308661a96e52_89099265($_smarty_tpl) {?><!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->
<div id="page-wrap">

   <div class="contenedor-responsive">

      <div id="conttranstotalcos">

         <div id="conttranscos">

            <div id="btranspresen">

               <div id="transimatge">
                  <img src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
" class="img-slider-territori"/>
               </div>

               <div id="transimatgeText">
                  <h1>
                     <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

                     <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                  </h1>
               </div>

            </div>

            <ul id='menu_planA'>
               <?php  $_smarty_tpl->tpl_vars['itemMenu'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['itemMenu']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['MENU']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['itemMenu']->key => $_smarty_tpl->tpl_vars['itemMenu']->value) {
$_smarty_tpl->tpl_vars['itemMenu']->_loop = true;
?>
               <a href=<?php echo $_smarty_tpl->tpl_vars['itemMenu']->value['link'];?>
 target="_<?php echo $_smarty_tpl->tpl_vars['itemMenu']->value['target'];?>
">
                  <li id='menu_planA_item'>
                     <?php echo $_smarty_tpl->tpl_vars['itemMenu']->value['name'];?>

                  </li>
               </a>
               <?php } ?>
            </ul>

            <div id="tcentre_planA">
               <h2><?php echo $_smarty_tpl->tpl_vars['LABEL01']->value;?>
</h2>

               <div id="taulapubilles">  
                  <div id="filapubilles">
                     <div class="colpubilles">
                        <a href="/docs/festes/cinta17/saludapr.pdf" target="_blank">
                              <img class="avatar3" src="/images/festes/cinta17/saludes/pgencat.jpg">
                        </a>                           
                        <br><br>
                        <div id="textnegreta">Carles Puigdemont i Casamajó</div>
                        <div id="textnormal">Molt Honorable President de la Generalitat de Catalunya</div>
                        <div id="textnormal"></div>
                     </div>
                     <div class="colpubilles"> 
                        <a href="/docs/festes/cinta17/saludaal.pdf" target="_blank">
                              <img class="avatar3" src="/images/festes/cinta17/saludes/atortosa.jpg">
                        </a>
                        <br><br>
                        <div id="textnegreta">Ferran Bel i Accensi</div>
                        <div id="textnormal">alcalde de Tortosa</div>
                        <div id="textnormal"></div>
                     </div>
                     <div class="colpubilles"> 
                        <a href="/docs/festes/cinta17/saludapg.pdf" target="_blank">
                              <img class="avatar3" src="/images/festes/cinta17/saludes/prego.jpg">
                        </a>
                        <br><br>
                        <div id="textnegreta">Pere Estupinyà Giné</div>
                        <div id="textnormal">pregoner festes de la Cinta 2017</div>
                        <div id="textnormal"></div>
                     </div>                   
                  </div>

                  <div id="filapubilles">
                     <div class="colpubilles">
                        <a href="/docs/festes/cinta17/saludabi.pdf" target="_blank">
                              <img class="avatar3" src="/images/festes/cinta17/saludes/btortosa.jpg">
                        </a>                           
                        <br><br>
                        <div id="textnegreta">Enrique Benavent Vidal</div>
                        <div id="textnormal">bisbe de Tortosa</div>
                        <div id="textnormal"></div>
                     </div>
                     <div class="colpubilles"> 
                        <a href="/docs/festes/cinta17/saludarg.pdf" target="_blank">
                              <img class="avatar3" src="/images/festes/cinta17/saludes/rfestes.jpg">
                        </a>
                        <br><br>
                        <div id="textnegreta">Domingo Tomàs i Audí</div>
                        <div id="textnormal">regidor de festes de Tortosa</div>
                        <div id="textnormal"></div>
                     </div>
                     <div class="colpubilles"> 
                        <a href="/docs/festes/cinta17/saludama.pdf" target="_blank">
                              <img class="avatar3" src="/images/festes/cinta17/saludes/pmajor.jpg">
                        </a>
                        <br><br>
                        <div id="textnegreta">Francesc Viñes i Albacar</div>
                        <div id="textnormal">primer majordom de la Reial Arxiconfraria Nostra Senyora de la Cinta</div>
                        <div id="textnormal"></div>
                     </div>                   
                  </div>

               </div>
            </div>

            <div class="separator"></div>

         </div>
      </div>
   </div>
</div><?php }} ?>
