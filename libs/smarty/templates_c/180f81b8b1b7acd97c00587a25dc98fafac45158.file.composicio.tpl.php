<?php /* Smarty version Smarty-3.1.16, created on 2018-02-28 12:57:25
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/junta-govern-local/composicio.tpl" */ ?>
<?php /*%%SmartyHeaderCode:5186854875a3084c7c4ff29-31820118%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '180f81b8b1b7acd97c00587a25dc98fafac45158' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/junta-govern-local/composicio.tpl',
      1 => 1519819034,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5186854875a3084c7c4ff29-31820118',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a3084c7ce6845_80332249',
  'variables' => 
  array (
    'image' => 0,
    'LABEL_TITOL1' => 0,
    'LABEL_TC0' => 0,
    'LABEL_TC1' => 0,
    'LABEL_TC2' => 0,
    'LABEL_FOTO' => 0,
    'LABEL_NOM' => 0,
    'LABEL_GRUP' => 0,
    'LABEL_CARREC' => 0,
    'JUNTAGOVERNLOCAL' => 0,
    'item' => 0,
    'LABEL_TC7' => 0,
    'LABEL_DATA' => 0,
    'LABEL_ACORD' => 0,
    'LABEL_DESC' => 0,
    'DECRETS' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a3084c7ce6845_80332249')) {function content_5a3084c7ce6845_80332249($_smarty_tpl) {?><html>
	<head>

        <?php echo $_smarty_tpl->getSubTemplate ("head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	</head>
	<body>

		<?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


		<div id="page-wrap">

            <div class="contenedor-responsive">

    <div id="conttranstotalcos">
        <div id="conttranscos">
            <div id="btranspresen">
                <div id="transimatge">
                    <img src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
" class="img-slider-territori"/>
                </div>
                <div id="transimatgeText">
                    <h1>
                        <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

                        <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                    </h1>
                </div>
            </div>

            <!-- Menu -->
                <?php echo $_smarty_tpl->getSubTemplate ("junta-govern-local/menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            <!--// Menu -->

            <div id="tcentre_planA" class="rectificat-junta-gov-local" style="margin-left: -36px; margin-top: -6px;">

                <font size="4" color="#666666"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC0']->value;?>
</font>
                <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
</p>
                <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
</p>

                <table>
				<thead>
                  <tr>
                    <th><?php echo $_smarty_tpl->tpl_vars['LABEL_FOTO']->value;?>
</th>
                    <th><?php echo $_smarty_tpl->tpl_vars['LABEL_NOM']->value;?>
</th>
                    <th><?php echo $_smarty_tpl->tpl_vars['LABEL_GRUP']->value;?>
</th>
					<th><?php echo $_smarty_tpl->tpl_vars['LABEL_CARREC']->value;?>
</th>
                    </tr>
					</thead>
					<tbody>
                    	<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['JUNTAGOVERNLOCAL']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
                    	<tr>
                        	<td><a href="<?php echo $_smarty_tpl->tpl_vars['item']->value['link'];?>
" target="<?php echo $_smarty_tpl->tpl_vars['item']->value['target'];?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['item']->value['foto'];?>
" class="avatar"/></a></td>
                        	<td><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
</td>
                        	<td><?php echo $_smarty_tpl->tpl_vars['item']->value['group'];?>
</td>
							<td><?php echo $_smarty_tpl->tpl_vars['item']->value['occupation'];?>
</td>
                   		</tr>
                    	<?php } ?>
						</tbody>
                </table>

                <!-- Membres -->
                <!--
                <ul>
                <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['JUNTAGOVERNLOCAL']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
                    <li><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
 - <?php echo $_smarty_tpl->tpl_vars['item']->value['group'];?>
 - <?php echo $_smarty_tpl->tpl_vars['item']->value['occupation'];?>
 - <a href="<?php echo $_smarty_tpl->tpl_vars['item']->value['link'];?>
" target="<?php echo $_smarty_tpl->tpl_vars['item']->value['target'];?>
"><i class="<?php echo $_smarty_tpl->tpl_vars['item']->value['icon'];?>
"></i></a></li> 
                <?php } ?>
                </ul>
                -->

                <!-- Taula Decrets -->

                <div class="separator"></div>
                <div class="separator"></div>

                <h3><?php echo $_smarty_tpl->tpl_vars['LABEL_TC7']->value;?>
</h3>
                <table>
                <thead>
                  <tr>
                    <th><?php echo $_smarty_tpl->tpl_vars['LABEL_DATA']->value;?>
</th>
                    <th><?php echo $_smarty_tpl->tpl_vars['LABEL_ACORD']->value;?>
</th>
                    <th><?php echo $_smarty_tpl->tpl_vars['LABEL_DESC']->value;?>
</th>
                    </tr>
                    </thead>
                    <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['DECRETS']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
                    <tr>
                        <td><?php echo $_smarty_tpl->tpl_vars['item']->value['date'];?>
</td>
                        <td><a href="<?php echo $_smarty_tpl->tpl_vars['item']->value['link'];?>
" target="<?php echo $_smarty_tpl->tpl_vars['item']->value['target'];?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value['agreement'];?>
</a></td>
                        <td><?php echo $_smarty_tpl->tpl_vars['item']->value['desc'];?>
</td>
                    </tr>
                    <?php } ?>
                </table>

                <!-- //Taula Decrets -->



                

            </div>
        </div>
    </div>
    </div>
</div>

		<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	</body>
</html><?php }} ?>
