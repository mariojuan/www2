<?php /* Smarty version Smarty-3.1.16, created on 2017-12-13 02:32:33
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/ple/comissions_informatives.tpl" */ ?>
<?php /*%%SmartyHeaderCode:13235065145a308331c80c81-86471314%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'dd928e691e1bcdb644017eeebcbd9625a1e6865d' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/ple/comissions_informatives.tpl',
      1 => 1499153931,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13235065145a308331c80c81-86471314',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'image' => 0,
    'LABEL_TITOL1' => 0,
    'LABEL_TITOLSECCIO' => 0,
    'LABEL_TC1' => 0,
    'LABEL_TC2' => 0,
    'LABEL_TC3' => 0,
    'LABEL_TC4' => 0,
    'LABEL_REPRESENTANT' => 0,
    'LABEL_REGIDORS' => 0,
    'COMISSIONS_REGIDORS' => 0,
    'item' => 0,
    'LABEL_ACORDS_ADOPTATS' => 0,
    'FILES' => 0,
    'LABEL_ACORDS_COM_INFO_VIG' => 0,
    'COMISSIONS' => 0,
    'LABEL_TC5' => 0,
    'LABEL_TC50' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a308331d604d5_18695607',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a308331d604d5_18695607')) {function content_5a308331d604d5_18695607($_smarty_tpl) {?><html>
    <head>

        <?php echo $_smarty_tpl->getSubTemplate ("head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


    </head>
    <body>

        <?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


        <div id="page-wrap">
        <div class="contenedor-responsive">
            <div id="conttranstotalcos">
                <div id="conttranscos">
                    <div id="btranspresen">
                        <div id="transimatge">
                            <img src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
" class="img-slider-territori"/>
                        </div>
                        <div id="transimatgeText">
                            <h1>
                                <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

                                <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                            </h1>
                        </div>
                    </div>

                    <!-- Menu -->
                    <?php echo $_smarty_tpl->getSubTemplate ("ple/menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

                    <!--// Menu -->

                    <div id="tcentre_planA">

                        <font size="4" color="#666666"><?php echo $_smarty_tpl->tpl_vars['LABEL_TITOLSECCIO']->value;?>
</font>

                        <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
</p>
                        <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
</p>
                        <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC3']->value;?>
</p>
                        <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC4']->value;?>
</p>

                        <!-- Taula compos -->

                        <table class="tableYves">
                            <tr>
                                <thead>
                                <th><?php echo $_smarty_tpl->tpl_vars['LABEL_REPRESENTANT']->value;?>
</th>
                                <th><?php echo $_smarty_tpl->tpl_vars['LABEL_REGIDORS']->value;?>
</th>
                                </thead>
                            </tr>

                            <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['COMISSIONS_REGIDORS']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
                                <tr>
                                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value['group'];?>
</td>
                                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value['num'];?>
</td>
                                </tr>
                            <?php } ?>

                        </table>

                        <!-- //Taula compos -->

                        <!-- Bloc 2 -->

                        <h3><?php echo $_smarty_tpl->tpl_vars['LABEL_ACORDS_ADOPTATS']->value;?>
</h3>

                        <ul class="listSenseBullets">

                            <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['FILES']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
                                <li>
                                    <i class="icon-file-pdf"></i><a href="<?php echo $_smarty_tpl->tpl_vars['item']->value['file'];?>
" target="<?php echo $_smarty_tpl->tpl_vars['item']->value['target'];?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
</a> 
                                </li>
                            <?php } ?>

                        </ul>

                        <!--// Bloc 2 -->

                        <div class="separator"></div>

                        <!--Bloc3-->

                        <h3><?php echo $_smarty_tpl->tpl_vars['LABEL_ACORDS_COM_INFO_VIG']->value;?>
</h3>

                        <ul class="listSenseBullets">
                            <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['COMISSIONS']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
                                <li>
                                    <i class="icon-link"></i><a href="<?php echo $_smarty_tpl->tpl_vars['item']->value['file'];?>
" target="<?php echo $_smarty_tpl->tpl_vars['item']->value['target'];?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
</a>
                                </li>
                            <?php } ?>
                        </ul>

                        </table>

                        <div class="separator"></div>
                        <!--//Bloc3-->

                        <p class="normativa"><strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC5']->value;?>
</strong> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC50']->value;?>
</p>

                    </div>

                    <div class="separator"></div>
                </div>
            </div>
            </div>
        </div>
        <?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    </body>
</html><?php }} ?>
