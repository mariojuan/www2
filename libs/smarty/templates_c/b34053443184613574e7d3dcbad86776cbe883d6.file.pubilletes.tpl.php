<?php /* Smarty version Smarty-3.1.16, created on 2017-12-13 02:48:59
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/festes/cinta17/pubilletes.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1548751345a30870ba27b44-74447184%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b34053443184613574e7d3dcbad86776cbe883d6' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/festes/cinta17/pubilletes.tpl',
      1 => 1503487904,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1548751345a30870ba27b44-74447184',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'image' => 0,
    'LABEL_TITOL1' => 0,
    'MENU' => 0,
    'itemMenu' => 0,
    'LABEL04' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a30870ba8a562_92893080',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a30870ba8a562_92893080')) {function content_5a30870ba8a562_92893080($_smarty_tpl) {?><!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->
<div id="page-wrap">

   <div class="contenedor-responsive">

      <div id="conttranstotalcos">

         <div id="conttranscos">

            <div id="btranspresen">

               <div id="transimatge">
                  <img src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
" class="img-slider-territori"/>
               </div>

               <div id="transimatgeText">
                  <h1>
                     <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

                     <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                  </h1>
               </div>

            </div>

            <ul id='menu_planA'>
               <?php  $_smarty_tpl->tpl_vars['itemMenu'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['itemMenu']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['MENU']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['itemMenu']->key => $_smarty_tpl->tpl_vars['itemMenu']->value) {
$_smarty_tpl->tpl_vars['itemMenu']->_loop = true;
?>
               <a href=<?php echo $_smarty_tpl->tpl_vars['itemMenu']->value['link'];?>
 target="_<?php echo $_smarty_tpl->tpl_vars['itemMenu']->value['target'];?>
">
                  <li id='menu_planA_item'>
                     <?php echo $_smarty_tpl->tpl_vars['itemMenu']->value['name'];?>

                  </li>
               </a>
               <?php } ?>
            </ul>

            <div id="tcentre_planA">
               <h2 style="text-align: center"><?php echo $_smarty_tpl->tpl_vars['LABEL04']->value;?>
</h2>
               <!--<p style="text-align: center"><img src="/images/laciutat/ifestes.jpg" class="img-responsive"></p>-->

               <div id="taulapubilles">  
                  <div id="filapubilles">
                     <div class="colunapubilla">
                        <a href="/images/festes/cinta17/fpubilletes/00-DTR.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/00-DTR.jpg" width="200" height="150">
                        </a>
                        <br>
                        <div id="textnegreta">DAFNE TEROL ROMERO</div>
                        <div id="textnegreta">Reina Infantil- Cinta 2017</div>
                        <div id="textnormal">ASSEMBLEA LOCAL DE LA CREU ROJA</div>
                     </div>                        
                  </div>
               </div>
      
               <br>

               <div id="taulapubilles">  
                  <div id="filapubilles">
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilletes/01-POG.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/01-POG.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">PAULA OBALAT GRANELL</div>
                        <div id="textnormal">AMICS DE L'ERMITA DEL COLL DE L'ALBA</div>
                     </div>      
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilletes/02-MBG.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/02-MBG.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">MIREIA BONFILL GILABERT</div>
                        <div id="textnormal">ASSOCIACIÓ AMICS DEL CAVALL EQUITOR</div>                        
                     </div>                        
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilletes/03-NCC.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/03-NCC.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">NAYARA CORTÉS COLOMÉ</div>
                        <div id="textnormal">ASSOCIACIÓ COLLA DE DIABLES I TAMBORS «LO GOLAFRE»</div                        
                     </div>
                  </div>

                  <div id="filapubilles">
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilletes/04-ALA.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/04-ALA.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">ABRIL LUNA AZARA</div>
                        <div id="textnormal">ASSOCIACIÓ CULTURAL COLLA JOVE DE DOLÇAINERS</div>                        
                     </div>                        
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilletes/05-JCF.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/05-JCF.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">JANA CURTO FORCADA</div>
                        <div id="textnormal">ASSOCIACIÓ CULTURAL DE DANSA A TORTOSA</div>                        
                     </div>
                     <div class="colpubilles"> 
                        <a href="/images/festes/cinta17/fpubilletes/06-NGM.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/06-NGM.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">NEREA GIMÈNEZ DE LOS GALANES MARCH</div>
                        <div id="textnormal">A. CULTURAL DE FERRERIES LLAMPEC NOIS</div>                        
                     </div>
                  </div>

                  <div id="filapubilles">
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilletes/07-IBM.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/07-IBM.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">IRENE BLANCH MONLLAO</div>
                        <div id="textnormal">ASSOCIACIÓ CULTURAL «4 MÉS 1»</div>                        
                     </div>     
                     <div class="colpubilles"> 
                        <a href="/images/festes/cinta17/fpubilletes/08-AAP.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/08-AAP.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">ALBA ALBIOL PANISELLO</div>
                        <div id="textnormal">ASSOCIACIÓ ESPORTIVA DE FÚTBOL SALA «BAIX EBRE»</div>                        
                     </div>
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilletes/09-ISF.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/09-ISF.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">IVETTE SALVADOR FRANCH</div>
                        <div id="textnormal">ASSOCIACIÓ DE GENT GRAN ESPLAI CAIXA TORTOSA</div>                        
                     </div>                        
                  </div>              

                  <div id="filapubilles">
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilletes/10-ATV.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/10-ATV.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">ALBA TALARN  VIDAL</div>
                        <div id="textnormal">ASSOCIACIÓ GRUP ABANDERATS DE TORTOSA</div>                        
                     </div>
                     <div class="colpubilles"> 
                        <a href="/images/festes/cinta17/fpubilletes/11-AMM.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/11-AMM.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">ÀNGELA MARZO MORESO</div>
                        <div id="textnormal">ASSOCIACIÓ JUVENIL TUNA FOLK DE TORTOSA</div>                        
                     </div>
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilletes/12-MPC.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/12-MPC.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">MARIONA PIÑOL CASTELLANO</div>
                        <div id="textnormal">ASSOCIACIÓ DE VEÏNS CENTRE-NUCLI HISTÒRIC</div>                        
                     </div>
                  </div>

                  <div id="filapubilles">
                     <div class="colpubilles"> 
                        <a href="/images/festes/cinta17/fpubilletes/13-MGC.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/13-MGC.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">MARIA GONZÀLEZ CASANOVA</div>
                        <div id="textnormal">ASSOCIACIÓ DE VEÏNS FERRERIES-SANT VICENT</div>                        
                     </div>
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilletes/14-COG.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/14-COG.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">CLARA ORTEGA GILABERT</div>
                        <div id="textnormal">ASSOCIACIÓ DE VEÏNS PINTOR CASANOVA – REMOLINS</div>                        
                     </div>                        
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilletes/15-LRP.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/15-LRP.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">LAIA ROIG PANISELLO</div>
                        <div id="textnormal">ASSOCIACIÓ DE VEÏNS DEL RASTRE</div>                        
                     </div>
                  </div>

                  <div id="filapubilles">
                     <div class="colpubilles"> 
                        <a href="/images/festes/cinta17/fpubilletes/16-MFB.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/16-MFB.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">MARIA FERRI BLANC</div>
                        <div id="textnormal">ASSOCIACIÓ DE VEÏNS SANT JOSEP DE LA MUNTANYA «ILERCAVÒNIA»</div>                        
                     </div>
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilletes/17-CBH.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/17-CBH.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">CLOE BERENGUÉ HASNAQUI</div>
                        <div id="textnormal">ASSOCIACIÓ DE VEÏNS DE SANTA CLARA</div>                        
                     </div>                        
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilletes/18-MVM.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/18-MVM.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">MARTA VALLÉS MESTRE</div>
                        <div id="textnormal">ASSOCIACIÓ DE VEÏNS DE LA SIMPÀTICA</div>                        
                     </div>
                  </div>

                  <div id="filapubilles">
                     <div class="colpubilles"> 
                        <a href="/images/festes/cinta17/fpubilletes/19-ALM.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/19-ALM.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">ABRIL LAJUNTA MARTÍ</div>
                        <div id="textnormal">ASSOCIACIÓ DE VEÏNS 13 DE GENER I HORTA</div>                        
                     </div>
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilletes/20-EPS.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/20-EPS.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">EIRA PIÑOL SAYAS</div>
                        <div id="textnormal">CANTAIRES DE L'EBRE DELTA</div>                        
                     </div>
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilletes/21-PPL.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/21-PPL.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">PAULA PAREDES LÓPEZ</div>
                        <div id="textnormal">CENTRE D'ESPORTS TORTOSA</div>                        
                     </div>                        
                  </div>


                  <div id="filapubilles">
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilletes/22-ASO.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/22-ASO.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">ALEXANDRA SIMON OBIOL</div>
                        <div id="textnormal">CLUB ATLETISME TERRES DE L'EBRE</div>                        
                     </div>
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilletes/23-AGB.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/23-AGB.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">ALEXANDRA GRAU BARDI</div>
                        <div id="textnormal">CLUB BÀSQUET CANTAIRES</div>                        
                     </div>
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilletes/24-AMP.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/24-AMP.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">ALBA MARTÍNEZ PRINCEP</div>
                        <div id="textnormal">CLUB FÚTBOL EBRE ESCOLA ESPORTIVA</div>                        
                     </div>                        
                  </div>

                  <div id="filapubilles">
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilletes/25-CRT.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/25-CRT.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">CARLA REVERTÉ TORRES</div>
                        <div id="textnormal">CLUB PATÍ DERTUSA</div>                        
                     </div>
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilletes/26-ECB.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/26-ECB.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">EMMA COLOMÉ BENET</div>
                        <div id="textnormal">CLUB TWIRLING TORTOSA</div>                        
                     </div>                        
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilletes/27-SSB.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/27-SSB.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">SARA SAGASETA DE LLUDOZ BEL</div>
                        <div id="textnormal">CLUB UNIVERSITARI</div>                        
                     </div>
                  </div>


                  <div id="filapubilles">
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilletes/28-AVN.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/28-AVN.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">ANDREA VALLÉS NAVICKAITE</div>
                        <div id="textnormal">COLLA GEGANTERA  I CORT DE BESTIES DE TORTOSA</div>                        
                     </div>
                     <div class="colpubilles"> 
                        <a href="/images/festes/cinta17/fpubilletes/29-PRM.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/29-PRM.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">PAULA ROBERT MARTÍ</div>
                        <div id="textnormal">CONSELL ASSESSOR D'ELS REGUERS</div>                        
                     </div>
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilletes/30-NPR.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/30-NPR.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">NOA PUIG REVERTÉ</div>
                        <div id="textnormal">CONSELL ASSESSOR DE VINALLOP</div>                        
                     </div>                        
                  </div>

                  <div id="filapubilles">
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilletes/31-EDV.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/31-EDV.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">EDURNE DOMENECH VALLS</div>
                        <div id="textnormal">ENTITAT MUNICIPAL DESCENTRALITZADA DE BÍTEM</div>                        
                     </div>
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilletes/32-LMP.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/32-LMP.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">LAURA MORENO PAVON</div>
                        <div id="textnormal">ENTITAT MUNICIPAL DESCENTRALITZADA DE CAMPREDÓ</div>                        
                     </div>
                     <div class="colpubilles"> 
                        <a href="/images/festes/cinta17/fpubilletes/33-AQG.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/33-AQG.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">AINHOA QUERAL GIL</div>
                        <div id="textnormal">ENTITAT MUNICIPAL DESCENTRALITZADA DE JESÚS</div>                        
                     </div>
                  </div>

                  <div id="filapubilles">
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilletes/34-CYC.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/34-CYC.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">MARIA CINTA YAGÜE CURTO</div>
                        <div id="textnormal">ESCUDERIA CUCAFERA RACING</div>
                     </div>
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilletes/35-NBF.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/35-NBF.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">NATALIA BERTOMEU FONTANET</div>
                        <div id="textnormal">MONTEPIO DE CONDUCTORS S. CRISTÒFOL DE TORTOSA I COMARQUES</div>
                     </div>
                     <div class="colpubilles"> 
                        <a href="/images/festes/cinta17/fpubilletes/36-MHM.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/36-MHM.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">MAYA PATRICIA HOLVIK MAURI</div>
                        <div id="textnormal">MOTO CLUB TORTOSA</div>                        
                     </div>
                  </div>

                  <div id="filapubilles">
                     <div class="colpubilles"> 
                        <a href="/images/festes/cinta17/fpubilletes/37-JDE.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/37-JDE.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">JÚLIA DÍAZ ESCORIZA</div>
                        <div id="textnormal">ORDE DE LA CUCAFERA</div>                        
                     </div>
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilletes/38-AGM.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/38-AGM.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">ARIADNA GARCIA MULET</div>
                        <div id="textnormal">ORFEÓ TORTOSÍ</div>                        
                     </div>                        
                     <div class="colpubilles"> 
                        <a href="/images/festes/cinta17/fpubilletes/39-NAM.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/39-NAM.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">NAIA ADAM MONCAYO</div>
                        <div id="textnormal">PENYA CICLISTA BAIX EBRE «JA ARRIBAREM»</div>                        
                     </div>
                  </div>


                  <div id="filapubilles">
                     <div class="colpubilles"> 
                        <a href="/images/festes/cinta17/fpubilletes/40-IFR.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/40-IFR.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">IRIA FERNÀNDEZ ROIGO</div>
                        <div id="textnormal">SOCIETAT DE PESCA ESPORTIVA</div>                        
                     </div>
                     <div class="colpubilles">
                     </div>                        
                     <div class="colpubilles">
                     </div>                        
                  </div>

               </div>

               <br>

               <div id="taulapubilles">  
                  <div id="filapubilles">
                     <div class="colunapubilla">
                        <!--<a href="/images/festes/cinta17/fpubilletes/pubilles_grup.jpg" target="_blank">-->
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilletes/pubilletes_grup.jpg" width="508" height="339">
                        <!--</a>-->
                        <br>
                        <div id="textnegreta">PUBILLETES FESTES DE LA CINTA 2017</div>

                     </div>                        
                  </div>
               </div>




            </div>

            <div class="separator"></div>

         </div>
      </div>
   </div>
</div><?php }} ?>
