<?php /* Smarty version Smarty-3.1.16, created on 2018-01-01 03:26:03
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/ple/comissio-informativa/inici.tpl" */ ?>
<?php /*%%SmartyHeaderCode:6596979125a499c3bac5a18-53182077%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '534b32c14987662e94ca0c07f8bf82fe5448a437' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/ple/comissio-informativa/inici.tpl',
      1 => 1496905219,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6596979125a499c3bac5a18-53182077',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'image' => 0,
    'LABEL_TITOL1' => 0,
    'LABEL_TCSUB' => 0,
    'LABEL_TC0' => 0,
    'LABEL_TC1' => 0,
    'LABEL_FOTO' => 0,
    'LABEL_NOM' => 0,
    'LABEL_GRUP' => 0,
    'LABEL_CARREC' => 0,
    'COMPOSICIO_MEMBRES' => 0,
    'item' => 0,
    'LABEL_TCSEC' => 0,
    'COMPOSICIO_SECRETARIS' => 0,
    'LABEL_TC2' => 0,
    'COMPETENCIES' => 0,
    'LABEL_TC3' => 0,
    'LABEL_TC4' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a499c3bbe9772_72521426',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a499c3bbe9772_72521426')) {function content_5a499c3bbe9772_72521426($_smarty_tpl) {?><html>
<head>
	<?php echo $_smarty_tpl->getSubTemplate ("head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

</head>
<body>

	<?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<div id="page-wrap">
<div id="conttranstotalcos">
	<div id="conttranscos">

		<div id="btranspresen">
			<div id="transimatge">			
			<img src=<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
 />			
			</div>
			<div id="transimatgeText">
				<h1>
				<?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

				</h1>
			</div>
		</div>

		<div class="subtitolinfeco" id="subtitolinfeco2">
            <?php echo $_smarty_tpl->tpl_vars['LABEL_TCSUB']->value;?>

        </div>

        <div class="marcback">
                    <a href="/ple/comissions_informatives.php">
                        <i class="icon-angle-double-up"></i>
                    </a>
        </div>

		<!-- Menu -->
                <?php echo $_smarty_tpl->getSubTemplate ("ple/comissio-informativa/menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

        <!--// Menu -->

		<div id="tcentre_planA">

		<font size="4" color="#666666"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC0']->value;?>
</font>

		<h3><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
</h3>

				<table>
				<thead>
                  <tr>
                    <th><?php echo $_smarty_tpl->tpl_vars['LABEL_FOTO']->value;?>
</th>
                    <th><?php echo $_smarty_tpl->tpl_vars['LABEL_NOM']->value;?>
</th>
                    <th><?php echo $_smarty_tpl->tpl_vars['LABEL_GRUP']->value;?>
</th>
					<th><?php echo $_smarty_tpl->tpl_vars['LABEL_CARREC']->value;?>
</th>
                    </tr>
					</thead>
					<tbody>
                    	<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['COMPOSICIO_MEMBRES']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
                    	<tr>
                        	<td><a href="<?php echo $_smarty_tpl->tpl_vars['item']->value['link'];?>
" target="<?php echo $_smarty_tpl->tpl_vars['item']->value['target'];?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['item']->value['foto'];?>
" class="avatar"/></a></td>
                        	<td><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
</td>
                        	<td><?php echo $_smarty_tpl->tpl_vars['item']->value['group'];?>
</td>
							<td><?php echo $_smarty_tpl->tpl_vars['item']->value['occupation'];?>
</td>
                   		</tr>
                    	<?php } ?>
						</tbody>
                </table>

		<h3><?php echo $_smarty_tpl->tpl_vars['LABEL_TCSEC']->value;?>
</h3>
		
		<ul>
		<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['COMPOSICIO_SECRETARIS']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
			<li><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
 - <?php echo $_smarty_tpl->tpl_vars['item']->value['occupation'];?>
</li>
		<?php } ?>
		</ul>
		
		
		<h3><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
</h3>

		<ul>
		<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['COMPETENCIES']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
			<li><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
</li>
		<?php } ?>
		</ul>

		<h3><?php echo $_smarty_tpl->tpl_vars['LABEL_TC3']->value;?>
</h3>
		<p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC4']->value;?>
</p>

		</div>
  	</div>
</div>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


</body>
</html>


<?php }} ?>
