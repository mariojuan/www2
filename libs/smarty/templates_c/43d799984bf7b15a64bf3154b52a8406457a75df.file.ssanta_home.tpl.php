<?php /* Smarty version Smarty-3.1.16, created on 2017-12-12 22:48:18
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/ssanta/ssanta_home.tpl" */ ?>
<?php /*%%SmartyHeaderCode:7096383715a304ea26a0132-65597597%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '43d799984bf7b15a64bf3154b52a8406457a75df' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/ssanta/ssanta_home.tpl',
      1 => 1499070880,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7096383715a304ea26a0132-65597597',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'image' => 0,
    'LABEL_TITOL1' => 0,
    'MENU' => 0,
    'itemMenu' => 0,
    'LABEL_TC0' => 0,
    'item' => 0,
    'LABEL_TC1' => 0,
    'LABEL_TC2' => 0,
    'LABEL_TC3' => 0,
    'LABEL_TC1B' => 0,
    'LABEL_TC2B' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a304ea2731468_19557548',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a304ea2731468_19557548')) {function content_5a304ea2731468_19557548($_smarty_tpl) {?><!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->
<div id="page-wrap">

   <div class="contenedor-responsive">

      <div id="conttranstotalcos">

         <div id="conttranscos">

            <div id="btranspresen">

               <div id="transimatge">			
                  <img src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
" class="img-slider-territori"/>			
               </div>

               <div id="transimatgeText">
                  <h1>
                     <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

                     <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                  </h1>
               </div>

            </div>

            <ul id='menu_planA'>
               <?php  $_smarty_tpl->tpl_vars['itemMenu'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['itemMenu']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['MENU']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['itemMenu']->key => $_smarty_tpl->tpl_vars['itemMenu']->value) {
$_smarty_tpl->tpl_vars['itemMenu']->_loop = true;
?>
               <a href=<?php echo $_smarty_tpl->tpl_vars['itemMenu']->value['link'];?>
 target="_<?php echo $_smarty_tpl->tpl_vars['itemMenu']->value['target'];?>
">
                  <li id='menu_planA_item'>
                     <?php echo $_smarty_tpl->tpl_vars['itemMenu']->value['name'];?>

                  </li>
               </a>
               <?php } ?>
            </ul>

            <div id="tcentre_planA">
               <h2><?php echo $_smarty_tpl->tpl_vars['LABEL_TC0']->value;?>
</h2>
               <?php if ($_smarty_tpl->tpl_vars['item']->value==1) {?>
               <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
</p>
               <div class="marcterme rectificacio-marcTerme-img-ssanta2017" style="left:120px;"><a href="http://www.tortosa.cat/webajt/ssanta/ssanta17/programa2017.pdf" target="_blank"><img src="/images/ssanta/cartell17.jpg" class="img-responsive"></a></div>
               <div class="separator"></div>
               <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
</p>
               <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC3']->value;?>
</p>
               <?php } elseif ($_smarty_tpl->tpl_vars['item']->value==2) {?>
               <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
<br>
                  <a href="http://www.tortosa.cat/webajt/gestiointerna/headlines/partpublica/indexllistaheadlines.asp?codi=5962" target="_blank">
                  <i><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1B']->value;?>
</i>
               </p>
               </a>
               <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
<br>
                  <a href="http://www.tortosa.cat/webajt/gestiointerna/headlines/partpublica/indexllistaheadlines.asp?codi=5782" target="_blank">
                  <i><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1B']->value;?>
</i>
               </p>
               </a>
               <?php } elseif ($_smarty_tpl->tpl_vars['item']->value==3) {?>
               <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
<br>
               <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
<br>
                  <a href="http://www.tortosa.cat/webajt/ajunta/consulta/index.asp" target="_blank">
                  <i><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2B']->value;?>
</i>
               </p>
               </a>
               <?php }?>
               <br><br><br>
            </div>

         </div>
      </div>
   </div>
</div><?php }} ?>
