<?php /* Smarty version Smarty-3.1.16, created on 2017-12-15 11:39:44
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/admin/templates/indicadors/edit_indicador_subgrup.tpl" */ ?>
<?php /*%%SmartyHeaderCode:17188627055a310771596c43-33443467%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '247f4f2f47d3c83d1e78b2768929430f0ec7b8e5' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/admin/templates/indicadors/edit_indicador_subgrup.tpl',
      1 => 1513240909,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '17188627055a310771596c43-33443467',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a3107716073c4_82256336',
  'variables' => 
  array (
    'item' => 0,
    'lang' => 0,
    'grups' => 0,
    'grup' => 0,
    'i' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a3107716073c4_82256336')) {function content_5a3107716073c4_82256336($_smarty_tpl) {?><section id="main" class="column">
   <h4 class="alert_info"><i class="icon-doc-new"></i><?php if ($_smarty_tpl->tpl_vars['item']->value[0]->ID=='') {?>Alta<?php } else { ?>Modificació<?php }?> Subgrup d'Indicador (<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
)</h4>
   <article class="module width_full">
      <div class="module_content">
         <form id="indicadorsGrupForm" method="post" action="indicadors_subgrup.php" novalidate="novalidate" enctype="multipart/form-data">
             <input type="hidden" name="accio" id="accio" value="edit_indicador_subgrup">
             <input type="hidden" name="lang" id="lang" value="<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
">
             <input type="hidden" name="ID" id="ID" value="<?php echo $_smarty_tpl->tpl_vars['item']->value[0]->ID;?>
">
            <?php if ($_smarty_tpl->tpl_vars['lang']->value=='ca') {?>
            <p>
                <label>Grup *</label>
                <select name="ID_GRUP" id="ID_GRUP">
                    <option value="">-- Seleccionar --</option>
                    <?php  $_smarty_tpl->tpl_vars['grup'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['grup']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['grups']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['grup']->key => $_smarty_tpl->tpl_vars['grup']->value) {
$_smarty_tpl->tpl_vars['grup']->_loop = true;
?>
                        <?php if ($_smarty_tpl->tpl_vars['grup']->value->ID==$_smarty_tpl->tpl_vars['item']->value[0]->ID_GRUP) {?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['grup']->value->ID;?>
" selected><?php echo $_smarty_tpl->tpl_vars['grup']->value->GRUP_INDICADOR;?>
</option>
                        <?php } else { ?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['grup']->value->ID;?>
"><?php echo $_smarty_tpl->tpl_vars['grup']->value->GRUP_INDICADOR;?>
</option>
                        <?php }?>
                    <?php } ?>
                </select>
            </p>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['lang']->value=='ca') {?>
            <p>
               <label>Codi *</label>
               <input type="text" name="CODI_SUBGRUP_INDICADOR" id="CODI_SUBGRUP_INDICADOR" value="<?php echo $_smarty_tpl->tpl_vars['item']->value[0]->CODI_SUBGRUP_INDICADOR;?>
">
            </p>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['lang']->value=='ca') {?>
            <p>
               <label>Tipus indicador *</label>
               <select name="TIPUS_INDICADOR" id="TIPUS_INDICADOR">
                   <option value="">-- Seleccionar --</option>
                   <option value="Infoparticipa" <?php if ($_smarty_tpl->tpl_vars['item']->value[0]->TIPUS_INDICADOR=="Infoparticipa") {?>selected<?php }?>>Infoparticipa</option>
                   <option value="ITA" <?php if ($_smarty_tpl->tpl_vars['item']->value[0]->TIPUS_INDICADOR=="ITA") {?>selected<?php }?>>ITA</option>
               </select>
            </p>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['lang']->value=='ca') {?>
            <p>
               <label>Exercici *</label>
                <select name="EXERCICI" id="EXERCICI">
                    <option value="">-- Seleccionar --</option>
                    <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? 2050+1 - (2017) : 2017-(2050)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 2017, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
                        <?php if ($_smarty_tpl->tpl_vars['i']->value==$_smarty_tpl->tpl_vars['item']->value[0]->EXERCICI) {?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" selected><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
</option>
                        <?php } else { ?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
</option>
                        <?php }?>
                    <?php }} ?>
                </select>
            </p>
            <?php }?>
             <p>
                 <label>Nom Subgrup *</label>
                 <input type="text" name="SUBGRUP_INDICADOR" id="SUBGRUP_INDICADOR" value="<?php echo $_smarty_tpl->tpl_vars['item']->value[0]->SUBGRUP_INDICADOR;?>
">
             </p>
            <?php if ($_smarty_tpl->tpl_vars['lang']->value=='ca') {?>
            <p>
                <label>Baixa</label>
                <input type="checkbox" name="BAIXA" id="BAIXA" <?php if ($_smarty_tpl->tpl_vars['item']->value[0]->BAIXA) {?>checked <?php }?>>
            </p>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['lang']->value=='ca') {?>
            <p>
                <label>Visible</label>
                <input type="checkbox" name="VISIBLE" id="VISIBLE" <?php if ($_smarty_tpl->tpl_vars['item']->value[0]->VISIBLE) {?>checked <?php }?>>
            </p>
            <?php }?>
            <p>
               <input class="submit" type="submit" value="  <?php if ($_smarty_tpl->tpl_vars['item']->value[0]->ID=='') {?>Alta<?php } else { ?>Modificació<?php }?>  ">
            </p>
            <p>
               <label>* Camps obligatoris</label>
            </p>
         </form>
      </div>
   </article>

   <!-- end of styles article -->
   <div class="spacer"></div>
</section>
<script>
    $().ready(function() {
        $("#indicadorsGrupForm").validate({
            rules: {
                ID_GRUP:  {
                    required: true
                },
                CODI_SUBGRUP_INDICADOR:  {
                    required: true
                },
                TIPUS_INDICADOR: {
                    required: true
                },
                EXERCICI: "required",
                SUBGRUP_INDICADOR: "required"
            },
            messages: {
                ID_GRUP:  {
                    required: "El camp de grup d'indicador és obligatori"
                },
                CODI_SUBGRUP_INDICADOR: {
                    required: "El camp de codi de subgrup és obligatori"
                },
                TIPUS_INDICADOR: {
                    required: "El camp de tipus d'indicador és obligatori"
                },
                EXERCICI: {
                    required: "El camp d'exercici és obligatori"
                },
                SUBGRUP_INDICADOR: {
                    required: "El camp de subgrup és obligatori"
                }
            },
            submitHandler: function(form) {
                if ($('#BAIXA').prop('checked')) {
                    $('#BAIXA').val("1");
                }
                else {
                    $('#BAIXA').val("0");
                }
                if($('#VISIBLE').prop('checked')) {
                    $('#VISIBLE').val("1");
                }
                else {
                    $('#VISIBLE').val("0");
                }
                form.submit();
            }
        });
    });
</script><?php }} ?>
