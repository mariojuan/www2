<?php /* Smarty version Smarty-3.1.16, created on 2017-12-12 16:10:21
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/noticies/llistat_noticies.tpl" */ ?>
<?php /*%%SmartyHeaderCode:6620879265a2ff15d3b5b04-11596999%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1d04508133b40bb8e85535cdfeb2b84361118a69' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/noticies/llistat_noticies.tpl',
      1 => 1497340717,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6620879265a2ff15d3b5b04-11596999',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'LABEL_TITOL1' => 0,
    'MENU' => 0,
    'itemMenu' => 0,
    'destination_form' => 0,
    'lblDataInici' => 0,
    'data_inici' => 0,
    'lblDataFi' => 0,
    'data_fi' => 0,
    'text' => 0,
    'lblCercar' => 0,
    'items' => 0,
    'item' => 0,
    'tematiques' => 0,
    'lang' => 0,
    'lblMesInfo' => 0,
    'lblNoData' => 0,
    'lastpage' => 0,
    'page' => 0,
    'page_ant' => 0,
    'lblAnt' => 0,
    'desp' => 0,
    'firstpage' => 0,
    'counter' => 0,
    'page_seg' => 0,
    'lblSeg' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a2ff15d5e7a16_12910164',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a2ff15d5e7a16_12910164')) {function content_5a2ff15d5e7a16_12910164($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/vhost/www2.tortosa.cat/home/html/libs/smarty/plugins/modifier.date_format.php';
if (!is_callable('smarty_modifier_truncate')) include '/var/www/vhost/www2.tortosa.cat/home/html/libs/smarty/plugins/modifier.truncate.php';
?>﻿<html>
    <head>
        <?php echo $_smarty_tpl->getSubTemplate ("head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    </head>
	<body id="headlines_list">
		<?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

        <div id="conttranstotalcos">
            <div id="conttranscos">
                <div id="btranspresen">
                    <div id="transimatge">
                        <img src="/images/butlletins/tira.jpg" />
                    </div>
                    <div id="transimatgeText">
                        <h1>
                            <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

                        </h1>
                    </div>
                </div>
                <ul id='menu_planA'>
                    <?php  $_smarty_tpl->tpl_vars['itemMenu'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['itemMenu']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['MENU']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['itemMenu']->key => $_smarty_tpl->tpl_vars['itemMenu']->value) {
$_smarty_tpl->tpl_vars['itemMenu']->_loop = true;
?>
                        <a href=<?php echo $_smarty_tpl->tpl_vars['itemMenu']->value;?>
>
                            <li id='menu_planA_item'>
                                <?php echo $_smarty_tpl->tpl_vars['itemMenu']->key;?>

                            </li>
                        </a>
                    <?php } ?>
                </ul>
                <div id="tcentre_planA">
                    <div class="search_box">
                        <form name="search_headlines" method="post" action="<?php echo $_smarty_tpl->tpl_vars['destination_form']->value;?>
">
                            <div class="item_form_container di_container">
                                <label><?php echo $_smarty_tpl->tpl_vars['lblDataInici']->value;?>
</label>
                                <input type="text" name="data_inici" class="search_di" value="<?php echo $_smarty_tpl->tpl_vars['data_inici']->value;?>
">
                            </div>
                            <div class="item_form_container df_container">
                                <label><?php echo $_smarty_tpl->tpl_vars['lblDataFi']->value;?>
</label>
                                <input type="text" name="data_fi" class="search_df" value="<?php echo $_smarty_tpl->tpl_vars['data_fi']->value;?>
">
                            </div>
                            <div class="item_form_container text_container">
                                <input type="text" name="text" class="sear_text" value="<?php echo $_smarty_tpl->tpl_vars['text']->value;?>
">
                            </div>
                            <div class="item_form_container submit_container">
                                <input type="submit" name="search_submit" value="<?php echo $_smarty_tpl->tpl_vars['lblCercar']->value;?>
">
                            </div>
                        </form>
                    </div>
                    <?php if (count($_smarty_tpl->tpl_vars['items']->value)>0) {?>
                        <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
                            <div class="headline_item_list">
                                <div class="headline_item_img">
                                <?php if (trim($_smarty_tpl->tpl_vars['item']->value->FOTO)=='') {?>
                                    <img src="/images/headlines/inotimage.jpg" width="220" height="125">
                                <?php } else { ?>
                                    <?php if ($_smarty_tpl->tpl_vars['item']->value->IMPORT=="0") {?>
                                        <img src="/images/headlines/min_<?php echo $_smarty_tpl->tpl_vars['item']->value->FOTO;?>
" width="220" height="125">
                                    <?php } else { ?>
                                        <img src="http://www.tortosa.cat/webajt/gestiointerna/headlines/fotos/<?php echo $_smarty_tpl->tpl_vars['item']->value->FOTO;?>
" width="220" height="125">
                                    <?php }?>
                                <?php }?>
                                </div>
                                <div class="headline_item_data"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['item']->value->DATA,"%d/%m/%G");?>
</div>
                                <div class="headline_title_item"><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['item']->value->TITOL,75,'',false);?>
</div>
                                <div class="headline_subtitle_item"><span><?php echo $_smarty_tpl->tpl_vars['tematiques']->value[$_smarty_tpl->tpl_vars['item']->value->ID];?>
.</span> <?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['item']->value->SUBTITOL,100,"...",false);?>
</div>
                                <div class="headline_link_item"><a href="/noticies/noticia.php?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
&id=<?php echo $_smarty_tpl->tpl_vars['item']->value->ID;?>
"><?php echo $_smarty_tpl->tpl_vars['lblMesInfo']->value;?>
</a></div>
                            </div>
                        <?php } ?>
                    <?php } else { ?>
                        <div class="headline_no_data">
                            <?php echo $_smarty_tpl->tpl_vars['lblNoData']->value;?>

                        </div>
                    <?php }?>
                    <div id="pagination">
                        <ul>
                            <?php if ($_smarty_tpl->tpl_vars['lastpage']->value>1) {?>
                                <?php if ($_smarty_tpl->tpl_vars['page']->value>1) {?>
                                    <?php $_smarty_tpl->tpl_vars["page_ant"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value-1, null, 0);?>
                                    <li><a href="index.php?accio=list&page=<?php echo $_smarty_tpl->tpl_vars['page_ant']->value;?>
&data_inici=<?php echo $_smarty_tpl->tpl_vars['data_inici']->value;?>
&data_fi=<?php echo $_smarty_tpl->tpl_vars['data_fi']->value;?>
&text=<?php echo $_smarty_tpl->tpl_vars['text']->value;?>
&lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['lblAnt']->value;?>
</a></li>
                                <?php }?>
                                <?php $_smarty_tpl->tpl_vars["desp"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value+4, null, 0);?>
                                <?php $_smarty_tpl->tpl_vars["firstpage"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value-4, null, 0);?>

                                <?php if ($_smarty_tpl->tpl_vars['desp']->value<$_smarty_tpl->tpl_vars['lastpage']->value) {?>
                                    <?php $_smarty_tpl->tpl_vars["lastpage"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value+4, null, 0);?>
                                <?php }?>
                                <?php if ($_smarty_tpl->tpl_vars['firstpage']->value<1) {?>
                                    <?php $_smarty_tpl->tpl_vars["firstpage"] = new Smarty_variable(1, null, 0);?>
                                <?php }?>

                                <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['counter']->step = 1;$_smarty_tpl->tpl_vars['counter']->total = (int) ceil(($_smarty_tpl->tpl_vars['counter']->step > 0 ? $_smarty_tpl->tpl_vars['lastpage']->value+1 - ($_smarty_tpl->tpl_vars['firstpage']->value) : $_smarty_tpl->tpl_vars['firstpage']->value-($_smarty_tpl->tpl_vars['lastpage']->value)+1)/abs($_smarty_tpl->tpl_vars['counter']->step));
if ($_smarty_tpl->tpl_vars['counter']->total > 0) {
for ($_smarty_tpl->tpl_vars['counter']->value = $_smarty_tpl->tpl_vars['firstpage']->value, $_smarty_tpl->tpl_vars['counter']->iteration = 1;$_smarty_tpl->tpl_vars['counter']->iteration <= $_smarty_tpl->tpl_vars['counter']->total;$_smarty_tpl->tpl_vars['counter']->value += $_smarty_tpl->tpl_vars['counter']->step, $_smarty_tpl->tpl_vars['counter']->iteration++) {
$_smarty_tpl->tpl_vars['counter']->first = $_smarty_tpl->tpl_vars['counter']->iteration == 1;$_smarty_tpl->tpl_vars['counter']->last = $_smarty_tpl->tpl_vars['counter']->iteration == $_smarty_tpl->tpl_vars['counter']->total;?>
                                    <?php if ($_smarty_tpl->tpl_vars['counter']->value==$_smarty_tpl->tpl_vars['page']->value) {?>
                                        <li><?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
</li>
                                    <?php } else { ?>
                                        <li><a href="index.php?accio=list&page=<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
&data_inici=<?php echo $_smarty_tpl->tpl_vars['data_inici']->value;?>
&data_fi=<?php echo $_smarty_tpl->tpl_vars['data_fi']->value;?>
&text=<?php echo $_smarty_tpl->tpl_vars['text']->value;?>
&lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
</a></li>
                                    <?php }?>
                                <?php }} ?>
                                <?php if ($_smarty_tpl->tpl_vars['page']->value<$_smarty_tpl->tpl_vars['lastpage']->value-1) {?>
                                    <?php $_smarty_tpl->tpl_vars["page_seg"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value+1, null, 0);?>
                                    <li><a href="index.php?accio=list&page=<?php echo $_smarty_tpl->tpl_vars['page_seg']->value;?>
&data_inici=<?php echo $_smarty_tpl->tpl_vars['data_inici']->value;?>
&data_fi=<?php echo $_smarty_tpl->tpl_vars['data_fi']->value;?>
&text=<?php echo $_smarty_tpl->tpl_vars['text']->value;?>
&lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['lblSeg']->value;?>
</a></li>
                                <?php }?>
                            <?php }?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $().ready(function() {
                $(".search_di").datepicker();
                $(".search_df").datepicker();
            });
        </script>
		<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

	</body>
</html>
<?php }} ?>
