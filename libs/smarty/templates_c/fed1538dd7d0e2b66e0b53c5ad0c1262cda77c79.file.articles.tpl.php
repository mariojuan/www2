<?php /* Smarty version Smarty-3.1.16, created on 2017-12-13 02:29:34
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/grups-politics/grup-politic/articles.tpl" */ ?>
<?php /*%%SmartyHeaderCode:4372645375a30827e0c76b6-25552488%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fed1538dd7d0e2b66e0b53c5ad0c1262cda77c79' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/grups-politics/grup-politic/articles.tpl',
      1 => 1499083229,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4372645375a30827e0c76b6-25552488',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'tiraPartit' => 0,
    'LABEL_TITOL1' => 0,
    'LABEL_TITOL1b' => 0,
    'lang' => 0,
    'LABEL_ARTICLES' => 0,
    'LABEL_noArticles' => 0,
    'items' => 0,
    'item' => 0,
    'tematiques' => 0,
    'GRUP_POLITIC' => 0,
    'lblNoData' => 0,
    'lastpage' => 0,
    'page' => 0,
    'page_ant' => 0,
    'lblAnt' => 0,
    'desp' => 0,
    'firstpage' => 0,
    'counter' => 0,
    'page_seg' => 0,
    'lblSeg' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a30827e1c09a2_73660392',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a30827e1c09a2_73660392')) {function content_5a30827e1c09a2_73660392($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/vhost/www2.tortosa.cat/home/html/libs/smarty/plugins/modifier.date_format.php';
if (!is_callable('smarty_modifier_truncate')) include '/var/www/vhost/www2.tortosa.cat/home/html/libs/smarty/plugins/modifier.truncate.php';
?><html>
   <head>
      <?php echo $_smarty_tpl->getSubTemplate ("head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

      <link href="/css/estils_headlines.css" rel="stylesheet" type="text/css">
   </head>
   <body id="headlines_list">
      <?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

      <div id="page-wrap">
         <div class="contenedor-responsive">
            <div id="conttranstotalcos">
               <div id="conttranscos">
                  <div id="btranspresen">
                     <div id="transimatge"> 
                        <img src="<?php echo $_smarty_tpl->tpl_vars['tiraPartit']->value;?>
" class="img-slider-territori"/>         
                     </div>
                     <div id="transimatgeText">
                        <h1>
                           <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

                           <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                        </h1>
                     </div>
                  </div>
                  <div class="subtitolinfeco" id="subtitolinfeco2">
                     <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1b']->value;?>

                  </div>
                  <div class="marcback">
                     <a href="/grups-politics/index.php?<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
">
                     <i class="icon-angle-double-up"></i>
                     </a>
                  </div>
                  <!-- Menu -->
                  <?php echo $_smarty_tpl->getSubTemplate ("grups-politics/grup-politic/menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

                  <!--// Menu -->
                  <!-- Taula compos -->
                  <div id="tcentre_planA" style="margin-left: -36px; margin-top: -6px;" class="correcio-grupPolitic-Articles">
                     <font size="4" color="#666666"><?php echo $_smarty_tpl->tpl_vars['LABEL_ARTICLES']->value;?>
</font>
                     <p><?php echo $_smarty_tpl->tpl_vars['LABEL_noArticles']->value;?>
</p>
                     <?php if (count($_smarty_tpl->tpl_vars['items']->value)>0) {?>
                     <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
                     <div class="headline_item_list">
                        <div class="headline_item_img">
                           <?php if (trim($_smarty_tpl->tpl_vars['item']->value->FOTO)=='') {?>
                           <img src="/images/headlines/inotimage.jpg" width="220" height="125">
                           <?php } else { ?>
                           <?php if ($_smarty_tpl->tpl_vars['item']->value->IMPORT=="0") {?>
                           <img src="/images/headlines/min_<?php echo $_smarty_tpl->tpl_vars['item']->value->FOTO;?>
" width="220" height="125">
                           <?php } else { ?>
                           <img src="http://www.tortosa.cat/webajt/gestiointerna/grups_municipals/documentacio/<?php echo $_smarty_tpl->tpl_vars['item']->value->FOTO;?>
" width="220" height="125">
                           <?php }?>
                           <?php }?>
                        </div>
                        <div class="headline_item_data"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['item']->value->DATA,"%d/%m/%G");?>
</div>
                        <div class="headline_title_item"><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['item']->value->TITOL,75,'',false);?>
</div>
                        <div class="headline_subtitle_item"><span><?php echo $_smarty_tpl->tpl_vars['tematiques']->value[$_smarty_tpl->tpl_vars['item']->value->ID];?>
</span> <?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['item']->value->SUBTITOL,100,"...",false);?>
</div>
                        <div class="headline_link_item"><a href="/grups-politics/article.php?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
&id=<?php echo $_smarty_tpl->tpl_vars['item']->value->ID;?>
&grup=<?php echo $_smarty_tpl->tpl_vars['GRUP_POLITIC']->value;?>
">més informació</a></div>
                        <p><br></p>
                     </div>
                     <?php } ?>
                     <?php } else { ?>
                     <div class="headline_no_data">
                        <?php echo $_smarty_tpl->tpl_vars['lblNoData']->value;?>

                     </div>
                     <?php }?>
                     <div id="pagination">
                        <ul>
                           <?php if ($_smarty_tpl->tpl_vars['lastpage']->value>1) {?>
                           <?php if ($_smarty_tpl->tpl_vars['page']->value>1) {?>
                           <?php $_smarty_tpl->tpl_vars["page_ant"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value-1, null, 0);?>
                           <li><a href="/grups-politics/<?php echo $_smarty_tpl->tpl_vars['GRUP_POLITIC']->value;?>
/articles.php?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
&page=<?php echo $_smarty_tpl->tpl_vars['page_ant']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['lblAnt']->value;?>
</a></li>
                           <?php }?>
                           <?php $_smarty_tpl->tpl_vars["desp"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value+4, null, 0);?>
                           <?php $_smarty_tpl->tpl_vars["firstpage"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value-4, null, 0);?>
                           <?php if ($_smarty_tpl->tpl_vars['desp']->value<$_smarty_tpl->tpl_vars['lastpage']->value) {?>
                           <?php $_smarty_tpl->tpl_vars["lastpage"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value+4, null, 0);?>
                           <?php }?>
                           <?php if ($_smarty_tpl->tpl_vars['firstpage']->value<1) {?>
                           <?php $_smarty_tpl->tpl_vars["firstpage"] = new Smarty_variable(1, null, 0);?>
                           <?php }?>
                           <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['counter']->step = 1;$_smarty_tpl->tpl_vars['counter']->total = (int) ceil(($_smarty_tpl->tpl_vars['counter']->step > 0 ? $_smarty_tpl->tpl_vars['lastpage']->value+1 - ($_smarty_tpl->tpl_vars['firstpage']->value) : $_smarty_tpl->tpl_vars['firstpage']->value-($_smarty_tpl->tpl_vars['lastpage']->value)+1)/abs($_smarty_tpl->tpl_vars['counter']->step));
if ($_smarty_tpl->tpl_vars['counter']->total > 0) {
for ($_smarty_tpl->tpl_vars['counter']->value = $_smarty_tpl->tpl_vars['firstpage']->value, $_smarty_tpl->tpl_vars['counter']->iteration = 1;$_smarty_tpl->tpl_vars['counter']->iteration <= $_smarty_tpl->tpl_vars['counter']->total;$_smarty_tpl->tpl_vars['counter']->value += $_smarty_tpl->tpl_vars['counter']->step, $_smarty_tpl->tpl_vars['counter']->iteration++) {
$_smarty_tpl->tpl_vars['counter']->first = $_smarty_tpl->tpl_vars['counter']->iteration == 1;$_smarty_tpl->tpl_vars['counter']->last = $_smarty_tpl->tpl_vars['counter']->iteration == $_smarty_tpl->tpl_vars['counter']->total;?>
                           <?php if ($_smarty_tpl->tpl_vars['counter']->value==$_smarty_tpl->tpl_vars['page']->value) {?>
                           <li><?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
</li>
                           <?php } else { ?>
                           <li><a href="/grups-politics/<?php echo $_smarty_tpl->tpl_vars['GRUP_POLITIC']->value;?>
/articles.php?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
&page=<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
</a></li>
                           <?php }?>
                           <?php }} ?>
                           <?php if ($_smarty_tpl->tpl_vars['page']->value<$_smarty_tpl->tpl_vars['lastpage']->value-1) {?>
                           <?php $_smarty_tpl->tpl_vars["page_seg"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value+1, null, 0);?>
                           <li><a href="/grups-politics/<?php echo $_smarty_tpl->tpl_vars['GRUP_POLITIC']->value;?>
/articles.php?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
&page=<?php echo $_smarty_tpl->tpl_vars['page_seg']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['lblSeg']->value;?>
</a></li>
                           <?php }?>
                           <?php }?>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      </div>
      <script>
         $().ready(function() {
             $(".search_di").datepicker();
             $(".search_df").datepicker();
         });
      </script>
      <?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

   </body>
</html><?php }} ?>
