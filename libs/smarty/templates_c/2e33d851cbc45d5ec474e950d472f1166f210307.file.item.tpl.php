<?php /* Smarty version Smarty-3.1.16, created on 2017-12-19 11:41:25
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/ordenances/item.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3772335075a38ecd544d131-54101708%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2e33d851cbc45d5ec474e950d472f1166f210307' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/ordenances/item.tpl',
      1 => 1496062051,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3772335075a38ecd544d131-54101708',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'image' => 0,
    'LABEL_TITOL_FISCAL' => 0,
    'MENU_FISCAL' => 0,
    'itemMenu' => 0,
    'lang' => 0,
    'LABEL_TITOL1' => 0,
    'lblCodiOrdenansa' => 0,
    'item' => 0,
    'lblDescripcio' => 0,
    'lblEnsGestor' => 0,
    'lblDataEntradaVigor' => 0,
    'lblOrganAprovacio' => 0,
    'lblDataAprovacio' => 0,
    'lblDataPublicacioAcordProv' => 0,
    'lblButlletiPublicacioAcordProv' => 0,
    'lblDataPublicacioAcordDef' => 0,
    'lblButlletiPublicacioAcordDef' => 0,
    'lblBack' => 0,
    'tipus' => 0,
    'page' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a38ecd553e662_83924036',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a38ecd553e662_83924036')) {function content_5a38ecd553e662_83924036($_smarty_tpl) {?><!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->

<div id="page-wrap">
<div id="conttranstotalcos">
    <div id="conttranscos">
        <div id="btranspresen">
            <div id="transimatge">          
            <img src=<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
 />            
            </div>
            <div id="transimatgeText">
                <h1>
                <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL_FISCAL']->value;?>

                </h1>
            </div>
        </div>
        <ul id='menu_planA'>
        <?php  $_smarty_tpl->tpl_vars['itemMenu'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['itemMenu']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['MENU_FISCAL']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['itemMenu']->key => $_smarty_tpl->tpl_vars['itemMenu']->value) {
$_smarty_tpl->tpl_vars['itemMenu']->_loop = true;
?>
            <a href=<?php echo $_smarty_tpl->tpl_vars['itemMenu']->value;?>
&lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
>
            <li id='menu_planA_item'>
                <?php echo $_smarty_tpl->tpl_vars['itemMenu']->key;?>

            </li>
            </a>
        <?php } ?>
        </ul>
        
        <div id="tcentre_planA">
            <div id="textintroinfeco" class="memoria-democratica">                  
                <h2><?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>
</h2>

                <table id="item" name="item">
                    <tr>
                        <td><?php echo $_smarty_tpl->tpl_vars['lblCodiOrdenansa']->value;?>
:</td>
                        <td><?php echo utf8_encode($_smarty_tpl->tpl_vars['item']->value->Codi_ord1);?>
 - <?php echo utf8_encode($_smarty_tpl->tpl_vars['item']->value->codi_ord);?>
</td>
                    </tr>
                    <tr>
                        <td><?php echo $_smarty_tpl->tpl_vars['lblDescripcio']->value;?>
:</td>
                        <td><?php echo utf8_encode($_smarty_tpl->tpl_vars['item']->value->descripcio_curta);?>
</td>
                    </tr>
                    <tr>
                        <td><?php echo $_smarty_tpl->tpl_vars['lblEnsGestor']->value;?>
:</td>
                        <td><?php echo utf8_encode($_smarty_tpl->tpl_vars['item']->value->servei_OA);?>
</td>
                    </tr>
                    <tr>
                        <td><?php echo $_smarty_tpl->tpl_vars['lblDataEntradaVigor']->value;?>
:</td>
                        <td><?php echo utf8_encode($_smarty_tpl->tpl_vars['item']->value->data_entrada_vigor);?>
</td>
                    </tr>
                    <tr>
                        <td><?php echo $_smarty_tpl->tpl_vars['lblOrganAprovacio']->value;?>
:</td>
                        <td><?php echo utf8_encode($_smarty_tpl->tpl_vars['item']->value->organ_aprovacio);?>
</td>
                    </tr>
                    <tr>
                        <td><?php echo $_smarty_tpl->tpl_vars['lblDataAprovacio']->value;?>
:</td>
                        <td><?php echo utf8_encode($_smarty_tpl->tpl_vars['item']->value->data_versio_aprovacio);?>
</td>
                    </tr>
                    <tr>
                        <td><?php echo $_smarty_tpl->tpl_vars['lblDataPublicacioAcordProv']->value;?>
:</td>
                        <td><?php echo utf8_encode($_smarty_tpl->tpl_vars['item']->value->data_publicacio_acord_prov);?>
</td>
                    </tr>
                    <tr>
                        <td><?php echo $_smarty_tpl->tpl_vars['lblButlletiPublicacioAcordProv']->value;?>
:</td>
                        <td><?php echo utf8_encode($_smarty_tpl->tpl_vars['item']->value->butlleti_publicacio_acord_prov);?>
</td>
                    </tr>
                    <tr>
                        <td><?php echo $_smarty_tpl->tpl_vars['lblDataPublicacioAcordDef']->value;?>
:</td>
                        <td><?php echo utf8_encode($_smarty_tpl->tpl_vars['item']->value->data_publicacio_acord_def);?>
</td>
                    </tr>
                    <tr>
                        <td><?php echo $_smarty_tpl->tpl_vars['lblButlletiPublicacioAcordDef']->value;?>
:</td>
                        <td><?php echo utf8_encode($_smarty_tpl->tpl_vars['item']->value->butlleti_publicacio_acord_def);?>
</td>
                    </tr>
                </table>

                <div id="buttons-container">
                    <input type="button" name="enrere" id="enrere" value="<?php echo $_smarty_tpl->tpl_vars['lblBack']->value;?>
" onclick="JavaScript:window.location.href='/ordenances/index.php?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
&tipus=<?php echo $_smarty_tpl->tpl_vars['tipus']->value;?>
&accio=list&page=<?php echo $_smarty_tpl->tpl_vars['page']->value;?>
';">
                </div>


            </div>
        </div>
    </div>
</div>
</div>
<?php }} ?>
