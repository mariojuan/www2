<?php /* Smarty version Smarty-3.1.16, created on 2017-12-12 20:52:30
         compiled from "templates/footer.tpl" */ ?>
<?php /*%%SmartyHeaderCode:4332368035a30337e335ad8-91565822%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9206ffe216f3f7c2e7655782292928f7d20e8be5' => 
    array (
      0 => 'templates/footer.tpl',
      1 => 1497942645,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4332368035a30337e335ad8-91565822',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'lang' => 0,
    'ca' => 0,
    'LABEL_POLPRIV' => 0,
    'LABEL_AVISL' => 0,
    'LABEL_COP' => 0,
    'LABEL_WEB_OPTIMITZADA' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a30337e3433f7_78341308',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a30337e3433f7_78341308')) {function content_5a30337e3433f7_78341308($_smarty_tpl) {?><div id="footer">
	<div id="contenidortotalpeu">
		<div id="contenidorpeu">
			<div id="privacitat">
				<a href="/docs/<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
_pprivacitat.pdf" hreflang="<?php echo $_smarty_tpl->tpl_vars['ca']->value;?>
" target="_target">
				<?php echo $_smarty_tpl->tpl_vars['LABEL_POLPRIV']->value;?>

				</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="/docs/<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
_alegal.pdf" hreflang="<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
" target="_blank">
				<?php echo $_smarty_tpl->tpl_vars['LABEL_AVISL']->value;?>

				</a>
			</div>
			<div id="credencials">
				<p>&copy; 
				<?php echo $_smarty_tpl->tpl_vars['LABEL_COP']->value;?>

				</p>
				<p>Pl. d'Espanya, 1 | 43500 Tortosa | Telf. 977 58 58 00 |
                    <a href="https://www.facebook.com/ajuntamentdetortosa/" target="_blank" title="facebook">
                        <i class="icon-facebook-official"></i>
                    </a>
                    <a href="https://twitter.com/Tortosa?ref_src=twsrc%5Etfw" target="_blank" title="twitter">
                        <i class="icon-twitter"></i>
                    </a>
                </p>
			</div>
            <div class="optimitzacio-navegadors">
                <?php echo $_smarty_tpl->tpl_vars['LABEL_WEB_OPTIMITZADA']->value;?>

            </div>
		</div>
	</div>
</div><?php }} ?>
