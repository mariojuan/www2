<?php /* Smarty version Smarty-3.1.16, created on 2017-12-12 15:10:50
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/planB.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1378240725a2fe36ab1aa64-96888099%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '82193fcffe282f28a88c92728b115589c4eb0788' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/planB.tpl',
      1 => 1498751311,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1378240725a2fe36ab1aa64-96888099',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'image' => 0,
    'LABEL_TITOL1' => 0,
    'LABEL_TXTINTRO' => 0,
    'menu_dinamic' => 0,
    'itemMenu' => 0,
    'icon' => 0,
    'subitemMenu' => 0,
    'MENU' => 0,
    'dataitemMenu' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a2fe36abe5911_42028571',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a2fe36abe5911_42028571')) {function content_5a2fe36abe5911_42028571($_smarty_tpl) {?><!-- PLANTILLA AMB NOMÉS UN MENÚ AL CENTRE. EL MENÚ ÉS TIPUS ACORDEÓ -->
<div id="page-wrap">

<div class="contenedor-responsive">

<div id="conttranstotalcos">
	<div id="conttranscos">
		<div id="btranspresen">
			<div id="transimatge">			
				<img src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
" class="img-slider-territori">			
			</div>
			<div id="transimatgeText">
				<h1>
				<?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

				</h1>
			</div>
		</div>
		<div id="transIntroText">
			<?php echo $_smarty_tpl->tpl_vars['LABEL_TXTINTRO']->value;?>

		</div>
		<ul id='menu' class='ulmenu'>
        <?php if (count($_smarty_tpl->tpl_vars['menu_dinamic']->value)>0) {?>
            <?php  $_smarty_tpl->tpl_vars['itemMenu'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['itemMenu']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['menu_dinamic']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['itemMenu']->key => $_smarty_tpl->tpl_vars['itemMenu']->value) {
$_smarty_tpl->tpl_vars['itemMenu']->_loop = true;
?>
                <li id='lititol'>
                    <a href='<?php if (count($_smarty_tpl->tpl_vars['itemMenu']->value)==0) {?><?php echo $_smarty_tpl->tpl_vars['itemMenu']->value;?>
<?php }?>'>
                        <i class='<?php echo $_smarty_tpl->tpl_vars['icon']->value;?>
'></i>
                        <?php echo $_smarty_tpl->tpl_vars['itemMenu']->key;?>

                    </a>
                    <ul class='ulmenusub'>
                    <?php  $_smarty_tpl->tpl_vars['subitemMenu'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['subitemMenu']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['itemMenu']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['subitemMenu']->key => $_smarty_tpl->tpl_vars['subitemMenu']->value) {
$_smarty_tpl->tpl_vars['subitemMenu']->_loop = true;
?>
                        <li id='lisubtitol'>
                            <a href='<?php echo $_smarty_tpl->tpl_vars['subitemMenu']->value[1];?>
'>
                                <?php echo $_smarty_tpl->tpl_vars['subitemMenu']->value[0];?>

                            </a>
                        </li>
                    <?php } ?>
                    </ul>
                </li>
            <?php } ?>
        <?php } else { ?>
            <?php  $_smarty_tpl->tpl_vars['itemMenu'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['itemMenu']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['MENU']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['itemMenu']->key => $_smarty_tpl->tpl_vars['itemMenu']->value) {
$_smarty_tpl->tpl_vars['itemMenu']->_loop = true;
?>
				<?php  $_smarty_tpl->tpl_vars['dataitemMenu'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['dataitemMenu']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['itemMenu']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['dataitemMenu']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['dataitemMenu']->key => $_smarty_tpl->tpl_vars['dataitemMenu']->value) {
$_smarty_tpl->tpl_vars['dataitemMenu']->_loop = true;
 $_smarty_tpl->tpl_vars['dataitemMenu']->index++;
?>
					<?php if ($_smarty_tpl->tpl_vars['dataitemMenu']->index>0) {?>
						<li id='lisubtitol'>
						<a href=<?php echo $_smarty_tpl->tpl_vars['dataitemMenu']->value;?>
>
						<?php echo $_smarty_tpl->tpl_vars['dataitemMenu']->key;?>

						</a>
						</li>
					<?php } else { ?>
						<li id='lititol'>
						<a href='#'>
						<i class='<?php echo $_smarty_tpl->tpl_vars['icon']->value;?>
'></i>
						<?php echo $_smarty_tpl->tpl_vars['dataitemMenu']->key;?>

						</a>
						<ul class='ulmenusub'>
					<?php }?>
				<?php } ?>
				</ul>
				</li>	
		    <?php } ?>
        <?php }?>
		</ul>
  	</div>
  	</div>
</div>
</div>

<script type="text/javascript" charset="utf-8">
$(function(){
	$('#menu li a').click(function(event){
		var elem = $(this).next();
		if(elem.is('ul')){
			event.preventDefault();
			$('#menu ul:visible').not(elem).slideUp();
			$('#menu li:visible').not(elem.parent("li")).css("background-image", "url(/images/ptranspa/fonsboto1.jpg)");
			elem.slideToggle();
			tipusfons=$(elem.parent('li')).css("background-image");
			fons="url(/images/ptranspa/fonsboto1.jpg)";
			if(tipusfons==fons){
				elem.parent("li").css("background-image", "url(/images/ptranspa/fonsboto2.jpg)");
			}
			else{
				elem.parent("li").css("background-image", "url(/images/ptranspa/fonsboto1.jpg)");
			}
		}
	});

});
</script> 
<?php }} ?>
