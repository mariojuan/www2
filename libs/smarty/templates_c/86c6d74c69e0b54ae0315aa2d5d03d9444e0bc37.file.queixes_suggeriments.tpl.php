<?php /* Smarty version Smarty-3.1.16, created on 2017-12-12 16:10:21
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/pciutadana/queixes_suggeriments.tpl" */ ?>
<?php /*%%SmartyHeaderCode:9653819485a2ff15d4d19c0-86033921%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '86c6d74c69e0b54ae0315aa2d5d03d9444e0bc37' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/pciutadana/queixes_suggeriments.tpl',
      1 => 1511949074,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '9653819485a2ff15d4d19c0-86033921',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'image' => 0,
    'LABEL_TITOL1' => 0,
    'LABEL_TITOL1b' => 0,
    'url_sistemes_back' => 0,
    'MENU' => 0,
    'itemMenu' => 0,
    'LABEL_TITOL0' => 0,
    'LABEL_TITOL5' => 0,
    'LABEL_TITOL5_2' => 0,
    'LABEL_TITOL5_1' => 0,
    'request' => 0,
    'lang' => 0,
    'LABEL_TITOL2' => 0,
    'LABEL_NOM' => 0,
    'LABEL_COGNOM1' => 0,
    'LABEL_COGNOM2' => 0,
    'LABEL_RAO_SOCIAL' => 0,
    'LABEL_TIPUS_DOCUMENT' => 0,
    'OPTION_TIPUS_DOCUMENT_1' => 0,
    'LABEL_NUMERO_DOCUMENT' => 0,
    'LABEL_TIPUS_VIA' => 0,
    'LABEL_NOM_VIA' => 0,
    'LABEL_NUMERO' => 0,
    'LABEL_LLETRA' => 0,
    'LABEL_KM' => 0,
    'LABEL_BLOC' => 0,
    'LABEL_ESCALA' => 0,
    'LABEL_PIS' => 0,
    'LABEL_PORTA' => 0,
    'LABEL_NUCLI_BARRI' => 0,
    'LABEL_PROVINCIA' => 0,
    'LABEL_MUNICIPI' => 0,
    'LABEL_CODI_POSTAL' => 0,
    'LABEL_TELEFON_FIX' => 0,
    'LABEL_TELEFON_MOBIL' => 0,
    'LABEL_EMAIL' => 0,
    'LABEL_TITOL3' => 0,
    'LABEL_TIPUS_PETICIO' => 0,
    'OPTION_TIPUS_PETICIO_1' => 0,
    'OPTION_TIPUS_PETICIO_2' => 0,
    'OPTION_TIPUS_PETICIO_3' => 0,
    'LABEL_AFECTA_A' => 0,
    'OPTION_AFECTA_A_1' => 0,
    'OPTION_AFECTA_A_2' => 0,
    'OPTION_AFECTA_A_3' => 0,
    'OPTION_AFECTA_A_4' => 0,
    'OPTION_AFECTA_A_5' => 0,
    'OPTION_AFECTA_A_6' => 0,
    'LABEL_DESCRIPCIO' => 0,
    'LABEL_ERROR_15' => 0,
    'BUTTON_SUBMIT' => 0,
    'LABEL_LOPD' => 0,
    'LABEL_RESPOSTA' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a2ff15d596be1_61066168',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a2ff15d596be1_61066168')) {function content_5a2ff15d596be1_61066168($_smarty_tpl) {?><div id="page-wrap">
   <div class="contenedor-responsive">
      <div id="conttranstotalcos">
         <div id="conttranscos">
            <div id="btranspresen">
               <div id="transimatge">        
                  <img src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
" class="img-slider-territori"/>       
               </div>
               <div id="transimatgeText">
                  <h1>
                     <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

                     <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                  </h1>
               </div>
            </div>
            <div class="subtitolinfeco" id="subtitolinfeco2">
               <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1b']->value;?>

            </div>
            <div class="marcback">
               <a href="<?php echo $_smarty_tpl->tpl_vars['url_sistemes_back']->value;?>
">
               <i class="icon-angle-double-up"></i>
               </a>
            </div>
            <ul id='menu_planA'>
               <?php  $_smarty_tpl->tpl_vars['itemMenu'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['itemMenu']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['MENU']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['itemMenu']->key => $_smarty_tpl->tpl_vars['itemMenu']->value) {
$_smarty_tpl->tpl_vars['itemMenu']->_loop = true;
?>
               <a href=<?php echo $_smarty_tpl->tpl_vars['itemMenu']->value;?>
>
                  <li id='menu_planA_item'>
                     <?php echo $_smarty_tpl->tpl_vars['itemMenu']->key;?>

                  </li>
               </a>
               <?php } ?>
            </ul>
            <div id="tcentre_planA" class="queixes_i_suggeriments">
               <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL0']->value;?>
</p>
               <div>
                   <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL5']->value;?>
<a href="<?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL5_2']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL5_1']->value;?>
</a>.</p>
               <?php if (!$_smarty_tpl->tpl_vars['request']->value) {?>
               <form id="queixes_i_suggeriments_form" method="post" action="queixes_suggeriments.php?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
" novalidate="novalidate" enctype="multipart/form-data">
                   <div class="row_full title">
                       <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL2']->value;?>

                   </div>
                   <div class="field_container">
                       <label><?php echo $_smarty_tpl->tpl_vars['LABEL_NOM']->value;?>
:*</label>
                       <input type="text" name="nom_sollicitant" id="nom_sollicitant" placeholder="<?php echo $_smarty_tpl->tpl_vars['LABEL_NOM']->value;?>
" aria-required="true">
                   </div>
                   <div class="field_container">
                       <label><?php echo $_smarty_tpl->tpl_vars['LABEL_COGNOM1']->value;?>
:*</label>
                       <input type="text" name="cognom1_sollicitant" id="cognom1_sollicitant" placeholder="<?php echo $_smarty_tpl->tpl_vars['LABEL_COGNOM1']->value;?>
" aria-required="true">
                   </div>
                   <div class="field_container">
                       <label><?php echo $_smarty_tpl->tpl_vars['LABEL_COGNOM2']->value;?>
:</label>
                       <input type="text" name="cognom2_sollicitant" id="cognom2_sollicitant" placeholder="<?php echo $_smarty_tpl->tpl_vars['LABEL_COGNOM2']->value;?>
">
                   </div>
                   <div class="field_container full">
                       <label><?php echo $_smarty_tpl->tpl_vars['LABEL_RAO_SOCIAL']->value;?>
:</label>
                       <input type="text" name="rao_social_sollicitant" id="rao_social_sollicitant" placeholder="<?php echo $_smarty_tpl->tpl_vars['LABEL_RAO_SOCIAL']->value;?>
">
                   </div>
                   <div class="new_row"></div>
                   <div class="field_container">
                       <label><?php echo $_smarty_tpl->tpl_vars['LABEL_TIPUS_DOCUMENT']->value;?>
:*</label>
                       <select name="tipus_document_sollicitant" id="tipus_document_sollicitant">
                           <option name=""></option>
                           <option name="DNI">DNI</option>
                           <option name="NIF">NIF</option>
                           <option name="Passaport"><?php echo $_smarty_tpl->tpl_vars['OPTION_TIPUS_DOCUMENT_1']->value;?>
</option>
                       </select>
                   </div>
                   <div class="field_container">
                       <label><?php echo $_smarty_tpl->tpl_vars['LABEL_NUMERO_DOCUMENT']->value;?>
:*</label>
                       <input type="text" name="numero_document_sollicitant" id="numero_document_sollicitant" placeholder="<?php echo $_smarty_tpl->tpl_vars['LABEL_COGNOM2']->value;?>
">
                   </div>
                   <div class="new_row"></div>
                   <div class="field_container">
                       <label class="tipus_via_sollicitant"><?php echo $_smarty_tpl->tpl_vars['LABEL_TIPUS_VIA']->value;?>
:*</label>
                       <input type="text" name="tipus_via_sollicitant" id="tipus_via_sollicitant" class="tipus_via_sollicitant" placeholder="<?php echo $_smarty_tpl->tpl_vars['LABEL_TIPUS_VIA']->value;?>
">
                   </div>
                   <div class="field_container">
                       <label><?php echo $_smarty_tpl->tpl_vars['LABEL_NOM_VIA']->value;?>
:*</label>
                       <input type="text" name="nom_via_sollicitant" id="nom_via_sollicitant" placeholder="<?php echo $_smarty_tpl->tpl_vars['LABEL_NOM_VIA']->value;?>
">
                   </div>
                   <div class="field_container">
                       <label class="numero_sollicitant"><?php echo $_smarty_tpl->tpl_vars['LABEL_NUMERO']->value;?>
:*</label>
                       <input type="text" name="numero_sollicitant" id="numero_sollicitant" class="numero_sollicitant" placeholder="<?php echo $_smarty_tpl->tpl_vars['LABEL_NUMERO']->value;?>
">
                   </div>
                   <div class="field_container">
                       <label class="numero_sollicitant"><?php echo $_smarty_tpl->tpl_vars['LABEL_LLETRA']->value;?>
:*</label>
                       <input type="text" name="lletra_sollicitant" id="lletra_sollicitant" class="lletra_sollicitant" placeholder="<?php echo $_smarty_tpl->tpl_vars['LABEL_LLETRA']->value;?>
">
                   </div>
                   <div class="field_container">
                       <label class="km_sollicitant"><?php echo $_smarty_tpl->tpl_vars['LABEL_KM']->value;?>
:</label>
                       <input type="text" name="km_sollicitant" id="km_sollicitant" class="km_sollicitant" placeholder="<?php echo $_smarty_tpl->tpl_vars['LABEL_KM']->value;?>
">
                   </div>
                   <div class="field_container">
                       <label class="bloc_sollicitant"><?php echo $_smarty_tpl->tpl_vars['LABEL_BLOC']->value;?>
:</label>
                       <input type="text" name="bloc_sollicitant" id="bloc_sollicitant" class="bloc_sollicitant" placeholder="<?php echo $_smarty_tpl->tpl_vars['LABEL_BLOC']->value;?>
">
                   </div>
                   <div class="field_container">
                       <label class="escala_sollicitant"><?php echo $_smarty_tpl->tpl_vars['LABEL_ESCALA']->value;?>
:</label>
                       <input type="text" name="escala_sollicitant" id="escala_sollicitant" class="escala_sollicitant" placeholder="<?php echo $_smarty_tpl->tpl_vars['LABEL_ESCALA']->value;?>
">
                   </div>
                   <div class="field_container">
                       <label class="pis_sollicitant"><?php echo $_smarty_tpl->tpl_vars['LABEL_PIS']->value;?>
:</label>
                       <input type="text" name="pis_sollicitant" id="pis_sollicitant" class="pis_sollicitant" placeholder="<?php echo $_smarty_tpl->tpl_vars['LABEL_PIS']->value;?>
">
                   </div>
                   <div class="field_container">
                       <label class="porta_sollicitant"><?php echo $_smarty_tpl->tpl_vars['LABEL_PORTA']->value;?>
:</label>
                       <input type="text" name="porta_sollicitant" id="porta_sollicitant" class="porta_sollicitant" placeholder="<?php echo $_smarty_tpl->tpl_vars['LABEL_PORTA']->value;?>
">
                   </div>
                   <div class="field_container">
                       <label class="nucli_barri_sollicitant"><?php echo $_smarty_tpl->tpl_vars['LABEL_NUCLI_BARRI']->value;?>
:</label>
                       <input type="text" name="nucli_barri_sollicitant" id="nucli_barri_sollicitant" class="nucli_barri_sollicitant" placeholder="<?php echo $_smarty_tpl->tpl_vars['LABEL_NUCLI_BARRI']->value;?>
">
                   </div>
                   <div class="field_container">
                       <label class="provincia_sollicitant"><?php echo $_smarty_tpl->tpl_vars['LABEL_PROVINCIA']->value;?>
:*</label>
                       <input type="text" name="provincia_sollicitant" id="provincia_sollicitant" class="provincia_sollicitant" placeholder="<?php echo $_smarty_tpl->tpl_vars['LABEL_PROVINCIA']->value;?>
">
                   </div>
                   <div class="field_container">
                       <label class="municipi_sollicitant"><?php echo $_smarty_tpl->tpl_vars['LABEL_MUNICIPI']->value;?>
:*</label>
                       <input type="text" name="municipi_sollicitant" id="municipi_sollicitant" class="municipi_sollicitant" placeholder="<?php echo $_smarty_tpl->tpl_vars['LABEL_MUNICIPI']->value;?>
">
                   </div>
                   <div class="field_container">
                       <label class="codi_postal_sollicitant"><?php echo $_smarty_tpl->tpl_vars['LABEL_CODI_POSTAL']->value;?>
:*</label>
                       <input type="text" name="codi_postal_sollicitant" id="codi_postal_sollicitant" class="codi_postal_sollicitant" placeholder="<?php echo $_smarty_tpl->tpl_vars['LABEL_CODI_POSTAL']->value;?>
">
                   </div>
                   <div class="field_container">
                       <label class="telefon_fix_sollicitant"><?php echo $_smarty_tpl->tpl_vars['LABEL_TELEFON_FIX']->value;?>
:</label>
                       <input type="text" name="telefon_fix_sollicitant" id="telefon_fix_sollicitant" class="telefon_fix_sollicitant" placeholder="<?php echo $_smarty_tpl->tpl_vars['LABEL_TELEFON_FIX']->value;?>
">
                   </div>
                   <div class="field_container">
                       <label class="telefon_mobil_sollicitant"><?php echo $_smarty_tpl->tpl_vars['LABEL_TELEFON_MOBIL']->value;?>
:*</label>
                       <input type="text" name="telefon_mobil_sollicitant" id="telefon_mobil_sollicitant" class="telefon_mobil_sollicitant" placeholder="<?php echo $_smarty_tpl->tpl_vars['LABEL_TELEFON_MOBIL']->value;?>
">
                   </div>
                   <div class="field_container">
                       <label class="email_sollicitant"><?php echo $_smarty_tpl->tpl_vars['LABEL_EMAIL']->value;?>
:</label>
                       <input type="text" name="email_sollicitant" id="email_sollicitant" class="email_sollicitant" placeholder="<?php echo $_smarty_tpl->tpl_vars['LABEL_EMAIL']->value;?>
">
                   </div>
                   <div class="queixes_i_suggeriments_separator">&nbsp;</div>
                   <div class="row_full title">
                       <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL3']->value;?>

                   </div>
                   <div class="field_container">
                       <label><?php echo $_smarty_tpl->tpl_vars['LABEL_TIPUS_PETICIO']->value;?>
:*</label>
                       <select name="tipus_peticio_sollicitant" id="tipus_peticio_sollicitant">
                           <option name=""></option>
                           <option name="Consulta"><?php echo $_smarty_tpl->tpl_vars['OPTION_TIPUS_PETICIO_1']->value;?>
</option>
                           <option name="Queixa"><?php echo $_smarty_tpl->tpl_vars['OPTION_TIPUS_PETICIO_2']->value;?>
</option>
                           <option name="Suggeriment"><?php echo $_smarty_tpl->tpl_vars['OPTION_TIPUS_PETICIO_3']->value;?>
</option>
                       </select>
                   </div>
                   <div class="field_container">
                       <label><?php echo $_smarty_tpl->tpl_vars['LABEL_AFECTA_A']->value;?>
:*</label>
                       <select name="afecta_a_sollicitant" id="afecta_a_sollicitant">
                           <option name=""></option>
                           <option name="Consulta"><?php echo $_smarty_tpl->tpl_vars['OPTION_AFECTA_A_1']->value;?>
</option>
                           <option name="Queixa"><?php echo $_smarty_tpl->tpl_vars['OPTION_AFECTA_A_2']->value;?>
</option>
                           <option name="Infrastructures municipals"><?php echo $_smarty_tpl->tpl_vars['OPTION_AFECTA_A_3']->value;?>
</option>
                           <option name="Edificis privats"><?php echo $_smarty_tpl->tpl_vars['OPTION_AFECTA_A_4']->value;?>
</option>
                           <option name="Veïns"><?php echo $_smarty_tpl->tpl_vars['OPTION_AFECTA_A_5']->value;?>
</option>
                           <option name="Altres"><?php echo $_smarty_tpl->tpl_vars['OPTION_AFECTA_A_6']->value;?>
</option>
                       </select>
                   </div>
                   <div class="new_row"></div>
                   <div class="field_container full">
                       <label class="descripcio_sollicitant"><?php echo $_smarty_tpl->tpl_vars['LABEL_DESCRIPCIO']->value;?>
:*</label>
                       <textarea name="descripcio_sollicitant" id="descripcio_sollicitant" class="descripcio_sollicitant" placeholder="<?php echo $_smarty_tpl->tpl_vars['LABEL_DESCRIPCIO']->value;?>
"></textarea>
                   </div>
                   <div class="field_container full">
                       <div class="g-recaptcha" data-sitekey="6Lfdwh4TAAAAAKajPw-1Rj8msRd8YVZx14Vwova-"></div>
                       <label id="recaptcha-error"><?php echo $_smarty_tpl->tpl_vars['LABEL_ERROR_15']->value;?>
</label>
                   </div>
                   <div class="field_container buttons">
                       <input class="submit" type="submit" value="<?php echo $_smarty_tpl->tpl_vars['BUTTON_SUBMIT']->value;?>
">
                   </div>
                   <div class="footer-lopd">
                       <?php echo $_smarty_tpl->tpl_vars['LABEL_LOPD']->value;?>

                   </div>
               </form>
               <?php } else { ?>
                   <?php echo $_smarty_tpl->tpl_vars['LABEL_RESPOSTA']->value;?>

               <?php }?>
               </div>
            </div>
         </div>
      </div>
   </div>         
</div>   <?php }} ?>
