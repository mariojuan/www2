<?php /* Smarty version Smarty-3.1.16, created on 2017-12-14 10:26:32
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/infeco/2018/ep18_pressupost.tpl" */ ?>
<?php /*%%SmartyHeaderCode:8533585715a3243c86bdfd2-49884711%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd3b92d36aa6578468609ca4ca70cd78fd2d4dbcd' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/infeco/2018/ep18_pressupost.tpl',
      1 => 1510923271,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8533585715a3243c86bdfd2-49884711',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'image' => 0,
    'LABEL_TITOL1' => 0,
    'LABEL_TC7_181' => 0,
    'lang' => 0,
    'LABEL_TC7_ITEM1' => 0,
    'LABEL_TC7_ITEM2' => 0,
    'LABEL_TC7_ITEM3b' => 0,
    'LABEL_TC7_ITEM4' => 0,
    'LABEL_TC7_ITEM5' => 0,
    'LABEL_TC7_ITEM6' => 0,
    'LABEL_TC7_ITEM7' => 0,
    'LABEL_TC7_ITEM8' => 0,
    'LABEL_TC7_AREA2' => 0,
    'LABEL_TC7_AREA3' => 0,
    'LABEL_TC7_AREA21' => 0,
    'LABEL_TC7_ITEM211b' => 0,
    'LABEL_TC7_ITEM212' => 0,
    'LABEL_TC7_ITEM213' => 0,
    'LABEL_TC7_AREA22' => 0,
    'LABEL_TC7_ITEM223' => 0,
    'LABEL_TC7_AREA23' => 0,
    'LABEL_TC7_ITEM231' => 0,
    'LABEL_TC7_ITEM232' => 0,
    'LABEL_TC7_ITEM233' => 0,
    'LABEL_TC7_ITEM234' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a3243c879b036_90310063',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a3243c879b036_90310063')) {function content_5a3243c879b036_90310063($_smarty_tpl) {?><html>
   <head>
      <title>
      </title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <?php echo $_smarty_tpl->getSubTemplate ("head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

   </head>
   <body>
      <?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

      <div id="page-wrap">
         <div class="contenedor-responsive">
            <div id="contexpretotalcos" class="continfecototalcos">
               <div id="conttranscos">
                  <div id="btranspresen">
                     <div id="transimatge">        
                        <img src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
" class="img-slider-territori">        
                     </div>
                     <div id="transimatgeText">
                        <h1>
                           <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

                        </h1>
                     </div>
                  </div>
                  <div class="subtitolinfeco" id="subtitolinfeco2">
                     <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_181']->value;?>

                  </div>
                  <div class="marcback">
                     <a href="infeco_ep18.php?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
">
                     <i class="icon-angle-double-up"></i>
                     </a>
                  </div>
                  <br><br>

                  <div class="info-econoc-finan-no-responsive">
                     <div id="taula3cpre">
                        <div id="taula3ccolpre">
                           <div id="columna1pre"><a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/tparticip.pdf" target="_blank">&raquo; <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM1']->value;?>
</a></div>
                           <div id="columna2pre"></div>
                           <div id="columna3pre"><a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/Memalc.pdf" target="_blank">&raquo; <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM2']->value;?>
</a></div>
                        </div>
                        <div id="taula3ccolpre">
                           <div id="columna1pre"><a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/GrupComparativa.pdf" target="_blank">&raquo; <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM3b']->value;?>
</a></div>
                           <div id="columna2pre"></div>
                           <div id="columna3pre"><a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/BE2018.pdf" target="_blank">&raquo; <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM4']->value;?>
</a></div>
                        </div>
                        <div id="taula3ccolpre">
                           <div id="columna1pre"><a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/evolucioPressupost.pdf" target="_blank">&raquo; <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM5']->value;?>
</a></div>
                           <div id="columna2pre"></div>
                           <div id="columna3pre"><a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/InformeInt.pdf" target="_blank">&raquo; <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM6']->value;?>
</a></div>
                        </div>
                        <div id="taula3ccolpre">
                           <div id="columna1pre" style="border-bottom: 0px solid #eee;"></div>
                           <div id="columna2pre"></div>
                           <div id="columna3pre"><a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/InformeIntEstab.pdf" target="_blank">&raquo; <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM7']->value;?>
</a></div>
                        </div>
                        <div id="taula3ccolpre">
                           <div id="columna1pre" style="border-bottom: 0px solid #eee;"></div>
                           <div id="columna2pre"></div>
                           <div id="columna3pre"><a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/Estatconsolida.pdf" target="_blank">&raquo; <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM8']->value;?>
</a></div>
                        </div>
                     </div>
                     <div id="taula3c">
                        <div id="taula3ccol">
                           <div id="columna1" style="font-size: 16px; background:#ED1F33; opacity: 0.86; color:#fff; text-align:center"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_AREA2']->value;?>
</div>
                           <div id="columna2" style="font-size: 16px; background:#f6f6f6; color:#fff; text-align:center"></div>
                           <div id="columna3" style="font-size: 16px; background:#ED1F33; opacity: 0.86; color:#fff; text-align:center"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_AREA3']->value;?>
</div>
                        </div>
                        <div id="taula3ccol">
                           <div id="columna1" style="font-size: 14px; background:#ED1F33; opacity: 0.56; color:#fff; text-align:center"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_AREA21']->value;?>
</div>
                           <div id="columna2"></div>
                           <div id="columna3" align="center"><a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/SMGUMTSA/p2018.pdf" target="_blank"><img src="/images/infeco/bGumtsa.jpg" width="186" height="40" border="0"></a></div>
                        </div>
                        <div id="taula3ccol">
                           <div id="columna1"><a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/IngressosComparativa.pdf" target="_blank">&raquo; <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM211b']->value;?>
</a></div>
                           <div id="columna2"></div>
                           <div id="columna3" align="center"><a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/SMTortosaMedia/p2018.pdf" target="_blank"><img src="/images/infeco/bMedia.jpg" width="186" height="40" border="0"></a></div>
                        </div>
                        <div id="taula3ccol">
                           <div id="columna1"><a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/IngressosClaseconomica.pdf" target="_blank">&raquo; <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM212']->value;?>
</a></div>
                           <div id="columna2"></div>
                           <div id="columna3" align="center"><a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/SMTortosaSport/p2018.pdf" target="_blank"><img src="/images/infeco/bSport.jpg" width="186" height="40" border="0"></a></div>
                        </div>
                        <div id="taula3ccol">
                           <div id="columna1"><a href="http://www.tortosa.cat/webajt/pressupost/2018/ingressos2018.csv" target="_self">&raquo; <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM213']->value;?>
 &nbsp;&nbsp;<img src="/images/infeco/iopendata.jpg" border="0" alt="dades obertes" title="dades obertes"></a></div>
                           <div id="columna2"></div>
                           <div id="columna3" align="center"><a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/SMGESAT/p2018.pdf" target="_blank"><img src="/images/infeco/bgesat.jpg" width="186" height="40" border="0"></a></div>
                        </div>
                        <div id="taula3ccol">
                           <div id="columna1" style="font-size: 14px; background:#ED1F33; opacity: 0.56; color:#fff; text-align:center"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_AREA22']->value;?>
</div>
                           <div id="columna2"></div>
                           <div id="columna3" align="center"><a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/SMTortosaSalut/p2018.pdf" target="_blank"><img src="/images/infeco/bsalut.jpg" width="186" height="40" border="0"></a></div>
                        </div>
                        <div id="taula3ccol">
                           <div id="columna1"><a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/DespesesComparativa.pdf" target="_blank">&raquo; <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM211b']->value;?>
</a></div>
                           <div id="columna2"></div>
                           <div id="columna3" align="center"><a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/EPEHospital/p2018.pdf" target="_blank"><img src="/images/infeco/bepel.jpg" width="186" height="40" border="0"></a></div>
                        </div>
                        <div id="taula3ccol">
                           <div id="columna1"><a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/DespesesClaseconomica.pdf" target="_blank">&raquo; <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM212']->value;?>
</a></div>
                           <div id="columna2"></div>
                           <div id="columna3" align="center"><a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/ConRutaReis/p2018.pdf" target="_blank"><img src="/images/infeco/b3reis.jpg" width="186" height="40" border="0"></a></div>
                        </div>
                        <div id="taula3ccol">
                           <div id="columna1"><a href="http://www.tortosa.cat/webajt/pressupost/2018/despeses2018.csv" target="_self">&raquo; <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM223']->value;?>
 &nbsp;&nbsp;<img src="/images/infeco/iopendata.jpg" border="0" alt="dades obertes" title="dades obertes"></a></div>
                           <div id="columna2"></div>
                           <div id="columna3" align="center"><a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/SMXEMSP/p2018.pdf" target="_blank"><img src="/images/infeco/bemsp.jpg" width="186" height="40" border="0"></a></div>
                        </div>
                        <div id="taula3ccol">
                           <div id="columna1" style="font-size: 14px; background:#ED1F33; opacity: 0.56; color:#fff; text-align:center"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_AREA23']->value;?>
</div>
                           <div id="columna2"></div>
                           <div id="columna3" align="center"><a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/SMXEscorxador/p2018.pdf" target="_blank"><img src="/images/infeco/bescorxa.jpg" width="186" height="40" border="0"></a></div>
                        </div>
                        <div id="taula3ccol">
                           <div id="columna1"><a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/Plantillapersonal2018.pdf" target="_blank">&raquo; <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM231']->value;?>
</a></div>
                           <div id="columna2"></div>
                           <div id="columna3" style="border-bottom: 0px solid #eee;"></div>
                        </div>
                        <div id="taula3ccol">
                           <div id="columna1"><a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/PFI.pdf" target="_blank">&raquo; <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM232']->value;?>
</a></div>
                           <div id="columna2"></div>
                           <div id="columna3" style="border-bottom: 0px solid #eee;"></div>
                        </div>
                        <div id="taula3ccol">
                           <div id="columna1"><a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/Estatdeldeute.pdf" target="_blank">&raquo; <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM233']->value;?>
</a></div>
                           <div id="columna2"></div>
                           <div id="columna3" style="border-bottom: 0px solid #eee;"></div>
                        </div>
                        <div id="taula3ccol">
                           <div id="columna1"><a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/IEF.pdf" target="_blank">&raquo; <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM234']->value;?>
</a></div>
                           <div id="columna2"></div>
                           <div id="columna3" style="border-bottom: 0px solid #eee;"></div>
                        </div>
                     </div>
                  </div>

                  <div class="info-econoc-finan-responsive">
                     <p><b>Documents</b></p>
                     <ul>
                        <li><a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/tparticip.pdf" target="_blank">&raquo; <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM1']->value;?>
</a></li>
                        <li><a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/Memalc.pdf" target="_blank">&raquo; <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM2']->value;?>
</a></li>
                        <li><a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/GrupComparativa.pdf" target="_blank">&raquo; <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM3b']->value;?>
</a></li>
                        <li><a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/BE2018.pdf" target="_blank">&raquo; <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM4']->value;?>
</a></li>
                        <li><a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/evolucioPressupost.pdf" target="_blank">&raquo; <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM5']->value;?>
</a></li>
                        <li><a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/InformeInt.pdf" target="_blank">&raquo; <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM6']->value;?>
</a></li>
                        <li><a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/InformeIntEstab.pdf" target="_blank">&raquo; <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM7']->value;?>
</a></li>
                        <li><a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/Estatconsolida.pdf" target="_blank">&raquo; <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM8']->value;?>
</a></li>
                     </ul>

                     <p><b>Ajuntament -</b> Estat ingressos</p>
                     <ul>
                        <li>
                           <a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/IngressosComparativa.pdf" target="_blank">&raquo; <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM211b']->value;?>
</a>
                        </li>
                        <li>
                           <a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/IngressosClaseconomica.pdf" target="_blank">&raquo; <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM212']->value;?>
</a> 
                        </li>
                        <li>
                           <a href="http://www.tortosa.cat/webajt/pressupost/2018/ingressos2018.csv" target="_self">&raquo; <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM213']->value;?>
 &nbsp;&nbsp;<img src="/images/infeco/iopendata.jpg" border="0" alt="dades obertes" title="dades obertes"></a>
                        </li>
                     </ul>

                     <p><b>Ajuntament -</b> Estat de despeses</p>
                     <ul>
                        <li>
                           <a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/DespesesComparativa.pdf" target="_blank">&raquo; <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM211b']->value;?>

                           </a>
                        </li>
                        <li>
                            <a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/DespesesClaseconomica.pdf" target="_blank">&raquo; <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM212']->value;?>
</a>
                        </li>
                        <li>
                           <a href="http://www.tortosa.cat/webajt/pressupost/2018/despeses2018.csv" target="_self">&raquo; <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM223']->value;?>
 &nbsp;&nbsp;<img src="/images/infeco/iopendata.jpg" border="0" alt="dades obertes" title="dades obertes"></a>
                        </li>
                     </ul>

                     <p><b>Ajuntament -</b> Annexos</p>
                     <ul>
                        <li>
                           <a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/Plantillapersonal2018.pdf" target="_blank">&raquo; <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM231']->value;?>
</a>
                        </li>
                        <li>
                            <a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/PFI.pdf" target="_blank">&raquo; <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM232']->value;?>
</a>
                        </li>
                        <li>
                           <a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/Estatdeldeute.pdf" target="_blank">&raquo; <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM233']->value;?>
</a>
                        </li>
                        <li>
                           <a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/IEF.pdf" target="_blank">&raquo; <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM234']->value;?>
</a>
                        </li>
                     </ul>

                     <p><b>Ens dependents</b></p>
                     <ul>
                        <li>
                           <a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/SMGUMTSA/p2018.pdf" target="_blank"><img src="/images/infeco/bGumtsa.jpg" width="186" height="40" border="0">
                           </a>
                        </li>
                        <li>
                            <a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/SMTortosaMedia/p2018.pdf" target="_blank"><img src="/images/infeco/bMedia.jpg" width="186" height="40" border="0"></a>
                        </li>
                        <li>
                           <a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/SMTortosaSport/p2018.pdf" target="_blank"><img src="/images/infeco/bSport.jpg" width="186" height="40" border="0"></a>
                        </li>
                        <li>
                           <a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/SMGESAT/p2018.pdf" target="_blank"><img src="/images/infeco/bgesat.jpg" width="186" height="40" border="0"></a>
                        </li>
                        <li>
                           <a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/SMTortosaSalut/p2018.pdf" target="_blank"><img src="/images/infeco/bsalut.jpg" width="186" height="40" border="0"></a>
                        </li>
                        <li>
                           <a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/EPEHospital/p2018.pdf" target="_blank"><img src="/images/infeco/bepel.jpg" width="186" height="40" border="0"></a>
                        </li>
                        <li>
                           <a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/ConRutaReis/p2018.pdf" target="_blank"><img src="/images/infeco/b3reis.jpg" width="186" height="40" border="0"></a>
                        </li>
                        <li>
                           <a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/SMXEMSP/p2018.pdf" target="_blank"><img src="/images/infeco/bemsp.jpg" width="186" height="40" border="0"></a>
                        </li>
                        <li>
                           <a href="http://www.tortosa.cat/webajt/pressupost/2018/pdf/SMXEscorxador/p2018.pdf" target="_blank"><img src="/images/infeco/bescorxa.jpg" width="186" height="40" border="0"></a>
                        </li>
                     </ul>

                  </div>
                        
               </div>
            </div>
         </div>
      </div>
      <?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

   </body>
</html><?php }} ?>
