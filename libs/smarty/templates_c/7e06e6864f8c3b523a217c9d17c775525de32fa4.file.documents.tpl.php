<?php /* Smarty version Smarty-3.1.16, created on 2017-12-13 02:44:34
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/grups-politics/grup-politic/documents.tpl" */ ?>
<?php /*%%SmartyHeaderCode:18840927175a30860295fb04-29332118%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7e06e6864f8c3b523a217c9d17c775525de32fa4' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/grups-politics/grup-politic/documents.tpl',
      1 => 1499150563,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18840927175a30860295fb04-29332118',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'image' => 0,
    'LABEL_TITOL1' => 0,
    'LABEL_TITOL1b' => 0,
    'elements' => 0,
    'value' => 0,
    'LABEL_TC2' => 0,
    'lastpage' => 0,
    'page' => 0,
    'lang' => 0,
    'grup' => 0,
    'page_ant' => 0,
    'lblAnt' => 0,
    'desp' => 0,
    'firstpage' => 0,
    'counter' => 0,
    'page_seg' => 0,
    'lblSeg' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a308602a1cd23_50075645',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a308602a1cd23_50075645')) {function content_5a308602a1cd23_50075645($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/vhost/www2.tortosa.cat/home/html/libs/smarty/plugins/modifier.date_format.php';
?><html>
   <head>
      <?php echo $_smarty_tpl->getSubTemplate ("head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

      <link href="/css/estils_headlines.css" rel="stylesheet" type="text/css">
   </head>
   <body id="gpdocuments">
      <?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

      <div id="page-wrap">

         <div class="contenedor-responsive">

            <div id="conttranstotalcos">

               <div id="conttranscos">

                  <div id="btranspresen">
                     <div id="transimatge">         
                        <img src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
" class="img-slider-territori"/>          
                     </div>
                     <div id="transimatgeText">
                        <h1>
                           <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

                        </h1>
                     </div>
                  </div>

                  <div class="subtitolinfeco" id="subtitolinfeco2">
                     <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1b']->value;?>

                  </div>

                  <div class="marcback">
                     <a href="/grups-politics/index.php">
                     <i class="icon-angle-double-up"></i>
                     </a>
                  </div>

                  <!-- Menu -->
                  <?php echo $_smarty_tpl->getSubTemplate ("grups-politics/grup-politic/menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

                  <!--// Menu -->
                  <!-- Taula compos -->
                  <!-- <div id="tcentre_planA" style="margin-left: -36px;"> -->
                  
                     

                     <?php if (isset($_smarty_tpl->tpl_vars['elements']->value)) {?>

                     <table class="tableYves">
                        <?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['value']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['elements']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value) {
$_smarty_tpl->tpl_vars['value']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['value']->key;
?>
                        <tr>
                           <td>
                              <a href="http://www.tortosa.cat/webajt/gestiointerna/grups_municipals/documentacio/<?php echo utf8_encode($_smarty_tpl->tpl_vars['value']->value->FITXER);?>
" target="_blank" style="text-decoration:none; color:#000000">
                              <i class="icon-file-pdf"></i>
                              </a>
                           </td>
                           <td>
                              <a href="http://www.tortosa.cat/webajt/gestiointerna/grups_municipals/documentacio/<?php echo utf8_encode($_smarty_tpl->tpl_vars['value']->value->FITXER);?>
" target="_blank" style="text-decoration:none; color:#000000">
                              <?php echo smarty_modifier_date_format(utf8_encode($_smarty_tpl->tpl_vars['value']->value->DATA),"%D");?>

                              </a>
                           </td>
                           <td>
                              <a href="http://www.tortosa.cat/webajt/gestiointerna/grups_municipals/documentacio/<?php echo utf8_encode($_smarty_tpl->tpl_vars['value']->value->FITXER);?>
" target="_blank" style="text-decoration:none; color:#000000">
                              <?php echo utf8_encode($_smarty_tpl->tpl_vars['value']->value->DESCRIPCIOCURTA);?>

                              </a>
                           </td>
                        </tr>
                        <?php } ?>
                     </table>

                     <?php } else { ?>
                     <?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>

                     <?php }?>
                     

                     <!--

                        <div id="taulaicorpo">
                           <?php if (count($_smarty_tpl->tpl_vars['elements']->value)>0) {?>
                           <?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['value']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['elements']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value) {
$_smarty_tpl->tpl_vars['value']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['value']->key;
?>
                           <div id="filaicorpo">
                              <div class="colicorpo ord-col1">
                                 <a href="http://www.tortosa.cat/webajt/gestiointerna/grups_municipals/documentacio/<?php echo utf8_encode($_smarty_tpl->tpl_vars['value']->value->FITXER);?>
" target="_blank" style="text-decoration:none; color:#000000">
                                 <i class="icon-file-pdf"></i>
                                 </a>
                              </div>
                              <div class="colicorpo ord-col2">
                                 <a href="http://www.tortosa.cat/webajt/gestiointerna/grups_municipals/documentacio/<?php echo utf8_encode($_smarty_tpl->tpl_vars['value']->value->FITXER);?>
" target="_blank" style="text-decoration:none; color:#000000">
                                 <?php echo smarty_modifier_date_format(utf8_encode($_smarty_tpl->tpl_vars['value']->value->DATA),"%D");?>

                                 </a>
                              </div>
                              <div class="colicorpo ord-col3">
                                 <a href="http://www.tortosa.cat/webajt/gestiointerna/grups_municipals/documentacio/<?php echo utf8_encode($_smarty_tpl->tpl_vars['value']->value->FITXER);?>
" target="_blank" style="text-decoration:none; color:#000000">
                                 <?php echo utf8_encode($_smarty_tpl->tpl_vars['value']->value->DESCRIPCIOCURTA);?>

                                 </a>
                              </div>
                           </div>
                           <?php } ?>
                           <?php } else { ?> 
                           <div id="filaicorpo">
                              <?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>

                           </div>
                           <?php }?>
                        </div>
                        
                        -->
                     <br>
                     <div id="pagination">
                        <ul>
                           <?php if ($_smarty_tpl->tpl_vars['lastpage']->value>1) {?>
                           <?php if ($_smarty_tpl->tpl_vars['page']->value>1) {?>
                           <?php $_smarty_tpl->tpl_vars["page_ant"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value-1, null, 0);?>
                           <li><a href="/grups-politics/documents.php?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
&gp=<?php echo $_smarty_tpl->tpl_vars['grup']->value;?>
&page=<?php echo $_smarty_tpl->tpl_vars['page_ant']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['lblAnt']->value;?>
</a></li>
                           <?php }?>
                           <?php $_smarty_tpl->tpl_vars["desp"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value+4, null, 0);?>
                           <?php $_smarty_tpl->tpl_vars["firstpage"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value-4, null, 0);?>
                           <?php if ($_smarty_tpl->tpl_vars['desp']->value<$_smarty_tpl->tpl_vars['lastpage']->value) {?>
                           <?php $_smarty_tpl->tpl_vars["lastpage"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value+4, null, 0);?>
                           <?php }?>
                           <?php if ($_smarty_tpl->tpl_vars['firstpage']->value<1) {?>
                           <?php $_smarty_tpl->tpl_vars["firstpage"] = new Smarty_variable(1, null, 0);?>
                           <?php }?>
                           <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['counter']->step = 1;$_smarty_tpl->tpl_vars['counter']->total = (int) ceil(($_smarty_tpl->tpl_vars['counter']->step > 0 ? $_smarty_tpl->tpl_vars['lastpage']->value+1 - ($_smarty_tpl->tpl_vars['firstpage']->value) : $_smarty_tpl->tpl_vars['firstpage']->value-($_smarty_tpl->tpl_vars['lastpage']->value)+1)/abs($_smarty_tpl->tpl_vars['counter']->step));
if ($_smarty_tpl->tpl_vars['counter']->total > 0) {
for ($_smarty_tpl->tpl_vars['counter']->value = $_smarty_tpl->tpl_vars['firstpage']->value, $_smarty_tpl->tpl_vars['counter']->iteration = 1;$_smarty_tpl->tpl_vars['counter']->iteration <= $_smarty_tpl->tpl_vars['counter']->total;$_smarty_tpl->tpl_vars['counter']->value += $_smarty_tpl->tpl_vars['counter']->step, $_smarty_tpl->tpl_vars['counter']->iteration++) {
$_smarty_tpl->tpl_vars['counter']->first = $_smarty_tpl->tpl_vars['counter']->iteration == 1;$_smarty_tpl->tpl_vars['counter']->last = $_smarty_tpl->tpl_vars['counter']->iteration == $_smarty_tpl->tpl_vars['counter']->total;?>
                           <?php if ($_smarty_tpl->tpl_vars['counter']->value==$_smarty_tpl->tpl_vars['page']->value) {?>
                           <li><?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
</li>
                           <?php } else { ?>
                           <li><a href="/grups-politics/documents.php?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
&gp=<?php echo $_smarty_tpl->tpl_vars['grup']->value;?>
&page=<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
</a></li>
                           <?php }?>
                           <?php }} ?>
                           <?php if ($_smarty_tpl->tpl_vars['page']->value<$_smarty_tpl->tpl_vars['lastpage']->value) {?>
                           <?php $_smarty_tpl->tpl_vars["page_seg"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value+1, null, 0);?>
                           <li><a href="/grups-politics/documents.php?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
&gp=<?php echo $_smarty_tpl->tpl_vars['grup']->value;?>
&page=<?php echo $_smarty_tpl->tpl_vars['page_seg']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['lblSeg']->value;?>
</a></li>
                           <?php }?>
                           <?php } else { ?>
                           <li>1</li>
                           <?php }?>
                        </ul>
                     </div>
                 
               </div>
            </div>
         </div>
      </div>
      <script>
         $().ready(function() {
             $(".search_di").datepicker();
             $(".search_df").datepicker();
         });
      </script>
      <?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

   </body>
</html><?php }} ?>
