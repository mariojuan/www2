<?php /* Smarty version Smarty-3.1.16, created on 2017-12-12 14:57:25
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/header_little.tpl" */ ?>
<?php /*%%SmartyHeaderCode:17192983875a2fe045bbf391-09136990%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1bf456e4d04c0ed65f65e1668575dd0e52966623' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/header_little.tpl',
      1 => 1509091341,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '17192983875a2fe045bbf391-09136990',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'LABEL_COP' => 0,
    'lang' => 0,
    'url_ca' => 0,
    'active_lang_ca' => 0,
    'url_es' => 0,
    'active_lang_es' => 0,
    'url_en' => 0,
    'active_lang_en' => 0,
    'url_fr' => 0,
    'active_lang_fr' => 0,
    'LABEL_TRADUCCIO' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a2fe045c2ca28_98401945',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a2fe045c2ca28_98401945')) {function content_5a2fe045c2ca28_98401945($_smarty_tpl) {?><!-- BARRA CAPÇALERA -->
<div class="row row-header">

   <div id="contenidortotalcap1">
      <div id="contenidorcap">
         <div id="contenidorcap1">
            <div id="imatgecontenidorcap1">
               <img src="/images/escutcap1R2.jpg" title="Ajuntament de Tortosa" alt="Ajuntament de Tortosa"/>
            </div>
         </div>
         <div id="contenidorcap_text">
             <div class="nomcap">
                <?php echo $_smarty_tpl->tpl_vars['LABEL_COP']->value;?>

             </div>
             <div class="idiomescap">
                 <ul id="idiomescapul">
                     <?php if ($_smarty_tpl->tpl_vars['lang']->value=="ca") {?>
                         <?php $_smarty_tpl->tpl_vars["active_lang_ca"] = new Smarty_variable("class='active'", null, 0);?>
                     <?php } elseif ($_smarty_tpl->tpl_vars['lang']->value=="es") {?>
                         <?php $_smarty_tpl->tpl_vars["active_lang_es"] = new Smarty_variable("class='active'", null, 0);?>
                     <?php } elseif ($_smarty_tpl->tpl_vars['lang']->value=="en") {?>
                         <?php $_smarty_tpl->tpl_vars["active_lang_en"] = new Smarty_variable("class='active'", null, 0);?>
                     <?php } elseif ($_smarty_tpl->tpl_vars['lang']->value=="fr") {?>
                         <?php $_smarty_tpl->tpl_vars["active_lang_fr"] = new Smarty_variable("class='active'", null, 0);?>
                     <?php }?>
                     <?php if ($_smarty_tpl->tpl_vars['url_ca']->value=='') {?>
                         <li><a href='/' hreflang="ca" <?php echo $_smarty_tpl->tpl_vars['active_lang_ca']->value;?>
>CAT</a></li>
                     <?php } else { ?>
                         <li><a href='<?php echo $_smarty_tpl->tpl_vars['url_ca']->value;?>
' hreflang="ca" <?php echo $_smarty_tpl->tpl_vars['active_lang_ca']->value;?>
>CAT</a></li>
                     <?php }?>
                     <?php if ($_smarty_tpl->tpl_vars['url_es']->value=='') {?>
                         <!--
                  ::
                  <li><a href='<?php echo $_smarty_tpl->tpl_vars['url_ca']->value;?>
' hreflang="es" <?php echo $_smarty_tpl->tpl_vars['active_lang_es']->value;?>
>ESP</a></li>
                  -->
                     <?php } else { ?>
                         ::
                         <li><a href='<?php echo $_smarty_tpl->tpl_vars['url_es']->value;?>
' hreflang="es" <?php echo $_smarty_tpl->tpl_vars['active_lang_es']->value;?>
>ESP</a></li>
                     <?php }?>
                     <?php if ($_smarty_tpl->tpl_vars['url_en']->value=='') {?>
                         <!--
                  ::
                  <li><a href='<?php echo $_smarty_tpl->tpl_vars['url_ca']->value;?>
' hreflang="en" <?php echo $_smarty_tpl->tpl_vars['active_lang_en']->value;?>
>ENG</a></li>
                  -->
                     <?php } else { ?>
                         ::
                         <li><a href='<?php echo $_smarty_tpl->tpl_vars['url_en']->value;?>
' hreflang="en" <?php echo $_smarty_tpl->tpl_vars['active_lang_en']->value;?>
>ENG</a></li>
                     <?php }?>
                     <?php if ($_smarty_tpl->tpl_vars['url_fr']->value=='') {?>
                         <!--
                  ::
                  <li><a href='<?php echo $_smarty_tpl->tpl_vars['url_ca']->value;?>
' hreflang="fr" <?php echo $_smarty_tpl->tpl_vars['active_lang_fr']->value;?>
>FRA</a></li>
                  -->
                     <?php } else { ?>
                         ::
                         <li><a href='<?php echo $_smarty_tpl->tpl_vars['url_fr']->value;?>
' hreflang="fr" <?php echo $_smarty_tpl->tpl_vars['active_lang_fr']->value;?>
>FRA</a></li>
                     <?php }?>
                 </ul>
             </div>

             <div class="divIdiomaGoogle">
                 <div id="button_translate"><?php echo $_smarty_tpl->tpl_vars['LABEL_TRADUCCIO']->value;?>
</div>
                 <div id="widget_translate">
                     <div id="google_translate_element" style="display: inline-block; top: -10px; position: relative;"></div>
                     
                         <script type="text/javascript">
                             function googleTranslateElementInit() {
                                 new google.translate.TranslateElement({pageLanguage: 'ca', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, multilanguagePage: true, gaTrack: true, gaId: 'UA-23454522-1'}, 'google_translate_element');
                             }
                         </script>
                         <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
                     
                 </div>
             </div>
         </div>
      </div>
   </div>
</div><?php }} ?>
