<?php /* Smarty version Smarty-3.1.16, created on 2018-02-28 12:02:02
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/ciutat/home_ciutat.tpl" */ ?>
<?php /*%%SmartyHeaderCode:4265365895a2ffb563c8b76-17377047%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '981778e29572d715a09faa7dfc17a393c020a727' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/ciutat/home_ciutat.tpl',
      1 => 1519815719,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4265365895a2ffb563c8b76-17377047',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a2ffb56522e29_70850682',
  'variables' => 
  array (
    'url_municipi' => 0,
    'LABEL_MENUV1A' => 0,
    'url_estadistica_habitants' => 0,
    'LABEL_MENUV3' => 0,
    'LABEL_MENUV3B' => 0,
    'url_imatges' => 0,
    'LABEL_MENUV1' => 0,
    'LABEL_MENUV1B' => 0,
    'url_territori' => 0,
    'LABEL_MENUV2A' => 0,
    'bannerslst' => 0,
    'banner' => 0,
    'LABEL_TITOL' => 0,
    'LABEL_BAGENDAC' => 0,
    'LABEL_BAGENDACB' => 0,
    'LABEL_BMUSEUC' => 0,
    'LABEL_BMUSEUCB' => 0,
    'LABEL_BTEATREC' => 0,
    'LABEL_BTEATRECB' => 0,
    'LABEL_BRADIOC' => 0,
    'LABEL_BRADIOB' => 0,
    'LABEL_TITOLLM' => 0,
    'url_cultura' => 0,
    'LABEL_BCIUTAT1' => 0,
    'LABEL_BCIUTAT2' => 0,
    'url_educacio' => 0,
    'LABEL_BCIUTAT3' => 0,
    'url_salut' => 0,
    'LABEL_BCIUTAT4' => 0,
    'url_transport' => 0,
    'LABEL_BCIUTAT5' => 0,
    'url_festes' => 0,
    'LABEL_BCIUTAT6' => 0,
    'url_ciutathistoria' => 0,
    'LABEL_BCIUTAT7' => 0,
    'LABEL_AGERMANAMENTS' => 0,
    'LABEL_BCIUTAT9' => 0,
    'LABEL_BITEM' => 0,
    'LABEL_CAMPREDO' => 0,
    'url_reguers' => 0,
    'LABEL_ELSREGUERS' => 0,
    'LABEL_JESUS' => 0,
    'url_vinallop' => 0,
    'LABEL_VINALLOP' => 0,
    'LABEL_TITOLTEMPS' => 0,
    'LABEL_CLIMA' => 0,
    'url_planols' => 0,
    'LABEL_PLANOLS' => 0,
    'LABEL_EQUIPAMENTS' => 0,
    'url_agermana' => 0,
    'LABEL_PATRIMONI' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a2ffb56522e29_70850682')) {function content_5a2ffb56522e29_70850682($_smarty_tpl) {?><div id="page-wrap">
   <div id="contenidortotalcos">
      <!-- FOTOS SLIDER -->
      <div class="rowCartellera">
         <div id="bciutatpresen">
            <ul id="bciutatmenu">
               <li >
                  <a href="<?php echo $_smarty_tpl->tpl_vars['url_municipi']->value;?>
">
                     <div id="ciutatbotoimatge">
                        <img src="/images/botons/bmunicipi.jpg" />                           
                     </div>
                     <div id="textbmenu1" class="textbotomciutat">
                        <?php echo $_smarty_tpl->tpl_vars['LABEL_MENUV1A']->value;?>

                     </div>
                  </a>
               </li>
               <li >
                  <a href="<?php echo $_smarty_tpl->tpl_vars['url_estadistica_habitants']->value;?>
">
                     <div id="ciutatbotoimatge">
                        <img src="/images/botons/besta.jpg" />                         
                     </div>
                     <div id="textbmenu2" class="textbotomciutat">
                        <?php echo $_smarty_tpl->tpl_vars['LABEL_MENUV3']->value;?>

                        <br>
                        <?php echo $_smarty_tpl->tpl_vars['LABEL_MENUV3B']->value;?>

                     </div>
                  </a>
               </li>
               <li>
                  <a href="<?php echo $_smarty_tpl->tpl_vars['url_imatges']->value;?>
">
                     <div id="ciutatbotoimatge">
                        <img src="/images/botons/bimatges.jpg" />                         
                     </div>
                     <div id="textbmenu3" class="textbotomciutat">
                        <?php echo $_smarty_tpl->tpl_vars['LABEL_MENUV1']->value;?>

                        <br>
                        <?php echo $_smarty_tpl->tpl_vars['LABEL_MENUV1B']->value;?>

                     </div>
                  </a>
               </li>
               <li >
                  <a href="<?php echo $_smarty_tpl->tpl_vars['url_territori']->value;?>
">
                     <div id="ciutatbotoimatge">
                        <img src="/images/botons/bterritori.jpg" />                          
                     </div>
                     <div id="textbmenu4" class="textbotomciutat">
                        <?php echo $_smarty_tpl->tpl_vars['LABEL_MENUV2A']->value;?>
  
                     </div>
                  </a>
               </li>
            </ul>
            <div id="ciutatimatge" class="bx-wrapper">
                <div class="bx-viewport" aria-live="polite" style="width: 100%; overflow: hidden; position: relative;">
                    <!-- Slider de la Ciutat -->
                    <ul class="bxslider" style="width: 4215%; position: relative; transition-duration: 0.5s; transform: translate3d(-438px, 0px, 0px);">
                        <?php  $_smarty_tpl->tpl_vars['banner'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['banner']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['bannerslst']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['banner']->key => $_smarty_tpl->tpl_vars['banner']->value) {
$_smarty_tpl->tpl_vars['banner']->_loop = true;
?>
                            <?php if ($_smarty_tpl->tpl_vars['banner']->value->TIPUS==3) {?>
                                <li style="float: left; list-style: outside none none; position: relative;" class="bx-clone" aria-hidden="true">
                                    <img src="/public_files/banners/<?php echo $_smarty_tpl->tpl_vars['banner']->value->IMATGE;?>
" class="img-slider-ciutat" alt="<?php echo $_smarty_tpl->tpl_vars['banner']->value->TITOL;?>
" title="<?php echo $_smarty_tpl->tpl_vars['banner']->value->TITOL;?>
"/>
                                </li>
                            <?php }?>
                        <?php } ?>
                    </ul>
                </div>
            </div>
            <div id="ciutatimatgeText" class="imatgeCapText">
               <h1>
                  <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL']->value;?>
&nbsp;&nbsp;
               </h1>
            </div>
         </div>
      </div>
      <!--/ FOTOS SLIDER -->
      <!-- BANNERS -->
      <div class="row">

         <div class="div-banners-ciutat">

         <div class="marcfinestra banner-mes-informacio">
            <div id="banner4ima">
               <a href="http://agenda.tortosa.cat/agenda-tortosa/" target="_blank">
               <img src="/images/banners/bnagenda2.jpg" title="Agenda" alt="Agenda">
               </a>         
            </div>
            <div class="banners4text" id="bannerlateraltext">
               <?php echo $_smarty_tpl->tpl_vars['LABEL_BAGENDAC']->value;?>

            </div>
         </div>
         <div class="marcfinestra banner-mes-informacio">
            <div id="banner4ima">
               <a href="http://www.tortosa.altanet.org/imact/agenda317.pdf" target="_blank"><img src="/images/banners/bnagecul.jpg" title="Agenda Cultural" alt="Agenda Cultural"></a>       
            </div>
            <div class="banners4text" id="bannerlateraltext">
               <?php echo $_smarty_tpl->tpl_vars['LABEL_BAGENDAC']->value;?>

            </div>
            <div class="banners4text" id="banners4textp">
               <?php echo $_smarty_tpl->tpl_vars['LABEL_BAGENDACB']->value;?>

            </div>
         </div>
         <div class="marcfinestra banner-mes-informacio">
            <div id="banner4ima">
               <a href="http://www.museudetortosa.cat" target="_blank"><img src="/images/banners/bnmuseuf.jpg" title="Museu de Tortosa" alt="Museu de Tortosa"></a>        
            </div>
            <div class="banners4text" id="bannerlateraltext">
               <?php echo $_smarty_tpl->tpl_vars['LABEL_BMUSEUC']->value;?>

            </div>
            <div class="banners4text" id="banners4textp">
               <?php echo $_smarty_tpl->tpl_vars['LABEL_BMUSEUCB']->value;?>

            </div>
         </div>
         <div class="marcfinestra banner-mes-informacio">
            <div id="banner4ima">
               <a href="http://www.teatreauditoritortosa.cat" target="_blank"><img src="/images/banners/bnteatref.jpg" title="Teatre Auditori Felip Pedrell" alt="Teatre Auditori Felip Pedrell"></a>       
            </div>
            <div class="banners4text" id="bannerlateraltext">
               <?php echo $_smarty_tpl->tpl_vars['LABEL_BTEATREC']->value;?>

            </div>
            <div class="banners4text" id="banners4textp">
               <?php echo $_smarty_tpl->tpl_vars['LABEL_BTEATRECB']->value;?>

            </div>
         </div>
         <div class="marcfinestra banner-mes-informacio banner-mes-informacio-last">
            <div id="banner4ima">
               <a href="http://www.radiotortosa.cat" target="_blank"><img src="/images/banners/bnradiot.jpg" title="Ràdio Tortosa" alt="Ràdio Tortosa"></a>       
            </div>
            <div class="banners4text" id="bannerlateraltext">
               <?php echo $_smarty_tpl->tpl_vars['LABEL_BRADIOC']->value;?>

            </div>
            <div class="banners4text" id="banners4textp">
               <?php echo $_smarty_tpl->tpl_vars['LABEL_BRADIOB']->value;?>

            </div>
         </div>

         </div>

      </div>
      <!--/ BANNERS -->
      <!-- VIU LA CIUTAT -->
      <div class="row">

         <div class="titolsSeccionsHome titolBig marge-titol-contingut">
            <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOLLM']->value;?>

         </div>

         <div class="div-banners-viuLaCiutat">

         <div class="column-left-viulaciutat">
            <a href="<?php echo $_smarty_tpl->tpl_vars['url_cultura']->value;?>
" target="_self" title="<?php echo $_smarty_tpl->tpl_vars['LABEL_BCIUTAT1']->value;?>
">
               <div class="marcfinestra2 banner-viulaciutat">
                  <img src="/images/botons/bciutatcul.jpg">
                  <div class="banners3text" id="tciutat">
                     <?php echo $_smarty_tpl->tpl_vars['LABEL_BCIUTAT1']->value;?>
 
                  </div>
               </div>
            </a>
            <a href="http://tortosasport.cat/" target="_blank" title="<?php echo $_smarty_tpl->tpl_vars['LABEL_BCIUTAT2']->value;?>
">
               <div class="marcfinestra2 banner-viulaciutat">
                  <img src="/images/botons/bciutatesport.jpg">
                  <div class="banners3text" id="tciutat">
                     <?php echo $_smarty_tpl->tpl_vars['LABEL_BCIUTAT2']->value;?>
 
                  </div>
               </div>
            </a>
            <a href="<?php echo $_smarty_tpl->tpl_vars['url_educacio']->value;?>
" target="_self" title="<?php echo $_smarty_tpl->tpl_vars['LABEL_BCIUTAT3']->value;?>
">
               <div class="marcfinestra2 banner-viulaciutat">
                  <img src="/images/botons/bciutatens.jpg">
                  <div class="banners3text" id="tciutat">
                     <?php echo $_smarty_tpl->tpl_vars['LABEL_BCIUTAT3']->value;?>
 
                  </div>
               </div>
            </a>
            <a href="<?php echo $_smarty_tpl->tpl_vars['url_salut']->value;?>
" target="_self" title="<?php echo $_smarty_tpl->tpl_vars['LABEL_BCIUTAT4']->value;?>
">
               <div class="marcfinestra2 banner-viulaciutat">
                  <img src="/images/botons/bciutatsalut.jpg">
                  <div class="banners3text" id="tciutat">
                     <?php echo $_smarty_tpl->tpl_vars['LABEL_BCIUTAT4']->value;?>
 
                  </div>
               </div>
            </a>
            <a href="<?php echo $_smarty_tpl->tpl_vars['url_transport']->value;?>
" target="_self" title="<?php echo $_smarty_tpl->tpl_vars['LABEL_BCIUTAT5']->value;?>
">
               <div class="marcfinestra2 banner-viulaciutat">
                  <img src="/images/botons/bciutattrans.jpg">
                  <div class="banners3text" id="tciutat">
                     <?php echo $_smarty_tpl->tpl_vars['LABEL_BCIUTAT5']->value;?>
 
                  </div>
               </div>
            </a>
            <a href="<?php echo $_smarty_tpl->tpl_vars['url_festes']->value;?>
" target="_self" title="<?php echo $_smarty_tpl->tpl_vars['LABEL_BCIUTAT6']->value;?>
">
               <div class="marcfinestra2 banner-viulaciutat">
                  <img src="/images/botons/bciutatfes.jpg">
                  <div class="banners3text" id="tciutat">
                     <?php echo $_smarty_tpl->tpl_vars['LABEL_BCIUTAT6']->value;?>
 
                  </div>
               </div>
            </a>
            <a href="<?php echo $_smarty_tpl->tpl_vars['url_ciutathistoria']->value;?>
" title="<?php echo $_smarty_tpl->tpl_vars['LABEL_BCIUTAT7']->value;?>
">
               <div class="marcfinestra2 banner-viulaciutat">
                  <img src="/images/botons/bciutathis.jpg">
                  <div class="banners3text" id="tciutat">
                     <?php echo $_smarty_tpl->tpl_vars['LABEL_BCIUTAT7']->value;?>
 
                  </div>
               </div>
            </a>
            <a href="/agermana/index.php" target="_self" title="<?php echo $_smarty_tpl->tpl_vars['LABEL_AGERMANAMENTS']->value;?>
">
               <div class="marcfinestra2 banner-viulaciutat">
                  <img src="/images/botons/bagermana.jpg">
                  <div class="banners3text" id="tciutat">
                     <?php echo $_smarty_tpl->tpl_vars['LABEL_AGERMANAMENTS']->value;?>
  
                  </div>
               </div>
            </a>
            <a href="/turisme-oci/index.php" target="_self" title="<?php echo $_smarty_tpl->tpl_vars['LABEL_BCIUTAT9']->value;?>
">
               <div class="marcfinestra2 banner-viulaciutat">
                  <img src="/images/botons/bciutattur.jpg">
                  <div class="banners3text" id="tciutat">
                     <?php echo $_smarty_tpl->tpl_vars['LABEL_BCIUTAT9']->value;?>
 
                  </div>
               </div>
            </a>
         </div>

         </div>

         <div class="column-right-viulaciutat">

         <div class="div-ciutat-agenda">

            <iframe width="100%" height="565" frameborder="0" src="http://agenda.tortosa.cat/agenda-tortosa-widget/"></iframe>

         </div>

         </div>

      </div>
      <!--/ VIU LA CIUTAT -->
      <!-- POBLES -->
      <div class="row">
         <div class="div-pobles">
         <div class="banner-pobles-ciutat">
            <div class="taula-n-ciutat">
               <div class="col-negocis" id="col1NPobles"><a href="http://www.bitem.altanet.org/" target="_blank"><?php echo $_smarty_tpl->tpl_vars['LABEL_BITEM']->value;?>
</a></div>
               <div class="col-negocis" id="col2NPobles"><a href="http://www.campredo.cat/" target="_blank"><?php echo $_smarty_tpl->tpl_vars['LABEL_CAMPREDO']->value;?>
</a></div>
               <div class="col-negocis" id="col3NPobles"><a href="<?php echo $_smarty_tpl->tpl_vars['url_reguers']->value;?>
" target="_self"><?php echo $_smarty_tpl->tpl_vars['LABEL_ELSREGUERS']->value;?>
</a></div>
               <div class="col-negocis" id="col4NPobles"><a href="http://www.jesus.cat" target="_blank"><?php echo $_smarty_tpl->tpl_vars['LABEL_JESUS']->value;?>
</a></div>
               <div class="col-negocis" id="col5NPobles"><a href="<?php echo $_smarty_tpl->tpl_vars['url_vinallop']->value;?>
" target="_self"><?php echo $_smarty_tpl->tpl_vars['LABEL_VINALLOP']->value;?>
</a></div>
            </div>
         </div>
         </div>
      </div>
      <!--/ POBLES -->
      <!-- EL TEMPS -->
      <div class="row responsive-ocult">

         <div class="titolsSeccionsHome titolBig marge-titol-contingut">
            <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOLTEMPS']->value;?>

         </div>

            
            <script type='text/javascript' src='http://www.aemet.es/ca/eltiempo/prediccion/municipios/launchwidget/tortosa-id43155?w=g4p01010000ohm99ccffw894z107x6699fft99ccffr0s4n1'>
            </script>
            <noscript>
               <a target='_blank' style='font-weight: bold;font-size: 1.20em;' href='http://www.aemet.es/ca/eltiempo/prediccion/municipios/tortosa-id43155'> El Temps. Consulteu la predicció de l'AEMET per Tortosa</a>
            </noscript>
            

      </div>
      <!--/ EL TEMPS -->

      <!-- +INFO -->
      <div class="row">

         <div class="div-mesInfo">

         <div class="info-general-ciutat">
               <div class="taula-n-ciutat-2">
               
                  <div class="col-negocis col-negocis2"><a href="http://www.meteotortosa.cat/meteo/" target="_blank"><?php echo $_smarty_tpl->tpl_vars['LABEL_CLIMA']->value;?>
</a></div>

                  <div class="col-negocis col-negocis2"><a href="<?php echo $_smarty_tpl->tpl_vars['url_planols']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['LABEL_PLANOLS']->value;?>
</a></div>
               
                  <div class="col-negocis col-negocis2"><a href="http://serveisoberts.gencat.cat/equipaments#?adreca.municipi=Tortosa" target="_blank"><?php echo $_smarty_tpl->tpl_vars['LABEL_EQUIPAMENTS']->value;?>
</a></div>
               
                  <div class="col-negocis col-negocis2"><a href="<?php echo $_smarty_tpl->tpl_vars['url_agermana']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['LABEL_AGERMANAMENTS']->value;?>
</a></div>

                  <div class="col-negocis col-negocis2"><a href="http://documents.tortosa.cat/" target="_blank"><?php echo $_smarty_tpl->tpl_vars['LABEL_PATRIMONI']->value;?>
</a></div>
               
               </div>
            </div>

            </div>

      </div>
      <!--/ +INFO -->

   </div>
</div><?php }} ?>
