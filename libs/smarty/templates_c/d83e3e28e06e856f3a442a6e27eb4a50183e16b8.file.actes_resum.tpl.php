<?php /* Smarty version Smarty-3.1.16, created on 2018-02-19 10:44:03
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/ple/actes_resum.tpl" */ ?>
<?php /*%%SmartyHeaderCode:826978805a2ff161402458-70350856%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd83e3e28e06e856f3a442a6e27eb4a50183e16b8' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/ple/actes_resum.tpl',
      1 => 1519033425,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '826978805a2ff161402458-70350856',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a2ff16150ee53_20087722',
  'variables' => 
  array (
    'image' => 0,
    'LABEL_TITOL1' => 0,
    'LABEL_TITOLSECCIO' => 0,
    'LABEL_TC0' => 0,
    'LABEL_TC1' => 0,
    'LABEL_TC2' => 0,
    'LABEL_TC4' => 0,
    'LABEL_TCRESUMS' => 0,
    'LABEL_TC2015' => 0,
    'LABEL_TCRESUM_AREES' => 0,
    'LABEL_TC2016' => 0,
    'LABEL_TC2017' => 0,
    'LABEL_TCTRACTAMENT_ESTADISTIC' => 0,
    'LABEL_TC3' => 0,
    'LABEL_TC2011' => 0,
    'LABEL_TC2012' => 0,
    'LABEL_TC2013' => 0,
    'LABEL_TC2014' => 0,
    'LABEL_TCINFORMACIO_TABULADA' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a2ff16150ee53_20087722')) {function content_5a2ff16150ee53_20087722($_smarty_tpl) {?><html>
<head>
	<?php echo $_smarty_tpl->getSubTemplate ("head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

</head>
<body>
	<?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<div id="page-wrap">

	<div class="contenedor-responsive">
	
		<div id="conttranstotalcos">
			<div id="conttranscos">
				<div id="btranspresen">
					<div id="transimatge">
						<img src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
" class="img-slider-territori"/>
					</div>
					<div id="transimatgeText">
						<h1>
							<?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

							<i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
						</h1>
					</div>
				</div>

				<!-- Menu -->
                <?php echo $_smarty_tpl->getSubTemplate ("ple/menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            	<!--// Menu -->

				<div id="tcentre_planA">

					<!-- Competències-->

					<font size="4" color="#666666"><?php echo $_smarty_tpl->tpl_vars['LABEL_TITOLSECCIO']->value;?>
</font>
					<p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC0']->value;?>
</p>
					<p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
</p>
					<p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
</p>

					

					<!-- Mandat 2015-2019-->

					
					<h3><?php echo $_smarty_tpl->tpl_vars['LABEL_TC4']->value;?>
</h3>
					<p><i><?php echo $_smarty_tpl->tpl_vars['LABEL_TCRESUMS']->value;?>
</i></p>
					<ul class="listSenseBullets">
						<li><i class="icon-file-pdf"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC2015']->value;?>
 - <a href="http://www.tortosa.cat/webajt/ajunta/om/dades-plens/resum-per-temes-2015-2.pdf"><?php echo $_smarty_tpl->tpl_vars['LABEL_TCRESUM_AREES']->value;?>
</a></li>
						<li><i class="icon-file-pdf"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC2016']->value;?>
 - <a href="http://www.tortosa.cat/webajt/ajunta/om/dades-plens/resum-per-temes-2016.pdf"><?php echo $_smarty_tpl->tpl_vars['LABEL_TCRESUM_AREES']->value;?>
</a></li>
						<li><i class="icon-file-pdf"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC2017']->value;?>
 - <a href="http://www.tortosa.cat/webajt/ajunta/om/dades-plens/resum-per-temes-2017.pdf"><?php echo $_smarty_tpl->tpl_vars['LABEL_TCRESUM_AREES']->value;?>
</a></li>
					</ul>

					<!-- <p><i><?php echo $_smarty_tpl->tpl_vars['LABEL_TCTRACTAMENT_ESTADISTIC']->value;?>
</i></p> -->

					<div class="separator"></div>

					<!-- Mandat 2011-2015 -->

					
					<h3><?php echo $_smarty_tpl->tpl_vars['LABEL_TC3']->value;?>
</h3>
					<p><i><?php echo $_smarty_tpl->tpl_vars['LABEL_TCRESUMS']->value;?>
</i></p>
					<ul class="listSenseBullets">
						<li><i class="icon-file-pdf"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC2011']->value;?>
 - <a href="http://www.tortosa.cat/webajt/ajunta/om/dades-plens/resum-per-temes-2011.pdf"><?php echo $_smarty_tpl->tpl_vars['LABEL_TCRESUM_AREES']->value;?>
</a></li>
						<li><i class="icon-file-pdf"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC2012']->value;?>
 - <a href="http://www.tortosa.cat/webajt/ajunta/om/dades-plens/resum-per-temes-2012.pdf"><?php echo $_smarty_tpl->tpl_vars['LABEL_TCRESUM_AREES']->value;?>
</a></li>
						<li><i class="icon-file-pdf"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC2013']->value;?>
 - <a href="http://www.tortosa.cat/webajt/ajunta/om/dades-plens/resum-per-temes-2013.pdf"><?php echo $_smarty_tpl->tpl_vars['LABEL_TCRESUM_AREES']->value;?>
</a></li>
						<li><i class="icon-file-pdf"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC2014']->value;?>
 - <a href="http://www.tortosa.cat/webajt/ajunta/om/dades-plens/resum-per-temes-2014.pdf"><?php echo $_smarty_tpl->tpl_vars['LABEL_TCRESUM_AREES']->value;?>
</a></li>
						<li><i class="icon-file-pdf"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC2015']->value;?>
 - <a href="http://www.tortosa.cat/webajt/ajunta/om/dades-plens/resum-per-temes-2015-1.pdf"><?php echo $_smarty_tpl->tpl_vars['LABEL_TCRESUM_AREES']->value;?>
</a></li>
					</ul>

					<p><i><?php echo $_smarty_tpl->tpl_vars['LABEL_TCTRACTAMENT_ESTADISTIC']->value;?>
</i></p>
					<p><?php echo $_smarty_tpl->tpl_vars['LABEL_TCINFORMACIO_TABULADA']->value;?>
</p>

					<a href="http://www.tortosa.cat/webajt/ajunta/om/dades-plens/estadistiques-grafiques-ple-per-temes-mandat-2011-2015.pdf">
						<img src="/templates/ple/img/grafic-mandat-2011-2015.jpg" height="130" width="300" class="marcterme img-responsive-95" />
					</a>

				</div>
			</div>
		</div>
		</div>
	</div>

	<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


</body>
</html><?php }} ?>
