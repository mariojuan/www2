<?php /* Smarty version Smarty-3.1.16, created on 2017-12-13 01:54:44
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/ple/competencies.tpl" */ ?>
<?php /*%%SmartyHeaderCode:4877330235a307a543c8b44-83507369%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c61810ac4439cb2e387ac783c222ee1e20faeeed' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/ple/competencies.tpl',
      1 => 1499151027,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4877330235a307a543c8b44-83507369',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'image' => 0,
    'LABEL_TITOL1' => 0,
    'LABEL_TC0' => 0,
    'LABEL_TC01' => 0,
    'LABEL_TC1' => 0,
    'LABEL_TC2' => 0,
    'LABEL_TC21' => 0,
    'LABEL_TC3' => 0,
    'LABEL_TC4' => 0,
    'LABEL_TC5' => 0,
    'LABEL_TC6' => 0,
    'LABEL_TC7' => 0,
    'LABEL_TC8' => 0,
    'LABEL_TC9' => 0,
    'LABEL_TC10' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a307a5446dcf1_88312271',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a307a5446dcf1_88312271')) {function content_5a307a5446dcf1_88312271($_smarty_tpl) {?><html>
   <head>
      <?php echo $_smarty_tpl->getSubTemplate ("head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

   </head>
   <body>
      <?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

      <div id="page-wrap">

         <div class="contenedor-responsive">

            <div id="conttranstotalcos">

               <div id="conttranscos">

                  <div id="btranspresen">

                     <div id="transimatge">
                        <img src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
" class="img-slider-territori"/>
                     </div>

                     <div id="transimatgeText">
                        <h1>
                           <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

                           <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                        </h1>
                     </div>

                  </div>

                  <!-- Menu -->
                  <?php echo $_smarty_tpl->getSubTemplate ("ple/menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

                  <!--// Menu -->
                  <div id="tcentre_planA">
                     <!-- Competències-->
                     <font size="4" color="#666666"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC0']->value;?>
</font>
                     <h3><?php echo $_smarty_tpl->tpl_vars['LABEL_TC01']->value;?>
</h3>
                     <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
</p>
                     <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
</p>
                     <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC21']->value;?>
</p>
                     <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC3']->value;?>
</p>
                     <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC4']->value;?>
</p>
                     <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC5']->value;?>
</p>
                     <!-- Delegacions-->
                     <h3><?php echo $_smarty_tpl->tpl_vars['LABEL_TC6']->value;?>
</h3>
                     <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC7']->value;?>
</p>
                     <p><i class="icon-file-pdf"></i><a href="http://www.tortosa.cat/webajt/ajunta/om/2015/APleDelegacioJGLn.pdf" target="_blank"> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC8']->value;?>
</a></p>
                     <p class="normativa"><strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC9']->value;?>
</strong> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC10']->value;?>
</p>
                  </div>
                  
                  <div class="separator"></div>
               </div>
            </div>
         </div>
      </div>
      <?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

   </body>
</html><?php }} ?>
