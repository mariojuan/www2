<?php /* Smarty version Smarty-3.1.16, created on 2017-12-13 02:33:38
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/subvencions/subvencions_associacions_veins.tpl" */ ?>
<?php /*%%SmartyHeaderCode:20031317825a308372dfe9a4-90902983%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '507920c04c42084e96183d40d3d375658e78f0a5' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/subvencions/subvencions_associacions_veins.tpl',
      1 => 1507283914,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '20031317825a308372dfe9a4-90902983',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'image' => 0,
    'LABEL_TITOL1' => 0,
    'LABEL_TITOL1b' => 0,
    'url_exercici_back' => 0,
    'MENU' => 0,
    'itemMenu' => 0,
    'LABEL_TITOL0' => 0,
    'LABEL_TC2_1PUNT_0' => 0,
    'LABEL_TC2_1TXT_0' => 0,
    'LABEL_TC2_1PUNT_1' => 0,
    'LABEL_TC2_1TXT_1' => 0,
    'LABEL_TC2_1PUNT_2' => 0,
    'LABEL_PUNT_2LI_1' => 0,
    'LABEL_TC2_1PUNT_3' => 0,
    'LABEL_PUNT_3LI_1' => 0,
    'LABEL_PUNT_3LI_2' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a308372e7d420_04141888',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a308372e7d420_04141888')) {function content_5a308372e7d420_04141888($_smarty_tpl) {?><!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->
<div id="page-wrap">
<div class="contenedor-responsive">
   <div id="conttranstotalcos">
      <div id="conttranscos">
         <div id="btranspresen">
            <div id="transimatge">			
               <img src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
" class="img-slider-territori"/>			
            </div>
            <div id="transimatgeText">
               <h1>
                  <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

                  <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
               </h1>
            </div>
         </div>
         <div class="subtitolinfeco" id="subtitolinfeco2">
            <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1b']->value;?>

         </div>
         <div class="marcback">
            <a href="<?php echo $_smarty_tpl->tpl_vars['url_exercici_back']->value;?>
">
            <i class="icon-angle-double-up"></i>
            </a>
         </div>
         <ul id='menu_planA'>
            <?php  $_smarty_tpl->tpl_vars['itemMenu'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['itemMenu']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['MENU']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['itemMenu']->key => $_smarty_tpl->tpl_vars['itemMenu']->value) {
$_smarty_tpl->tpl_vars['itemMenu']->_loop = true;
?>
            <a href=<?php echo $_smarty_tpl->tpl_vars['itemMenu']->value;?>
>
               <li id='menu_planA_item'>
                  <?php echo $_smarty_tpl->tpl_vars['itemMenu']->key;?>

               </li>
            </a>
            <?php } ?>
         </ul>
         <div id="tcentre_planA">
            <font size=4 color=#666666>
            <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL0']->value;?>

            </font>

            <ul class="subvencions1">
               <li>
                  <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2_1PUNT_0']->value;?>
</strong></p>
                  <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2_1TXT_0']->value;?>
</p>
               </li>
               <li>
                  <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2_1PUNT_1']->value;?>
</strong></p>
                  <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2_1TXT_1']->value;?>
</p>
               </li>
               <li>
                  <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2_1PUNT_2']->value;?>
</strong></p>
                  <ul>
                     <li><?php echo $_smarty_tpl->tpl_vars['LABEL_PUNT_2LI_1']->value;?>
</li>
                  </ul>
               </li>
               <li>
                  <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2_1PUNT_3']->value;?>
</strong></p>
                  <ul class="subvencions1">
                     <li><i class="icon-file-pdf"></i>&nbsp;<a href="/subvencions/2017/bases_sub_aavv_17.pdf" target="_blank"><?php echo $_smarty_tpl->tpl_vars['LABEL_PUNT_3LI_1']->value;?>
</a></li>
                     <li><i class="icon-file-pdf"></i>&nbsp;<a href="/subvencions/2017/annex_sub_aavv_17.pdf" target="_blank"><?php echo $_smarty_tpl->tpl_vars['LABEL_PUNT_3LI_2']->value;?>
</a></li>
                  </ul>
               </li>
            </ul>
            <br><br><br>
         </div>
      </div>
   </div>
</div>
</div><?php }} ?>
