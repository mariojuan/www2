<?php /* Smarty version Smarty-3.1.16, created on 2017-12-12 14:59:48
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/borsa/fitxa.tpl" */ ?>
<?php /*%%SmartyHeaderCode:19588414485a2fe0d4448810-69664562%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '086a0323a28c383424f937154f0048181def89c5' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/borsa/fitxa.tpl',
      1 => 1497343165,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '19588414485a2fe0d4448810-69664562',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'LABEL_TITOL1' => 0,
    'MENU' => 0,
    'itemMenu' => 0,
    'elements' => 0,
    'lblAnunci' => 0,
    'lblObjecte' => 0,
    'lblDenominacio' => 0,
    'lblRegimJuridic' => 0,
    'lblCaracter' => 0,
    'lblEscala' => 0,
    'lblSubescala' => 0,
    'lblClasse' => 0,
    'lblGrup' => 0,
    'lblNumPlaces' => 0,
    'lblSeleccio' => 0,
    'lblRequisits' => 0,
    'lblPresenSol' => 0,
    'lblDataIniciPresentacio' => 0,
    'lblDataFiPresentacio' => 0,
    'lblDretsExamen' => 0,
    'lblDocumentacio' => 0,
    'lblBases' => 0,
    'lblResultats' => 0,
    'lblLlistaAdmisosExclososProvisional' => 0,
    'lblLlistaAdmisosExclososDefinitiva' => 0,
    'lblResultatsBorsaTreball' => 0,
    'lblLlistaBorsaTreball' => 0,
    'lblTornar' => 0,
    'lang' => 0,
    'page' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a2fe0d4522116_80734424',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a2fe0d4522116_80734424')) {function content_5a2fe0d4522116_80734424($_smarty_tpl) {?><html>
    <head>
        <?php echo $_smarty_tpl->getSubTemplate ("head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    </head>
	<body id="borsa">
		<?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

        <div id="page-wrap">
            <div id="conttranstotalcos">
                <div id="conttranscos">
                    <div id="btranspresen">
                        <div id="transimatge">
                            <img src="/images/laciutat/municipi/tira.jpg" />
                        </div>
                        <div id="transimatgeText">
                            <h1>
                                <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

                            </h1>
                        </div>
                    </div>
                    <ul id='menu_planA'>
                        <?php  $_smarty_tpl->tpl_vars['itemMenu'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['itemMenu']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['MENU']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['itemMenu']->key => $_smarty_tpl->tpl_vars['itemMenu']->value) {
$_smarty_tpl->tpl_vars['itemMenu']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['itemMenu']->key;
?>
                            <a href=<?php echo $_smarty_tpl->tpl_vars['itemMenu']->value['link'];?>
 target="<?php echo $_smarty_tpl->tpl_vars['itemMenu']->value['target'];?>
">
                                <li id='menu_planA_item'>
                                    <?php echo $_smarty_tpl->tpl_vars['itemMenu']->value['name'];?>

                                </li>
                            </a>
                        <?php } ?>
                    </ul>
                    <div id="tcentre_planA">
                        <h2><?php echo $_smarty_tpl->tpl_vars['elements']->value[0]->denominacio;?>
</h2>
                        <?php if (count($_smarty_tpl->tpl_vars['elements']->value)>0) {?>
                            <p><div class="field"><?php echo $_smarty_tpl->tpl_vars['lblAnunci']->value;?>
</div><div class="value-field"><?php echo $_smarty_tpl->tpl_vars['elements']->value[0]->anunci;?>
</div></p>
                            <p><div class="field"><?php echo $_smarty_tpl->tpl_vars['lblObjecte']->value;?>
</div><div class="value-field"><?php echo $_smarty_tpl->tpl_vars['elements']->value[0]->objecte;?>
</div></p>
                            <p><div class="field"><?php echo $_smarty_tpl->tpl_vars['lblDenominacio']->value;?>
</div><div class="value-field"><?php echo $_smarty_tpl->tpl_vars['elements']->value[0]->denominacio;?>
</div></p>
                            <p><div class="field"><?php echo $_smarty_tpl->tpl_vars['lblRegimJuridic']->value;?>
</div><div class="value-field"><?php echo $_smarty_tpl->tpl_vars['elements']->value[0]->regim_juridic;?>
</div></p>
                            <p><div class="field"><?php echo $_smarty_tpl->tpl_vars['lblCaracter']->value;?>
</div><div class="value-field"><?php echo $_smarty_tpl->tpl_vars['elements']->value[0]->caracter;?>
</div></p>
                            <p><div class="field"><?php echo $_smarty_tpl->tpl_vars['lblEscala']->value;?>
</div><div class="value-field"><?php echo $_smarty_tpl->tpl_vars['elements']->value[0]->escala;?>
</div></p>
                            <p><div class="field"><?php echo $_smarty_tpl->tpl_vars['lblSubescala']->value;?>
</div><div class="value-field"><?php echo $_smarty_tpl->tpl_vars['elements']->value[0]->subescala;?>
</div></p>
                            <p><div class="field"><?php echo $_smarty_tpl->tpl_vars['lblClasse']->value;?>
</div><div class="value-field"><?php echo $_smarty_tpl->tpl_vars['elements']->value[0]->classe;?>
</div></p>
                            <p><div class="field"><?php echo $_smarty_tpl->tpl_vars['lblGrup']->value;?>
</div><div class="value-field"><?php echo $_smarty_tpl->tpl_vars['elements']->value[0]->grup;?>
</div></p>
                            <p><div class="field"><?php echo $_smarty_tpl->tpl_vars['lblNumPlaces']->value;?>
</div><div class="value-field"><?php echo $_smarty_tpl->tpl_vars['elements']->value[0]->num_places;?>
</div></p>
                            <p><div class="field"><?php echo $_smarty_tpl->tpl_vars['lblSeleccio']->value;?>
</div><div class="value-field"><?php echo $_smarty_tpl->tpl_vars['elements']->value[0]->seleccio;?>
</div></p>
                            <p><div class="field"><?php echo $_smarty_tpl->tpl_vars['lblRequisits']->value;?>
</div><div class="value-field"><?php echo $_smarty_tpl->tpl_vars['elements']->value[0]->requisits;?>
</div></p>
                            <p><div class="field"><?php echo $_smarty_tpl->tpl_vars['lblPresenSol']->value;?>
</div><div class="value-field"><?php echo $_smarty_tpl->tpl_vars['elements']->value[0]->presen_solicituds;?>
</div></p>
                            <p><div class="field"><?php echo $_smarty_tpl->tpl_vars['lblDataIniciPresentacio']->value;?>
</div><div class="value-field"><?php echo $_smarty_tpl->tpl_vars['elements']->value[0]->data_inici_presen;?>
</div></p>
                            <p><div class="field"><?php echo $_smarty_tpl->tpl_vars['lblDataFiPresentacio']->value;?>
</div><div class="value-field"><?php echo $_smarty_tpl->tpl_vars['elements']->value[0]->data_final_presen;?>
</div></p>
                            <p><div class="field"><?php echo $_smarty_tpl->tpl_vars['lblDretsExamen']->value;?>
</div><div class="value-field"><?php echo $_smarty_tpl->tpl_vars['elements']->value[0]->drets_examen;?>
</div></p>
                            <p><div class="field"><?php echo $_smarty_tpl->tpl_vars['lblDocumentacio']->value;?>
</div><div class="value-field"><a href="http://www.tortosa.cat/webajt/gestiointerna/borsa/bases/<?php echo $_smarty_tpl->tpl_vars['elements']->value[0]->bases;?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['lblBases']->value;?>
</a></div></p>
                            <p>
                                <div class="field"><?php echo $_smarty_tpl->tpl_vars['lblResultats']->value;?>
</div>
                                <div class="value-field">
                                    <ul>
                                        <?php if (trim($_smarty_tpl->tpl_vars['elements']->value[0]->llista_admesos_exclosos)!='') {?>
                                        <li><i class="icon-file-pdf"></i> <a href="http://www.tortosa.cat/webajt/gestiointerna/borsa/adm_excl_prov/<?php echo $_smarty_tpl->tpl_vars['elements']->value[0]->llista_admesos_exclosos;?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['lblLlistaAdmisosExclososProvisional']->value;?>
</a></li>
                                        <?php }?>
                                        <?php if (trim($_smarty_tpl->tpl_vars['elements']->value[0]->llista_admesos_exclosos_defini)!='') {?>
                                            <li><i class="icon-file-pdf"></i> <a href="http://www.tortosa.cat/webajt/gestiointerna/borsa/adm_excl_def/<?php echo $_smarty_tpl->tpl_vars['elements']->value[0]->llista_admesos_exclosos_defini;?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['lblLlistaAdmisosExclososDefinitiva']->value;?>
</a></li>
                                        <?php }?>
                                        <?php if (trim($_smarty_tpl->tpl_vars['elements']->value[0]->resultats1)!='') {?>
                                            <li><i class="icon-file-pdf"></i> <a href="http://www.tortosa.cat/webajt/gestiointerna/borsa/resultats/<?php echo $_smarty_tpl->tpl_vars['elements']->value[0]->resultats1;?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['lblResultats']->value;?>
</a></li>
                                        <?php }?>
                                        <?php if (trim($_smarty_tpl->tpl_vars['elements']->value[0]->borsa_provisional)!='') {?>
                                            <li><i class="icon-file-pdf"></i> <a href="http://www.tortosa.cat/webajt/gestiointerna/borsa/resultats/<?php echo $_smarty_tpl->tpl_vars['elements']->value[0]->borsa_provisional;?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['lblResultatsBorsaTreball']->value;?>
</a></li>
                                        <?php }?>
                                        <?php if (trim($_smarty_tpl->tpl_vars['elements']->value[0]->borsa_definitiva)!='') {?>
                                            <li><i class="icon-file-pdf"></i> <a href="http://www.tortosa.cat/webajt/gestiointerna/borsa/borsa/<?php echo $_smarty_tpl->tpl_vars['elements']->value[0]->borsa_definitiva;?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['lblLlistaBorsaTreball']->value;?>
</a></li>
                                        <?php }?>
                                    </ul>
                                </div>
                            </p>
                        <?php }?>
                        <div id="buttons-container">
                            <input type="button" name="enrere" id="enrere" value="<?php echo $_smarty_tpl->tpl_vars['lblTornar']->value;?>
" onclick="JavaScript:window.location.href='/borsa/index.php?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
&page=<?php echo $_smarty_tpl->tpl_vars['page']->value;?>
';">
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

	</body>
</html><?php }} ?>
