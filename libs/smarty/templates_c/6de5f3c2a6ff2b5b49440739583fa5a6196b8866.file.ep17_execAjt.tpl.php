<?php /* Smarty version Smarty-3.1.16, created on 2018-02-20 10:21:12
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/infeco/ep17_execAjt.tpl" */ ?>
<?php /*%%SmartyHeaderCode:762240715a30867f263fc4-30539658%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6de5f3c2a6ff2b5b49440739583fa5a6196b8866' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/infeco/ep17_execAjt.tpl',
      1 => 1519118467,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '762240715a30867f263fc4-30539658',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a30867f2df212_25197022',
  'variables' => 
  array (
    'image' => 0,
    'LABEL_TITOL1' => 0,
    'LABEL_TC7_171' => 0,
    'lang' => 0,
    'LABEL_TC7_INTRO' => 0,
    'LABEL_TC7_TITOL1' => 0,
    'LABEL_TC7_TITOL1B' => 0,
    'LABEL_TC7_TITOL2' => 0,
    'LABEL_TC7_TRIM1' => 0,
    'LABEL_TC7_TRIM2' => 0,
    'LABEL_TC7_TRIM3' => 0,
    'LABEL_TC7_TRIM4' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a30867f2df212_25197022')) {function content_5a30867f2df212_25197022($_smarty_tpl) {?><html>
   <head>
      <title>
      </title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <?php echo $_smarty_tpl->getSubTemplate ("head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

   </head>
   <body>
      <?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

      <div id="page-wrap">
         <div class="contenedor-responsive">
            <div id="contexpretotalcos" class="continfecototalcos">
               <div id="conttranscos">
                  <div id="btranspresen">
                     <div id="transimatge">         
                        <img src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
" class="img-slider-territori">           
                     </div>
                     <div id="transimatgeText">
                        <h1>
                           <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

                        </h1>
                     </div>
                  </div>
                  <div class="subtitolinfeco" id="subtitolinfeco2">
                     <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_171']->value;?>

                  </div>
                  <div class="marcback">
                     <a href="infeco_ep17.php?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
">
                     <i class="icon-angle-double-up"></i>
                     </a>
                  </div>
                  <br><br>

                  <div class="info-econoc-finan-no-responsive">

                  <div id="textintro_execAjt">
                     <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_INTRO']->value;?>

                  </div>
                  <div id="caixaexecajt2">
                     <div id="caixamodpre" class="caixaexecajt17">
                        <a href="infeco_execajt17modpre.php">
                        <br><?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_TITOL1']->value;?>
<br><?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_TITOL1B']->value;?>
 
                        </a>       
                     </div>
                     <div id="caixaexectrim" class="caixaexecajt17">
                        <div id="titolcapexectrim17">
                           <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_TITOL2']->value;?>
         
                        </div>
                        <a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/ajt/1rtrim2017.pdf">
                           <div id="capexectrim1" class="capexectrim">
                              <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_TRIM1']->value;?>

                           </div>
                        </a>
                        <a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/ajt/2ntrim2017.pdf">
                           <div id="capexectrim2" class="capexectrim">
                              <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_TRIM2']->value;?>

                           </div>
                        </a>
                        <a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/ajt/3ntrim2017.pdf">
                           <div id="capexectrim3" class="capexectrim">
                              <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_TRIM3']->value;?>

                           </div>
                        </a>
                        <a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/ajt/4ttrim2017.pdf">
                           <div id="capexectrim4" class="capexectrim">
                              <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_TRIM4']->value;?>

                           </div>
                        </a>
                     </div>
                  </div>

                  </div>

                  <div class="info-econoc-finan-responsive">
                     <i class="icon-link"></i>
                     <a href="infeco_execajt17modpre.php">
                        <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_TITOL1']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_TITOL1B']->value;?>
 
                        </a>
                  </div>

               </div>
            </div>
         </div>
      </div>
      <?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

   </body>
</html><?php }} ?>
