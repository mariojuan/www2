<?php /* Smarty version Smarty-3.1.16, created on 2018-01-18 10:42:36
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/admin/templates/list_urbanisme_element.tpl" */ ?>
<?php /*%%SmartyHeaderCode:19827156415a606c0c362588-56918583%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a1774c62ae95930edbb112c86c0d818048b2fec8' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/admin/templates/list_urbanisme_element.tpl',
      1 => 1435734170,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '19827156415a606c0c362588-56918583',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'items' => 0,
    'item' => 0,
    'page' => 0,
    'lastpage' => 0,
    'page_ant' => 0,
    'desp' => 0,
    'firstpage' => 0,
    'counter' => 0,
    'page_seg' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a606c0c82ab65_71302976',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a606c0c82ab65_71302976')) {function content_5a606c0c82ab65_71302976($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/vhost/www2.tortosa.cat/home/html/libs/smarty/plugins/modifier.date_format.php';
?><section id="main" class="column">

<h4 class="alert_info"><i class="icon-pencil"></i>Llistat elements urbanisme <span id="search_content"><a href="JavaScript:search_content();"><i class="icon-search"></i></a></span> </h4>
<article class="module width_full">
    <div class="module_content">
        <table class="list_table" name="list_urbanisme_element" id="list_urbanisme_element">
            <tr>
                <th>Títol</th>
                <th>Data</th>
                <th></th>
                <th></th>
            </tr>
            <tr><td colspan="4"></td></tr>
            <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
                <tr>
                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value->TITOL;?>
</td>
                    <td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['item']->value->DATA,"%d/%m/%G");?>
</td>
                    <td class="link_item"><a href="urbanisme.php?accio=edit_urbanisme_element&id=<?php echo $_smarty_tpl->tpl_vars['item']->value->ID;?>
&page=<?php echo $_smarty_tpl->tpl_vars['page']->value;?>
">Modificar</a></td>
                    <td class="link_item"><a href="JavaScript: delete_item('<?php echo $_smarty_tpl->tpl_vars['item']->value->ID;?>
', '<?php echo $_smarty_tpl->tpl_vars['page']->value;?>
');">Esborrar</a></td>
                </tr>
            <?php } ?>
        </table>
        <div id="pagination">
            <ul>
                <?php if ($_smarty_tpl->tpl_vars['lastpage']->value>1) {?>
                    <?php if ($_smarty_tpl->tpl_vars['page']->value>1) {?>
                        <?php $_smarty_tpl->tpl_vars["page_ant"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value-1, null, 0);?>
                        <li><a href="urbanisme.php?accio=list&page=<?php echo $_smarty_tpl->tpl_vars['page_ant']->value;?>
">Ant</a></li>
                    <?php }?>
                    <?php $_smarty_tpl->tpl_vars["desp"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value+4, null, 0);?>
                    <?php $_smarty_tpl->tpl_vars["firstpage"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value-4, null, 0);?>

                    <?php if ($_smarty_tpl->tpl_vars['desp']->value<$_smarty_tpl->tpl_vars['lastpage']->value) {?>
                        <?php $_smarty_tpl->tpl_vars["lastpage"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value+4, null, 0);?>
                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['firstpage']->value<1) {?>
                        <?php $_smarty_tpl->tpl_vars["firstpage"] = new Smarty_variable(1, null, 0);?>
                    <?php }?>

                    <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['counter']->step = 1;$_smarty_tpl->tpl_vars['counter']->total = (int) ceil(($_smarty_tpl->tpl_vars['counter']->step > 0 ? $_smarty_tpl->tpl_vars['lastpage']->value+1 - ($_smarty_tpl->tpl_vars['firstpage']->value) : $_smarty_tpl->tpl_vars['firstpage']->value-($_smarty_tpl->tpl_vars['lastpage']->value)+1)/abs($_smarty_tpl->tpl_vars['counter']->step));
if ($_smarty_tpl->tpl_vars['counter']->total > 0) {
for ($_smarty_tpl->tpl_vars['counter']->value = $_smarty_tpl->tpl_vars['firstpage']->value, $_smarty_tpl->tpl_vars['counter']->iteration = 1;$_smarty_tpl->tpl_vars['counter']->iteration <= $_smarty_tpl->tpl_vars['counter']->total;$_smarty_tpl->tpl_vars['counter']->value += $_smarty_tpl->tpl_vars['counter']->step, $_smarty_tpl->tpl_vars['counter']->iteration++) {
$_smarty_tpl->tpl_vars['counter']->first = $_smarty_tpl->tpl_vars['counter']->iteration == 1;$_smarty_tpl->tpl_vars['counter']->last = $_smarty_tpl->tpl_vars['counter']->iteration == $_smarty_tpl->tpl_vars['counter']->total;?>
                        <?php if ($_smarty_tpl->tpl_vars['counter']->value==$_smarty_tpl->tpl_vars['page']->value) {?>
                            <li><?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
</li>
                        <?php } else { ?>
                            <li><a href="urbanisme.php?accio=list&page=<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
</a></li>
                        <?php }?>
                    <?php }} ?>
                    <?php if ($_smarty_tpl->tpl_vars['page']->value<$_smarty_tpl->tpl_vars['lastpage']->value-1) {?>
                        <?php $_smarty_tpl->tpl_vars["page_seg"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value+1, null, 0);?>
                        <li><a href="urbanisme.php?accio=list&page=<?php echo $_smarty_tpl->tpl_vars['page_seg']->value;?>
">Seg</a></li>
                    <?php }?>
                <?php }?>
            </ul>
        </div>
    </div>
</article>

<div id="search-content">
    <h4>Cerca</h4>
    <form name="search" id="search" method="post" action="urbanisme.php">
        <p>
            <label>Títol</label>
            <input type="text" name="titol" id="titol">
        </p>
        <p>
            <input type="submit" id="cercar" name="cercar" value="Cercar">
            <input type="submit" id="tancar" name="tancar" value="Tancar">
        </p>
    </form>
</div>
<!-- end of styles article -->


<div class="spacer"></div>
</section>

<script>
    /*
    $.validator.setDefaults({
        submitHandler: function() {
            alert("submitted!");
        }
    });
    */

    $().ready(function() {
        $("#tancar").click(function() {
            $('div#search-content').css('display', 'none');
        });
    });

    function delete_item(id, page) {
        if(confirm("Vols esborrar aquest element?"))
            window.location.href="urbanisme.php?accio=delete_urbanisme_element&id="+id+"&page="+page;
    }

    function search_content() {
        $('div#search-content').css('display', 'block');
    }
</script><?php }} ?>
