<?php /* Smarty version Smarty-3.1.16, created on 2018-02-28 09:06:07
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/pciutadana/pciutadana_home.tpl" */ ?>
<?php /*%%SmartyHeaderCode:8058363915a2ff15e085de7-29476250%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '199edd92cc2ec20f4ce7f7c20328e9d821ee9447' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/pciutadana/pciutadana_home.tpl',
      1 => 1519805165,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8058363915a2ff15e085de7-29476250',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a2ff15e1332b0_74813975',
  'variables' => 
  array (
    'image' => 0,
    'LABEL_TITOL1' => 0,
    'MENU' => 0,
    'itemMenu' => 0,
    'LABEL_TITOL0' => 0,
    'item' => 0,
    'LABEL_TC1' => 0,
    'LABEL_TC1B' => 0,
    'LABEL_TC2' => 0,
    'LABEL_TC2B' => 0,
    'LABEL_TC3' => 0,
    'LABEL_TC4' => 0,
    'LABEL_TC5' => 0,
    'LABEL_TC6' => 0,
    'LABEL_TC7' => 0,
    'LABEL_TC1C' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a2ff15e1332b0_74813975')) {function content_5a2ff15e1332b0_74813975($_smarty_tpl) {?><!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->
<div id="page-wrap">

   <div class="contenedor-responsive">

      <div id="conttranstotalcos">

         <div id="conttranscos">

            <div id="btranspresen">
               <div id="transimatge">			
                  <img src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
" class="img-slider-territori"/>			
               </div>
               <div id="transimatgeText">
                  <h1>
                     <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

                     <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                  </h1>
               </div>
            </div>

            <ul id='menu_planA'>
               <?php  $_smarty_tpl->tpl_vars['itemMenu'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['itemMenu']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['MENU']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['itemMenu']->key => $_smarty_tpl->tpl_vars['itemMenu']->value) {
$_smarty_tpl->tpl_vars['itemMenu']->_loop = true;
?>
               <a href=<?php echo $_smarty_tpl->tpl_vars['itemMenu']->value;?>
>
                  <li id='menu_planA_item'>
                     <?php echo $_smarty_tpl->tpl_vars['itemMenu']->key;?>

                  </li>
               </a>
               <?php } ?>
            </ul>

            <div id="tcentre_planA">
               <font size=4 color=#666666>
               <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL0']->value;?>

               </font>
               <?php if ($_smarty_tpl->tpl_vars['item']->value==1) {?>
               <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
<br>
                  <i><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1B']->value;?>
</i>
               </p>
               <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
<br>
                  <i><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2B']->value;?>
</i>
               </p>
               <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC3']->value;?>
</p>
               <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC4']->value;?>
</p>
               <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC5']->value;?>
</p>
               <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC6']->value;?>
</p>
               <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC7']->value;?>
</p>
               <img src="/images/pciutadana/esquema.jpg" class="img-responsive">
               <?php } elseif ($_smarty_tpl->tpl_vars['item']->value==2) {?>
               <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC3']->value;?>
&nbsp;<a href="/docs/audiencies/audiencia2018.pdf"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1C']->value;?>
</a><br>
                  <a href="http://www2.tortosa.cat/noticies/noticia.php?lang=ca&id=12388" target="_blank">
                  <i><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1B']->value;?>
</i></a>
               </p>

               <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
&nbsp;<a href="/docs/audiencies/audiencia2017.pdf"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1C']->value;?>
</a><br>
                  <a href="http://www2.tortosa.cat/noticies/noticia.php?lang=ca&id=5901" target="_blank">
                  <i><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1B']->value;?>
</i></a>
               </p>
               
               <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
<br>
                  <a href="http://www2.tortosa.cat/noticies/noticia.php?lang=ca&id=5721" target="_blank">
                  <i><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1B']->value;?>
</i></a>
               </p>
               
               <?php } elseif ($_smarty_tpl->tpl_vars['item']->value==3) {?>
               <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
<br>
               <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
<br>
                  <a href="http://www2.tortosa.cat/pciutadana/2016/consultamonument.php" target="_blank">
                  <i><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2B']->value;?>
</i>
               </p>
               </a>
               <?php }?>
               <br><br><br>
            </div>

         </div>
      </div>
   </div>
</div>
<?php }} ?>
