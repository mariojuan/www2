<?php /* Smarty version Smarty-3.1.16, created on 2017-12-13 02:29:54
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/infeco/ep17_infeco.tpl" */ ?>
<?php /*%%SmartyHeaderCode:17448218675a308292982192-31843940%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0322448e7b3e2f86ceea3757f3b9959287ee5408' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/infeco/ep17_infeco.tpl',
      1 => 1511254489,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '17448218675a308292982192-31843940',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'image' => 0,
    'LABEL_TITOL1' => 0,
    'LABEL_TC7_17' => 0,
    'lang' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a308292a336e6_81099466',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a308292a336e6_81099466')) {function content_5a308292a336e6_81099466($_smarty_tpl) {?><html>
    <head>
        <title>

        </title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <?php echo $_smarty_tpl->getSubTemplate ("head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    </head>
    <body>
        <?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

        
    

<div id="page-wrap">

<div class="contenedor-responsive">

<div id="contexpretotalcos" class="continfecototalcos">

	<div id="conttranscos">
		<div id="btranspresen">
			<div id="transimatge">			
				<img src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
" class="img-slider-territori">			
			</div>
			<div id="transimatgeText">
				<h1>
				<?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

				</h1>
			</div>
		</div>
        <div class="subtitolinfeco" id="subtitolinfeco2">
            <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_17']->value;?>

        </div>
        <div class="marcback">
                    <a href="infeco_ep.php?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
">
                    <i class="icon-angle-double-up"></i>
                    </a>
        </div>
        <br><br>

        <div id="menu2017" class="div-contenidor-info-econom">
            <a href="infeco_presup17.php?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
">
            <div id="bpresgen" class="marcServeisCiutat bpresgen">
                Pressupost General<br>2017
            </div>  
            </a>
            <a href="infeco_execgrup17.php?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
">
            <div id="bpresgen2" class="marcServeisCiutat bpresgen">
                Grup<br>Municipal<br><font size="2">Sector Adm. Pública<br>Consolidat</font>
            </div>
            </a>  
            <a href="infeco_execajt17.php?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
">
            <div id="bpresgen3" class="marcServeisCiutat bpresgen">
                <br>Ajuntament
            </div>
            </a>
            <div id="bpresgen4" class="marcServeisCiutat bpresgen">
                <br>Ajuntament<br><font size="2">Liquidació</font>
            </div>  
            <div id="bpresgen5" class="marcServeisCiutat bpresgen">
               Ens<br>dependents<br><font size="2"><br>Comptes anuals</font>
            </div>
            <div id="bpresgen6" class="marcServeisCiutat bpresgen">
                Compte<br>General
            </div>  
        </div>

        <div class="div-contenidor-info-econom-list">

            <div class="llista-pressupostos">

                <p><b>Inici</b></p>
                <ul>
                    <li>
                        <a href="infeco_presup17.php?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
">
                            Pressupost General 2017
                        </a>
                    </li>
                </ul>

                <p><b>Execució</b></p>
                <ul>
                    <li>
                        <a href="infeco_execajt17.php?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
">
                            Ajuntament
                        </a>
                    </li>
                </ul>

                <!--
                <p>Tractament</p>
                <ul>
                    <li>
                        Pressupost General 2017
                    </li>
                </ul>
                -->

            </div>

        </div>

  	</div>
    </div>
</div>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

</body>
</html><?php }} ?>
