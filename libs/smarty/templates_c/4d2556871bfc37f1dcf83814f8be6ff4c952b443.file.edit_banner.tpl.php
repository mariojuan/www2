<?php /* Smarty version Smarty-3.1.16, created on 2017-12-14 10:30:59
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/admin/templates/banners/edit_banner.tpl" */ ?>
<?php /*%%SmartyHeaderCode:9756309235a3244d3b00ae0-98420546%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4d2556871bfc37f1dcf83814f8be6ff4c952b443' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/admin/templates/banners/edit_banner.tpl',
      1 => 1509000891,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '9756309235a3244d3b00ae0-98420546',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'item' => 0,
    'imatge' => 0,
    'link' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a3244d3bc6855_98003926',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a3244d3bc6855_98003926')) {function content_5a3244d3bc6855_98003926($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/vhost/www2.tortosa.cat/home/html/libs/smarty/plugins/modifier.date_format.php';
?><section id="main" class="column">
   <h4 class="alert_info"><i class="icon-doc-new"></i>Alta banner</h4>

   <article class="module width_full">
      <div class="module_content">
         <form id="bannersForm" method="post" action="banners.php" novalidate="novalidate" enctype="multipart/form-data">
            <input type="hidden" name="accio" id="accio" value="edit_banners">

            <p>
               <label>Títol *</label>
               <input type="text" name="TITOL" id="TITOL" value="<?php echo $_smarty_tpl->tpl_vars['item']->value->TITOL;?>
">
            </p>
           
            <p>
               <label>Imatge *</label>
               <?php if (trim($_smarty_tpl->tpl_vars['item']->value->IMATGE)!='') {?>
                   <span class="foto"><img src="/public_files/banners/<?php echo $_smarty_tpl->tpl_vars['item']->value->IMATGE;?>
"></span>
                   <?php $_smarty_tpl->tpl_vars["imatge"] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['item']->value->IMATGE), null, 0);?>
               <?php }?>
               <input type="file" name="FOTO" id="FOTO">
               <input type="hidden" name="FOTO_AUX" id="FOTO_AUX" value="<?php echo $_smarty_tpl->tpl_vars['imatge']->value;?>
">
               </br>
               Imatge home: 895x304 px</br>
               Imatge campanyes: 146x246 px</br>
               Imatge pàgina ciutat: 740x312 px
            </p>

            <?php if ($_smarty_tpl->tpl_vars['item']->value->FOTO!='') {?>
            <p>
               <label>Esborrar foto</label>
               <input type="checkbox" name="delete_foto" id="delete_foto">
            </p>
            <?php }?>

            <p>
               <label>Enllaç</label>
               <input type="text" name="LINK" id="LINK" value="<?php echo $_smarty_tpl->tpl_vars['item']->value->LINK;?>
">
            </p>

            <p>
               <label>Fitxer</label>
               <?php if (trim($_smarty_tpl->tpl_vars['item']->value->FITXER_LINK)!='') {?>
                    <?php $_smarty_tpl->tpl_vars["link"] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['item']->value->FITXER_LINK), null, 0);?>
               <?php }?>
                <span><?php echo $_smarty_tpl->tpl_vars['item']->value->FITXER_LINK;?>
</span>
               <input type="file" name="FITXER_LINK" id="FITXER_LINK">
               <input type="hidden" name="FITXER_LINK_AUX" id="FITXER_LINK_AUX" value="<?php echo $_smarty_tpl->tpl_vars['link']->value;?>
">
            </p>

            <p>
               <label>Data</label>
               <input type="text" name="DATA" id="DATA" value="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['item']->value->DATA,"%d/%m/%G");?>
">
            </p>

            <p>
               <label>Data inici visualització</label>
               <input type="text" name="DATA_INICI" id="DATA_INICI" value="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['item']->value->DATA_INICI,"%d/%m/%G");?>
">
            </p>

            <p>
               <label>Data fi visualització</label>
               <input type="text" name="DATA_FI" id="DATA_FI" value="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['item']->value->DATA_FI,"%d/%m/%G");?>
">
            </p>

            <p>
               <label>Tipologia *</label>
               <select name="TIPUS" id="TIPUS">
                  <option value="" selected>-- Seleccionar --</option>
                  <option value="1" <?php if ($_smarty_tpl->tpl_vars['item']->value->TIPUS=="1") {?> selected <?php }?>>Home</option>
                  <option value="2" <?php if ($_smarty_tpl->tpl_vars['item']->value->TIPUS=="2") {?> selected <?php }?>>Campanya</option>
                  <option value="3" <?php if ($_smarty_tpl->tpl_vars['item']->value->TIPUS=="3") {?> selected <?php }?>>Ciutat</option>
               </select>
            </p>
            <p>
               <label>Actiu</label>
               <input type="checkbox" name="ACTIU" id="ACTIU" <?php if ($_smarty_tpl->tpl_vars['item']->value->ACTIU==1) {?> checked <?php }?>>
            </p>
            <p>
               <input class="submit" type="submit" value="Submit">
            </p>
            <p>
               <label>* Camps obligatoris</label>
            </p>
            <input type="hidden" name="ADJUNT" id="ADJUNT" value="">
            <input type="hidden" name="FOTO" id="FOTO" value="">
            <input type="hidden" name="ID" id="ID" value="<?php echo $_smarty_tpl->tpl_vars['item']->value->ID;?>
">
         </form>
      </div>
   </article>

   <!-- end of styles article -->
   <div class="spacer"></div>
</section>

<script>
    $().ready(function() {
        $( "#DATA" ).datepicker();
        $( "#DATA_INICI" ).datepicker();
        $( "#DATA_FI" ).datepicker();


        // validate signup form on keyup and submit
        
        $("#bannersForm").validate({
            rules: {
                TITOL:  {
                    required: true,
                    maxlength: 150
                },
                FOTO: {
                    required: comproveImage()
                },
                DATA: "required",
                DATA_INICI: "required",
                DATA_FI: "required",
                TIPUS: "required"
            },
            messages: {
                TITOL: {
                    required: "El camp de títol és obligatori",
                    maxlength: "El camp de títol és massa llarg"
                },
                FOTO: comproveImageMessages(),
                DATA: "El camp de data és obligatori",
                DATA_INICI: "El camp de data inici visualització és obligatori",
                DATA_FI: "El camp de data fi visualització és obligatori",
                TIPUS: "El camp de tipologia és obligatori"
            }
        });

        function comproveImage() {
            if(($("#FOTO").val()=="") && ($("#FOTO_AUX").val()=="")) {
                return true;
            }
            else {
                return false;
            }
        }

        function comproveImageMessages() {
            if(($("#FOTO").val()=="") && ($("#FOTO_AUX").val()=="")) {
                return "El camp d'imatge és obligatori";
            }
            else {
                return null;
            }
        }
    });
</script><?php }} ?>
