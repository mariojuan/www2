<?php /* Smarty version Smarty-3.1.16, created on 2017-12-12 16:10:21
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/ciutat/ciutat_incidencies_via_publica.tpl" */ ?>
<?php /*%%SmartyHeaderCode:504471215a2ff15d56a565-73236074%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '40e371709caf3a2f25c7884dc6ce3080d8ff8dfb' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/ciutat/ciutat_incidencies_via_publica.tpl',
      1 => 1511527788,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '504471215a2ff15d56a565-73236074',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'LABEL_7' => 0,
    'LABEL_8' => 0,
    'LABEL_9' => 0,
    'LABEL_10' => 0,
    'LABEL_6' => 0,
    'LABEL_11' => 0,
    'LABEL_12' => 0,
    'LABEL_13' => 0,
    'LABEL_14' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a2ff15d635277_10368732',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a2ff15d635277_10368732')) {function content_5a2ff15d635277_10368732($_smarty_tpl) {?><html>
	<head>
        <?php echo $_smarty_tpl->getSubTemplate ("head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

        <script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyAgGXTi0OfrjbhveozxlIpPH3fTxHo8HnU"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <script type="text/javascript">
            var captcha_value = false;
            $().ready(function() {
                $('#latitud').prop('disabled', true);
                $('#longitud').prop('disabled', true);
                $('input[type=radio][id=localitzacio_mapa_si]').change(function() {
                    if($('#localitzacio_mapa_si').is(':checked')) {
                        $('#longitud').rules("add",
                        {
                             required: true,
                             messages: {
                                 required: "Els camps localització són obligatoris"
                             }
                        });
                    }
                });
                $('input[type=radio][id=localitzacio_mapa_no]').change(function() {
                    if($('#localitzacio_mapa_no').is(':checked')) {
                        $('#nucli').rules("add",
                        {
                            required: true,
                            messages: {
                                required: "<?php echo $_smarty_tpl->tpl_vars['LABEL_7']->value;?>
"
                            }
                        });
                        $('#barri').rules("add",
                        {
                            required: true,
                            messages: {
                                required: "<?php echo $_smarty_tpl->tpl_vars['LABEL_8']->value;?>
"
                            }
                        });
                        $('#carrer').rules("add",
                        {
                            required: true,
                            messages: {
                                required: "<?php echo $_smarty_tpl->tpl_vars['LABEL_9']->value;?>
"
                            }
                        });
                        $('#num').rules("add",
                        {
                            required: true,
                            messages: {
                                required: "<?php echo $_smarty_tpl->tpl_vars['LABEL_10']->value;?>
"
                            }
                        });
                    }
                });

                $("#incidencies_via_publica_form").validate({
                    rules: {
                        descripcio:  {
                            required: true
                        },
                        nom:  {
                            required: true
                        },
                        cognoms:  {
                            required: true
                        },
                        email:  {
                            required: true
                        },
                        telefon:  {
                            required: true
                        }
                    },
                    messages: {
                        descripcio: {
                            required: "<?php echo $_smarty_tpl->tpl_vars['LABEL_6']->value;?>
"
                        },
                        nom: {
                            required: "<?php echo $_smarty_tpl->tpl_vars['LABEL_11']->value;?>
"
                        },
                        cognoms: {
                            required: "<?php echo $_smarty_tpl->tpl_vars['LABEL_12']->value;?>
"
                        },
                        email: {
                            required: "<?php echo $_smarty_tpl->tpl_vars['LABEL_13']->value;?>
"
                        },
                        telefon: {
                            required: "<?php echo $_smarty_tpl->tpl_vars['LABEL_14']->value;?>
"
                        }
                    },
                    submitHandler: function(form) {
                        captcha()
                        if(captcha_value) {
                            form.submit();
                        }
                    }
                });

                $("#incidencies_via_publica_form").validate();

                $('#nucli').change(function() {
                    var nucli_id = $('#nucli').val();
                    $('#nucli_name').val($('#nucli option[value="'+nucli_id+'"]').text());
                    var xmlhttp = new XMLHttpRequest();
                    xmlhttp.onreadystatechange = function() {
                        if (this.readyState == 4 && this.status == 200) {
                            var barris = this.responseText.split("||");
                            $('#barri').empty();
                            $('#barri').append('<option value="">-- Barri --</option>');
                            $.each(barris, function(key, barri) {
                                var barri_item = barri.split("|");
                                $('#barri').append('<option value="'+barri_item[0]+'">'+barri_item[1]+'</option>');
                            });
                        }
                    };
                    xmlhttp.open("POST", "http://www2.tortosa.cat/ciutat/getbarris.php", true);
                    xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                    xmlhttp.send("nucli_id=" + nucli_id);
                });
                $('#barri').change(function() {
                    var barri_id = $('#barri').val();
                    $('#barri_name').val($('#barri option[value="'+barri_id+'"]').text());
                });

                $('#carrer').keyup(function() {
                    $('#suggestions').css('display', 'block');
                    var nucli_id = $('#nucli').val();
                    var xmlhttp = new XMLHttpRequest();
                    xmlhttp.onreadystatechange = function() {
                        if (this.readyState == 4 && this.status == 200) {
                            var carrer = this.responseText;
                            $('#suggestions').fadeIn(1000).html(carrer);
                            //Al hacer click en algua de las sugerencias
                            $('.suggest-element a').click(function(){
                                //Obtenemos la id unica de la sugerencia pulsada
                                var id = $(this).attr('id');
                                //Editamos el valor del input con data de la sugerencia pulsada
                                $('#carrer').val($('#'+id).attr('data'));
                                //Hacemos desaparecer el resto de sugerencias
                                $('#suggestions').fadeOut(1000);
                            });
                        }
                    };
                    xmlhttp.open("POST", "http://www2.tortosa.cat/ciutat/getcarrers.php", true);
                    xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                    xmlhttp.send("carrer="+$('#carrer').val()+"&nucli="+$('#nucli').val());
                });
            });

            function captcha() {
                var xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        var out = this.responseText;
                        if(out==1) {
                            captcha_value = true;
                        }
                        else {
                            $('#recaptcha-error').css('display', 'block');
                        }
                    }
                };
                xmlhttp.open("POST", "http://www2.tortosa.cat/libs/verify-recaptcha.php", false);
                xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                xmlhttp.send("g-recaptcha-response="+grecaptcha.getResponse());
            }

            function conmutalocalitzacioradios(val) {
                if(val) {
                    $('#localitzacio_mapa_si').attr('checked', true);
                    $('#localitzacio_mapa_no').attr('checked', false);
                    $('#localitzacio_mapa').css('display', 'block');
                    $('#localitzacio_no_mapa').css('display', 'none');
                    $('#map_canvas').css('display', 'block');
                }
                else {
                    $('#localitzacio_mapa_si').attr('checked', false);
                    $('#localitzacio_mapa_no').attr('checked', true);
                    $('#localitzacio_mapa').css('display', 'none');
                    $('#localitzacio_no_mapa').css('display', 'block');
                    $('#map_canvas').css('display', 'none');
                }
            }
        </script>
        <style>
            .suggest-element{
                margin-left:175px;
                margin-top:5px;
                width:320px;
                cursor:pointer;
            }
            #suggestions {
                height:150px;
                overflow: auto;
                display: none;
                margin-top: -15px;
            }
        </style>
	</head>
	<body id="incidencies_via_publica">
		<?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

		<?php echo $_smarty_tpl->getSubTemplate ("ciutat/incidencies_via_publica.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('image'=>"/images/iviapublica/tira.jpg"), 0);?>

		<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

	</body>
</html><?php }} ?>
