<?php /* Smarty version Smarty-3.1.16, created on 2017-12-12 17:12:51
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/alcalde/escriu_alcalde.tpl" */ ?>
<?php /*%%SmartyHeaderCode:18531795725a300003230e98-48981883%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4860285e628da37ac245488a43c34afed50f13b5' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/alcalde/escriu_alcalde.tpl',
      1 => 1508223672,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18531795725a300003230e98-48981883',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'FIELD_1_REQUIRED' => 0,
    'FIELD_1_OPTION' => 0,
    'FIELD_2_REQUIRED' => 0,
    'FIELD_2_EMAILVALID' => 0,
    'FIELD_3_REQUIRED' => 0,
    'FIELD_6_REQUIRED' => 0,
    'FIELD_7_REQUIRED' => 0,
    'LABEL_TITOL1' => 0,
    'MENU' => 0,
    'itemMenu' => 0,
    'LABEL_TC1' => 0,
    'request' => 0,
    'LABEL_TC6' => 0,
    'LABEL_TC7' => 0,
    'lang' => 0,
    'FIELD_1' => 0,
    'FIELD_2' => 0,
    'FIELD_3' => 0,
    'FIELD_4' => 0,
    'FIELD_5' => 0,
    'FIELD_6' => 0,
    'FIELD_7' => 0,
    'LABEL_TC3' => 0,
    'recaptcha' => 0,
    'BUTTON_SUBMIT' => 0,
    'FIELD_8_REQUIRED' => 0,
    'LABEL_TC5' => 0,
    'LABEL_TC4' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a300003314dc2_48593487',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a300003314dc2_48593487')) {function content_5a300003314dc2_48593487($_smarty_tpl) {?><html>
<head>
    <?php echo $_smarty_tpl->getSubTemplate ("head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <script type="text/javascript">
        $().ready(function() {
            $("#EscriuAlcaldeForm").submit(function() {
                return captcha();
            });
            $("#EscriuAlcaldeForm").validate({
                rules: {
                    nom:  {
                        required: true,
                        maxlength: 150
                    },
                    email:  {
                        required: true,
                        email: true
                    },
                    poblacio:   {
                        required: true
                    },
                    assumpte:   {
                        required: true
                    },
                    comentari:   {
                        required: true
                    },
                    recaptcha_response_field: {
                        required: true
                    }
                },
                messages: {
                    nom: {
                        required: "<?php echo $_smarty_tpl->tpl_vars['FIELD_1_REQUIRED']->value;?>
",
                        maxlength: "<?php echo $_smarty_tpl->tpl_vars['FIELD_1_OPTION']->value;?>
"
                    },
                    email: {
                        required: "<?php echo $_smarty_tpl->tpl_vars['FIELD_2_REQUIRED']->value;?>
",
                        email: "<?php echo $_smarty_tpl->tpl_vars['FIELD_2_EMAILVALID']->value;?>
"
                    },
                    poblacio: {
                        required: "<?php echo $_smarty_tpl->tpl_vars['FIELD_3_REQUIRED']->value;?>
"
                    },
                    assumpte: {
                        required: "<?php echo $_smarty_tpl->tpl_vars['FIELD_6_REQUIRED']->value;?>
"
                    },
                    comentari: {
                        required: "<?php echo $_smarty_tpl->tpl_vars['FIELD_7_REQUIRED']->value;?>
"
                    },
                    recaptcha_response_field: {
                        required: "El camp del codi de validació és obligatori"
                    }
                }
            });
        });



        function captcha(){
            var v1 = $("input#recaptcha_challenge_field").val();
            var v2 = $("input#recaptcha_response_field").val();

            $.ajax({
                type: "POST",
                url: "../../libs/verify-captcha.php",
                data: {
                    "recaptcha_challenge_field" : v1,
                    "recaptcha_response_field" : v2
                },
                dataType: "text",
                error: function(){
                    Recaptcha.reload();
                },
                success: function(data){
                    if(data==1) {
                        Recaptcha.reload();
                        return true;
                    }
                    else {
                        $("#recaptcha-error").html("<p>El codi de verificaci&oacute; humana &eacute;s incorrecte</p>");
                        $("#recaptcha-error").css('display', 'block');
                        Recaptcha.reload();
                        return false;
                    }
                }
            });
        }
    </script>
</head>
<body>
    <?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <div id="page-wrap">
    <div class="contenedor-responsive">
        <div id="conttranstotalcos">
            <div id="conttranscos">
                <div id="btranspresen">
                    <div id="transimatge">
                        <img src="/images/alcalde/palcalde.jpg" class="img-slider-territori"/>
                    </div>
                    <div id="transimatgeText">
                        <h1>
                            <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

                            <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                        </h1>
                    </div>
                </div>
                <ul id='menu_planA'>
                    <?php  $_smarty_tpl->tpl_vars['itemMenu'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['itemMenu']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['MENU']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['itemMenu']->key => $_smarty_tpl->tpl_vars['itemMenu']->value) {
$_smarty_tpl->tpl_vars['itemMenu']->_loop = true;
?>
                        <a href=<?php echo $_smarty_tpl->tpl_vars['itemMenu']->value;?>
>
                            <li id='menu_planA_item'>
                                <?php echo $_smarty_tpl->tpl_vars['itemMenu']->key;?>

                            </li>
                        </a>
                    <?php } ?>
                </ul>
                <div id="tcentre_planA">
                    <p class="title escriu_alcalde"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
</p>
                    <?php if (!$_smarty_tpl->tpl_vars['request']->value) {?>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC6']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC7']->value;?>
</p>
                    <form id="EscriuAlcaldeForm" method="post" action="alccontac.php?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
" novalidate="novalidate">
                        <input type="hidden" name="accio" id="accio" value="edit_headlines">
                        <p>
                            <label><?php echo $_smarty_tpl->tpl_vars['FIELD_1']->value;?>
 *</label>
                            <input type="text" name="nom" id="nom" value="" aria-required="true" class="error" aria-invalid="true" maxlength="50">
                        </p>
                        <p>
                            <label><?php echo $_smarty_tpl->tpl_vars['FIELD_2']->value;?>
 *</label>
                            <input type="text" name="email" id="email" value="" aria-required="true" class="error" aria-invalid="true" maxlength="75">
                        </p>
                        <p>
                            <label><?php echo $_smarty_tpl->tpl_vars['FIELD_3']->value;?>
 *</label>
                            <input type="text" name="poblacio" id="poblacio" value="" aria-required="true" class="error" aria-invalid="true" maxlength="75">
                        </p>
                        <p>
                            <label><?php echo $_smarty_tpl->tpl_vars['FIELD_4']->value;?>
</label>
                            <input type="text" name="comarca" id="comarca" value="" aria-required="true" class="error" aria-invalid="true" maxlength="75">
                        </p>
                        <p>
                            <label><?php echo $_smarty_tpl->tpl_vars['FIELD_5']->value;?>
</label>
                            <input type="text" name="pais" id="pais" value="" aria-required="true" class="error" aria-invalid="true" maxlength="75">
                        </p>
                        <p>
                            <label><?php echo $_smarty_tpl->tpl_vars['FIELD_6']->value;?>
 *</label>
                            <input type="text" name="assumpte" id="assumpte" value="" aria-required="true" class="error" aria-invalid="true" maxlength="75">
                        </p>
                        <p>
                            <label><?php echo $_smarty_tpl->tpl_vars['FIELD_7']->value;?>
 *</label>
                            <textarea name="comentari" id="comentari" aria-required="true" class="error" aria-invalid="true"></textarea>
                        </p>
                        <p>
                            <?php echo $_smarty_tpl->tpl_vars['LABEL_TC3']->value;?>

                            <?php echo $_smarty_tpl->tpl_vars['recaptcha']->value;?>

                            <label id="recaptcha-error" class="error" for="recaptcha-error" style="display: none;"></label>
                        </p>
                        <p>
                            <input class="submit" type="submit" value="<?php echo $_smarty_tpl->tpl_vars['BUTTON_SUBMIT']->value;?>
">
                        </p>
                        <p>
                            <label>* <?php echo $_smarty_tpl->tpl_vars['FIELD_8_REQUIRED']->value;?>
</label>
                        </p>
                        <p class="footer-lopd">
                            <?php echo $_smarty_tpl->tpl_vars['LABEL_TC5']->value;?>

                        </p>
                    </form>
                    <?php } else { ?>
                        <?php echo $_smarty_tpl->tpl_vars['LABEL_TC4']->value;?>

                    <?php }?>
                </div>
            </div>
        </div>
        </div>
    </div>
    <?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

</body>
</html><?php }} ?>
