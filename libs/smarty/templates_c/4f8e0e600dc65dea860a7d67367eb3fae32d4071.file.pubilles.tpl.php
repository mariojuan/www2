<?php /* Smarty version Smarty-3.1.16, created on 2017-12-13 02:48:34
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/festes/cinta17/pubilles.tpl" */ ?>
<?php /*%%SmartyHeaderCode:14012806395a3086f237c1a1-41751149%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4f8e0e600dc65dea860a7d67367eb3fae32d4071' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/festes/cinta17/pubilles.tpl',
      1 => 1503487717,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14012806395a3086f237c1a1-41751149',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'image' => 0,
    'LABEL_TITOL1' => 0,
    'MENU' => 0,
    'itemMenu' => 0,
    'LABEL04' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a3086f23fefa5_06630746',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a3086f23fefa5_06630746')) {function content_5a3086f23fefa5_06630746($_smarty_tpl) {?><!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->
<div id="page-wrap">

   <div class="contenedor-responsive">

      <div id="conttranstotalcos">

         <div id="conttranscos">

            <div id="btranspresen">

               <div id="transimatge">
                  <img src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
" class="img-slider-territori"/>
               </div>

               <div id="transimatgeText">
                  <h1>
                     <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

                     <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                  </h1>
               </div>

            </div>

            <ul id='menu_planA'>
               <?php  $_smarty_tpl->tpl_vars['itemMenu'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['itemMenu']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['MENU']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['itemMenu']->key => $_smarty_tpl->tpl_vars['itemMenu']->value) {
$_smarty_tpl->tpl_vars['itemMenu']->_loop = true;
?>
               <a href=<?php echo $_smarty_tpl->tpl_vars['itemMenu']->value['link'];?>
 target="_<?php echo $_smarty_tpl->tpl_vars['itemMenu']->value['target'];?>
">
                  <li id='menu_planA_item'>
                     <?php echo $_smarty_tpl->tpl_vars['itemMenu']->value['name'];?>

                  </li>
               </a>
               <?php } ?>
            </ul>

            <div id="tcentre_planA">
               <h2 style="text-align: center"><?php echo $_smarty_tpl->tpl_vars['LABEL04']->value;?>
</h2>
               <!--<p style="text-align: center"><img src="/images/laciutat/ifestes.jpg" class="img-responsive"></p>-->

               <div id="taulapubilles">  
                  <div id="filapubilles">
                     <div class="colunapubilla">
                        <a href="/images/festes/cinta17/fpubilles/00-MBS.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/00-MBS.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">MARIA BEL SALVADÓ</div>
                        <div id="textnegreta">Reina - Cinta 2017</div>
                        <div id="textnormal">CLUB PATÍ DERTUSA</div>
                     </div>                        
                  </div>
               </div>
      
               <br>

               <div id="taulapubilles">  
                  <div id="filapubilles">
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilles/01-EZN.jpg" target="_blank">
                           
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/01-EZN.jpg" width="150" height="200">
                           
                        </a>                           
                        <br>
                        <div id="textnegreta">ESTEL ZARAGOZA NAVARRO</div>
                        <div id="textnormal">AGRUPACIÓ CENTRE COMERCIAL TORTOSA</div>
                     </div>
                     <div class="colpubilles"> 
                        <a href="/images/festes/cinta17/fpubilles/02-MGT.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/02-MGT.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">MARTA GRIFOLL TORTA</div>
                        <div id="textnormal">AGRUPACIÓ VETERANS FÚTBOL TORTOSA</div>
                     </div>
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilles/03-GDB.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/03-GDB.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">GEORGINA DUCH BARBERÀ</div>
                        <div id="textnormal">AMICS DE L'ERMITA DEL COLL DE L'ALBA</div>
                     </div>                        
                  </div>

                  <div id="filapubilles">
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilles/04-MHE.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/04-MHE.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">MARELI HOMEDES ESTAÑOL</div>
                        <div id="textnormal">AMICS DE L'ERMITA DE LA PETJA</div>                        
                     </div>
                     <div class="colpubilles"> 
                        <a href="/images/festes/cinta17/fpubilles/05-AMB.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/05-AMB.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">AINOA MAURI BURGOS</div>
                        <div id="textnormal">ASSEMBLEA LOCAL DE LA CREU ROJA</div>                        
                     </div>
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilles/06-LGB.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/06-LGB.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">LAURA GABALDON BONFILL</div>
                        <div id="textnormal">ASSOCIACIÓ AMICS DEL CAVALL EQUITOR</div>                        
                     </div>                        
                  </div>

                  <div id="filapubilles">
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilles/07-IAV.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/07-IAV.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">IRIS ALSO VILLALBA</div>
                        <div id="textnormal">ASSOCIACIÓ COLLA DE DIABLES I TAMBORS «LO GOLAFRE»</div>                        
                     </div>
                     <div class="colpubilles"> 
                        <a href="/images/festes/cinta17/fpubilles/08-LFG.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/08-LFG.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">LAURA FABREGUES GALIANA</div>
                        <div id="textnormal">ASSOCIACIÓ CPS RENAIXEMENT</div>                        
                     </div>
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilles/09-AFB.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/09-AFB.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">ALBA FABREGAT BERENGUE</div>
                        <div id="textnormal">ASSOCIACIÓ CULTURAL COLLA JOVE DE DOLÇAINERS</div>                        
                     </div>                        
                  </div>

                  <div id="filapubilles">
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilles/10-APD.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/10-APD.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">AINA PRATS DALMAU</div>
                        <div id="textnormal">ASSOCIACIÓ CULTURAL DE DANSA A TORTOSA</div>                        
                     </div>
                     <div class="colpubilles"> 
                        <a href="/images/festes/cinta17/fpubilles/11-ZGB.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/11-ZGB.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">ZAIRA GARZON BOSCH</div>
                        <div id="textnormal">ASSOCIACIÓ CULTURAL DE FERRERIES LLAMPEC NOIS</div>                        
                     </div>
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilles/12-ABM.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/12-ABM.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">ARIADNA BLANCH MONLLAO</div>
                        <div id="textnormal">ASSOCIACIÓ CULTURAL «4 MÉS 1»</div>                        
                     </div>                        
                  </div>

                  <div id="filapubilles">
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilles/13-LGV.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/13-LGV.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">LAN GONZÀLEZ VIDAL</div>
                        <div id="textnormal">ASSOCIACIÓ CULTURAL VENT FORT</div>                        
                     </div>
                     <div class="colpubilles"> 
                        <a href="/images/festes/cinta17/fpubilles/14-GCS.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/14-GCS.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">GEMMA COLOMÉ SERRANO</div>
                        <div id="textnormal">ASSOCIACIÓ ESPORTIVA DE FÚTBOL SALA «BAIX EBRE»</div>                        
                     </div>
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilles/15-CAA.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/15-CAA.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">CLAUDIA ARAUJO ALBIOL</div>
                        <div id="textnormal">ASSOCIACIÓN DE GENT GRAN ESPLAI CAIXA TORTOSA</div>                        
                     </div>                        
                  </div>

                  <div id="filapubilles">
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilles/16-AGR.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/16-AGR.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">ANNI GONZÀLEZ RAMÍREZ</div>
                        <div id="textnormal">ASSOCIACIÓ GRUP ABANDERATS DE TORTOSA</div>                        
                     </div>
                     <div class="colpubilles"> 
                        <a href="/images/festes/cinta17/fpubilles/17-CVQ.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/17-CVQ.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">CINTA VERICAT QUEROL</div>
                        <div id="textnormal">ASSOCIACIÓ JUVENIL TUNA FOLK DE TORTOSA</div>                        
                     </div>
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilles/18-LCL.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/18-LCL.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">LAURA CUGAT LLEIXÀ</div>
                        <div id="textnormal">ASSOCIACIÓ 4x4 TERRES DE L'EBRE</div>                        
                     </div>                        
                  </div>

                  <div id="filapubilles">
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilles/19-CMC.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/19-CMC.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">CINTA MEDINA CANALDA</div>
                        <div id="textnormal">ASSOCIACIÓ DE VEÏNS CENTRE-NUCLI HISTÒRIC</div>                        
                     </div>
                     <div class="colpubilles"> 
                        <a href="/images/festes/cinta17/fpubilles/20-LTP.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/20-LTP.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">LAURA TAPIA PIÑANA</div>
                        <div id="textnormal">ASSOCIACIÓ DE VEÏNS FERRERIES-SANT VICENT</div>                        
                     </div>
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilles/21-LSF.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/21-LSF.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">LAIA SUBIRATS FAVÀ</div>
                        <div id="textnormal">ASSOCIACIÓ DE VEÏNS PINTOR CASANOVA – REMOLINS</div>                        
                     </div>                        
                  </div>

                  <div id="filapubilles">
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilles/22-AQM.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/22-AQM.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">ÀNGELA QUEROL MARCH</div>
                        <div id="textnormal">ASSOCIACIÓ DE VEÏNS DEL RASTRE</div>                        
                     </div>
                     <div class="colpubilles"> 
                        <a href="/images/festes/cinta17/fpubilles/23-GMN.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/23-GMN.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">GEMMA MARTÍNEZ NAVARRO</div>
                        <div id="textnormal">ASSOCIACIÓ DE VEÏNS SANT JOSEP DE LA MUNTANYA «ILERCAVÒNIA»</div>                        
                     </div>
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilles/24-ABH.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/24-ABH.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">ANAÏS NADIA BERENGUÉ HASNAOUI</div>
                        <div id="textnormal">ASSOCIACIÓ DE VEÏNS DE SANTA CLARA</div>                        
                     </div>                        
                  </div>

                  <div id="filapubilles">
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilles/25-RMF.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/25-RMF.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">RITA MINGUELL FAIGES</div>
                        <div id="textnormal">ASSOCIACIÓ DE VEÏNS DE LA SIMPÀTICA</div>                        
                     </div>
                     <div class="colpubilles"> 
                        <a href="/images/festes/cinta17/fpubilles/26-MCG.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/26-MCG.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">MIREIA CID GINÉ</div>
                        <div id="textnormal">ASSOCIACIÓ DE VEÏNS 13 DE GENER I HORTA</div>                        
                     </div>
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilles/27-CGC.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/27-CGC.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">CARLA GALBE CASANOVA</div>
                        <div id="textnormal">ASSOCIACIÓ DE VENEDORS DEL MERCAT</div>                        
                     </div>                        
                  </div>

                  <div id="filapubilles">
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilles/28-PEG.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/28-PEG.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">PAULA ESTUPIÑÀ GILABERT</div>
                        <div id="textnormal">CANTAIRES DE L'EBRE DELTA</div>                        
                     </div>
                     <div class="colpubilles"> 
                        <a href="/images/festes/cinta17/fpubilles/29-XMR.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/29-XMR.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">XÈNIA MUÑOZ ROÈ</div>
                        <div id="textnormal">CASTELLERS DE TORTOSA</div>                        
                     </div>
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilles/30-LAC.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/30-LAC.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">LAIA AUDÍ CARLES</div>
                        <div id="textnormal">CENTRE D'ESPORTS TORTOSA</div>                        
                     </div>                        
                  </div>

                  <div id="filapubilles">
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilles/31-AAP.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/31-AAP.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">AIDA ACCENSI PELLISA</div>
                        <div id="textnormal">CLUB ATLETISME TERRES DE L'EBRE</div>                        
                     </div>
                     <div class="colpubilles"> 
                        <a href="/images/festes/cinta17/fpubilles/32-CSC.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/32-CSC.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">CLAUDIA SORNI CHAVARRIA</div>
                        <div id="textnormal">CLUB DE BALL ESPORTIU MARKING DANCE</div>                        
                     </div>
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilles/33-GAM.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/33-GAM.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">GEMMA ALONSO MESEGUER</div>
                        <div id="textnormal">CLUB BALL ESPORTIU RITME</div>                        
                     </div>                        
                  </div>

                  <div id="filapubilles">
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilles/34-AAV.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/34-AAV.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">ALEX AYMERICH VILLARROYA</div>
                        <div id="textnormal">CLUB BÀSQUET CANTAIRES</div>                        
                     </div>
                     <div class="colpubilles"> 
                        <a href="/images/festes/cinta17/fpubilles/35-MGG.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/35-MGG.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">MARIONA GUARDIA GONZÀLEZ</div>
                        <div id="textnormal">CLUB DEPORTIU TORTOSA</div>                        
                     </div>
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilles/36-ASG.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/36-ASG.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">ANDREA SALES GARCIA</div>
                        <div id="textnormal">CLUB FÚTBOL EBRE ESCOLA ESPORTIVA</div>                        
                     </div>                        
                  </div>

                  <div id="filapubilles">
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilles/37-PMV.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/37-PMV.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">PAULA MARQUES VICIENT</div>
                        <div id="textnormal">CLUB FÚTBOL FEMENÍ TORTOSA-EBRE</div>                        
                     </div>
                     <div class="colpubilles"> 
                        <a href="/images/festes/cinta17/fpubilles/38-CJV.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/38-CJV.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">CINTA JULVE VILLÓ</div>
                        <div id="textnormal">CLUB DE LEONES TORTOSA</div>                        
                     </div>
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilles/39-CLV.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/39-CLV.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">CARME LLEIXÀ VALLS</div>
                        <div id="textnormal">CLUB NATACIÓ TORTOSA</div>                        
                     </div>                        
                  </div>

                  <div id="filapubilles">
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilles/40-PCR.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/40-PCR.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">PAULA CID RODRÍGUEZ DE GUZMAN</div>
                        <div id="textnormal">CLUB DE REM TORTOSA</div>                        
                     </div>
                     <div class="colpubilles"> 
                        <a href="/images/festes/cinta17/fpubilles/41-JCM.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/41-JCM.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">JULIA CASTELLÀ MORALEJO</div>
                        <div id="textnormal">CLUB DE TENNIS TORTOSA</div>                        
                     </div>
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilles/42-ARG.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/42-ARG.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">ALBA REVERTE GASSÓ</div>
                        <div id="textnormal">CLUB TWIRLING TORTOSA</div>                        
                     </div>                        
                  </div>

                  <div id="filapubilles">
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilles/43-CMS.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/43-CMS.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">CARLA MATAMOROS SANCHIS</div>
                        <div id="textnormal">CLUB UNIVERSITARI</div>                        
                     </div>
                     <div class="colpubilles"> 
                        <a href="/images/festes/cinta17/fpubilles/44-NFP.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/44-NFP.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">NEREA FORÉS PINEDO</div>
                        <div id="textnormal">CONSELL ASSESSOR D'ELS REGUERS</div>                        
                     </div>
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilles/45-VFG.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/45-VFG.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">VICKY FERRÉ GINOVART</div>
                        <div id="textnormal">CONSELL ASSESSOR DE VINALLOP</div>                        
                     </div>                        
                  </div>

                  <div id="filapubilles">
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilles/46-APE.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/46-APE.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">AINHOA PANISELLO ESPINOSA</div>
                        <div id="textnormal">ENTITAT MUNICIPAL DESCENTRALITZADA DE CAMPREDÓ</div>                        
                     </div>
                     <div class="colpubilles"> 
                        <a href="/images/festes/cinta17/fpubilles/47-NTR.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/47-NTR.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">NEREA TOMÀS RIPOLLÉS</div>
                        <div id="textnormal">ENTITAT MUNICIPAL DESCENTRALITZADA DE JESÚS</div>                        
                     </div>
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilles/48-ABO.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/48-ABO.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">ALBA BLASCO OLIVER</div>
                        <div id="textnormal">ESCOLA DE FÚTBOL DERTUSA</div>                        
                     </div>                        
                  </div>

                  <div id="filapubilles">
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilles/49-LSC.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/49-LSC.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">LAIA SAGRERA CARRASCOSA</div>
                        <div id="textnormal">MONTEPIO DE CONDUCTORS S. CRISTÒFOL DE TORTOSA I COMARQUES</div>                        
                     </div>
                     <div class="colpubilles"> 
                        <a href="/images/festes/cinta17/fpubilles/50-ALG.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/50-ALG.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">ALBA LLORACH GARCIA</div>
                        <div id="textnormal">MOTO CLUB TORTOSA</div>                        
                     </div>
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilles/51-PVM.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/51-PVM.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">PAULA VIDAL MAQUEDA</div>
                        <div id="textnormal">MOTO CLUB TOT MOTOR ELS REGUERS</div>                        
                     </div>                        
                  </div>

                  <div id="filapubilles">
                     <div class="colpubilles"> 
                        <a href="/images/festes/cinta17/fpubilles/52-JRA.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/52-JRA.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">JÚLIA RODA ADAME</div>
                        <div id="textnormal">ORDE DE LA CUCAFERA</div>                        
                     </div>
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilles/53-CQN.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/53-CQN.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">CÀMERON QUERAL NAVARRO</div>
                        <div id="textnormal">ORFEÓ TORTOSÍ</div>                        
                     </div>                        
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilles/54-CVA.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/54-CVA.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">CINTA VERGÉS ALCON</div>
                        <div id="textnormal">PATRONAT ESCOLAR OBRER DE LA SAGRADA FAMÍLIA</div>                        
                     </div>
                  </div>

                  <div id="filapubilles">
                     <div class="colpubilles"> 
                        <a href="/images/festes/cinta17/fpubilles/55-RVM.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/55-RVM.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">RAQUEL VILLÓ MARTÍ</div>
                        <div id="textnormal">PENYA CICLISTA BAIX EBRE «JA ARRIBAREM»</div>                        
                     </div>
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilles/56-LPM.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/56-LPM.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">LAURA PITART MONLLAO</div>
                        <div id="textnormal">SOCIETAT DE CAÇADORS DE TORTOSA</div>                        
                     </div>                        
                     <div class="colpubilles">
                        <a href="/images/festes/cinta17/fpubilles/57-SOS.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/57-SOS.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">SARA ORTIGA SANCHEZ</div>
                        <div id="textnormal">TORTOSA ATHLÈTIC CLUB JUDO I JUJITSU</div>                        
                     </div>
                  </div>

                  <div id="filapubilles">
                     <div class="colpubilles"> 
                        <a href="/images/festes/cinta17/fpubilles/58-IBC.jpg" target="_blank">
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/58-IBC.jpg" width="150" height="200">
                        </a>
                        <br>
                        <div id="textnegreta">IRIS BALAGUÉ CHORTO</div>
                        <div id="textnormal">UNIÓ EXCURSIONISTA DE CATALUNYA</div>                        
                     </div>
                     <div class="colpubilles">
                     </div>                        
                     <div class="colpubilles">
                     </div>                        
                  </div>

               </div>

               <br>

               <div id="taulapubilles">  
                  <div id="filapubilles">
                     <div class="colunapubilla">
                        <!--<a href="/images/festes/cinta17/fpubilles/pubilles_grup.jpg" target="_blank">-->
                           <img class="marcpubilla" src="/images/festes/cinta17/pubilles/pubilles_grup.jpg" width="508" height="339">
                        <!--</a>-->
                        <br>
                        <div id="textnegreta">PUBILLES FESTES DE LA CINTA 2017</div>

                     </div>                        
                  </div>
               </div>




            </div>

            <div class="separator"></div>

         </div>
      </div>
   </div>
</div><?php }} ?>
