<?php /* Smarty version Smarty-3.1.16, created on 2018-03-06 09:36:01
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/pciutadana/organsparticipa_home.tpl" */ ?>
<?php /*%%SmartyHeaderCode:8095365895a307c7578c324-23707676%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '480df2089ae3dbc0eecdd96cb5403a1ed4471b82' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/pciutadana/organsparticipa_home.tpl',
      1 => 1520325358,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8095365895a307c7578c324-23707676',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a307c75894ca4_76672459',
  'variables' => 
  array (
    'image' => 0,
    'LABEL_TITOL1' => 0,
    'LABEL_TITOL1b' => 0,
    'url_organs_back' => 0,
    'MENU' => 0,
    'itemMenu' => 0,
    'LABEL_TITOL0' => 0,
    'item' => 0,
    'LABEL_TC1' => 0,
    'LABEL_TC2' => 0,
    'LABEL_TC4' => 0,
    'LABEL_TC5' => 0,
    'LABEL_TC6' => 0,
    'LABEL_TC7' => 0,
    'LABEL_TC9' => 0,
    'LABEL_TC10' => 0,
    'LABEL_TC11' => 0,
    'LABEL_TC12' => 0,
    'LABEL_TC13' => 0,
    'LABEL_TC3' => 0,
    'LABEL_TC8' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a307c75894ca4_76672459')) {function content_5a307c75894ca4_76672459($_smarty_tpl) {?><!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->
<div id="page-wrap">

   <div class="contenedor-responsive">
      <div id="conttranstotalcos">
         <div id="conttranscos">
            <div id="btranspresen">
               <div id="transimatge">			
                  <img src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
" class="img-slider-territori"/>			
               </div>
               <div id="transimatgeText">
                  <h1>
                     <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

                     <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                  </h1>
               </div>
            </div>
            <div class="subtitolinfeco" id="subtitolinfeco2">
               <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1b']->value;?>

               
            </div>
            <div class="marcback">
               <a href="<?php echo $_smarty_tpl->tpl_vars['url_organs_back']->value;?>
">
               <i class="icon-angle-double-up"></i>
               </a>
            </div>
            <ul id='menu_planA'>
               <?php  $_smarty_tpl->tpl_vars['itemMenu'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['itemMenu']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['MENU']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['itemMenu']->key => $_smarty_tpl->tpl_vars['itemMenu']->value) {
$_smarty_tpl->tpl_vars['itemMenu']->_loop = true;
?>
               <a href=<?php echo $_smarty_tpl->tpl_vars['itemMenu']->value;?>
>
                  <li id='menu_planA_item'>
                     <?php echo $_smarty_tpl->tpl_vars['itemMenu']->key;?>

                  </li>
               </a>
               <?php } ?>
            </ul>

            <div id="tcentre_planA">
               <font size=4 color=#666666 >
               <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL0']->value;?>

               </font>

               <?php if ($_smarty_tpl->tpl_vars['item']->value==1) {?>
               <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
</p>
               <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
</p>

               <?php } elseif ($_smarty_tpl->tpl_vars['item']->value==2) {?>

               <!-- Relació organs -->
                  
                  <!-- Consell ciutat -->

                  <a href="http://www.tortosa.cat/webajt/ajunta/participacio/conciu.pdf">
                     <div id="" class="" style="margin-top: 10px;">
                     <i class="icon-users colorBlack"></i>
                        <?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['LABEL_TC4']->value;?>

                     </div>
                  </a>
                  <br>
                  <!-- /Consell ciutat -->

                  <!-- Consell de pobles -->
                  <a href="http://www.tortosa.cat/webajt/ajunta/participacio/conpob.pdf">
                     <div id="" class="">
                     <i class="icon-users colorBlack"></i>
                        <?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['LABEL_TC5']->value;?>

                     </div>
                  </a>

                  <ul id="" class="">
                     <li><a href="http://www.tortosa.cat/webajt/ajunta/participacio/ConsellReguers.pdf">Els Reguers</a></li>
                     <li><a href="http://www.tortosa.cat/webajt/ajunta/participacio/ConsellVinallop.pdf">Vinallop</a></li>
                  </ul>
                  <br>

                  <!-- /Consell de pobles -->

                  <!-- Consell de barri -->
                  <a href="http://www.tortosa.cat/webajt/ajunta/participacio/conpob.pdf">
                     <div id="" class="">
                     <i class="icon-users colorBlack"></i>
                        <?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['LABEL_TC6']->value;?>

                     </div>
                  </a>
                  <br>
                  <!-- /Consell de barri -->

                  <!-- Consells sectorials -->
                  <a href="http://www.tortosa.cat/webajt/ajunta/participacio/consec.pdf">
                     <div id="" class="">
                     <i class="icon-users colorBlack"></i>
                        <?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7']->value;?>

                     </div>
                  </a>

                  <ul id="" class="">
                     <li><a href="/acciosocial/as7.php"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC9']->value;?>
</a></li>
                     <li><a href="http://www.tortosa.cat/webajt/ajunta/participacio/ConsellEscolar.pdf"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC10']->value;?>
</a></li>
                     <li><a href="http://www.museudetortosa.cat/museu/consell-assessor"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC11']->value;?>
</a></li>
                     <li><a href="http://www.tortosa.cat/webajt/ajunta/participacio/ConsellSSocials.pdf"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC12']->value;?>
</a></li>
                     <li><a href="http://www2.tortosa.cat/lgtbi/index.php"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC13']->value;?>
</a></li>
                  </ul>

                  <!-- /Consells sectorials -->

                  <!-- Comissions específiques -->
                  <a href="http://www.tortosa.cat/webajt/ajunta/participacio/comes.pdf">
                     <div id="" class="">
                     <i class="icon-users colorBlack"></i>
                        <?php echo $_smarty_tpl->tpl_vars['LABEL_TC3']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['LABEL_TC8']->value;?>

                     </div>
                  </a>
                  <!-- /Comissions específiques -->
               
               <!-- /Relació organs -->

               <?php } elseif ($_smarty_tpl->tpl_vars['item']->value==3) {?>
               <ul>
                  <li>
                     <?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
 de Vinallop
                     <br/>
                     <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/vinallop-acta-25112015.pdf" target="_blanl">2015-1</a>, <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/acta03122015_2.pdf" target="_blanl">2015-2</a>, <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/acta19052016_1.pdf" target="_blanl">2016-1</a>, <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/acta02082016_2.pdf" target="_blanl">2016-2</a>, <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/acta03112016_3.pdf" target="_blanl">2016-3</a>, <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/acta09032017_1.pdf" target="_blanl">2017-1</a>, <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/acta06072017_2.pdf" target="_blanl">2017-2</a>
                  </li>
                  <br><br>
                  <li>
                     <?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
 dels Reguers<br/>
                     <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/acta12016r.pdf" target="_blank">2016/1</a>, <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/acta22016r.pdf" target="_blank">2016/2</a>, <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/acta32017r.pdf" target="_blank">2017/3</a>
                  </li>
                  <br><br>
                  <li>
                     <?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['LABEL_TC11']->value;?>
<br/>
                     <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/acta-consell-assessor-museu-30-09-13.pdf" target="_blank">2013</a>, <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/acta-consell-assessor-museu-27-02-14.pdf" target="_blank">2014</a>, <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/acta-consell-assessor-museu-10-02-15.pdf" target="_blank">2015</a>, <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/CCE15032017_0001.pdf" target="_blank">2016</a>, <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/ConsellAssessorMuseu27-01-17.pdf" target="_blank">2017</a> 
                  </li>
                  <br><br>
                  <li>
                     <?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['LABEL_TC10']->value;?>
<br/>
                     <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/acta-reunio-26022010.pdf" target="_blank">2010/1</a>,
                     <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/acta-reunio-11062010.pdf" target="_blank">2010/2</a>,
                     <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/acta-reunio-09062011.pdf" target="_blank">2011</a>,
                     <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/acta-reunio-21062012.pdf" target="_blank">2012</a>,
                     <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/acta-reunio-09042014.pdf" target="_blank">2014</a>
                     (<a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/annex-1-2014.pdf" target="_blank">Annex 1</a>, <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/annex-2-2014.pdf" target="_blank">Annex 2</a>,
                     <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/annex-3-2014.pdf" target="_blank">Annex 3</a>),
                     <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/acta-reunio-28052015.pdf" target="_blank">2015</a>,
                     <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/acta-reunio-CEM-11-05-2016.pdf" target="_blank">2016</a>,
                     <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/CEM14-06-2017.pdf" target="_blank">2017</a>
                  </li>
                  <br><br>
                  <li>
                     <?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['LABEL_TC12']->value;?>
<br/>
                     <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/Acta1_110516_ServeisSocials.pdf" target="_blank">2016/1</a>
                  </li>
               </ul>
               <?php }?>

               <br><br><br>
            </div>

         </div>
      </div>
   </div>
</div><?php }} ?>
