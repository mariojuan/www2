<?php /* Smarty version Smarty-3.1.16, created on 2018-02-01 14:50:19
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/infeco/ep17_execGrup.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12221317045a3087d8df30b4-96203992%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5dc26da1fe71b7fa103651b4dc1cc5e193513b4d' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/infeco/ep17_execGrup.tpl',
      1 => 1517493014,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12221317045a3087d8df30b4-96203992',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a3087d8e7a0a5_53868304',
  'variables' => 
  array (
    'image' => 0,
    'LABEL_TITOL1' => 0,
    'LABEL_TC7_171' => 0,
    'lang' => 0,
    'LABEL_TC7_INTRO1' => 0,
    'LABEL_TC7_INTRO2' => 0,
    'LABEL_TC7_ENS1' => 0,
    'LABEL_TC7_ENS2' => 0,
    'LABEL_TC7_ENS3' => 0,
    'LABEL_TC7_ENS4' => 0,
    'LABEL_TC7_ENS5' => 0,
    'LABEL_TC7_ENS6' => 0,
    'LABEL_TC7_INTRO3' => 0,
    'LABEL_TC7_TITOL2' => 0,
    'LABEL_TC7_TRIM1' => 0,
    'LABEL_TC7_SUBMENU1' => 0,
    'LABEL_TC7_SUBMENU2' => 0,
    'LABEL_TC7_SUBMENU3' => 0,
    'LABEL_TC7_SUBMENU4' => 0,
    'LABEL_TC7_SUBMENU5' => 0,
    'LABEL_TC7_TRIM2' => 0,
    'LABEL_TC7_TRIM3' => 0,
    'LABEL_TC7_TRIM4' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a3087d8e7a0a5_53868304')) {function content_5a3087d8e7a0a5_53868304($_smarty_tpl) {?><html>
   <head>
      <title>
      </title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <?php echo $_smarty_tpl->getSubTemplate ("head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

   </head>
   <body>
      <?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

      <div id="page-wrap">
         <div class="contenedor-responsive">
            <div id="contexpretotalcos" class="continfecototalcos">
               <div id="conttranscos">
                  <div id="btranspresen">
                     <div id="transimatge">         
                        <img src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
" class="img-slider-territori">           
                     </div>
                     <div id="transimatgeText">
                        <h1>
                           <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

                        </h1>
                     </div>
                  </div>
                  <div class="subtitolinfeco" id="subtitolinfeco2">
                     <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_171']->value;?>

                  </div>
                  <div class="marcback">
                     <a href="infeco_ep17.php?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
">
                     <i class="icon-angle-double-up"></i>
                     </a>
                  </div>
                  <br><br>

                  <div class="info-econoc-finan-no-responsive">

                  <div id="textintro_execGrup">
                     <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_INTRO1']->value;?>
</p>
                     <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_INTRO2']->value;?>
</p>
                  </div>

                  <ul>
                     <li><?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ENS1']->value;?>
</li>
                     <li><?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ENS2']->value;?>
</li>
                     <li><?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ENS3']->value;?>
</li>
                     <li><?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ENS4']->value;?>
</li>
                     <li><?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ENS5']->value;?>
</li>
                     <li><?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ENS6']->value;?>
</li>
                  </ul>
                  <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_INTRO3']->value;?>
</p>
                  <div class="separator"></div>
                  <div id="caixaexecgrup">
                        <div id="titolcapexectrim17">
                           <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_TITOL2']->value;?>
         
                        </div>
                        <div id="titolcaptrim">
                           <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_TRIM1']->value;?>
         
                        </div>
                        <ul class="exectrim">
                              <li><a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/grup/1t/Estabilitat_financera.pdf"><i class="icon-file-pdf"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_SUBMENU1']->value;?>
</a></li>
                              <li><a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/grup/1t/Estabilitat_pressupostaria.pdf"><i class="icon-file-pdf"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_SUBMENU2']->value;?>
</a></li>
                              <li><a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/grup/1t/Morositat_1T_2017.pdf"><i class="icon-file-pdf"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_SUBMENU3']->value;?>
</a></li>
                              <li><a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/grup/1t/PMP_1T_2017_Detall.pdf"><i class="icon-file-pdf"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_SUBMENU4']->value;?>
</a></li>
                              <li><a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/grup/1t/PMP_1T_2017_Resum.pdf"><i class="icon-file-pdf"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_SUBMENU5']->value;?>
</a></li>
                        </ul>
                        <div id="titolcaptrim">
                           <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_TRIM2']->value;?>
         
                        </div>
                        <ul class="exectrim">
                              <li><a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/grup/2t/Estabilitat_financera.pdf"><i class="icon-file-pdf"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_SUBMENU1']->value;?>
</a></li>
                              <li><a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/grup/2t/Estabilitat_pressupostaria.pdf"><i class="icon-file-pdf"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_SUBMENU2']->value;?>
</a></li>
                              <li><a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/grup/2t/Morositat_2T_2017.pdf"><i class="icon-file-pdf"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_SUBMENU3']->value;?>
</a></li>
                              <li><a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/grup/2t/PMP_2T_2017_Detall.pdf"><i class="icon-file-pdf"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_SUBMENU4']->value;?>
</a></li>
                              <li><a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/grup/2t/PMP_2T_2017_Resum.pdf"><i class="icon-file-pdf"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_SUBMENU5']->value;?>
</a></li>
                        </ul>
                        <div id="titolcaptrim">
                           <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_TRIM3']->value;?>
         
                        </div>
                        <ul class="exectrim">
                              <li><a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/grup/3t/Estabilitat_financera.pdf"><i class="icon-file-pdf"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_SUBMENU1']->value;?>
</a></li>
                              <li><a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/grup/3t/Estabilitat_pressupostaria.pdf"><i class="icon-file-pdf"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_SUBMENU2']->value;?>
</a></li>
                              <li><a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/grup/3t/Morositat_3T_2017.pdf"><i class="icon-file-pdf"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_SUBMENU3']->value;?>
</a></li>
                              <li><a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/grup/3t/PMP_3T_2017_Detall.pdf"><i class="icon-file-pdf"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_SUBMENU4']->value;?>
</a></li>
                              <li><a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/grup/3t/PMP_3T_2017_Resum.pdf"><i class="icon-file-pdf"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_SUBMENU5']->value;?>
</a></li>
                        </ul>
                        <div id="titolcaptrim">
                           <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_TRIM4']->value;?>
         
                        </div>
                        <ul class="exectrim">
                              <li><a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/grup/4t/Estabilitat_financera.pdf"><i class="icon-file-pdf"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_SUBMENU1']->value;?>
</a></li>
                              <li><a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/grup/4t/Estabilitat_pressupostaria.pdf"><i class="icon-file-pdf"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_SUBMENU2']->value;?>
</a></li>
                              <li><a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/grup/4t/Morositat_4t_2017.pdf"><i class="icon-file-pdf"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_SUBMENU3']->value;?>
</a></li>
                              <li><a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/grup/4t/PMP_4T_2017_Detall.pdf"><i class="icon-file-pdf"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_SUBMENU4']->value;?>
</a></li>
                              <li><a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/grup/4t/PMP_4T_2017_Resum.pdf"><i class="icon-file-pdf"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_SUBMENU5']->value;?>
</a></li>
                        </ul>
                  </div>

                  </div>

               </div>
            </div>
         </div>
      </div>
      <?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

   </body>
</html><?php }} ?>
