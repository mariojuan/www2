<?php /* Smarty version Smarty-3.1.16, created on 2018-03-09 13:29:06
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/admin/templates/ofertes_ocupacio/edit_oferta_ocupacio.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3359609105a782a24e76946-97045870%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'db4fbdc81092596578bb7c566af18d5cbcbfe4f6' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/admin/templates/ofertes_ocupacio/edit_oferta_ocupacio.tpl',
      1 => 1520598415,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3359609105a782a24e76946-97045870',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a782a25434026_30058024',
  'variables' => 
  array (
    'item' => 0,
    'lang' => 0,
    'items_ens' => 0,
    'ens' => 0,
    'items_files' => 0,
    'item_file' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a782a25434026_30058024')) {function content_5a782a25434026_30058024($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/vhost/www2.tortosa.cat/home/html/libs/smarty/plugins/modifier.date_format.php';
?><section id="main" class="column">
   <h4 class="alert_info"><i class="icon-doc-new"></i><?php if ($_smarty_tpl->tpl_vars['item']->value[0]->ID=='') {?>Alta<?php } else { ?>Modificació<?php }?> Oferta Ocupació (<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
)</h4>
   <article class="module width_full">
      <div class="module_content">
         <form id="ofertesocupacioForm" method="post" action="oferta.php" novalidate="novalidate" enctype="multipart/form-data">
             <input type="hidden" name="accio" id="accio" value="edit_oferta">
             <input type="hidden" name="lang" id="lang" value="<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
">
             <input type="hidden" name="ID" id="ID" value="<?php echo $_smarty_tpl->tpl_vars['item']->value[0]->ID;?>
">
             <input type="hidden" name="FITXERS" id="FITXERS" value="">
             <?php if ($_smarty_tpl->tpl_vars['lang']->value=='ca') {?>
             <p>
                 <label>ENS *</label>
                 <select name="ID_ENS" id="ID_ENS">
                     <option value="">-- Seleccionar --</option>
                     <?php  $_smarty_tpl->tpl_vars['ens'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['ens']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['items_ens']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['ens']->key => $_smarty_tpl->tpl_vars['ens']->value) {
$_smarty_tpl->tpl_vars['ens']->_loop = true;
?>
                         <?php if ($_smarty_tpl->tpl_vars['ens']->value->ID==$_smarty_tpl->tpl_vars['item']->value[0]->ID_ENS) {?>
                             <option value="<?php echo $_smarty_tpl->tpl_vars['ens']->value->ID;?>
" selected><?php echo $_smarty_tpl->tpl_vars['ens']->value->DESC_CURTA;?>
</option>
                         <?php } else { ?>
                             <option value="<?php echo $_smarty_tpl->tpl_vars['ens']->value->ID;?>
"><?php echo $_smarty_tpl->tpl_vars['ens']->value->DESC_CURTA;?>
</option>
                         <?php }?>
                     <?php } ?>
                 </select>
             </p>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['lang']->value=='ca') {?>
            <p>
                <label>Número Expedient *</label>
                <input type="text" name="NUMERO_EXPEDIENT" id="NUMERO_EXPEDIENT" value="<?php echo $_smarty_tpl->tpl_vars['item']->value[0]->NUMERO_EXPEDIENT;?>
">
            </p>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['lang']->value=='ca') {?>
             <p>
                 <label>Tipus oferta *</label>
                 <select name="TIPUS_OFERTA" id="TIPUS_OFERTA">
                     <option value="">-- Seleccionar --</option>
                         <?php if ($_smarty_tpl->tpl_vars['item']->value[0]->TIPUS_OFERTA=="Places") {?>
                             <option value="Places" selected>Places</option>
                         <?php } else { ?>
                             <option value="Places">Places</option>
                         <?php }?>
                         <?php if ($_smarty_tpl->tpl_vars['item']->value[0]->TIPUS_OFERTA=="Constitucio borsa de treball") {?>
                             <option value="Constitucio borsa de treball" selected>Constitució borsa de treball</option>
                         <?php } else { ?>
                             <option value="Constitucio borsa de treball">Constitució borsa de treball</option>
                         <?php }?>
                 </select>
             </p>
            <?php }?>
             <p>
                 <label>Descripció curta *</label>
                 <input type="text" name="DESCRIPCIO_CURTA" id="DESCRIPCIO_CURTA" value="<?php echo $_smarty_tpl->tpl_vars['item']->value[0]->DESCRIPCIO_CURTA;?>
">
             </p>
             <p>
                 <label>Objecte</label>
                 <textarea name="OBJECTE" id="OBJECTE"><?php echo $_smarty_tpl->tpl_vars['item']->value[0]->OBJECTE;?>
</textarea>
             </p>
             <p>
                 <label>Denominació *</label>
                 <input type="text" name="DENOMINACIO" id="DENOMINACIO" value="<?php echo $_smarty_tpl->tpl_vars['item']->value[0]->DENOMINACIO;?>
">
             </p>
             <p>
                 <label>Regim jurídic *</label>
                 <input type="text" name="REGIM_JURIDIC" id="REGIM_JURIDIC" value="<?php echo $_smarty_tpl->tpl_vars['item']->value[0]->REGIM_JURIDIC;?>
">
             </p>
             <p>
                 <label>Caràcter *</label>
                 <input type="text" name="CARACTER" id="CARACTER" value="<?php echo $_smarty_tpl->tpl_vars['item']->value[0]->CARACTER;?>
">
             </p>
             <p>
                 <label>Escala</label>
                 <input type="text" name="ESCALA" id="ESCALA" value="<?php echo $_smarty_tpl->tpl_vars['item']->value[0]->ESCALA;?>
">
             </p>
             <p>
                 <label>Subescala</label>
                 <input type="text" name="SUBESCALA" id="SUBESCALA" value="<?php echo $_smarty_tpl->tpl_vars['item']->value[0]->SUBESCALA;?>
">
             </p>
             <p>
                 <label>Classe</label>
                 <input type="text" name="CLASSE" id="CLASSE" value="<?php echo $_smarty_tpl->tpl_vars['item']->value[0]->CLASSE;?>
">
             </p>
             <p>
                 <label>Grup de Classificació *</label>
                 <input type="text" name="GRUP" id="GRUP" value="<?php echo $_smarty_tpl->tpl_vars['item']->value[0]->GRUP;?>
">
             </p>
             <?php if ($_smarty_tpl->tpl_vars['lang']->value=='ca') {?>
             <p>
                 <label>Número de Places *</label>
                 <input type="text" name="NUM_PLACES" id="NUM_PLACES" value="<?php echo $_smarty_tpl->tpl_vars['item']->value[0]->NUM_PLACES;?>
">
             </p>
             <?php }?>
             <p>
                 <label>Sistema de Selecció *</label>
                 <input type="text" name="SISTEMA_SELECCIO" id="SISTEMA_SELECCIO" value="<?php echo $_smarty_tpl->tpl_vars['item']->value[0]->SISTEMA_SELECCIO;?>
">
             </p>
             <p>
                 <label>Requisits</label>
                 <textarea name="REQUISITS" id="REQUISITS"><?php echo $_smarty_tpl->tpl_vars['item']->value[0]->REQUISITS;?>
</textarea>
             </p>
             <?php if ($_smarty_tpl->tpl_vars['lang']->value=='ca') {?>
             <p>
                 <label style="width: 130px">Data Inici Presentació Sol·licituds</label>
                 <input type="text" name="DATA_INICI_PRESENT_SOLLICITUDS" id="DATA_INICI_PRESENT_SOLLICITUDS" value="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['item']->value[0]->DATA_INICI_PRESENT_SOLLICITUDS,"%d/%m/%G");?>
">
             </p>
             <?php }?>
             <?php if ($_smarty_tpl->tpl_vars['lang']->value=='ca') {?>
                 <p>
                     <label style="width: 130px">Data Fi Presentació Sol·licituds</label>
                     <input type="text" name="DATA_FI_PRESENT_SOLLICITUDS" id="DATA_FI_PRESENT_SOLLICITUDS" value="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['item']->value[0]->DATA_FI_PRESENT_SOLLICITUDS,"%d/%m/%G");?>
">
                 </p>
             <?php }?>
             <p>
                 <label>Drets Examen</label>
                 <textarea name="DRETS_EXAMEN" id="DRETS_EXAMEN"><?php echo $_smarty_tpl->tpl_vars['item']->value[0]->DRETS_EXAMEN;?>
</textarea>
             </p>
             <?php if ($_smarty_tpl->tpl_vars['lang']->value=='ca') {?>
                 <p>
                     <label style="width: 130px">Data Inici Presentació Alegacions</label>
                     <input type="text" name="DATA_INICI_PRESENT_ALEGACIONS" id="DATA_INICI_PRESENT_ALEGACIONS" value="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['item']->value[0]->DATA_INICI_PRESENT_ALEGACIONS,"%d/%m/%G");?>
">
                 </p>
             <?php }?>
             <?php if ($_smarty_tpl->tpl_vars['lang']->value=='ca') {?>
                 <p>
                     <label style="width: 130px">Data Fi Presentació Alegacions</label>
                     <input type="text" name="DATA_FI_PRESENT_ALEGACIONS" id="DATA_FI_PRESENT_ALEGACIONS" value="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['item']->value[0]->DATA_FI_PRESENT_ALEGACIONS,"%d/%m/%G");?>
">
                 </p>
             <?php }?>
             <?php if ($_smarty_tpl->tpl_vars['lang']->value=='ca') {?>
                 <p>
                     <label style="width: 130px">Data Inici Vigència Borsa Treball</label>
                     <input type="text" name="DATA_INICI_VIGENCIA_BORSA_TREBALL" id="DATA_INICI_VIGENCIA_BORSA_TREBALL" value="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['item']->value[0]->DATA_INICI_VIGENCIA_BORSA_TREBALL,"%d/%m/%G");?>
">
                 </p>
             <?php }?>
             <?php if ($_smarty_tpl->tpl_vars['lang']->value=='ca') {?>
                 <p>
                     <label style="width: 130px">Duració Borsa Treball</label>
                     <input type="text" name="DURACIO_BORSA_TREBALL" id="DURACIO_BORSA_TREBALL" value="<?php echo $_smarty_tpl->tpl_vars['item']->value[0]->DURACIO_BORSA_TREBALL;?>
">
                 </p>
             <?php }?>
             <?php if ($_smarty_tpl->tpl_vars['lang']->value=='ca') {?>
                 <p>
                     <label style="width: 130px">Data Inici Visualització</label>
                     <input type="text" name="DATA_INICI_VISUALITZACIO" id="DATA_INICI_VISUALITZACIO" value="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['item']->value[0]->DATA_INICI_VISUALITZACIO,"%d/%m/%G");?>
">
                 </p>
             <?php }?>
             <?php if ($_smarty_tpl->tpl_vars['lang']->value=='ca') {?>
                 <p>
                     <label style="width: 130px">Data Fi Visualització</label>
                     <input type="text" name="DATA_FI_VISUALITZACIO" id="DATA_FI_VISUALITZACIO" value="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['item']->value[0]->DATA_FI_VISUALITZACIO,"%d/%m/%G");?>
">
                 </p>
             <?php }?>
             <?php if ($_smarty_tpl->tpl_vars['lang']->value=='ca') {?>
             <p>
                 <label>Estat *</label>
                 <select name="ESTAT" id="ESTAT">
                     <option value="">-- Seleccionar --</option>
                     <?php if ($_smarty_tpl->tpl_vars['item']->value[0]->ESTAT=="Aprovacio bases") {?>
                         <option value="Aprovacio bases" selected>Aprovació bases</option>
                     <?php } else { ?>
                         <option value="Aprovacio bases">Aprovacio bases</option>
                     <?php }?>
                     <?php if ($_smarty_tpl->tpl_vars['item']->value[0]->ESTAT=="Pendent convocatoria") {?>
                         <option value="Pendent convocatoria" selected>Pendent convocatòria</option>
                     <?php } else { ?>
                         <option value="Pendent convocatoria">Pendent convocatòria</option>
                     <?php }?>
                     <?php if ($_smarty_tpl->tpl_vars['item']->value[0]->ESTAT=="Obert periode de sollicituds") {?>
                         <option value="Obert periode de sollicituds" selected>Obert període de sol·licituds</option>
                     <?php } else { ?>
                         <option value="Obert periode de sollicituds">Obert període de sol·licituds</option>
                     <?php }?>
                     <?php if ($_smarty_tpl->tpl_vars['item']->value[0]->ESTAT=="En proces de seleccio") {?>
                         <option value="En proces de seleccio" selected>En procés de selecció</option>
                     <?php } else { ?>
                         <option value="En proces de seleccio">En procés de selecció</option>
                     <?php }?>
                     <?php if ($_smarty_tpl->tpl_vars['item']->value[0]->ESTAT=="Borsa vigent") {?>
                         <option value="Borsa vigent" selected>Borsa vigent</option>
                     <?php } else { ?>
                         <option value="Borsa vigent">Borsa vigent</option>
                     <?php }?>
                     <?php if ($_smarty_tpl->tpl_vars['item']->value[0]->ESTAT=="Borsa no vigent") {?>
                         <option value="Borsa no vigent" selected>Borsa no vigent</option>
                     <?php } else { ?>
                         <option value="Borsa no vigent">Borsa no vigent</option>
                     <?php }?>
                 </select>
             </p>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['lang']->value=='ca') {?>
            <p>
                <label>Descripció fitxer</label>
                <input type="text" name="DESCRIPCIO" id="DESCRIPCIO" value="" class="file">
            </p>
            <p>
                <label>Fitxer *</label>
                <input type="file" name="fitxer" id="fitxer" value="" class="fitxer">
            </p>
            <p>
                <i class="icon-upload-outline icon icon-upload"></i>
            </p>
            <div id="file_list">
            <?php  $_smarty_tpl->tpl_vars['item_file'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item_file']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['items_files']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item_file']->key => $_smarty_tpl->tpl_vars['item_file']->value) {
$_smarty_tpl->tpl_vars['item_file']->_loop = true;
?>
                <div class="uploaded_file">
                    <div>
                        <i class="icon-file-pdf"></i><?php echo $_smarty_tpl->tpl_vars['item_file']->value->DESCRIPCIO;?>

                    </div>
                    <div>
                        <i class="icon-trash-empty" id="<?php echo $_smarty_tpl->tpl_vars['item_file']->value->NOM_FITXER;?>
"></i>
                    </div>
                </div>
            <?php } ?>
            </div>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['lang']->value=='ca') {?>
            <p>
                <label>Baixa</label>
                <input type="checkbox" name="BAIXA" id="BAIXA" <?php if ($_smarty_tpl->tpl_vars['item']->value[0]->BAIXA) {?>checked <?php }?>>
            </p>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['lang']->value=='ca') {?>
            <p>
                <label>Visible</label>
                <input type="checkbox" name="VISIBLE" id="VISIBLE" <?php if ($_smarty_tpl->tpl_vars['item']->value[0]->VISIBLE) {?>checked <?php }?>>
            </p>
            <?php }?>
            <p>
               <input class="submit" type="submit" value="  <?php if ($_smarty_tpl->tpl_vars['item']->value[0]->ID=='') {?>Alta<?php } else { ?>Modificació<?php }?>  ">
            </p>
            <p>
               <label>* Camps obligatoris</label>
            </p>
         </form>
      </div>
   </article>

   <!-- end of styles article -->
   <div class="spacer"></div>
</section>
<script type="text/javascript">
    $().ready(function() {
        $( "#DATA_INICI_PRESENT_SOLLICITUDS" ).datepicker();
        $( "#DATA_FI_PRESENT_SOLLICITUDS" ).datepicker();
        $( "#DATA_INICI_PRESENT_ALEGACIONS" ).datepicker();
        $( "#DATA_FI_PRESENT_ALEGACIONS" ).datepicker();
        $( "#DATA_INICI_VIGENCIA_BORSA_TREBALL" ).datepicker();
        $( "#DATA_INICI_VISUALITZACIO" ).datepicker();
        $( "#DATA_FI_VISUALITZACIO" ).datepicker();

        $("#ofertesocupacioForm").validate({
            rules: {
                ID_ENS:  {
                    required: true
                },
                NUMERO_EXPEDIENT:  {
                    required: true
                },
                TIPUS_OFERTA:  {
                    required: true
                },
                DENOMINACIO: {
                    required: true
                },
                REGIM_JURIDIC:  {
                    required: true
                },
                CARACTER:  {
                    required: true
                },
                GRUP:  {
                    required: true
                },
                NUM_PLACES:  {
                    required: true
                },
                ESTAT:  {
                    required: true
                }
            },
            messages: {
                ID_ENS:  {
                    required: "El camp de ENS és obligatori"
                },
                NUMERO_EXPEDIENT:  {
                    required: "El camp de Número d'Expedient és obligatori"
                },
                TIPUS_OFERTA:  {
                    required: "El camp de Tipus d'Oferta és obligatori"
                },
                DENOMINACIO:  {
                    required: "El camp de Denominació és obligatori"
                },
                REGIM_JURIDIC:  {
                    required: "El camp de Règim Jurídic és obligatori"
                },
                CARACTER:  {
                    required: "El camp de Caràcter és obligatori"
                },
                GRUP:  {
                    required: "El camp de Grup és obligatori"
                },
                NUM_PLACES:  {
                    required: "El camp de Número Places és obligatori"
                },
                ESTAT:  {
                    required: "El camp d'Estat és obligatori"
                }
            },
            submitHandler: function(form) {
                if ($('#BAIXA').prop('checked')) {
                    $('#BAIXA').val("1");
                }
                else {
                    $('#BAIXA').val("0");
                }
                if($('#VISIBLE').prop('checked')) {
                    $('#VISIBLE').val("1");
                }
                else {
                    $('#VISIBLE').val("0");
                }
                var fitxers_arr = new Array();
                $('.icon-trash-empty').each(function() {
                    fitxers_arr.push($(this).attr('id'));
                });
                $('#FITXERS').val(fitxers_arr.toString());
                form.submit();
            }
        });
        $('i.icon-upload').click(function() {
            if($('#DESCRIPCIO').val()!='' && ($('#fitxer').val()!='')) {
                var file_data = $('#fitxer').prop('files')[0];
                var form_data = new FormData();
                form_data.append('accio', 'upload');
                form_data.append('file', file_data);
                form_data.append('descripcio', $('#DESCRIPCIO').val());
                form_data.append('carpeta', 'ofertes_ocupacio');
                form_data.append('taula_bbdd', 'OO_OFERTA_OCUPACIO');
                <?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['item']->value[0]->ID;?>
<?php $_tmp1=ob_get_clean();?><?php if ($_tmp1=='') {?>
                form_data.append('id_taula_bbdd', '0');
                <?php } else { ?>
                form_data.append('id_taula_bbdd', '<?php echo $_smarty_tpl->tpl_vars['item']->value[0]->ID;?>
');
                <?php }?>
                //alert(form_data);
                $.ajax({
                    url: 'http://www2.tortosa.cat/admin/upload-files.php', // point to server-side PHP script
                    dataType: 'text',  // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success: function(php_script_response){
                        if(php_script_response!="") {
                            var content = '<div class="uploaded_file"><div><i class="icon-file-pdf"></i>'+$('#DESCRIPCIO').val()+'</div><div><i class="icon-trash-empty" id="'+php_script_response+'"></i></div></div>';
                            $('#file_list').append(content);
                            $('#DESCRIPCIO').val("");
                            $('#fitxer').val("");
                        }
                    }
                });
            }
        });
    });
    $(document).on('click', '.icon-trash-empty', function() {
        var form_data = new FormData();
        <?php if ($_smarty_tpl->tpl_vars['item']->value[0]->ID) {?>
        form_data.append('accio', 'disable');
        <?php } else { ?>
        form_data.append('accio', 'delete');
        <?php }?>
        form_data.append('carpeta', 'ofertes_ocupacio');
        form_data.append('taula_bbdd', 'OO_OFERTA_OCUPACIO');
        form_data.append('file', $(this).attr('id'));
        var elem = $(this);
        $.ajax({
            url: 'http://www2.tortosa.cat/admin/upload-files.php', // point to server-side PHP script
            dataType: 'text',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function(php_script_response) {
                if(php_script_response=="1") {
                    elem.parent().parent().remove();
                }
            }
        });
    });
</script><?php }} ?>
