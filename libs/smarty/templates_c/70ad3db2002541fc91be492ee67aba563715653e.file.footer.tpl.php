<?php /* Smarty version Smarty-3.1.16, created on 2017-12-12 14:57:25
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/footer.tpl" */ ?>
<?php /*%%SmartyHeaderCode:4833594885a2fe045cb56c2-79482047%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '70ad3db2002541fc91be492ee67aba563715653e' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/footer.tpl',
      1 => 1497942645,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4833594885a2fe045cb56c2-79482047',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'lang' => 0,
    'ca' => 0,
    'LABEL_POLPRIV' => 0,
    'LABEL_AVISL' => 0,
    'LABEL_COP' => 0,
    'LABEL_WEB_OPTIMITZADA' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a2fe045cfd7f7_56825437',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a2fe045cfd7f7_56825437')) {function content_5a2fe045cfd7f7_56825437($_smarty_tpl) {?><div id="footer">
	<div id="contenidortotalpeu">
		<div id="contenidorpeu">
			<div id="privacitat">
				<a href="/docs/<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
_pprivacitat.pdf" hreflang="<?php echo $_smarty_tpl->tpl_vars['ca']->value;?>
" target="_target">
				<?php echo $_smarty_tpl->tpl_vars['LABEL_POLPRIV']->value;?>

				</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="/docs/<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
_alegal.pdf" hreflang="<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
" target="_blank">
				<?php echo $_smarty_tpl->tpl_vars['LABEL_AVISL']->value;?>

				</a>
			</div>
			<div id="credencials">
				<p>&copy; 
				<?php echo $_smarty_tpl->tpl_vars['LABEL_COP']->value;?>

				</p>
				<p>Pl. d'Espanya, 1 | 43500 Tortosa | Telf. 977 58 58 00 |
                    <a href="https://www.facebook.com/ajuntamentdetortosa/" target="_blank" title="facebook">
                        <i class="icon-facebook-official"></i>
                    </a>
                    <a href="https://twitter.com/Tortosa?ref_src=twsrc%5Etfw" target="_blank" title="twitter">
                        <i class="icon-twitter"></i>
                    </a>
                </p>
			</div>
            <div class="optimitzacio-navegadors">
                <?php echo $_smarty_tpl->tpl_vars['LABEL_WEB_OPTIMITZADA']->value;?>

            </div>
		</div>
	</div>
</div><?php }} ?>
