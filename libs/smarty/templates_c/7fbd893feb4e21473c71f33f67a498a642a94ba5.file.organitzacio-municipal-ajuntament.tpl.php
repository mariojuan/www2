<?php /* Smarty version Smarty-3.1.16, created on 2017-12-18 12:19:31
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/organitzacio-municipal-ajuntament/organitzacio-municipal-ajuntament.tpl" */ ?>
<?php /*%%SmartyHeaderCode:20710964465a2ff15fb3f303-33950360%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7fbd893feb4e21473c71f33f67a498a642a94ba5' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/organitzacio-municipal-ajuntament/organitzacio-municipal-ajuntament.tpl',
      1 => 1513595968,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '20710964465a2ff15fb3f303-33950360',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a2ff15fbc9259_05883542',
  'variables' => 
  array (
    'image' => 0,
    'LABEL_TITOL1' => 0,
    'LABEL_TC0' => 0,
    'LABEL_TC1' => 0,
    'lang' => 0,
    'LABEL_TC2' => 0,
    'LABEL_TC3' => 0,
    'LABEL_TC4' => 0,
    'LABEL_TC5' => 0,
    'LABEL_TC6' => 0,
    'LABEL_TC7' => 0,
    'LABEL_CUADRE_PLE' => 0,
    'LABEL_CUADRE_INFORMATIVES' => 0,
    'LABEL_CUADRE_ESPECIAL_COMPTES' => 0,
    'LABEL_CUADRE_PORTAVEUS' => 0,
    'LABEL_CUADRE_COORDINACIO_POBLES' => 0,
    'LABEL_CUADRE_COORDINACIO_BARRIS' => 0,
    'LABEL_CUADRE_ORGANS_PARTICIPACIO' => 0,
    'LABEL_CUADRE_ALCALDIA' => 0,
    'LABEL_CUADRE_TINENCIES' => 0,
    'LABEL_CUADRE_JUNTA_GOV_LOCAL' => 0,
    'LABEL_CUADRE_ORGANIT_GOVERN_MUNICIPAL' => 0,
    'LABEL_CUADRE_ESTRUCT_ORGANITZ_ADMIN' => 0,
    'LABEL_CUADRE_EMPLEATS_PUBLICS' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a2ff15fbc9259_05883542')) {function content_5a2ff15fbc9259_05883542($_smarty_tpl) {?><html>
   <head>
      <?php echo $_smarty_tpl->getSubTemplate ("head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

   </head>
   <body>
      <?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

      <div id="page-wrap">

      <div class="contenedor-responsive">

         <div id="conttranstotalcos">
            <div id="conttranscos">
               <div id="btranspresen">
                  <div id="transimatge">
                     <img src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
" class="img-slider-territori"/>
                  </div>
                  <div id="transimatgeText">
                     <h1>
                        <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

                     </h1>
                  </div>
               </div>
               <div id="tcentre_municipi">
                  <font size="4" color="#666666"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC0']->value;?>
</font>
                  <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
</p>
                  <ul>
                     <li><a href="http://www2.tortosa.cat/organs/index.php?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
" target=""><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
</a></li>
                     <li><a href="http://www2.tortosa.cat/organitzacio-govern-municipal/index.php?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
" target=""><?php echo $_smarty_tpl->tpl_vars['LABEL_TC3']->value;?>
</a></li>
                     <li><?php echo $_smarty_tpl->tpl_vars['LABEL_TC4']->value;?>
</li>
                     <li><?php echo $_smarty_tpl->tpl_vars['LABEL_TC5']->value;?>
</li>
                  </ul>
                  <!-- <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC6']->value;?>
 <a href="" target="_blank"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC7']->value;?>
</a></p> -->
                  <img src="http://www.tortosa.cat/webajt/ajunta/om/Estructura8.jpg" usemap="#Map" class="img-responsive"/>

                  <p>Items informatius</p>
                     <ul>
                        <li>
                           <a href="<?php echo $_smarty_tpl->tpl_vars['LABEL_CUADRE_PLE']->value;?>
" target="_self" alt="El Ple">El Ple</a>
                           <ul>
                              <li>
                                 <a href="<?php echo $_smarty_tpl->tpl_vars['LABEL_CUADRE_INFORMATIVES']->value;?>
" target="_blank" alt="Comissions Informatives">Comissions Informatives</a>
                              </li>
                              <li>
                                 <a href="<?php echo $_smarty_tpl->tpl_vars['LABEL_CUADRE_ESPECIAL_COMPTES']->value;?>
" target="_blank" alt="Comissió Especial de Comptes">Comissió Especial de Comptes</a>
                              </li>
                              <li>
                                 <a href="<?php echo $_smarty_tpl->tpl_vars['LABEL_CUADRE_PORTAVEUS']->value;?>
" target="_blank" alt="Junta de portaveus">Junta de portaveus</a>
                              </li>
                              <li>
                                 <a  href="<?php echo $_smarty_tpl->tpl_vars['LABEL_CUADRE_COORDINACIO_POBLES']->value;?>
" target="_blank" alt="Junta Coordinació de govern als pobles">Junta Coordinació de govern als pobles </a>
                              </li>
                              <li>
                                 <a href="<?php echo $_smarty_tpl->tpl_vars['LABEL_CUADRE_COORDINACIO_BARRIS']->value;?>
" target="_blank" alt="Junta Coordinació de govern als barris">Junta Coordinació de govern als barris</a>
                              </li>
                              <li>
                                 <a href="<?php echo $_smarty_tpl->tpl_vars['LABEL_CUADRE_ORGANS_PARTICIPACIO']->value;?>
" target="_blank" alt="Òrgans de participació">Òrgans de participació</a>
                              </li>
                           </ul>
                        </li>
                        <li>
                           <a href="<?php echo $_smarty_tpl->tpl_vars['LABEL_CUADRE_ALCALDIA']->value;?>
" target="_self" alt="Alcaldia-Presidència">Alcaldia-Presidència</a>
                           <ul>
                              <li>
                                 <a href="<?php echo $_smarty_tpl->tpl_vars['LABEL_CUADRE_TINENCIES']->value;?>
" target="_blank" alt="Tinències d'Alcaldia">Tinències d'Alcaldia</a>
                              </li>
                           </ul>
                        </li>
                        <li>
                           <a href="<?php echo $_smarty_tpl->tpl_vars['LABEL_CUADRE_JUNTA_GOV_LOCAL']->value;?>
" target="_self" alt="Junta Govern Local">Junta Govern Local</a>
                        </li>
                        <li>
                           <a href="<?php echo $_smarty_tpl->tpl_vars['LABEL_CUADRE_ORGANIT_GOVERN_MUNICIPAL']->value;?>
" target="_self" alt="Organització del govern municipal">Organització del govern municipal</a>
                        </li>
                        <li>
                           <a href="<?php echo $_smarty_tpl->tpl_vars['LABEL_CUADRE_ESTRUCT_ORGANITZ_ADMIN']->value;?>
" target="_self" alt="Estructura organitzativa de l'Administració">Estructura organitzativa de l'Administració</a>
                        </li>
                        <li>
                           <a href="<?php echo $_smarty_tpl->tpl_vars['LABEL_CUADRE_EMPLEATS_PUBLICS']->value;?>
" target="_self" alt="Empleats Públics">Empleats Públics</a>
                        </li>
                     </ul>
               </div>
               <div class="separator"></div>
            </div>
         </div>

         </div>

      </div>
      <?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

   </body>
</html><?php }} ?>
