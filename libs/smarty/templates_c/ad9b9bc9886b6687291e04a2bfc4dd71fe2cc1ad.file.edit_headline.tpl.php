<?php /* Smarty version Smarty-3.1.16, created on 2017-12-15 13:29:21
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/admin/templates/edit_headline.tpl" */ ?>
<?php /*%%SmartyHeaderCode:17708625475a33c021e5ede4-38171583%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ad9b9bc9886b6687291e04a2bfc4dd71fe2cc1ad' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/admin/templates/edit_headline.tpl',
      1 => 1498568458,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '17708625475a33c021e5ede4-38171583',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'item' => 0,
    'items_tipologia' => 0,
    'fila' => 0,
    'items_tematica' => 0,
    'items_tematica_headlines' => 0,
    'item_tematica_headlines' => 0,
    'selected_value' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a33c02211fbd3_49063634',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a33c02211fbd3_49063634')) {function content_5a33c02211fbd3_49063634($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/vhost/www2.tortosa.cat/home/html/libs/smarty/plugins/modifier.date_format.php';
?><section id="main" class="column">

<h4 class="alert_info"><i class="icon-doc-new"></i>Alta headline</h4>
<article class="module width_full">
    <div class="module_content">
        <form id="headlinesForm" method="post" action="headlines.php" novalidate="novalidate" enctype="multipart/form-data">
            <input type="hidden" name="accio" id="accio" value="edit_headlines">
            <p>
                <label>Títol *</label>
                <input type="text" name="TITOL" id="TITOL" value="<?php echo $_smarty_tpl->tpl_vars['item']->value->TITOL;?>
">
            </p>
            <p>
                <label>Subtítol *</label>
                <textarea name="SUBTITOL" id="SUBTITUOL"><?php echo $_smarty_tpl->tpl_vars['item']->value->SUBTITOL;?>
</textarea>
            </p>
            <p>
                <label>Descripció *</label>
                <textarea name="DESCRIPCIO" id="DESCRIPCIO"><?php echo $_smarty_tpl->tpl_vars['item']->value->DESCRIPCIO;?>
</textarea>
            </p>
            <p>
                <label>Imatge</label>
                <?php if (trim($_smarty_tpl->tpl_vars['item']->value->FOTO)!='') {?>
                    <?php if ($_smarty_tpl->tpl_vars['item']->value->IMPORT==1) {?>
                        <span class="foto"><img src="http://www.tortosa.cat/webajt/gestiointerna/headlines/fotos/<?php echo $_smarty_tpl->tpl_vars['item']->value->FOTO;?>
"></span>
                    <?php } else { ?>
                        <span class="foto"><img src="/images/headlines/res_<?php echo $_smarty_tpl->tpl_vars['item']->value->FOTO;?>
"></span>
                    <?php }?>
                <?php } else { ?>
                    <span class="foto"><img src="/images/headlines/inotimage.jpg"></span>
                <?php }?>
                <input type="file" name="FOTO" id="FOTO">
                <input type="hidden" name="FOTO_AUX" id="FOTO_AUX" value="<?php echo $_smarty_tpl->tpl_vars['item']->value->FOTO;?>
">
            </p>
            <?php if ($_smarty_tpl->tpl_vars['item']->value->FOTO!='') {?>
            <p>
                <label>Esborrar foto</label>
                <input type="checkbox" name="delete_foto" id="delete_foto">
            </p>
            <?php }?>
            <p>
                <label>Enllaç</label>
                <input type="text" name="LINK" id="LINK" value="<?php echo $_smarty_tpl->tpl_vars['item']->value->LINK;?>
">
            </p>
            <p>
                <label>Data</label>
                <input type="text" name="DATA" id="DATA" value="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['item']->value->DATA,"%d/%m/%G");?>
">
            </p>
            <p>
                <label>Data inici visualització</label>
                <input type="text" name="DIV" id="DIV" value="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['item']->value->DIV,"%d/%m/%G");?>
">
            </p>
            <p>
                <label>Data fi visualització</label>
                <input type="text" name="DFV" id="DFV" value="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['item']->value->DFV,"%d/%m/%G");?>
">
            </p>
            <p>
                <label>Fitxer adjunt</label>
                <input type="file" name="ADJUNT" id="ADJUNT">
                <input type="hidden" name="ADJUNT_AUX" id="ADJUNT_AUX" value="<?php echo $_smarty_tpl->tpl_vars['item']->value->ADJUNT;?>
">
                <?php if ($_smarty_tpl->tpl_vars['item']->value->ADJUNT!='') {?>
                    <div class="adjunt"><?php echo $_smarty_tpl->tpl_vars['item']->value->ADJUNT;?>
</div><?php }?>
            </p>
            <?php if ($_smarty_tpl->tpl_vars['item']->value->ADJUNT!='') {?>
                <p>
                    <label>Esborrar fitxer adjunt</label>
                    <input type="checkbox" name="delete_adjunt" id="delete_adjunt">
                </p>
            <?php }?>
            <p>
                <label>Tipologia *</label>
                <select name="ID_TIPOLOGIA" id="ID_TIPOLOGIA">
                    <option value="">-- Seleccionar --</option>
                <?php  $_smarty_tpl->tpl_vars['fila'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['fila']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['items_tipologia']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['fila']->key => $_smarty_tpl->tpl_vars['fila']->value) {
$_smarty_tpl->tpl_vars['fila']->_loop = true;
?>
                    <?php if ($_smarty_tpl->tpl_vars['item']->value->ID_TIPOLOGIA==$_smarty_tpl->tpl_vars['fila']->value->ID) {?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['fila']->value->ID;?>
" selected><?php echo $_smarty_tpl->tpl_vars['fila']->value->TIPOLOGIA;?>
</option>
                    <?php } else { ?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['fila']->value->ID;?>
"><?php echo $_smarty_tpl->tpl_vars['fila']->value->TIPOLOGIA;?>
</option>
                    <?php }?>
                   
                <?php } ?> 
                </select>
            </p>
            <p>
                <label>Tipologia</label>
                <select name="ID_TEMATICA[]" id="ID_TEMATICA" multiple>
                    <option value="">-- Seleccionar --</option>
                    <?php  $_smarty_tpl->tpl_vars['fila'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['fila']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['items_tematica']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['fila']->key => $_smarty_tpl->tpl_vars['fila']->value) {
$_smarty_tpl->tpl_vars['fila']->_loop = true;
?>
                        <?php  $_smarty_tpl->tpl_vars['item_tematica_headlines'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item_tematica_headlines']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['items_tematica_headlines']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item_tematica_headlines']->key => $_smarty_tpl->tpl_vars['item_tematica_headlines']->value) {
$_smarty_tpl->tpl_vars['item_tematica_headlines']->_loop = true;
?>
                            <?php if ($_smarty_tpl->tpl_vars['fila']->value->ID==$_smarty_tpl->tpl_vars['item_tematica_headlines']->value->ID_TEMATICA) {?>
                                <?php $_smarty_tpl->tpl_vars["selected_value"] = new Smarty_variable("selected", null, 0);?>
                                <?php break 1?>
                            <?php } else { ?>
                                <?php $_smarty_tpl->tpl_vars["selected_value"] = new Smarty_variable('', null, 0);?>
                            <?php }?>
                        <?php } ?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['fila']->value->ID;?>
" <?php echo $_smarty_tpl->tpl_vars['selected_value']->value;?>
><?php echo $_smarty_tpl->tpl_vars['fila']->value->TEMATICA;?>
</option>
                    <?php } ?>
                </select>
            </p>
            <p>
                <label>Actiu</label>
                <input type="checkbox" name="ACTIVA" id="ACTIVA" <?php if ($_smarty_tpl->tpl_vars['item']->value->ACTIVA==1) {?> checked <?php }?>>
            </p>
            <p>
                <input class="submit" type="submit" value="Submit">
            </p>
            <p>
                <label>* Camps obligatoris</label>
            </p>
            <input type="hidden" name="ADJUNT" id="ADJUNT" value="">
            <input type="hidden" name="FOTO" id="FOTO" value="">
            <input type="hidden" name="ID" id="ID" value="<?php echo $_smarty_tpl->tpl_vars['item']->value->ID;?>
">
        </form>
    </div>
</article>

<!-- end of styles article -->


<div class="spacer"></div>
</section>

<script>
    /*
    $.validator.setDefaults({
        submitHandler: function() {
            alert("submitted!");
        }
    });
    */

    $().ready(function() {
        $( "#DATA" ).datepicker();
        $( "#DIV" ).datepicker();
        $( "#DFV" ).datepicker();


        // validate signup form on keyup and submit
        
        $("#headlinesForm").validate({
            rules: {
                TITOL:  {
                    required: true,
                    maxlength: 150
                },
                SUBTITOL: "required",
                DESCRIPCIO: "required",
                ID_TIPOLOGIA: "required"
            },
            messages: {
                TITOL: {
                    required: "El camp de títol és obligatori",
                    maxlength: "El camp de títol és massa llarg"
                },
                SUBTITOL: "El camp de subtítol és obligatori",
                DESCRIPCIO: "El camp de descripcio és obligatori",
                ID_TIPOLOGIA: "El camp de tipologia és obligatori"
            }
            /*
            rules: {
                firstname: "required",
                lastname: "required",
                username: {
                    required: true,
                    minlength: 2
                },
                password: {
                    required: true,
                    minlength: 5
                },
                confirm_password: {
                    required: true,
                    minlength: 5,
                    equalTo: "#password"
                },
                email: {
                    required: true,
                    email: true
                },
                topic: {
                    required: "#newsletter:checked",
                    minlength: 2
                },
                agree: "required"
            },
            messages: {
                firstname: "Please enter your firstname",
                lastname: "Please enter your lastname",
                username: {
                    required: "Please enter a username",
                    minlength: "Your username must consist of at least 2 characters"
                },
                password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long"
                },
                confirm_password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long",
                    equalTo: "Please enter the same password as above"
                },
                email: "Please enter a valid email address",
                agree: "Please accept our policy"
            }
            */
            
        });
        
        // propose username by combining first- and lastname
        /*
        $("#username").focus(function() {
            var firstname = $("#firstname").val();
            var lastname = $("#lastname").val();
            if (firstname && lastname && !this.value) {
                this.value = firstname + "." + lastname;
            }
        });

        //code to hide topic selection, disable for demo
        var newsletter = $("#newsletter");
        // newsletter topics are optional, hide at first
        var inital = newsletter.is(":checked");
        var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
        var topicInputs = topics.find("input").attr("disabled", !inital);
        // show when newsletter is checked
        newsletter.click(function() {
            topics[this.checked ? "removeClass" : "addClass"]("gray");
            topicInputs.attr("disabled", !this.checked);
        });
        */
    });
</script><?php }} ?>
