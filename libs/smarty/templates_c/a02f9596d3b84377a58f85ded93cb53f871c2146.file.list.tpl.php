<?php /* Smarty version Smarty-3.1.16, created on 2017-12-13 02:45:14
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/memoria-democratica2/list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:18966972215a30862a2d2c72-25197318%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a02f9596d3b84377a58f85ded93cb53f871c2146' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/memoria-democratica2/list.tpl',
      1 => 1499252421,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18966972215a30862a2d2c72-25197318',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'LABEL_TITOL1' => 0,
    'MENU' => 0,
    'itemMenu' => 0,
    'lang' => 0,
    'lblName' => 0,
    'lblCategory' => 0,
    'lblSelect' => 0,
    'categories' => 0,
    'category' => 0,
    'lblConsult' => 0,
    'lblDelete' => 0,
    'lblSeeItem' => 0,
    'items' => 0,
    'item' => 0,
    'page' => 0,
    'lastpage' => 0,
    'page_ant' => 0,
    'lblAnt' => 0,
    'desp' => 0,
    'firstpage' => 0,
    'counter' => 0,
    'page_seg' => 0,
    'lblSeg' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a30862a3c6201_30365180',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a30862a3c6201_30365180')) {function content_5a30862a3c6201_30365180($_smarty_tpl) {?><html>
<head>
    <?php echo $_smarty_tpl->getSubTemplate ("head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

</head>
<body>
<?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<div id="page-wrap">
    <div id="conttranstotalcos">
        <div id="conttranscos">
            <div id="btranspresen">
                <div id="transimatge">
                    <img src="/images/laciutat/municipi/tira.jpg" class="img-slider-territori"/>
                </div>
                <div id="transimatgeText">
                    <h1>
                        <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

                    </h1>
                </div>
            </div>
            <ul id='menu_planA'>
                <?php  $_smarty_tpl->tpl_vars['itemMenu'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['itemMenu']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['MENU']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['itemMenu']->key => $_smarty_tpl->tpl_vars['itemMenu']->value) {
$_smarty_tpl->tpl_vars['itemMenu']->_loop = true;
?>
                    <a href=<?php echo $_smarty_tpl->tpl_vars['itemMenu']->value;?>
>
                        <li id='menu_planA_item'>
                            <?php echo $_smarty_tpl->tpl_vars['itemMenu']->key;?>

                        </li>
                    </a>
                <?php } ?>
            </ul>
            <div id="tcentre_planA" class="memoria-democratica">
                <h2><?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>
</h2>
                <div>
                    <form name="cerca" id="cerca" action="base-dades.php?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
" method="post">
                        <input type="hidden" name="accio" id="accio" value="list">
                        <ul>
                            <li>
                                <label><?php echo $_smarty_tpl->tpl_vars['lblName']->value;?>
</label>
                                <input type="text" name="nom" id="nom" value=""">
                            </li>
                            <li>
                                <label><?php echo $_smarty_tpl->tpl_vars['lblCategory']->value;?>
</label>
                                <select name="categoria" id="categoria">
                                    <option value="">-- <?php echo $_smarty_tpl->tpl_vars['lblSelect']->value;?>
 --</option>
                                    <?php  $_smarty_tpl->tpl_vars['category'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['category']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['categories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['category']->key => $_smarty_tpl->tpl_vars['category']->value) {
$_smarty_tpl->tpl_vars['category']->_loop = true;
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['category']->value->CATEGORIA;?>
"><?php echo $_smarty_tpl->tpl_vars['category']->value->CATEGORIA;?>
</th>
                                        <?php } ?>
                                </select>
                            </li>
                            <li id="buttons_container">
                                <input type="submit" name="consultar" id="consultar" value="<?php echo $_smarty_tpl->tpl_vars['lblConsult']->value;?>
">
                                <input type="reset" name="esborrar" id="esborrar" value="<?php echo $_smarty_tpl->tpl_vars['lblDelete']->value;?>
">
                            </li>
                        </ul>
                    </form>
                </div>
                <table name="list-items" id="list-items">
                    <tr>
                        <th><?php echo $_smarty_tpl->tpl_vars['lblName']->value;?>
</th>
                        <th><?php echo $_smarty_tpl->tpl_vars['lblCategory']->value;?>
</th>
                        <th><?php echo $_smarty_tpl->tpl_vars['lblSeeItem']->value;?>
</th>
                    </tr>
                    <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
                        <tr>
                            <td><?php echo $_smarty_tpl->tpl_vars['item']->value->NOM;?>
</td>
                            <td><?php echo $_smarty_tpl->tpl_vars['item']->value->CATEGORIA;?>
</td>
                            <td class="link_item"><a href="/memoria-democratica/base-dades.php?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
&accio=file&id=<?php echo $_smarty_tpl->tpl_vars['item']->value->ID;?>
&page=<?php echo $_smarty_tpl->tpl_vars['page']->value;?>
">[+]</a></td>
                        </tr>
                    <?php } ?>
                </table>
                <div id="pagination">
                    <ul>
                        <?php if ($_smarty_tpl->tpl_vars['lastpage']->value>1) {?>
                            <?php if ($_smarty_tpl->tpl_vars['page']->value>1) {?>
                                <?php $_smarty_tpl->tpl_vars["page_ant"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value-1, null, 0);?>
                                <li><a href="/memoria-democratica/base-dades.php?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
&accio=list&page=<?php echo $_smarty_tpl->tpl_vars['page_ant']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['lblAnt']->value;?>
</a></li>
                            <?php }?>
                            <?php $_smarty_tpl->tpl_vars["desp"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value+4, null, 0);?>
                            <?php $_smarty_tpl->tpl_vars["firstpage"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value-4, null, 0);?>

                            <?php if ($_smarty_tpl->tpl_vars['desp']->value<$_smarty_tpl->tpl_vars['lastpage']->value) {?>
                                <?php $_smarty_tpl->tpl_vars["lastpage"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value+4, null, 0);?>
                            <?php }?>
                            <?php if ($_smarty_tpl->tpl_vars['firstpage']->value<1) {?>
                                <?php $_smarty_tpl->tpl_vars["firstpage"] = new Smarty_variable(1, null, 0);?>
                            <?php }?>

                            <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['counter']->step = 1;$_smarty_tpl->tpl_vars['counter']->total = (int) ceil(($_smarty_tpl->tpl_vars['counter']->step > 0 ? $_smarty_tpl->tpl_vars['lastpage']->value+1 - ($_smarty_tpl->tpl_vars['firstpage']->value) : $_smarty_tpl->tpl_vars['firstpage']->value-($_smarty_tpl->tpl_vars['lastpage']->value)+1)/abs($_smarty_tpl->tpl_vars['counter']->step));
if ($_smarty_tpl->tpl_vars['counter']->total > 0) {
for ($_smarty_tpl->tpl_vars['counter']->value = $_smarty_tpl->tpl_vars['firstpage']->value, $_smarty_tpl->tpl_vars['counter']->iteration = 1;$_smarty_tpl->tpl_vars['counter']->iteration <= $_smarty_tpl->tpl_vars['counter']->total;$_smarty_tpl->tpl_vars['counter']->value += $_smarty_tpl->tpl_vars['counter']->step, $_smarty_tpl->tpl_vars['counter']->iteration++) {
$_smarty_tpl->tpl_vars['counter']->first = $_smarty_tpl->tpl_vars['counter']->iteration == 1;$_smarty_tpl->tpl_vars['counter']->last = $_smarty_tpl->tpl_vars['counter']->iteration == $_smarty_tpl->tpl_vars['counter']->total;?>
                                <?php if ($_smarty_tpl->tpl_vars['counter']->value==$_smarty_tpl->tpl_vars['page']->value) {?>
                                    <li><?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
</li>
                                <?php } else { ?>
                                    <li><a href="/memoria-democratica/base-dades.php?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
&accio=list&page=<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
</a></li>
                                <?php }?>
                            <?php }} ?>
                            <?php if ($_smarty_tpl->tpl_vars['page']->value<$_smarty_tpl->tpl_vars['lastpage']->value) {?>
                                <?php $_smarty_tpl->tpl_vars["page_seg"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value+1, null, 0);?>
                                <li><a href="/memoria-democratica/base-dades.php?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
&accio=list&page=<?php echo $_smarty_tpl->tpl_vars['page_seg']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['lblSeg']->value;?>
</a></li>
                            <?php }?>
                        <?php }?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

</body>
</html><?php }} ?>
