<?php /* Smarty version Smarty-3.1.16, created on 2017-12-13 10:03:08
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/admin/templates/indicadors/list_indicador_grup.tpl" */ ?>
<?php /*%%SmartyHeaderCode:14290808495a30eccc2e3d80-70891312%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2e5f414871e61a79de7a9e9cce5d608d1070a66f' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/admin/templates/indicadors/list_indicador_grup.tpl',
      1 => 1513082915,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14290808495a30eccc2e3d80-70891312',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'items' => 0,
    'item' => 0,
    'info_usuari' => 0,
    'page' => 0,
    'lastpage' => 0,
    'page_ant' => 0,
    'desp' => 0,
    'firstpage' => 0,
    'counter' => 0,
    'page_seg' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a30eccc35de26_64316508',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a30eccc35de26_64316508')) {function content_5a30eccc35de26_64316508($_smarty_tpl) {?><section id="main" class="column">

<h4 class="alert_info"><i class="icon-pencil"></i>Llistat Grups d'Indicadors</h4>
<article class="module width_full">
    <div class="module_content">
        <table class="list_table" name="list_banners" id="list_banners">
            <tr>
                <th style="width: 55%">Indicador</th>
                <th style="width: 10%">Exercici</th>
                <th style="width: 15%">Tipus indicador</th>
                <th style="width: 5%">Visible</th>
                <th style="width: 5%">Baixa</th>
                <th style="width: 5%"></th>
                <th style="width: 5%"></th>
            </tr>
            <tr><td colspan="7"></td></tr>
            <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
                <tr>
                	
                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value->GRUP_INDICADOR;?>
</td>
                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value->EXERCICI;?>
</td>
                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value->TIPUS_INDICADOR;?>
</td>
                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value->VISIBLE;?>
</td>
                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value->BAIXA;?>
</td>
                    <td class="link_item modify">
                        <span><i class="icon-pencil"></i></span>
                        <div class="language-<?php echo $_smarty_tpl->tpl_vars['item']->value->ID;?>
 language">
                            <ul>
                                <li><a href="/admin/indicadors_transparencia/indicadors_grup.php?id=<?php echo $_smarty_tpl->tpl_vars['item']->value->ID;?>
&lang=ca&accio=edit_indicador_grup">ca</a></li>
                                <li><a href="/admin/indicadors_transparencia/indicadors_grup.php?id=<?php echo $_smarty_tpl->tpl_vars['item']->value->ID;?>
&lang=es&accio=edit_indicador_grup">es</a></li>
                                <li><a href="/admin/indicadors_transparencia/indicadors_grup.php?id=<?php echo $_smarty_tpl->tpl_vars['item']->value->ID;?>
&lang=en&accio=edit_indicador_grup">en</a></li>
                                <li><a href="/admin/indicadors_transparencia/indicadors_grup.php?id=<?php echo $_smarty_tpl->tpl_vars['item']->value->ID;?>
&lang=fr&accio=edit_indicador_grup">fr</a></li>
                            </ul>
                        </div>
                    </td>
                    <td class="link_item">
                        <?php if ($_smarty_tpl->tpl_vars['info_usuari']->value["rol"]=="Superadministrador") {?>
                        <a href="JavaScript: delete_item('<?php echo $_smarty_tpl->tpl_vars['item']->value->ID;?>
', '<?php echo $_smarty_tpl->tpl_vars['page']->value;?>
');"><i class="icon-trash-empty"></i></a></td>
                        <?php }?>
                    <!--<a href="JavaScript: delete_item('<?php echo $_smarty_tpl->tpl_vars['item']->value->ID;?>
', '<?php echo $_smarty_tpl->tpl_vars['page']->value;?>
');">Esborrar</a>-->
                </tr>
            <?php } ?>
        </table>
    </div>
    <div id="pagination">
        <ul>
            <?php if ($_smarty_tpl->tpl_vars['lastpage']->value>1) {?>
                <?php if ($_smarty_tpl->tpl_vars['page']->value>1) {?>
                    <?php $_smarty_tpl->tpl_vars["page_ant"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value-1, null, 0);?>
                    <li><a href="indicadors_grup.php?page=<?php echo $_smarty_tpl->tpl_vars['page_ant']->value;?>
">Ant</a></li>
                <?php }?>
                <?php $_smarty_tpl->tpl_vars["desp"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value+4, null, 0);?>
                <?php $_smarty_tpl->tpl_vars["firstpage"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value-4, null, 0);?>

                <?php if ($_smarty_tpl->tpl_vars['desp']->value<$_smarty_tpl->tpl_vars['lastpage']->value) {?>
                    <?php $_smarty_tpl->tpl_vars["lastpage"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value+4, null, 0);?>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['firstpage']->value<1) {?>
                    <?php $_smarty_tpl->tpl_vars["firstpage"] = new Smarty_variable(1, null, 0);?>
                <?php }?>

                <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['counter']->step = 1;$_smarty_tpl->tpl_vars['counter']->total = (int) ceil(($_smarty_tpl->tpl_vars['counter']->step > 0 ? $_smarty_tpl->tpl_vars['lastpage']->value+1 - ($_smarty_tpl->tpl_vars['firstpage']->value) : $_smarty_tpl->tpl_vars['firstpage']->value-($_smarty_tpl->tpl_vars['lastpage']->value)+1)/abs($_smarty_tpl->tpl_vars['counter']->step));
if ($_smarty_tpl->tpl_vars['counter']->total > 0) {
for ($_smarty_tpl->tpl_vars['counter']->value = $_smarty_tpl->tpl_vars['firstpage']->value, $_smarty_tpl->tpl_vars['counter']->iteration = 1;$_smarty_tpl->tpl_vars['counter']->iteration <= $_smarty_tpl->tpl_vars['counter']->total;$_smarty_tpl->tpl_vars['counter']->value += $_smarty_tpl->tpl_vars['counter']->step, $_smarty_tpl->tpl_vars['counter']->iteration++) {
$_smarty_tpl->tpl_vars['counter']->first = $_smarty_tpl->tpl_vars['counter']->iteration == 1;$_smarty_tpl->tpl_vars['counter']->last = $_smarty_tpl->tpl_vars['counter']->iteration == $_smarty_tpl->tpl_vars['counter']->total;?>
                    <?php if ($_smarty_tpl->tpl_vars['counter']->value==$_smarty_tpl->tpl_vars['page']->value) {?>
                        <li><?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
</li>
                    <?php } else { ?>
                        <li><a href="indicadors_grup.php?page=<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
</a></li>
                    <?php }?>
                <?php }} ?>
                <?php if ($_smarty_tpl->tpl_vars['page']->value<$_smarty_tpl->tpl_vars['lastpage']->value-1) {?>
                    <?php $_smarty_tpl->tpl_vars["page_seg"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value+1, null, 0);?>
                    <li><a href="indicadors_grup.php?page=<?php echo $_smarty_tpl->tpl_vars['page_seg']->value;?>
">Seg</a></li>
                <?php }?>
            <?php }?>
        </ul>
    </div>
</article>

<!-- end of styles article -->


<div class="spacer"></div>
</section>

<script>
    /*
    $.validator.setDefaults({
        submitHandler: function() {
            alert("submitted!");
        }
    });
    */

    $().ready(function() {
        $(".modify").click(function() {

            if($(this).children("div").css('display')==="none") {
                $(".modify").children("div").css("display", "none");
                $(this).children("div").css("display", "block");
                exit();
            }
            if($(this).children("div").css('display')==="block") {
                $(this).children("div").css("display", "none");
                exit();
            }
        });
    });

    function delete_item(id, page) {
        if(confirm("Vols esborrar aquest element?"))
            window.location.href="indicadors_grup.php?accio=delete_indicador_grup&id="+id+"&page="+page;
    }
</script><?php }} ?>
