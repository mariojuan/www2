<?php /* Smarty version Smarty-3.1.16, created on 2017-12-13 08:24:29
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/admin/templates/list_headline.tpl" */ ?>
<?php /*%%SmartyHeaderCode:5485416035a30d5ad84a3d3-08626077%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '762ef568ede5ddba532c4a3e43edc7888a8f97c0' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/admin/templates/list_headline.tpl',
      1 => 1491490808,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5485416035a30d5ad84a3d3-08626077',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'items' => 0,
    'item' => 0,
    'page' => 0,
    'lastpage' => 0,
    'page_ant' => 0,
    'desp' => 0,
    'firstpage' => 0,
    'counter' => 0,
    'page_seg' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a30d5ad940c24_35409652',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a30d5ad940c24_35409652')) {function content_5a30d5ad940c24_35409652($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/vhost/www2.tortosa.cat/home/html/libs/smarty/plugins/modifier.date_format.php';
?><section id="main" class="column">

<h4 class="alert_info"><i class="icon-pencil"></i>Llistat headlines</h4>
<article class="module width_full">
    <div class="module_content">
        <table class="list_table" name="list_headlines" id="list_headlines">
            <tr>
                <th>Títol</th>
                <th>Data</th>
                <th></th>
                <th></th>
            </tr>
            <tr><td colspan="4"></td></tr>
            <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
                <tr>
                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value->TITOL;?>
</td>
                    <td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['item']->value->DATA,"%d/%m/%G");?>
</td>
                    <td class="link_item"><a href="headlines.php?accio=edit_headlines&id=<?php echo $_smarty_tpl->tpl_vars['item']->value->ID;?>
&page=<?php echo $_smarty_tpl->tpl_vars['page']->value;?>
">Modificar</a></td>
                    <td class="link_item"><a href="JavaScript: delete_item('<?php echo $_smarty_tpl->tpl_vars['item']->value->ID;?>
', '<?php echo $_smarty_tpl->tpl_vars['page']->value;?>
');">Esborrar</a></td>
                </tr>
            <?php } ?>
        </table>
    </div>
    <div id="pagination">
        <ul>
            <?php if ($_smarty_tpl->tpl_vars['lastpage']->value>1) {?>
                <?php if ($_smarty_tpl->tpl_vars['page']->value>1) {?>
                    <?php $_smarty_tpl->tpl_vars["page_ant"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value-1, null, 0);?>
                    <li><a href="headlines.php?page=<?php echo $_smarty_tpl->tpl_vars['page_ant']->value;?>
">Ant</a></li>
                <?php }?>
                <?php $_smarty_tpl->tpl_vars["desp"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value+4, null, 0);?>
                <?php $_smarty_tpl->tpl_vars["firstpage"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value-4, null, 0);?>

                <?php if ($_smarty_tpl->tpl_vars['desp']->value<$_smarty_tpl->tpl_vars['lastpage']->value) {?>
                    <?php $_smarty_tpl->tpl_vars["lastpage"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value+4, null, 0);?>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['firstpage']->value<1) {?>
                    <?php $_smarty_tpl->tpl_vars["firstpage"] = new Smarty_variable(1, null, 0);?>
                <?php }?>

                <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['counter']->step = 1;$_smarty_tpl->tpl_vars['counter']->total = (int) ceil(($_smarty_tpl->tpl_vars['counter']->step > 0 ? $_smarty_tpl->tpl_vars['lastpage']->value+1 - ($_smarty_tpl->tpl_vars['firstpage']->value) : $_smarty_tpl->tpl_vars['firstpage']->value-($_smarty_tpl->tpl_vars['lastpage']->value)+1)/abs($_smarty_tpl->tpl_vars['counter']->step));
if ($_smarty_tpl->tpl_vars['counter']->total > 0) {
for ($_smarty_tpl->tpl_vars['counter']->value = $_smarty_tpl->tpl_vars['firstpage']->value, $_smarty_tpl->tpl_vars['counter']->iteration = 1;$_smarty_tpl->tpl_vars['counter']->iteration <= $_smarty_tpl->tpl_vars['counter']->total;$_smarty_tpl->tpl_vars['counter']->value += $_smarty_tpl->tpl_vars['counter']->step, $_smarty_tpl->tpl_vars['counter']->iteration++) {
$_smarty_tpl->tpl_vars['counter']->first = $_smarty_tpl->tpl_vars['counter']->iteration == 1;$_smarty_tpl->tpl_vars['counter']->last = $_smarty_tpl->tpl_vars['counter']->iteration == $_smarty_tpl->tpl_vars['counter']->total;?>
                    <?php if ($_smarty_tpl->tpl_vars['counter']->value==$_smarty_tpl->tpl_vars['page']->value) {?>
                        <li><?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
</li>
                    <?php } else { ?>
                        <li><a href="headlines.php?page=<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
</a></li>
                    <?php }?>
                <?php }} ?>
                <?php if ($_smarty_tpl->tpl_vars['page']->value<$_smarty_tpl->tpl_vars['lastpage']->value-1) {?>
                    <?php $_smarty_tpl->tpl_vars["page_seg"] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value+1, null, 0);?>
                    <li><a href="headlines.php?page=<?php echo $_smarty_tpl->tpl_vars['page_seg']->value;?>
">Seg</a></li>
                <?php }?>
            <?php }?>
        </ul>
    </div>
</article>

<!-- end of styles article -->


<div class="spacer"></div>
</section>

<script>
    /*
    $.validator.setDefaults({
        submitHandler: function() {
            alert("submitted!");
        }
    });
    */

    $().ready(function() {

    });

    function delete_item(id, page) {
        if(confirm("Vols esborrar aquest element?"))
            window.location.href="headlines.php?accio=delete_headlines&id="+id+"&page="+page;
    }
</script><?php }} ?>
