<?php /* Smarty version Smarty-3.1.16, created on 2017-12-14 09:20:23
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/admin/templates/indicadors/edit_indicador_grup.tpl" */ ?>
<?php /*%%SmartyHeaderCode:7114592285a30f016848043-21289889%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7056c55a46d2010bdefd47fafa4c79631498584c' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/admin/templates/indicadors/edit_indicador_grup.tpl',
      1 => 1513239621,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7114592285a30f016848043-21289889',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a30f0168d7405_98504504',
  'variables' => 
  array (
    'item' => 0,
    'lang' => 0,
    'i' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a30f0168d7405_98504504')) {function content_5a30f0168d7405_98504504($_smarty_tpl) {?><section id="main" class="column">
   <h4 class="alert_info"><i class="icon-doc-new"></i><?php if ($_smarty_tpl->tpl_vars['item']->value[0]->ID=='') {?>Alta<?php } else { ?>Modificació<?php }?> Grup d'Indicador (<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
)</h4>
   <article class="module width_full">
      <div class="module_content">
         <form id="indicadorsGrupForm" method="post" action="indicadors_grup.php" novalidate="novalidate" enctype="multipart/form-data">
             <input type="hidden" name="accio" id="accio" value="edit_indicador_grup">
             <input type="hidden" name="lang" id="lang" value="<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
">
             <input type="hidden" name="ID" id="ID" value="<?php echo $_smarty_tpl->tpl_vars['item']->value[0]->ID;?>
">
            <?php if ($_smarty_tpl->tpl_vars['lang']->value=='ca') {?>
            <p>
               <label>Codi *</label>
               <input type="text" name="CODI_GRUP_INDICADOR" id="CODI_GRUP_INDICADOR" value="<?php echo $_smarty_tpl->tpl_vars['item']->value[0]->CODI_GRUP_INDICADOR;?>
">
            </p>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['lang']->value=='ca') {?>
            <p>
               <label>Tipus indicador *</label>
               <select name="TIPUS_INDICADOR" id="TIPUS_INDICADOR">
                   <option value="">-- Seleccionar --</option>
                   <option value="Infoparticipa" <?php if ($_smarty_tpl->tpl_vars['item']->value[0]->TIPUS_INDICADOR=="Infoparticipa") {?>selected<?php }?>>Infoparticipa</option>
                   <option value="ITA" <?php if ($_smarty_tpl->tpl_vars['item']->value[0]->TIPUS_INDICADOR=="ITA") {?>selected<?php }?>>ITA</option>
               </select>
            </p>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['lang']->value=='ca') {?>
            <p>
               <label>Exercici *</label>
                <select name="EXERCICI" id="EXERCICI">
                    <option value="">-- Seleccionar --</option>
                    <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? 2050+1 - (2017) : 2017-(2050)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 2017, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
                        <?php if ($_smarty_tpl->tpl_vars['i']->value==$_smarty_tpl->tpl_vars['item']->value[0]->EXERCICI) {?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" selected><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
</option>
                        <?php } else { ?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
</option>
                        <?php }?>
                    <?php }} ?>
                </select>
            </p>
            <p>
                <label>Nom Grup *</label>
                <input type="text" name="GRUP_INDICADOR" id="GRUP_INDICADOR" value="<?php echo $_smarty_tpl->tpl_vars['item']->value[0]->GRUP_INDICADOR;?>
">
            </p>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['lang']->value=='ca') {?>
            <p>
                <label>Baixa</label>
                <input type="checkbox" name="BAIXA" id="BAIXA" <?php if ($_smarty_tpl->tpl_vars['item']->value[0]->BAIXA) {?>checked <?php }?>>
            </p>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['lang']->value=='ca') {?>
            <p>
                <label>Visible</label>
                <input type="checkbox" name="VISIBLE" id="VISIBLE" <?php if ($_smarty_tpl->tpl_vars['item']->value[0]->VISIBLE) {?>checked <?php }?>>
            </p>
            <?php }?>
            <p>
               <input class="submit" type="submit" value="  <?php if ($_smarty_tpl->tpl_vars['item']->value[0]->ID=='') {?>Alta<?php } else { ?>Modificació<?php }?>  ">
            </p>
            <p>
               <label>* Camps obligatoris</label>
            </p>
         </form>
      </div>
   </article>

   <!-- end of styles article -->
   <div class="spacer"></div>
</section>
<script>
    $().ready(function() {
        $("#indicadorsGrupForm").validate({
            rules: {
                CODI_GRUP_INDICADOR:  {
                    required: true
                },
                TIPUS_INDICADOR: {
                    required: true
                },
                EXERCICI: "required",
                GRUP_INDICADOR: "required"
            },
            messages: {
                CODI_GRUP_INDICADOR: {
                    required: "El camp de codi de grup és obligatori"
                },
                TIPUS_INDICADOR: {
                    required: "El camp de tipus d'indicador és obligatori"
                },
                EXERCICI: {
                    required: "El camp d'exercici és obligatori"
                },
                GRUP_INDICADOR: {
                    required: "El camp de grup és obligatori"
                }
            },
            submitHandler: function(form) {
                if ($('#BAIXA').prop('checked')) {
                    $('#BAIXA').val("1");
                }
                else {
                    $('#BAIXA').val("0");
                }
                if($('#VISIBLE').prop('checked')) {
                    $('#VISIBLE').val("1");
                }
                else {
                    $('#VISIBLE').val("0");
                }
                form.submit();
            }
        });
    });
</script><?php }} ?>
