<?php /* Smarty version Smarty-3.1.16, created on 2017-12-12 16:10:22
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/arxiu/arxiu_home.tpl" */ ?>
<?php /*%%SmartyHeaderCode:17798933825a2ff15e69aa14-90915032%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '93796ce44a816ccdccab1b28a7ae4e11797fbdc1' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/arxiu/arxiu_home.tpl',
      1 => 1499252182,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '17798933825a2ff15e69aa14-90915032',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'image' => 0,
    'LABEL_TITOL1' => 0,
    'MENU' => 0,
    'itemMenu' => 0,
    'type' => 0,
    'LABEL_TC0' => 0,
    'LABEL_TC1' => 0,
    'LABEL_TC2' => 0,
    'LABEL_TC3' => 0,
    'LABEL_TC0_C1' => 0,
    'LABEL_TC0_C2' => 0,
    'LABEL_TC0_C3' => 0,
    'TC_DOC_CONSULTABLES' => 0,
    'item' => 0,
    'LABEL_TC4' => 0,
    'LABEL_TC5' => 0,
    'LABEL_TC6' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a2ff15e76bc91_63395789',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a2ff15e76bc91_63395789')) {function content_5a2ff15e76bc91_63395789($_smarty_tpl) {?><!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->
<div id="page-wrap">

<div class="contenedor-responsive">

    <div id="conttranstotalcos">
        <div id="conttranscos">
        
            <div id="btranspresen">
                <div id="transimatge">
                <img src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
" class="img-slider-territori"/>
                </div>
                <div id="transimatgeText">
                    <h1>
                    <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

                    <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                    </h1>
                </div>
            </div>

            <ul id='menu_planA'>
            <?php  $_smarty_tpl->tpl_vars['itemMenu'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['itemMenu']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['MENU']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['itemMenu']->key => $_smarty_tpl->tpl_vars['itemMenu']->value) {
$_smarty_tpl->tpl_vars['itemMenu']->_loop = true;
?>
                <a href=<?php echo $_smarty_tpl->tpl_vars['itemMenu']->value['link'];?>
 target="_<?php echo $_smarty_tpl->tpl_vars['itemMenu']->value['target'];?>
">
                <li id='menu_planA_item'>
                    <?php echo $_smarty_tpl->tpl_vars['itemMenu']->value['name'];?>

                </li>
                </a>
            <?php } ?>
            </ul>
            
            <div id="tcentre_planA">
                <?php if ($_smarty_tpl->tpl_vars['type']->value==1) {?>
                <!-- Presentació Arxiu-->
                    <font size=4 color=#666666><?php echo $_smarty_tpl->tpl_vars['LABEL_TC0']->value;?>
</font>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
</p>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC3']->value;?>
</p>

                <?php } elseif ($_smarty_tpl->tpl_vars['type']->value==2) {?>
                 <!-- Tràmits de Consulta -->
                    <font size=4 color=#666666><?php echo $_smarty_tpl->tpl_vars['LABEL_TC0']->value;?>
</font>
                  
                    <p>
                        <?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>

                        <a href="mailto:arxiu.ajuntament@tortosa.cat" target="_blank"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
</a>
                        <?php echo $_smarty_tpl->tpl_vars['LABEL_TC3']->value;?>

                    </p>   

                <?php } elseif ($_smarty_tpl->tpl_vars['type']->value==3) {?>
                    <!-- Documents Consultables-->
                     <h2> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC0']->value;?>
</h2>
                    
                    <div id="taulaicorpo arxiu_taula">
                        <!-- Capsal de la taula-->
                        <div id='filaicorpo'>
                            <div  class="colicorpo arxiu_col1 arxiu_col_cap">
                                 <?php echo $_smarty_tpl->tpl_vars['LABEL_TC0_C1']->value;?>
                                
                            </div>
                            <div  class="colicorpo arxiu_col2 arxiu_col_cap">
                                <?php echo $_smarty_tpl->tpl_vars['LABEL_TC0_C2']->value;?>

                            </div>
                            <div  class="colicorpo arxiu_col3 arxiu_col_cap">
                                <?php echo $_smarty_tpl->tpl_vars['LABEL_TC0_C3']->value;?>

                            </div>
                        </div>

                        
                        <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['TC_DOC_CONSULTABLES']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
                           <div id='filaicorpo'> 
                                <?php if ($_smarty_tpl->tpl_vars['item']->value['col1']/2==intval($_smarty_tpl->tpl_vars['item']->value['col1']/2)) {?><!-- columa color -->
                                    <div   class="colicorpo arxiu_col1 arxiu_col_alterna"> <!-- Columna 1 -->
                                        <?php echo $_smarty_tpl->tpl_vars['item']->value['col1'];?>

                                    </div>
                                    <div   class="colicorpo arxiu_col2 arxiu_col_alterna"> <!-- Columna 2 -->
                                        <?php echo $_smarty_tpl->tpl_vars['item']->value['col2'];?>

                                    </div>
                                    <div   class="colicorpo arxiu_col3 arxiu_col_alterna"> <!-- Columna 3 -->
                                        <?php echo $_smarty_tpl->tpl_vars['item']->value['col3'];?>

                                    </div>
                                    </div> 
                                <?php } else { ?>
                                
                                    <div   class="colicorpo arxiu_col1"> <!-- Columna 1 -->
                                        <?php echo $_smarty_tpl->tpl_vars['item']->value['col1'];?>

                                    </div>
                                    <div   class="colicorpo arxiu_col2"> <!-- Columna 2 -->
                                        <?php echo $_smarty_tpl->tpl_vars['item']->value['col2'];?>

                                    </div>
                                    <div   class="colicorpo arxiu_col3"> <!-- Columna 3 -->
                                        <?php echo $_smarty_tpl->tpl_vars['item']->value['col3'];?>

                                    </div>
                                    </div> 
                                <?php }?>

                        <?php } ?>              
                        
                    </div>                    


                <?php } elseif ($_smarty_tpl->tpl_vars['type']->value==4) {?>
                 <!-- Dipòsit legal-->
                    <font size=4 color=#666666><?php echo $_smarty_tpl->tpl_vars['LABEL_TC0']->value;?>
</font>
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
</p>  
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
</p>  
                    <p>
                        <a href="http://www.tortosa.cat/webajt/ajunta/arxiu/dl_2013_2015.pdf" target="_blank"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC3']->value;?>
 </a>
                    </p>  
                         
                <?php } elseif ($_smarty_tpl->tpl_vars['type']->value==5) {?>
                  <!-- Repositori Digital-->
                    <font size=4 color=#666666><?php echo $_smarty_tpl->tpl_vars['LABEL_TC0']->value;?>
</font>
                    <p>
                        <?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
 <a href="http://documents.tortosa.cat/" target="_blank">  <?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
 </a>   <?php echo $_smarty_tpl->tpl_vars['LABEL_TC3']->value;?>

                    </p>     
                  
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC4']->value;?>
</p>  
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC5']->value;?>
</p>  
                    <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC6']->value;?>
</p> 

                <?php } elseif ($_smarty_tpl->tpl_vars['type']->value==6) {?>
                
                <?php }?>
            </div>
            <div class="separator"></div>
        </div>
    </div>
    </div>
</div>
<?php }} ?>
