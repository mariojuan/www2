<?php /* Smarty version Smarty-3.1.16, created on 2017-12-13 02:43:44
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/infeco/evolucio.tpl" */ ?>
<?php /*%%SmartyHeaderCode:20035984925a3085d05587c4-36642229%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f480da652edb83323fb5b1cc08ffd9afd4115938' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/infeco/evolucio.tpl',
      1 => 1499415616,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '20035984925a3085d05587c4-36642229',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'image' => 0,
    'LABEL_TITOL1' => 0,
    'LABEL_TC7_171' => 0,
    'lang' => 0,
    'LABEL_TC7_INTRO' => 0,
    'LABEL_TC7_TITOL1' => 0,
    'url_evolucio1' => 0,
    'LABEL_TC7_ITEM1' => 0,
    'url_evolucio2' => 0,
    'LABEL_TC7_ITEM2' => 0,
    'url_evolucio3' => 0,
    'LABEL_TC7_ITEM3' => 0,
    'url_evolucio4' => 0,
    'LABEL_TC7_ITEM4' => 0,
    'url_evolucio5' => 0,
    'LABEL_TC7_ITEM5' => 0,
    'LABEL_TC7_TITOL2' => 0,
    'url_evolucio6' => 0,
    'LABEL_TC7_ITEM6' => 0,
    'url_evolucio7' => 0,
    'LABEL_TC7_ITEM7' => 0,
    'url_evolucio8' => 0,
    'LABEL_TC7_ITEM8' => 0,
    'url_evolucio9' => 0,
    'LABEL_TC7_ITEM9' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a3085d05ff714_57548442',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a3085d05ff714_57548442')) {function content_5a3085d05ff714_57548442($_smarty_tpl) {?><html>
    <head>
        <title>

        </title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <?php echo $_smarty_tpl->getSubTemplate ("head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    </head>
    <body>
        <?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

        
    

<div id="page-wrap">

<div class="contenedor-responsive">

<div id="contexpretotalcos" class="continfecototalcos">
	<div id="conttranscos">
		<div id="btranspresen">
			<div id="transimatge">			
				<img src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
" class="img-slider-territori">			
			</div>
			<div id="transimatgeText">
				<h1>
				<?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

				</h1>
			</div>
		</div>
        <div class="subtitolinfeco" id="subtitolinfeco2">
            <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_171']->value;?>

        </div>
        <div class="marcback">
                    <a href="index.php?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
">
                    <i class="icon-angle-double-up"></i>
                    </a>
        </div>
        <br><br>
        <div id="textintro_execAjt">
        <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_INTRO']->value;?>

        </div>
        <ul class="indexepi">
            <li><samp class="titolsubra"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_TITOL1']->value;?>
</samp></li>
                <ul>
                    <li><a href="<?php echo $_smarty_tpl->tpl_vars['url_evolucio1']->value;?>
" target="_self"><i class="icon-chart-bar"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM1']->value;?>
</a></li>
                    <li><a href="<?php echo $_smarty_tpl->tpl_vars['url_evolucio2']->value;?>
" target="_self"><i class="icon-chart-bar"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM2']->value;?>
</a></li>
                    <li><a href="<?php echo $_smarty_tpl->tpl_vars['url_evolucio3']->value;?>
" target="_self"><i class="icon-doc-text"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM3']->value;?>
</a></li>
                    <li><a href="<?php echo $_smarty_tpl->tpl_vars['url_evolucio4']->value;?>
" target="_self"><i class="icon-chart-line"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM4']->value;?>
</a></li>
                    <li><a href="<?php echo $_smarty_tpl->tpl_vars['url_evolucio5']->value;?>
" target="_self"><i class="icon-chart-line"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM5']->value;?>
</a></li>
                </ul>
                <br>
            <li><samp class="titolsubra"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_TITOL2']->value;?>
</samp></li>
                <ul>
                    <li><a href="<?php echo $_smarty_tpl->tpl_vars['url_evolucio6']->value;?>
"><i class="icon-chart-line"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM6']->value;?>
</a></li>
                    <li><a href="<?php echo $_smarty_tpl->tpl_vars['url_evolucio7']->value;?>
"><i class="icon-chart-line"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM7']->value;?>
</a></li>
                    <li><a href="<?php echo $_smarty_tpl->tpl_vars['url_evolucio8']->value;?>
"><i class="icon-chart-bar"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM8']->value;?>
</a></li>
                    <li><a href="<?php echo $_smarty_tpl->tpl_vars['url_evolucio9']->value;?>
"><i class="icon-chart-bar"></i> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7_ITEM9']->value;?>
</a></li>
                </ul>
        </ul>
        
  	 </div>
     </div>
</div>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

</body>
</html><?php }} ?>
