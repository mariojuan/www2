<?php /* Smarty version Smarty-3.1.16, created on 2017-12-13 02:49:09
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/festes/cinta17/programa.tpl" */ ?>
<?php /*%%SmartyHeaderCode:4327654215a30871573e841-98984084%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '22acc6e233d006fdd8e8793cfd6d070d90e93008' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/festes/cinta17/programa.tpl',
      1 => 1503489121,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4327654215a30871573e841-98984084',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'image' => 0,
    'LABEL_TITOL1' => 0,
    'MENU' => 0,
    'itemMenu' => 0,
    'LABEL02' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a30871579e6d1_91547049',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a30871579e6d1_91547049')) {function content_5a30871579e6d1_91547049($_smarty_tpl) {?><!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->
<div id="page-wrap">

   <div class="contenedor-responsive">

      <div id="conttranstotalcos">

         <div id="conttranscos">

            <div id="btranspresen">

               <div id="transimatge">
                  <img src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
" class="img-slider-territori"/>
               </div>

               <div id="transimatgeText">
                  <h1>
                     <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

                     <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                  </h1>
               </div>

            </div>

            <ul id='menu_planA'>
               <?php  $_smarty_tpl->tpl_vars['itemMenu'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['itemMenu']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['MENU']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['itemMenu']->key => $_smarty_tpl->tpl_vars['itemMenu']->value) {
$_smarty_tpl->tpl_vars['itemMenu']->_loop = true;
?>
               <a href=<?php echo $_smarty_tpl->tpl_vars['itemMenu']->value['link'];?>
 target="_<?php echo $_smarty_tpl->tpl_vars['itemMenu']->value['target'];?>
">
                  <li id='menu_planA_item'>
                     <?php echo $_smarty_tpl->tpl_vars['itemMenu']->value['name'];?>

                  </li>
               </a>
               <?php } ?>
            </ul>

            <div id="tcentre_planA">
               <h2><?php echo $_smarty_tpl->tpl_vars['LABEL02']->value;?>
</h2>

               <div id="taulapubilles">  
                  <div id="filapubilles">
                     <div class="colunapubilla">
                        <a href="http://www.tortosa.cat/webajt/festes/cinta17/programa/prog2017.pdf" target="_blank">
                           <img class="marcprograma" src="/images/festes/cinta17/programa/iprograma.jpg" width="150" height="212">
                        </a>
                        <br><br>
                        <div id="textnegreta">Programa</div>
                        <div id="textnormal"><i class="icon-file-pdf"></i></div>
                        <div id="textnormal">Del 31/8 al 4/9</div>
                     </div>                        
                  </div>
               </div>
      
                <div class="separator"></div> 
                <br>

               <div id="taulapubilles">  
                  <div id="filapubilles">
                     <div class="colprograma">
                        <a href="http://www.tortosa.cat/webajt/festes/cinta17/programa/prog2608.pdf" target="_blank">
                           <img class="marcprograma" src="/images/festes/cinta17/programa/i2608.jpg" width="125" height="177">
                        </a>                           
                        <br><br>
                        <div id="textnormal">Dissabte, 26 d'agost</div>
                        <div id="textnormal"><i class="icon-file-pdf"></i></div>
                     </div>
                     <div class="colprograma"> 
                        <a href="http://www.tortosa.cat/webajt/festes/cinta17/programa/prog3108.pdf" target="_blank">
                           <img class="marcprograma" src="/images/festes/cinta17/programa/i3108.jpg" width="125" height="177">
                        </a>
                        <br><br>
                        <div id="textnormal">Dijous, 31 d'agost</div>
                        <div id="textnormal"><i class="icon-file-pdf"></i></div>
                     </div>
                     <div class="colprograma">
                        <a href="http://www.tortosa.cat/webajt/festes/cinta17/programa/prog0109.pdf" target="_blank">
                           <img class="marcprograma" src="/images/festes/cinta17/programa/i0109.jpg" width="125" height="177">
                        </a>
                        <br><br>
                        <div id="textnormal">Divendres, 1 de setembre</div>
                        <div id="textnormal"><i class="icon-file-pdf"></i></div>
                     </div> 
                     <div class="colprograma">
                        <a href="http://www.tortosa.cat/webajt/festes/cinta17/programa/prog0209.pdf" target="_blank">
                           <img class="marcprograma" src="/images/festes/cinta17/programa/i0209.jpg" width="125" height="177">
                        </a>
                        <br><br>
                        <div id="textnormal">Dissabte, 2 de setembre</div>
                        <div id="textnormal"><i class="icon-file-pdf"></i></div>                       
                     </div>                       
                  </div>

                  <div id="filapubilles">
                     
                     <div class="colprograma"> 
                        <a href="http://www.tortosa.cat/webajt/festes/cinta17/programa/prog0309.pdf" target="_blank">
                           <img class="marcprograma" src="/images/festes/cinta17/programa/i0309.jpg" width="125" height="177">
                        </a>
                        <br><br>
                        <div id="textnormal">Diumenge, 3 de setembre</div>
                        <div id="textnormal">Dia de la Cinta</div> 
                        <div id="textnormal"><i class="icon-file-pdf"></i></div>                       
                     </div>
                     <div class="colprograma">
                        <a href="http://www.tortosa.cat/webajt/festes/cinta17/programa/prog0409.pdf" target="_blank">
                           <img class="marcprograma" src="/images/festes/cinta17/programa/i0409.jpg" width="125" height="177">
                        </a>
                        <br><br>
                        <div id="textnormal">Dilluns, 4 de setembre</div>
                        <div id="textnormal">Diada del Riu</div> 
                        <div id="textnormal"><i class="icon-file-pdf"></i></div>                       
                     </div>      
                     <div class="colprograma">
                        <a href="http://www.tortosa.cat/webajt/festes/cinta17/programa/progAltres.pdf" target="_blank">
                           <img class="marcprograma" src="/images/festes/cinta17/programa/iAltres.jpg" width="125" height="177">
                        </a>
                        <br><br>
                        <div id="textnormal">Altres activitats</div>
                        <div id="textnormal"><i class="icon-file-pdf"></i></div>                        
                     </div>
                     <div class="colprograma"> 
                        <a href="http://www.tortosa.cat/webajt/festes/cinta17/programa/progInforma.pdf" target="_blank">
                           <img class="marcprograma" src="/images/festes/cinta17/programa/iInforma.jpg" width="125" height="177">
                        </a>
                        <br><br>
                        <div id="textnormal">L'Ajuntament informa</div>
                        <div id="textnormal"><i class="icon-file-pdf"></i></div>                        
                     </div>                      
                  </div>

               </div>
            </div>

            <div class="separator"></div>

         </div>
      </div>
   </div>
</div><?php }} ?>
