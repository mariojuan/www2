<?php /* Smarty version Smarty-3.1.16, created on 2017-12-13 01:48:19
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/reguers/reguers_home.tpl" */ ?>
<?php /*%%SmartyHeaderCode:9791927545a3078d3d66195-79451437%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3899c4504ada76614383168cc6296cb715289d77' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/reguers/reguers_home.tpl',
      1 => 1499075720,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '9791927545a3078d3d66195-79451437',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'image' => 0,
    'LABEL_TITOL1' => 0,
    'MENU' => 0,
    'itemMenu' => 0,
    'LABEL_TITOL0' => 0,
    'item' => 0,
    'LABEL_TC1' => 0,
    'LABEL_TC2' => 0,
    'LABEL_TC3' => 0,
    'LABEL_TC4' => 0,
    'LABEL_TC5' => 0,
    'LABEL_TC6' => 0,
    'LABEL_TC7' => 0,
    'LABEL_TC12' => 0,
    'LABEL_TC8' => 0,
    'LABEL_TC9' => 0,
    'LABEL_TC10' => 0,
    'LABEL_TC11' => 0,
    'LABEL_TC13' => 0,
    'LABEL_TC14' => 0,
    'LABEL_TC15' => 0,
    'LABEL_TC16' => 0,
    'LABEL_TC17' => 0,
    'LABEL_TC18' => 0,
    'LABEL_TC19' => 0,
    'LABEL_TC20' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a3078d4015fb6_95573179',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a3078d4015fb6_95573179')) {function content_5a3078d4015fb6_95573179($_smarty_tpl) {?><!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->
<div id="page-wrap">

<div class="contenedor-responsive">

   <div id="conttranstotalcos">

      <div id="conttranscos">

         <div id="btranspresen">

            <div id="transimatge">			
               <img src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
" class="img-slider-territori"/>			
            </div>

            <div id="transimatgeText">
               <h1>
                  <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

                  <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
               </h1>
            </div>

         </div>

         <ul id='menu_planA'>
            <?php  $_smarty_tpl->tpl_vars['itemMenu'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['itemMenu']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['MENU']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['itemMenu']->key => $_smarty_tpl->tpl_vars['itemMenu']->value) {
$_smarty_tpl->tpl_vars['itemMenu']->_loop = true;
?>
            <a href=<?php echo $_smarty_tpl->tpl_vars['itemMenu']->value;?>
>
               <li id='menu_planA_item'>
                  <?php echo $_smarty_tpl->tpl_vars['itemMenu']->key;?>

               </li>
            </a>
            <?php } ?>
         </ul>

         <div id="tcentre_planA">
            <font size=4 color=#666666><?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL0']->value;?>
</font>
            <?php if ($_smarty_tpl->tpl_vars['item']->value==1) {?>
            <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
</strong></p>
            <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
</p>
            <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC3']->value;?>
</p>
            <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC4']->value;?>
</p>
            <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC5']->value;?>
&nbsp;<a href="mailto:reguers@tortosa.cat" target="_blank">reguers@tortosa.cat</a></p>
            <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC6']->value;?>
</p>
            <br><br>
            <div class="marcterme rectificacio-mapa-reguers">
               <iframe class="mapa-responsive" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3018.48232569288!2d0.44752601541442966!3d40.839333079318266!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a0ef7fb13173a7%3A0xe545edd32ebbeb7!2s43527+Els+Reguers%2C+Tarragona!5e0!3m2!1ses!2ses!4v1497005451424" width="400" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            <?php } elseif ($_smarty_tpl->tpl_vars['item']->value==2) {?>
            <ul class="subvencions1">
               <li>
                  <i class="icon-location"></i>&nbsp;<strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
</strong>
                  <ul>
                     <li><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
</li>
                     <li><?php echo $_smarty_tpl->tpl_vars['LABEL_TC3']->value;?>
</li>
                  </ul>
               </li>
               <li>
                  <i class="icon-location"></i>&nbsp;<strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC4']->value;?>
</strong>
                  <ul>
                     <li><?php echo $_smarty_tpl->tpl_vars['LABEL_TC5']->value;?>
</li>
                     <li><?php echo $_smarty_tpl->tpl_vars['LABEL_TC6']->value;?>
</li>
                     <li><?php echo $_smarty_tpl->tpl_vars['LABEL_TC7']->value;?>
</li>
                  </ul>
               </li>
               <li>
                  <i class="icon-location"></i>&nbsp;<strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC12']->value;?>
</strong>
               </li>
            </ul>
            <?php } elseif ($_smarty_tpl->tpl_vars['item']->value==3) {?>
            <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
</p>
            <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
</p>
            <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC3']->value;?>
</p>
            <?php } elseif ($_smarty_tpl->tpl_vars['item']->value==4) {?>
            <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
</p>
            <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
<strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC3']->value;?>
</strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC4']->value;?>
</p>
            <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC5']->value;?>
<strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC6']->value;?>
</strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC7']->value;?>
</p>
            <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC8']->value;?>
<strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC9']->value;?>
</strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC10']->value;?>
</p>
            <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC11']->value;?>
<strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC12']->value;?>
</strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC13']->value;?>
<strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC14']->value;?>
</strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC15']->value;?>
</p>
            <p><strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC16']->value;?>
</strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC17']->value;?>
</p>
            <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC18']->value;?>
<strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC19']->value;?>
</strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC20']->value;?>
</p>
            <?php } elseif ($_smarty_tpl->tpl_vars['item']->value==5) {?>
            <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
</p>
            <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
</p>
            <?php }?>
         </div>

      </div>
   </div>
</div><?php }} ?>
