<?php /* Smarty version Smarty-3.1.16, created on 2018-02-20 12:00:47
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/perfilc/perfilc_home.tpl" */ ?>
<?php /*%%SmartyHeaderCode:17952368685a2fe4076a42a3-13980290%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '10fee6c007d6e48675e851a007ea4951efd3d760' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/perfilc/perfilc_home.tpl',
      1 => 1519124438,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '17952368685a2fe4076a42a3-13980290',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a2fe407730544_28610929',
  'variables' => 
  array (
    'image' => 0,
    'LABEL_TITOL1' => 0,
    'MENU' => 0,
    'itemMenu' => 0,
    'LABEL_TITOL0' => 0,
    'item' => 0,
    'LABEL_TC1' => 0,
    'LABEL_TC2' => 0,
    'LABEL_TC3' => 0,
    'LABEL_TC4' => 0,
    'LABEL_TC11' => 0,
    'LABEL_TC12' => 0,
    'LABEL_TC8' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a2fe407730544_28610929')) {function content_5a2fe407730544_28610929($_smarty_tpl) {?><!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->
<div id="page-wrap" >
   <div class="contenedor-responsive">
      <div id="contpdctotalcos">
         <div id="conttranscos">
            <div id="btranspresen">
               <div id="transimatge">			
                  <img src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
" class="img-slider-territori"/>			
               </div>
               <div id="transimatgeText">
                  <h1>
                     <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

                     <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                  </h1>
               </div>
            </div>
            <ul id='menu_planA'>
               <?php  $_smarty_tpl->tpl_vars['itemMenu'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['itemMenu']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['MENU']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['itemMenu']->key => $_smarty_tpl->tpl_vars['itemMenu']->value) {
$_smarty_tpl->tpl_vars['itemMenu']->_loop = true;
?>
               <a href=<?php echo $_smarty_tpl->tpl_vars['itemMenu']->value;?>
>
                  <li id='menu_planA_item'>
                     <?php echo $_smarty_tpl->tpl_vars['itemMenu']->key;?>

                  </li>
               </a>
               <?php } ?>
            </ul>
            <div id="tcentre_planA">
               <font size=4 color=#666666>
               <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL0']->value;?>

               </font>
               <?php if ($_smarty_tpl->tpl_vars['item']->value==1) {?>
               <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
<br>
               <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
<br>
               <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC3']->value;?>
</p>
               <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC4']->value;?>
</p>
               <?php } elseif ($_smarty_tpl->tpl_vars['item']->value==2) {?>
               <center>
                  <a href="https://contractaciopublica.gencat.cat/ecofin_pscp/AppJava/cap.pscp?reqCode=viewDetail&keyword=tortosa&idCap=14665802&ambit=" target="_blank">
                     <div id="caixapdc1" class="marcfinestra caixapdcMod">
                        <?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
<br><?php echo $_smarty_tpl->tpl_vars['LABEL_TC11']->value;?>

                     </div>
                  </a>
                  <div id="caixapdc2">
                     <i class="icon-link"></i>&nbsp;
                     <a href="http://www.tortosa.cat/webajt/gestiointerna/licitacions/partpublica/index3.asp" target="_blank">
                     <?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
.&nbsp;
                     <?php echo $_smarty_tpl->tpl_vars['LABEL_TC12']->value;?>

                     </a>
                  </div>
               </center>
               <div id="titolaltrespdc">
                  <?php echo $_smarty_tpl->tpl_vars['LABEL_TC8']->value;?>

               </div>
               <ul class="ul_indexpcMod">
                  <li><i class="icon-link-ext"></i>&nbsp;<a href="http://transparencia.aiguesdetortosa.cat/ca/perfilcontractant" target="_blank">Empresa Municipal de Serveis Públics, SL</a></li>
                  <li><i class="icon-link-ext"></i>&nbsp;<a href="http://www.gumtsa.cat/licit.php" target="_blank">Empresa Gestió Urbanística Municipal de Tortosa, SA (GUMTSA)</a></li>
                  <li><i class="icon-link-ext"></i>&nbsp;<a href="https://contractaciopublica.gencat.cat/ecofin_pscp/AppJava/cap.pscp?reqCode=viewDetail&idCap=26439161" target="_blank">Tortosa Media, SL</a></li>
                  <li><i class="icon-link-ext"></i>&nbsp;<a href="http://www.tortosasport.cat/contractes-i-subvencions/" target="_blank">Tortosasport, SL</a></li>
                  <li><i class="icon-link-ext"></i>&nbsp;<a href="http://clinicaterresebre.cat/?cat=6" target="_blank">Tortosa Salut, SL</a></li>
                  <li><i class="icon-link-ext"></i>&nbsp;<a href="http://hospitalsantacreutortosa.cat/instruccions-gesat/" target="_blank">Gestió Sanitària i Assistencial de Tortosa SAM (GESAT)</a></li>
                  <li><i class="icon-link-ext"></i>&nbsp;<a href="http://hospitalsantacreutortosa.cat/instruccions-epel/" target="_blank">EPEL Hospital Llars de la Santa Creu</a></li>
               </ul>
               <?php } elseif ($_smarty_tpl->tpl_vars['item']->value==3) {?>
                  <p><a href="http://www.tortosa.cat/webajt/pressupost/2017/2017ResumTercers.pdf" target="_blank"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
</a></p>
                  <p><a href="http://www.tortosa.cat/webajt/pressupost/2016/2016ResumTercers.pdf" target="_blank"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
</a></p>
                  <p><a href="http://www.tortosa.cat/webajt/pressupost/2015/2015ResumTercers.pdf" target="_blank"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC3']->value;?>
</a></p>
               <?php }?>
            </div>
         </div>
      </div>
   </div>
</div>
<?php }} ?>
