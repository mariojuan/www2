<?php /* Smarty version Smarty-3.1.16, created on 2018-02-14 14:53:26
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/indicadors-transparencia/indicadors.tpl" */ ?>
<?php /*%%SmartyHeaderCode:10027724465a328357baa760-11112126%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd3c1f079fdd55df62669b2f4fd65380babdd8c94' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/indicadors-transparencia/indicadors.tpl',
      1 => 1518616403,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '10027724465a328357baa760-11112126',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a328357c19132_36932336',
  'variables' => 
  array (
    'LABEL_TITOL1' => 0,
    'LABEL_TITOL2' => 0,
    'lang' => 0,
    'LABEL_TXTINTRO' => 0,
    'grups' => 0,
    'grup' => 0,
    'subgrups' => 0,
    'subgrup' => 0,
    'indicadors' => 0,
    'indicador' => 0,
    'indicadors_links' => 0,
    'link' => 0,
    'hihaindicadors' => 0,
    'hihasubgrups' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a328357c19132_36932336')) {function content_5a328357c19132_36932336($_smarty_tpl) {?><html>
	<head>
        <?php echo $_smarty_tpl->getSubTemplate ("head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

	</head>
	<body id="indicadors">
        <?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

        <!-- PLANTILLA AMB NOMÉS UN MENÚ AL CENTRE. EL MENÚ ÉS TIPUS ACORDEÓ -->
        <div id="page-wrap">
            <div class="contenedor-responsive">
                <div id="conttranstotalcos">
                    <div id="conttranscos">
                        <div id="btranspresen">
                            <div id="transimatge">
                                <img src="/images/indicadors-transparencia/tira.jpg" class="img-slider-territori">
                            </div>
                            <div id="transimatgeText">
                                <h1>
                                    <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

                                </h1>
                            </div>
                        </div>
                        <div class="subtitolinfeco" id="subtitolinfeco2">
                            <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL2']->value;?>

                        </div>
                        <div class="marcback">
                            <a href="/indicadors-transparencia/?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
">
                                <i class="icon-angle-double-up"></i>
                            </a>
                        </div>
                        <div style="padding-top: 15px; clear: both">
                            <?php echo $_smarty_tpl->tpl_vars['LABEL_TXTINTRO']->value;?>

                        </div>
                        <ul id='menu' class='ulmenu' style="padding-left: 5px">
                            <?php if (count($_smarty_tpl->tpl_vars['grups']->value)>0) {?>
                                <?php  $_smarty_tpl->tpl_vars['grup'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['grup']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['grups']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['grup']->key => $_smarty_tpl->tpl_vars['grup']->value) {
$_smarty_tpl->tpl_vars['grup']->_loop = true;
?>
                                    <li>
                                        <a href>
                                            <?php echo $_smarty_tpl->tpl_vars['grup']->value->CODI_GRUP_INDICADOR;?>
 - <?php echo $_smarty_tpl->tpl_vars['grup']->value->GRUP_INDICADOR;?>

                                        </a>
                                        <!-- Subgrups -->
                                        <?php $_smarty_tpl->tpl_vars["hihasubgrups"] = new Smarty_variable("0", null, 0);?>
                                        <?php  $_smarty_tpl->tpl_vars['subgrup'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['subgrup']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['subgrups']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['subgrup']->key => $_smarty_tpl->tpl_vars['subgrup']->value) {
$_smarty_tpl->tpl_vars['subgrup']->_loop = true;
?>
                                            <?php if ($_smarty_tpl->tpl_vars['subgrup']->value->ID_GRUP==$_smarty_tpl->tpl_vars['grup']->value->ID) {?>
                                                <?php if ($_smarty_tpl->tpl_vars['subgrup']->key==0) {?>
                                                    <ul class='ulmenusub'>
                                                    <?php $_smarty_tpl->tpl_vars["hihasubgrups"] = new Smarty_variable("1", null, 0);?>
                                                <?php }?>
                                                <li>
                                                    <a href>
                                                        <?php echo $_smarty_tpl->tpl_vars['subgrup']->value->CODI_SUBGRUP_INDICADOR;?>
 - <?php echo $_smarty_tpl->tpl_vars['subgrup']->value->SUBGRUP_INDICADOR;?>

                                                    </a>
                                                    <!-- Indicadors -->
                                                    <?php $_smarty_tpl->tpl_vars["hihaindicadors"] = new Smarty_variable("0", null, 0);?>
                                                    <?php  $_smarty_tpl->tpl_vars['indicador'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['indicador']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['indicadors']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['indicador']->key => $_smarty_tpl->tpl_vars['indicador']->value) {
$_smarty_tpl->tpl_vars['indicador']->_loop = true;
?>
                                                        <?php if ($_smarty_tpl->tpl_vars['indicador']->value->ID_GRUP==$_smarty_tpl->tpl_vars['grup']->value->ID&&$_smarty_tpl->tpl_vars['indicador']->value->ID_SUBGRUP==$_smarty_tpl->tpl_vars['subgrup']->value->ID) {?>
                                                            <?php if ($_smarty_tpl->tpl_vars['indicador']->key==0) {?>
                                                                <ul class='llistat_indicadors'>
                                                                <?php $_smarty_tpl->tpl_vars["hihaindicadors"] = new Smarty_variable("1", null, 0);?>
                                                            <?php }?>
                                                            <li>
                                                                <?php if ($_smarty_tpl->tpl_vars['indicador']->value->LINK!='') {?>
                                                                    <i class="icon-link" style="float: left; padding-top: 8px;"></i><a href="<?php echo $_smarty_tpl->tpl_vars['indicador']->value->LINK;?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['indicador']->value->CODI_INDICADOR;?>
 - <?php echo $_smarty_tpl->tpl_vars['indicador']->value->INDICADOR;?>
</a>
                                                                <?php } else { ?>
                                                                <label style="font-weight: 700; font-size: 14px;"><?php echo $_smarty_tpl->tpl_vars['indicador']->value->CODI_INDICADOR;?>
 - <?php echo $_smarty_tpl->tpl_vars['indicador']->value->INDICADOR;?>
</label>
                                                                <?php }?>
                                                                <?php if ($_smarty_tpl->tpl_vars['indicador']->value->COMENTARIS!='') {?>
                                                                <p>
                                                                    <?php echo $_smarty_tpl->tpl_vars['indicador']->value->COMENTARIS;?>

                                                                </p>
                                                                <?php }?>
                                                                <?php  $_smarty_tpl->tpl_vars['link'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['link']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['indicadors_links']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['link']->key => $_smarty_tpl->tpl_vars['link']->value) {
$_smarty_tpl->tpl_vars['link']->_loop = true;
?>
                                                                    <?php if ($_smarty_tpl->tpl_vars['link']->value->ID_INDICADOR==$_smarty_tpl->tpl_vars['indicador']->value->ID) {?>
                                                                    <p>
                                                                        <i class="icon-link" style="float: left; padding-top: 8px;"></i><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->LINK;?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['link']->value->DESCRIPCIO;?>
</a>
                                                                    </p>
                                                                    <?php }?>
                                                                <?php } ?>
                                                            </li>
                                                        <?php }?>
                                                    <?php } ?>
                                                    <?php if ($_smarty_tpl->tpl_vars['hihaindicadors']->value==1) {?>
                                                        </ul>
                                                    <?php }?>
                                                </li>
                                            <?php }?>
                                        <?php } ?>
                                        <?php if ($_smarty_tpl->tpl_vars['hihasubgrups']->value==1) {?>
                                        </ul>
                                        <?php }?>
                                        <!-- Indicadors sense subgrup -->
                                        <ul class='llistat_indicadors ulmenusub'>
                                            <?php  $_smarty_tpl->tpl_vars['indicador'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['indicador']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['indicadors']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['indicador']->key => $_smarty_tpl->tpl_vars['indicador']->value) {
$_smarty_tpl->tpl_vars['indicador']->_loop = true;
?>
                                                <?php if ($_smarty_tpl->tpl_vars['indicador']->value->ID_SUBGRUP==0&&$_smarty_tpl->tpl_vars['indicador']->value->ID_GRUP==$_smarty_tpl->tpl_vars['grup']->value->ID) {?>
                                                <li style="margin-bottom: 20px">
                                                    <?php if ($_smarty_tpl->tpl_vars['indicador']->value->LINK!='') {?>
                                                    <a href="<?php echo $_smarty_tpl->tpl_vars['indicador']->value->LINK;?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['indicador']->value->CODI_INDICADOR;?>
 - <?php echo $_smarty_tpl->tpl_vars['indicador']->value->INDICADOR;?>
</a>
                                                    <?php } else { ?>
                                                        <label style="font-weight: 700; font-size: 14px;"><?php echo $_smarty_tpl->tpl_vars['indicador']->value->CODI_INDICADOR;?>
 - <?php echo $_smarty_tpl->tpl_vars['indicador']->value->INDICADOR;?>
</label>
                                                    <?php }?>
                                                    <p>
                                                        <?php echo $_smarty_tpl->tpl_vars['indicador']->value->COMENTARIS;?>

                                                    </p>
                                                    <?php  $_smarty_tpl->tpl_vars['link'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['link']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['indicadors_links']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['link']->key => $_smarty_tpl->tpl_vars['link']->value) {
$_smarty_tpl->tpl_vars['link']->_loop = true;
?>
                                                        <?php if ($_smarty_tpl->tpl_vars['link']->value->ID_INDICADOR==$_smarty_tpl->tpl_vars['indicador']->value->ID) {?>
                                                            <p>
                                                                <i class="icon-link" style="float: left; padding-top: 8px;"></i><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->LINK;?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['link']->value->DESCRIPCIO;?>
</a>
                                                            </p>
                                                        <?php }?>
                                                    <?php } ?>
                                                </li>
                                                <?php }?>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                <?php } ?>
                            <?php }?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript" charset="utf-8">
            $(function(){
                $('#menu li a').click(function(event){
                    var elem = $(this).next();
                    var elem1 = $(this).next().next();

                    if(elem.is('ul')){
                        event.preventDefault();
                        //$('#menu ul:visible').not(elem).slideUp();
                        $('#menu > li:visible').not(elem.parent("li")).css("background-image", "url(/images/ptranspa/fonsboto1.jpg)");
                        elem.slideToggle();
                        elem1.slideToggle();
                        tipusfons=$(elem.parent('> li')).css("background-image");
                        fons="url(/images/ptranspa/fonsboto1.jpg)";
                        if(tipusfons==fons){
                            elem.parent("li").css("background-image", "url(/images/ptranspa/fonsboto2.jpg)");
                        }
                        else{
                            elem.parent("li").css("background-image", "url(/images/ptranspa/fonsboto1.jpg)");
                        }
                    }
                });

            });
        </script>

        <?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

	</body>
</html><?php }} ?>
