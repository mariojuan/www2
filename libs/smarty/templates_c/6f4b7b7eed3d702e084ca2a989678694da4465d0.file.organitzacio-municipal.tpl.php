<?php /* Smarty version Smarty-3.1.16, created on 2017-12-12 16:10:23
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/organitzacio-municipal/organitzacio-municipal.tpl" */ ?>
<?php /*%%SmartyHeaderCode:6483384365a2ff15fa406a9-89078999%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6f4b7b7eed3d702e084ca2a989678694da4465d0' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/organitzacio-municipal/organitzacio-municipal.tpl',
      1 => 1508493859,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6483384365a2ff15fa406a9-89078999',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'image' => 0,
    'LABEL_TITOL1' => 0,
    'LABEL_TC0' => 0,
    'LABEL_TC1' => 0,
    'LABEL_TC2' => 0,
    'LABEL_TC3' => 0,
    'lang' => 0,
    'LABEL_MESINFO' => 0,
    'LABEL_TC4' => 0,
    'LABEL_TC5' => 0,
    'LABEL_TC6' => 0,
    'LABEL_TC7' => 0,
    'LABEL_CUADRE_PCIUTADANA' => 0,
    'LABEL_CUADRE_ESTADISTICA' => 0,
    'LABEL_CUADRE_ELECCIONS' => 0,
    'LABEL_CUADRE_AJUNTAMENT' => 0,
    'LABEL_CUADRE_DESCENTRALITZADES' => 0,
    'LABEL_CUADRE_ALTRES_ENS' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a2ff15fb28f65_76263535',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a2ff15fb28f65_76263535')) {function content_5a2ff15fb28f65_76263535($_smarty_tpl) {?><html>
   <head>
      <?php echo $_smarty_tpl->getSubTemplate ("head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

   </head>
   <body>
      <?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

      <div id="page-wrap">

         <div class="contenedor-responsive">

            <div id="conttranstotalcos">
               <div id="conttranscos">
                  <div id="btranspresen">

                     <div id="transimatge">
                        <img src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
" class="img-slider-territori" />
                     </div>

                     <div id="transimatgeText">
                        <h1>
                           <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

                        </h1>
                     </div>
                  </div>
                  <div id="tcentre_municipi">
                     <font size="4" color="#666666"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC0']->value;?>
</font>
                     <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
</p>
                     <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
</p>
                     <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC3']->value;?>
 <a href="http://www2.tortosa.cat/organitzacio-municipal-entitats-municipals-descentralitzades/index.php?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['LABEL_MESINFO']->value;?>
</a></p>
                     <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC4']->value;?>
</p>
                     <!-- <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC5']->value;?>
 <a href="" target="_blank"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC6']->value;?>
</a></p> -->
                     <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC7']->value;?>
 <a href="http://www2.tortosa.cat/organitzacio-municipal-participacio-altres-ens?lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['LABEL_MESINFO']->value;?>
</a></p>
                     <img src="http://www.tortosa.cat/webajt/ajunta/om/Estructura7.jpg" class="img-responsive"/>

                     <p>Items informatius</p>
                     <ul>
                        <li>
                           <a href="<?php echo $_smarty_tpl->tpl_vars['LABEL_CUADRE_PCIUTADANA']->value;?>
" target="_self" alt="Participaci&oacute; ciutadana">Participació ciutadana</a>
                        </li>
                        <li>
                           <a href="<?php echo $_smarty_tpl->tpl_vars['LABEL_CUADRE_ESTADISTICA']->value;?>
" target="_self" alt="Estad&iacute;stica d'habitants">Estadística d'habitants</a>
                        </li>
                        <li>
                           <a href="<?php echo $_smarty_tpl->tpl_vars['LABEL_CUADRE_ELECCIONS']->value;?>
" target="_blank" alt="Eleccions">Eleccions</a>
                        </li>
                        <li>
                           Grup municipal
                           <ul>
                              <li>
                                 <a href="<?php echo $_smarty_tpl->tpl_vars['LABEL_CUADRE_AJUNTAMENT']->value;?>
" target="_self">Ajuntament</a>
                              </li>
                              <li>
                                 Ens dependents
                                 <ul>
                                    <li>
                                       capital íntegrament municipal
                                       <ul>
                                          <li>
                                             <a href="http://www.tortosa.cat/webajt/ajunta/om/ens-dependents/tortosasport/fitxa-ens-dependent.pdf" target="_blank" alt="TortosaSport, SL">Tortosasport, SL</a>
                                          </li>
                                          <li>
                                             <a href="http://www.tortosa.cat/webajt/ajunta/om/ens-dependents/tortosa-media/fitxa-ens-dependent.pdf" target="_blank" alt="TortosaMedia, SL">Tortosamedia, SL</a>
                                          </li>
                                          <li>
                                             <a href="http://www.tortosa.cat/webajt/ajunta/om/ens-dependents/gumtsa/fitxa-ens-dependent.pdf" target="_blank" alt="GUMTSA">Gestió urbanística municipal de Tortosa, SA (GUMTSA)</a>
                                          </li>
                                          <li>
                                             <a  href="http://www.tortosa.cat/webajt/ajunta/om/ens-dependents/epel-hospital/fitxa-ens-dependent.pdf" target="_blank" alt="EPE Hospital i Llars de la Santa Creu">EPEL Hospital i Llars de la Santa Creu</a>
                                          </li>
                                          <li>
                                             <a href="http://www.tortosa.cat/webajt/ajunta/om/ens-dependents/gesat/fitxa-ens-dependent.pdf" target="_blank" alt="GESAT, SA">Gestió Sanitària Assistencial de Tortosa, SAM</a>
                                          </li>
                                          <li>
                                             <a href="http://www.tortosa.cat/webajt/ajunta/om/ens-dependents/tortosa-salut/fitxa-ens-dependent.pdf" target="_blank" alt="Tortosa Salut, SL">Tortosa Salut, SL</a>
                                          </li>
                                       </ul>
                                       <li>
                                          capital majoritariament municipal
                                          <ul>
                                             <li>
                                                <a href="http://www.tortosa.cat/webajt/ajunta/om/ens-dependents/emsp/fitxa-ens-dependent.pdf" target="_blank" alt="EMSP, SL">Empresa Municipal de Serveis Públics, SL</a>
                                             </li>
                                             <li>
                                                <a href="http://www.tortosa.cat/webajt/ajunta/om/ens-dependents/escorxador/fitxa-ens-dependent.pdf" target="_blank">Empresa Mixta Escorxador Tortosa, SL</a>
                                             </li>
                                          </ul>
                                       </li>
                                    </li>
                                 </ul>
                              </li>
                           </ul>
                        </li>
                        <li>
                           <a href="<?php echo $_smarty_tpl->tpl_vars['LABEL_CUADRE_DESCENTRALITZADES']->value;?>
" target="_self" alt="Entitats Municipals Descentralitzades de Jes&uacute;s">Entitats Municipals Descentralitzades</a>
                           <ul>
                              <li>
                                 <a href="http://www.bitem.altanet.org/" target="_blank" alt="EMD de B&iacute;tem">EMD de Bítem</a>
                              </li>
                              <li>
                                 <a href="http://www.campredo.cat/" target="_blank" alt="EMD de Campred&oacute;">EMD de Campredó</a>
                              </li>
                              <li>
                                 <a href="http://www.jesus.cat" target="_blank" alt="EMd de Jesús">EMD de Jesús</a>
                              </li>
                           </ul>
                        </li>
                        <li>
                           <a href="<?php echo $_smarty_tpl->tpl_vars['LABEL_CUADRE_ALTRES_ENS']->value;?>
" target="_self">Participació en altres ens</a>
                        </li>
                     </ul>
                  </div>
                  <div class="separator"></div>
               </div>
            </div>

         </div>

      </div>
      <?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

   </body>
</html><?php }} ?>
