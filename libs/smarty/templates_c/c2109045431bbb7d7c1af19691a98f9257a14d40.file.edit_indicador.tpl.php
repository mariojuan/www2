<?php /* Smarty version Smarty-3.1.16, created on 2018-01-19 09:06:51
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/admin/templates/indicadors/edit_indicador.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11719766885a322cb859fc15-21054807%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c2109045431bbb7d7c1af19691a98f9257a14d40' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/admin/templates/indicadors/edit_indicador.tpl',
      1 => 1516349209,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11719766885a322cb859fc15-21054807',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a322cb85d25f4_18178725',
  'variables' => 
  array (
    'item' => 0,
    'lang' => 0,
    'links' => 0,
    'grups' => 0,
    'grup' => 0,
    'subgrups' => 0,
    'subgrup' => 0,
    'i' => 0,
    'cont' => 0,
    'link' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a322cb85d25f4_18178725')) {function content_5a322cb85d25f4_18178725($_smarty_tpl) {?><section id="main" class="column">
   <h4 class="alert_info"><i class="icon-doc-new"></i><?php if ($_smarty_tpl->tpl_vars['item']->value[0]->ID=='') {?>Alta<?php } else { ?>Modificació<?php }?> d'Indicador (<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
)</h4>
   <article class="module width_full">
      <div class="module_content">
         <form id="indicadorsGrupForm" method="post" action="indicadors.php" novalidate="novalidate" enctype="multipart/form-data">
             <input type="hidden" name="accio" id="accio" value="edit_indicador">
             <input type="hidden" name="lang" id="lang" value="<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
">
             <input type="hidden" name="ID" id="ID" value="<?php echo $_smarty_tpl->tpl_vars['item']->value[0]->ID;?>
">
             <input type="hidden" name="max_links" id="max_links" value="<?php if (count($_smarty_tpl->tpl_vars['links']->value)==0) {?>1<?php } else { ?><?php echo count($_smarty_tpl->tpl_vars['links']->value);?>
<?php }?>">
            <?php if ($_smarty_tpl->tpl_vars['lang']->value=='ca') {?>
            <p>
                <label>Grup *</label>
                <select name="ID_GRUP" id="ID_GRUP">
                    <option value="">-- Seleccionar --</option>
                    <?php  $_smarty_tpl->tpl_vars['grup'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['grup']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['grups']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['grup']->key => $_smarty_tpl->tpl_vars['grup']->value) {
$_smarty_tpl->tpl_vars['grup']->_loop = true;
?>
                        <?php if ($_smarty_tpl->tpl_vars['grup']->value->ID==$_smarty_tpl->tpl_vars['item']->value[0]->ID_GRUP) {?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['grup']->value->ID;?>
" selected><?php echo $_smarty_tpl->tpl_vars['grup']->value->GRUP_INDICADOR;?>
</option>
                        <?php } else { ?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['grup']->value->ID;?>
"><?php echo $_smarty_tpl->tpl_vars['grup']->value->GRUP_INDICADOR;?>
</option>
                        <?php }?>
                    <?php } ?>
                </select>
            </p>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['lang']->value=='ca') {?>
                 <p>
                     <label>Subgrup</label>
                     <select name="ID_SUBGRUP" id="ID_SUBGRUP">
                         <option value="">-- Seleccionar --</option>
                         <?php  $_smarty_tpl->tpl_vars['subgrup'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['subgrup']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['subgrups']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['subgrup']->key => $_smarty_tpl->tpl_vars['subgrup']->value) {
$_smarty_tpl->tpl_vars['subgrup']->_loop = true;
?>
                             <?php if ($_smarty_tpl->tpl_vars['subgrup']->value->ID==$_smarty_tpl->tpl_vars['item']->value[0]->ID_SUBGRUP) {?>
                                 <option value="<?php echo $_smarty_tpl->tpl_vars['subgrup']->value->ID;?>
" selected><?php echo $_smarty_tpl->tpl_vars['subgrup']->value->SUBGRUP_INDICADOR;?>
</option>
                             <?php } else { ?>
                                 <option value="<?php echo $_smarty_tpl->tpl_vars['subgrup']->value->ID;?>
"><?php echo $_smarty_tpl->tpl_vars['subgrup']->value->SUBGRUP_INDICADOR;?>
</option>
                             <?php }?>
                         <?php } ?>
                     </select>
                 </p>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['lang']->value=='ca') {?>
            <p>
               <label>Codi *</label>
               <input type="text" name="CODI_INDICADOR" id="CODI_INDICADOR" value="<?php echo $_smarty_tpl->tpl_vars['item']->value[0]->CODI_INDICADOR;?>
">
            </p>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['lang']->value=='ca') {?>
            <p>
               <label>Tipus indicador *</label>
               <select name="TIPUS_INDICADOR" id="TIPUS_INDICADOR">
                   <option value="">-- Seleccionar --</option>
                   <option value="Infoparticipa" <?php if ($_smarty_tpl->tpl_vars['item']->value[0]->TIPUS_INDICADOR=="Infoparticipa") {?>selected<?php }?>>Infoparticipa</option>
                   <option value="ITA" <?php if ($_smarty_tpl->tpl_vars['item']->value[0]->TIPUS_INDICADOR=="ITA") {?>selected<?php }?>>ITA</option>
               </select>
            </p>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['lang']->value=='ca') {?>
            <p>
               <label>Exercici *</label>
                <select name="EXERCICI" id="EXERCICI">
                    <option value="">-- Seleccionar --</option>
                    <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? 2050+1 - (2017) : 2017-(2050)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 2017, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
                        <?php if ($_smarty_tpl->tpl_vars['i']->value==$_smarty_tpl->tpl_vars['item']->value[0]->EXERCICI) {?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" selected><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
</option>
                        <?php } else { ?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
</option>
                        <?php }?>
                    <?php }} ?>
                </select>
            </p>
            <?php }?>
            <p>
                 <label>Nom Indicador *</label>
                 <input type="text" name="INDICADOR" id="INDICADOR" value="<?php echo $_smarty_tpl->tpl_vars['item']->value[0]->INDICADOR;?>
">
            </p>
            <p id="add_link" style="cursor: hand">afegir enllaç</p>
            <div id="links_list">
                <?php $_smarty_tpl->tpl_vars["cont"] = new Smarty_variable("0", null, 0);?>
                <?php if (count($_smarty_tpl->tpl_vars['links']->value)>0) {?>
                    <?php  $_smarty_tpl->tpl_vars['link'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['link']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['links']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['link']->key => $_smarty_tpl->tpl_vars['link']->value) {
$_smarty_tpl->tpl_vars['link']->_loop = true;
?>
                        <?php $_smarty_tpl->tpl_vars["cont"] = new Smarty_variable($_smarty_tpl->tpl_vars['cont']->value+1, null, 0);?>
                        <div id="fila_link<?php echo $_smarty_tpl->tpl_vars['cont']->value;?>
">
                        <p>
                            <label>Descripció enllaç</label>
                            <input type="text" name="DESCRIPCIO<?php echo $_smarty_tpl->tpl_vars['cont']->value;?>
" id="DESCRIPCIO<?php echo $_smarty_tpl->tpl_vars['cont']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['link']->value->DESCRIPCIO;?>
" class="link">
                        </p>
                        <p>
                            <label>Enllaç *</label>
                            <input type="text" name="LINK<?php echo $_smarty_tpl->tpl_vars['cont']->value;?>
" id="LINK<?php echo $_smarty_tpl->tpl_vars['cont']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['link']->value->LINK;?>
" class="link"><i class="icon-trash-empty" id="<?php echo $_smarty_tpl->tpl_vars['cont']->value;?>
"></i>
                        </p>
                        </div>
                    <?php } ?>
                <?php } else { ?>
                    <?php $_smarty_tpl->tpl_vars["cont"] = new Smarty_variable($_smarty_tpl->tpl_vars['cont']->value+1, null, 0);?>
                    <div id="fila_link<?php echo $_smarty_tpl->tpl_vars['cont']->value;?>
">
                    <p>
                        <label>Descripció enllaç</label>
                        <input type="text" name="DESCRIPCIO<?php echo $_smarty_tpl->tpl_vars['cont']->value;?>
" id="DESCRIPCIO<?php echo $_smarty_tpl->tpl_vars['cont']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['link']->value->DESCRIPCIO;?>
" class="link">
                    </p>
                    <p>
                        <label>Enllaç *</label>
                        <input type="text" name="LINK<?php echo $_smarty_tpl->tpl_vars['cont']->value;?>
" id="LINK<?php echo $_smarty_tpl->tpl_vars['cont']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['link']->value->LINK;?>
" class="link"><i class="icon-trash-empty" id="<?php echo $_smarty_tpl->tpl_vars['cont']->value;?>
"></i>
                    </p>
                    </div>
                <?php }?>
            </div>
            <?php if ($_smarty_tpl->tpl_vars['lang']->value=='ca') {?>
                <p>
                    <label>Estat *</label>
                    <select name="ESTAT" id="ESTAT">
                        <option value="">-- Seleccionar --</option>
                        <?php if ($_smarty_tpl->tpl_vars['item']->value[0]->ESTAT=="Completat") {?>
                        <option value="Completat" selected>Completat</option>
                        <?php } else { ?>
                        <option value="Completat">Completat</option>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['item']->value[0]->ESTAT=="No completat") {?>
                        <option value="No completat" selected>No completat</option>
                        <?php } else { ?>
                        <option value="No completat">No completat</option>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['item']->value[0]->ESTAT=="En proces") {?>
                        <option value="En proces" selected>En procés</option>
                        <?php } else { ?>
                        <option value="En proces">En procés</option>
                        <?php }?>
                    </select>
                </p>
            <?php }?>
            <p>
                 <label>Comentaris</label>
                 <textarea name="COMENTARIS" id="COMENTARIS"><?php echo $_smarty_tpl->tpl_vars['item']->value[0]->COMENTARIS;?>
</textarea>
            </p>
            <?php if ($_smarty_tpl->tpl_vars['lang']->value=='ca') {?>
            <p>
                <label>Baixa</label>
                <input type="checkbox" name="BAIXA" id="BAIXA" <?php if ($_smarty_tpl->tpl_vars['item']->value[0]->BAIXA) {?>checked <?php }?>>
            </p>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['lang']->value=='ca') {?>
            <p>
                <label>Visible</label>
                <input type="checkbox" name="VISIBLE" id="VISIBLE" <?php if ($_smarty_tpl->tpl_vars['item']->value[0]->VISIBLE) {?>checked <?php }?>>
            </p>
            <?php }?>
            <p>
               <input class="submit" type="submit" value="  <?php if ($_smarty_tpl->tpl_vars['item']->value[0]->ID=='') {?>Alta<?php } else { ?>Modificació<?php }?>  ">
            </p>
            <p>
               <label>* Camps obligatoris</label>
            </p>
         </form>
      </div>
   </article>

   <!-- end of styles article -->
   <div class="spacer"></div>
</section>
<script>
    $().ready(function() {
        var cont = <?php echo $_smarty_tpl->tpl_vars['cont']->value+1;?>
;
        $("#indicadorsGrupForm").validate({
            rules: {
                ID_GRUP:  {
                    required: true
                },
                CODI_INDICADOR:  {
                    required: true
                },
                TIPUS_INDICADOR: {
                    required: true
                },
                EXERCICI: "required",
                INDICADOR: "required",
                LINK: "required",
                ESTAT: "required"
            },
            messages: {
                ID_GRUP:  {
                    required: "El camp de grup d'indicador és obligatori"
                },
                CODI_INDICADOR: {
                    required: "El camp de codi d'indicador és obligatori"
                },
                TIPUS_INDICADOR: {
                    required: "El camp de tipus d'indicador és obligatori"
                },
                EXERCICI: {
                    required: "El camp d'exercici és obligatori"
                },
                INDICADOR: {
                    required: "El camp d'indicador és obligatori"
                },
                LINK: {
                    required: "El camp d'enllaç és obligatori"
                },
                ESTAT: {
                    required: "El camp d'estat és obligatori"
                }
            },
            submitHandler: function(form) {
                if ($('#BAIXA').prop('checked')) {
                    $('#BAIXA').val("1");
                }
                else {
                    $('#BAIXA').val("0");
                }
                if($('#VISIBLE').prop('checked')) {
                    $('#VISIBLE').val("1");
                }
                else {
                    $('#VISIBLE').val("0");
                }
                form.submit();
            }
        });
        $('#add_link').click(function(){
            var content = '' +
                    '<div id="fila_link'+cont+'"> ' +
                    '<p> ' +
                            '<label>Descripció enllaç</label>' +
                            ' <input type="text" name="DESCRIPCIO'+cont+'" id="DESCRIPCIO'+cont+'" class="link" value="">' +
                          '</p>' +
                    '<p>' +
                        '<label>Enllaç *</label>'+
                        ' <input type="text" name="LINK'+cont+'" id="LINK'+cont+'" value="" class="link"><i class="icon-trash-empty" id="'+cont+'"></i>' +
                    '</p>' +
                    '</div>';
            $('#links_list').append(content);
            $('#max_links').val(cont);
            cont++;
        });
        $(document).on('click', '.icon-trash-empty', function() {
            $('#DESCRIPCIO'+$(this).attr('id')).val('');
            $('#LINK'+$(this).attr('id')).val('');
            $('#fila_link'+$(this).attr('id')).remove();
            cont--;
        });
    });
</script><?php }} ?>
