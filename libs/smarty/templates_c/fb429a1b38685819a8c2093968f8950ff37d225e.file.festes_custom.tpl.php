<?php /* Smarty version Smarty-3.1.16, created on 2018-01-18 14:41:52
         compiled from "/var/www/vhost/www2.tortosa.cat/home/html/templates/festes/festes_custom.tpl" */ ?>
<?php /*%%SmartyHeaderCode:19595478405a301f5af1a230-77289840%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fb429a1b38685819a8c2093968f8950ff37d225e' => 
    array (
      0 => '/var/www/vhost/www2.tortosa.cat/home/html/templates/festes/festes_custom.tpl',
      1 => 1516282908,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '19595478405a301f5af1a230-77289840',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5a301f5b09ff96_30723347',
  'variables' => 
  array (
    'image' => 0,
    'LABEL_TITOL1' => 0,
    'MENU' => 0,
    'itemMenu' => 0,
    'type' => 0,
    'LABEL_TC0' => 0,
    'LABEL_TC1' => 0,
    'LABEL_TC2' => 0,
    'LABEL_TC3' => 0,
    'LABEL_TC4' => 0,
    'LABEL_TC00' => 0,
    'LABEL_TC6' => 0,
    'LABEL_TC7' => 0,
    'LABEL_TC7b' => 0,
    'LABEL_TC8' => 0,
    'LABEL_TC8b' => 0,
    'LABEL_TC9' => 0,
    'LABEL_TC9b' => 0,
    'LABEL_TC10' => 0,
    'LABEL_TC11' => 0,
    'LABEL_TC000' => 0,
    'LABEL_TC12b' => 0,
    'LABEL_TC13' => 0,
    'LABEL_TC14' => 0,
    'LABEL_TC15' => 0,
    'LABEL_TC16' => 0,
    'LABEL_TC17' => 0,
    'LABEL_TC0000' => 0,
    'LABEL_TC12a' => 0,
    'LABEL_TC18' => 0,
    'LABEL_TC19' => 0,
    'LABEL_TC20' => 0,
    'LABEL_TC21' => 0,
    'LABEL_TC22' => 0,
    'LABEL_TC23' => 0,
    'LABEL_TC24' => 0,
    'LABEL_TC25' => 0,
    'LABEL_TC26' => 0,
    'LABEL_TC27' => 0,
    'LABEL_TC5' => 0,
    'LABEL_TC12' => 0,
    'PORTADES' => 0,
    'itemPortada' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a301f5b09ff96_30723347')) {function content_5a301f5b09ff96_30723347($_smarty_tpl) {?><!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->
<div id="page-wrap">

   <div class="contenedor-responsive">

      <div id="conttranstotalcos">

         <div id="conttranscos">

            <div id="btranspresen">

               <div id="transimatge">
                  <img src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
" class="img-slider-territori"/>
               </div>

               <div id="transimatgeText">
                  <h1>
                     <?php echo $_smarty_tpl->tpl_vars['LABEL_TITOL1']->value;?>

                     <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                  </h1>
               </div>

            </div>

            <ul id='menu_planA'>
               <?php  $_smarty_tpl->tpl_vars['itemMenu'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['itemMenu']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['MENU']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['itemMenu']->key => $_smarty_tpl->tpl_vars['itemMenu']->value) {
$_smarty_tpl->tpl_vars['itemMenu']->_loop = true;
?>
               <a href=<?php echo $_smarty_tpl->tpl_vars['itemMenu']->value['link'];?>
 target="_<?php echo $_smarty_tpl->tpl_vars['itemMenu']->value['target'];?>
">
                  <li id='menu_planA_item'>
                     <?php echo $_smarty_tpl->tpl_vars['itemMenu']->value['name'];?>

                  </li>
               </a>
               <?php } ?>
            </ul>

            <div id="tcentre_planA">
               <?php if ($_smarty_tpl->tpl_vars['type']->value==1) {?>
               <h2><?php echo $_smarty_tpl->tpl_vars['LABEL_TC0']->value;?>
</h2>
               <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
</p>
               <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
</p>
               <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC3']->value;?>
</p>
               <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC4']->value;?>
</p>
               <?php } elseif ($_smarty_tpl->tpl_vars['type']->value==6) {?>
               <h2><?php echo $_smarty_tpl->tpl_vars['LABEL_TC00']->value;?>
</h2>
               <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC6']->value;?>
</p>
               <p><strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC7']->value;?>
</strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC7b']->value;?>
</p>
               <p><strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC8']->value;?>
</strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC8b']->value;?>
</p>
               <p><strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC9']->value;?>
</strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC9b']->value;?>
</p>
               <p><strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC10']->value;?>
</strong> <a href="mailto:<?php echo $_smarty_tpl->tpl_vars['LABEL_TC11']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC11']->value;?>
</a></p>
               <?php } elseif ($_smarty_tpl->tpl_vars['type']->value==7) {?>
               <h2><?php echo $_smarty_tpl->tpl_vars['LABEL_TC000']->value;?>
</h2>
               <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC12b']->value;?>
</p>
               <ul class="subvencions1">
                  <li><i class="icon-sun"></i>&nbsp;<?php echo $_smarty_tpl->tpl_vars['LABEL_TC13']->value;?>
</li>
                  <li><i class="icon-sun"></i>&nbsp;<?php echo $_smarty_tpl->tpl_vars['LABEL_TC14']->value;?>
</li>
                  <li><i class="icon-sun"></i>&nbsp;<?php echo $_smarty_tpl->tpl_vars['LABEL_TC15']->value;?>
</li>
                  <li><i class="icon-sun"></i>&nbsp;<?php echo $_smarty_tpl->tpl_vars['LABEL_TC16']->value;?>
</li>
                  <li><i class="icon-sun"></i>&nbsp;<?php echo $_smarty_tpl->tpl_vars['LABEL_TC17']->value;?>
</li>
               </ul>
               <?php } elseif ($_smarty_tpl->tpl_vars['type']->value==8) {?>
               <h2><?php echo $_smarty_tpl->tpl_vars['LABEL_TC0000']->value;?>
</h2>
               <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC12a']->value;?>
</p>
               <ul class="subvencions1">
                  <li><i class="icon-sun"></i>&nbsp;<a href="http://www.bitem.altanet.org/municipi/festesmajors/index.php" target="_blank"><strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC18']->value;?>
</strong></a>:&nbsp;<?php echo $_smarty_tpl->tpl_vars['LABEL_TC19']->value;?>
</li>
                  <li><i class="icon-sun"></i>&nbsp;<a href="http://www.campredo.cat/ca/area/festes" target="_blank"><strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC20']->value;?>
</strong></a>:&nbsp;<?php echo $_smarty_tpl->tpl_vars['LABEL_TC21']->value;?>
</li>
                  <li><i class="icon-sun"></i>&nbsp;<strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC22']->value;?>
</strong>:&nbsp;<?php echo $_smarty_tpl->tpl_vars['LABEL_TC23']->value;?>
</li>
                  <li><i class="icon-sun"></i>&nbsp;<a href="http://www.emdjesus.cat/index.php?id=2" target="_blank"><strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC24']->value;?>
</strong></a>:&nbsp;<?php echo $_smarty_tpl->tpl_vars['LABEL_TC25']->value;?>
</li>
                  <li><i class="icon-sun"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/pobles/vinallop/festes.asp" target="_blank"><strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC26']->value;?>
</strong></a>:&nbsp;<?php echo $_smarty_tpl->tpl_vars['LABEL_TC27']->value;?>
</li>
               </ul>
               <?php } elseif ($_smarty_tpl->tpl_vars['type']->value==4) {?>
               <h2><?php echo $_smarty_tpl->tpl_vars['LABEL_TC0']->value;?>
</h2>
               <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC15']->value;?>
</p>
               <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC16']->value;?>
</p>
               <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC17']->value;?>
</p>
               <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC18']->value;?>
</p>
               <?php } elseif ($_smarty_tpl->tpl_vars['type']->value==2) {?>
               <h2 style="text-align: center"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC14']->value;?>
</h2>
               <p style="text-align: center"><img src="/images/laciutat/ifestes.jpg" class="img-responsive"></p>
               <?php } elseif ($_smarty_tpl->tpl_vars['type']->value==3) {?>
                  <h2><?php echo $_smarty_tpl->tpl_vars['LABEL_TC9']->value;?>
</h2>
                  <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC1']->value;?>
 <a href="mailto:festes@tortosa.cat">festes@tortosa.cat</a> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
 <strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC3']->value;?>
</strong></p>
                  <p><strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC4']->value;?>
</strong>: <?php echo $_smarty_tpl->tpl_vars['LABEL_TC6']->value;?>
</p>
                  <p><strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC5']->value;?>
</strong>: <?php echo $_smarty_tpl->tpl_vars['LABEL_TC7']->value;?>
</p>
                  <div class="separator"></div>
                  <p><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/festes/Fitxapubilla.pdf"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC8']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['LABEL_TC4']->value;?>
</a></p>
                  <p><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/festes/Fitxapubilleta.pdf"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC8']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['LABEL_TC5']->value;?>
</a></p>
               <?php } elseif ($_smarty_tpl->tpl_vars['type']->value==5) {?>
               <h2><?php echo $_smarty_tpl->tpl_vars['LABEL_TC10']->value;?>
</h2>
               <p><?php echo $_smarty_tpl->tpl_vars['LABEL_TC11']->value;?>
 <a href="mailto:festes@tortosa.cat">festes@tortosa.cat</a> <?php echo $_smarty_tpl->tpl_vars['LABEL_TC2']->value;?>
 <strong><?php echo $_smarty_tpl->tpl_vars['LABEL_TC3']->value;?>
</strong></p>
               <p><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/festes/FormulariOfrena.pdf"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC8']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['LABEL_TC12']->value;?>
</a></p>
               <p><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/festes/FormulariActivitats.pdf"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC8']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['LABEL_TC13']->value;?>
</a></p>
               <?php } elseif ($_smarty_tpl->tpl_vars['type']->value==9) {?>
                  <h2><?php echo $_smarty_tpl->tpl_vars['LABEL_TC00']->value;?>
</h2>
                  <?php  $_smarty_tpl->tpl_vars['itemPortada'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['itemPortada']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['PORTADES']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['itemPortada']->key => $_smarty_tpl->tpl_vars['itemPortada']->value) {
$_smarty_tpl->tpl_vars['itemPortada']->_loop = true;
?>
                     <a href=<?php echo $_smarty_tpl->tpl_vars['itemPortada']->value['link'];?>
 target="_<?php echo $_smarty_tpl->tpl_vars['itemPortada']->value['target'];?>
" style="text-decoration:none">
                        <div class="list_portades_festes">
                           <div class="portades_item_img">
                              <img src="/images/festes/portades/cinta<?php echo $_smarty_tpl->tpl_vars['itemPortada']->value['name'];?>
.jpg" width="93" height="135">
                           </div>
                           <div class="portades_title_item"><?php echo $_smarty_tpl->tpl_vars['LABEL_TC19']->value;?>
<?php echo $_smarty_tpl->tpl_vars['itemPortada']->value['name'];?>
</div>
                           <div class="portades_subtitle_item">Tortosa,<?php echo $_smarty_tpl->tpl_vars['itemPortada']->value['subname'];?>
</div>
                        </div>
                     </a>
                  <?php } ?>
               <?php } elseif ($_smarty_tpl->tpl_vars['type']->value==10) {?>
                  <h2><?php echo $_smarty_tpl->tpl_vars['LABEL_TC20']->value;?>
</h2>
                  <iframe width="560" height="315" src="https://www.youtube.com/embed/tzn5W5AnB8w" frameborder="0" allowfullscreen></iframe>               
               <?php } elseif ($_smarty_tpl->tpl_vars['type']->value==11) {?>
                  <h2><?php echo $_smarty_tpl->tpl_vars['LABEL_TC21']->value;?>
</h2>
                  <br>
                  <br>
                  <center>
                     <video width="300"  height="30" controls="" autoplay="" name="media"><source src="/festes/pdt.mp3" type="audio/mpeg"></video>                  
                  </center>
               <?php }?>
            </div>

            <div class="separator"></div>

         </div>
      </div>
   </div>
</div><?php }} ?>
