<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Paco
 * Date: 24/01/14
 * Time: 23:49
 * To change this template use File | Settings | File Templates.
 */

namespace webtortosa;

require_once(FOLDER_LIBS."/smarty/Smarty.class.php");

class Smarty_web extends \Smarty {
    public function __construct() {
        parent::__construct();
        $this->setTemplateDir(WEB_TEMPLATES);
        $this->setConfigDir(WEB_CONFIG);
        $this->setCacheDir(WEB_CACHE);
        $this->setCompileDir(WEB_TEMPLATESC);
    }
}