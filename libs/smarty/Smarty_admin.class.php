<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Paco
 * Date: 24/01/14
 * Time: 23:49
 * To change this template use File | Settings | File Templates.
 */

namespace webtortosa;

require_once(FOLDER_LIBS."/smarty/Smarty.class.php");

class Smarty_admin extends \Smarty {
    public function __construct() {
        parent::__construct();
        $this->setTemplateDir(ADMIN_TEMPLATES);
        $this->setConfigDir(ADMIN_CONFIG);
        $this->setCacheDir(ADMIN_CACHE);
        $this->setCompileDir(ADMIN_TEMPLATESC);
    }
}