<?php
    namespace webtortosa;

    require_once("../../config.cfg");
    
    require_once (FOLDER_MODEL."/db.class.php");    
    require_once (FOLDER_MODEL."/pressupostos-participatius.class.php");

    $ppdb = new \webtortosa\pressupostos_participatius();

    $return_code = "-1";

    if(isset($_POST['proposta1']) && !empty($_POST['proposta1'])):
        $existeix = $ppdb->existeixIEsPropostaValida($_POST['proposta1']);
        if ($existeix == false):
            $return_code = "1";
        endif;
    endif;

    if($return_code=="-1" && isset($_POST['proposta2']) && !empty($_POST['proposta2'])):
        $existeix = $ppdb->existeixIEsPropostaValida($_POST['proposta2']);
        if ($existeix == false):
            $return_code = "2";
        endif;
    endif;

    if($return_code=="-1" && isset($_POST['proposta3']) && !empty($_POST['proposta3'])):
        $existeix = $ppdb->existeixIEsPropostaValida($_POST['proposta3']);
        if ($existeix == false):
            $return_code = "3";
        endif;
    endif;

    echo $return_code;
?>