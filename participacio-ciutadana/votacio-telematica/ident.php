<?
namespace webtortosa;

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once("../../config.cfg");
require_once(FOLDER_CONTROLLER . "/web_controller.php");
require_once(FOLDER_CONTROLLER . "/web_controller_votacio_telematica.php");

//se instancia al controlador
$controller = new web_controller_votacio_telematica();
$file_disabled = "disabled.txt";

$fecha_actual = strtotime(date("d-m-Y H:i:00",time()));
//$fecha_actual = strtotime("29-04-2018 01:01:10");
$fecha_inici = strtotime("28-04-2018 00:00:00");
$fecha_fi = strtotime("11-05-2018 23:59:59");

/* --------------------------- Activa - Desactiva la web ---------------------*/
// Procés de desactivar la web.
if(isset($_GET['codi'])&&($_GET['codi']=="34desactiva43")) {
    $controller->desactiva($file_disabled);
    die;
}

// Procés d'activar la web.
if(isset($_GET['codi'])&&($_GET['codi']=="34activa43")) {
    $path_file_disabled = dirname(__FILE__) . "/"  . $file_disabled;
    $controller->activa($path_file_disabled);
    die;
}
/* -------------------------------------------------------------------------*/

if(isset($_GET['codi'])&&($_GET['codi']=="backup43")) {
    $path_file_disabled = dirname(__FILE__) . "/"  . $file_disabled;
    $controller->backup_pressupostos_participatius($file_disabled, $path_file_disabled);
}


// Comprova si la web està desactivada
if(file_exists(dirname(__FILE__) . "/"  . $file_disabled) && !$_POST) {
    $controller->votacio_telematica_manteniment($_GET['lang']);
    die;
}


if ( ($fecha_actual > $fecha_inici) && ($fecha_actual < $fecha_fi)) {
	//if(isset($_GET['test']))
	    //Comentat per seguretat, per a que ningú entri
        //$controller->votacio_telematica_ident_v2($_GET['lang'], $_POST);
	//else
	//    $controller->votacio_telematica_ident($_GET['lang'], $_POST);
} else { 
	    $controller->votacio_telematica_fora_termini($_GET['lang'], $_POST);
}
?>