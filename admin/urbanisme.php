<?
namespace webtortosa;

require_once("../config.cfg");
require_once(FOLDER_CONTROLLER . "/admin_controller.php");


$controller = new admin_controller();


//se instancia al controlador

switch ($_REQUEST['accio']) {
    case 'edit_urbanisme_element':
        $controller->edit_urbanisme_element($_GET, $_POST);
        break;
    case 'delete_urbanisme_element':
        $controller->delete_urbanisme_element($_GET, $_POST);
        break;
    default:
        $controller->list_urbanisme_elements($_GET, $_POST);
        break;
}
?>