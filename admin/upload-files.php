<?php
require_once ("../config.cfg");
require_once (FOLDER_MODEL."/db.class.php");
require_once (FOLDER_MODEL . "/fitxers.class.php");

$accio = $_POST['accio'];

if($accio=="upload") {
    $descripcio = $_POST['descripcio'];
    $carpeta = $_POST['carpeta'];
    $taula_bbdd = $_POST['taula_bbdd'];
    $id_taula_bbdd = $_POST['id_taula_bbdd'];
    $extension = end(explode(".", $_FILES['file']['name']));

    if (0 < $_FILES['file']['error']) {
        echo "0";
    } else {
        $nom_fitxer = substr(md5(uniqid(rand())), 0, 10) . "." . $extension;
        move_uploaded_file($_FILES['file']['tmp_name'], FOLDER_PUBLIC_FILES . '/' . $carpeta . '/' . $nom_fitxer);
        $data_file = new \webtortosa\fitxers();
        $params = array(
            'TAULA' => $taula_bbdd,
            'ID_TAULA' => $id_taula_bbdd,
            'DESCRIPCIO' => $descripcio,
            'NOM_FITXER' => $nom_fitxer,
            'TIPUS_FITXER' => $extension,
            'TAMANY' => filesize(FOLDER_PUBLIC_FILES . '/' . $carpeta . '/' . $nom_fitxer),
            'BAIXA' => 0
        );
        $data_file->addFitxer($params);
        echo $nom_fitxer;
    }
}

if($accio=="disable") {
    $nom_fitxer = $_POST['file'];
    $taula_bbdd = $_POST['taula_bbdd'];
    $data_file = new \webtortosa\fitxers();
    $params = array(
        'TAULA' => $taula_bbdd,
        'NOM_FITXER' => $nom_fitxer
    );
    $data_file->disabledFitxer($params);
    echo "1";
}

if($accio=="delete") {
    $nom_fitxer = $_POST['file'];
    $taula_bbdd = $_POST['taula_bbdd'];
    $carpeta    = $_POST['carpeta'];
    $data_file = new \webtortosa\fitxers();
    $params = array(
        'TAULA' => $taula_bbdd,
        'NOM_FITXER' => $nom_fitxer
    );
    $data_file->deleteFitxer($params);
    unlink(FOLDER_PUBLIC_FILES . '/' . $carpeta . '/' . $nom_fitxer);
    echo "1";
}
?>