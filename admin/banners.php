<?
namespace webtortosa;

require_once("../config.cfg");
require_once(FOLDER_CONTROLLER . "/admin_controller.php");


$controller = new admin_controller();


//se instancia al controlador

switch ($_REQUEST['accio']) {
    case 'edit_banners':
        $controller->edit_banner($_GET, $_POST, $_FILES);
        break;
    case 'delete_banners':
        $controller->delete_banner($_GET, $_POST);
        break;
    default:
        $controller->list_banner($_GET, $_POST);
        break;
}
?>