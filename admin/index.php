<?PHP
namespace webtortosa;

require_once("../config.cfg");
require_once(FOLDER_CONTROLLER . "/admin_controller.php");

$_SESSION['tiempo'] = time();
$controller = new admin_controller(false);

$user       =  $_POST['user']!="" ? $_POST['user'] : NULL;
$password   =  $_POST['password']!="" ? $_POST['password'] : NULL;

$controller->login($user, $password);
?>