<?
namespace webtortosa;

require_once("../config.cfg");
require_once(FOLDER_CONTROLLER . "/admin_controller.php");


$controller = new admin_controller();


//se instancia al controlador

switch ($_REQUEST['accio']) {
    case 'edit_headlines':
        $controller->edit_headline($_GET, $_POST, $_FILES);
        break;
    case 'delete_headlines':
        $controller->delete_headline($_GET, $_POST);
        break;
    default:
        $controller->list_headline($_GET, $_POST);
        break;
}
?>