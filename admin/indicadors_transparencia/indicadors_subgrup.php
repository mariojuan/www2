<?
namespace webtortosa;

require_once("../../config.cfg");
require_once(FOLDER_CONTROLLER . "/admin_controller.php");


$controller = new admin_controller();


//se instancia al controlador

switch ($_REQUEST['accio']) {
    case 'edit_indicador_subgrup':
        $controller->edit_indicador_subgrup($_GET, $_POST);
        break;
    case 'delete_indicador_subgrup':
        $controller->delete_indicador_subgrup($_GET, $_POST);
        break;
    default:
        $controller->list_indicador_subgrup($_GET, $_POST);
        break;
}
?>