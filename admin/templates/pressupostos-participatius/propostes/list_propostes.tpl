<section id="main" class="column">

<h4 class="alert_info"><i class="icon-pencil"></i>Pressupostos participatius: Llistat d'actuacions (Execució)</h4>
<article class="module width_full">
    <div class="module_content">
        <table class="list_table2" name="list_propostes" id="list_propostes">
            <tr>
                <th align="left">Núm. proposta</th>
                <th align="left">Descripció</th>
                <th></th>
            </tr>
            <tr><td colspan="4"></td></tr>
            {foreach $items as $item}
                <tr>
                    <td>{$item->ID_PROPOSTA}</td>
                    <td>{utf8_encode($item->DESC_CURTA)}</td>
                    <td class="link_item" align="right"><a href="propostes.php?accio=edit_proposta&id={$item->ID_PROPOSTA}&page={$page}">Modificar</a></td>
                </tr>
            {/foreach}
        </table>
    </div>
    <div id="pagination">
        <ul>
            {if $lastpage>1}
                {if $page> 1}
                    {assign var="page_ant" value=$page - 1}
                    <li><a href="propostes.php?page={$page_ant}">Ant</a></li>
                {/if}
                {assign var="desp" value=$page + 4}
                {assign var="firstpage" value=$page - 4}

                {if $desp < $lastpage}
                    {assign var="lastpage" value=$page + 4}
                {/if}
                {if $firstpage < 1}
                    {assign var="firstpage" value=1}
                {/if}

                {for $counter=$firstpage to $lastpage}
                    {if $counter==$page}
                        <li>{$counter}</li>
                    {else}
                        <li><a href="propostes.php?page={$counter}">{$counter}</a></li>
                    {/if}
                {/for}
                {if $page < $lastpage - 1}
                    {assign var="page_seg" value=$page + 1}
                    <li><a href="propostes.php?page={$page_seg}">Seg</a></li>
                {/if}
            {/if}
        </ul>
    </div>
</article>

<!-- end of styles article -->


<div class="spacer"></div>
</section>

<script>
    /*
    $.validator.setDefaults({
        submitHandler: function() {
            alert("submitted!");
        }
    });
    */

    $().ready(function() {

    });

    function delete_item(id, page) {
        if(confirm("Vols esborrar aquest element?"))
            window.location.href="actuacions.php?accio=delete_actuacio&id="+id+"&page="+page;
    }
</script>