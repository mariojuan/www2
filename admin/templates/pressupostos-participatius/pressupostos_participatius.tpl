<script type="text/javascript">
    $(document).on('click', ".seleccionar", function () {
        var cont = 0;

        $("input[name='cb_pp[]']").each( function () {
            if($(this).is(":checked"))
                cont++;
        });
        if(cont>1 || cont==0) {
            $( "#tdocument-error-votar" ).html( "<p>Té que seleccionar una persona</p>" );
            $( "#tdocument-error-votar" ).css('display', 'block');
        }
        else {
            var parametros = {
                "action"                    : "select",
                "id"                        : $("input[name='cb_pp[]']:checked").val()

            };
            $.ajax({
                data:  parametros,
                url:   'pressupostos_participatius.php',
                type:  'post',
                beforeSend: function () {
                    $("#result").html("Processant consulta, esperi si us plau...");
                },
                success:  function (response) {
                    $("#option1-error").html("");
                    $("#option2-error").html("");
                    $("#option3-error").html("");
                    $("#option4-error").html("");
                    if(response=="error") {
                        /*
                         $("#result").html('');
                         $("#result").css('display', 'none');
                         $("#no-result").css('display', 'inline-block');
                         */
                    }
                    else {
                        $("#result").css('color','#ED1F33');
                        $("#result").css('font-weight','bold');
                        $("#result").html(response);
                    }
                }
            });
        }

    });
    $(document).on('click', ".novotar", function () {
        if(validate()) {
            consulta();
        }
        return false;
    });
    $(document).on('click', ".votar", function () {
        var parametros = {
            "action"                    : "registry",
            "id"                        : $("#id").val()

        };
        $.ajax({
            data:  parametros,
            url:   'pressupostos_participatius.php',
            type:  'post',
            beforeSend: function () {
                $("#result").html("Processant consulta, esperi si us plau...");
            },
            success:  function (response) {
                $("#option1-error").html("");
                $("#option2-error").html("");
                $("#option3-error").html("");
                $("#option4-error").html("");
                if(response=="error") {
                    /*
                     $("#result").html('');
                     $("#result").css('display', 'none');
                     $("#no-result").css('display', 'inline-block');
                     */
                }
                else {
                    $("#result").css('color','#ED1F33');
                    $("#result").css('font-weight','bold');
                    $("#result").css('font-size','20px');
                    $('#presspartform').css('display', 'none');
                    $("#result").html(response);
                }
            }
        });

    });
    $(document).ready(function() {
        $('#tdocument').change(function() {
            erase_fields();
            switch($( "#tdocument" ).val()){
                case "1":
                    $('.option0').css('display', 'none');
                    $( "#tdocument-error" ).css('display', 'none');
                    $('.option1').css('display', 'block');
                    $('.option2').css('display', 'none');
                    $('.option3').css('display', 'none');
                    break;
                case "2":
                    $( "#tdocument-error" ).css('display', 'none');
                    $('.option0').css('display', 'none');
                    $('.option1').css('display', 'none');
                    $('.option2').css('display', 'block');
                    $('.option3').css('display', 'none');
                    break;
                case "3":
                    $( "#tdocument-error" ).css('display', 'none');
                    $('.option0').css('display', 'none');
                    $('.option1').css('display', 'none');
                    $('.option2').css('display', 'none');
                    $('.option3').css('display', 'block');
                    break;
                case "4":
                    $( "#tdocument-error" ).css('display', 'none');
                    $('.option0').css('display', 'block');
                    $('.option1').css('display', 'none');
                    $('.option2').css('display', 'none');
                    $('.option3').css('display', 'none');
                    break;
                default:
                    $('.option0').css('display', 'none');
                    $('.option1').css('display', 'none');
                    $('.option2').css('display', 'none');
                    $('.option3').css('display', 'none');
                    break;
            }
        });

        $('.submit').click(function() {
            if(validate()) {
                consulta();
            }
            return false;
        });

        $('.reset').click(function() {
            $( "#tdocument-error" ).css('display', 'none');
            $('.option0').css('display', 'none');
            $('.option1').css('display', 'none');
            $('.option2').css('display', 'none');
            $('.option3').css('display', 'none');
            $('#result').empty();
        });
    });
    function consulta() {
        var parametros = {
            "TDOCUMENT"                 : $("#tdocument").val(),
            "action"                    : "consult",
            "nom"                       : $("#nom").val(),
            "cognoms1"                  : $("#cognoms1").val(),
            "cognoms2"                  : $("#cognoms2").val(),
            "dni_number"                : $("#dni_number").val(),
            "dni_letter"                : $("#dni_letter").val(),
            "option2_passport_number"   : $("#option2_passport_number").val(),
            "tr_letter_1"               : $("#tr_letter_1").val(),
            "tr_number"                 : $("#tr_number").val(),
            "tr_letter_2"               : $("#tr_letter_2").val()
        };
        $.ajax({
            data:  parametros,
            url:   'pressupostos_participatius.php',
            type:  'post',
            beforeSend: function () {
                $("#result").html("Processant consulta, esperi si us plau...");
            },
            success:  function (response) {
                $("#option1-error").html("");
                $("#option2-error").html("");
                $("#option3-error").html("");
                $("#option4-error").html("");
                if(response=="error") {
                    /*
                     $("#result").html('');
                     $("#result").css('display', 'none');
                     $("#no-result").css('display', 'inline-block');
                     */
                }
                else {
                    $("#result").css('color','#ED1F33');
                    $("#result").css('font-weight','bold');
                    $("#result").css('font-size','12px');
                    $("#result").html(response);
                }
            }
        });
    }

    function validate() {
        if($( "#tdocument" ).val()=="") {
            $( "#tdocument-error" ).html( "<p>El tipus de document &eacute;s obligatori</p>" );
            $( "#tdocument-error" ).css('display', 'block');
            return false;
        }
        else {
            $("#tdocument-error").css('display', 'none');
            switch ($("#tdocument").val()) {
                case "1":
                    if ($('#dni_number').val() == "") {
                        $("#option1-error").html("<p>El n&uacute;mero del DNI / NIF &eacute;s obligatori</p>");
                        $("#option1-error").css('display', 'block');
                        return false;
                    }

                    if ($('#dni_letter').val() == "") {
                        $("#option1-error").html("<p>La lletra del DNI / NIF &eacute;s obligatori</p>");
                        $("#option1-error").css('display', 'block');
                        return false;
                    }
                    if (!$.isNumeric($('#dni_number').val())) {
                        $("#option1-error").html("<p>El n&uacute;mero del DNI / NIF ha de ser un num&egrave;ric</p>");
                        $("#option1-err").css('display', 'block');
                        return false;
                    }

                    if (($('#dni_number').val().length < 4) || ($('#dni_number').val().length > 8)) {
                        $("#option1-error").html("<p>El n&uacute;mero del DNI / NIF incorrecte</p>");
                        $("#option1-error").css('display', 'block');
                        return false;
                    } else {
                        while ($('#dni_number').val().charAt(0) == '0') {
                            $('#dni_number').val($('#dni_number').val().substring(1, $('#dni_number').val().length));
                        }
                    }

                    if (!$('#dni_letter').val().match(/^[a-zA-Z]+$/)) {
                        $("#option1-error").html("<p>La lletra del DNI / NIF &eacute;s incorrecta</p>");
                        $("#option1-error").css('display', 'block');
                        return false;
                    }

                    break;

                case "2":
                    if ($('#option2_passport_number').val() == "") {
                        $("#option2-error").html("<p>El n&uacute;mero del Passaport &eacute;s obligatori</p>");
                        $("#option2-error").css('display', 'block');
                        return false;
                    }

                    if (!$('#option2_passport_number').val().match(/^[a-zA-Z0-9]+$/)) {
                        $("#option2-error").html("<p>El n&uacute;mero del Passaport &eacute;s incorrecte</p>");
                        $("#option2-error").css('display', 'block');
                        return false;
                    }

                    break;
                case "3":
                    if ($('#tr_letter_1').val() == "") {
                        $("#option3-error").html("<p>La primera lletra de la tarjeta de resid&egrave;ncia &eacute;s obligatori</p>");
                        $("#option3-error").css('display', 'block');
                        return false;
                    }
                    if ((!$('#tr_letter_1').val().match(/^[a-zA-Z]+$/))||(($('#tr_letter_1').val()!="x") && ($('#tr_letter_1').val()!="X") && ($('#tr_letter_1').val()!="y") && ($('#tr_letter_1').val()!="Y"))) {
                        $("#option3-error").html("<p>La primera lletra de la tarjeta de resid&egrave;ncia &eacute;s incorrecta</p>");
                        $("#option3-error").css('display', 'block');
                        return false;
                    }
                    if (!$.isNumeric($('#tr_number').val())) {
                        $("#option3-error").html("<p>El n&uacute;mero de la tarjeta de resid&egrave;ncia ha de ser un num&egrave;ric</p>");
                        $("#option3-error").css('display', 'block');
                        return false;
                    }
                    if ($('#tr_number').val().length > 8) {
                        $("#option3-error").html("<p>El n&uacute;mero de la tarjeta de resid&egrave;ncia &eacute;s incorrecta</p>");
                        $("#option3-error").css('display', 'block');
                        return false;
                    }

                    if ($('#tr_letter_2').val() == "") {
                        $("#option3-error").html("<p>La segona lletra de la tarjeta de resid&egrave;ncia &eacute;s obligatori</p>");
                        $("#option3-error").css('display', 'block');
                        return false;
                    }

                    break;

                case "4":
                    if ($('#nom').val() == "") {
                        $("#option4-error").html("<p>El nom &eacute;s obligatori</p>");
                        $("#option4-error").css('display', 'block');
                        return false;
                    }
                    if ($('#cognoms1').val() == "") {
                        $("#option4-error").html("<p>El cognom 1 &eacute;s obligatori</p>");
                        $("#option4-error").css('display', 'block');
                        return false;
                    }
                    break;
            }
        }
        return true;
    }

    function erase_fields() {
        $('#nom').val("");
        $('#cognoms1').val("");
        $('#cognoms2').val("");
        $('#dni_number').val("");
        $('#dni_letter').val("");
        $('#option2_passport_number').val("");
        $('#tr_letter_1').val("");
        $('#tr_number').val("");
        $('#tr_letter_2').val("");
    }
</script>
<section id="main" class="column pressupostos-participatius">
    <h4 class="alert_info"><i class="icon-pencil"></i>Pressupostos Participatius</h4>
    <article class="module width_full">
        <div class="module_content">
            <form id="presspartform" method="post" action="pressupostos_participatius.php" novalidate="novalidate" enctype="multipart/form-data">
                <label>Tipus de document</label>
                <select id="tdocument" name="tdocument">
                    <option value="">Tipus de document</option>
                    <option value="1">DNI / NIF</option>
                    <option value="2">Passaport</option>
                    <option value="3">NIE</option>
                    <option value="4">Nom i cognoms</option>
                </select>
                <label id="tdocument-error" class="error" for="tdocument-error" style="display: none;"></label>
                <p class="option0">
                    <label>Nom</label>
                    <input type="text" name="nom" id="nom" value="">
                </p>
                <p class="option0">
                    <label>Cognom 1</label>
                    <input type="text" name="cognoms1" id="cognoms1" value="">
                </p>
                <p class="option0">
                    <label>Cognom 2</label>
                    <input type="text" name="cognoms2" id="cognoms2" value="">
                    <label id="option4-error" class="error" for="option4-error" style="display: none;"></label>
                </p>
                <p class="option1">
                    <label>NIF</label>
                    <span>
                        Número<br/>
                        <input type="text" id="dni_number" name="dni_number" value="" maxlength="8">
                    </span>
                    <span>
                        Lletra<br/>
                        <input type="text" id="dni_letter" name="dni_letter" value="" maxlength="1">
                    </span>
                    <label id="option1-error" class="error" for="option1-error" style="display: none;"></label>
                </p>
                <p class="option2">
                    <span>
                    Passaport
                    <input type="text" id="option2_passport_number" name="option2_passport_number" value="{$txt_passport_number}">
                </span>
                    <label id="option2-error" class="error" for="option2-error" style="display: none;"></label>
                </p>
                <p class="option3">
                    <label>Tarjeta de residència</label>
                    <span>
                        Lletra<br>
                        <input type="text" id="tr_letter_1" name="tr_letter_1" value="" maxlength="1">
                    </span>
                    <span>
                        Número<br>
                        <input type="text" id="tr_number" name="tr_number" value="" maxlength="8">
                    </span>
                    <span>
                        Lletra<br>
                        <input type="text" id="tr_letter_2" name="tr_letter_2" value="" maxlength="1">
                    </span>
                    <label id="option3-error" class="error" for="option3-error" style="display: none;"></label>
                </p>
                <p>
                    <input class="submit" type="submit" value="Consultar">
                    <input class="reset" type="reset" name="esborrar" value="Esborrar">
                </p>
            </form>
            <p>
                <div id="result">

                </div>
                <div id="no-result">
                    <p>{$txt_no_result_1}</p>
                </div>
            </p>
        </div>
    </article>
</section>