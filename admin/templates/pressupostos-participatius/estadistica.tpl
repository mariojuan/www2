<script type="text/javascript">
    $(document).ready(function() {
        $('.submit').click(function() {
            if($( "#lloc_votacio" ).val()=="") {
                $( "#tdocument-error" ).html( "<p>El lloc de votació &eacute;s obligatori</p>" );
                $( "#tdocument-error" ).css('display', 'block');
                return false;
            }
            return true;
        });
    });
</script>
<section id="main" class="column pressupostos-participatius">
    <h4 class="alert_info"><i class="icon-pencil"></i>Estadística Pressupostos Participatius</h4>
    <article class="module width_full">
        <div class="module_content">
            <form id="presspartform" method="post" action="estadistica.php" novalidate="novalidate" enctype="multipart/form-data">
                <label>Lloc votació</label>
                <select id="lloc_votacio" name="lloc_votacio">
                    <option value="">-- Seleccionar --</option>
                    <option value="1">Ajuntament de Tortosa</option>
                    <option value="2">Fira ExpoEbre</option>
                    <option value="3">Els Reguers</option>
                    <option value="4">Vinallop</option>
                    <option value="5">Bítem</option>
                    <option value="6">Campredó</option>
                    <option value="7">Jesús</option>
                    <option value="8">::: Tots :::</option>
                </select>
                <label id="tdocument-error" class="error" for="tdocument-error" style="display: none;"></label>
                <p>
                    <input class="submit" type="submit" value="Consultar">
                    <input class="reset" type="reset" name="esborrar" value="Esborrar">
                </p>
            </form>
            {if $action=="list"}
            <div class="estadistica-container">
                Número vots {$lloc}: {$numero_vots}
            </div>
            {/if}
            {if $action=="list_totals"}
                <div class="estadistica-container">
                    <table>
                        <tr>
                            <th>Seu</td>
                            <th>Vots</td>
                            <th>%</td>
                        </tr>
                    {foreach $vots as $item}
                        {assign var="totals" value=$totals + $item->total_vots}
                        {assign var="percent" value=$item->total_vots / $total_vots * 100}
                        <tr>
                            <td>{$item->LLOC_VOTACIO}</td>
                            <td>{$item->total_vots|number_format:0:'.':'.'}</td>
                            <td>{$percent|string_format:"%.2f"} %</td>
                        </tr>
                    {/foreach}
                        <tr>
                            <td colspan="3" style="height: 1px;"></td>
                        </tr>
                        <tr>
                            <td>Totals</td>
                            <td><strong>{$totals|number_format:0:'.':'.'}</strong></td>
                            {assign var="percent" value=$totals / $total_vots * 100}
                            <td><strong>{$percent|string_format:"%.2f"} %</strong></td>
                        </tr>
                    </table>
                </div>
            {/if}

        </div>
    </article>
</section>