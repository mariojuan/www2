<header id="header">
    <hgroup>
        <h1 class="site_title"><a href="principal.php">Gestor Ajuntament de Tortosa</a></h1>
        <h2 class="section_title"></h2><div class="btn_view_site"><a href="http://www2.tortosa.cat">Visitar web</a></div>
    </hgroup>
</header> <!-- end of header bar -->

<section id="secondary_bar">
    <div class="user">
        <p><i class="icon-user"></i> {$info_empresa['contacto']} {$info_usuari['contacte']}</p>
        <!-- <a class="logout_user" href="#" title="Logout">Logout</a> -->
    </div>
    <div class="breadcrumbs_container">
        <article class="breadcrumbs"><a href="/admin/principal.php">Gestor</a> <div class="breadcrumb_divider"></div> <a class="current">{$location}</a></article>
    </div>
</section><!-- end of secondary bar -->