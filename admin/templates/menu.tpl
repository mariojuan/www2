<aside id="sidebar" class="column">
    <!--
    <form class="quick_search">
        <input type="text" value="Quick Search">
    </form>
    <hr/>
    -->
    <!--
    1.- Superadministrador
    2.- Unitat de Comunicació i Màrketing (CMK)
    3.- Grups Polítics (GP)
    -->

    {if ($info_usuari['rol']=='Superadministrador')}
        <h3>Ofertes d'Ocupació</h3>
        <ul class="toggle">
            <li class="icn_new_article"><i class="icon-doc-new"></i>
                <a href="/admin/ofertes_ocupacio/oferta.php?accio=edit_oferta">
                    Nova oferta
                </a>
            </li>
            <li class="icn_edit_article"><i class="icon-pencil"></i>
                <a href="/admin/ofertes_ocupacio/oferta.php">Gestió ofertes</a>
            </li>
        </ul>
    {/if}

    {if ($info_usuari['rol']=='Superadministrador')}
        <h3>Indicadors Transparència</h3>
        <ul class="toggle">
            <li class="icn_new_article"><i class="icon-doc-new"></i>
                <a href="/admin/indicadors_transparencia/indicadors_grup.php?accio=edit_indicador_grup">
                    Nou grup
                </a>
            </li>
            <li class="icn_edit_article"><i class="icon-pencil"></i>
                <a href="/admin/indicadors_transparencia/indicadors_grup.php">Gestió grups</a>
            </li>
            <li class="icn_new_article"><i class="icon-doc-new"></i>
                <a href="/admin/indicadors_transparencia/indicadors_subgrup.php?accio=edit_indicador_subgrup">
                    Nou subgrup
                </a>
            </li>
            <li class="icn_edit_article"><i class="icon-pencil"></i>
                <a href="/admin/indicadors_transparencia/indicadors_subgrup.php">Gestió subgrups</a>
            </li>
            <li class="icn_new_article"><i class="icon-doc-new"></i>
                <a href="/admin/indicadors_transparencia/indicadors.php?accio=edit_indicador">
                    Nou indicador
                </a>
            </li>
            <li class="icn_edit_article"><i class="icon-pencil"></i>
                <a href="/admin/indicadors_transparencia/indicadors.php">Gestió indicadors</a>
            </li>
        </ul>
    {/if}
    
    {if ($info_usuari['rol']=='Superadministrador') || ($info_usuari['unitat']=="CMK") || ($info_usuari['unitat']=="GP")}
    <h3>Notícies</h3>
    <ul class="toggle">
        <li class="icn_new_article"><i class="icon-doc-new"></i><a href="/admin/headlines.php?accio=edit_headlines">Nova notícia</a></li>
        <li class="icn_edit_article"><i class="icon-pencil"></i><a href="/admin/headlines.php">Gestió notícies</a></li>
    </ul>
    {/if}

    {if ($info_usuari['rol']=='Superadministrador')}
    <h3>Banners</h3>
    <ul class="toggle">
        <li class="icn_new_article"><i class="icon-doc-new"></i>
            <a href="/admin/banners.php?accio=edit_banners">
                Nou banner
            </a>
        </li>
        <li class="icn_edit_article"><i class="icon-pencil"></i>
            <a href="/admin/banners.php">Gestió banners</a>
        </li>
    </ul>
    {/if}

    {if ($info_usuari['rol']=='Superadministrador')}
    <h3>Telèfons</h3>
    <ul class="toggle">
        <li class="icn_new_article"><i class="icon-doc-new"></i>
            <a href="/admin/telefons.php?accio=edit_telefon">
                Nou telèfon
            </a>
        </li>
        <li class="icn_edit_article"><i class="icon-pencil"></i>
            <a href="/admin/telefons.php">Gestió telèfons</a>
        </li>
    </ul>
    {/if}

    {if ($info_usuari['rol']=='Superadministrador') || ($info_usuari['unitat']=='INF')}
    <h3>Eleccions</h3>
    <ul class="toggle">
        <li class="icn_edit_article"><i class="icon-pencil"></i><a href="/admin/edit_participacio.php">Edició mesa</a>
        </li>        
        <li class="icn_edit_article"><i class="icon-pencil"></i><a href="/admin/edit_resultat.php">Edició participació i resultats</a>
        </li>        
        <li class="icn_edit_article"><i class="icon-pencil"></i>&nbsp;&nbsp;&nbsp;Històric
            <ul>
                <li class="icn_edit_article"><i class="icon-pencil"></i><a href="/admin/edit_participacio_historic.php">Edició mesa</a>
                </li>        
                <li class="icn_edit_article"><i class="icon-pencil"></i><a href="/admin/edit_resultat_historic.php">Edició participació i resultats</a>
                </li>        
            </ul>
        </li>        
    </ul>
    {/if}
    {if ($info_usuari['rol']=='Superadministrador') || ($info_usuari['unitat']=="URB")}
        <h3>Urbanisme</h3>
        <ul class="toggle">
            <li class="icn_new_article"><i class="icon-doc-new"></i><a href="/admin/urbanisme.php?accio=edit_urbanisme_element">Nou Element</a></li>
            <li class="icn_edit_article"><i class="icon-pencil"></i><a href="/admin/urbanisme.php">Gestió elements</a></li>
        </ul>
    {/if}
    {if ($info_usuari['rol']=='Superadministrador') || ($info_usuari['unitat']=="PP")}
        <h3>Servei d'Atenció Ciutadana</h3>
        <ul class="toggle">
            <li class="icn_edit_article"><i class="icon-pencil"></i>&nbsp;&nbsp;&nbsp;Pressupostos Participatius
                <ul>
                    <li class="icn_edit_article"><i class="icon-pencil"></i><a href="/admin/pressupostos-participatius/pressupostos_participatius.php">Votació</a>
                    </li>
                    <li class="icn_edit_article"><i class="icon-pencil"></i><a href="/admin/pressupostos-participatius/estadistica.php">Estadística</a>
                    </li>
                </ul>
            </li>
        </ul>
    {/if}

    {if ($info_usuari['rol']=='Superadministrador') || ($info_usuari['unitat']=='INF')}
    <!--
        <h3>Pressupostos participatius</h3>
    <ul class="toggle">
        <li class="icn_edit_article"><i class="icon-pencil"></i><a href="/admin/pressupostos-participatius/propostes/propostes.php?accio=edit_proposta">Nova actuació</a>
        </li>        
        <li class="icn_edit_article"><i class="icon-pencil"></i><a href="/admin/pressupostos-participatius/propostes/propostes.php">Gestió d'actuacions</a>
        </li>                
    </ul>
    -->
    {/if}

    {if $info_usuari['rol']=='Superadministrador'}
    <!--
    <h3>Usuaris</h3>
    <ul class="toggle">
        <li class="icn_add_user"><a href="edit_user.php">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nou usuari</a></li>
        <li class="icn_view_users"><a href="list_user.php">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Gestió usuaris</a></li>
    </ul>
    -->
    {/if}
    <h3>Altres</h3>
    <ul class="toggle">
        <li class="icn_jump_back"><i class="icon-logout"></i><a href="/admin/logout.php">Sortir</a></li>
    </ul>

    <footer>
        <hr />
        <p><strong>Copyright &copy; 2018 Website Ajuntament de Tortosa</strong></p>
    </footer>
</aside><!-- end of sidebar -->