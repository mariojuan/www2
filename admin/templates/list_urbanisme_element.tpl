<section id="main" class="column">

<h4 class="alert_info"><i class="icon-pencil"></i>Llistat elements urbanisme <span id="search_content"><a href="JavaScript:search_content();"><i class="icon-search"></i></a></span> </h4>
<article class="module width_full">
    <div class="module_content">
        <table class="list_table" name="list_urbanisme_element" id="list_urbanisme_element">
            <tr>
                <th>Títol</th>
                <th>Data</th>
                <th></th>
                <th></th>
            </tr>
            <tr><td colspan="4"></td></tr>
            {foreach $items as $item}
                <tr>
                    <td>{$item->TITOL}</td>
                    <td>{$item->DATA|date_format:"%d/%m/%G"}</td>
                    <td class="link_item"><a href="urbanisme.php?accio=edit_urbanisme_element&id={$item->ID}&page={$page}">Modificar</a></td>
                    <td class="link_item"><a href="JavaScript: delete_item('{$item->ID}', '{$page}');">Esborrar</a></td>
                </tr>
            {/foreach}
        </table>
        <div id="pagination">
            <ul>
                {if $lastpage>1}
                    {if $page> 1}
                        {assign var="page_ant" value=$page - 1}
                        <li><a href="urbanisme.php?accio=list&page={$page_ant}">Ant</a></li>
                    {/if}
                    {assign var="desp" value=$page + 4}
                    {assign var="firstpage" value=$page - 4}

                    {if $desp < $lastpage}
                        {assign var="lastpage" value=$page + 4}
                    {/if}
                    {if $firstpage < 1}
                        {assign var="firstpage" value=1}
                    {/if}

                    {for $counter=$firstpage to $lastpage}
                        {if $counter==$page}
                            <li>{$counter}</li>
                        {else}
                            <li><a href="urbanisme.php?accio=list&page={$counter}">{$counter}</a></li>
                        {/if}
                    {/for}
                    {if $page < $lastpage - 1}
                        {assign var="page_seg" value=$page + 1}
                        <li><a href="urbanisme.php?accio=list&page={$page_seg}">Seg</a></li>
                    {/if}
                {/if}
            </ul>
        </div>
    </div>
</article>

<div id="search-content">
    <h4>Cerca</h4>
    <form name="search" id="search" method="post" action="urbanisme.php">
        <p>
            <label>Títol</label>
            <input type="text" name="titol" id="titol">
        </p>
        <p>
            <input type="submit" id="cercar" name="cercar" value="Cercar">
            <input type="submit" id="tancar" name="tancar" value="Tancar">
        </p>
    </form>
</div>
<!-- end of styles article -->


<div class="spacer"></div>
</section>

<script>
    /*
    $.validator.setDefaults({
        submitHandler: function() {
            alert("submitted!");
        }
    });
    */

    $().ready(function() {
        $("#tancar").click(function() {
            $('div#search-content').css('display', 'none');
        });
    });

    function delete_item(id, page) {
        if(confirm("Vols esborrar aquest element?"))
            window.location.href="urbanisme.php?accio=delete_urbanisme_element&id="+id+"&page="+page;
    }

    function search_content() {
        $('div#search-content').css('display', 'block');
    }
</script>