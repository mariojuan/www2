{function name=links num_link=0 link_items=null}
    <p class="links" {if $link_items[$num_link]->TITOL==""}style="display:none"{/if} id="link{$num_link}">
        <label>Enllaç</label>
        <input type="text" name="TEXT_LINK_{$num_link}" id="TEXT_LINK_{$num_link}" value="{if $link_items[$num_link]->TITOL!=""}{$link_items[$num_link]->TITOL}{else}títol{/if}">
        <input type="text" name="LINK_{$num_link}" class="link" id="LINK_{$num_link}" value="{if $link_items[$num_link]->LINK!=""}{$link_items[$num_link]->LINK}{else}link{/if}">
        <select name="TIPUS_{$num_link}" id="TIPUS_{$num_link}">
            <option value="">-- seleccionar --</option>
            <option value="diari oficial" {if $link_items[$num_link]->TIPUS=="diari oficial"}selected{/if}>Diari Oficial</option>
            <option value="altres" {if $link_items[$num_link]->TIPUS=="altres"}selected{/if}>Altres</option>
        </select> <a href="JavaScript:delete_link('{$num_link}');"><i class="icon-trash-empty" title="Esborrar enllaç" alt="Esborrar enllaç"></i></a>
        <input type="hidden" name="link_active_{$num_link}" id="link_active_{$num_link}" value="{if $link_items[$num_link]->TITOL!=""}1{else}0{/if}">
    </p>
{/function}

<section id="main" class="column">

<h4 class="alert_info"><i class="icon-doc-new"></i>Alta element urbanisme</h4>
<article class="module width_full">
    <div class="module_content">
        <form id="urbanismeForm" method="post" action="urbanisme.php" novalidate="novalidate" enctype="multipart/form-data">
            <input type="hidden" name="accio" id="accio" value="edit_urbanisme_element">
            <p>
                <label>Títol *</label>
                <input type="text" name="TITOL" id="TITOL" value="{$item->TITOL}">
            </p>
            <p>
                <label>Enllaç</label>
                <input type="text" name="LINK" id="LINK" value="{$item->LINK}" class="link">
            </p>
            <p>
                <label>Data</label>
                <input type="text" name="DATA" id="DATA" value="{$item->DATA|date_format:"%d/%m/%G"}">
            </p>
            <p>
                <label>Menú superior</label>
                <select name="ID_MENU_NAVEGACIO" id="ID_MENU_NAVEGACIO">
                    <option value="">-- Seleccionar --</option>
                    {foreach from=$items_menu item=fila}
                        {if $item->ID_MENU_NAVEGACIO==$fila->ID}
                            <option value="{$fila->ID}" selected>{$fila->TITOL} - Nivell: {$fila->NIVELL}</option>
                        {else}
                            <option value="{$fila->ID}">{$fila->TITOL} - Nivell: {$fila->NIVELL}</option>
                        {/if}
                    {/foreach}
                </select>
            </p>
            <p>
                <label>Actiu</label>
                <input type="checkbox" name="ACTIVA" id="ACTIVA" {if $item->ACTIVA==1} checked {/if}>
            </p>
            <p>
                <label>Enllaços</label>
            </p>
            <p><a href="JavaScript:new_link();">Nou enllaç</a></p>
            {for $i=0 to 100}
                {links num_link=$i link_items=$link_items}
            {/for}
            <p>
                <input class="submit" type="submit" value="Desar">
            </p>
            <p>
                <label>* Camps obligatoris</label>
            </p>
            <input type="hidden" name="ID" id="ID" value="{$item->ID}">
        </form>
    </div>
</article>



<!-- end of styles article -->

<div class="spacer"></div>
</section>

<script>
    /*
    $.validator.setDefaults({
        submitHandler: function() {
            alert("submitted!");
        }
    });
    */
    var num_elements = {$link_items|@count};
    function new_link() {
        $("#link"+num_elements).css("display","block");
        $("#link_active_"+num_elements).val('1');
        num_elements++;
    }

    function delete_link(num_item) {
        $("#link"+num_item).css("display","none");
        $("#link_active_"+num_item).val('0');
    }

    $().ready(function() {
        $( "#DATA" ).datepicker();

        // validate signup form on keyup and submit
        
        $("#urbanismeForm").validate({
            rules: {
                TITOL:  {
                    required: true,
                    maxlength: 200
                }
            },
            messages: {
                TITOL: {
                    required: "El camp de títol és obligatori",
                    maxlength: "El camp de títol és massa llarg"
                }
            }
        });

        {for $i=0 to 100}
        $("#urbanismeForm input#TEXT_LINK_{$i}").focus(function() {
            if($(this).val()=='títol')
                $(this).val('');
        });
        $("#urbanismeForm input#LINK_{$i}").focus(function() {
            if($(this).val()=='link')
                $(this).val('');
        });
        {/for}
    });
</script>