<section id="main" class="column">
   <h4 class="alert_info"><i class="icon-doc-new"></i>Alta banner</h4>

   <article class="module width_full">
      <div class="module_content">
         <form id="bannersForm" method="post" action="banners.php" novalidate="novalidate" enctype="multipart/form-data">
            <input type="hidden" name="accio" id="accio" value="edit_banners">

            <p>
               <label>Títol *</label>
               <input type="text" name="TITOL" id="TITOL" value="{$item->TITOL}">
            </p>
           
            <p>
               <label>Imatge *</label>
               {if $item->IMATGE|trim!=""}
                   <span class="foto"><img src="/public_files/banners/{$item->IMATGE}"></span>
                   {assign var="imatge" value="{$item->IMATGE}"}
               {/if}
               <input type="file" name="FOTO" id="FOTO">
               <input type="hidden" name="FOTO_AUX" id="FOTO_AUX" value="{$imatge}">
               </br>
               Imatge home: 895x304 px</br>
               Imatge campanyes: 146x246 px</br>
               Imatge pàgina ciutat: 740x312 px
            </p>

            {if $item->FOTO!=""}
            <p>
               <label>Esborrar foto</label>
               <input type="checkbox" name="delete_foto" id="delete_foto">
            </p>
            {/if}

            <p>
               <label>Enllaç</label>
               <input type="text" name="LINK" id="LINK" value="{$item->LINK}">
            </p>

            <p>
               <label>Fitxer</label>
               {if $item->FITXER_LINK|trim!=""}
                    {assign var="link" value="{$item->FITXER_LINK}"}
               {/if}
                <span>{$item->FITXER_LINK}</span>
               <input type="file" name="FITXER_LINK" id="FITXER_LINK">
               <input type="hidden" name="FITXER_LINK_AUX" id="FITXER_LINK_AUX" value="{$link}">
            </p>

            <p>
               <label>Data</label>
               <input type="text" name="DATA" id="DATA" value="{$item->DATA|date_format:"%d/%m/%G"}">
            </p>

            <p>
               <label>Data inici visualització</label>
               <input type="text" name="DATA_INICI" id="DATA_INICI" value="{$item->DATA_INICI|date_format:"%d/%m/%G"}">
            </p>

            <p>
               <label>Data fi visualització</label>
               <input type="text" name="DATA_FI" id="DATA_FI" value="{$item->DATA_FI|date_format:"%d/%m/%G"}">
            </p>

            <p>
               <label>Tipologia *</label>
               <select name="TIPUS" id="TIPUS">
                  <option value="" selected>-- Seleccionar --</option>
                  <option value="1" {if $item->TIPUS=="1"} selected {/if}>Home</option>
                  <option value="2" {if $item->TIPUS=="2"} selected {/if}>Campanya</option>
                  <option value="3" {if $item->TIPUS=="3"} selected {/if}>Ciutat</option>
               </select>
            </p>
            <p>
               <label>Actiu</label>
               <input type="checkbox" name="ACTIU" id="ACTIU" {if $item->ACTIU==1} checked {/if}>
            </p>
            <p>
               <input class="submit" type="submit" value="Submit">
            </p>
            <p>
               <label>* Camps obligatoris</label>
            </p>
            <input type="hidden" name="ADJUNT" id="ADJUNT" value="">
            <input type="hidden" name="FOTO" id="FOTO" value="">
            <input type="hidden" name="ID" id="ID" value="{$item->ID}">
         </form>
      </div>
   </article>

   <!-- end of styles article -->
   <div class="spacer"></div>
</section>

<script>
    $().ready(function() {
        $( "#DATA" ).datepicker();
        $( "#DATA_INICI" ).datepicker();
        $( "#DATA_FI" ).datepicker();


        // validate signup form on keyup and submit
        
        $("#bannersForm").validate({
            rules: {
                TITOL:  {
                    required: true,
                    maxlength: 150
                },
                FOTO: {
                    required: comproveImage()
                },
                DATA: "required",
                DATA_INICI: "required",
                DATA_FI: "required",
                TIPUS: "required"
            },
            messages: {
                TITOL: {
                    required: "El camp de títol és obligatori",
                    maxlength: "El camp de títol és massa llarg"
                },
                FOTO: comproveImageMessages(),
                DATA: "El camp de data és obligatori",
                DATA_INICI: "El camp de data inici visualització és obligatori",
                DATA_FI: "El camp de data fi visualització és obligatori",
                TIPUS: "El camp de tipologia és obligatori"
            }
        });

        function comproveImage() {
            if(($("#FOTO").val()=="") && ($("#FOTO_AUX").val()=="")) {
                return true;
            }
            else {
                return false;
            }
        }

        function comproveImageMessages() {
            if(($("#FOTO").val()=="") && ($("#FOTO_AUX").val()=="")) {
                return "El camp d'imatge és obligatori";
            }
            else {
                return null;
            }
        }
    });
</script>