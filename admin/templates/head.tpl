<html lang="es">

<head>
    <meta charset="utf-8"/>
    <title>Gestor de continguts - Ajuntament de Tortosa</title>
    <!--<meta http-equiv="refresh" content="900; url=http://www2.tortosa.cat/admin" />-->
    <link rel="stylesheet" href="/admin/css/layout.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="/admin/css/jquery-ui.css">

    <link rel="stylesheet" href="/fonts/fontello-fafedd5f/css/search.css">
    <link rel="stylesheet" href="/admin/fontello/css/animation.css">
    <!--[if IE 7]>
    <link rel="stylesheet" href="css/fontello-ie7.css"><![endif]-->
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="/admin/css/ie.css" type="text/css" media="screen" />
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script src="/admin/js/jquery-1.11.1.js" type="text/javascript"></script>
    <script src="/admin/js/jquery-ui.js" type="text/javascript"></script>
    <script src="/admin/js/jquery.validate.js" type="text/javascript"></script>

</head>


<body>