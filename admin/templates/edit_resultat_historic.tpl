<section id="main" class="column">

<h4 class="alert_info_historic"><i class="icon-pencil"></i>Històric participació i resultats</h4>
{if $result_update=="1"}
<div id="div_resultat">
    <h4 id="resultat_modificacio" class="alert_success">Modificacions realitzades</h4>
</div>
{/if}
<article class="module width_full">
    <div class="module_content">
        <form id="resultatForm" method="post" action="edit_resultat.php" novalidate="novalidate" >
            <br>

            <p>
                <label>Comici *</label>
                <select name="RefEleccio" id="RefEleccio">
                    <option value="">-- Seleccionar un comici --</option>
                {foreach from=$items_eleccions item=fila} 
                    <option value="{$fila->IdEleccio}">{$fila->Denominacio}</option>                   
                {/foreach} 

                </select>
            </p>

            <p>
                <label>Col·legi *</label>
                <label id="colegis">
                    <select name="RefColegi" id="RefColegi">
                        <option value="">-- Selecciona un col·legi --</option>
                    </select>
                </label>
            </p>

            <p>
                <label>Mesa *</label>
                <label id="meses">
                    <select name="IdMesa" id="IdMesa">
                        <option value="">-- Selecciona un col·legi --</option>
                    </select>
                </label>
            </p>

            <p>
                &nbsp;
            </p>

            <p>
                <label>Districte</label>
                <input class="medio" type="text" name="Districte" id="Districte" readonly>
                <label class="center">Secció</label>
                <input class="medio" type="text" name="Seccio" id="Seccio" readonly>
                <label class="center">Lletra</label>
                <input class="corto" type="text" name="Lletra" id="Lletra" readonly>
            </p>

            <p>
                <label>Participació 14:00</label>
                <input class="medio" type="text" name="Vots1Participacio" id="Vots1Participacio">
                <label class="center">18:00</label>
                <input class="medio" type="text" name="Vots2Participacio" id="Vots2Participacio">
                <label class="center">20:00</label>
                <input class="medio" type="text" name="Vots3Participacio" id="Vots3Participacio">
            </p>

            <p>
                &nbsp;
            </p>

            <div id="resultats_partits" class="padding-right50">

            </div>

            <p>
                &nbsp;
            </p>

            <center>
                <p>
                    <input class="submit" type="submit" value="Acceptar" id="Acceptar" name="Acceptar">
                </p>

                <p>
                    <label>* Camps obligatoris</label>
                </p>
            </center>

        </form>
    </div>
</article>

<!-- end of styles article -->

<script>

    function resetFields() { 
        $('#Districte').val("");
        $('#Seccio').val("");
        $('#Lletra').val("");
        $('#Vots1Participacio').val("");
        $('#Vots2Participacio').val("");
        $('#Vots3Participacio').val("");
        $('#resultats_partits').html("");
    }

    function carregaColegis() { 
        $.ajax({ 
            url: 'carregaColegis.php',
            data: 'IdEleccio='+$('#RefEleccio option:selected').val(),                
            type: 'get',
            success: function(output) {
                $('#resultat_modificacio').css("display","none");

                $('#RefColegi').html(output);
                $('#IdMesa').html("<option value=\"\">-- Selecciona un col·legi --</option>");

                resetFields();
            }
        });
    }

    $().ready(function() {

        $("#resultatForm").validate({
            rules: {
                IdMesa: "required"
            },
            messages: {
                IdMesa: "Atenció! Heu de seleccionar una mesa per editar-la."
            }
        });


        $('#RefEleccio').change(function() {
           carregaColegis();
        });    
            
        $('#RefColegi').change(function() {
            $.ajax({ 
                url: 'carregaMeses.php',
                data: 'IdEleccio='+$('#RefEleccio option:selected').val() + '&IdColegi='+$('#RefColegi option:selected').val(),
                type: 'get',
                success: function(output) {
                    $('#IdMesa').html(output);

                    resetFields();
                }
            });
        });        
        
        $('#IdMesa').change(function() {
            $.ajax({ 
                url: 'carregaCandidaturesResultatsMesa.php',
                data: 'IdEleccio='+$('#RefEleccio option:selected').val() + 
                     '&IdColegi=' + $('#RefColegi option:selected').val() + 
                     '&IdMesa='+$('#IdMesa option:selected').val(),
                type: 'get',
                success: function(output) {

                    if (output.indexOf('#')!=-1) {     

                        result = output.split('#');
                        $('#Districte').val(result[3]);
                        $('#Seccio').val(result[4]);
                        $('#Lletra').val(result[5]);
                        $('#Vots1Participacio').val(result[17]);
                        $('#Vots2Participacio').val(result[18]);
                        $('#Vots3Participacio').val(result[19]);

                        // 20 -> vots finals

                        $numCandidatures = result[21];
                        $posVector = 22;
                        $htmlCandidats = "<p><hr>";

                        for (x=1;x<=$numCandidatures;x++)  { 
                            $IdCandidatura = result[$posVector]; 
                            $posVector = $posVector + 1;
                            $Sigles = result[$posVector]; 
                            $posVector = $posVector + 1;
                            $Denominacio = result[$posVector]; 
                            $posVector = $posVector + 1;
                            $Vots = result[$posVector]; 
                            $posVector = $posVector + 1;                                                        

                            $htmlCandidats +=  "<label>" + $Denominacio +  "</label>" + 
                                                "<input class=\"medio_right\" type=\"text\" name=\"" + 
                                                 $IdCandidatura + "\" id=\""  + 
                                                 $IdCandidatura + "\" value=\"" + $Vots + "\"><br>"  + 
                                              "<hr style='clear:both'>";
                        }
                        $htmlCandidats += "</p>";

                        $('#resultats_partits').html($htmlCandidats);

                        $('input.medio_right').css('width','50px');                        
                        $('input.medio_right').css('float','right');                        
                    }
                }
            });
        });    

    });

</script>



<div class="spacer"></div>
</section>

