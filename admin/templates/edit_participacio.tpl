<section id="main" class="column">

<h4 class="alert_info"><i class="icon-pencil"></i>Edició mesa</h4>
{if $result_update=="1"}
<div id="div_resultat">
    <h4 id="resultat_modificacio" class="alert_success">Modificació realitzada</h4>
</div>
{/if}
<article class="module width_full">
    <div class="module_content">
        <form id="participacioForm" method="post" action="edit_participacio.php" novalidate="novalidate" >
            <br>
            
            <p>
                <label>Comici *</label>
                <select name="RefEleccio" id="RefEleccio">
                    <!--<option value="">-- Seleccionar un comici --</option>-->
                {foreach from=$items_eleccions item=fila} 
                    <option value="{$fila->IdEleccio}">{$fila->Denominacio}</option>                   
                {/foreach} 
                </select>
            </p>

            <p>
                <label>Col·legi *</label>
                <label id="colegis">
                    <select name="RefColegi" id="RefColegi">
                        <option value="">-- Selecciona un col·legi --</option>
                    </select>
                </label>
            </p>

            <p>
                <label>Mesa *</label>
                <label id="meses">
                    <select name="IdMesa" id="IdMesa">
                        <option value="">-- Selecciona un col·legi --</option>
                    </select>
                </label>
            </p>

            <p>
                &nbsp;
            </p>

            <p>
                <label>Districte</label>
                <input class="medio" type="text" name="Districte" id="Districte" disabled>
                <label class="center">Secció</label>
                <input class="medio" type="text" name="Seccio" id="Seccio" disabled>
                <label class="center">Lletra</label>
                <input class="corto" type="text" name="Lletra" id="Lletra" disabled>
                <label class="center">Rang alfabètic</label>
                <input class="medio" type="text" name="RangAlfabetic" id="RangAlfabetic">
            </p>

            <p>
                <label>Cens votants</label>
                <input class="medio" type="text" name="CensVotants" id="CensVotants">
                <label class="center">Inicial</label>
                <input class="medio" type="text" name="CensVotantsInicial" id="CensVotantsInicial">
                <label class="center">Final</label>
                <input class="medio" type="text" name="CensVotantsFinal" id="CensVotantsFinal">
            </p>

            <p>
                <label>Rep. Administració</label>
                <input class="extralargo" type="text" name="RepresentantAdmMesa" id="RepresentantAdmMesa" >
                <label class="center">Telf.</label>
                <input class="mediolargo" type="text" name="TelRepresentantAdmMesa" id="TelRepresentantAdmMesa" >
            </p>

            <p>
                <label>Rep. Transmissió</label>
                <input class="extralargo" type="text" name="RepresentantTransmissioDadesMesa" id="RepresentantTransmissioDadesMesa" >
                <label class="center">Telf.</label>
                <input class="mediolargo" type="text" name="TelRepresentantTransmissioDadesMesa" id="TelRepresentantTransmissioDadesMesa" >
            </p>

            <p>
                <label>President</label>
                <input class="extralargo" type="text" name="President" id="President" >
                <label class="center">Telf.</label>
                <input class="mediolargo" type="text" name="TelefonPresident" id="TelefonPresident" >
            </p>

            <p>
                &nbsp;
            </p>

            <p>
                <label>Participació 14:00</label>
                <input class="medio" type="text" name="Vots1Participacio" id="Vots1Participacio">
            </p>

            <p>
                <label>Participació 18:00</label>
                <input class="medio" type="text" name="Vots2Participacio" id="Vots2Participacio">
            </p>

            <p>
                <label>Participació 20:00</label>
                <input class="medio" type="text" name="Vots3Participacio" id="Vots3Participacio">
            </p>

            <p>
                &nbsp;
            </p>

            <p>
                <label>Observacions</label>
                <textarea class="extralargo" name="Observacions" id="Observacions"></textarea>
            </p>

            <p>
                &nbsp;
            </p>

            <center>
                <p>
                    <input class="submit" type="submit" value="Acceptar" id="Acceptar" name="Acceptar">
                </p>

                <p>
                    <label>* Camps obligatoris</label>
                </p>
            </center>

        </form>
    </div>
</article>

<!-- end of styles article -->

<script>

    function resetFields() { 
        $('#Districte').val("");
        $('#Seccio').val("");
        $('#Lletra').val("");
        $('#RangAlfabetic').val("");
        $('#CensVotants').val("");
        $('#CensVotantsInicial').val("");
        $('#CensVotantsFinal').val("");
        $('#RepresentantAdmMesa').val("");
        $('#TelRepresentantAdmMesa').val("");
        $('#RepresentantTransmissioDadesMesa').val("");
        $('#TelRepresentantTransmissioDadesMesa').val("");
        $('#President').val("");
        $('#TelefonPresident').val("");
        $('#Observacions').val("");
        $('#Vots1Participacio').val("");
        $('#Vots2Participacio').val("");
        $('#Vots3Participacio').val("");
    }

    function carregaColegis() { 
        $.ajax({ 
            url: 'carregaColegis.php',
            data: 'IdEleccio='+$('#RefEleccio option:selected').val(),                
            type: 'get',
            success: function(output) {
                $('#resultat_modificacio').css("display","none");

                $('#RefColegi').html(output);
                $('#IdMesa').html("<option value=\"\">-- Selecciona un col·legi --</option>");

                resetFields();
            }
        });
    }

    $().ready(function() {
        carregaColegis();

        $("#participacioForm").validate({
            rules: {
                IdMesa: "required"
            },
            messages: {
                IdMesa: "Atenció! Heu de seleccionar una mesa per editar-la."
            }
        });


        $('#RefEleccio').change(function() {
            carregaColegis();
        });    
            
        $('#RefColegi').change(function() {
            $.ajax({ 
                url: 'carregaMeses.php',
                data: 'IdEleccio='+$('#RefEleccio option:selected').val() + '&IdColegi='+$('#RefColegi option:selected').val(),
                type: 'get',
                success: function(output) {
                    $('#IdMesa').html(output);

                    resetFields();
                }
            });
        });        
        
        $('#IdMesa').change(function() {
            $.ajax({ 
                url: 'carregaMesa.php',
                data: 'IdEleccio='+$('#RefEleccio option:selected').val() + 
                     '&IdColegi=' + $('#RefColegi option:selected').val() + 
                     '&IdMesa='+$('#IdMesa option:selected').val(),
                type: 'get',
                success: function(output) {

                    if (output.indexOf('#')!=-1) { 
                        result = output.split('#');
                        $('#Districte').val(result[3]);
                        $('#Seccio').val(result[4]);
                        $('#Lletra').val(result[5]);
                        $('#RangAlfabetic').val(result[6]);
                        $('#CensVotants').val(result[7]);
                        $('#CensVotantsInicial').val(result[8]);
                        $('#CensVotantsFinal').val(result[9]);
                        $('#RepresentantAdmMesa').val(result[10]);
                        $('#TelRepresentantAdmMesa').val(result[11]);
                        $('#RepresentantTransmissioDadesMesa').val(result[12]);
                        $('#TelRepresentantTransmissioDadesMesa').val(result[13]);
                        $('#President').val(result[14]);
                        $('#TelefonPresident').val(result[15]);
                        $('#Observacions').val(result[16]);
                        $('#Vots1Participacio').val(result[17]);
                        $('#Vots2Participacio').val(result[18]);
                        $('#Vots3Participacio').val(result[19]);
                    }
                }
            });
        });    

    });

</script>



<div class="spacer"></div>
</section>

