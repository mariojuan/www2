<section id="main" class="column">

<h4 class="alert_info"><i class="icon-doc-new"></i>Alta headline</h4>
<article class="module width_full">
    <div class="module_content">
        <form id="headlinesForm" method="post" action="headlines.php" novalidate="novalidate" enctype="multipart/form-data">
            <input type="hidden" name="accio" id="accio" value="edit_headlines">
            <p>
                <label>Títol *</label>
                <input type="text" name="TITOL" id="TITOL" value="{$item->TITOL}">
            </p>
            <p>
                <label>Subtítol *</label>
                <textarea name="SUBTITOL" id="SUBTITUOL">{$item->SUBTITOL}</textarea>
            </p>
            <p>
                <label>Descripció *</label>
                <textarea name="DESCRIPCIO" id="DESCRIPCIO">{$item->DESCRIPCIO}</textarea>
            </p>
            <p>
                <label>Imatge</label>
                {if $item->FOTO|trim!=""}
                    {if $item->IMPORT==1}
                        <span class="foto"><img src="http://www.tortosa.cat/webajt/gestiointerna/headlines/fotos/{$item->FOTO}"></span>
                    {else}
                        <span class="foto"><img src="/images/headlines/res_{$item->FOTO}"></span>
                    {/if}
                {else}
                    <span class="foto"><img src="/images/headlines/inotimage.jpg"></span>
                {/if}
                <input type="file" name="FOTO" id="FOTO">
                <input type="hidden" name="FOTO_AUX" id="FOTO_AUX" value="{$item->FOTO}">
            </p>
            {if $item->FOTO!=""}
            <p>
                <label>Esborrar foto</label>
                <input type="checkbox" name="delete_foto" id="delete_foto">
            </p>
            {/if}
            <p>
                <label>Enllaç</label>
                <input type="text" name="LINK" id="LINK" value="{$item->LINK}">
            </p>
            <p>
                <label>Data</label>
                <input type="text" name="DATA" id="DATA" value="{$item->DATA|date_format:"%d/%m/%G"}">
            </p>
            <p>
                <label>Data inici visualització</label>
                <input type="text" name="DIV" id="DIV" value="{$item->DIV|date_format:"%d/%m/%G"}">
            </p>
            <p>
                <label>Data fi visualització</label>
                <input type="text" name="DFV" id="DFV" value="{$item->DFV|date_format:"%d/%m/%G"}">
            </p>
            <p>
                <label>Fitxer adjunt</label>
                <input type="file" name="ADJUNT" id="ADJUNT">
                <input type="hidden" name="ADJUNT_AUX" id="ADJUNT_AUX" value="{$item->ADJUNT}">
                {if $item->ADJUNT!=""}
                    <div class="adjunt">{$item->ADJUNT}</div>{/if}
            </p>
            {if $item->ADJUNT!=""}
                <p>
                    <label>Esborrar fitxer adjunt</label>
                    <input type="checkbox" name="delete_adjunt" id="delete_adjunt">
                </p>
            {/if}
            <p>
                <label>Tipologia *</label>
                <select name="ID_TIPOLOGIA" id="ID_TIPOLOGIA">
                    <option value="">-- Seleccionar --</option>
                {foreach from=$items_tipologia item=fila}
                    {if $item->ID_TIPOLOGIA==$fila->ID}
                        <option value="{$fila->ID}" selected>{$fila->TIPOLOGIA}</option>
                    {else}
                        <option value="{$fila->ID}">{$fila->TIPOLOGIA}</option>
                    {/if}
                   
                {/foreach} 
                </select>
            </p>
            <p>
                <label>Tipologia</label>
                <select name="ID_TEMATICA[]" id="ID_TEMATICA" multiple>
                    <option value="">-- Seleccionar --</option>
                    {foreach from=$items_tematica item=fila}
                        {foreach from=$items_tematica_headlines item=item_tematica_headlines}
                            {if $fila->ID==$item_tematica_headlines->ID_TEMATICA}
                                {assign var="selected_value" value="selected"}
                                {break}
                            {else}
                                {assign var="selected_value" value=""}
                            {/if}
                        {/foreach}
                        <option value="{$fila->ID}" {$selected_value}>{$fila->TEMATICA}</option>
                    {/foreach}
                </select>
            </p>
            <p>
                <label>Actiu</label>
                <input type="checkbox" name="ACTIVA" id="ACTIVA" {if $item->ACTIVA==1} checked {/if}>
            </p>
            <p>
                <input class="submit" type="submit" value="Submit">
            </p>
            <p>
                <label>* Camps obligatoris</label>
            </p>
            <input type="hidden" name="ADJUNT" id="ADJUNT" value="">
            <input type="hidden" name="FOTO" id="FOTO" value="">
            <input type="hidden" name="ID" id="ID" value="{$item->ID}">
        </form>
    </div>
</article>

<!-- end of styles article -->


<div class="spacer"></div>
</section>

<script>
    /*
    $.validator.setDefaults({
        submitHandler: function() {
            alert("submitted!");
        }
    });
    */

    $().ready(function() {
        $( "#DATA" ).datepicker();
        $( "#DIV" ).datepicker();
        $( "#DFV" ).datepicker();


        // validate signup form on keyup and submit
        
        $("#headlinesForm").validate({
            rules: {
                TITOL:  {
                    required: true,
                    maxlength: 150
                },
                SUBTITOL: "required",
                DESCRIPCIO: "required",
                ID_TIPOLOGIA: "required"
            },
            messages: {
                TITOL: {
                    required: "El camp de títol és obligatori",
                    maxlength: "El camp de títol és massa llarg"
                },
                SUBTITOL: "El camp de subtítol és obligatori",
                DESCRIPCIO: "El camp de descripcio és obligatori",
                ID_TIPOLOGIA: "El camp de tipologia és obligatori"
            }
            /*
            rules: {
                firstname: "required",
                lastname: "required",
                username: {
                    required: true,
                    minlength: 2
                },
                password: {
                    required: true,
                    minlength: 5
                },
                confirm_password: {
                    required: true,
                    minlength: 5,
                    equalTo: "#password"
                },
                email: {
                    required: true,
                    email: true
                },
                topic: {
                    required: "#newsletter:checked",
                    minlength: 2
                },
                agree: "required"
            },
            messages: {
                firstname: "Please enter your firstname",
                lastname: "Please enter your lastname",
                username: {
                    required: "Please enter a username",
                    minlength: "Your username must consist of at least 2 characters"
                },
                password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long"
                },
                confirm_password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long",
                    equalTo: "Please enter the same password as above"
                },
                email: "Please enter a valid email address",
                agree: "Please accept our policy"
            }
            */
            
        });
        
        // propose username by combining first- and lastname
        /*
        $("#username").focus(function() {
            var firstname = $("#firstname").val();
            var lastname = $("#lastname").val();
            if (firstname && lastname && !this.value) {
                this.value = firstname + "." + lastname;
            }
        });

        //code to hide topic selection, disable for demo
        var newsletter = $("#newsletter");
        // newsletter topics are optional, hide at first
        var inital = newsletter.is(":checked");
        var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
        var topicInputs = topics.find("input").attr("disabled", !inital);
        // show when newsletter is checked
        newsletter.click(function() {
            topics[this.checked ? "removeClass" : "addClass"]("gray");
            topicInputs.attr("disabled", !this.checked);
        });
        */
    });
</script>