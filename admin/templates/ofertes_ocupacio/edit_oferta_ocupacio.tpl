<section id="main" class="column">
   <h4 class="alert_info"><i class="icon-doc-new"></i>{if $item[0]->ID==""}Alta{else}Modificació{/if} Oferta Ocupació ({$lang})</h4>
   <article class="module width_full">
      <div class="module_content">
         <form id="ofertesocupacioForm" method="post" action="oferta.php" novalidate="novalidate" enctype="multipart/form-data">
             <input type="hidden" name="accio" id="accio" value="edit_oferta">
             <input type="hidden" name="lang" id="lang" value="{$lang}">
             <input type="hidden" name="ID" id="ID" value="{$item[0]->ID}">
             <input type="hidden" name="FITXERS" id="FITXERS" value="">
             {if $lang==ca}
             <p>
                 <label>ENS *</label>
                 <select name="ID_ENS" id="ID_ENS">
                     <option value="">-- Seleccionar --</option>
                     {foreach $items_ens as $ens}
                         {if $ens->ID == $item[0]->ID_ENS}
                             <option value="{$ens->ID}" selected>{$ens->DESC_CURTA}</option>
                         {else}
                             <option value="{$ens->ID}">{$ens->DESC_CURTA}</option>
                         {/if}
                     {/foreach}
                 </select>
             </p>
            {/if}
            {if $lang==ca}
            <p>
                <label>Número Expedient *</label>
                <input type="text" name="NUMERO_EXPEDIENT" id="NUMERO_EXPEDIENT" value="{$item[0]->NUMERO_EXPEDIENT}">
            </p>
            {/if}
            {if $lang==ca}
             <p>
                 <label>Tipus oferta *</label>
                 <select name="TIPUS_OFERTA" id="TIPUS_OFERTA">
                     <option value="">-- Seleccionar --</option>
                         {if $item[0]->TIPUS_OFERTA == "Places"}
                             <option value="Places" selected>Places</option>
                         {else}
                             <option value="Places">Places</option>
                         {/if}
                         {if $item[0]->TIPUS_OFERTA == "Constitucio borsa de treball"}
                             <option value="Constitucio borsa de treball" selected>Constitució borsa de treball</option>
                         {else}
                             <option value="Constitucio borsa de treball">Constitució borsa de treball</option>
                         {/if}
                 </select>
             </p>
            {/if}
             <p>
                 <label>Descripció curta *</label>
                 <input type="text" name="DESCRIPCIO_CURTA" id="DESCRIPCIO_CURTA" value="{$item[0]->DESCRIPCIO_CURTA}">
             </p>
             <p>
                 <label>Objecte</label>
                 <textarea name="OBJECTE" id="OBJECTE">{$item[0]->OBJECTE}</textarea>
             </p>
             <p>
                 <label>Denominació *</label>
                 <input type="text" name="DENOMINACIO" id="DENOMINACIO" value="{$item[0]->DENOMINACIO}">
             </p>
             <p>
                 <label>Regim jurídic *</label>
                 <input type="text" name="REGIM_JURIDIC" id="REGIM_JURIDIC" value="{$item[0]->REGIM_JURIDIC}">
             </p>
             <p>
                 <label>Caràcter *</label>
                 <input type="text" name="CARACTER" id="CARACTER" value="{$item[0]->CARACTER}">
             </p>
             <p>
                 <label>Escala</label>
                 <input type="text" name="ESCALA" id="ESCALA" value="{$item[0]->ESCALA}">
             </p>
             <p>
                 <label>Subescala</label>
                 <input type="text" name="SUBESCALA" id="SUBESCALA" value="{$item[0]->SUBESCALA}">
             </p>
             <p>
                 <label>Classe</label>
                 <input type="text" name="CLASSE" id="CLASSE" value="{$item[0]->CLASSE}">
             </p>
             <p>
                 <label>Grup de Classificació *</label>
                 <input type="text" name="GRUP" id="GRUP" value="{$item[0]->GRUP}">
             </p>
             {if $lang==ca}
             <p>
                 <label>Número de Places *</label>
                 <input type="text" name="NUM_PLACES" id="NUM_PLACES" value="{$item[0]->NUM_PLACES}">
             </p>
             {/if}
             <p>
                 <label>Sistema de Selecció *</label>
                 <input type="text" name="SISTEMA_SELECCIO" id="SISTEMA_SELECCIO" value="{$item[0]->SISTEMA_SELECCIO}">
             </p>
             <p>
                 <label>Requisits</label>
                 <textarea name="REQUISITS" id="REQUISITS">{$item[0]->REQUISITS}</textarea>
             </p>
             {if $lang==ca}
             <p>
                 <label style="width: 130px">Data Inici Presentació Sol·licituds</label>
                 <input type="text" name="DATA_INICI_PRESENT_SOLLICITUDS" id="DATA_INICI_PRESENT_SOLLICITUDS" value="{$item[0]->DATA_INICI_PRESENT_SOLLICITUDS|date_format:"%d/%m/%G"}">
             </p>
             {/if}
             {if $lang==ca}
                 <p>
                     <label style="width: 130px">Data Fi Presentació Sol·licituds</label>
                     <input type="text" name="DATA_FI_PRESENT_SOLLICITUDS" id="DATA_FI_PRESENT_SOLLICITUDS" value="{$item[0]->DATA_FI_PRESENT_SOLLICITUDS|date_format:"%d/%m/%G"}">
                 </p>
             {/if}
             <p>
                 <label>Drets Examen</label>
                 <textarea name="DRETS_EXAMEN" id="DRETS_EXAMEN">{$item[0]->DRETS_EXAMEN}</textarea>
             </p>
             {if $lang==ca}
                 <p>
                     <label style="width: 130px">Data Inici Presentació Alegacions</label>
                     <input type="text" name="DATA_INICI_PRESENT_ALEGACIONS" id="DATA_INICI_PRESENT_ALEGACIONS" value="{$item[0]->DATA_INICI_PRESENT_ALEGACIONS|date_format:"%d/%m/%G"}">
                 </p>
             {/if}
             {if $lang==ca}
                 <p>
                     <label style="width: 130px">Data Fi Presentació Alegacions</label>
                     <input type="text" name="DATA_FI_PRESENT_ALEGACIONS" id="DATA_FI_PRESENT_ALEGACIONS" value="{$item[0]->DATA_FI_PRESENT_ALEGACIONS|date_format:"%d/%m/%G"}">
                 </p>
             {/if}
             {if $lang==ca}
                 <p>
                     <label style="width: 130px">Data Inici Vigència Borsa Treball</label>
                     <input type="text" name="DATA_INICI_VIGENCIA_BORSA_TREBALL" id="DATA_INICI_VIGENCIA_BORSA_TREBALL" value="{$item[0]->DATA_INICI_VIGENCIA_BORSA_TREBALL|date_format:"%d/%m/%G"}">
                 </p>
             {/if}
             {if $lang==ca}
                 <p>
                     <label style="width: 130px">Data Inici Visualització</label>
                     <input type="text" name="DATA_INICI_VISUALITZACIO" id="DATA_INICI_VISUALITZACIO" value="{$item[0]->DATA_INICI_VISUALITZACIO|date_format:"%d/%m/%G"}">
                 </p>
             {/if}
             {if $lang==ca}
                 <p>
                     <label style="width: 130px">Data Fi Visualització</label>
                     <input type="text" name="DATA_FI_VISUALITZACIO" id="DATA_FI_VISUALITZACIO" value="{$item[0]->DATA_FI_VISUALITZACIO|date_format:"%d/%m/%G"}">
                 </p>
             {/if}
             {if $lang==ca}
             <p>
                 <label>Estat *</label>
                 <select name="ESTAT" id="ESTAT">
                     <option value="">-- Seleccionar --</option>
                     {if $item[0]->ESTAT == "Aprovacio bases"}
                         <option value="Aprovacio bases" selected>Aprovació bases</option>
                     {else}
                         <option value="Aprovacio bases">Aprovacio bases</option>
                     {/if}
                     {if $item[0]->ESTAT == "Pendent convocatoria"}
                         <option value="Pendent convocatoria" selected>Pendent convocatòria</option>
                     {else}
                         <option value="Pendent convocatoria">Pendent convocatòria</option>
                     {/if}
                     {if $item[0]->ESTAT == "Obert periode de sollicituds"}
                         <option value="Obert periode de sollicituds" selected>Obert període de sol·licituds</option>
                     {else}
                         <option value="Obert periode de sollicituds">Obert període de sol·licituds</option>
                     {/if}
                     {if $item[0]->ESTAT == "En proces de seleccio"}
                         <option value="En proces de seleccio" selected>En procés de selecció</option>
                     {else}
                         <option value="En proces de seleccio">En procés de selecció</option>
                     {/if}
                     {if $item[0]->ESTAT == "Borsa vigent"}
                         <option value="Borsa vigent" selected>Borsa vigent</option>
                     {else}
                         <option value="Borsa vigent">Borsa vigent</option>
                     {/if}
                     {if $item[0]->ESTAT == "Borsa no vigent"}
                         <option value="Borsa no vigent" selected>Borsa no vigent</option>
                     {else}
                         <option value="Borsa no vigent">Borsa no vigent</option>
                     {/if}
                 </select>
             </p>
            {/if}
            {if $lang==ca}
            <div style="border: 1px solid #c0c0c0; padding: 2px">
                <p>
                    <label>Descripció fitxer</label>
                    <input type="text" name="DESCRIPCIO" id="DESCRIPCIO" value="" class="file">
                </p>
                <p>
                    <label>Informació *</label>
                    <select name="INFORMACIO_FITXER" id="INFORMACIO_FITXER">
                        <option value="">-- Seleccionar --</option>
                        <option value="bases">Bases</option>
                        <option value="llista-provisional">Llista provisional</option>
                        <option value="llista-definitiva">Llista definitiva</option>
                        <option value="resultats">Resultats</option>
                        <option value="borsa-treball-provisional">Borsa treball provisional</option>
                        <option value="borsa-treball-definitiva">Borsa treball definitiva</option>
                    </select>
                </p>
                <p>
                    <label>Fitxer *</label>
                    <input type="file" name="fitxer" id="fitxer" value="" class="fitxer">
                </p>
                <p>
                    <i class="icon-upload-outline icon icon-upload"></i>
                </p>
            </div>
            <div id="file_list">
            {foreach $items_files as $item_file}
                <div class="uploaded_file">
                    <div>
                        <a href="/public_files/ofertes_ocupacio/{$item_file->NOM_FITXER}" target="_blank"><i class="icon-file-pdf"></i>{$item_file->DESCRIPCIO}</a>
                    </div>
                    <div>
                        <i class="icon-trash-empty" id="{$item_file->NOM_FITXER}" title=""></i>
                    </div>
                </div>
            {/foreach}
            </div>
            {/if}
            {if $lang==ca}
            <p>
                <label>Baixa</label>
                <input type="checkbox" name="BAIXA" id="BAIXA" {if $item[0]->BAIXA}checked {/if}>
            </p>
            {/if}
            {if $lang==ca}
            <p>
                <label>Visible</label>
                <input type="checkbox" name="VISIBLE" id="VISIBLE" {if $item[0]->VISIBLE}checked {/if}>
            </p>
            {/if}
            <p>
               <input class="submit" type="submit" value="  {if $item[0]->ID==""}Alta{else}Modificació{/if}  ">
            </p>
            <p>
               <label>* Camps obligatoris</label>
            </p>
         </form>
      </div>
   </article>

   <!-- end of styles article -->
   <div class="spacer"></div>
</section>
<script type="text/javascript">
    permet_fitxer = true;
    $().ready(function() {
        $( "#DATA_INICI_PRESENT_SOLLICITUDS" ).datepicker();
        $( "#DATA_FI_PRESENT_SOLLICITUDS" ).datepicker();
        $( "#DATA_INICI_PRESENT_ALEGACIONS" ).datepicker();
        $( "#DATA_FI_PRESENT_ALEGACIONS" ).datepicker();
        $( "#DATA_INICI_VIGENCIA_BORSA_TREBALL" ).datepicker();
        $( "#DATA_INICI_VISUALITZACIO" ).datepicker();
        $( "#DATA_FI_VISUALITZACIO" ).datepicker();

        $("#ofertesocupacioForm").validate({
            rules: {
                ID_ENS:  {
                    required: true
                },
                NUMERO_EXPEDIENT:  {
                    required: true
                },
                TIPUS_OFERTA:  {
                    required: true
                },
                DENOMINACIO: {
                    required: true
                },
                REGIM_JURIDIC:  {
                    required: true
                },
                CARACTER:  {
                    required: true
                },
                GRUP:  {
                    required: true
                },
                NUM_PLACES:  {
                    required: true
                },
                ESTAT:  {
                    required: true
                }
            },
            messages: {
                ID_ENS:  {
                    required: "El camp de ENS és obligatori"
                },
                NUMERO_EXPEDIENT:  {
                    required: "El camp de Número d'Expedient és obligatori"
                },
                TIPUS_OFERTA:  {
                    required: "El camp de Tipus d'Oferta és obligatori"
                },
                DENOMINACIO:  {
                    required: "El camp de Denominació és obligatori"
                },
                REGIM_JURIDIC:  {
                    required: "El camp de Règim Jurídic és obligatori"
                },
                CARACTER:  {
                    required: "El camp de Caràcter és obligatori"
                },
                GRUP:  {
                    required: "El camp de Grup és obligatori"
                },
                NUM_PLACES:  {
                    required: "El camp de Número Places és obligatori"
                },
                ESTAT:  {
                    required: "El camp d'Estat és obligatori"
                }
            },
            submitHandler: function(form) {
                if ($('#BAIXA').prop('checked')) {
                    $('#BAIXA').val("1");
                }
                else {
                    $('#BAIXA').val("0");
                }
                if($('#VISIBLE').prop('checked')) {
                    $('#VISIBLE').val("1");
                }
                else {
                    $('#VISIBLE').val("0");
                }
                var fitxers_arr = new Array();
                $('.icon-trash-empty').each(function() {
                    fitxers_arr.push($(this).attr('id'));
                });
                $('#FITXERS').val(fitxers_arr.toString());
                form.submit();
            }
        });
        $('#fitxer').bind('change', function() {

            //this.files[0].size gets the size of your file.
           if(this.files[0].size<2000000) {
                permet_fitxer = true;
           }
            else {
                permet_fitxer = false;
           }
        });
        $('i.icon-upload').click(function() {
            if($('#DESCRIPCIO').val()!='' && ($('#fitxer').val()!='') &&(permet_fitxer)) {
                var file_data = $('#fitxer').prop('files')[0];
                var form_data = new FormData();
                form_data.append('accio', 'upload');
                form_data.append('file', file_data);
                form_data.append('descripcio', $('#DESCRIPCIO').val());
                form_data.append('informacio_fitxer', $('#INFORMACIO_FITXER').val());
                form_data.append('carpeta', 'ofertes_ocupacio');
                form_data.append('taula_bbdd', 'OO_OFERTA_OCUPACIO');
                {if {$item[0]->ID}==""}
                form_data.append('id_taula_bbdd', '0');
                {else}
                form_data.append('id_taula_bbdd', '{$item[0]->ID}');
                {/if}
                //alert(form_data);
                $.ajax({
                    url: 'https://www2.tortosa.cat/admin/upload-files.php', // point to server-side PHP script
                    dataType: 'text',  // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success: function(php_script_response){
                        if(php_script_response!="") {
                            var content = '<div class="uploaded_file"><div><a href="/public_files/ofertes_ocupacio/'+$('#fitxer').val("")+'" target="_blank"><i class="icon-file-pdf"></i>'+$('#DESCRIPCIO').val()+'</a></div><div><i class="icon-trash-empty" id="'+php_script_response+'" title="Esborrar '+$('#DESCRIPCIO').val()+'"></i></div></div>';
                            $('#file_list').append(content);
                            $('#DESCRIPCIO').val("");
                            $('#fitxer').val("");
                        }
                    }
                });
            }
        });
    });
    $(document).on('click', '.icon-trash-empty', function() {
        var form_data = new FormData();
        { /* if $item[0]->ID */ }
        //form_data.append('accio', 'disable');
        { /* else */ }
        form_data.append('accio', 'delete');
        { /*/if */ }
        form_data.append('carpeta', 'ofertes_ocupacio');
        form_data.append('taula_bbdd', 'OO_OFERTA_OCUPACIO');
        form_data.append('file', $(this).attr('id'));
        var elem = $(this);
        $.ajax({
            url: 'https://www2.tortosa.cat/admin/upload-files.php', // point to server-side PHP script
            dataType: 'text',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function(php_script_response) {
                if(php_script_response=="1") {
                    elem.parent().parent().remove();
                }
            }
        });
    });
</script>