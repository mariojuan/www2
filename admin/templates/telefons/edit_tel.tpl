<section id="main" class="column">
   <h4 class="alert_info"><i class="icon-doc-new"></i>Alta telèfon</h4>

   <article class="module width_full">
      <div class="module_content">
         <form id="headlinesForm" method="post" action="telefons.php" novalidate="novalidate" enctype="multipart/form-data">
            <input type="hidden" name="accio" id="accio" value="edit_telefon">

            <input type="hidden" name="ID" id="ID" value="{$item->ID}">

            <p>
               <label>Servei *</label>
               <input type="text" name="SERVEI" id="SERVEI" value="{$item->SERVEI}">
            </p>

            <p>
               <label>Correu</label>
               <input type="text" name="CORREU" id="CORREU" value="{$item->CORREU}">
            </p>

            <p>
               <label>Telèfon</label>
               <input type="text" name="TELEFON" id="TELEFON" value="{$item->TELEFON}">
            </p>

            <p>
               <input class="submit" type="submit" value="Submit">
            </p>
            <p>
               <label>* Camps obligatoris</label>
            </p>
            
         </form>
      </div>
   </article>

   <!-- end of styles article -->
   <div class="spacer"></div>
</section>