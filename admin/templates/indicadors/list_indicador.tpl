<section id="main" class="column">

<h4 class="alert_info"><i class="icon-pencil"></i>Llistat d'Indicadors</h4>
<article class="module width_full">
    <div class="module_content">
        <table class="list_table" name="list_banners" id="list_banners">
            <tr>
                <th style="width: 5%">Codi</th>
                <th style="width: 30%">Indicador</th>
                <th style="width: 15%">Grup</th>
                <th style="width: 25%">Subgrup</th>
                <th style="width: 10%">Estat</th>
                <th style="width: 5%">Tipus</th>
                <th style="width: 5%">Visible</th>
                <th style="width: 5%">Baixa</th>
            </tr>
            <tr><td colspan="9"></td></tr>
            {foreach $items as $item}
                <tr>
                    <td>{$item->CODI_INDICADOR}</td>
                    <td>{$item->INDICADOR}</td>
                    <td>{$item->GRUP_INDICADOR}</td>
                    <td>{$item->SUBGRUP_INDICADOR}</td>
                    <td>{$item->ESTAT}</td>
                    <td>{$item->TIPUS_INDICADOR}</td>
                    <td>{$item->VISIBLE}</td>
                    <td>{$item->BAIXA}</td>
                    <td class="link_item modify">
                        <span><i class="icon-pencil"></i></span>
                        <div class="language-{$item->ID} language">
                            <ul>
                                <li><a href="/admin/indicadors_transparencia/indicadors.php?id={$item->ID}&lang=ca&accio=edit_indicador">ca</a></li>
                                <li><a href="/admin/indicadors_transparencia/indicadors.php?id={$item->ID}&lang=es&accio=edit_indicador">es</a></li>
                                <li><a href="/admin/indicadors_transparencia/indicadors.php?id={$item->ID}&lang=en&accio=edit_indicador">en</a></li>
                                <li><a href="/admin/indicadors_transparencia/indicadors.php?id={$item->ID}&lang=fr&accio=edit_indicador">fr</a></li>
                            </ul>
                        </div>
                    </td>
                    <td class="link_item">
                        {if $info_usuari["rol"]=="Superadministrador"}
                        <a href="JavaScript: delete_item('{$item->ID}', '{$page}');"><i class="icon-trash-empty"></i></a></td>
                        {/if}
                </tr>
            {/foreach}
        </table>
    </div>
    <div id="pagination">
        <ul>
            {if $lastpage>1}
                {if $page> 1}
                    {assign var="page_ant" value=$page - 1}
                    <li><a href="indicadors.php?page={$page_ant}">Ant</a></li>
                {/if}
                {assign var="desp" value=$page + 4}
                {assign var="firstpage" value=$page - 4}

                {if $desp < $lastpage}
                    {assign var="lastpage" value=$page + 4}
                {/if}
                {if $firstpage < 1}
                    {assign var="firstpage" value=1}
                {/if}

                {for $counter=$firstpage to $lastpage}
                    {if $counter==$page}
                        <li>{$counter}</li>
                    {else}
                        <li><a href="indicadors.php?page={$counter}">{$counter}</a></li>
                    {/if}
                {/for}
                {if $page < $lastpage - 1}
                    {assign var="page_seg" value=$page + 1}
                    <li><a href="indicadors.php?page={$page_seg}">Seg</a></li>
                {/if}
            {/if}
        </ul>
    </div>
</article>

<!-- end of styles article -->


<div class="spacer"></div>
</section>

<script>
    /*
     $.validator.setDefaults({
     submitHandler: function() {
     alert("submitted!");
     }
     });
     */

    $().ready(function() {
        $(".modify").click(function() {

            if($(this).children("div").css('display')==="none") {
                $(".modify").children("div").css("display", "none");
                $(this).children("div").css("display", "block");
                exit();
            }
            if($(this).children("div").css('display')==="block") {
                $(this).children("div").css("display", "none");
                exit();
            }
        });
    });

    function delete_item(id, page) {
        if(confirm("Vols esborrar aquest element?"))
            window.location.href="indicadors.php?accio=delete_indicador&id="+id+"&page="+page;
    }
</script>