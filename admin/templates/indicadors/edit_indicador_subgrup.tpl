<section id="main" class="column">
   <h4 class="alert_info"><i class="icon-doc-new"></i>{if $item[0]->ID==""}Alta{else}Modificació{/if} Subgrup d'Indicador ({$lang})</h4>
   <article class="module width_full">
      <div class="module_content">
         <form id="indicadorsGrupForm" method="post" action="indicadors_subgrup.php" novalidate="novalidate" enctype="multipart/form-data">
             <input type="hidden" name="accio" id="accio" value="edit_indicador_subgrup">
             <input type="hidden" name="lang" id="lang" value="{$lang}">
             <input type="hidden" name="ID" id="ID" value="{$item[0]->ID}">
            {if $lang==ca}
            <p>
                <label>Grup *</label>
                <select name="ID_GRUP" id="ID_GRUP">
                    <option value="">-- Seleccionar --</option>
                    {foreach $grups as $grup}
                        {if $grup->ID == $item[0]->ID_GRUP}
                        <option value="{$grup->ID}" selected>{$grup->GRUP_INDICADOR}</option>
                        {else}
                            <option value="{$grup->ID}">{$grup->GRUP_INDICADOR}</option>
                        {/if}
                    {/foreach}
                </select>
            </p>
            {/if}
            {if $lang==ca}
            <p>
               <label>Codi *</label>
               <input type="text" name="CODI_SUBGRUP_INDICADOR" id="CODI_SUBGRUP_INDICADOR" value="{$item[0]->CODI_SUBGRUP_INDICADOR}">
            </p>
            {/if}
            {if $lang==ca}
            <p>
               <label>Tipus indicador *</label>
               <select name="TIPUS_INDICADOR" id="TIPUS_INDICADOR">
                   <option value="">-- Seleccionar --</option>
                   <option value="Infoparticipa" {if $item[0]->TIPUS_INDICADOR=="Infoparticipa"}selected{/if}>Infoparticipa</option>
                   <option value="ITA" {if $item[0]->TIPUS_INDICADOR=="ITA"}selected{/if}>ITA</option>
               </select>
            </p>
            {/if}
            {if $lang==ca}
            <p>
               <label>Exercici *</label>
                <select name="EXERCICI" id="EXERCICI">
                    <option value="">-- Seleccionar --</option>
                    {for $i=2017 to 2050}
                        {if $i==$item[0]->EXERCICI}
                            <option value="{$i}" selected>{$i}</option>
                        {else}
                            <option value="{$i}">{$i}</option>
                        {/if}
                    {/for}
                </select>
            </p>
            {/if}
             <p>
                 <label>Nom Subgrup *</label>
                 <input type="text" name="SUBGRUP_INDICADOR" id="SUBGRUP_INDICADOR" value="{$item[0]->SUBGRUP_INDICADOR}">
             </p>
            {if $lang==ca}
            <p>
                <label>Baixa</label>
                <input type="checkbox" name="BAIXA" id="BAIXA" {if $item[0]->BAIXA}checked {/if}>
            </p>
            {/if}
            {if $lang==ca}
            <p>
                <label>Visible</label>
                <input type="checkbox" name="VISIBLE" id="VISIBLE" {if $item[0]->VISIBLE}checked {/if}>
            </p>
            {/if}
            <p>
               <input class="submit" type="submit" value="  {if $item[0]->ID==""}Alta{else}Modificació{/if}  ">
            </p>
            <p>
               <label>* Camps obligatoris</label>
            </p>
         </form>
      </div>
   </article>

   <!-- end of styles article -->
   <div class="spacer"></div>
</section>
<script>
    $().ready(function() {
        $("#indicadorsGrupForm").validate({
            rules: {
                ID_GRUP:  {
                    required: true
                },
                CODI_SUBGRUP_INDICADOR:  {
                    required: true
                },
                TIPUS_INDICADOR: {
                    required: true
                },
                EXERCICI: "required",
                SUBGRUP_INDICADOR: "required"
            },
            messages: {
                ID_GRUP:  {
                    required: "El camp de grup d'indicador és obligatori"
                },
                CODI_SUBGRUP_INDICADOR: {
                    required: "El camp de codi de subgrup és obligatori"
                },
                TIPUS_INDICADOR: {
                    required: "El camp de tipus d'indicador és obligatori"
                },
                EXERCICI: {
                    required: "El camp d'exercici és obligatori"
                },
                SUBGRUP_INDICADOR: {
                    required: "El camp de subgrup és obligatori"
                }
            },
            submitHandler: function(form) {
                if ($('#BAIXA').prop('checked')) {
                    $('#BAIXA').val("1");
                }
                else {
                    $('#BAIXA').val("0");
                }
                if($('#VISIBLE').prop('checked')) {
                    $('#VISIBLE').val("1");
                }
                else {
                    $('#VISIBLE').val("0");
                }
                form.submit();
            }
        });
    });
</script>