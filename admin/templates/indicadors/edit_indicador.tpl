<section id="main" class="column">
   <h4 class="alert_info"><i class="icon-doc-new"></i>{if $item[0]->ID==""}Alta{else}Modificació{/if} d'Indicador ({$lang})</h4>
   <article class="module width_full">
      <div class="module_content">
         <form id="indicadorsGrupForm" method="post" action="indicadors.php" novalidate="novalidate" enctype="multipart/form-data">
             <input type="hidden" name="accio" id="accio" value="edit_indicador">
             <input type="hidden" name="lang" id="lang" value="{$lang}">
             <input type="hidden" name="ID" id="ID" value="{$item[0]->ID}">
             <input type="hidden" name="max_links" id="max_links" value="{if $links|@count==0}1{else}{$links|@count}{/if}">
            {if $lang==ca}
            <p>
                <label>Grup *</label>
                <select name="ID_GRUP" id="ID_GRUP">
                    <option value="">-- Seleccionar --</option>
                    {foreach $grups as $grup}
                        {if $grup->ID == $item[0]->ID_GRUP}
                        <option value="{$grup->ID}" selected>{$grup->GRUP_INDICADOR}</option>
                        {else}
                            <option value="{$grup->ID}">{$grup->GRUP_INDICADOR}</option>
                        {/if}
                    {/foreach}
                </select>
            </p>
            {/if}
            {if $lang==ca}
                 <p>
                     <label>Subgrup</label>
                     <select name="ID_SUBGRUP" id="ID_SUBGRUP">
                         <option value="">-- Seleccionar --</option>
                         {foreach $subgrups as $subgrup}
                             {if $subgrup->ID == $item[0]->ID_SUBGRUP}
                                 <option value="{$subgrup->ID}" selected>{$subgrup->SUBGRUP_INDICADOR}</option>
                             {else}
                                 <option value="{$subgrup->ID}">{$subgrup->SUBGRUP_INDICADOR}</option>
                             {/if}
                         {/foreach}
                     </select>
                 </p>
            {/if}
            {if $lang==ca}
            <p>
               <label>Codi *</label>
               <input type="text" name="CODI_INDICADOR" id="CODI_INDICADOR" value="{$item[0]->CODI_INDICADOR}">
            </p>
            {/if}
            {if $lang==ca}
            <p>
               <label>Tipus indicador *</label>
               <select name="TIPUS_INDICADOR" id="TIPUS_INDICADOR">
                   <option value="">-- Seleccionar --</option>
                   <option value="Infoparticipa" {if $item[0]->TIPUS_INDICADOR=="Infoparticipa"}selected{/if}>Infoparticipa</option>
                   <option value="ITA" {if $item[0]->TIPUS_INDICADOR=="ITA"}selected{/if}>ITA</option>
               </select>
            </p>
            {/if}
            {if $lang==ca}
            <p>
               <label>Exercici *</label>
                <select name="EXERCICI" id="EXERCICI">
                    <option value="">-- Seleccionar --</option>
                    {for $i=2017 to 2050}
                        {if $i==$item[0]->EXERCICI}
                            <option value="{$i}" selected>{$i}</option>
                        {else}
                            <option value="{$i}">{$i}</option>
                        {/if}
                    {/for}
                </select>
            </p>
            {/if}
            <p>
                 <label>Nom Indicador *</label>
                 <input type="text" name="INDICADOR" id="INDICADOR" value="{$item[0]->INDICADOR}">
            </p>
            <p id="add_link" style="cursor: hand">afegir enllaç</p>
            <div id="links_list">
                {assign var="cont" value="0"}
                {if $links|@count>0}
                    {foreach $links as $link}
                        {assign var="cont" value=$cont+1}
                        <div id="fila_link{$cont}">
                        <p>
                            <label>Descripció enllaç</label>
                            <input type="text" name="DESCRIPCIO{$cont}" id="DESCRIPCIO{$cont}" value="{$link->DESCRIPCIO}" class="link">
                        </p>
                        <p>
                            <label>Enllaç *</label>
                            <input type="text" name="LINK{$cont}" id="LINK{$cont}" value="{$link->LINK}" class="link"><i class="icon-trash-empty" id="{$cont}"></i>
                        </p>
                        </div>
                    {/foreach}
                {else}
                    {assign var="cont" value=$cont+1}
                    <div id="fila_link{$cont}">
                    <p>
                        <label>Descripció enllaç</label>
                        <input type="text" name="DESCRIPCIO{$cont}" id="DESCRIPCIO{$cont}" value="{$link->DESCRIPCIO}" class="link">
                    </p>
                    <p>
                        <label>Enllaç *</label>
                        <input type="text" name="LINK{$cont}" id="LINK{$cont}" value="{$link->LINK}" class="link"><i class="icon-trash-empty" id="{$cont}"></i>
                    </p>
                    </div>
                {/if}
            </div>
            {if $lang==ca}
                <p>
                    <label>Estat *</label>
                    <select name="ESTAT" id="ESTAT">
                        <option value="">-- Seleccionar --</option>
                        {if $item[0]->ESTAT=="Completat"}
                        <option value="Completat" selected>Completat</option>
                        {else}
                        <option value="Completat">Completat</option>
                        {/if}
                        {if $item[0]->ESTAT=="No completat"}
                        <option value="No completat" selected>No completat</option>
                        {else}
                        <option value="No completat">No completat</option>
                        {/if}
                        {if $item[0]->ESTAT=="En proces"}
                        <option value="En proces" selected>En procés</option>
                        {else}
                        <option value="En proces">En procés</option>
                        {/if}
                    </select>
                </p>
            {/if}
            <p>
                 <label>Comentaris</label>
                 <textarea name="COMENTARIS" id="COMENTARIS">{$item[0]->COMENTARIS}</textarea>
            </p>
            {if $lang==ca}
            <p>
                <label>Baixa</label>
                <input type="checkbox" name="BAIXA" id="BAIXA" {if $item[0]->BAIXA}checked {/if}>
            </p>
            {/if}
            {if $lang==ca}
            <p>
                <label>Visible</label>
                <input type="checkbox" name="VISIBLE" id="VISIBLE" {if $item[0]->VISIBLE}checked {/if}>
            </p>
            {/if}
            <p>
               <input class="submit" type="submit" value="  {if $item[0]->ID==""}Alta{else}Modificació{/if}  ">
            </p>
            <p>
               <label>* Camps obligatoris</label>
            </p>
         </form>
      </div>
   </article>

   <!-- end of styles article -->
   <div class="spacer"></div>
</section>
<script>
    $().ready(function() {
        var cont = {$cont+1};
        $("#indicadorsGrupForm").validate({
            rules: {
                ID_GRUP:  {
                    required: true
                },
                CODI_INDICADOR:  {
                    required: true
                },
                TIPUS_INDICADOR: {
                    required: true
                },
                EXERCICI: "required",
                INDICADOR: "required",
                LINK: "required",
                ESTAT: "required"
            },
            messages: {
                ID_GRUP:  {
                    required: "El camp de grup d'indicador és obligatori"
                },
                CODI_INDICADOR: {
                    required: "El camp de codi d'indicador és obligatori"
                },
                TIPUS_INDICADOR: {
                    required: "El camp de tipus d'indicador és obligatori"
                },
                EXERCICI: {
                    required: "El camp d'exercici és obligatori"
                },
                INDICADOR: {
                    required: "El camp d'indicador és obligatori"
                },
                LINK: {
                    required: "El camp d'enllaç és obligatori"
                },
                ESTAT: {
                    required: "El camp d'estat és obligatori"
                }
            },
            submitHandler: function(form) {
                if ($('#BAIXA').prop('checked')) {
                    $('#BAIXA').val("1");
                }
                else {
                    $('#BAIXA').val("0");
                }
                if($('#VISIBLE').prop('checked')) {
                    $('#VISIBLE').val("1");
                }
                else {
                    $('#VISIBLE').val("0");
                }
                form.submit();
            }
        });
        $('#add_link').click(function(){
            var content = '' +
                    '<div id="fila_link'+cont+'"> ' +
                    '<p> ' +
                            '<label>Descripció enllaç</label>' +
                            ' <input type="text" name="DESCRIPCIO'+cont+'" id="DESCRIPCIO'+cont+'" class="link" value="">' +
                          '</p>' +
                    '<p>' +
                        '<label>Enllaç *</label>'+
                        ' <input type="text" name="LINK'+cont+'" id="LINK'+cont+'" value="" class="link"><i class="icon-trash-empty" id="'+cont+'"></i>' +
                    '</p>' +
                    '</div>';
            $('#links_list').append(content);
            $('#max_links').val(cont);
            cont++;
        });
        $(document).on('click', '.icon-trash-empty', function() {
            $('#DESCRIPCIO'+$(this).attr('id')).val('');
            $('#LINK'+$(this).attr('id')).val('');
            $('#fila_link'+$(this).attr('id')).remove();
            cont--;
        });
    });
</script>