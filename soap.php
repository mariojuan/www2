<?php

/*
ini_set('soap.wsdl_cache_enabled', 0);
ini_set('soap.wsdl_cache_ttl', 900);
ini_set('default_socket_timeout', 15);

$param1 = "a";

$params = array('param1'=>$param1);


$wsdl = 'http://psisbeta.catcert.net/wsdl/dss-pre.wsdl';

$options = array(
    'uri'=>'http://schemas.xmlsoap.org/soap/envelope/',
    'style'=>SOAP_RPC,
    'use'=>SOAP_ENCODED,
    'soap_version'=>SOAP_1_2,
    'cache_wsdl'=>WSDL_CACHE_NONE,
    'connection_timeout'=>15,
    'trace'=>true,
    'encoding'=>'UTF-8',
    'exceptions'=>true,
);

try {
    $soap = new SoapClient($wsdl, $options);
    $data = $soap->method($params);
}
catch(Exception $e) {
    die($e->getMessage());
}

var_dump($data);

die;
*/

$wsdlUrl = 'http://psisbeta.catcert.net/wsdl/dss-pre.wsdl';

// Initiate SOAP client
$client = new SoapClient(
    $wsdlUrl,
    array(
        'stream_context'=>stream_context_create(array(
                'ssl' => array(
                    'verify_peer' => true,
                    'cafile' => __DIR__ . '/cacert.pem',
                    'CN_match' => 'prime.cosigntrial.com'
                )
            )
        )
    ));

// Send the request
$output = $client->DssSign($signRequest);
?>