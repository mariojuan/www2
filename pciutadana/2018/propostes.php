<?php
namespace webtortosa;

require_once("../../config.cfg");
require_once(FOLDER_CONTROLLER . "/web_controller.php");

//se instancia al controlador
$controller = new web_controller();

// Se li passa el número 9 que indica que és propostes
$controller->presparticipa2018($_GET['lang'], 9);
?>