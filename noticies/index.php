<?php
namespace webtortosa;

require_once("../config.cfg");
require_once(FOLDER_CONTROLLER . "/web_controller.php");

//se instancia al controlador
$controller = new web_controller();
/*
 * Paràmetre 2: element del menú
 * Paràmetre 3: identificador per a la tira d'imatge
 */

$controller->noticies($_GET['lang'], 3, 2, $_GET, $_POST);
?>