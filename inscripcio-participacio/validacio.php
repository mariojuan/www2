<?
namespace webtortosa;

require_once("../config.cfg");
require_once(FOLDER_CONTROLLER . "/web_controller.php");
require_once(FOLDER_CONTROLLER . "/web_controller_registre_telematic.php");

//se instancia al controlador
$controller = new web_controller_registre_telematic();

if($_GET['test']) {
    $controller->desencripta($_GET[test]);
    die;
}


$controller->validacio_registre_telematic($_GET);
?>