<?
namespace webtortosa;

require_once("../config.cfg");
require_once(FOLDER_CONTROLLER . "/web_controller.php");
require_once(FOLDER_CONTROLLER . "/web_controller_registre_telematic.php");
require_once(FOLDER_CONTROLLER . "/web_controller_votacio_telematica.php");


$fecha_actual = strtotime(date("d-m-Y H:i:00",time()));
/*
if(isset($_GET['test1'])) {
    $fecha_actual = strtotime("29-05-2018 01:01:10");
}
*/

//$fecha_actual = strtotime("29-04-2018 01:01:10");
$fecha_inici = strtotime("28-04-2018 00:00:00");
$fecha_fi = strtotime("11-05-2018 23:59:59");

if ( ($fecha_actual > $fecha_inici) && ($fecha_actual < $fecha_fi)) {
    $controller = new web_controller_registre_telematic();
    $controller->registre_telematic($_GET['lang'], $_POST);
}
else {
    $controller = new web_controller_votacio_telematica();
    $controller->votacio_telematica_fora_termini($_GET['lang'], $_POST);
}
?>