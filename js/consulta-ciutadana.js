$(document).ready(function() {
    var lletra = "Lletra";
    var numero = "N\u00FAmero";
    var numero_passaport = "N\u00FAmero de passaport";
    var isMobile = window.matchMedia("only screen and (max-width: 768px)");

    $('.option1').css('display', 'none');
    $('.option2').css('display', 'none');
    $('.option3').css('display', 'none');

    $('#enviar').click(function() {
        if(other_validates()) {
            $("#option1-error").css('display', 'none');
            $("#option2-error").css('display', 'none');
            $("#option3-error").css('display', 'none');
            $("#recaptcha-error").css('display', 'none');
            $("#result").css('display', 'inline-block');
            erase_results();
            captcha();
            return false;
        }
        return false;
    });

    function other_validates() {
        if($( "#tdocument" ).val()=="") {
            $( "#tdocument-error" ).html( "<p>El tipus de document &eacute;s obligatori</p>" );
            $( "#tdocument-error" ).css('display', 'block');
            return false;
        }
        else {
            $( "#tdocument-error" ).css('display', 'none');
            switch ($("#tdocument").val()) {
                case "1":
                    if (($('#dni_number').val() == "") || ($('#dni_number').val() == numero)) {
                        $("#option1-error").html("<p>El n&uacute;mero del DNI / NIF &eacute;s obligatori</p>");
                        $("#option1-error").css('display', 'block');
                        return false;
                    }
                    if (($('#dni_letter').val() == "") || ($('#dni_letter').val() == lletra)) {
                        $("#option1-error").html("<p>La lletra del DNI / NIF &eacute;s obligatori</p>");
                        $("#option1-error").css('display', 'block');
                        return false;
                    }
                    if (!$.isNumeric($('#dni_number').val())) {
                        $("#option1-error").html("<p>El n&uacute;mero del DNI / NIF ha de ser un num&egrave;ric</p>");
                        $("#option1-err$('#dni_number').val()or").css('display', 'block');
                        return false;
                    }

                    if (($('#dni_number').val().length < 4) || ($('#dni_number').val().length > 8)) {
                        $("#option1-error").html("<p>El n&uacute;mero del DNI / NIF incorrecte</p>");
                        $("#option1-error").css('display', 'block');
                        return false;
                    } else {
                        while ($('#dni_number').val().charAt(0)=='0') {
                            $('#dni_number').val($('#dni_number').val().substring(1,$('#dni_number').val().length));
                        }

                    }

                    if (!$('#dni_letter').val().match(/^[a-zA-Z]+$/)) {
                        $("#option1-error").html("<p>La lletra del DNI / NIF &eacute;s incorrecta</p>");
                        $("#option1-error").css('display', 'block');
                        return false;
                    }
                    break;
                case "2":
                    if (($('#option2_passport_number').val() == "") || ($('#option2_passport_number').val() == numero_passaport)) {
                        $("#option2-error").html("<p>El n&uacute;mero del Passaport &eacute;s obligatori</p>");
                        $("#option2-error").css('display', 'block');
                        return false;
                    }
                    if (!$('#option2_passport_number').val().match(/^[a-zA-Z0-9]+$/)) {
                        $("#option2-error").html("<p>El n&uacute;mero del Passaport &eacute;s incorrecte</p>");
                        $("#option2-error").css('display', 'block');
                        return false;
                    }
                    break;

                case "3":
                    if (($('#tr_letter_1').val() == "") || ($('#tr_letter_1').val() == lletra)) {
                        $("#option3-error").html("<p>La primera lletra de la tarjeta de resid&egrave;ncia &eacute;s obligatori</p>");
                        $("#option3-error").css('display', 'block');
                        return false;
                    }
                    if ((!$('#tr_letter_1').val().match(/^[a-zA-Z]+$/))||(($('#tr_letter_1').val()!="x") && ($('#tr_letter_1').val()!="X") && ($('#tr_letter_1').val()!="y") && ($('#tr_letter_1').val()!="Y"))) {
                        $("#option3-error").html("<p>La primera lletra de la tarjeta de resid&egrave;ncia &eacute;s incorrecta</p>");
                        $("#option3-error").css('display', 'block');
                        return false;
                    }
                    if (!$.isNumeric($('#tr_number').val())) {
                        $("#option3-error").html("<p>El n&uacute;mero de la tarjeta de resid&egrave;ncia ha de ser un num&egrave;ric</p>");
                        $("#option3-error").css('display', 'block');
                        return false;
                    }
                    if ($('#tr_number').val().length > 8) {
                        $("#option3-error").html("<p>El n&uacute;mero de la tarjeta de resid&egrave;ncia &eacute;s incorrecta</p>");
                        $("#option3-error").css('display', 'block');
                        return false;
                    }

                    if (($('#tr_letter_2').val() == "") || ($('#tr_letter_2').val() == lletra)) {
                        $("#option3-error").html("<p>La segona lletra de la tarjeta de resid&egrave;ncia &eacute;s obligatori</p>");
                        $("#option3-error").css('display', 'block');
                        return false;
                    }

                    break;
            }
        }
        return true;
    }

    $('#tdocument').change(function() {
        erase_fields();
        switch($( "#tdocument" ).val()){
            case "1":
                $( "#tdocument-error" ).css('display', 'none');
                $('.option1').css('display', 'block');
                if($('#dni_number').val()=="")
                    $('#dni_number').val("N\u00FAmero");
                if($('#dni_letter').val()=="")
                    $('#dni_letter').val('Lletra');
                $('.option2').css('display', 'none');
                $('.option3').css('display', 'none');
                break;
            case "2":
                $( "#tdocument-error" ).css('display', 'none');
                $('.option1').css('display', 'none');
                $('.option2').css('display', 'block');
                $('.option3').css('display', 'none');
                break;
            case "3":
                $( "#tdocument-error" ).css('display', 'none');
                $('.option1').css('display', 'none');
                $('.option2').css('display', 'none');
                $('.option3').css('display', 'block');
                break;
            default:
                $('.option1').css('display', 'none');
                $('.option2').css('display', 'none');
                $('.option3').css('display', 'none');
                break;
        }
    });

    $('input:text').click(function() {
        if(($(this).val()==numero)||($(this).val()==lletra)||($(this).val()==numero_passaport))
            $(this).val('');
    });

    function consulta() {
        var parametros = {
            "TDOCUMENT" : $("#tdocument").val(),
            "DNI_NUMBER" : $("#dni_number").val(),
            "DNI_LETTER" : $("#dni_letter").val(),
            "OPTION2_PASSPORT_NUMBER": $("#option2_passport_number").val(),
            "TR_LETTER_1": $("#tr_letter_1").val(),
            "TR_NUMBER": $("#tr_number").val(),
            "TR_LETTER_2": $("#tr_letter_2").val()
        };
        $.ajax({
            data:  parametros,
            url:   'search.php',
            type:  'post',
            beforeSend: function () {
                $("#result").html("Processant consulta, esperi si us plau...");
            },
            success:  function (response) {
                if(response=="error") {
                    $("#result").html('');
                    $("#result").css('display', 'none');
                    $("#no-result").css('display', 'inline-block');
                }
                else {
                    $("#result").html('La vostra mesa de consulta est\u00e0 ubicada a <div id="local">'+response+'</div>');

                    switch(response) {
                        case 'Centre C\u00EDvic de Ferreries':
                            $("#result").append("<div class='adreca'>Pla\u00E7a Mestre Moncl\u00FAs, 6 <br/>43500 Tortosa</div>");
                            break;
                        case 'Teatre Auditori Felip Pedrell':
                            $("#result").append("<div class='adreca'>Passeig de Ribera, 11 <br/>43500 Tortosa</div>");
                            break;
                        case 'Ajuntament':
                            $("#result").append("<div class='adreca'>Pl d'Espanya, 1 <br/>43500 Tortosa</div>");
                            break;
                        case 'Sala d\'usos m\u00FAltiples del Grup del Temple':
                            $("#result").append("<div class='adreca'>Bosch Fustegueres, 2 <br/>43500 Tortosa</div>");
                            break;
                        case 'Aula Did\u00e0ctica del Museu':
                            $("#result").append("<div class='adreca'>Rambla Felip Pedrell, 3 <br/>43500 Tortosa</div>");
                            break;
                        case 'CEIP Daniel Mangran\u00E9':
                            $("#result").append("<div class='adreca'>Ctra. Roquetes, 18 <br/>43590 Jes\u00FAs</div>");
                            break;
                        case 'CEIP dels Reguers':
                            $("#result").append("<div class='adreca'>C/ de la Rioja, 9 <br/>43527 Els Reguers</div>");
                            break;
                        case 'EMD de Campred\u00F3':
                            $("#result").append("<div class='adreca'>C/ Rafael Escardo Valls, 48 <br/>43897 Campred\u00F3</div>");
                            break;
                        case 'Consell Comarcal del Baix Ebre':
                            $("#result").append("<div class='adreca'>C/ Barcelona, 152 <br/>43500 Tortosa</div>");
                            break;
                        case 'CEIP B\u00EDtem':
                            $("#result").append("<div class='adreca'>C/ dels Hereus Gassol, 37 <br/>43510 Tortosa</div>");
                            break;
                        case 'CEIP de Vinallop':
                            $("#result").append("<div class='adreca'>C/ Santa Tecla, 12 <br/>43517 Vinallop</div>");
                            break;
                    }
                    $("#result").append("<div class='mes_info'>Per a m\u00e9s informaci\u00f3:<div><ul><li>Trucant al tel\u00e8fon 977 585 805.</li> <li>Per correu electr\u00f2nic <a href=\"mailto:sac.tortosa@tortosa.cat\">sac.tortosa@tortosa.cat</a>.</li> <li>Presencialment al Servei d'Atenci\u00f3 al Ciutad\u00e0 (SAC) municipal, en horari de dilluns a divendres de 9 a 14 hores i dijous de 16.30h a 18.30 hores.</li></div></div>");
                }
                if (isMobile.matches) {
                // You are in mobile browser
                    $('html, body').animate({scrollTop: '+=250px'}, 800);
                }
            }
        });
    }

    function erase_fields() {
        $('#dni_number').val(numero);
        $('#dni_letter').val(lletra);
        $('#option2_passport_number').val(numero_passaport);
        $('#tr_letter_1').val(lletra);
        $('#tr_number').val(numero);
        $('#tr_letter_2').val(lletra);
    }

    function erase_results() {
        $("#result").html('');
        $("#no-result").css('display', 'none');
    }

    function captcha(){
        var v1 = $("input#recaptcha_challenge_field").val();
        var v2 = $("input#recaptcha_response_field").val();

        $.ajax({
            type: "POST",
            url: "verify-captcha.php",
            data: {
                "recaptcha_challenge_field" : v1,
                "recaptcha_response_field" : v2
            },
            dataType: "text",
            error: function(){
                Recaptcha.reload();
            },
            success: function(data){
                if(data==1) {

                    consulta();
                    Recaptcha.reload();
                }
                else {
                    $("#recaptcha-error").html("<p>El codi de verificaci&oacute; humana &eacute;s incorrecte</p>");
                    $("#recaptcha-error").css('display', 'block');
                    Recaptcha.reload();
                }
            }
        });
    }
});