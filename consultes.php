<?
namespace webtortosa;

require_once("config.cfg");
require_once(FOLDER_CONTROLLER . "/web_controller.php");


//se instancia al controlador
$controller = new web_controller();

switch ($_GET['accio']) {
    case 'participacio_totals':
        $controller->consultesParticipacioTotals($_GET);
        break;
    case 'participacio_meses':
        $controller->consultesParticipacioMeses($_GET);
        break;
    case 'resultats_totals':
        $controller->consultesResultatsTotals($_GET);
        break;
    case 'resultats_meses':
        $controller->consultesResultatsMeses($_GET);
        break;
    default:
        $controller->consultesParticipacioTotals($_GET);
        break;
}
?>