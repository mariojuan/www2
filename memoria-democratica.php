<?
namespace webtortosa;

require_once("config.cfg");
require_once(FOLDER_CONTROLLER . "/web_controller.php");


//se instancia al controlador
$controller = new web_controller();

switch ($_REQUEST['accio']) {
    case "list":
        $controller->getListMemoriaData($_GET, $_POST);
        break;
    case "file":
        $controller->getFileItem($_GET, $_POST);
        break;
    default:
        $controller->getMemoriaData($_GET, $_POST);
        break;
}
?>