<html>
	<head>

        {include file="head.tpl"}

	</head>
	<body>

		{include file="header.tpl"}

		<div id="page-wrap">
    <div id="conttranstotalcos">
        <div id="conttranscos">
            <div id="btranspresen">
                <div id="transimatge">
                    <img src={$image} />
                </div>
                <div id="transimatgeText">
                    <h1>
                        {$LABEL_TITOL1}
                    </h1>
                </div>
            </div>

            <div id="tcentre_municipi">

                <font size="4" color="#666666">{$LABEL_TC0}</font>

                <p>{$LABEL_TC1}</p>
                <p>{$LABEL_TC2}</p>
                <p>{$LABEL_TC3}</p>
                <p>{$LABEL_TC4}</p>
                <p>{$LABEL_TC5}</p>
                <p>{$LABEL_TC6}</p>

                <ul>
                {foreach $EMDS as $item}
                  <li><a href="{$item['link']}" target="{$item['target']}">{$item['name']}</a></li>    
                {/foreach}
                </ul>

                <p>{$LABEL_TC7}</p>

                <ul>
                {foreach $CONVENIS as $item}
                    <li><a href="{$item['link']}" target="{$item['target']}">{$item['name']}</a></li>
                {/foreach}
                </ul>

            </div>

            <div class="separator"></div>
        </div>
    </div>
</div>

		{include file="footer.tpl"}

	</body>
</html>