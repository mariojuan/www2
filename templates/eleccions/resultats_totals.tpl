{include file="templates/header.tpl"}
<div id="page-wrap" class="negocis eleccions">
    <div id="contajttotalcos">
        <div id="conttranscos">
            <div id="bciutatpresen">
                <div id="ciutatimatge">
                    <img src="/images/negocis/cap.jpg" />
                </div>
                <div id="negocisimatgeText" class="imatgeCapText">
                    <h1>
                        {$LABEL_TITOL}
                        &nbsp;&nbsp;
                    </h1>
                </div>
            </div>

            {include file="templates/eleccions/menu_eleccions.tpl"}
            <div id="tcentre_planA">
                <!--
                <div id="title">
                    <input type="button" name="imprimir" value="Imprimir" id="printer" class="print" onclick="PrintElem('#tcentre_planA', 'resultats_totals')">
                </div>
                <div id="logos">
                    <div>
                        {if $logo_eleccions!=""}
                            <img src="templates/eleccions/images/{$logo_eleccions}">
                        {else}
                            {$title}
                        {/if}
                    </div>
                    <div>
                        <img src="templates/eleccions/images/logo-ajuntament.jpg">
                    </div>
                </div>
                -->
                <div id="section_title">
                    {$title}
                    {if $subtitle}
                        <br/><span id="text_dades_informatives">{$subtitle}</span>
                    {/if}
                </div>
                {if isset($taula1)}
                <div>
                    <table class="data_table">
                        <tr>
                            {assign var='titols' value = $taula1['0']}
                            {foreach $titols as $titol}
                                {if $titol@key==0}
                                    <th>{$titol}</th>
                                {else}
                                    <th colspan="4">{$titol}</th>
                                {/if}
                            {/foreach}
                        </tr>
                        {foreach $taula1 as $valors}
                            {if $valors@key>0}
                                <tr>
                                {foreach $valors as $valor}
                                    {if $valors@key==3}
                                        <td class="td_color">{$valor}</td>
                                    {elseif $valors@key==1 OR $valors@key==2}
                                        {if $valor@key>0}
                                            <td colspan="4">{$valor}</td>
                                        {else}
                                            <td>{$valor}</td>
                                        {/if}
                                    {elseif $valors@key==6 OR $valors@key==9}
                                        <td class="td_color">{$valor}</td>
                                    {else}
                                        <td>{$valor}</td>
                                    {/if}
                                {/foreach}
                                </tr>
                            {/if}
                        {/foreach}
                    </table>
                </div>
                {/if}

                {if isset($taula2)}
                <div>
                    <table class="data_table">
                        <tr>
                            {assign var='titols' value = $taula2['0']}
                            {foreach $titols as $titol}
                            <th>{$titol}</th>
                            {/foreach}
                        </tr>
                        {foreach $taula2 as $valors}
                            <tr>
                            {if $valors@key>0}
                                {foreach $valors as $valor}
                                    <td>{$valor}</td>
                                {/foreach}
                            {/if}
                            </tr>
                        {/foreach}
                    </table>
                </div>
                {/if}

                {$chart1}
                <div id="chart-1">

                </div>
                {$chart2}
                <div id="chart-2">

                </div>

                {$chart3}
                <div id="chart-3">

                </div>

                {$chart4}
                <div id="chart-4">

                </div>
            </div>
        </div>
	</div>
	{include file="templates/footer.tpl"}
</div>