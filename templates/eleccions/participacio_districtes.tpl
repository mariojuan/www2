{include file="templates/header.tpl"}
<div id="page-wrap" class="negocis eleccions">
    <div id="contajttotalcos">
        <div id="conttranscos">
            <div id="bciutatpresen">
                <div id="ciutatimatge">
                    <img src="/images/negocis/cap.jpg" />
                </div>
                <div id="negocisimatgeText" class="imatgeCapText">
                    <h1>
                        {$LABEL_TITOL}
                        &nbsp;&nbsp;
                    </h1>
                </div>
            </div>
            {include file="templates/eleccions/menu_eleccions.tpl"}
            <div id="tcentre_planA">
                <!--
                <div id="title">
                <input type="button" name="imprimir" value="Imprimir" id="printer" class="print" onclick="PrintElem('#tcentre_planA', 'participacio_districtes')">
                </div>
                <div id="logos">
                    <div>
                        {if $logo_eleccions!=""}
                            <img src="templates/eleccions/images/{$logo_eleccions}">
                        {else}
                            {$title}
                        {/if}
                    </div>
                    <div>
                        <img src="templates/eleccions/images/logo-ajuntament.jpg">
                    </div>
                </div>
                -->
                <div id="section_title">
                    {$title}
                    {if $subtitle}
                        <br/><span id="text_dades_informatives">{$subtitle}</span>
                    {/if}
                </div>
                {$chart1}
                <div id="chart-1">

                </div>
                {$chart2}
                <div id="chart-2">

                </div>
                {if isset($taula1)}
                <div>
                    <table class="data_table">
                        <tr>
                            {assign var='titols' value = $taula1['0']}
                            {foreach $titols as $titol}
                            <th colspan="4">{$titol}</th>
                            {/foreach}
                        </tr>
                        {foreach $taula1 as $valors}
                            <tr>
                            {if $valors@key>0}
                                {foreach $valors as $valor}
                                    {if (($valors@key==1 && $valor@key==1) || ($valors@key==2 && $valor@key==1))}
                                        <td colspan="3">{$valor}</td>
                                    {else}
                                        <td>{$valor}</td>
                                    {/if}
                                {/foreach}
                            {/if}
                            </tr>
                        {/foreach}
                    </table>
                </div>
                {/if}

                {if isset($taula2)}
                <div>
                    <table class="data_table">
                        <tr>
                            {assign var='titols' value = $taula2['0']}
                            {foreach $titols as $titol}
                                <th colspan="4">{$titol}</th>
                            {/foreach}
                        </tr>
                        {foreach $taula2 as $valors}
                            <tr>
                            {if $valors@key>0}
                                {foreach $valors as $valor}
                                    {if (($valors@key==1 && $valor@key==1) || ($valors@key==2 && $valor@key==1))}
                                        <td colspan="3">{$valor}</td>
                                    {else}
                                        <td>{$valor}</td>
                                    {/if}
                                {/foreach}
                            {/if}
                            </tr>
                        {/foreach}
                    </table>
                </div>
                {/if}
                {if isset($taula3)}
                <div>
                    <table class="data_table">
                        <tr>
                            {assign var='titols' value = $taula3['0']}
                            {foreach $titols as $titol}
                                <th colspan="4">{$titol}</th>
                            {/foreach}
                        </tr>
                        {foreach $taula3 as $valors}
                            <tr>
                            {if $valors@key>0}
                                {foreach $valors as $valor}
                                    {if (($valors@key==1 && $valor@key==1) || ($valors@key==2 && $valor@key==1))}
                                        <td colspan="3">{$valor}</td>
                                    {else}
                                        <td>{$valor}</td>
                                    {/if}
                                {/foreach}
                            {/if}
                            </tr>
                        {/foreach}
                    </table>
                </div>
                {/if}
                {if isset($taula4)}
                <div>
                    <table class="data_table">
                        <tr>
                            {assign var='titols' value = $taula4['0']}
                            {foreach $titols as $titol}
                                <th colspan="4">{$titol}</th>
                            {/foreach}
                        </tr>
                        {foreach $taula4 as $valors}
                            <tr>
                            {if $valors@key>0}
                                {foreach $valors as $valor}
                                    {if (($valors@key==1 && $valor@key==1) || ($valors@key==2 && $valor@key==1))}
                                        <td colspan="3">{$valor}</td>
                                    {else}
                                        <td>{$valor}</td>
                                    {/if}
                                {/foreach}
                            {/if}
                            </tr>
                        {/foreach}
                    </table>
                </div>
                {/if}
            </div>
        </div>
    </div>
</div>
{include file="templates/footer.tpl"}