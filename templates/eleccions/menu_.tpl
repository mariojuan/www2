{if $MENU|@count>0}
	<div id="menu_planA">
		<select name="eleccions" id="eleccions">
			{foreach from=$eleccions key=key item=value}
				{if $value->IdEleccio==$id_eleccio}
					<option value="{$value->IdEleccio}" selected>{$value->Denominacio}</option>
				{else}
					<option value="{$value->IdEleccio}">{$value->Denominacio}</option>
				{/if}
			{/foreach}	
		</select>
		{foreach from=$MENU key=key item=value}
			{if $key!='Electes 2015'}
				{if $value[0]==""}
					<p class="titol_menu">{$key}</p>
				{elseif $value[1]!=""}
					<p>- <a href="{$value[0]}" target="{$value[1]}">{$key}</a></p>
				{else}
					<p>- <a href="{$value[0]}">{$key}</a></p>
				{/if}
			{else}
				{if $id_eleccio=='2015MUN'}
					<p>- <a href="{$value[0]}">{$key}</a></p>
				{/if}
			{/if}
		{/foreach}
	</div>
{/if}