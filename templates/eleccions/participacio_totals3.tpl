<div id="conttranstotalcos">
	{include file="templates/header_web_antiga3.tpl"}
	<div id="conttranscos">
		{include file="templates/menu_web_antiga3.tpl"}
		<div id="tcentre_planA">
			<div id="title">
				<input type="button" name="imprimir" value="Imprimir" id="printer" class="print" onclick="PrintElem('#tcentre_planA', 'participacio_totals')">
			</div>
			<div id="logos">
				<div>
                    {if $logo_eleccions!=""}
					<img src="templates/eleccions/images/{$logo_eleccions}">
                    {else}
                        {$title}
                    {/if}
				</div>
				<div>
					<img src="templates/eleccions/images/logo-ajuntament.jpg">
				</div>
			</div>
			<div id="section_title">
                {$title}
                {if $subtitle}				
                    <br/><span id="text_dades_informatives">{$subtitle}</span>
                {/if}
            </div>
			{if isset($taula1)}
			<div>
				<table class="data_table">
					<tr>
						{assign var='titols' value = $taula1['0']}
						{foreach $titols as $titol}
						<th>{$titol}</th>
						{/foreach}
					</tr>
					{foreach $taula1 as $valors}
						<tr>
						{if $valors@key>0}
							{foreach $valors as $valor}
								<td>{$valor}</td>
							{/foreach}
						{/if}
						</tr>
					{/foreach}
				</table>
			</div>
			{/if}
			
			<br>
			<br>

			{if isset($taula2)}
			<div>
				<table class="data_table">
					<tr>
						{assign var='titols' value = $taula2['0']}
						{foreach $titols as $titol}
							{if $titol@key==0}
								<th>{$titol}</th>
							{else}
								<th colspan="2">{$titol}</th>
							{/if}
						{/foreach}
					</tr>
					{foreach $taula2 as $valors}
						<tr>
						{if $valors@key>0}
							{foreach $valors as $valor}
								<td>{$valor}</td>
							{/foreach}
						{/if}
						</tr>
					{/foreach}
				</table>
			</div>
			{/if}

			<div id="chart-1" class="chart">
                <div class="caption">{$CaptionChart1}</div>
                <div class="subcaption">{$SubCaptionChart1}</div>
                {$chart1}
			</div>

		</div>
	</div>
	{include file="templates/footer_web_antiga.tpl"}
</div>
</body>
</html>