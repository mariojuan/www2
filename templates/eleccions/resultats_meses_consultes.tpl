<div id="conttranstotalcos">
	{include file="templates/header_web_antiga.tpl"}
	<div id="conttranscos">
		{include file="templates/menu_web_antiga.tpl"}
		<div id="tcentre_planA">
			<div id="title">
				<input type="button" name="imprimir" value="Imprimir" id="printer" class="print" onclick="PrintElem('#tcentre_planA', 'participacio_districtes')">
			</div>
            <div id="logos">
                <div>
                    {if $logo_eleccions!=""}
                        <img src="templates/eleccions/images/{$logo_eleccions}">
                    {else}
                        {$title}
                    {/if}
                </div>
                <div>
                    <img src="templates/eleccions/images/logo-ajuntament.jpg">
                </div>
            </div>
            <div id="section_title">
                {$title}
                {if $subtitle}
                    <br/><span id="text_dades_informatives">{$subtitle}</span>
                {/if}
            </div>
			<div id="selector">
				<select name="meses" id="meses" onchange="JavaScript:window.location.href='consultes.php?accio=resultats_meses&meses='+this.options[this.selectedIndex].value;">
					<option value="">-- Selecionar --</option>
					{foreach $data_seccions as $item_seccio}
						{assign var="districte_seccio_mesa" value="{$item_seccio->Districte}|{$item_seccio->Seccio}|{$item_seccio->Mesa}"}
						{if $num_mesa==$districte_seccio_mesa}
							<option value="{$item_seccio->Districte}|{$item_seccio->Seccio}|{$item_seccio->Mesa}" selected>{$item_seccio->Mesa} - {$item_seccio->NomColegi}</option>
						{else}
							<option value="{$item_seccio->Districte}|{$item_seccio->Seccio}|{$item_seccio->Mesa}">{$item_seccio->Mesa} - {$item_seccio->NomColegi}</option>
						{/if}
					{/foreach}
				</select>
			</div>
			{if isset($taula1)}
			<div>
				<table class="data_table">
					<tr>
						{assign var='titols' value = $taula1['0']}
						{foreach $titols as $titol}
						<th colspan="3">{$titol}</th>
						{/foreach}
					</tr>
					{foreach $taula1 as $valors}
						<tr>
						{if $valors@key>0}
							{foreach $valors as $valor}
								{if $valor=="Partit" OR $valor=="Vots" OR $valor=="% s/ mesa"}
									<td class="td_color">{$valor}</td>
							    {else}
							    	<td>{$valor}</td>
							    {/if}									
							{/foreach}
						{/if}
						</tr>
					{/foreach}
				</table>
			</div>
			{/if}

			{$chart1}
			<div id="chart-1" class="chart">
				
			</div>
		</div>
	</div>
	{include file="templates/footer_web_antiga.tpl"}
</div>