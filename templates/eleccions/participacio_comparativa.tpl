{include file="templates/header.tpl"}
<div id="page-wrap" class="negocis eleccions">

    <div id="contajttotalcos">
        <div id="conttranscos">
            <div id="bciutatpresen">
                <div id="ciutatimatge">
                    <img src="/images/negocis/cap.jpg" />
                </div>
                <div id="negocisimatgeText" class="imatgeCapText">
                    <h1>
                        {$LABEL_TITOL}
                        &nbsp;&nbsp;
                    </h1>
                </div>
            </div>

            {include file="templates/eleccions/menu_eleccions.tpl"}
            <div id="tcentre_planA">
                <!--
                <div id="title">
                    <input type="button" name="imprimir" value="Imprimir" id="printer" class="print" onclick="PrintElem('#tcentre_planA', 'participacio_comparativa')">
                </div>
                <div id="logos">
                    <div>
                        {if $logo_eleccions!=""}
                            <img src="templates/eleccions/images/{$logo_eleccions}">
                        {else}
                            {$title}
                        {/if}
                    </div>
                    <div>
                        <img src="templates/eleccions/images/logo-ajuntament.jpg">
                    </div>
                </div>
                -->
                <div id="section_title">
                    {if $apartat=="eleccions"}
                        <div>
                        {$lblText01}:
                        <select name="eleccions_a_comparar" id="eleccions_a_comparar">
                            {foreach from=$eleccions key=key item=value}
                                {if $value->IdEleccio==$idelecciocomparativa}
                                    <option value="{$value->IdEleccio}" selected>{$value->Denominacio}</option>
                                {else}
                                    <option value="{$value->IdEleccio}">{$value->Denominacio}</option>
                                {/if}
                            {/foreach}
                        </select>
                        </div>
                    {/if}
                    {$title}
                    {if $subtitle}
                        <br/><span id="text_dades_informatives">{$subtitle}</span>
                    {/if}
                </div>
                {if isset($taula1)}
                <div>
                    <table class="data_table">
                        <tr>
                            {assign var='titols' value = $taula1['0']}
                            {foreach $titols as $titol}
                            <th>{$titol}</th>
                            {/foreach}
                        </tr>
                        {foreach $taula1 as $valors}
                            <tr>
                            {if $valors@key>0}
                                {foreach $valors as $valor}
                                    <td>{$valor}</td>
                                {/foreach}
                            {/if}
                            </tr>
                        {/foreach}
                    </table>
                </div>
                {/if}

                {if isset($taula2)}
                <div>
                    <table class="data_table">
                        <tr>
                            {assign var='titols' value = $taula2['0']}
                            {foreach $titols as $titol}
                                {if $titol@key==0}
                                    <th>{$titol}</th>
                                {else}
                                    <th colspan="2">{$titol}</th>
                                {/if}
                            {/foreach}
                        </tr>
                        {foreach $taula2 as $valors}
                            <tr>
                            {if $valors@key>0}
                                {foreach $valors as $valor}
                                    <td>{$valor}</td>
                                {/foreach}
                            {/if}
                            </tr>
                        {/foreach}
                    </table>
                </div>
                {/if}

                <div id="chart-1" class="chart">
                    <div class="caption">{$CaptionChart1}</div>
                    <div class="subcaption">{$SubCaptionChart1}</div>
                    {$chart1}
                </div>

                <div id="chart-2" class="chart">
                    <div class="caption">{$CaptionChart2}</div>
                    <div class="subcaption">{$SubCaptionChart2}</div>
                    {$chart2}
                </div>

                <div id="chart-3" class="chart">
                    <div class="caption">{$CaptionChart3}</div>
                    <div class="subcaption">{$SubCaptionChart3}</div>
                    {$chart3}
                </div>
            </div>
        </div>
	</div>
	{include file="templates/footer.tpl"}
</div>