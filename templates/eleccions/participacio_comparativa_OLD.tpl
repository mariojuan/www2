<div id="conttranstotalcos">
	{include file="templates/header.tpl"}
	<div id="conttranscos">
		{include file="templates/menu.tpl"}
		<div id="tcentre_planA">
			<div id="title">
				<input type="button" name="imprimir" value="Imprimir" id="printer" class="print" onclick="PrintElem('#tcentre_planA', 'participacio_totals')">
			</div>
			<div id="logos">
				<div>
					<img src="templates/eleccions/images/eleccions-municipals-2015.jpg">
				</div>
				<div>
					<img src="templates/eleccions/images/logo-ajuntament.jpg">
				</div>
			</div>
			<div id="section_title">{$title}</div>
			{if isset($taula1)}
			<div>
				<table class="data_table">
					<tr>
						{assign var='titols' value = $taula1['0']}
						{foreach $titols as $titol}
						<th>{$titol}</th>
						{/foreach}
					</tr>
					{foreach $taula1 as $valors}
						<tr>
						{if $valors@key>0}
							{foreach $valors as $valor}
								<td>{$valor}</td>
							{/foreach}
						{/if}
						</tr>
					{/foreach}
				</table>
			</div>
			{/if}

			{if isset($taula2)}
			<div>
				<table class="data_table">
					<tr>
						{assign var='titols' value = $taula2['0']}
						{foreach $titols as $titol}
							{if $titol@key==0}
								<th>{$titol}</th>
							{else}
								<th colspan="2">{$titol}</th>
							{/if}
						{/foreach}
					</tr>
					{foreach $taula2 as $valors}
						<tr>
						{if $valors@key>0}
							{foreach $valors as $valor}
								<td>{$valor}</td>
							{/foreach}
						{/if}
						</tr>
					{/foreach}
				</table>
			</div>
			{/if}
			{$chart1}
			<div id="chart-1">
				
			</div>
			{$chart2}
			<div id="chart-2">
				
			</div>

			{$chart3}
			<div id="chart-3">
				
			</div>
		</div>
	</div>
	{include file="templates/footer.tpl"}
</div>