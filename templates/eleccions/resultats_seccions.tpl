{include file="templates/header.tpl"}
<div id="page-wrap" class="negocis eleccions">
    <div id="contajttotalcos">
        <div id="conttranscos">
            <div id="bciutatpresen">
                <div id="ciutatimatge">
                    <img src="/images/negocis/cap.jpg" />
                </div>
                <div id="negocisimatgeText" class="imatgeCapText">
                    <h1>
                        {$LABEL_TITOL}
                        &nbsp;&nbsp;
                    </h1>
                </div>
            </div>

            {include file="templates/eleccions/menu_eleccions.tpl"}
            <div id="tcentre_planA">
                <!--
                <div id="title">
                    <input type="button" name="imprimir" value="Imprimir" id="printer" class="print" onclick="PrintElem('#tcentre_planA', 'participacio_districtes')">
                </div>
                <div id="logos">
                    <div>
                        {if $logo_eleccions!=""}
                            <img src="templates/eleccions/images/{$logo_eleccions}">
                        {else}
                            {$title}
                        {/if}
                    </div>
                    <div>
                        <img src="templates/eleccions/images/logo-ajuntament.jpg">
                    </div>
                </div>
                -->
                <div id="section_title">
                    {$title}
                    {if $subtitle}
                        <br/><span id="text_dades_informatives">{$subtitle}</span>
                    {/if}
                </div>
                <div id="selector">
                    <select name="seccions" id="seccions" onchange="JavaScript:window.location.href='eleccions.php?accio=resultats_seccions&seccions='+this.options[this.selectedIndex].value;">
                        <option value="">-- Selecionar --</option>
                        {foreach $data_seccions as $item_seccio}
                            {assign var="districte_seccio" value="{$item_seccio->Districte}|{$item_seccio->Seccio}"}
                            {if $num_seccio==$districte_seccio}
                                <option value="{$item_seccio->Districte}|{$item_seccio->Seccio}" selected>Districte {$item_seccio->Districte} - Seccio {$item_seccio->Seccio}</option>
                            {else}
                                <option value="{$item_seccio->Districte}|{$item_seccio->Seccio}">Districte {$item_seccio->Districte} - Seccio {$item_seccio->Seccio}</option>
                            {/if}
                        {/foreach}
                    </select>
                </div>
                {if isset($taula1)}
                <div>
                    <table class="data_table">
                        <tr>
                            {assign var='titols' value = $taula1['0']}
                            {foreach $titols as $titol}
                            <th colspan="3">{$titol}</th>
                            {/foreach}
                        </tr>
                        {foreach $taula1 as $valors}
                            <tr>
                            {if $valors@key>0}
                                {foreach $valors as $valor}
                                    {if $valor=="Partit" OR $valor=="Vots" OR $valor=="% s/ secció"}
                                        <td class="td_color">{$valor}</td>
                                    {else}
                                        <td>{$valor}</td>
                                    {/if}
                                {/foreach}
                            {/if}
                            </tr>
                        {/foreach}
                    </table>
                </div>
                {/if}

                {$chart1}
                <div id="chart-1" class="chart">

                </div>
            </div>
        </div>
	</div>
	{include file="templates/footer.tpl"}
</div>