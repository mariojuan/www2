<html>
<head>
    {include file="head.tpl"}
    <script src='https://www.google.com/recaptcha/api.js?hl={$lang}'></script>
    <script type="text/javascript">
        var captcha_value = false;
        $(document).ready(function() {
            //Afegim anys al select
            for(i=1900; i<={'Y'|date}; i++) {
                {if $any}
                if(i!={$any})
                    $('#any').append('<option value="'+i+'">'+i+'</option>');
                else
                   $('#any').append('<option value="'+i+'" selected>'+i+'</option>');
                {else}
                $('#any').append('<option value="'+i+'">'+i+'</option>');
                {/if}
            }

            $("#registre_telematic_form").validate({
                rules: {
                    tdocument:  {
                        required: true
                    },
                    primer_cognom: {
                        required: true,
                        maxlength: 30
                    },
                    numero_persones_empadronades: {
                        required: true
                    },
                    nucli: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true,
                        maxlength: 50
                    },
                    confirmacio_email: {
                        required: true,
                        email: true,
                        equalTo: "#email",
                        maxlength: 50
                    },
                    dia: {
                        required: true
                    },
                    mes: {
                        required: true
                    },
                    any: {
                        required: true
                    }
                },
                messages: {
                    tdocument:  {
                        required: "{$LABEL_ERROR_FIELD}"
                    },
                    primer_cognom: {
                        required: "{$LABEL_ERROR_FIELD}",
                        maxlength: "{$LABEL_FIELD_6_1}",
                        lettersonly: "{$LABEL_FIELD_6_3}"
                    },
                    numero_persones_empadronades: {
                        required: "{$LABEL_ERROR_FIELD}"
                    },
                    nucli: {
                        required: "{$LABEL_ERROR_FIELD}"
                    },
                    email: {
                        required: "{$LABEL_ERROR_FIELD}",
                        email: "{$LABEL_ERROR_FIELD_3}",
                        maxlength: "{$LABEL_FIELD_11_1}"
                    },
                    confirmacio_email: {
                        required: "{$LABEL_ERROR_FIELD}",
                        email: "{$LABEL_ERROR_FIELD_3}",
                        equalTo: "{$LABEL_ERROR_FIELD_4}",
                        maxlength: "{$LABEL_FIELD_11_1}"
                    },
                    dia: {
                        required: "{$LABEL_ERROR_FIELD}"
                    },
                    mes: {
                        required: "{$LABEL_ERROR_FIELD}"
                    },
                    any: {
                        required: "{$LABEL_ERROR_FIELD}"
                    }
                },
                submitHandler: function(form) {
                    captcha();
                    if(captcha_value) {
                        form.submit();
                    }
                }
            });
            jQuery.validator.addMethod("lettersonly", function(value, element) {
                return this.optional(element) || /^[a-z]+$/i.test(value);
            }, "Letters only please");
            $('#tdocument').change(function() {
                switch($( "#tdocument" ).val()){
                    case "1":
                        buida_camps();
                        $('.option0').css('display', 'none');
                        $( "#tdocument-error" ).css('display', 'none');
                        $('.option1').css('display', 'block');
                        $('.option2').css('display', 'none');
                        $('.option3').css('display', 'none');
                        $('#dni_number').rules("add",
                        {
                            required: true,
                            number: true,
                            maxlength: 8,
                            messages: {
                                required: "{$LABEL_ERROR_FIELD}",
                                number: "{$LABEL_FIELD_2_3}"
                            }
                        });
                        $('#dni_letter').rules("add",
                        {
                            required: true,
                            lettersonly: true,
                            maxlength: 1,
                            messages: {
                                required: "{$LABEL_ERROR_FIELD}",
                                lettersonly: "{$LABEL_FIELD_2_4}"
                            }
                        });
                    break;
                    case "2":
                        buida_camps();
                        $( "#tdocument-error" ).css('display', 'none');
                        $('.option1').css('display', 'none');
                        $('.option2').css('display', 'block');
                        $('.option3').css('display', 'none');
                        $('#option2_passport_number').rules("add",
                        {
                            required: true,
                            messages: {
                                required: "{$LABEL_ERROR_FIELD}"
                            }
                        });
                    break;
                    case "3":
                        buida_camps();
                        $( "#tdocument-error" ).css('display', 'none');
                        $('.option1').css('display', 'none');
                        $('.option2').css('display', 'none');
                        $('.option3').css('display', 'block');
                        $('#tr_letter_1').rules("add",
                        {
                            required: true,
                            messages: {
                                required: "{$LABEL_ERROR_FIELD}"
                            }
                        });
                        $('#tr_number').rules("add",
                        {
                            required: true,
                            messages: {
                                required: "{$LABEL_ERROR_FIELD}"
                            }
                        });
                        $('#tr_letter_2').rules("add",
                        {
                            required: true,
                            messages: {
                                required: "{$LABEL_ERROR_FIELD}"
                            }
                        });
                    break;
                    default:
                        buida_camps();
                        $('.option1').css('display', 'none');
                        $('.option2').css('display', 'none');
                        $('.option3').css('display', 'none');
                    break;
                }
            });
        });

        function captcha() {
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    var out = this.responseText;
                    if(out=='1') {
                        captcha_value = true;
                    }
                    else {
                        $('#recaptcha-error').css('display', 'block');
                    }
                }
            };
            xmlhttp.open("POST", "https://www2.tortosa.cat/libs/verify-recaptcha.php", false);
            xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            xmlhttp.send("g-recaptcha-response="+grecaptcha.getResponse());
        }
        function buida_camps() {
            $('#dni_number').val('');
            $('#dni_letter').val('');
            $('#option2_passport_number').val('');
            $('#tr_letter_1').val('');
            $('#tr_number').val('');
            $('#tr_letter_2').val('');
        }
    </script>
</head>
<body>
    {include file="header.tpl"}
    <div id="page-wrap">
        <div class="contenedor-responsive">
            <div id="conttranstotalcos">
                <div id="conttranscos">
                    <div id="btranspresen">
                        <div id="transimatge">
                            <img src="/images/pciutadana/tira.jpg" class="img-slider-territori"/>
                        </div>
                        <div id="transimatgeText">
                            <h1>
                                {$LABEL_TITOL1}
                            </h1>
                        </div>
                    </div>
                    <div class="subtitolinfeco subtitol_generic" id="subtitolinfeco2">
                        {$LABEL_TITOL1b}
                        <a href="{$URL_PPART_BACK}">
                            <i class="icon-angle-double-up"></i>
                        </a>
                    </div>
                    <div class="formulari_generic">
                        <h2>{$LABEL_TITOL2}</h2>
                        {if $message==""}
                        <p></p>
                        <!--<p class="title">{$TITLE_1}</p>-->
                        <form id="registre_telematic_form" method="post" action="index.php?lang={$lang}" novalidate="novalidate" enctype="multipart/form-data">
                            <p>
                                <label>{$LABEL_FIELD_1} *</label>
                                <select name="tdocument" id="tdocument" placeholder="{$LABEL_FIELD_1}">
                                    <option value="">{$LABEL_FIELD_1_1}</option>
                                    <option value="1" {if $tdocument=="1"}selected{/if}>DNI</option>
                                    <option value="2" {if $tdocument=="2"}selected{/if}>{$LABEL_FIELD_3_1}</option>
                                    <option value="3" {if $tdocument=="3"}selected{/if}>NIE</option>
                                </select>
                            </p>
                            <p class="option1" {if $tdocument=="1"}style="display:block"{/if}>
                                <label>NIF *</label>
                                <span>
                                    {$LABEL_FIELD_2_1} <span style="font-size: 10px; vertical-align: middle">{$LABEL_FIELD_2_5}</span><br/>
                                    <input type="text" id="dni_number" name="dni_number" value="{$dni_number}" maxlength="8">
                                </span>
                                <span>
                                    {$LABEL_FIELD_2_2}<br/>
                                    <input type="text" id="dni_letter" name="dni_letter" value="{$dni_letter}" maxlength="1">
                                </span>
                                 <label id="option1-error" class="error" for="option1-error" style="display: none;"></label>
                            </p>
                            <p class="option2" {if $tdocument=="2"}style="display:block"{/if}>
                                <label>{$LABEL_FIELD_3_1} *</label>
                                <span>
                                    <input type="text" id="option2_passport_number" name="option2_passport_number" value="{$option2_passport_number}" maxlength="12">
                                </span>
                                <label id="option2-error" class="error" for="option2-error" style="display: none;"></label>
                            </p>
                            <p class="option3" {if $tdocument=="3"}style="display:block"{/if}>
                                <label>{$LABEL_FIELD_4_1} *</label>
                                <span>
                                    {$LABEL_FIELD_2_2}<br>
                                    <input type="text" id="tr_letter_1" name="tr_letter_1" value="{$tr_letter_1}" maxlength="1">
                                </span>
                                <span>
                                    {$LABEL_FIELD_2_1}<br>
                                    <input type="text" id="tr_number" name="tr_number" value="{$tr_number}" maxlength="8">
                                </span>
                                <span>
                                    {$LABEL_FIELD_2_2}<br>
                                    <input type="text" id="tr_letter_2" name="tr_letter_2" value="{$tr_letter_2}" maxlength="1">
                                </span>
                                <label id="option3-error" class="error" for="option3-error" style="display: none;"></label>
                            </p>
                            <p>
                                <label>{$LABEL_FIELD_5} *</label>
                                <span>
                                    <!--<input type="text" name="data_naixement" id="data_naixement" value="{$data_naixement}">-->
                                    <select name="dia" id="dia">
                                        <option value="">-- {$LABEL_FIELD_5_1} --</option>
                                        <option value="1" {if $dia==1}selected{/if}>1</option>
                                        <option value="2" {if $dia==2}selected{/if}>2</option>
                                        <option value="3" {if $dia==3}selected{/if}>3</option>
                                        <option value="4" {if $dia==4}selected{/if}>4</option>
                                        <option value="5" {if $dia==5}selected{/if}>5</option>
                                        <option value="6" {if $dia==6}selected{/if}>6</option>
                                        <option value="7" {if $dia==7}selected{/if}>7</option>
                                        <option value="8" {if $dia==8}selected{/if}>8</option>
                                        <option value="9" {if $dia==9}selected{/if}>9</option>
                                        <option value="10" {if $dia==10}selected{/if}>10</option>
                                        <option value="11" {if $dia==11}selected{/if}>11</option>
                                        <option value="12" {if $dia==12}selected{/if}>12</option>
                                        <option value="13" {if $dia==13}selected{/if}>13</option>
                                        <option value="14" {if $dia==14}selected{/if}>14</option>
                                        <option value="15" {if $dia==15}selected{/if}>15</option>
                                        <option value="16" {if $dia==16}selected{/if}>16</option>
                                        <option value="17" {if $dia==17}selected{/if}>17</option>
                                        <option value="18" {if $dia==18}selected{/if}>18</option>
                                        <option value="19" {if $dia==19}selected{/if}>19</option>
                                        <option value="20" {if $dia==20}selected{/if}>20</option>
                                        <option value="21" {if $dia==21}selected{/if}>21</option>
                                        <option value="22" {if $dia==22}selected{/if}>22</option>
                                        <option value="23" {if $dia==23}selected{/if}>23</option>
                                        <option value="24" {if $dia==24}selected{/if}>24</option>
                                        <option value="25" {if $dia==25}selected{/if}>25</option>
                                        <option value="26" {if $dia==26}selected{/if}>26</option>
                                        <option value="27" {if $dia==27}selected{/if}>27</option>
                                        <option value="28" {if $dia==28}selected{/if}>28</option>
                                        <option value="29" {if $dia==29}selected{/if}>29</option>
                                        <option value="30" {if $dia==30}selected{/if}>30</option>
                                        <option value="31" {if $dia==31}selected{/if}>31</option>
                                    </select>
                                </span>
                                <span>
                                    <select name="mes" id="mes">
                                        <option value="">-- {$LABEL_FIELD_5_2} --</option>
                                        <option value="01" {if $mes==1}selected{/if}>{$LABEL_FIELD_5_2_1}</option>
                                        <option value="02" {if $mes==2}selected{/if}>{$LABEL_FIELD_5_2_2}</option>
                                        <option value="03" {if $mes==3}selected{/if}>{$LABEL_FIELD_5_2_3}</option>
                                        <option value="04" {if $mes==4}selected{/if}>{$LABEL_FIELD_5_2_4}</option>
                                        <option value="05" {if $mes==5}selected{/if}>{$LABEL_FIELD_5_2_5}</option>
                                        <option value="06" {if $mes==6}selected{/if}>{$LABEL_FIELD_5_2_6}</option>
                                        <option value="07" {if $mes==7}selected{/if}>{$LABEL_FIELD_5_2_7}</option>
                                        <option value="08" {if $mes==8}selected{/if}>{$LABEL_FIELD_5_2_8}</option>
                                        <option value="09" {if $mes==9}selected{/if}>{$LABEL_FIELD_5_2_9}</option>
                                        <option value="010" {if $mes==10}selected{/if}>{$LABEL_FIELD_5_2_10}</option>
                                        <option value="011" {if $mes==11}selected{/if}>{$LABEL_FIELD_5_2_11}</option>
                                        <option value="012" {if $mes==12}selected{/if}>{$LABEL_FIELD_5_2_12}</option>
                                    </select>
                                </span>
                                <span>
                                    <select name="any" id="any">
                                        <option value="">-- {$LABEL_FIELD_5_3} --</option>
                                    </select>
                                </span>
                            </p>
                            <p>
                                <label>{$LABEL_FIELD_6} *</label>
                                <span style="font-size:12px">{$LABEL_FIELD_6_2}</span>
                                <span>
                                    <input type="text" name="primer_cognom" id="primer_cognom" value="{$primer_cognom}" maxlength="50">
                                </span>
                            </p>
                            <p>
                                <label>{$LABEL_FIELD_8} *</label>
                                <span>
                                    <select name="nucli" id="nucli">
                                        <option value="">-- {$LABEL_FIELD_8_1} --</option>
                                        {foreach $nuclis as $item_nucli}
                                            {if $item_nucli->ID!=0}
                                            <option value="{$item_nucli->ID}" {if $item_nucli->ID==$nucli}selected{/if}>{$item_nucli->DESCRIPCIO}</option>
                                            {else}
                                                <option value="{$item_nucli->ID}" {if $item_nucli->ID==$nucli}selected{/if}>{$LABEL_FIELD_8_2}</option>
                                            {/if}
                                        {/foreach}
                                    </select>
                                </span>
                            </p>
                            <p class="title">{$TITLE_2}</p>
                            <p>
                                <label>{$LABEL_FIELD_11} *</label>
                                <span>
                                    <input type="text" name="email" id="email" autocomplete="off" value="{$email}">
                                </span>
                            </p>
                            <p>
                                <label>{$LABEL_FIELD_12} *</label>
                                <span>
                                    <input type="text" name="confirmacio_email" id="confirmacio_email" autocomplete="off">
                                </span>
                            </p>
                            <p>
                                {$LABEL_FIELD_10}
                            <div class="g-recaptcha" data-sitekey="6Lfdwh4TAAAAAKajPw-1Rj8msRd8YVZx14Vwova-"></div>
                            <label id="recaptcha-error">{$LABEL_ERROR_FIELD_2}</label>
                            </p>
                            <p class="lopd-text">
                                {$LOPD_TEXT}
                            </p>
                            <p style="font-size: 12px">{$LABEL_1}</p>
                            <p style="font-size: 12px">{$LABEL_TEXT_3}</p>
                            <p>
                                <input type="submit" name="enviar" id="enviar" value="{$LABEL_SUBMIT}">
                            </p>
                            <p style="font-size: 12px">
                                {$LABEL_TEXT_2}
                            </p>
                        </form>
                        {else}
                            {if $message=="missatge_validacio"}
                                <p class="ppart_titol_resposta">{$MISSATGE_VALIDACIO_1}</p>
                                <p>{$MISSATGE_VALIDACIO_2}</p>
                                <p>{$MISSATGE_VALIDACIO_3}</p>
                                <p>{$MISSATGE_VALIDACIO_4}</p>
                                <p>{$MISSATGE_VALIDACIO_5}</p>
                                <p>{$MISSATGE_VALIDACIO_6} <a href="mailto:&#105;&#110;&#102;&#111;&#064;&#116;&#111;&#114;&#116;&#111;&#115;&#097;&#046;&#099;&#097;&#116;">&#105;&#110;&#102;&#111;&#064;&#116;&#111;&#114;&#116;&#111;&#115;&#097;&#046;&#099;&#097;&#116;</a></p>
                                <p>{$MISSATGE_VALIDACIO_7}</p>
                                <p>{$MISSATGE_VALIDACIO_8}</p>
                            {elseif $message=="missatge_confirmacio"}
                                <p>{$MISSATGE_CONFIRMACIO_1}</p>
                                <p>{$MISSATGE_CONFIRMACIO_2}</p>
                                <p>{$MISSATGE_CONFIRMACIO_3} <a href="https://www2.tortosa.cat/participacio-ciutadana/votacio-telematica/">{$LABEL_TEXT_1}</a></p>
                                <p>{$MISSATGE_CONFIRMACIO_4}</p>
                            {elseif $message=="missatge_no_confirmacio"}
                                <p>{$MISSATGE_NO_CONFIRMACIO_1}</p>
                                <p>{$MISSATGE_NO_CONFIRMACIO_2}</p>
                                <p>{$MISSATGE_NO_CONFIRMACIO_3} <a href="mailto:&#105;&#110;&#102;&#111;&#064;&#116;&#111;&#114;&#116;&#111;&#115;&#097;&#046;&#099;&#097;&#116;">&#105;&#110;&#102;&#111;&#064;&#116;&#111;&#114;&#116;&#111;&#115;&#097;&#046;&#099;&#097;&#116;</a> {$MISSATGE_NO_CONFIRMACIO_4}</p>
                            {elseif $message=="missatge_inscrit_anteriorment_i_no_validat"}
                                <p>{$MISSATGE_REGISTRAT_ANTERIORMENT_1}</p>
                                <p>{$MISSATGE_REGISTRAT_ANTERIORMENT_2}</p>
                                <p>{$MISSATGE_REGISTRAT_ANTERIORMENT_3}</p>
                            {elseif $message=="missatge_inscrit_anteriorment_i_validat"}
                                <p>{$MISSATGE_REGISTRAT_ANTERIORMENT_4}</p>
                                <p>{$MISSATGE_REGISTRAT_ANTERIORMENT_5}</p>
                                <p>{$MISSATGE_REGISTRAT_ANTERIORMENT_6}</p>
                            {elseif $message=="missatge_inscrit_anteriorment_validat_i_votat"}
                                <p>{$MISSATGE_REGISTRAT_ANTERIORMENT_7}</p>
                                <p>{$MISSATGE_REGISTRAT_ANTERIORMENT_8}</p>
                            {elseif $message=="missatge_no_censat"}
                                <p>{$MISSATGE_NO_CENS_1}</p>
                                <p>{$MISSATGE_NO_CONFIRMACIO_3} <a href="mailto:&#105;&#110;&#102;&#111;&#064;&#116;&#111;&#114;&#116;&#111;&#115;&#097;&#046;&#099;&#097;&#116;">&#105;&#110;&#102;&#111;&#064;&#116;&#111;&#114;&#116;&#111;&#115;&#097;&#046;&#099;&#097;&#116;</a> {$MISSATGE_NO_CONFIRMACIO_4}</p>
                                <p>
                                    <form name="registre_telematic_back" action="index.php?lang={$lang}" method="post">
                                    <input type="hidden" name="tdocument" id="tdocument" value="{$tdocument}">
                                    <input type="hidden" name="dni_number" id="dni_number" value="{$dni_number}">
                                    <input type="hidden" name="dni_letter" id="dni_letter" value="{$dni_letter}">
                                    <input type="hidden" name="option2_passport_number" id="option2_passport_number" value="{$option2_passport_number}">
                                    <input type="hidden" name="tr_letter_1" id="tr_letter_1" value="{$tr_letter_1}">
                                    <input type="hidden" name="tr_number" id="tr_number" value="{$tr_number}">
                                    <input type="hidden" name="tr_letter_2" id="tr_letter_2" value="{$tr_letter_2}">
                                    <input type="hidden" name="dia" id="dia" value="{$dia}">
                                    <input type="hidden" name="mes" id="dia" value="{$mes}">
                                    <input type="hidden" name="any" id="dia" value="{$any}">
                                    <input type="hidden" name="primer_cognom" id="primer_cognom" value="{$primer_cognom}">
                                    <input type="hidden" name="nucli" id="nucli" value="{$nucli}">
                                    <input type="hidden" name="nom_carrer" id="nom_carrer" value="{$nom_carrer}">
                                    <input type="hidden" name="email" id="email" value="{$email}">
                                    <input type="hidden" name="telefon" id="telefon" value="{$telefon}">
                                    <input type="hidden" name="back" id="back" value="1">
                                    <input type="submit" name="tornar" id="tornar" value="{$LABEL_TORNAR}">
                                    </form>
                                </p>
                            {elseif $message=="probrema_dades_cens"}
                                <p>{$MISSATGE_NO_CENS_1}</p>
                                <p>{$MISSATGE_NO_CONFIRMACIO_2} <a href="mailto:&#105;&#110;&#102;&#111;&#064;&#116;&#111;&#114;&#116;&#111;&#115;&#097;&#046;&#099;&#097;&#116;">&#105;&#110;&#102;&#111;&#064;&#116;&#111;&#114;&#116;&#111;&#115;&#097;&#046;&#099;&#097;&#116;</a></p>
                            {elseif $message=="missatge_registrat_amb_mateix_email"}
                                <p>{$MISSATGE_REGISTRAT_AMB_MATEIX_EMAIL_1}</p>
                                <p>{$MISSATGE_REGISTRAT_AMB_MATEIX_EMAIL_2}</p>
                                <p>{$MISSATGE_REGISTRAT_AMB_MATEIX_EMAIL_3}</p>
                                <p>
                                <form name="registre_telematic_back" action="index.php?lang={$lang}" method="post">
                                    <input type="hidden" name="tdocument" id="tdocument" value="{$tdocument}">
                                    <input type="hidden" name="dni_number" id="dni_number" value="{$dni_number}">
                                    <input type="hidden" name="dni_letter" id="dni_letter" value="{$dni_letter}">
                                    <input type="hidden" name="option2_passport_number" id="option2_passport_number" value="{$option2_passport_number}">
                                    <input type="hidden" name="tr_letter_1" id="tr_letter_1" value="{$tr_letter_1}">
                                    <input type="hidden" name="tr_number" id="tr_number" value="{$tr_number}">
                                    <input type="hidden" name="tr_letter_2" id="tr_letter_2" value="{$tr_letter_2}">
                                    <input type="hidden" name="dia" id="dia" value="{$dia}">
                                    <input type="hidden" name="mes" id="dia" value="{$mes}">
                                    <input type="hidden" name="any" id="dia" value="{$any}">
                                    <input type="hidden" name="primer_cognom" id="primer_cognom" value="{$primer_cognom}">
                                    <input type="hidden" name="nucli" id="nucli" value="{$nucli}">
                                    <input type="hidden" name="nom_carrer" id="nom_carrer" value="{$nom_carrer}">
                                    <input type="hidden" name="email" id="email" value="{$email}">
                                    <input type="hidden" name="telefon" id="telefon" value="{$telefon}">
                                    <input type="hidden" name="back" id="back" value="1">
                                    <input type="submit" name="tornar" id="tornar" value="{$LABEL_TORNAR}">
                                </form>
                                </p>
                            {/if}
                        {/if}
                    </div>
                </div>
            </div>
        </div>
    </div>
    {include file="footer.tpl"}
</body>
</html>