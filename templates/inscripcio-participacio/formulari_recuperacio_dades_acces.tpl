<html>
<head>
    {include file="head.tpl"}
    <script src='https://www.google.com/recaptcha/api.js?hl={$lang}'></script>
    <script type="text/javascript">
        var captcha_value = false;
        $(document).ready(function() {
            $("#registre_telematic_form").validate({
                rules: {
                    tdocument:  {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true,
                        maxlength: 50
                    },
                    confirmacio_email: {
                        required: true,
                        email: true,
                        equalTo: "#email",
                        maxlength: 50
                    }
                },
                messages: {
                    tdocument:  {
                        required: "{$LABEL_ERROR_FIELD}"
                    },
                    email: {
                        required: "{$LABEL_ERROR_FIELD}",
                        email: "{$LABEL_ERROR_FIELD_3}",
                        maxlength: "{$LABEL_FIELD_11_1}"
                    },
                    confirmacio_email: {
                        required: "{$LABEL_ERROR_FIELD}",
                        email: "{$LABEL_ERROR_FIELD_3}",
                        equalTo: "{$LABEL_ERROR_FIELD_4}",
                        maxlength: "{$LABEL_FIELD_11_1}"
                    }
                },
                submitHandler: function(form) {
                    captcha();
                    if(captcha_value) {
                        form.submit();
                    }
                }
            });
            jQuery.validator.addMethod("lettersonly", function(value, element) {
                return this.optional(element) || /^[a-z]+$/i.test(value);
            }, "Letters only please");

            $('#tdocument').change(function() {
                switch($( "#tdocument" ).val()){
                    case "1":
                        buida_camps();
                        $('.option0').css('display', 'none');
                        $( "#tdocument-error" ).css('display', 'none');
                        $('.option1').css('display', 'block');
                        $('.option2').css('display', 'none');
                        $('.option3').css('display', 'none');
                        $('#dni_number').rules("add",
                        {
                            required: true,
                            number: true,
                            maxlength: 8,
                            messages: {
                                required: "{$LABEL_ERROR_FIELD}",
                                number: "{$LABEL_FIELD_2_3}"
                            }
                        });
                        $('#dni_letter').rules("add",
                        {
                            required: true,
                            lettersonly: true,
                            maxlength: 1,
                            messages: {
                                required: "{$LABEL_ERROR_FIELD}",
                                lettersonly: "{$LABEL_FIELD_2_4}"
                            }
                        });
                    break;
                    case "2":
                        buida_camps();
                        $( "#tdocument-error" ).css('display', 'none');
                        $('.option1').css('display', 'none');
                        $('.option2').css('display', 'block');
                        $('.option3').css('display', 'none');
                        $('#option2_passport_number').rules("add",
                        {
                            required: true,
                            messages: {
                                required: "{$LABEL_ERROR_FIELD}"
                            }
                        });
                    break;
                    case "3":
                        buida_camps();
                        $( "#tdocument-error" ).css('display', 'none');
                        $('.option1').css('display', 'none');
                        $('.option2').css('display', 'none');
                        $('.option3').css('display', 'block');
                        $('#tr_letter_1').rules("add",
                        {
                            required: true,
                            messages: {
                                required: "{$LABEL_ERROR_FIELD}"
                            }
                        });
                        $('#tr_number').rules("add",
                        {
                            required: true,
                            messages: {
                                required: "{$LABEL_ERROR_FIELD}"
                            }
                        });
                        $('#tr_letter_2').rules("add",
                        {
                            required: true,
                            messages: {
                                required: "{$LABEL_ERROR_FIELD}"
                            }
                        });
                    break;
                    default:
                        buida_camps();
                        $('.option1').css('display', 'none');
                        $('.option2').css('display', 'none');
                        $('.option3').css('display', 'none');
                    break;
                }
            });
        });
        function captcha() {
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    var out = this.responseText;
                    if(out=='1') {
                        captcha_value = true;
                    }
                    else {
                        $('#recaptcha-error').css('display', 'block');
                    }
                }
            };
            xmlhttp.open("POST", "https://www2.tortosa.cat/libs/verify-recaptcha.php", false);
            xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            xmlhttp.send("g-recaptcha-response="+grecaptcha.getResponse());
        }
        function buida_camps() {
            $('#dni_number').val('');
            $('#dni_letter').val('');
            $('#option2_passport_number').val('');
            $('#tr_letter_1').val('');
            $('#tr_number').val('');
            $('#tr_letter_2').val('');
        }
    </script>
</head>
<body>
    {include file="header.tpl"}
    <div id="page-wrap">
        <div class="contenedor-responsive">
            <div id="conttranstotalcos">
                <div id="conttranscos">
                    <div id="btranspresen">
                        <div id="transimatge">
                            <img src="/images/pciutadana/tira.jpg" class="img-slider-territori"/>
                        </div>
                        <div id="transimatgeText">
                            <h1>
                                {$LABEL_TITOL1}
                            </h1>
                        </div>
                    </div>
                    <div class="formulari_generic">
                        <h2>{$LABEL_TITOL3}</h2>
                        {if $message==""}
                        <p></p>
                        <!--<p>{$TITLE_3}</p>-->
                        <form id="registre_telematic_form" method="post" action="recuperacio_dades_acces.php?lang={$lang}" novalidate="novalidate" enctype="multipart/form-data">
                            <p>
                                <label>{$LABEL_FIELD_1} *</label>
                                <select name="tdocument" id="tdocument" placeholder="{$LABEL_FIELD_1}">
                                    <option value="">{$LABEL_FIELD_1}</option>
                                    <option value="1">DNI</option>
                                    <option value="2">Passaport</option>
                                    <option value="3">NIE</option>
                                </select>
                            </p>
                            <p class="option1">
                                <label>NIF *</label>
                                <span>
                                    {$LABEL_FIELD_2_1} <span style="font-size: 10px; vertical-align: middle">{$LABEL_FIELD_2_5}</span><br/>
                                    <input type="text" id="dni_number" name="dni_number" value="" maxlength="8">
                                </span>
                                <span>
                                    {$LABEL_FIELD_2_2}<br/>
                                    <input type="text" id="dni_letter" name="dni_letter" value="" maxlength="1">
                                </span>
                                 <label id="option1-error" class="error" for="option1-error" style="display: none;"></label>
                            </p>
                            <p class="option2">
                                <label>{$LABEL_FIELD_3_1} *</label>
                                <span>
                                    <input type="text" id="option2_passport_number" name="option2_passport_number" value="{$txt_passport_number}">
                                </span>
                                <label id="option2-error" class="error" for="option2-error" style="display: none;"></label>
                            </p>
                            <p class="option3">
                                <label>{$LABEL_FIELD_4_1} *</label>
                                <span>
                                    {$LABEL_FIELD_2_2}<br>
                                    <input type="text" id="tr_letter_1" name="tr_letter_1" value="" maxlength="1">
                                </span>
                                <span>
                                    {$LABEL_FIELD_2_1}<br>
                                    <input type="text" id="tr_number" name="tr_number" value="" maxlength="8">
                                </span>
                                <span>
                                    {$LABEL_FIELD_2_2}<br>
                                    <input type="text" id="tr_letter_2" name="tr_letter_2" value="" maxlength="1">
                                </span>
                                <label id="option3-error" class="error" for="option3-error" style="display: none;"></label>
                            </p>
                            <p>
                                <label>{$LABEL_FIELD_11} * <span style="font-size: 10px; vertical-align: middle">{$LABEL_FIELD_11_2}</span></label>
                                <span>
                                    <input type="text" name="email" id="email" autocomplete="off">
                                </span>
                            </p>
                            <p>
                                <label>{$LABEL_FIELD_12} *</label>
                                <span>
                                    <input type="text" name="confirmacio_email" id="confirmacio_email" autocomplete="off">
                                </span>
                            </p>
                            <p>
                                {$LABEL_FIELD_10}
                            <div class="g-recaptcha" data-sitekey="6Lfdwh4TAAAAAKajPw-1Rj8msRd8YVZx14Vwova-"></div>
                            <label id="recaptcha-error">{$LABEL_ERROR_FIELD_2}</label>
                            </p>
                            <p style="font-size: 12px">{$LABEL_1}</p>
                            <p style="font-size: 12px">{$LABEL_TEXT_3}</p>
                            <p>
                                <input type="submit" name="enviar" id="enviar" value="{$LABEL_SUBMIT_1}">
                            </p>
                            <p style="font-size: 12px">
                                {$LABEL_TEXT_2}
                            </p>
                        </form>
                        {else}
                            {if $message=="rda_missatge_confirmacio"}
                                <p>{$MISSATGE_RECUPERACIO_CONFIRMACIO_1}</p>
                                <p>{$MISSATGE_RECUPERACIO_CONFIRMACIO_2}</p>
                                <p>{$MISSATGE_RECUPERACIO_CONFIRMACIO_3} <a href="http://www2.tortosa.cat/participacio-ciutadana/votacio-telematica/">{$LABEL_TEXT_1}</a></p>
                            {elseif $message=="rda_no_validat"}
                                <p>{$MISSATGE_NO_RECUPERACIO_4}</p>
                                <p>{$MISSATGE_NO_RECUPERACIO_5} <a href="mailto:&#105;&#110;&#102;&#111;&#064;&#116;&#111;&#114;&#116;&#111;&#115;&#097;&#046;&#099;&#097;&#116;">&#105;&#110;&#102;&#111;&#064;&#116;&#111;&#114;&#116;&#111;&#115;&#097;&#046;&#099;&#097;&#116;</a></p>
                                <p>{$MISSATGE_NO_RECUPERACIO_6}</p>
                            {elseif $message=="rda_error_recuperacio"}
                                <p>{$MISSATGE_NO_RECUPERACIO_1}</p>
                                <p>{$MISSATGE_NO_RECUPERACIO_2}</p>
                                <p>{$MISSATGE_NO_RECUPERACIO_3} <a href="mailto:&#105;&#110;&#102;&#111;&#064;&#116;&#111;&#114;&#116;&#111;&#115;&#097;&#046;&#099;&#097;&#116;">&#105;&#110;&#102;&#111;&#064;&#116;&#111;&#114;&#116;&#111;&#115;&#097;&#046;&#099;&#097;&#116;</a></p>
                            {/if}

                        {/if}
                    </div>
                </div>
            </div>
        </div>
    </div>
    {include file="footer.tpl"}
</body>
</html>