<html>
   <head>
      {include file="head.tpl"}
      <link href="/css/estils_headlines.css" rel="stylesheet" type="text/css">
   </head>
   <body id="headlines_list">
      {include file="header.tpl"}
      <div id="page-wrap">
         <div class="contenedor-responsive">
            <div id="conttranstotalcos">
               <div id="conttranscos">
                  <div id="btranspresen">
                     <div id="transimatge"> 
                        <img src="{$tiraPartit}" class="img-slider-territori"/>         
                     </div>
                     <div id="transimatgeText">
                        <h1>
                           {$LABEL_TITOL1}
                           <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                        </h1>
                     </div>
                  </div>
                  <div class="subtitolinfeco" id="subtitolinfeco2">
                     {$LABEL_TITOL1b}
                  </div>
                  <div class="marcback">
                     <a href="/grups-politics/index.php?{$lang}">
                     <i class="icon-angle-double-up"></i>
                     </a>
                  </div>
                  <!-- Menu -->
                  {include file="grups-politics/grup-politic/menu.tpl"}
                  <!--// Menu -->
                  <!-- Taula compos -->
                  <div id="tcentre_planA" style="margin-left: -36px; margin-top: -6px;" class="correcio-grupPolitic-Articles">
                     <font size="4" color="#666666">{$LABEL_ARTICLES}</font>
                     <p>{$LABEL_noArticles}</p>
                     {if $items|@count>0}
                     {foreach $items as $item}
                     <div class="headline_item_list">
                        <div class="headline_item_img">
                           {if $item->FOTO|trim ==""}
                           <img src="/images/headlines/inotimage.jpg" width="220" height="125">
                           {else}
                           {if $item->IMPORT=="0"}
                           <img src="/images/headlines/min_{$item->FOTO}" width="220" height="125">
                           {else}
                           <img src="http://www.tortosa.cat/webajt/gestiointerna/grups_municipals/documentacio/{$item->FOTO}" width="220" height="125">
                           {/if}
                           {/if}
                        </div>
                        <div class="headline_item_data">{$item->DATA|date_format:"%d/%m/%G"}</div>
                        <div class="headline_title_item">{$item->TITOL|truncate:75:"":false}</div>
                        <div class="headline_subtitle_item"><span>{$tematiques[$item->ID]}</span> {$item->SUBTITOL|truncate:100:"...":false}</div>
                        <div class="headline_link_item"><a href="/grups-politics/article.php?lang={$lang}&id={$item->ID}&grup={$GRUP_POLITIC}">més informació</a></div>
                        <p><br></p>
                     </div>
                     {/foreach}
                     {else}
                     <div class="headline_no_data">
                        {$lblNoData}
                     </div>
                     {/if}
                     <div id="pagination">
                        <ul>
                           {if $lastpage>1}
                           {if $page> 1}
                           {assign var="page_ant" value=$page - 1}
                           <li><a href="/grups-politics/{$GRUP_POLITIC}/articles.php?lang={$lang}&page={$page_ant}">{$lblAnt}</a></li>
                           {/if}
                           {assign var="desp" value=$page + 4}
                           {assign var="firstpage" value=$page - 4}
                           {if $desp < $lastpage}
                           {assign var="lastpage" value=$page + 4}
                           {/if}
                           {if $firstpage < 1}
                           {assign var="firstpage" value=1}
                           {/if}
                           {for $counter=$firstpage to $lastpage}
                           {if $counter==$page}
                           <li>{$counter}</li>
                           {else}
                           <li><a href="/grups-politics/{$GRUP_POLITIC}/articles.php?lang={$lang}&page={$counter}">{$counter}</a></li>
                           {/if}
                           {/for}
                           {if $page < $lastpage - 1}
                           {assign var="page_seg" value=$page + 1}
                           <li><a href="/grups-politics/{$GRUP_POLITIC}/articles.php?lang={$lang}&page={$page_seg}">{$lblSeg}</a></li>
                           {/if}
                           {/if}
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      </div>
      <script>
         $().ready(function() {
             $(".search_di").datepicker();
             $(".search_df").datepicker();
         });
      </script>
      {include file="footer.tpl"}
   </body>
</html>