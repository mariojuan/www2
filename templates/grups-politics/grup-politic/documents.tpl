<html>
   <head>
      {include file="head.tpl"}
      <link href="/css/estils_headlines.css" rel="stylesheet" type="text/css">
   </head>
   <body id="gpdocuments">
      {include file="header.tpl"}
      <div id="page-wrap">

         <div class="contenedor-responsive">

            <div id="conttranstotalcos">

               <div id="conttranscos">

                  <div id="btranspresen">
                     <div id="transimatge">         
                        <img src="{$image}" class="img-slider-territori"/>          
                     </div>
                     <div id="transimatgeText">
                        <h1>
                           {$LABEL_TITOL1}
                        </h1>
                     </div>
                  </div>

                  <div class="subtitolinfeco" id="subtitolinfeco2">
                     {$LABEL_TITOL1b}
                  </div>

                  <div class="marcback">
                     <a href="/grups-politics/index.php">
                     <i class="icon-angle-double-up"></i>
                     </a>
                  </div>

                  <!-- Menu -->
                  {include file="grups-politics/grup-politic/menu.tpl"}
                  <!--// Menu -->
                  <!-- Taula compos -->
                  <!-- <div id="tcentre_planA" style="margin-left: -36px;"> -->
                  
                     

                     {if isset($elements)}

                     <table class="tableYves">
                        {foreach from=$elements key=key item=value}
                        <tr>
                           <td>
                              <a href="http://www.tortosa.cat/webajt/gestiointerna/grups_municipals/documentacio/{utf8_encode($value->FITXER)}" target="_blank" style="text-decoration:none; color:#000000">
                              <i class="icon-file-pdf"></i>
                              </a>
                           </td>
                           <td>
                              <a href="http://www.tortosa.cat/webajt/gestiointerna/grups_municipals/documentacio/{utf8_encode($value->FITXER)}" target="_blank" style="text-decoration:none; color:#000000">
                              {utf8_encode($value->DATA)|date_format:"%D"}
                              </a>
                           </td>
                           <td>
                              <a href="http://www.tortosa.cat/webajt/gestiointerna/grups_municipals/documentacio/{utf8_encode($value->FITXER)}" target="_blank" style="text-decoration:none; color:#000000">
                              {utf8_encode($value->DESCRIPCIOCURTA)}
                              </a>
                           </td>
                        </tr>
                        {/foreach}
                     </table>

                     {else}
                     {$LABEL_TC2}
                     {/if}
                     

                     <!--

                        <div id="taulaicorpo">
                           {if $elements|count>0}
                           {foreach from=$elements key=key item=value}
                           <div id="filaicorpo">
                              <div class="colicorpo ord-col1">
                                 <a href="http://www.tortosa.cat/webajt/gestiointerna/grups_municipals/documentacio/{utf8_encode($value->FITXER)}" target="_blank" style="text-decoration:none; color:#000000">
                                 <i class="icon-file-pdf"></i>
                                 </a>
                              </div>
                              <div class="colicorpo ord-col2">
                                 <a href="http://www.tortosa.cat/webajt/gestiointerna/grups_municipals/documentacio/{utf8_encode($value->FITXER)}" target="_blank" style="text-decoration:none; color:#000000">
                                 {utf8_encode($value->DATA)|date_format:"%D"}
                                 </a>
                              </div>
                              <div class="colicorpo ord-col3">
                                 <a href="http://www.tortosa.cat/webajt/gestiointerna/grups_municipals/documentacio/{utf8_encode($value->FITXER)}" target="_blank" style="text-decoration:none; color:#000000">
                                 {utf8_encode($value->DESCRIPCIOCURTA)}
                                 </a>
                              </div>
                           </div>
                           {/foreach}
                           {else} 
                           <div id="filaicorpo">
                              {$LABEL_TC2}
                           </div>
                           {/if}
                        </div>
                        
                        -->
                     <br>
                     <div id="pagination">
                        <ul>
                           {if $lastpage>1}
                           {if $page> 1}
                           {assign var="page_ant" value=$page - 1}
                           <li><a href="/grups-politics/documents.php?lang={$lang}&gp={$grup}&page={$page_ant}">{$lblAnt}</a></li>
                           {/if}
                           {assign var="desp" value=$page + 4}
                           {assign var="firstpage" value=$page - 4}
                           {if $desp < $lastpage}
                           {assign var="lastpage" value=$page + 4}
                           {/if}
                           {if $firstpage < 1}
                           {assign var="firstpage" value=1}
                           {/if}
                           {for $counter=$firstpage to $lastpage}
                           {if $counter==$page}
                           <li>{$counter}</li>
                           {else}
                           <li><a href="/grups-politics/documents.php?lang={$lang}&gp={$grup}&page={$counter}">{$counter}</a></li>
                           {/if}
                           {/for}
                           {if $page < $lastpage}
                           {assign var="page_seg" value=$page + 1}
                           <li><a href="/grups-politics/documents.php?lang={$lang}&gp={$grup}&page={$page_seg}">{$lblSeg}</a></li>
                           {/if}
                           {else}
                           <li>1</li>
                           {/if}
                        </ul>
                     </div>
                 
               </div>
            </div>
         </div>
      </div>
      <script>
         $().ready(function() {
             $(".search_di").datepicker();
             $(".search_df").datepicker();
         });
      </script>
      {include file="footer.tpl"}
   </body>
</html>