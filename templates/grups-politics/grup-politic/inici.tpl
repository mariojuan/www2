<html>
   <head>
      {include file="head.tpl"}
   </head>
   <body>
      {include file="header.tpl"}
      <div id="page-wrap">

         <div class="contenedor-responsive">

            <div id="conttranstotalcos">

               <div id="conttranscos">

                  <div id="btranspresen">

                     <div id="transimatge">         
                        <img src="{$tiraPartit}" class="img-slider-territori"/>         
                     </div>

                     <div id="transimatgeText">
                        <h1>
                           {$LABEL_TITOL1}
                           <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                        </h1>
                     </div>

                  </div>

                  <div class="subtitolinfeco" id="subtitolinfeco2">
                     {$LABEL_TITOL1b}
                  </div>

                  <div class="marcback">
                     <a href="/grups-politics/index.php?{$lang}">
                     <i class="icon-angle-double-up"></i>
                     </a>
                  </div>
                  
                  <!-- Menu -->
                  {include file="grups-politics/grup-politic/menu.tpl"}
                  <!--// Menu -->
                  <!-- Taula compos -->
                  <font size="4" color="#666666">{$LABEL_INICI}</font>
                  <div class="separator"></div>
                  <table class="tableYves">
                     <tr>
                     <thead>
                        <th>{$LABEL_NOM}</th>
                        <th>{$LABEL_CARREC}</th>
                        <th>{$LABEL_FITXA}</th>
                     </thead>
                     </tr>
                     {foreach $PERSONAL as $item}
                     <tr>
                        <td>{$item['name']}</td>
                        <td>{$item['occupation']}</td>
                        <td><a href="{$item['link']}" target="{$item['target']}"><i class="{$item['icon']}"></i></a></td>
                     </tr>
                     {/foreach}
                  </table>
               </div>
            </div>
         </div>
      </div>
      {include file="footer.tpl"}
   </body>
</html>