<html>
   <head>
      <title>
      </title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      {include file="head.tpl"}
   </head>
   <body>
      {include file="header.tpl"}
      <div id="page-wrap">
         <div class="contenedor-responsive">
            <div id="conttranstotalcos">
               <div id="conttranscos">
                  <div id="btranspresen">
                     <div id="transimatge">
                        <img src="/images/butlletins/tira.jpg" class="img-slider-territori"/>
                     </div>
                  </div>
                  <div id="transimatgeText">
                     <h1>
                        {$LABEL_TITOL1}
                     </h1>
                  </div>
                  <div class="caixadatanoticia">
                     {$LABEL_TC1} {$date_publish} {$LABEL_TC2} {$time_publish} h {$LABEL_TC3}
                  </div>
                  <div class='caixatextnoticia caixatextnoticia-mod'>
                     <div id='noticia_titol'>{$TITOL}</div>
                     <div id='noticia_subtitol'>{$SUBTITOL}</div>
                     <div id='noticia_cos'>{$COS}

                        <div class="noticia_foto">{$IMAGE}</div>
                        <div>
                        <h3>{$LABEL_TITOL3}</h3>
                        <ul>
                           {foreach $LABEL_TC_TIPOLOGIA as $tipologia_item}
                           <li>{$tipologia_item}</li>
                           {/foreach}
                        </ul>
                        <h3>{$LABEL_TITOL2}</h3>
                        <ul>
                           {foreach $LABEL_TC_TEMATICA as $tematica_item}
                           <li>{$tematica_item}</li>
                           {/foreach}
                        </ul>
                     </div>

                     </div>

                     <div id="buttons-container">
                        <!-- <input type="button" name="enrere" id="enrere" value="{$lblBack}" onclick="JavaScript:window.location.href='/ordenances/index.php?lang={$lang}&tipus={$tipus}&accio=list&page={$page}';"> -->
                        <input type="button" name="enrere" id="enrere" value="Tornar" onclick="JavaScript:window.location.href='/grups-politics/{$GRUP_POLITIC}/articles.php?lang={$lang}';">
                     </div>

                     <!--

                     <div class="div-tipologia-tematica-foto">

                     <div class="noticia_foto">{$IMAGE}</div>
                     <div id='sidebar'>
                        <h3>{$LABEL_TITOL3}</h3>
                        <ul>
                           {foreach $LABEL_TC_TIPOLOGIA as $tipologia_item}
                           <li>{$tipologia_item}</li>
                           {/foreach}
                        </ul>
                        <h3>{$LABEL_TITOL2}</h3>
                        <ul>
                           {foreach $LABEL_TC_TEMATICA as $tematica_item}
                           <li>{$tematica_item}</li>
                           {/foreach}
                        </ul>
                     </div>

                     </div>

                     -->

                  </div>
                  <div id='noticia_espaipeu'></div>
               </div>
            </div>
         </div>
      </div>
      {include file="footer.tpl"}
   </body>
</html>