<html>
   <head>
      {include file="head.tpl"}
   </head>
   <body>
      {include file="header.tpl"}
      <div id="page-wrap">

         <div class="contenedor-responsive">

         <div id="conttranstotalcos">
            <div id="conttranscos">
               <div id="btranspresen">
                  <div id="transimatge">
                     <img src="{$image}" class="img-slider-territori"/>
                  </div>
                  <div id="transimatgeText">
                     <h1>
                        {$LABEL_TITOL1}
                        <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                     </h1>
                  </div>
               </div>
               <ul id='menu_planA'>
                  {foreach $MENU as $itemMenu}
                  <a href={$itemMenu['link']} target="{$itemMenu['target']}">
                     <li id='menu_planA_item'>
                        {$itemMenu['name']}
                     </li>
                  </a>
                  {/foreach}
               </ul>
               <div id="tcentre_planA">
                  <font size="4" color="#666666">{$LABEL_TC0}</font>
                  <p>{$LABEL_TC1}</p>
                  <p>{$LABEL_TC2}</p>
                  <p>{$LABEL_TC3}</p>
                  <p>{$LABEL_TC4}</p>
                  <p>{$LABEL_TC5}</p>
                  <p>{$LABEL_TC6}</p>
                  <a href="http://www.tortosa.cat/webajt/ajunta/om/retribucions/AcordPle.pdf" target="_blank">{$LABEL_TC7}</a></p>
                  <p class="normativa"><strong>{$LABEL_TC8}</strong> {$LABEL_TC9}</p>
               </div>
               <div class="separator"></div>
            </div>
         </div>
         </div>
      </div>
      {include file="footer.tpl"}
   </body>
</html>