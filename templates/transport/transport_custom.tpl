<!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->
<div id="page-wrap">

   <div class="contenedor-responsive">

      <div id="conttranstotalcos">

         <div id="conttranscos">

            <div id="btranspresen">

               <div id="transimatge">        
                  <img src="{$image}" class="img-slider-territori"/>       
               </div>

               <div id="transimatgeText">
                  <h1>
                     {$LABEL_TITOL1}

                     <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>

                  </h1>
               </div>

            </div>

            <ul id='menu_planA'>
               {foreach $MENU as $itemMenu}
               <a href={$itemMenu}>
                  <li id='menu_planA_item'>
                     {$itemMenu@key}
                  </li>
               </a>
               {/foreach}
            </ul>

            <div id="tcentre_planA">
               <h2>{$LABEL_TC0}</h2>
               {if $item==1}
               <p>{$LABEL_TC1}</p>
               {elseif $item==2}
               <font size=2 color=#333333>
               <strong>
               {$LABEL_TC1_1}
               </strong>
               </font>
               <br><br>
               {$LABEL_TC1_11}
               <br><br>
               <iframe class="mapa-responsive" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1546236.9239863667!2d-0.5995881688499914!3d40.80612581051421!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a0e4a4887de549%3A0x4750f0d9aa735b5c!2sTortosa%2C+Tarragona!5e0!3m2!1ses!2ses!4v1488975737835" width="560" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
               <br><br>
               <br><br>
               <font size=2 color=#333333>
               <strong>
               {$LABEL_TC1_2}
               </strong>      
               </font><br><br>
               {$LABEL_TC1_21}
               <ul class="Negocis_comunica">
                  <li>{$LABEL_TC1_211}</li>
                  <ul class="Negocis_comunica">
                     <li>{$LABEL_TC1_2111}</li>
                     <li>{$LABEL_TC1_2112}</li>
                  </ul>
                  <li>{$LABEL_TC1_212}</li>
                  <ul class="Negocis_comunica">
                     <li>{$LABEL_TC1_2121}</li>
                     <li>{$LABEL_TC1_2122}</li>
                  </ul>
                  <li>{$LABEL_TC1_213}</li>
                  <ul class="Negocis_comunica">
                     <li>{$LABEL_TC1_2131}</li>
                  </ul>
               </ul>
               {$LABEL_TC1_22}
               <br>
               {$LABEL_TC1_22b}
               <ul class="Negocis_comunica">
                  <li>{$LABEL_TC1_221}</li>
                  <li>{$LABEL_TC1_222}</li>
                  <li>{$LABEL_TC1_223}</li>
                  <li>{$LABEL_TC1_224}</li>
               </ul>
               {$LABEL_TC1_23}
               <br>
               {$LABEL_TC1_23b}
               <ul class="Negocis_comunica">
                  <li>{$LABEL_TC1_231}</li>
                  <li>{$LABEL_TC1_232}</li>
                  <li>{$LABEL_TC1_233}</li>
                  <li>{$LABEL_TC1_234}</li>
                  <li>{$LABEL_TC1_235}</li>
               </ul>
               {elseif $item==3}
               <p>{$LABEL_TC1}</p>
               <p>{$LABEL_TC20}<a href="{$url_HIFE}" target="_blank"><strong>{$LABEL_TC23}</strong></a>{$LABEL_TC24}</p>
               <p>&nbsp;</p>
               <p><strong>{$LABEL_TC25}</strong></p>
               <p>{$LABEL_TC2}</p>
               <p><i class="icon-bus"></i><strong>{$LABEL_TC03}</strong>&nbsp;{$LABEL_TC3}</p>
               <p><i class="icon-bus"></i><strong>{$LABEL_TC04}</strong>&nbsp;{$LABEL_TC4}</p>
               <br>
               <p><strong>{$LABEL_TC5}</strong></p>
               <a href="/images/transport/tarifesbus.jpg" target="_blank">
               <img src="/images/transport/tarifesbus_thumb.jpg">
               </a>
               <p style="font-size: 12px"><i>{$LABEL_TC5B}</i></p>
               <br>
               <p><i class="icon-bus"></i><strong>{$LABEL_TC7}</strong></p>
               <p>{$LABEL_TC6}</p>
               <br>
               <p><i class="icon-bus"></i><strong>{$LABEL_TC9}</strong></p>
               <p>{$LABEL_TC8}</p>
               <p>&nbsp;</p>
               <p><i class="icon-link-ext"></i>&nbsp;<a href="http://www.hife.es/ca-ES/LineaUrbana/8" target="_blank">{$LABEL_TC11}</a></p>
               {elseif $item==4}
               <p>{$LABEL_TC1}<a href="http://rodalies.gencat.cat/web/.content/pdf/horaris/R16.pdf" target="_blank"><strong>{$LABEL_TC2}</strong></a></p>
               <iframe class="mapa-responsive" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3019.938599171274!2d0.5207483154912786!3d40.80734303971401!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a0e4a646d87403%3A0xb44f0d348c3d72fb!2sTortosa!5e0!3m2!1ses!2sus!4v1497026673550" width="550" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
               <p>&nbsp;</p>
               <p>{$LABEL_TC3}</p>
               <iframe class="mapa-responsive" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3022.385461557733!2d0.6121253154899503!3d40.75354604300956!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a10262697b8305%3A0xc9f56467c17cc7fa!2sL&#39;Aldea-Amposta-Tortosa!5e0!3m2!1ses!2sus!4v1497027305695" width="550" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
               {elseif $item==5}
               <p>{$LABEL_TC1}</p>
               <p><strong>{$LABEL_TC2}</strong></p>
               <ul class="Negocis_comunica">
                  <li>{$LABEL_TC3}</li>
                  <li>{$LABEL_TC4}</li>
                  <li>{$LABEL_TC5}</li>
               </ul>
               <p><strong>{$LABEL_TC6}</strong></p>
               <ul class="Negocis_comunica">
                  <li>{$LABEL_TC7}</li>
                  <li>{$LABEL_TC8}</li>
                  <li>{$LABEL_TC9}</li>
                  <li>{$LABEL_TC10}</li>
               </ul>
               {elseif $item==6}
               <p>{$LABEL_TC1}</p>
               <iframe class="mapa-responsive" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d12079.932140676306!2d0.5113682309220597!3d40.80636658650567!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a0e4a8c81a3485%3A0x7f4b9263313acc7b!2sAutocares+Hife!5e0!3m2!1ses!2sus!4v1497025654407" width="550" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
               {/if}       
               <!--<ul>
                  <li><a href="http://www.hife.es/ca-ES/LineaUrbana/8" target="_blank">{$LABEL_TC3}</a></li>
                  <li><a href="http://www.hife.es/ca-ES/Paginas/8" target="_blank">{$LABEL_TC4}</a></li>
                  </ul>
                  <br><br>
                  {$LABEL_TC2}
                  <ul>
                  <li>
                  <a href="http://rodalies.gencat.cat/web/.content/pdf/horaris/R16.pdf" target="_blank">{$LABEL_TC5}</a>
                  </li>
                  </ul>-->
            </div>

         </div>
      </div>

   </div>

</div>