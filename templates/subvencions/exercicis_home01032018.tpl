<!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->
<div id="page-wrap">
<div class="contenedor-responsive">
   <div id="conttranstotalcos">
      <div id="conttranscos">
         <div id="btranspresen">
            <div id="transimatge">			
               <img src="{$image}" class="img-slider-territori"/>			
            </div>
            <div id="transimatgeText">
               <h1>
                  {$LABEL_TITOL1}
                  <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
               </h1>
            </div>
         </div>
         <div class="subtitolinfeco" id="subtitolinfeco2">
            {$LABEL_TITOL1b}
         </div>
         <div class="marcback">
            <a href="{$url_exercici_back}">
            <i class="icon-angle-double-up"></i>
            </a>
         </div>
         <ul id='menu_planA'>
            {foreach $MENU as $itemMenu}
            <a href={$itemMenu}>
               <li id='menu_planA_item'>
                  {$itemMenu@key}
               </li>
            </a>
            {/foreach}
         </ul>
         <div id="tcentre_planA">
            <font size=4 color=#666666>
            {$LABEL_TITOL0}
            </font>
            {if $item==1 and $item2==1}
                <p>{$LABEL_TC1}</p>
            {else if $item==2 and $item2==1}
                <p>{$LABEL_TC1}</p>
            {else if $item==1 and $item2==2}
            <ul>
               <li><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/ajunta/subvencions/2013/BasesGenerals.pdf" target="_blank">{$LABEL_TC1}</a></li>
            </ul>
            {else if $item==1 and $item2==3}
            <!-- Subvencions 2017 -->
            <ul class="subvencions1">
               <li>
                  <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong>{$LABEL_TC2_1PUNT_0}</strong></p>
                  <p>{$LABEL_TC2_1TXT_0_1}</p>
               </li>
               <li>
                  <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong>{$LABEL_TC2_1PUNT_1}</strong></p>
                  <p>{$LABEL_TC2_1TXT_1}</p>
                  <p>{$LABEL_TC2_1TXT_2}</p>
                  <ul>
                     <li>{$LABEL_PUNT_1LI_1}</li>
                     <li>{$LABEL_PUNT_1LI_2}</li>
                     <li>{$LABEL_PUNT_1LI_3}</li>
                     <li>{$LABEL_PUNT_1LI_4}</li>
                  </ul>
               </li>
               <li>
                  <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong>{$LABEL_TC2_1PUNT_2}</strong></p>
                  <ul>
                     <li>{$LABEL_PUNT_2LI_1}</li>
                  </ul>
               </li>
               <li>
                  <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong>{$LABEL_TC2_1PUNT_3}</strong></p>
                  <ul class="subvencions1">
                     <li><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/gestiointerna/tramits/imp_associats/Bases.pdf" target="_blank">{$LABEL_PUNT_3LI_1}</a></li>
                     <!--<li><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/gestiointerna/tramits/imp_associats/Convocatoria.pdf" target="_blank">{$LABEL_PUNT_3LI_2}</a></li>-->
                     <li><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/gestiointerna/tramits/imp_associats/M0501-02_2016limitSubvencio.pdf" target="_blank">{$LABEL_PUNT_3LI_3}</a></li>
                     <li><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/gestiointerna/tramits/imp_associats/relacio%20documents.pdf" target="_blank">{$LABEL_PUNT_3LI_4}</a></li>
                     <li><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/gestiointerna/tramits/imp_associats/Sol.licitud.pdf" target="_blank">{$LABEL_PUNT_3LI_5}</a></li>
                     <li><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/gestiointerna/tramits/imp_associats/Dades%20bancaries%20creditors.pdf" target="_blank">{$LABEL_PUNT_3LI_6}</a></li>
                     <li><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/gestiointerna/tramits/imp_associats/Declaracio%20comunitats.pdf" target="_blank">{$LABEL_PUNT_3LI_7}</a></li>
                     <li><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/gestiointerna/tramits/imp_associats/declaracio%20deducio%20iva.pdf" target="_blank">{$LABEL_PUNT_3LI_8}</a></li>
                     <li><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/gestiointerna/tramits/imp_associats/declaracio%20estar%20corrent%20segsoc%20%20i%20aeat.pdf" target="_blank">{$LABEL_PUNT_3LI_9}</a></li>
                     <li><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/gestiointerna/tramits/imp_associats/declaracio%20particulars.pdf" target="_blank">{$LABEL_PUNT_3LI_10}</a></li>
                     <li><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/gestiointerna/tramits/imp_associats/autoritzacio%20AEAT.pdf" target="_blank">{$LABEL_PUNT_3LI_11}</a></li>
                  </ul>
               </li>
               <li>
                  <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong>{$LABEL_TC2_1PUNT_4}</strong></p>
                  <ul class="subvencions1">
                     <li><i class="icon-link"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/ajunta/tramits/tra_fitxa_01.asp?Codi=291" target="_blank">{$LABEL_PUNT_4LI_1}</a></li>
                  </ul>
               </li>
            </ul>
            {else if $item==2 and $item2==3}
                <!-- Subvencions 2018 -->
                <ul class="subvencions1">
                    <li>
                        <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong>{$LABEL_TC2_1PUNT_0}</strong></p>
                        <p>{$LABEL_TC2_1TXT_0_0}</p>
                    </li>
                    <li>
                        <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong>{$LABEL_TC2_1PUNT_1}</strong></p>
                        <p>{$LABEL_TC2_1TXT_1}</p>
                        <p>{$LABEL_TC2_1TXT_2}</p>
                        <ul>
                            <li>{$LABEL_PUNT_1LI_1}</li>
                            <li>{$LABEL_PUNT_1LI_2}</li>
                            <li>{$LABEL_PUNT_1LI_3}</li>
                            <li>{$LABEL_PUNT_1LI_4}</li>
                        </ul>
                    </li>
                    <li>
                        <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong>{$LABEL_TC2_1PUNT_2}</strong></p>
                        <ul>
                            <li>{$LABEL_PUNT_2LI_1}</li>
                        </ul>
                    </li>
                    <li>
                        <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong>{$LABEL_TC2_1PUNT_3}</strong></p>
                        <ul class="subvencions1">
                            <li><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/gestiointerna/tramits/imp_associats/Bases.pdf" target="_blank">{$LABEL_PUNT_3LI_1}</a></li>
                            <li><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/gestiointerna/tramits/imp_associats/M0501-02_2016limitSubvencio.pdf" target="_blank">{$LABEL_PUNT_3LI_3}</a></li>
                            <li><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/gestiointerna/tramits/imp_associats/relacio%20documents.pdf" target="_blank">{$LABEL_PUNT_3LI_4}</a></li>
                            <li><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/gestiointerna/tramits/imp_associats/Sol.licitud.pdf" target="_blank">{$LABEL_PUNT_3LI_5}</a></li>
                            <li><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/gestiointerna/tramits/imp_associats/Dades%20bancaries%20creditors.pdf" target="_blank">{$LABEL_PUNT_3LI_6}</a></li>
                            <li><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/gestiointerna/tramits/imp_associats/Declaracio%20comunitats.pdf" target="_blank">{$LABEL_PUNT_3LI_7}</a></li>
                            <li><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/gestiointerna/tramits/imp_associats/declaracio%20deducio%20iva.pdf" target="_blank">{$LABEL_PUNT_3LI_8}</a></li>
                            <li><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/gestiointerna/tramits/imp_associats/declaracio%20estar%20corrent%20segsoc%20%20i%20aeat.pdf" target="_blank">{$LABEL_PUNT_3LI_9}</a></li>
                            <li><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/gestiointerna/tramits/imp_associats/declaracio%20particulars.pdf" target="_blank">{$LABEL_PUNT_3LI_10}</a></li>
                            <li><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/gestiointerna/tramits/imp_associats/autoritzacio%20AEAT.pdf" target="_blank">{$LABEL_PUNT_3LI_11}</a></li>
                        </ul>
                    </li>
                    <li>
                        <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong>{$LABEL_TC2_1PUNT_4}</strong></p>
                        <ul class="subvencions1">
                            <li><i class="icon-link"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/ajunta/tramits/tra_fitxa_01.asp?Codi=291" target="_blank">{$LABEL_PUNT_4LI_1}</a></li>
                        </ul>
                    </li>
                </ul>
            {else if $item==2 and $item2==4}
                <!-- Subvencions façana edificis plaça Espanya 2018 -->
                <ul class="subvencions1">
                    <li>
                        <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong>{$LABEL_TC2_1PUNT_0}</strong></p>
                        <p>{$LABEL_TC2_1TXT_0}</p>
                    </li>
                    <li>
                        <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong>{$LABEL_TC2_1PUNT_1}</strong></p>
                        <p>{$LABEL_TC2_1TXT_1}</p>
                        <p>{$LABEL_TC2_1TXT_2}</p>
                        <ul>
                            <li>{$LABEL_PUNT_1LI_1}</li>
                            <li>{$LABEL_PUNT_1LI_2}</li>
                            <li>{$LABEL_PUNT_1LI_3}</li>
                            <li>{$LABEL_PUNT_1LI_4}</li>
                        </ul>
                    </li>
                    <li>
                        <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong>{$LABEL_TC2_1PUNT_2}</strong></p>
                        <ul>
                            <li>{$LABEL_PUNT_2LI_1}</li>
                        </ul>
                    </li>
                    <li>
                        <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong>{$LABEL_TC2_1PUNT_3}</strong></p>
                        <ul class="subvencions1">
                            <li><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/gestiointerna/tramits/imp_associats/bases_329.pdf" target="_blank">{$LABEL_PUNT_3LI_1}</a></li>
                            <li><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/gestiointerna/tramits/imp_associats/planol-delimiracio_329.pdf" target="_blank">{$LABEL_PUNT_3LI_3}</a></li>
                            <li><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/gestiointerna/tramits/imp_associats/	relacio-documents-ajut-reh-pl-espanya_329.pdf" target="_blank">{$LABEL_PUNT_3LI_4}</a></li>
                            <li><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/gestiointerna/tramits/imp_associats/	sollicitud_329.pdf" target="_blank">{$LABEL_PUNT_3LI_5}</a></li>
                            <li><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/gestiointerna/tramits/imp_associats/Dades%20bancaries%20creditors.pdf" target="_blank">{$LABEL_PUNT_3LI_6}</a></li>
                            <li><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/gestiointerna/tramits/imp_associats/declaracio-jurada-comunitats_329.pdf" target="_blank">{$LABEL_PUNT_3LI_7}</a></li>
                            <li><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/gestiointerna/tramits/imp_associats/declaracio-deducio-iva_329.pdf" target="_blank">{$LABEL_PUNT_3LI_8}</a></li>
                            <li><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/gestiointerna/tramits/imp_associats/declaracio-jurada-corrent-segsoc-ajt-aeat_329.pdf" target="_blank">{$LABEL_PUNT_3LI_9}</a></li>
                            <li><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/gestiointerna/tramits/imp_associats/declaracio-jurada-particulars_329.pdf" target="_blank">{$LABEL_PUNT_3LI_10}</a></li>
                            <li><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/gestiointerna/tramits/imp_associats/autoritzacio%20AEAT.pdf" target="_blank">{$LABEL_PUNT_3LI_11}</a></li>
                            <li><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/gestiointerna/tramits/imp_associats/convocatoria_329.pdf" target="_blank">{$LABEL_PUNT_3LI_2}</a></li>
                        </ul>
                    </li>
                    <li>
                        <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong>{$LABEL_TC2_1PUNT_4}</strong></p>
                        <ul class="subvencions1">
                            <li><i class="icon-link"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/ajunta/tramits/tra_fitxa_01.asp?Codi=329" target="_blank">{$LABEL_PUNT_4LI_1}</a></li>
                        </ul>
                    </li>
                </ul>
            {/if}
            <br><br><br>
         </div>
      </div>
   </div>
</div>
</div>