<!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->

<div id="page-wrap">

<div class="contenedor-responsive">

<div id="conttranstotalcos">
	<div id="conttranscos">

		<div id="btranspresen">
			<div id="transimatge">			
			<img src="{$image}" class="img-slider-territori"/>			
			</div>
			<div id="transimatgeText">
				<h1>
				{$LABEL_TITOL1}
				<i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
				</h1>
			</div>
		</div>

		<ul id='menu_planA'>
		{foreach $MENU as $itemMenu}
			<a href={$itemMenu}>
			<li id='menu_planA_item'>
				{$itemMenu@key}
			</li>
			</a>
		{/foreach}
		</ul>

		<div id="tcentre_planA">
			{if $item > 3}
				<font size=4 color=#666666>
				{$LABEL_TITOL20}
				</font>
				<br><br>
				<ul class="entitats">
					<li>
						<i class="icon-link-ext"></i><a href="http://exteriors.gencat.cat/ca/ambits-dactuacio/relacions-institucionals/relacions-intergovernamentals/registre-de-convenis-de-collaboracio-i-cooperacio/consulta-del-registre-de-convenis-de-collaboracio-i-cooperacio/" target="_blank">{$LABEL_TC3}</a>
					</li>
					<li>
						<i class="icon-file-pdf"></i><a href="http://www.tortosa.altanet.org/transparencia/contractes-convenis-subvencions/convenis-2017.pdf" target="_blank">{$LABEL_TC4}</a>
					</li>
					<li>
						<i class="icon-file-pdf"></i><a href="http://www.tortosa.altanet.org/transparencia/contractes-convenis-subvencions/convenis-2016.pdf" target="_blank">{$LABEL_TC5}</a>
					</li>
					<li>
						<i class="icon-file-pdf"></i><a href="http://www.tortosa.altanet.org/transparencia/contractes-convenis-subvencions/convenis-2015.pdf" target="_blank">{$LABEL_TC6}</a>
					</li>
				</ul>
				<br><br><br>
			{else}
				<font size=4 color=#666666>
				{$LABEL_TITOL0}
				</font>
				<p>{$LABEL_TC1}</p>
				<p>{$LABEL_TC2}</p>
				<br><br><br>
			{/if}
		</div>
		
  	</div>
</div>
</div>


