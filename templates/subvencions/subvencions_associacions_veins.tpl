<!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->
<div id="page-wrap">
<div class="contenedor-responsive">
   <div id="conttranstotalcos">
      <div id="conttranscos">
         <div id="btranspresen">
            <div id="transimatge">			
               <img src="{$image}" class="img-slider-territori"/>			
            </div>
            <div id="transimatgeText">
               <h1>
                  {$LABEL_TITOL1}
                  <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
               </h1>
            </div>
         </div>
         <div class="subtitolinfeco" id="subtitolinfeco2">
            {$LABEL_TITOL1b}
         </div>
         <div class="marcback">
            <a href="{$url_exercici_back}">
            <i class="icon-angle-double-up"></i>
            </a>
         </div>
         <ul id='menu_planA'>
            {foreach $MENU as $itemMenu}
            <a href={$itemMenu}>
               <li id='menu_planA_item'>
                  {$itemMenu@key}
               </li>
            </a>
            {/foreach}
         </ul>
         <div id="tcentre_planA">
            <font size=4 color=#666666>
            {$LABEL_TITOL0}
            </font>

            <ul class="subvencions1">
               <li>
                  <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong>{$LABEL_TC2_1PUNT_0}</strong></p>
                  <p>{$LABEL_TC2_1TXT_0}</p>
               </li>
               <li>
                  <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong>{$LABEL_TC2_1PUNT_1}</strong></p>
                  <p>{$LABEL_TC2_1TXT_1}</p>
               </li>
               <li>
                  <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong>{$LABEL_TC2_1PUNT_2}</strong></p>
                  <ul>
                     <li>{$LABEL_PUNT_2LI_1}</li>
                  </ul>
               </li>
               <li>
                  <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong>{$LABEL_TC2_1PUNT_3}</strong></p>
                  <ul class="subvencions1">
                     <li><i class="icon-file-pdf"></i>&nbsp;<a href="/subvencions/2017/bases_sub_aavv_17.pdf" target="_blank">{$LABEL_PUNT_3LI_1}</a></li>
                     <li><i class="icon-file-pdf"></i>&nbsp;<a href="/subvencions/2017/annex_sub_aavv_17.pdf" target="_blank">{$LABEL_PUNT_3LI_2}</a></li>
                  </ul>
               </li>
            </ul>
            <br><br><br>
         </div>
      </div>
   </div>
</div>
</div>