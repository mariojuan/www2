<!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->

<div id="page-wrap">

<div class="contenedor-responsive">

<div id="conttranstotalcos">
	<div id="conttranscos">
		<div id="btranspresen">
			<div id="transimatge">			
			<img src="{$image}" class="img-slider-territori"/>			
			</div>
			<div id="transimatgeText">
				<h1>
				{$LABEL_TITOL1}
				<i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
				</h1>
			</div>
		</div>

		<ul id='menu_planA'>
		{foreach $MENU as $itemMenu}
			<a href={$itemMenu}>
			<li id='menu_planA_item'>
				{$itemMenu@key}
			</li>
			</a>
		{/foreach}
		</ul>

		<div id="tcentre_planA">
			<font size=4 color=#666666>
			{$LABEL_TITOL0}
			</font>
			<p>{$LABEL_TC1}</p>
			<ul class="subvencions1">
				
				<li><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/ajunta/subvencions/Subvencions16/resum2016.pdf" target="_blank">{$LABEL_TC2}</a></li>
				<li><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/ajunta/subvencions/Subvencions15/resumresolucions2015.pdf" target="_blank">{$LABEL_TC3}</a></li>
				<li><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/ajunta/subvencions/2014/resum2014.pdf" target="_blank">{$LABEL_TC4}</a></li>
				<li><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/ajunta/subvencions/2013/resum2013.pdf" target="_blank">{$LABEL_TC5}</a></li>
			</ul>
			<br><br><br>
		</div>

  	</div>
</div>
</div>
</div>


