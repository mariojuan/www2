<html>
    <head>
        <title>

        </title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        {include file="head.tpl"}
    </head>
    <body>
        {include file="header.tpl"}
        
    

<div id="page-wrap">

<div class="contenedor-responsive">

<div id="contexpretotalcos" class="continfecototalcos">
	<div id="conttranscos">
		<div id="btranspresen">
			<div id="transimatge">			
				<img src="{$image}" class="img-slider-territori">			
			</div>
			<div id="transimatgeText">
				<h1>
				{$LABEL_TITOL1}
				</h1>
			</div>
		</div>
        <div class="subtitolinfeco" id="subtitolinfeco2">
            {if $item == 1}
                {$LABEL_TC7_1711}
            {elseif $item==2}
                {$LABEL_TC7_1712}
            {elseif $item==3}
                {$LABEL_TC7_1713}
            {elseif $item==4}
                {$LABEL_TC7_1714}
            {elseif $item==5}
                {$LABEL_TC7_1715}
            {elseif $item==6}
                {$LABEL_TC7_1716}
            {elseif $item==7}
                {$LABEL_TC7_1717}
            {elseif $item==8}
                {$LABEL_TC7_1718}
            {elseif $item==9}
                {$LABEL_TC7_1719}
            {/if}
        </div>
        <div class="marcback">
                    <a href="infeco_evolucio.php">
                    <i class="icon-angle-double-up"></i>
                    </a>
        </div>
        <br><br>

        <div id="evoluciograph" class="marcterme info-econoc-finan-no-responsive">
            <img src="{$graph}">
        </div>

        <div class="marcterme info-econoc-finan-responsive evoluciograph-mod">
            <img src="{$graph}" class="img-responsive">
        </div>

        <div id="evoluciotxt">
            {if $item == 1}
                {$LABEL_TXT_1711}
            {elseif $item==2}
                {$LABEL_TXT_1712}
            {elseif $item==3}
                {$LABEL_TXT_1713}
            {elseif $item==4}
                {$LABEL_TXT_1714}
            {elseif $item==5}
                {$LABEL_TXT_1715}
            {elseif $item==6}
                {$LABEL_TXT_1716}
            {elseif $item==7}
                {$LABEL_TXT_1717}
            {elseif $item==8}
                {$LABEL_TXT_1718}
            {elseif $item==9}
                {$LABEL_TXT_1719}
            {/if}
        </div>
  	 </div>
</div>
</div>
</div>
{include file="footer.tpl"}
</body>
</html>