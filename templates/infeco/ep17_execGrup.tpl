<html>
   <head>
      <title>
      </title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      {include file="head.tpl"}
   </head>
   <body>
      {include file="header.tpl"}
      <div id="page-wrap">
         <div class="contenedor-responsive">
            <div id="contexpretotalcos" class="continfecototalcos">
               <div id="conttranscos">
                  <div id="btranspresen">
                     <div id="transimatge">         
                        <img src="{$image}" class="img-slider-territori">           
                     </div>
                     <div id="transimatgeText">
                        <h1>
                           {$LABEL_TITOL1}
                        </h1>
                     </div>
                  </div>
                  <div class="subtitolinfeco" id="subtitolinfeco2">
                     {$LABEL_TC7_171}
                  </div>
                  <div class="marcback">
                     <a href="infeco_ep17.php?lang={$lang}">
                     <i class="icon-angle-double-up"></i>
                     </a>
                  </div>
                  <br><br>

                  <div class="info-econoc-finan-no-responsive">

                  <div id="textintro_execGrup">
                     <p>{$LABEL_TC7_INTRO1}</p>
                     <p>{$LABEL_TC7_INTRO2}</p>
                  </div>

                  <ul>
                     <li>{$LABEL_TC7_ENS1}</li>
                     <li>{$LABEL_TC7_ENS2}</li>
                     <li>{$LABEL_TC7_ENS3}</li>
                     <li>{$LABEL_TC7_ENS4}</li>
                     <li>{$LABEL_TC7_ENS5}</li>
                     <li>{$LABEL_TC7_ENS6}</li>
                  </ul>
                  <p>{$LABEL_TC7_INTRO3}</p>
                  <div class="separator"></div>
                  <div id="caixaexecgrup">
                        <div id="titolcapexectrim17">
                           {$LABEL_TC7_TITOL2}         
                        </div>
                        <div id="titolcaptrim">
                           {$LABEL_TC7_TRIM1}         
                        </div>
                        <ul class="exectrim">
                              <li><a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/grup/1t/Estabilitat_financera.pdf"><i class="icon-file-pdf"></i> {$LABEL_TC7_SUBMENU1}</a></li>
                              <li><a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/grup/1t/Estabilitat_pressupostaria.pdf"><i class="icon-file-pdf"></i> {$LABEL_TC7_SUBMENU2}</a></li>
                              <li><a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/grup/1t/Morositat_1T_2017.pdf"><i class="icon-file-pdf"></i> {$LABEL_TC7_SUBMENU3}</a></li>
                              <li><a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/grup/1t/PMP_1T_2017_Detall.pdf"><i class="icon-file-pdf"></i> {$LABEL_TC7_SUBMENU4}</a></li>
                              <li><a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/grup/1t/PMP_1T_2017_Resum.pdf"><i class="icon-file-pdf"></i> {$LABEL_TC7_SUBMENU5}</a></li>
                        </ul>
                        <div id="titolcaptrim">
                           {$LABEL_TC7_TRIM2}         
                        </div>
                        <ul class="exectrim">
                              <li><a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/grup/2t/Estabilitat_financera.pdf"><i class="icon-file-pdf"></i> {$LABEL_TC7_SUBMENU1}</a></li>
                              <li><a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/grup/2t/Estabilitat_pressupostaria.pdf"><i class="icon-file-pdf"></i> {$LABEL_TC7_SUBMENU2}</a></li>
                              <li><a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/grup/2t/Morositat_2T_2017.pdf"><i class="icon-file-pdf"></i> {$LABEL_TC7_SUBMENU3}</a></li>
                              <li><a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/grup/2t/PMP_2T_2017_Detall.pdf"><i class="icon-file-pdf"></i> {$LABEL_TC7_SUBMENU4}</a></li>
                              <li><a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/grup/2t/PMP_2T_2017_Resum.pdf"><i class="icon-file-pdf"></i> {$LABEL_TC7_SUBMENU5}</a></li>
                        </ul>
                        <div id="titolcaptrim">
                           {$LABEL_TC7_TRIM3}         
                        </div>
                        <ul class="exectrim">
                              <li><a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/grup/3t/Estabilitat_financera.pdf"><i class="icon-file-pdf"></i> {$LABEL_TC7_SUBMENU1}</a></li>
                              <li><a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/grup/3t/Estabilitat_pressupostaria.pdf"><i class="icon-file-pdf"></i> {$LABEL_TC7_SUBMENU2}</a></li>
                              <li><a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/grup/3t/Morositat_3T_2017.pdf"><i class="icon-file-pdf"></i> {$LABEL_TC7_SUBMENU3}</a></li>
                              <li><a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/grup/3t/PMP_3T_2017_Detall.pdf"><i class="icon-file-pdf"></i> {$LABEL_TC7_SUBMENU4}</a></li>
                              <li><a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/grup/3t/PMP_3T_2017_Resum.pdf"><i class="icon-file-pdf"></i> {$LABEL_TC7_SUBMENU5}</a></li>
                        </ul>
                        <div id="titolcaptrim">
                           {$LABEL_TC7_TRIM4}         
                        </div>
                        <ul class="exectrim">
                              <li><a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/grup/4t/Estabilitat_financera.pdf"><i class="icon-file-pdf"></i> {$LABEL_TC7_SUBMENU1}</a></li>
                              <li><a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/grup/4t/Estabilitat_pressupostaria.pdf"><i class="icon-file-pdf"></i> {$LABEL_TC7_SUBMENU2}</a></li>
                              <li><a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/grup/4t/Morositat_4t_2017.pdf"><i class="icon-file-pdf"></i> {$LABEL_TC7_SUBMENU3}</a></li>
                              <li><a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/grup/4t/PMP_4T_2017_Detall.pdf"><i class="icon-file-pdf"></i> {$LABEL_TC7_SUBMENU4}</a></li>
                              <li><a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/grup/4t/PMP_4T_2017_Resum.pdf"><i class="icon-file-pdf"></i> {$LABEL_TC7_SUBMENU5}</a></li>
                        </ul>
                  </div>

                  </div>

               </div>
            </div>
         </div>
      </div>
      {include file="footer.tpl"}
   </body>
</html>