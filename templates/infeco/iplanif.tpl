<html>
    <head>
        <title>

        </title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        {include file="head.tpl"}
    </head>
    <body>
        {include file="header.tpl"}
        
    

<div id="page-wrap">
<div id="contexpretotalcos" class="continfecototalcos">
	<div id="conttranscos">
		<div id="btranspresen">
			<div id="transimatge">			
				<img src={$image}>			
			</div>
			<div id="transimatgeText">
				<h1>
				{$LABEL_TITOL1}
				</h1>
			</div>
		</div>
        <div class="subtitolinfeco" id="subtitolinfeco2">
            {$LABEL_TC7_171}
        </div>
        <div class="marcback">
                    <a href="index.php?lang={$lang}">
                    <i class="icon-angle-double-up"></i>
                    </a>
        </div>
        <br><br>
        <div id="textintro_execAjt">
        {$LABEL_TC7_INTRO}
        </div>
        <ul class="indexepi">
                    <li><a href="http://www.tortosa.cat/webajt/pressupost/planificacio/LiniesFonamentalsP-2018.pdf" target="_self"><i class="icon-file-pdf"></i> {$LABEL_TC7_ITEM7}</a></li>
                    <li><a href="http://www.tortosa.cat/webajt/pressupost/planificacio/Decret_ppressup2018_2020.pdf" target="_self"><i class="icon-file-pdf"></i> {$LABEL_TC7_ITEM1}</a></li>
                    <li><a href="http://www.tortosa.cat/webajt/pressupost/planificacio/LiniesFonamentalsP-2017.pdf" target="_self"><i class="icon-file-pdf"></i> {$LABEL_TC7_ITEM2}</a></li>
                    <li><a href="http://www.tortosa.cat/webajt/pressupost/planificacio/PlaP2017-2019.pdf" target="_self"><i class="icon-file-pdf"></i> {$LABEL_TC7_ITEM3}</a></li>
                    <li><a href="http://www.tortosa.cat/webajt/pressupost/planificacio/220902032016.pdf" target="_self"><i class="icon-file-pdf"></i> {$LABEL_TC7_ITEM4}</a></li>
                    <li><a href="http://www.tortosa.cat/webajt/pressupost/planificacio/Plaeconfin2015-2016.pdf" target="_self"><i class="icon-file-pdf"></i> {$LABEL_TC7_ITEM5}</a></li>
                    <li><a href="http://www.tortosa.cat/webajt/pressupost/planificacio/069302032016.pdf"><i class="icon-file-pdf"></i> {$LABEL_TC7_ITEM6}</a></li>
        </ul>
        
  	 </div>
</div>
</div>
{include file="footer.tpl"}
</body>
</html>