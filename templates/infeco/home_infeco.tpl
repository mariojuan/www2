<!-- PLANTILLA SENSE MENÚ A L'ESQUERRA AMB FILES DE 3 COLUMNES DE CAIXES/BOTONS -->
<!--<link rel='stylesheet' href='/fonts/fontello-a238a014/css/fontello.css'>
   <link rel='stylesheet' href='/fonts/fontello-a238a014/css/animation.css'>-->
<div id="page-wrap">
   <div class="contenedor-responsive">
      <div id="conttranstotalcos">
         <div id="conttranscos">

            <div id="btranspresen">
               <div id="transimatge">           
                  <img src="{$image}" class="img-slider-territori">         
               </div>
               <div id="transimatgeText">
                  <h1>
                     {$LABEL_TITOL1}
                  </h1>
               </div>
            </div>

            <div id="textintroinfeco" class="textintroinfeco">
               {$LABEL_TC1}
               <br><br>
               {$LABEL_TC2}
            </div>

            <div class="div-contenedor-responsive-infoEconocFinan">

            <div class="contenedor-info-econoc-finan-element">
               <a href="/infeco/infeco_ep.php?lang={$lang}">
                  <div class="marcArees botoinfeco">
                     <img src="/images/infeco/b1v2.jpg" border="0" class="foto-infeco">
                  </div>
                  <div class="botoinfeco2" id="titolbotoinfeco">
                     {$LABEL_TC3}
                  </div>
                  <div class="botoinfeco2" id="textbotoinfeco">
                     {$LABEL_TC31}
                  </div>
               </a>
            </div>

            <div class="contenedor-info-econoc-finan-element">
               <a href="/infeco/infeco_evolucio.php?lang={$lang}">
                  <div class="marcArees botoinfeco">
                     <img src="/images/infeco/b2v2.jpg" border="0" class="foto-infeco"/>
                  </div>
                  <div class="botoinfeco2" id="titolbotoinfeco">
                     {$LABEL_TC4}
                  </div>
                  <div class="botoinfeco2" id="textbotoinfeco">
                     {$LABEL_TC41}
                  </div>
               </a>
            </div>

            <div class="contenedor-info-econoc-finan-element">
               <a href="/infeco/infeco_iplanif.php?lang={$lang}">
                  <div class="marcArees botoinfeco">
                     <img src="/images/infeco/b3v2.jpg" border="0" class="foto-infeco"/>
                  </div>
                  <div class="botoinfeco2" id="titolbotoinfeco">
                     {$LABEL_TC5}
                  </div>
                  <div class="botoinfeco2" id="textbotoinfeco">
                     {$LABEL_TC51}
                  </div>
               </a>
            </div>

            <div class="contenedor-info-econoc-finan-element">
               <a href="http://expedients.sindicatura.cat/VerEnteLiferayCG.php?Codigo=431554000&l=ca_ES" targer="_blank">
                  <div class="marcArees botoinfeco">
                     <img src="/images/infeco/b4v2.jpg" border="0" class="foto-infeco"/>
                  </div>
                  <div class="botoinfeco2" id="titolbotoinfeco">
                     {$LABEL_TC6}<br>&nbsp;
                  </div>
                  <div class="botoinfeco2" id="textbotoinfeco">
                     {$LABEL_TC61}
                  </div>
               </a>
            </div>

            </div>

         </div>
      </div>
   </div>
</div>