<html>
    <head>
        <title>

        </title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        {include file="head.tpl"}
    </head>
    <body>
        {include file="header.tpl"}
        
<div id="page-wrap">

<div class="contenedor-responsive">

<div id="contexpretotalcos" class="continfecototalcos">
	<div id="contModPrecos">
		<div id="btranspresen">
			<div id="transimatge">			
				<img src="{$image}" class="img-slider-territori">			
			</div>
			<div id="transimatgeText">
				<h1>
				{$LABEL_TITOL1}
				</h1>
			</div>
		</div>
        <div class="subtitolinfeco" id="subtitolinfeco2">
            {$LABEL_TC7_171}
        </div>
        <div class="marcback">
                    <a href="infeco_execajt17.php">
                    <i class="icon-angle-double-up"></i>
                    </a>
        </div>
        <br><br>

        <div class="info-econoc-finan-responsive_2">
            <div id="tablaModPre">
                <div class="fila cabecera">
                    <div class="columna">{$LABEL_TC7_CAP1}</div>
                    <div class="columna">{$LABEL_TC7_CAP2}</div>
                    <div class="columna">{$LABEL_TC7_CAP3}</div>
                    <div class="columna">{$LABEL_TC7_CAP4}</div>
                    <div class="columna">{$LABEL_TC7_CAP5}</div>
                </div>
                <div class="fila">
                    <div class="columna">13/01/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUS}</div>
                    <div class="columna">56/2017</div>
                    <div class="columna">1</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre12017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>
                <div class="fila">
                    <div class="columna">13/01/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUS}</div>
                    <div class="columna">58/2016</div>
                    <div class="columna">2</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre22017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>
                <div class="fila">
                    <div class="columna">23/01/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUS}</div>
                    <div class="columna">139/2016</div>
                    <div class="columna">3</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre32017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>
                <div class="fila">
                    <div class="columna">7/02/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUSB}</div>
                    <div class="columna"></div>
                    <div class="columna">4</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre42017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>

                <div class="fila">
                    <div class="columna">07/03/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUSB}</div>
                    <div class="columna"></div>
                    <div class="columna">5</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre52017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>
                <div class="fila">
                    <div class="columna">04/04/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUSB}</div>
                    <div class="columna"></div>
                    <div class="columna">6</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre62017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>
                <div class="fila">
                    <div class="columna">06/04/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUS}</div>
                    <div class="columna">715/2017</div>
                    <div class="columna">7</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre72017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>
                <div class="fila">
                    <div class="columna">29/03/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUS}</div>
                    <div class="columna">652/2017</div>
                    <div class="columna">8</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre82017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>

                <div class="fila">
                    <div class="columna">04/05/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUS}</div>
                    <div class="columna">925/2017</div>
                    <div class="columna">9</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre92017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>
                <div class="fila">
                    <div class="columna">04/05/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUS}</div>
                    <div class="columna">926/2017</div>
                    <div class="columna">10</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre102017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>
                <div class="fila">
                    <div class="columna">23/05/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUS}</div>
                    <div class="columna">1097/2017</div>
                    <div class="columna">11</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre112017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>
                <div class="fila">
                    <div class="columna">13/06/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUSB}</div>
                    <div class="columna"></div>
                    <div class="columna">12</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre122017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>

                <div class="fila">
                    <div class="columna">13/06/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUS}</div>
                    <div class="columna">1287/2017</div>
                    <div class="columna">13</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre132017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>
                <div class="fila">
                    <div class="columna">4/07/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUSB}</div>
                    <div class="columna"></div>
                    <div class="columna">14</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre142017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>
                <div class="fila">
                    <div class="columna">11/07/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUS}</div>
                    <div class="columna">1523/2017</div>
                    <div class="columna">15</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre152017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>
                <div class="fila">
                    <div class="columna">11/07/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUS}</div>
                    <div class="columna">1526/2017</div>
                    <div class="columna">16</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre162017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>

                <div class="fila">
                    <div class="columna">11/07/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUS}</div>
                    <div class="columna">1527/2017</div>
                    <div class="columna">17</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre172017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>
                <div class="fila">
                    <div class="columna">13/07/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUSB}</div>
                    <div class="columna"></div>
                    <div class="columna">18</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre182017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>
                <div class="fila">
                    <div class="columna">18/07/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUS}</div>
                    <div class="columna">1610/2017</div>
                    <div class="columna">19</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre192017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>
                <div class="fila">
                    <div class="columna">18/07/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUS}</div>
                    <div class="columna">1611/2017</div>
                    <div class="columna">20</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre202017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>

                <div class="fila">
                    <div class="columna">02/08/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUS}</div>
                    <div class="columna">1767/2017</div>
                    <div class="columna">21</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre212017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>
                <div class="fila">
                    <div class="columna">02/08/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUS}</div>
                    <div class="columna">1768/2017</div>
                    <div class="columna">22</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre222017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>
                <div class="fila">
                    <div class="columna">31/08/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUS}</div>
                    <div class="columna">1933/2017</div>
                    <div class="columna">23</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre232017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>
                <div class="fila">
                    <div class="columna">19/09/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUSB}</div>
                    <div class="columna"></div>
                    <div class="columna">24</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre242017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>

                <div class="fila">
                    <div class="columna">08/09/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUS}</div>
                    <div class="columna">2009/2017</div>
                    <div class="columna">25</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre252017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>
                <div class="fila">
                    <div class="columna">20/09/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUS}</div>
                    <div class="columna">2078/2017</div>
                    <div class="columna">26</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre262017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>
                <div class="fila">
                    <div class="columna">20/09/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUS}</div>
                    <div class="columna">2085/2017</div>
                    <div class="columna">27</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre272017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>
                <div class="fila">
                    <div class="columna">05/10/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUSB}</div>
                    <div class="columna"></div>
                    <div class="columna">28</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre282017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>

                <div class="fila">
                    <div class="columna">11/10/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUS}</div>
                    <div class="columna">2240/2017</div>
                    <div class="columna">29</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre292017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>
                <div class="fila">
                    <div class="columna">13/10/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUS}</div>
                    <div class="columna">2291/2017</div>
                    <div class="columna">30</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre302017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>
                <div class="fila">
                    <div class="columna">30/10/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUS}</div>
                    <div class="columna">2425/2017</div>
                    <div class="columna">31</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre312017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>
                <div class="fila">
                    <div class="columna">7/11/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUSB}</div>
                    <div class="columna"></div>
                    <div class="columna">32</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre322017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>
                <div class="fila">
                    <div class="columna">7/11/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUSB}</div>
                    <div class="columna"></div>
                    <div class="columna">33</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre332017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>
                <div class="fila">
                    <div class="columna">7/11/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUSB}</div>
                    <div class="columna"></div>
                    <div class="columna">34</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre342017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>
                <div class="fila">
                    <div class="columna">7/11/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUS}</div>
                    <div class="columna">2483/2017</div>
                    <div class="columna">35</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre352017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>
                <div class="fila">
                    <div class="columna">20/11/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUS}</div>
                    <div class="columna">2675/2017</div>
                    <div class="columna">36</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre362017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>
                <div class="fila">
                    <div class="columna">23/11/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUS}</div>
                    <div class="columna">2739/2017</div>
                    <div class="columna">37</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre372017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>
                <div class="fila">
                    <div class="columna">28/11/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUS}</div>
                    <div class="columna">2758/2017</div>
                    <div class="columna">38</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre382017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>
                <div class="fila">
                    <div class="columna">30/11/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUS}</div>
                    <div class="columna">2792/2017</div>
                    <div class="columna">39</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre392017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>
                <div class="fila">
                    <div class="columna">11/12/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUS}</div>
                    <div class="columna">2835/2017</div>
                    <div class="columna">40</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre402017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>
                <div class="fila">
                    <div class="columna">19/12/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUS}</div>
                    <div class="columna">2901/2017</div>
                    <div class="columna">41</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre412017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>
                <div class="fila">
                    <div class="columna">19/12/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUS}</div>
                    <div class="columna">2900/2017</div>
                    <div class="columna">42</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre422017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>
                <div class="fila">
                    <div class="columna">28/12/2017</div>
                    <div class="columna">{$LABEL_TC7_TIPUS}</div>
                    <div class="columna">2976/2017</div>
                    <div class="columna">43</div>
                    <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre432017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
                </div>
        
            </div>

        </div>
        <!--
        <div class="info-econoc-finan-responsive">

            <table >
                <thead>
                  <tr>
                    <th>{$LABEL_TC7_CAP1}</th>
                    <th>{$LABEL_TC7_CAP2}</th>
                    <th>{$LABEL_TC7_CAP3}</th>
                    <th>{$LABEL_TC7_CAP4}</th>
                    <th>{$LABEL_TC7_CAP5}</td>
                    </tr>
                    </thead>
                    
                    <tr>
                        <td><b>{$LABEL_TC7_CAP1}:</b> 13/01/2017</td>
                        <td><b>{$LABEL_TC7_CAP2}:</b> {$LABEL_TC7_TIPUS}</td>
                        <td><b>{$LABEL_TC7_CAP3}:</b> 56/2017</td>
                        <td><b>{$LABEL_TC7_CAP4}:</b> 1</td>
                        <td>
                            <b>{$LABEL_TC7_CAP5}:</b>
                            <a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre12017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a>
                        </td>
                    </tr>

                    <tr>
                        <td><b>{$LABEL_TC7_CAP1}:</b> 13/01/2017</td>
                        <td><b>{$LABEL_TC7_CAP2}:</b> {$LABEL_TC7_TIPUS}</td>
                        <td><b>{$LABEL_TC7_CAP3}:</b> 58/2016</td>
                        <td><b>{$LABEL_TC7_CAP4}:</b> 2</td>
                        <td>
                            <b>{$LABEL_TC7_CAP5}:</b>
                            <a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre22017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a>
                        </td>
                    </tr>

                    <tr>
                        <td><b>{$LABEL_TC7_CAP1}:</b> 23/01/2017</td>
                        <td><b>{$LABEL_TC7_CAP2}:</b> {$LABEL_TC7_TIPUS}</td>
                        <td><b>{$LABEL_TC7_CAP3}:</b> 139/2016</td>
                        <td><b>{$LABEL_TC7_CAP4}:</b> 3</td>
                        <td>
                            <b>{$LABEL_TC7_CAP5}:</b>
                            <a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre32017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a>
                        </td>
                    </tr>

                    <tr>
                        <td><b>{$LABEL_TC7_CAP1}:</b> 7/02/2017</td>
                        <td><b>{$LABEL_TC7_CAP2}:</b> {$LABEL_TC7_TIPUSB}</td>
                        <td><b>{$LABEL_TC7_CAP3}:</b> </td>
                        <td><b>{$LABEL_TC7_CAP4}:</b> 4</td>
                        <td>
                            <b>{$LABEL_TC7_CAP5}:</b>
                            <a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre42017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a>
                        </td>
                    </tr>
                    
                </table>

        </div>
        -->
  	 </div>
     </div>
</div>
</div>
{include file="footer.tpl"}
</body>
</html>