<html>
   <head>
      <title>
      </title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      {include file="head.tpl"}
   </head>
   <body>
      {include file="header.tpl"}
      <div id="page-wrap">
         <div class="contenedor-responsive">
            <div id="contexpretotalcos" class="continfecototalcos">
               <div id="conttranscos">
                  <div id="btranspresen">
                     <div id="transimatge">         
                        <img src="{$image}" class="img-slider-territori">           
                     </div>
                     <div id="transimatgeText">
                        <h1>
                           {$LABEL_TITOL1}
                        </h1>
                     </div>
                  </div>
                  <div class="subtitolinfeco" id="subtitolinfeco2">
                     {$LABEL_TC7_181}
                  </div>
                  <div class="marcback">
                     <a href="infeco_ep18.php?lang={$lang}">
                     <i class="icon-angle-double-up"></i>
                     </a>
                  </div>
                  <br><br>

                  <div class="info-econoc-finan-no-responsive">

                  <div id="textintro_execAjt">
                     {$LABEL_TC7_INTRO}
                  </div>
                  <div id="caixaexecajt2">
                     <div id="caixamodpre" class="caixaexecajt17">
                        <a href="infeco_execajt18modpre.php">
                        <br>{$LABEL_TC7_TITOL1}<br>{$LABEL_TC7_TITOL1B} 
                        </a>       
                     </div>
                     <div id="caixaexectrim" class="caixaexecajt17">
                        <div id="titolcapexectrim17">
                           {$LABEL_TC7_TITOL2}         
                        </div>
                        
                           <div id="capexectrim1" class="capexectrim capexectrim_inactiu">
                              {$LABEL_TC7_TRIM1}
                           </div>
                        
                        
                           <div id="capexectrim2" class=" capexectrim capexectrim_inactiu">
                              {$LABEL_TC7_TRIM2}
                           </div>
                        
                        
                           <div id="capexectrim3" class="capexectrim capexectrim_inactiu">
                              {$LABEL_TC7_TRIM3}
                           </div>
                        
                        
                           <div id="capexectrim4" class="capexectrim capexectrim_inactiu">
                              {$LABEL_TC7_TRIM4}
                           </div>
                        
                     </div>
                  </div>

                  </div>

                  <!--<div class="info-econoc-finan-responsive">
                     <i class="icon-link"></i>
                     <a href="infeco_execajt18modpre.php">
                        {$LABEL_TC7_TITOL1} {$LABEL_TC7_TITOL1B} 
                        </a>
                  </div>-->

               </div>
            </div>
         </div>
      </div>
      {include file="footer.tpl"}
   </body>
</html>