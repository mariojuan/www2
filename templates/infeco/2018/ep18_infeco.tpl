<html>
    <head>
        <title>

        </title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        {include file="head.tpl"}
    </head>
    <body>
        {include file="header.tpl"}
        
    

<div id="page-wrap">

<div class="contenedor-responsive">

<div id="contexpretotalcos" class="continfecototalcos">

	<div id="conttranscos">
		<div id="btranspresen">
			<div id="transimatge">			
				<img src="{$image}" class="img-slider-territori">			
			</div>
			<div id="transimatgeText">
				<h1>
				{$LABEL_TITOL1}
				</h1>
			</div>
		</div>
        <div class="subtitolinfeco" id="subtitolinfeco2">
            {$LABEL_TC7_181}
        </div>
        <div class="marcback">
                    <a href="../infeco_ep.php?lang={$lang}">
                    <i class="icon-angle-double-up"></i>
                    </a>
        </div>
        <br><br>

        <div id="menu2017" class="div-contenidor-info-econom">
            <a href="infeco_presup18.php?lang={$lang}">
            <div id="bpresgen" class="marcServeisCiutat bpresgen">
                Pressupost General<br>2018
            </div>  
            </a>
            <div id="bpresgen2" class="marcServeisCiutat bpresgen bpresgen_inactiu">
                Grup<br>Municipal<br><font size="2">Sector Adm. Pública<br>Consolidat</font>
            </div> 
            <a href="http://www2.tortosa.cat/infeco/2018/infeco_execajt18.php?lang={$lang}"> 
            <div id="bpresgen3" class="marcServeisCiutat bpresgen">
                <br>Ajuntament
            </div>
            </a>
            <div id="bpresgen4" class="marcServeisCiutat bpresgen">
                <br>Ajuntament<br><font size="2">Liquidació</font>
            </div>  
            <div id="bpresgen5" class="marcServeisCiutat bpresgen">
               Ens<br>dependents<br><font size="2"><br>Comptes anuals</font>
            </div>
            <div id="bpresgen6" class="marcServeisCiutat bpresgen">
                Compte<br>General
            </div>  
        </div>

        <div class="div-contenidor-info-econom-list">

            <div class="llista-pressupostos">

                <p><b>Inici</b></p>
                <ul>
                    <li>
                        <a href="infeco_presup18.php?lang={$lang}">
                            Pressupost General 2018
                        </a>
                    </li>
                </ul>

                <p><b>Execució</b></p>
                <ul>
                    <li>
                        <a href="infeco_execajt18.php?lang={$lang}">
                            Ajuntament
                        </a>
                    </li>
                </ul>

                
                <!--<p>Tractament</p>
                <ul>
                    <li>
                        Pressupost General 2017
                    </li>
                </ul>
                -->

            </div>

        </div>

  	</div>
    </div>
</div>
</div>
{include file="footer.tpl"}
</body>
</html>