<html>
    <head>
        <title>

        </title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        {include file="head.tpl"}
    </head>
    <body>
        {include file="header.tpl"}
        
<div id="page-wrap">

<div class="contenedor-responsive">

<div id="contexpretotalcos" class="continfecototalcos">
	<div id="contModPrecos">
		<div id="btranspresen">
			<div id="transimatge">			
				<img src="{$image}" class="img-slider-territori">			
			</div>
			<div id="transimatgeText">
				<h1>
				{$LABEL_TITOL1}
				</h1>
			</div>
		</div>
        <div class="subtitolinfeco" id="subtitolinfeco2">
            {$LABEL_TC7_181}
        </div>
        <div class="marcback">
                    <a href="infeco_execajt18.php">
                    <i class="icon-angle-double-up"></i>
                    </a>
        </div>
        <br><br>

        <div class="info-econoc-finan-no-responsive">

        <div id="tablaModPre">
            <div class="fila cabecera">
                <div class="columna">{$LABEL_TC7_CAP1}</div>
                <div class="columna">{$LABEL_TC7_CAP2}</div>
                <div class="columna">{$LABEL_TC7_CAP3}</div>
                <div class="columna">{$LABEL_TC7_CAP4}</div>
                <div class="columna">{$LABEL_TC7_CAP5}</div>
            </div>
            <div class="fila">
                <div class="columna">15/01/2018</div>
                <div class="columna">{$LABEL_TC7_TIPUS}</div>
                <div class="columna">110/2018</div>
                <div class="columna">1</div>
                <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2018/ModPre/modpre12018.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
            </div>
            <div class="fila">
                <div class="columna">15/01/2018</div>
                <div class="columna">{$LABEL_TC7_TIPUS}</div>
                <div class="columna">112/2018</div>
                <div class="columna">2</div>
                <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2018/ModPre/modpre22018.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
            </div>
            <div class="fila">
                <div class="columna">23/01/2018</div>
                <div class="columna">{$LABEL_TC7_TIPUS}</div>
                <div class="columna">173/2018</div>
                <div class="columna">3</div>
                <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2018/ModPre/modpre32018.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
            </div>
            <div class="fila">
                <div class="columna">25/01/2018</div>
                <div class="columna">{$LABEL_TC7_TIPUS}</div>
                <div class="columna">220/2018</div>
                <div class="columna">4</div>
                <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2018/ModPre/modpre42018.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
            </div>

            <div class="fila">
                <div class="columna">29/01/2018</div>
                <div class="columna">{$LABEL_TC7_TIPUS}</div>
                <div class="columna">267/2018</div>
                <div class="columna">5</div>
                <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2018/ModPre/modpre52018.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
            </div>
            <div class="fila">
                <div class="columna">01/02/2018</div>
                <div class="columna">{$LABEL_TC7_TIPUS}</div>
                <div class="columna">309/2018</div>
                <div class="columna">6</div>
                <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2018/ModPre/modpre62018.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
            </div>
            <div class="fila">
                <div class="columna">07/02/2018</div>
                <div class="columna">{$LABEL_TC7_TIPUS}</div>
                <div class="columna">347/2018</div>
                <div class="columna">7</div>
                <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2018/ModPre/modpre72018.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
            </div>
            <div class="fila">
                <div class="columna">01/03/2018</div>
                <div class="columna">{$LABEL_TC7_TIPUS}</div>
                <div class="columna">561/2018</div>
                <div class="columna">8</div>
                <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2018/ModPre/modpre82018.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
            </div>
        </div>

        </div>

        
        
  	 </div>
     </div>
</div>
</div>
{include file="footer.tpl"}
</body>
</html>