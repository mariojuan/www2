<html>
    <head>
        <title>

        </title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        {include file="head.tpl"}
    </head>
    <body>
        {include file="header.tpl"}
        
    

<div id="page-wrap">

<div class="contenedor-responsive">

<div id="contexpretotalcos" class="continfecototalcos">
	<div id="conttranscos">
		<div id="btranspresen">
			<div id="transimatge">			
				<img src="{$image}" class="img-slider-territori">			
			</div>
			<div id="transimatgeText">
				<h1>
				{$LABEL_TITOL1}
				</h1>
			</div>
		</div>
        <div class="subtitolinfeco" id="subtitolinfeco2">
            {$LABEL_TC7_171}
        </div>
        <div class="marcback">
                    <a href="index.php?lang={$lang}">
                    <i class="icon-angle-double-up"></i>
                    </a>
        </div>
        <br><br>
        <div id="textintro_execAjt">
        {$LABEL_TC7_INTRO}
        </div>
        <ul class="indexepi">
            <li><samp class="titolsubra">{$LABEL_TC7_TITOL1}</samp></li>
                <ul>
                    <li><a href="{$url_evolucio1}" target="_self"><i class="icon-chart-bar"></i> {$LABEL_TC7_ITEM1}</a></li>
                    <li><a href="{$url_evolucio2}" target="_self"><i class="icon-chart-bar"></i> {$LABEL_TC7_ITEM2}</a></li>
                    <li><a href="{$url_evolucio3}" target="_self"><i class="icon-doc-text"></i> {$LABEL_TC7_ITEM3}</a></li>
                    <li><a href="{$url_evolucio4}" target="_self"><i class="icon-chart-line"></i> {$LABEL_TC7_ITEM4}</a></li>
                    <li><a href="{$url_evolucio5}" target="_self"><i class="icon-chart-line"></i> {$LABEL_TC7_ITEM5}</a></li>
                </ul>
                <br>
            <li><samp class="titolsubra">{$LABEL_TC7_TITOL2}</samp></li>
                <ul>
                    <li><a href="{$url_evolucio6}"><i class="icon-chart-line"></i> {$LABEL_TC7_ITEM6}</a></li>
                    <li><a href="{$url_evolucio7}"><i class="icon-chart-line"></i> {$LABEL_TC7_ITEM7}</a></li>
                    <li><a href="{$url_evolucio8}"><i class="icon-chart-bar"></i> {$LABEL_TC7_ITEM8}</a></li>
                    <li><a href="{$url_evolucio9}"><i class="icon-chart-bar"></i> {$LABEL_TC7_ITEM9}</a></li>
                </ul>
        </ul>
        
  	 </div>
     </div>
</div>
</div>
{include file="footer.tpl"}
</body>
</html>