<html>
   <head>
      <title>
      </title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      {include file="head.tpl"}
   </head>
   <body>
      {include file="header.tpl"}
      <div id="page-wrap">
         <div class="contenedor-responsive">
            <div id="contexpretotalcos" class="continfecototalcos">
               <div id="conttranscos">
                  <div id="btranspresen">
                     <div id="transimatge">         
                        <img src="{$image}" class="img-slider-territori">           
                     </div>
                     <div id="transimatgeText">
                        <h1>
                           {$LABEL_TITOL1}
                        </h1>
                     </div>
                  </div>
                  <div class="subtitolinfeco" id="subtitolinfeco2">
                     {$LABEL_TC7_171}
                  </div>
                  <div class="marcback">
                     <a href="infeco_ep17.php?lang={$lang}">
                     <i class="icon-angle-double-up"></i>
                     </a>
                  </div>
                  <br><br>

                  <div class="info-econoc-finan-no-responsive">

                  <div id="textintro_execAjt">
                     {$LABEL_TC7_INTRO}
                  </div>
                  <div id="caixaexecajt2">
                     <div id="caixamodpre" class="caixaexecajt17">
                        <a href="infeco_execajt17modpre.php">
                        <br>{$LABEL_TC7_TITOL1}<br>{$LABEL_TC7_TITOL1B} 
                        </a>       
                     </div>
                     <div id="caixaexectrim" class="caixaexecajt17">
                        <div id="titolcapexectrim17">
                           {$LABEL_TC7_TITOL2}         
                        </div>
                        <a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/ajt/1rtrim2017.pdf">
                           <div id="capexectrim1" class="capexectrim">
                              {$LABEL_TC7_TRIM1}
                           </div>
                        </a>
                        <a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/ajt/2ntrim2017.pdf">
                           <div id="capexectrim2" class="capexectrim">
                              {$LABEL_TC7_TRIM2}
                           </div>
                        </a>
                        <a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/ajt/3ntrim2017.pdf">
                           <div id="capexectrim3" class="capexectrim">
                              {$LABEL_TC7_TRIM3}
                           </div>
                        </a>
                        <a href="http://www.tortosa.altanet.org/pressupost/2017/execucio/ajt/4ttrim2017.pdf">
                           <div id="capexectrim4" class="capexectrim">
                              {$LABEL_TC7_TRIM4}
                           </div>
                        </a>
                     </div>
                  </div>

                  </div>

                  <div class="info-econoc-finan-responsive">
                     <i class="icon-link"></i>
                     <a href="infeco_execajt17modpre.php">
                        {$LABEL_TC7_TITOL1} {$LABEL_TC7_TITOL1B} 
                        </a>
                  </div>

               </div>
            </div>
         </div>
      </div>
      {include file="footer.tpl"}
   </body>
</html>