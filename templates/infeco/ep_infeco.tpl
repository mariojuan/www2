<!-- PLANTILLA SENSE MENÚ A L'ESQUERRA AMB FILES DE 3 COLUMNES DE CAIXES/BOTONS -->
<!--<link rel='stylesheet' href='/fonts/fontello-a238a014/css/fontello.css'>
   <link rel='stylesheet' href='/fonts/fontello-a238a014/css/animation.css'>-->
<div id="page-wrap">
   <div class="contenedor-responsive">
      <div id="continfecototalcos" class="continfecototalcos">
         <div id="conttranscos">
            <div id="btranspresen">
               <div id="transimatge">           
                  <img src="{$image}" class="img-slider-territori">         
               </div>
               <div id="transimatgeText">
                  <h1>
                     {$LABEL_TITOL1}
                  </h1>
               </div>
            </div>
            <div class="subtitolinfeco" id="subtitolinfeco2">
               {$LABEL_TC7}
            </div>
            <div class="marcback">
               <a href="index.php?lang={$lang}">
               <i class="icon-angle-double-up"></i>
               </a>
            </div>
            <div id="textintroinfeco_ep" class="textintroinfeco">
               {$LABEL_TC71}
            </div>
            <br><br>

            <div class="div-contenidor-info-econom">

               <a href="/infeco/2018/infeco_ep18.php?lang={$lang}">
                  <div class="marcArees exf1 exc1">
                     <img src="/images/infeco/iexercici2018.jpg">
                  </div>
               </a>
                <div class="marcArees exf1 exc2">
                </div>
                <div class="marcArees exf1 exc3">
                </div>
                <div class="marcArees exf1 exc4">
                </div>
                <div class="marcArees exf1 exc5">
                </div>

               <a href="/infeco/infeco_ep17.php?lang={$lang}">
                  <div class="marcArees exf2 exc1" style="clear:both">
                     <img src="/images/infeco/iexercici2017.jpg">
                  </div>
               </a>
               <a href="http://www.tortosa.cat/webajt/pressupost/2016/index_infpre.asp">
                  <div class="marcArees  exf2 exc2">
                     <img src="/images/infeco/iexercici2016.jpg">
                  </div>
               </a>
               <a href="http://www.tortosa.cat/webajt/pressupost/2015/index_infpre.asp">
                  <div class="marcArees  exf2 exc3">
                     <img src="/images/infeco/iexercici2015.jpg">
                  </div>
               </a>
               <a href="http://www.tortosa.cat/webajt/pressupost/2014/index_infpre.asp">
                  <div class="marcArees  exf2 exc4">
                     <img src="/images/infeco/iexercici2014.jpg">
                  </div>
               </a>
               <a href="http://www.tortosa.cat/webajt/pressupost/2013/index_infpre.asp">
                  <div class="marcArees  exf2 exc5">
                     <img src="/images/infeco/iexercici2013.jpg">
                  </div>
               </a>
               <a href="http://www.tortosa.cat/webajt/pressupost/2012/index_infpre.asp">
                  <div class="marcArees exf3 exc1">
                     <img src="/images/infeco/iexercici2012.jpg">
                  </div>
               </a>
               <a href="http://www.tortosa.cat/webajt/pressupost/2011/index_infpre.asp">
                  <div class="marcArees exf3 exc2">
                     <img src="/images/infeco/iexercici2011.jpg">
                  </div>
               </a>
               <a href="http://www.tortosa.cat/webajt/pressupost/2010/index.asp">
                  <div class="marcArees exf3 exc3">
                     <img src="/images/infeco/iexercici2010.jpg">
                  </div>
               </a>
               <a href="http://www.tortosa.cat/webajt/pressupost/2009/index.html">
                  <div class="marcArees exf3 exc4">
                     <img src="/images/infeco/iexercici2009.jpg">
                  </div>
               </a>
               <a href="http://www.tortosa.cat/webajt/pressupost/2008/index.html">
                  <div class="marcArees exf3 exc5">
                     <img src="/images/infeco/iexercici2008.jpg">
                  </div>
               </a>
               <a href="http://www.tortosa.cat/webajt/pressupost/2007/index.html">
                  <div class="marcArees exf4 exc1">
                     <img src="/images/infeco/iexercici2007.jpg">
                  </div>
               </a>
               <a href="http://www.tortosa.cat/webajt/pressupost/2006/index.html">
                  <div class="marcArees exf4 exc2">
                     <img src="/images/infeco/iexercici2006.jpg">
                  </div>
               </a>
               <a href="http://www.tortosa.altanet.org/ajtms/tortosa/p2005/index.html">
                  <div class="marcArees exf4 exc3">
                     <img src="/images/infeco/iexercici2005.jpg">
                  </div>
               </a>
               <a href="http://www.tortosa.altanet.org/ajtms/tortosa/p2004/index.html">
                  <div class="marcArees exf4 exc4">
                     <img src="/images/infeco/iexercici2004.jpg">
                  </div>
               </a>
               <a href="http://www.tortosa.altanet.org/ajtms/tortosa/p2003/index.html">
                  <div class="marcArees exf4 exc5">
                     <img src="/images/infeco/iexercici2003.jpg">
                  </div>
               </a>

            </div>

            <div class="div-contenidor-info-econom-list">

               <ul>
                  <li>
                     <a href="/infeco/2018/infeco_ep18.php?lang={$lang}">
                        Pressupost 2018
                     </a>
                  </li>
                  <li>
                     <a href="/infeco/infeco_ep17.php?lang={$lang}">
                        Pressupost 2017
                     </a>
                  </li>
                  <li>
                     <a href="http://www.tortosa.cat/webajt/pressupost/2016/index_infpre.asp">
                        Pressupost 2016
                     </a>
                  </li>
                  <li>
                     <a href="http://www.tortosa.cat/webajt/pressupost/2015/index_infpre.asp">
                        Pressupost 2015
                     </a>
                  </li>
                  <li>
                     <a href="http://www.tortosa.cat/webajt/pressupost/2014/index_infpre.asp">
                        Pressupost 2014
                     </a>
                  </li>
                  <li>
                     <a href="http://www.tortosa.cat/webajt/pressupost/2013/index_infpre.asp">
                        Pressupost 2013
                     </a>
                  </li>
                  <li>
                     <a href="http://www.tortosa.cat/webajt/pressupost/2012/index_infpre.asp">
                        Pressupost 2012
                     </a>
                  </li>
                  <li>
                     <a href="http://www.tortosa.cat/webajt/pressupost/2011/index_infpre.asp">
                        Pressupost 2011
                     </a>
                  </li>
                  <li>
                     <a href="http://www.tortosa.cat/webajt/pressupost/2010/index.asp">
                        Pressupost 2010
                     </a>
                  </li>
                  <li>
                     <a href="http://www.tortosa.cat/webajt/pressupost/2009/index.html">
                        Pressupost 2009
                     </a>
                  </li>
                  <li>
                     <a href="http://www.tortosa.cat/webajt/pressupost/2008/index.html">
                        Pressupost 2008
                     </a>
                  </li>
                  <li>
                     <a href="http://www.tortosa.cat/webajt/pressupost/2007/index.html">
                        Pressupost 2007
                     </a>
                  </li>
                  <li>
                     <a href="http://www.tortosa.cat/webajt/pressupost/2006/index.html">
                        Pressupost 2006
                     </a>
                  </li>
                  <li>
                     <a href="http://www.tortosa.altanet.org/ajtms/tortosa/p2005/index.html">
                        Pressupost 2005
                     </a>
                  </li>
                  <li>
                     <a href="http://www.tortosa.altanet.org/ajtms/tortosa/p2004/index.html">
                        Pressupost 2004
                     </a>
                  </li>
                  <li>
                     <a href="http://www.tortosa.altanet.org/ajtms/tortosa/p2003/index.html">
                        Pressupost 2003
                     </a>
                  </li>
               </ul>

            </div>

         </div>
      </div>
   </div>
</div>