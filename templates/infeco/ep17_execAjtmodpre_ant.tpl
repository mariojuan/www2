<html>
    <head>
        <title>

        </title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        {include file="head.tpl"}
    </head>
    <body>
        {include file="header.tpl"}
        
<div id="page-wrap">

<div class="contenedor-responsive">

<div id="contexpretotalcos" class="continfecototalcos">
	<div id="conttranscos">
		<div id="btranspresen">
			<div id="transimatge">			
				<img src="{$image}" class="img-slider-territori">			
			</div>
			<div id="transimatgeText">
				<h1>
				{$LABEL_TITOL1}
				</h1>
			</div>
		</div>
        <div class="subtitolinfeco" id="subtitolinfeco2">
            {$LABEL_TC7_171}
        </div>
        <div class="marcback">
                    <a href="infeco_execajt17.php">
                    <i class="icon-angle-double-up"></i>
                    </a>
        </div>
        <br><br>

        <div class="info-econoc-finan-no-responsive">

        <div id="tablaModPre">
            <div class="fila cabecera">
                <div class="columna">{$LABEL_TC7_CAP1}</div>
                <div class="columna">{$LABEL_TC7_CAP2}</div>
                <div class="columna">{$LABEL_TC7_CAP3}</div>
                <div class="columna">{$LABEL_TC7_CAP4}</div>
                <div class="columna">{$LABEL_TC7_CAP5}</div>
            </div>
            <div class="fila">
                <div class="columna">13/01/2017</div>
                <div class="columna">{$LABEL_TC7_TIPUS}</div>
                <div class="columna">56/2017</div>
                <div class="columna">1</div>
                <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre12017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
            </div>
            <div class="fila">
                <div class="columna">13/01/2017</div>
                <div class="columna">{$LABEL_TC7_TIPUS}</div>
                <div class="columna">58/2016</div>
                <div class="columna">2</div>
                <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre22017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
            </div>
            <div class="fila">
                <div class="columna">23/01/2017</div>
                <div class="columna">{$LABEL_TC7_TIPUS}</div>
                <div class="columna">139/2016</div>
                <div class="columna">3</div>
                <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre32017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
            </div>
            <div class="fila">
                <div class="columna">7/02/2017</div>
                <div class="columna">{$LABEL_TC7_TIPUSB}</div>
                <div class="columna"></div>
                <div class="columna">4</div>
                <div class="columna"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre42017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a></div>
            </div>
    
        </div>

        </div>

        <div class="info-econoc-finan-responsive">

            <table >
                <thead>
                  <tr>
                    <th>{$LABEL_TC7_CAP1}</th>
                    <th>{$LABEL_TC7_CAP2}</th>
                    <th>{$LABEL_TC7_CAP3}</th>
                    <th>{$LABEL_TC7_CAP4}</th>
                    <th>{$LABEL_TC7_CAP5}</td>
                    </tr>
                    </thead>
                    
                    <tr>
                        <td><b>{$LABEL_TC7_CAP1}:</b> 13/01/2017</td>
                        <td><b>{$LABEL_TC7_CAP2}:</b> {$LABEL_TC7_TIPUS}</td>
                        <td><b>{$LABEL_TC7_CAP3}:</b> 56/2017</td>
                        <td><b>{$LABEL_TC7_CAP4}:</b> 1</td>
                        <td>
                            <b>{$LABEL_TC7_CAP5}:</b>
                            <a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre12017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a>
                        </td>
                    </tr>

                    <tr>
                        <td><b>{$LABEL_TC7_CAP1}:</b> 13/01/2017</td>
                        <td><b>{$LABEL_TC7_CAP2}:</b> {$LABEL_TC7_TIPUS}</td>
                        <td><b>{$LABEL_TC7_CAP3}:</b> 58/2016</td>
                        <td><b>{$LABEL_TC7_CAP4}:</b> 2</td>
                        <td>
                            <b>{$LABEL_TC7_CAP5}:</b>
                            <a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre22017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a>
                        </td>
                    </tr>

                    <tr>
                        <td><b>{$LABEL_TC7_CAP1}:</b> 23/01/2017</td>
                        <td><b>{$LABEL_TC7_CAP2}:</b> {$LABEL_TC7_TIPUS}</td>
                        <td><b>{$LABEL_TC7_CAP3}:</b> 139/2016</td>
                        <td><b>{$LABEL_TC7_CAP4}:</b> 3</td>
                        <td>
                            <b>{$LABEL_TC7_CAP5}:</b>
                            <a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre32017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a>
                        </td>
                    </tr>

                    <tr>
                        <td><b>{$LABEL_TC7_CAP1}:</b> 7/02/2017</td>
                        <td><b>{$LABEL_TC7_CAP2}:</b> {$LABEL_TC7_TIPUSB}</td>
                        <td><b>{$LABEL_TC7_CAP3}:</b> </td>
                        <td><b>{$LABEL_TC7_CAP4}:</b> 4</td>
                        <td>
                            <b>{$LABEL_TC7_CAP5}:</b>
                            <a href="http://www.tortosa.cat/webajt/pressupost/2017/ModPre/modpre42017.pdf" target="_blank"><img src="/images/icons/ipdf.gif" width="20" height="20"></a>
                        </td>
                    </tr>
                    
                </table>

        </div>
        
  	 </div>
     </div>
</div>
</div>
{include file="footer.tpl"}
</body>
</html>