<html>
   <head>
      <title>
      </title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      {include file="head.tpl"}
   </head>
   <body>
      {include file="header.tpl"}
      <div id="page-wrap">
         <div class="contenedor-responsive">
            <div id="contexpretotalcos" class="continfecototalcos">
               <div id="conttranscos">
                  <div id="btranspresen">
                     <div id="transimatge">        
                        <img src="{$image}" class="img-slider-territori">        
                     </div>
                     <div id="transimatgeText">
                        <h1>
                           {$LABEL_TITOL1}
                        </h1>
                     </div>
                  </div>
                  <div class="subtitolinfeco" id="subtitolinfeco2">
                     {$LABEL_TC7_171}
                  </div>
                  <div class="marcback">
                     <a href="infeco_ep17.php?lang={$lang}">
                     <i class="icon-angle-double-up"></i>
                     </a>
                  </div>
                  <br><br>

                  <div class="info-econoc-finan-no-responsive">
                     <div id="taula3cpre">
                        <div id="taula3ccolpre">
                           <div id="columna1pre"><a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/tparticip.pdf" target="_blank">&raquo; {$LABEL_TC7_ITEM1}</a></div>
                           <div id="columna2pre"></div>
                           <div id="columna3pre"><a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/Memalc.pdf" target="_blank">&raquo; {$LABEL_TC7_ITEM2}</a></div>
                        </div>
                        <div id="taula3ccolpre">
                           <div id="columna1pre"><a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/GrupComparativa.pdf" target="_blank">&raquo; {$LABEL_TC7_ITEM3}</a></div>
                           <div id="columna2pre"></div>
                           <div id="columna3pre"><a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/BE2017.pdf" target="_blank">&raquo; {$LABEL_TC7_ITEM4}</a></div>
                        </div>
                        <div id="taula3ccolpre">
                           <div id="columna1pre"><a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/evolucioPressupost.pdf" target="_blank">&raquo; {$LABEL_TC7_ITEM5}</a></div>
                           <div id="columna2pre"></div>
                           <div id="columna3pre"><a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/InformeInt.pdf" target="_blank">&raquo; {$LABEL_TC7_ITEM6}</a></div>
                        </div>
                        <div id="taula3ccolpre">
                           <div id="columna1pre" style="border-bottom: 0px solid #eee;"></div>
                           <div id="columna2pre"></div>
                           <div id="columna3pre"><a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/InformeIntEstab.pdf" target="_blank">&raquo; {$LABEL_TC7_ITEM7}</a></div>
                        </div>
                        <div id="taula3ccolpre">
                           <div id="columna1pre" style="border-bottom: 0px solid #eee;"></div>
                           <div id="columna2pre"></div>
                           <div id="columna3pre"><a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/Estatconsolida.pdf" target="_blank">&raquo; {$LABEL_TC7_ITEM8}</a></div>
                        </div>
                     </div>
                     <div id="taula3c">
                        <div id="taula3ccol">
                           <div id="columna1" style="font-size: 16px; background:#ED1F33; opacity: 0.86; color:#fff; text-align:center">{$LABEL_TC7_AREA2}</div>
                           <div id="columna2" style="font-size: 16px; background:#f6f6f6; color:#fff; text-align:center"></div>
                           <div id="columna3" style="font-size: 16px; background:#ED1F33; opacity: 0.86; color:#fff; text-align:center">{$LABEL_TC7_AREA3}</div>
                        </div>
                        <div id="taula3ccol">
                           <div id="columna1" style="font-size: 14px; background:#ED1F33; opacity: 0.56; color:#fff; text-align:center">{$LABEL_TC7_AREA21}</div>
                           <div id="columna2"></div>
                           <div id="columna3" align="center"><a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/SMGUMTSA/p2017.pdf" target="_blank"><img src="/images/infeco/bGumtsa.jpg" width="186" height="40" border="0"></a></div>
                        </div>
                        <div id="taula3ccol">
                           <div id="columna1"><a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/IngressosComparativa.pdf" target="_blank">&raquo; {$LABEL_TC7_ITEM211}</a></div>
                           <div id="columna2"></div>
                           <div id="columna3" align="center"><a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/SMTortosaMedia/p2017.pdf" target="_blank"><img src="/images/infeco/bMedia.jpg" width="186" height="40" border="0"></a></div>
                        </div>
                        <div id="taula3ccol">
                           <div id="columna1"><a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/IngressosClaseconomica.pdf" target="_blank">&raquo; {$LABEL_TC7_ITEM212}</a></div>
                           <div id="columna2"></div>
                           <div id="columna3" align="center"><a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/SMTortosaSport/p2017.pdf" target="_blank"><img src="/images/infeco/bSport.jpg" width="186" height="40" border="0"></a></div>
                        </div>
                        <div id="taula3ccol">
                           <div id="columna1"><a href="http://www.tortosa.cat/webajt/pressupost/2017/ingressos2017.csv" target="_self">&raquo; {$LABEL_TC7_ITEM213} &nbsp;&nbsp;<img src="/images/infeco/iopendata.jpg" border="0" alt="dades obertes" title="dades obertes"></a></div>
                           <div id="columna2"></div>
                           <div id="columna3" align="center"><a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/SMGESAT/p2017.pdf" target="_blank"><img src="/images/infeco/bgesat.jpg" width="186" height="40" border="0"></a></div>
                        </div>
                        <div id="taula3ccol">
                           <div id="columna1" style="font-size: 14px; background:#ED1F33; opacity: 0.56; color:#fff; text-align:center">{$LABEL_TC7_AREA22}</div>
                           <div id="columna2"></div>
                           <div id="columna3" align="center"><a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/SMTortosaSalut/p2017.pdf" target="_blank"><img src="/images/infeco/bsalut.jpg" width="186" height="40" border="0"></a></div>
                        </div>
                        <div id="taula3ccol">
                           <div id="columna1"><a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/DespesesComparativa.pdf" target="_blank">&raquo; {$LABEL_TC7_ITEM211}</a></div>
                           <div id="columna2"></div>
                           <div id="columna3" align="center"><a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/EPEHospital/p2017.pdf" target="_blank"><img src="/images/infeco/bepel.jpg" width="186" height="40" border="0"></a></div>
                        </div>
                        <div id="taula3ccol">
                           <div id="columna1"><a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/DespesesClaseconomica.pdf" target="_blank">&raquo; {$LABEL_TC7_ITEM212}</a></div>
                           <div id="columna2"></div>
                           <div id="columna3" align="center"><a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/ConRutaReis/p2017.pdf" target="_blank"><img src="/images/infeco/b3reis.jpg" width="186" height="40" border="0"></a></div>
                        </div>
                        <div id="taula3ccol">
                           <div id="columna1"><a href="http://www.tortosa.cat/webajt/pressupost/2017/despeses2017.csv" target="_self">&raquo; {$LABEL_TC7_ITEM223} &nbsp;&nbsp;<img src="/images/infeco/iopendata.jpg" border="0" alt="dades obertes" title="dades obertes"></a></div>
                           <div id="columna2"></div>
                           <div id="columna3" align="center"><a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/SMXEMSP/p2017.pdf" target="_blank"><img src="/images/infeco/bemsp.jpg" width="186" height="40" border="0"></a></div>
                        </div>
                        <div id="taula3ccol">
                           <div id="columna1" style="font-size: 14px; background:#ED1F33; opacity: 0.56; color:#fff; text-align:center">{$LABEL_TC7_AREA23}</div>
                           <div id="columna2"></div>
                           <div id="columna3" align="center"><a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/SMXEscorxador/p2017.pdf" target="_blank"><img src="/images/infeco/bescorxa.jpg" width="186" height="40" border="0"></a></div>
                        </div>
                        <div id="taula3ccol">
                           <div id="columna1"><a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/Plantillapersonal2017.pdf" target="_blank">&raquo; {$LABEL_TC7_ITEM231}</a></div>
                           <div id="columna2"></div>
                           <div id="columna3" style="border-bottom: 0px solid #eee;"></div>
                        </div>
                        <div id="taula3ccol">
                           <div id="columna1"><a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/PFI.pdf" target="_blank">&raquo; {$LABEL_TC7_ITEM232}</a></div>
                           <div id="columna2"></div>
                           <div id="columna3" style="border-bottom: 0px solid #eee;"></div>
                        </div>
                        <div id="taula3ccol">
                           <div id="columna1"><a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/Estatdeldeute.pdf" target="_blank">&raquo; {$LABEL_TC7_ITEM233}</a></div>
                           <div id="columna2"></div>
                           <div id="columna3" style="border-bottom: 0px solid #eee;"></div>
                        </div>
                        <div id="taula3ccol">
                           <div id="columna1"><a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/IEF.pdf" target="_blank">&raquo; {$LABEL_TC7_ITEM234}</a></div>
                           <div id="columna2"></div>
                           <div id="columna3" style="border-bottom: 0px solid #eee;"></div>
                        </div>
                     </div>
                  </div>

                  <div class="info-econoc-finan-responsive">
                     <p><b>Documents</b></p>
                     <ul>
                        <li><a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/tparticip.pdf" target="_blank">&raquo; {$LABEL_TC7_ITEM1}</a></li>
                        <li><a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/Memalc.pdf" target="_blank">&raquo; {$LABEL_TC7_ITEM2}</a></li>
                        <li><a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/GrupComparativa.pdf" target="_blank">&raquo; {$LABEL_TC7_ITEM3}</a></li>
                        <li><a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/BE2017.pdf" target="_blank">&raquo; {$LABEL_TC7_ITEM4}</a></li>
                        <li><a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/evolucioPressupost.pdf" target="_blank">&raquo; {$LABEL_TC7_ITEM5}</a></li>
                        <li><a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/InformeInt.pdf" target="_blank">&raquo; {$LABEL_TC7_ITEM6}</a></li>
                        <li><a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/InformeIntEstab.pdf" target="_blank">&raquo; {$LABEL_TC7_ITEM7}</a></li>
                        <li><a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/Estatconsolida.pdf" target="_blank">&raquo; {$LABEL_TC7_ITEM8}</a></li>
                     </ul>

                     <p><b>Ajuntament -</b> Estat ingressos</p>
                     <ul>
                        <li>
                           <a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/IngressosComparativa.pdf" target="_blank">&raquo; {$LABEL_TC7_ITEM211}</a>
                        </li>
                        <li>
                           <a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/IngressosClaseconomica.pdf" target="_blank">&raquo; {$LABEL_TC7_ITEM212}</a> 
                        </li>
                        <li>
                           <a href="http://www.tortosa.cat/webajt/pressupost/2017/ingressos2017.csv" target="_self">&raquo; {$LABEL_TC7_ITEM213} &nbsp;&nbsp;<img src="/images/infeco/iopendata.jpg" border="0" alt="dades obertes" title="dades obertes"></a>
                        </li>
                     </ul>

                     <p><b>Ajuntament -</b> Estat de despeses</p>
                     <ul>
                        <li>
                           <a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/DespesesComparativa.pdf" target="_blank">&raquo; {$LABEL_TC7_ITEM211}
                           </a>
                        </li>
                        <li>
                            <a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/DespesesClaseconomica.pdf" target="_blank">&raquo; {$LABEL_TC7_ITEM212}</a>
                        </li>
                        <li>
                           <a href="http://www.tortosa.cat/webajt/pressupost/2017/despeses2017.csv" target="_self">&raquo; {$LABEL_TC7_ITEM223} &nbsp;&nbsp;<img src="/images/infeco/iopendata.jpg" border="0" alt="dades obertes" title="dades obertes"></a>
                        </li>
                     </ul>

                     <p><b>Ajuntament -</b> Annexos</p>
                     <ul>
                        <li>
                           <a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/Plantillapersonal2017.pdf" target="_blank">&raquo; {$LABEL_TC7_ITEM231}</a>
                        </li>
                        <li>
                            <a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/PFI.pdf" target="_blank">&raquo; {$LABEL_TC7_ITEM232}</a>
                        </li>
                        <li>
                           <a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/Estatdeldeute.pdf" target="_blank">&raquo; {$LABEL_TC7_ITEM233}</a>
                        </li>
                        <li>
                           <a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/IEF.pdf" target="_blank">&raquo; {$LABEL_TC7_ITEM234}</a>
                        </li>
                     </ul>

                     <p><b>Ens dependents</b></p>
                     <ul>
                        <li>
                           <a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/SMGUMTSA/p2017.pdf" target="_blank"><img src="/images/infeco/bGumtsa.jpg" width="186" height="40" border="0">
                           </a>
                        </li>
                        <li>
                            <a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/SMTortosaMedia/p2017.pdf" target="_blank"><img src="/images/infeco/bMedia.jpg" width="186" height="40" border="0"></a>
                        </li>
                        <li>
                           <a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/SMTortosaSport/p2017.pdf" target="_blank"><img src="/images/infeco/bSport.jpg" width="186" height="40" border="0"></a>
                        </li>
                        <li>
                           <a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/SMGESAT/p2017.pdf" target="_blank"><img src="/images/infeco/bgesat.jpg" width="186" height="40" border="0"></a>
                        </li>
                        <li>
                           <a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/SMTortosaSalut/p2017.pdf" target="_blank"><img src="/images/infeco/bsalut.jpg" width="186" height="40" border="0"></a>
                        </li>
                        <li>
                           <a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/EPEHospital/p2017.pdf" target="_blank"><img src="/images/infeco/bepel.jpg" width="186" height="40" border="0"></a>
                        </li>
                        <li>
                           <a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/ConRutaReis/p2017.pdf" target="_blank"><img src="/images/infeco/b3reis.jpg" width="186" height="40" border="0"></a>
                        </li>
                        <li>
                           <a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/SMXEMSP/p2017.pdf" target="_blank"><img src="/images/infeco/bemsp.jpg" width="186" height="40" border="0"></a>
                        </li>
                        <li>
                           <a href="http://www.tortosa.cat/webajt/pressupost/2017/pdf/SMXEscorxador/p2017.pdf" target="_blank"><img src="/images/infeco/bescorxa.jpg" width="186" height="40" border="0"></a>
                        </li>
                     </ul>

                  </div>
                        
               </div>
            </div>
         </div>
      </div>
      {include file="footer.tpl"}
   </body>
</html>