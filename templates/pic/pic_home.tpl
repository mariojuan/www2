<div id="page-wrap">
<div id="conttranstotalcos">
	<div id="conttranscos">
		<div id="btranspresen">
			<div id="transimatge">			
				<img src={$image}>			
			</div>
			<div id="transimatgeText">
				<h1>
				{$LABEL_TITOL1}
				</h1>
			</div>
		</div>
		<div id="tcentre_municipi">
			<p>{$LABEL_PARAG1}</p>
			<ul>
				<li>{$LABEL_PARAG2}</li>
				<li>{$LABEL_PARAG3}</li>
				<li>{$LABEL_PARAG4}</li>
			</ul>
			<p style="text-align:center">{$LABEL_PARAG5}</p>
			<p style="text-align:center">{$LABEL_PARAG6}</p>
			<p>{$LABEL_PARAG7}<a href="http://www.tortosa.cat/webajt/cadastre/autoritzacio.pdf" target="_blank">{$LABEL_PARAG8}</a></p>
			<p>{$LABEL_PARAG9}</p>
			<p style="text-align:right"><a href="http://www.sedecatastro.gob.es/" target="_blank">{$LABEL_PARAG10}</a></p>
		</div>
	
	</div>
</div>
</div>
