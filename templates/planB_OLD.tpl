<!-- PLANTILLA AMB NOMÉS UN MENÚ AL CENTRE. EL MENÚ ÉS TIPUS ACORDEÓ -->
<div id="page-wrap">
<div id="conttranstotalcos">
	<div id="conttranscos">
		<div id="btranspresen">
			<div id="transimatge">			
				<img src={$image}>			
			</div>
			<div id="transimatgeText">
				<h1>
				{$LABEL_TITOL1}
				</h1>
			</div>
		</div>
		<ul id='menu' class='ulmenu'>
        {if $menu_dinamic|@count>0}
            {foreach $menu_dinamic as $itemMenu}
                <li id='lititol'>
                    <a href='{if $itemMenu|count==0}{$itemMenu}{/if}'>
                        <i class='{$icon}'></i>
                        {$itemMenu@key}
                    </a>
                    <ul class='ulmenusub'>
                    {foreach $itemMenu as $subitemMenu}
                        <li id='lisubtitol'>
                            <a href='{$subitemMenu[1]}'>
                                {$subitemMenu[0]}
                            </a>
                        </li>
                    {/foreach}
                    </ul>
                </li>
            {/foreach}
        {else}
            {foreach $MENU as $itemMenu}
				{foreach $itemMenu as $dataitemMenu}
					{if $dataitemMenu@index > 0}
						<li id='lisubtitol'>
						<a href={$dataitemMenu}>
						{$dataitemMenu@key}
						</a>
						</li>
					{else}
						<li id='lititol'>
						<a href='#'>
						<i class='{$icon}'></i>
						{$dataitemMenu@key}
						</a>
						<ul class='ulmenusub'>
					{/if}
				{/foreach}
				</ul>
				</li>	
		    {/foreach}
        {/if}
		</ul>
  	</div>
</div>
</div>

<script type="text/javascript" charset="utf-8">
$(function(){
	$('#menu li a').click(function(event){
		var elem = $(this).next();
		if(elem.is('ul')){
			event.preventDefault();
			$('#menu ul:visible').not(elem).slideUp();
			$('#menu li:visible').not(elem.parent("li")).css("background-image", "url(/images/ptranspa/fonsboto1.jpg)");
			elem.slideToggle();
			tipusfons=$(elem.parent('li')).css("background-image");
			fons="url(/images/ptranspa/fonsboto1.jpg)";
			if(tipusfons==fons){
				elem.parent("li").css("background-image", "url(/images/ptranspa/fonsboto2.jpg)");
			}
			else{
				elem.parent("li").css("background-image", "url(/images/ptranspa/fonsboto1.jpg)");
			}
		}
	});

});
</script> 
