<!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->
<div id="page-wrap">

   <div class="contenedor-responsive">

      <div id="conttranstotalcos">

         <div id="conttranscos">

            <div id="btranspresen">

               <div id="transimatge">			
                  <img src="{$image}" class="img-slider-territori"/>			
               </div>

               <div id="transimatgeText">
                  <h1>
                     {$LABEL_TITOL1}
                     <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                  </h1>
               </div>

            </div>

            <ul id='menu_planA'>
               {foreach $MENU as $itemMenu}
               <a href={$itemMenu['link']} target="_{$itemMenu['target']}">
                  <li id='menu_planA_item'>
                     {$itemMenu['name']}
                  </li>
               </a>
               {/foreach}
            </ul>

            <div id="tcentre_planA">
               <h2>{$LABEL_TC0}</h2>
               {if $item==1}
               <p>{$LABEL_TC1}</p>
               <div class="marcterme rectificacio-marcTerme-img-ssanta2017" style="left:120px;"><a href="http://www.tortosa.cat/webajt/ssanta/ssanta18/programa2018.pdf" target="_blank"><img src="/images/ssanta/cartell18.jpg" class="img-responsive"></a></div>
               <div class="separator"></div>
               <p>{$LABEL_TC2}</p>
               <p>{$LABEL_TC3}</p>
               {elseif $item==2}
               <p>{$LABEL_TC1}<br>
                  <a href="http://www.tortosa.cat/webajt/gestiointerna/headlines/partpublica/indexllistaheadlines.asp?codi=5962" target="_blank">
                  <i>{$LABEL_TC1B}</i>
               </p>
               </a>
               <p>{$LABEL_TC2}<br>
                  <a href="http://www.tortosa.cat/webajt/gestiointerna/headlines/partpublica/indexllistaheadlines.asp?codi=5782" target="_blank">
                  <i>{$LABEL_TC1B}</i>
               </p>
               </a>
               {elseif $item==3}
               <p>{$LABEL_TC1}<br>
               <p>{$LABEL_TC2}<br>
                  <a href="http://www.tortosa.cat/webajt/ajunta/consulta/index.asp" target="_blank">
                  <i>{$LABEL_TC2B}</i>
               </p>
               </a>
               {/if}
               <br><br><br>
            </div>

         </div>
      </div>
   </div>
</div>