<!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->

<div id="page-wrap">
<div id="conttranstotalcos">
	<div id="conttranscos">
		<div id="btranspresen">
			<div id="transimatge">			
			<img src={$image} />			
			</div>
			<div id="transimatgeText">
				<h1>
				{$LABEL_TITOL1}
				</h1>
			</div>
		</div>
		<div class="subtitolinfeco" id="subtitolinfeco2">
            {$LABEL_TITOL1b}
        </div>
        <div class="marcback">
                    <a href="{$url_back}">
                    <i class="icon-angle-double-up"></i>
                    </a>
        </div>
		<ul id='menu_planA'>
		{foreach $MENU as $itemMenu}
                <a href={$itemMenu['link']} target="_{$itemMenu['target']}">
                <li id='menu_planA_item'>
                    {$itemMenu['name']}
                </li>
                </a>
        {/foreach}
		</ul>
		<div id="tcentre_planA">
			<h2>{$LABEL_TC0}</h2>
			{if $item==1}
				<p>{$LABEL_TC1}</p>
				<div class="marcterme" style="left:40px;"><img src="/images/ssanta/centrei.jpg"></div>
				<div style="clear:both;"></div>
				<p align="center" style="font-size: 10px"><i>{$LABEL_TC2}</i></p> 
			{elseif $item==2}
				<p>{$LABEL_TC1}</p>
				<p>{$LABEL_TC2}</p>
				<p>{$LABEL_TC3}</p>
				<p>{$LABEL_TC4}</p>
				<p>{$LABEL_TC5}</p>
				<p>{$LABEL_TC6}</p>
				<p>{$LABEL_TC7}</p>
				<p>{$LABEL_TC8}</p>
				<p>{$LABEL_TC9}</p>
				<p>{$LABEL_TC10}</p>
				<p>{$LABEL_TC11}</p>
			{elseif $item==3}
				<ul class="subvencions1">
					{foreach $MPASSOS as $itemMenu}
                		<li>
                			<i class="icon-file-pdf"></i>&nbsp;
                			<a href="{$itemMenu['link']}" target="_blank">
                			{$itemMenu['name']}
                			</a>
                		</li>
        			{/foreach}
				</ul>
			{elseif $item==4}
				<p><strong>{$LABEL_TC1}</strong></p>
				<p>{$LABEL_TC2}</p>
				<p>{$LABEL_TC3}</p>
				<p><strong>{$LABEL_TC4}</strong></p>
				<p>{$LABEL_TC5}</p>
				<p><strong>{$LABEL_TC6}</strong></p>
				<p>{$LABEL_TC7}</p>
			{elseif $item==5}
				<p><strong>{$LABEL_TC1}</strong>{$LABEL_TC2}</p>
				<p><strong>{$LABEL_TC3}</strong>{$LABEL_TC4}</p>
				<p><strong>{$LABEL_TC5}</strong>{$LABEL_TC6}</p>
				<p><strong>{$LABEL_TC7}</strong><a href="mailto:{$LABEL_TC8}">{$LABEL_TC8}</a></p>
			{/if}
			<br><br><br>
		</div>
  	</div>
</div>
</div>


