<!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->
<div id="page-wrap">

   <div class="contenedor-responsive">

      <div id="conttranstotalcos">

         <div id="conttranscos">

            <div id="btranspresen">

               <div id="transimatge">			
                  <img src="{$image}" class="img-slider-territori"/>			
               </div>

               <div id="transimatgeText">
                  <h1>
                     {$LABEL_TITOL1}
                     <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                  </h1>
               </div>

            </div>

            <div class="subtitolinfeco" id="subtitolinfeco2">
               {$LABEL_TITOL1b}
            </div>

            <div class="marcback">
               <a href="{$url_back}">
               <i class="icon-angle-double-up"></i>
               </a>
            </div>

            <ul id='menu_planA'>
               {foreach $MENU as $itemMenu}
               <a href={$itemMenu['link']} target="_{$itemMenu['target']}">
                  <li id='menu_planA_item'>
                     {$itemMenu['name']}
                  </li>
               </a>
               {/foreach}
            </ul>

            <div id="tcentre_planA">
               <h2>{$LABEL_TC0}</h2>
               {if $item==1}
               <p>{$LABEL_TC1}</p>
               <div class="separator"></div>
               <div class="marcterme rectificacio-marcTerme-img-ssanta2017" style="left:170px;"><img src="/images/ssanta/img1.jpg" class="img-responsive"></div>
               <div class="separator"></div>
               <p>{$LABEL_TC2}</p>
               <p>{$LABEL_TC3}</p>
               <p>{$LABEL_TC4}</p>
               <p>{$LABEL_TC5}</p>
               <p>{$LABEL_TC6}</p>
               <p>{$LABEL_TC7}</p>
               {elseif $item==2}
               <ul class="subvencions1">
                  {foreach $MISTERIS36 as $itemMenu}
                  <li>
                     <p style="margin:0px"><strong>{$itemMenu['name']}</strong></p>
                     <p style="margin:0px">{$LABEL_AUTOR}{$itemMenu['autor']}</p>
                     <p style="margin-top:0px; margin-bottom:20px">{$LABEL_DATA}{$itemMenu['data']}</p>
                  </li>
                  {/foreach}
               </ul>
               {elseif $item==3}
               <ul class="subvencions1">
                  {foreach $MISTERIS as $itemMenu}
                  <li>
                     <p style="margin:0px"><strong>{$itemMenu['name']}</strong></p>
                     <p style="margin:0px">{$LABEL_AUTOR}{$itemMenu['autor']}</p>
                     <p style="margin-top:0px; margin-bottom:20px">{$LABEL_DATA}{$itemMenu['data']}</p>
                  </li>
                  {/foreach}
               </ul>
               {elseif $item==4}
               <div class="marcterme"><img src="/images/ssanta/img3.jpg"></div>
               <div class="separator"></div>
               <p><strong>{$LABEL_TC1}</strong></p>
               <p>{$LABEL_TC2}</p>
               <p>{$LABEL_TC3}</p>
               <p>{$LABEL_TC4}</p>
               <div class="separator"></div>
               <p><strong>{$LABEL_TC5}</strong></p>
               <p>{$LABEL_TC6}</p>
               <div class="separator"></div>
               <p><strong>{$LABEL_TC7}</strong></p>
               <p>{$LABEL_TC8}</p>
               <div class="separator"></div>
               <p><strong>{$LABEL_TC9}</strong></p>
               <p>{$LABEL_TC10}</p>
               <div class="separator"></div>
               <p><strong>{$LABEL_TC11}</strong></p>
               <p>{$LABEL_TC12}</p>
               <div class="separator"></div>
               <p><strong>{$LABEL_TC13}</strong></p>
               <p>{$LABEL_TC14}</p>
               <div class="separator"></div>
               <p><strong>{$LABEL_TC15}</strong></p>
               <p>{$LABEL_TC16}</p>
               <p>{$LABEL_TC17}</p>
               <p>{$LABEL_TC18}</p>
               <p>{$LABEL_TC19}</p>
               <div class="separator"></div>
               <p><strong>{$LABEL_TC20}</strong></p>
               <p>{$LABEL_TC21}</p>
               <div class="separator"></div>
               <p><strong>{$LABEL_TC22}</strong></p>
               <p>{$LABEL_TC23}</p>
               <p>{$LABEL_TC24}</p>
               <p>{$LABEL_TC25}</p>
               <p>{$LABEL_TC26}</p>
               <p>{$LABEL_TC27}</p>
               <p>{$LABEL_TC28}</p>
               <p>{$LABEL_TC29}</p>
               <p>{$LABEL_TC30}</p>
               <p>{$LABEL_TC31}</p>
               <p>{$LABEL_TC32}</p>
               <p>{$LABEL_TC33}</p>
               <p>{$LABEL_TC34}</p>
               {elseif $item==5}
               <p>{$LABEL_TC1}</p>
               {elseif $item==6}
               <p>{$LABEL_TC1}</p>
               <p>{$LABEL_TC2}</p>
               <p>{$LABEL_TC3}</p>
               <p>{$LABEL_TC4}</p>
               <p>Vicent Josep Ruiz Prades</p>
               <p><strong>{$LABEL_TC5}</strong></p>
               {elseif $item==7}
               <p>{$LABEL_TC1}</p>
               <p>{$LABEL_TC2}</p>
               <p>{$LABEL_TC3}</p>
               <div class="marcterme rectificacio-marcTerme-img-ssanta2017" style="left:160px;"><img src="/images/ssanta/img2.jpg" class="img-responsive"></div>
               <div class="separator"></div>
               <p><strong>{$LABEL_TC4}</strong></p>
               <p>{$LABEL_TC5}</p>
               <p>{$LABEL_TC6}</p>
               <p>{$LABEL_TC7}</p>
               <p>{$LABEL_TC8}</p>
               <p>{$LABEL_TC9}</p>
               <p>{$LABEL_TC10}</p>
               <p>{$LABEL_TC11}</p>
               <div class="separator"></div>
               <p><strong>{$LABEL_TC12}</strong></p>
               <p>{$LABEL_TC13}</p>
               <div class="separator"></div>
               <p><strong>{$LABEL_TC14}</strong></p>
               <p>{$LABEL_TC15}</p>
               <p>{$LABEL_TC16}</p>
               <p>{$LABEL_TC17}</p>
               <div class="separator"></div>
               <p><strong>{$LABEL_TC18}</strong></p>
               <p>{$LABEL_TC19}</p>
               <p>{$LABEL_TC20}</p>
               <p>{$LABEL_TC21}</p>
               <p>{$LABEL_TC22}</p>
               <p>{$LABEL_TC23}</p>
               <p>{$LABEL_TC24}</p>
               <p>{$LABEL_TC25}</p>
               <p>{$LABEL_TC26}</p>
               <p>{$LABEL_TC27}</p>
               <div class="separator"></div>
               <p><strong>{$LABEL_TC28}</strong></p>
               <p>{$LABEL_TC29}</p>
               <p>{$LABEL_TC30}</p>
               <div class="separator"></div>
               <p><strong>{$LABEL_TC31}</strong></p>
               <p>{$LABEL_TC32}</p>
               <p>{$LABEL_TC33}</p>
               <div class="separator"></div>
               <p><strong>{$LABEL_TC34}</strong></p>
               <p>{$LABEL_TC35}</p>
               <div class="separator"></div>
               <p><strong>{$LABEL_TC36}</strong></p>
               <p>{$LABEL_TC37}</p>
               {elseif $item==8}
               <ul class="subvencions1">
                  <li><i class="icon-ok"></i>{$LABEL_TC1}</li>
                  <li><i class="icon-ok"></i>{$LABEL_TC2}</li>
                  <li><i class="icon-ok"></i>{$LABEL_TC3}</li>
                  <li><i class="icon-ok"></i>{$LABEL_TC4}</li>
               </ul>
               {/if}
               <br><br><br>

            </div>
         </div>
      </div>
   </div>
</div>