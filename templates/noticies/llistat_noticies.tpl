﻿<html>
    <head>
        {include file="head.tpl"}
    </head>
	<body id="headlines_list">
		{include file="header.tpl"}
        <div id="conttranstotalcos">
            <div id="conttranscos">
                <div id="btranspresen">
                    <div id="transimatge">
                        <img src="/images/butlletins/tira.jpg" />
                    </div>
                    <div id="transimatgeText">
                        <h1>
                            {$LABEL_TITOL1}
                        </h1>
                    </div>
                </div>
                <ul id='menu_planA'>
                    {foreach $MENU as $itemMenu}
                        <a href={$itemMenu}>
                            <li id='menu_planA_item'>
                                {$itemMenu@key}
                            </li>
                        </a>
                    {/foreach}
                </ul>
                <div id="tcentre_planA">
                    <div class="search_box">
                        <form name="search_headlines" method="post" action="{$destination_form}">
                            <div class="item_form_container di_container">
                                <label>{$lblDataInici}</label>
                                <input type="text" name="data_inici" class="search_di" value="{$data_inici}">
                            </div>
                            <div class="item_form_container df_container">
                                <label>{$lblDataFi}</label>
                                <input type="text" name="data_fi" class="search_df" value="{$data_fi}">
                            </div>
                            <div class="item_form_container text_container">
                                <input type="text" name="text" class="sear_text" value="{$text}">
                            </div>
                            <div class="item_form_container submit_container">
                                <input type="submit" name="search_submit" value="{$lblCercar}">
                            </div>
                        </form>
                    </div>
                    {if $items|@count>0}
                        {foreach $items as $item}
                            <div class="headline_item_list">
                                <div class="headline_item_img">
                                {if $item->FOTO|trim ==""}
                                    <img src="/images/headlines/inotimage.jpg" width="220" height="125">
                                {else}
                                    {if $item->IMPORT=="0"}
                                        <img src="/images/headlines/min_{$item->FOTO}" width="220" height="125">
                                    {else}
                                        <img src="http://www.tortosa.cat/webajt/gestiointerna/headlines/fotos/{$item->FOTO}" width="220" height="125">
                                    {/if}
                                {/if}
                                </div>
                                <div class="headline_item_data">{$item->DATA|date_format:"%d/%m/%G"}</div>
                                <div class="headline_title_item">{$item->TITOL|truncate:75:"":false}</div>
                                <div class="headline_subtitle_item"><span>{$tematiques[$item->ID]}.</span> {$item->SUBTITOL|truncate:100:"...":false}</div>
                                <div class="headline_link_item"><a href="/noticies/noticia.php?lang={$lang}&id={$item->ID}">{$lblMesInfo}</a></div>
                            </div>
                        {/foreach}
                    {else}
                        <div class="headline_no_data">
                            {$lblNoData}
                        </div>
                    {/if}
                    <div id="pagination">
                        <ul>
                            {if $lastpage>1}
                                {if $page> 1}
                                    {assign var="page_ant" value=$page - 1}
                                    <li><a href="index.php?accio=list&page={$page_ant}&data_inici={$data_inici}&data_fi={$data_fi}&text={$text}&lang={$lang}">{$lblAnt}</a></li>
                                {/if}
                                {assign var="desp" value=$page + 4}
                                {assign var="firstpage" value=$page - 4}

                                {if $desp < $lastpage}
                                    {assign var="lastpage" value=$page + 4}
                                {/if}
                                {if $firstpage < 1}
                                    {assign var="firstpage" value=1}
                                {/if}

                                {for $counter=$firstpage to $lastpage}
                                    {if $counter==$page}
                                        <li>{$counter}</li>
                                    {else}
                                        <li><a href="index.php?accio=list&page={$counter}&data_inici={$data_inici}&data_fi={$data_fi}&text={$text}&lang={$lang}">{$counter}</a></li>
                                    {/if}
                                {/for}
                                {if $page < $lastpage - 1}
                                    {assign var="page_seg" value=$page + 1}
                                    <li><a href="index.php?accio=list&page={$page_seg}&data_inici={$data_inici}&data_fi={$data_fi}&text={$text}&lang={$lang}">{$lblSeg}</a></li>
                                {/if}
                            {/if}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $().ready(function() {
                $(".search_di").datepicker();
                $(".search_df").datepicker();
            });
        </script>
		{include file="footer.tpl"}
	</body>
</html>
