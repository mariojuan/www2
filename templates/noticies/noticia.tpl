<html>
    <head>
        {include file="head.tpl"}

        <!-- PrettyPhoto -->

        <link rel="stylesheet" href="/css/prettyPhoto.css">
        <script type="text/javascript" src="/js/imagesloaded.pkgd.min.js"></script>
        <script type="text/javascript" src="/js/jquery.prettyPhoto.js"></script>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function(){
                $("a[rel^='prettyPhoto']").prettyPhoto({
                    allow_resize
                });
        </script>
    </head>
	<body>
		{include file="header.tpl"}
        <div id="page-wrap">
            <div id="conttranstotalcos">
                <div id="conttranscos">
                    <div id="btranspresen">
                        <div id="transimatge">
                            <img src=/images/butlletins/tira.jpg />
                        </div>
                    </div>
                    <div id="transimatgeText">
                        <h1>
                            {$LABEL_TITOL1}
                        </h1>
                    </div>
                    <div id='caixadatanoticia'>
                        {$LABEL_TC1} {$date_publish}
                    </div>
                    <div class='caixatextnoticia'>
                        <div id='noticia_titol'>{$TITOL}</div>
                        <div id='noticia_subtitol'>{$SUBTITOL}</div>
                        <div id='noticia_cos'>{$COS}</div>
                    </div>

                    <div id='noticia_foto'>

                        <a href="http://www2.tortosa.cat/images/headlines/res_{$IMG_NAME_ON_DB}" rel="prettyPhoto">{$IMAGE}</a>

                    </div>

                    <div id='sidebar'>
                        <h3>{$LABEL_TITOL3}</h3>
                        <ul>
                            {foreach $LABEL_TC_TIPOLOGIA as $tipologia_item}
                                <li>{$tipologia_item}</li>
                            {/foreach}
                        </ul>
                        <h3>{$LABEL_TITOL2}</h3>
                        <ul>
                        {foreach $LABEL_TC_TEMATICA as $tematica_item}
                            <li>{$tematica_item}</li>
                        {/foreach}
                        </ul>
                        {if $LINK!=""}
                        <div>
                            <h3>{$LABEL_TITOL4}</h3>
                            <p><a href="{$LINK}" target="_blank"><i class="icon-link"></i></a></p>
                        </div>
                        {/if}
                        {if $ADJUNT!=""}
                            <div>
                                <h3>{$LABEL_TITOL4}</h3>
                                <p><a href="http://www2.tortosa.cat/public_files/headlines/{$ADJUNT}" target="_blank"><i class="icon-link"></i></a></p>
                            </div>
                        {/if}
                    </div>

                    <div id='noticia_espaipeu'></div>
                </div>

            </div>
        </div>
		{include file="footer.tpl"}

        <script type="text/javascript" charset="utf-8">
            $(document).ready(function(){
                $("a[rel^='prettyPhoto']").prettyPhoto();
            });
        </script>

	</body>
</html>