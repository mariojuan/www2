<!-- PLANTILLA SENSE MENÚ A L'ESQUERRA AMB FILES DE 3 COLUMNES DE CAIXES/BOTONS -->
<link rel='stylesheet' href='/fonts/fontello-a238a014/css/fontello.css'>
<link rel='stylesheet' href='/fonts/fontello-a238a014/css/animation.css'>

<div id="page-wrap">

<div class="contenedor-responsive">

   <div id="conttranstotalcos">

      <div id="conttranscos">

         <div id="btranspresen">

            <div id="transimatge">			
               <img src="{$image}" class="img-slider-territori">			
            </div>

            <div id="transimatgeText">
               <h1>
                  {$LABEL_TITOL1}
               </h1>
            </div>

         </div>

         <div id="contmenuquad">
            {$ntitol = $ntitol-1}

            <div class="div-marge-ciutatPlanols">

            {for $ctitol=0 to $ntitol}
            <a href={$URL[$ctitol]} targe="_blank">
               {if fmod($ctitol, 3)==0}
               <div class="menuquad" id='menuquad0'>
                  {else}
                  <div class='menuquad'>
                     {/if}
                     <div id="menuquad_text">
                        {$MENU[$ctitol]}
                     </div>
                     <div id="menuquad_subtext">
                        <i class='{$ICON[$ctitol]}'></i><br>
                        {$PES[$ctitol]}
                     </div>
                  </div>
            </a>
            {/for}
            </div>

            </div>

         </div>
      </div>
   </div>
</div>