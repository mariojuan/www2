<html>
	<head>
        {include file="head.tpl"}
	</head>
	<body id="indicadors">
        {include file="header.tpl"}
        <!-- PLANTILLA AMB NOMÉS UN MENÚ AL CENTRE. EL MENÚ ÉS TIPUS ACORDEÓ -->
        <div id="page-wrap">
            <div class="contenedor-responsive">
                <div id="conttranstotalcos">
                    <div id="conttranscos">
                        <div id="btranspresen">
                            <div id="transimatge">
                                <img src="/images/indicadors-transparencia/tira.jpg" class="img-slider-territori">
                            </div>
                            <div id="transimatgeText">
                                <h1>
                                    {$LABEL_TITOL1}
                                </h1>
                            </div>
                        </div>
                        <div class="subtitolinfeco" id="subtitolinfeco2">
                            {$LABEL_TITOL2}
                        </div>
                        <div class="marcback">
                            <a href="/indicadors-transparencia/?lang={$lang}">
                                <i class="icon-angle-double-up"></i>
                            </a>
                        </div>
                        <div style="padding-top: 15px; clear: both">
                            {$LABEL_TXTINTRO}
                        </div>
                        <ul id='menu' class='ulmenu' style="padding-left: 5px">
                            {if $grups|@count>0}
                                {foreach $grups as $grup}
                                    <li>
                                        <a href>
                                            {$grup->CODI_GRUP_INDICADOR} - {$grup->GRUP_INDICADOR}
                                        </a>
                                        <!-- Subgrups -->
                                        {assign var="hihasubgrups" value="0"}
                                        {foreach $subgrups as $subgrup}
                                            {if $subgrup->ID_GRUP == $grup->ID}
                                                {if $subgrup@key==0}
                                                    <ul class='ulmenusub'>
                                                    {assign var="hihasubgrups" value="1"}
                                                {/if}
                                                <li>
                                                    <a href>
                                                        {$subgrup->CODI_SUBGRUP_INDICADOR} - {$subgrup->SUBGRUP_INDICADOR}
                                                    </a>
                                                    <!-- Indicadors -->
                                                    {assign var="hihaindicadors" value="0"}
                                                    {foreach $indicadors as $indicador}
                                                        {if $indicador->ID_GRUP == $grup->ID && $indicador->ID_SUBGRUP == $subgrup->ID}
                                                            {if $indicador@key==0}
                                                                <ul class='llistat_indicadors'>
                                                                {assign var="hihaindicadors" value="1"}
                                                            {/if}
                                                            <li>
                                                                {if $indicador->LINK!=""}
                                                                    <i class="icon-link" style="float: left; padding-top: 8px;"></i><a href="{$indicador->LINK}" target="_blank">{$indicador->CODI_INDICADOR} - {$indicador->INDICADOR}</a>
                                                                {else}
                                                                <label style="font-weight: 700; font-size: 14px;">{$indicador->CODI_INDICADOR} - {$indicador->INDICADOR}</label>
                                                                {/if}
                                                                {if $indicador->COMENTARIS!=""}
                                                                <p>
                                                                    {$indicador->COMENTARIS}
                                                                </p>
                                                                {/if}
                                                                {foreach $indicadors_links as $link}
                                                                    {if $link->ID_INDICADOR==$indicador->ID}
                                                                    <p>
                                                                        <i class="icon-link" style="float: left; padding-top: 8px;"></i><a href="{$link->LINK}" target="_blank">{$link->DESCRIPCIO}</a>
                                                                    </p>
                                                                    {/if}
                                                                {/foreach}
                                                            </li>
                                                        {/if}
                                                    {/foreach}
                                                    {if $hihaindicadors == 1}
                                                        </ul>
                                                    {/if}
                                                </li>
                                            {/if}
                                        {/foreach}
                                        {if $hihasubgrups == 1}
                                        </ul>
                                        {/if}
                                        <!-- Indicadors sense subgrup -->
                                        <ul class='llistat_indicadors ulmenusub'>
                                            {foreach $indicadors as $indicador}
                                                {if $indicador->ID_SUBGRUP == 0 && $indicador->ID_GRUP == $grup->ID}
                                                <li style="margin-bottom: 20px">
                                                    {if $indicador->LINK!=""}
                                                    <a href="{$indicador->LINK}" target="_blank">{$indicador->CODI_INDICADOR} - {$indicador->INDICADOR}</a>
                                                    {else}
                                                        <label style="font-weight: 700; font-size: 14px;">{$indicador->CODI_INDICADOR} - {$indicador->INDICADOR}</label>
                                                    {/if}
                                                    <p>
                                                        {$indicador->COMENTARIS}
                                                    </p>
                                                    {foreach $indicadors_links as $link}
                                                        {if $link->ID_INDICADOR==$indicador->ID}
                                                            <p>
                                                                <i class="icon-link" style="float: left; padding-top: 8px;"></i><a href="{$link->LINK}" target="_blank">{$link->DESCRIPCIO}</a>
                                                            </p>
                                                        {/if}
                                                    {/foreach}
                                                </li>
                                                {/if}
                                            {/foreach}
                                        </ul>
                                    </li>
                                {/foreach}
                            {/if}
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript" charset="utf-8">
            $(function(){
                $('#menu li a').click(function(event){
                    var elem = $(this).next();
                    var elem1 = $(this).next().next();

                    if(elem.is('ul')){
                        event.preventDefault();
                        //$('#menu ul:visible').not(elem).slideUp();
                        $('#menu > li:visible').not(elem.parent("li")).css("background-image", "url(/images/ptranspa/fonsboto1.jpg)");
                        elem.slideToggle();
                        elem1.slideToggle();
                        tipusfons=$(elem.parent('> li')).css("background-image");
                        fons="url(/images/ptranspa/fonsboto1.jpg)";
                        if(tipusfons==fons){
                            elem.parent("li").css("background-image", "url(/images/ptranspa/fonsboto2.jpg)");
                        }
                        else{
                            elem.parent("li").css("background-image", "url(/images/ptranspa/fonsboto1.jpg)");
                        }
                    }
                });

            });
        </script>

        {include file="footer.tpl"}
	</body>
</html>