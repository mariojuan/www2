<!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->
<div id="page-wrap">

    <div class="contenedor-responsive">

        <div id="conttranstotalcos">

            <div id="conttranscos">

                <div id="btranspresen">

                    <div id="transimatge">
                        <img src="{$image}" class="img-slider-territori"/>
                    </div>

                    <div id="transimatgeText">
                        <h1>
                            {$LABEL_TITOL1}

                            <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>

                        </h1>
                    </div>

                </div>

                <ul id='menu_planA'>
                    {foreach $MENU as $itemMenu}
                        <a href={$itemMenu}>
                            <li id='menu_planA_item'>
                                {$itemMenu@key}
                            </li>
                        </a>
                    {/foreach}
                </ul>

                <div id="tcentre_planA">
                    {$LABEL_TC1}
                    <div style="margin-top: 25px">
                        <div style="float: left; margin: 0 10px 10px 0">
                            <img src="/images/infoparticipa/2013.jpg" style="width: 200px">
                        </div>
                        <div style="float: left; margin: 0 10px 10px 0">
                            <img src="/images/infoparticipa/2014.jpg" style="width: 200px">
                        </div>
                        <div style="float: left; margin: 0 10px 10px 0">
                            <img src="/images/infoparticipa/2015.jpg" style="width: 200px">
                        </div>
                        <div style="float: left; margin: 0 10px 10px 0">
                            <img src="/images/infoparticipa/segell-infoparticipa-2016.jpg" style="width: 200px">
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>