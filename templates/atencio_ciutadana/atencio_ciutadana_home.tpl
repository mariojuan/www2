<!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->
<div id="page-wrap">

   <div class="contenedor-responsive">

      <div id="conttranstotalcos">
         <div id="conttranscos">

            <div id="btranspresen">

               <div id="transimatge">
                  <img src="{$image}" class="img-slider-territori"/>
               </div>

               <div id="transimatgeText">
                  <h1>
                     {$LABEL_TITOL1}
                     <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                  </h1>
               </div>

            </div>

            <ul id='menu_planA'>
               {foreach $MENU as $itemMenu}
               <a href={$itemMenu['link']} target="_{$itemMenu['target']}">
                  <li id='menu_planA_item'>
                     {$itemMenu['name']}
                  </li>
               </a>
               {/foreach}
            </ul>

            <div id="tcentre_planA">
               {if $type==1}
               <!-- Presentació Atenció Ciutadana-->
               <h2>{$LABEL_TC0}</h2>
               <p>{$LABEL_TC1}</p>
               {elseif $type==2}
               <!--Horaris d'atenció -->
               <h2>{$LABEL_TC0}</h2>
               <p>{$LABEL_TC1}</p>
               <ul class="subvencions1">
                  <li> <i class="icon-bank"> </i> &nbsp;
                     {$LABEL_TC2} <a href="http://www.seu-e.cat/web/tortosa" target="_blank">{$LABEL_TC3} </a>
                  </li>
                  <li >
                     <i class="icon-bank"> </i> &nbsp;
                     {$LABEL_TC4} <b> {$LABEL_TC5} </b>
                     <ul>
                        <li> 
                           {$LABEL_TC6}
                        </li>
                        <li> 
                           {$LABEL_TC7}
                        </li>
                     </ul>
                  </li>
                  <li><i class="icon-bank"> </i> &nbsp;
                     {$LABEL_TC8}
                  </li>
                  <li>
                     <i class="icon-bank"> </i> &nbsp;
                     {$LABEL_TC9} <b> {$LABEL_TC10} </b>
                     <ul>
                        <li> 
                           {$LABEL_TC11}
                        </li>
                        <li> 
                           {$LABEL_TC12}
                        </li>
                        <li> 
                           {$LABEL_TC13}
                        </li>
                     </ul>
                  </li>
                  <li>
                     <i class="icon-bank"> </i> &nbsp;
                     {$LABEL_TC14} <b> {$LABEL_TC15} </b>
                     <ul>
                        <li> 
                           {$LABEL_TC16}
                        </li>
                        <li> 
                           {$LABEL_TC17}
                        </li>
                        <li> 
                           {$LABEL_TC18}
                        </li>
                     </ul>
                  </li>
               </ul>
               {elseif $type==3}
               <!--Seu Electrònica -->
               <h2>{$LABEL_TC0}</h2>
               <p>{$LABEL_TC1}  <a href="http://www.seu-e.cat/web/tortosa" target="_blank">{$LABEL_TC2} </a>
               </p>
               {elseif $type==4}
               <!-- Contacta-->
               <h2>{$LABEL_TC0}</h2>
               <p><b>{$LABEL_TC1}</b> {$LABEL_TC2}</p>
               <p><b>{$LABEL_TC3}</b> {$LABEL_TC4}</p>
               <p><b>{$LABEL_TC5}</b> <a href="mailto:{$LABEL_TC6}" target="_blank">{$LABEL_TC6}</a></p>
               {/if}
            </div>
            
            <div class="separator"></div>
         </div>
      </div>
   </div>
</div>