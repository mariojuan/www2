{include file="header_little.tpl"}


<!-- BARRA SUBHEADER (MENU) -->
<div class="row row-header row-header-menu">

	<!-- TITOL -->
   <div class="titolcap">
      <h1>Tortosa</h1>
   </div>
   <!--/ TITOL -->

   <!-- BOTÓ HOME -->
   
   <div id="bhome">
      {if $lang=='es'}
      <a href="/es/">
      {elseif $lang=='en'}
      <a href="/en/">
      {else}
      <a href="/">
         {/if}
         <div id="menuh_text"><i class='icon-home'></i></div>
      </a>
   </div>
   
   <!--/ BOTÓ HOME -->

   <!-- MENÚ -->

   <div class="row">
   		<p class="menu-responsive">
   			<a href="{if $lang=="ca"}/ciutat/{elseif $lang=="es"} /{$lang}/ciudad/{elseif $lang=="en"} /ciutat/index.php?lang=en{elseif $lang=="fr"} /ciutat/index.php?lang=fr{else}/ciutat/index.php{/if}" hreflang="ca">{$LABEL_MENUH1}
   			</a>
   		</p>
   		<p class="menu-responsive">
   			<a href="{if $lang=="ca"}/ajunta/index.php{elseif $lang=="es"} /ajunta/index.php?lang=es{elseif $lang=="en"} /ajunta/index.php?lang=en {elseif $lang=="fr"}/ajunta/index.php?lang=fr{else}/ajunta/index.php{/if}">{$LABEL_MENUH2}
   			</a>
   		</p>
   		<p class="menu-responsive">
   			<a href="{if $lang=="ca"}/negocis/index.php{elseif $lang=="es"} /negocis/index.php?lang=es{elseif $lang=="en"} /negocis/index.php?lang=en{elseif $lang=="fr"}/negocis/index.php?lang=fr{else}/negocis/index.php{/if}" hreflang="ca">{$LABEL_MENUH4}
   			</a>
   		</p>
   		<p class="menu-responsive">
   			<a href="{if $lang=="ca"}http://www.tortosaturisme.cat{elseif $lang=="es"}http://www.tortosaturisme.cat/es/{elseif $lang=="en"} http://www.tortosaturisme.cat/en/{elseif $lang=="fr"} http://www.tortosaturisme.cat/fr/{else}http://www.tortosaturisme.cat{/if}" hreflang="ca" target="_blank">{$LABEL_MENUH3}
   			</a>
   		</p>
   </div>

   <div id="menuh">
      <div id="bmenu">
         <a href="{if $lang=="ca"}/ciutat/{elseif $lang=="es"} /{$lang}/ciudad/{elseif $lang=="en"} /ciutat/index.php?lang=en{elseif $lang=="fr"} /ciutat/index.php?lang=fr{else}/ciutat/index.php{/if}" hreflang="ca">
         <div id="menuh_text">
            {$LABEL_MENUH1}
         </div>
         </a>
      </div>
      <div id="bmenu">
         <a href="{if $lang=="ca"}/ajunta/index.php{elseif $lang=="es"} /ajunta/index.php?lang=es{elseif $lang=="en"} /ajunta/index.php?lang=en {elseif $lang=="fr"}/ajunta/index.php?lang=fr{else}/ajunta/index.php{/if}">
         <div id="menuh_text">
            {$LABEL_MENUH2}
         </div>
         </a>
      </div>
      <div id="bmenu">
         <a href="{if $lang=="ca"}/negocis/index.php{elseif $lang=="es"} /negocis/index.php?lang=es{elseif $lang=="en"} /negocis/index.php?lang=en{elseif $lang=="fr"}/negocis/index.php?lang=fr{else}/negocis/index.php{/if}" hreflang="ca">
         <div id="menuh_text">
            {$LABEL_MENUH4}
         </div>
         </a>
      </div>
      <div id="bmenu">
         <a href="{if $lang=="ca"}http://www.tortosaturisme.cat{elseif $lang=="es"}http://www.tortosaturisme.cat/es/{elseif $lang=="en"} http://www.tortosaturisme.cat/en/{elseif $lang=="fr"} http://www.tortosaturisme.cat/fr/{else}http://www.tortosaturisme.cat{/if}" hreflang="ca" target="_blank">
         <div id="menuh_text">
            {$LABEL_MENUH3}
         </div>
         </a>
      </div>
   </div>
   
   <!--/ MENÚ -->

</div>
<!--/ BARRA SUBHEADER (MENU) -->