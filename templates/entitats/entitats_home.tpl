<!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->
<div id="page-wrap">

   <div class="contenedor-responsive">

      <div id="conttranstotalcos">

         <div id="conttranscos">

            <div id="btranspresen">

               <div id="transimatge">        
                  <img src="{$image}" class="img-slider-territori"/>       
               </div>

               <div id="transimatgeText">
                  <h1>
                     {$LABEL_TITOL1}

                     <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>

                  </h1>
               </div>

            </div>

            <ul id='menu_planA'>
               {foreach $MENU as $itemMenu}
               <a href={$itemMenu}>
                  <li id='menu_planA_item'>
                     {$itemMenu@key}
                  </li>
               </a>
               {/foreach}
            </ul>

            <div id="tcentre_planA">
               <font size=4 color=#666666>{$LABEL_TC1}</font>
               <p>{$LABEL_TC2}</p>
               {if $item==1}
                  <ul class="entitats">
                     <li><i class="icon-file-pdf"></i><a href="http://www.tortosa.cat/webajt/ajunta/registre_entitats.pdf" target="_blank">{$LABEL_TC3}</a></li>
                     <li><i class="icon-file-pdf"></i><a href="http://www.tortosa.cat/webajt/ajunta/SolidaritatEntitats.pdf" target="_blank">{$LABEL_TC4}</a></li>
                  </ul>
                  <p>{$LABEL_TC5}</p>
                  <ul class="entitats">
                     <li>
                        {$LABEL_TC6}
                        <ul class="entitats">
                           <li><i class="icon-link-ext"></i><a href="http://justicia.gencat.cat/ca/ambits/grups_interes/consulta_grups_interes/index.html" target="_blank">{$LABEL_TC7}</a></li>
                        </ul>
                        <p></p>
                     </li>
                     <li>
                        {$LABEL_TC8}
                        <ul class="entitats">
                           <li><i class="icon-link-ext"></i><a href="http://justicia.gencat.cat/ca/tramits/tramits-temes/registre-grups-interes" target="_blank">{$LABEL_TC9}</a></li>
                        </ul>
                     </li>
                  </ul>
               {else if $item==2}
                  <a href="mailto:protocol@tortosa.cat">{$LABEL_TC3}</a>
               {/if}
            </div>

         </div>
      </div>
   </div>
</div>