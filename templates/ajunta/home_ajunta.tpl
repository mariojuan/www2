<div id="page-wrap">
   <div id="contenidortotalcos">
      <!-- FOTOS SLIDER -->
      <div class="rowCartellera">
         <div id="bciutatpresen" class="bciutatpresen-ajuntament">
            <ul id="bciutatmenu">
               <li >
                  <a href="{$url_lalcalde}">
                     <div id="ciutatbotoimatge">
                        <img src="/images/ajunta/balc.jpg" >
                     </div>
                     <div id="textbmenu1" class="textbotomciutat">
                        {$LABEL_BALC}
                     </div>
                  </a>
               </li>
               <li >
                  <a href="{$url_composicio_actual}">
                     <div id="ciutatbotoimatge">
                        <img src="/images/ajunta/bconsistori.jpg" />
                     </div>
                     <div id="textbmenu2" class="textbotomciutat">
                        {$LABEL_BCONSIS}
                     </div>
                  </a>
               </li>
               <li >
                  <a href="{$url_grups_politics}">
                     <div id="ciutatbotoimatge">
                        <img src="/images/ajunta/bgrups.jpg" />
                     </div>
                     <div id="textbmenu3" class="textbotomciutat">
                        {$LABEL_BGRUPS}&nbsp;{$LABEL_BGRUPSB}
                     </div>
                  </a>
               </li>
               <li >
                  <a href="{$url_defensorc}">
                     <div id="ciutatbotoimatge">
                        <img src="/images/ajunta/bdefensor.jpg" />
                     </div>
                     <div id="textbmenu4" class="textbotomciutat">
                        {$LABEL_DEFENSOR}
                        <br\>
                        {$LABEL_DEFENSORB}
                     </div>
                  </a>
               </li>
            </ul>
            <div id="ciutatimatge">
               <img src="/images/ajunta/ajuntament.jpg" class="img-slider-ajuntament"/>
            </div>
            <div id="ajuntaimatgeText" class="imatgeCapText">
               <h1>
                  {$LABEL_TITOL}
                  &nbsp;&nbsp;
               </h1>
            </div>
         </div>
      </div>
      <!--/ FOTOS SLIDER -->
      <!-- BANNERS -->
      <div class="row">
         <div class="titolsSeccionsHome titolBig marge-titol-contingut">
            {$LABEL_TITOLAJC}&nbsp;<font color="#000">{$LABEL_TITOLAJCB}</font>
         </div>

         <div class="div-banners-ajunta">

         <div class="marcfinestra2 banner2 banner2-correccio">
            <a href="{$url_organs}" target="_self" title="Òrgans de govern">
               <div class="banner-image-ajuntaaunclic">
                  <img src="/images/ajunta/bnorgans.jpg" width="204" height="144">
               </div>
               <div id="textbclic" class="textbotoclic">
                  {$LABEL_BOG}
                  <br>
                  {$LABEL_BOGB}
               </div>
            </a>
         </div>

         <div class="marcfinestra2 banner2 banner2-correccio">
            <a href="{$url_omunicipal}" target="_self" title="Organització municipal">
               <div class="banner-image-ajuntaaunclic">
                  <img src="/images/ajunta/bnorganitza.jpg" width="204" height="144">
               </div>
               <div id="textbclic" class="textbotoclic">
                  {$LABEL_BOM}
                  <br>
                  {$LABEL_BOMB}
               </div>
            </a>
         </div>

         <div class="marcfinestra2 banner2 banner2-correccio">
            <a href="{$url_infeco}" target="_self" title="Informació econòmico financera">
               <div class="banner-image-ajuntaaunclic">
                  <img src="/images/ajunta/bninfeco.jpg" width="204" height="144">        
               </div>
               <div id="textbclic" class="textbotoclic">
                  {$LABEL_BPRESUP}
                  <br>
                  {$LABEL_BPRESUP2}
               </div>
            </a>
         </div>

         <div class="marcfinestra2 banner2 banner2-last banner2-correccio">
            <a href="{$url_pam}" title="Planificació (PAM)">
               <div class="banner-image-ajuntaaunclic">
                  <img src="/images/ajunta/bnplanifica.jpg" width="204" height="144">      
               </div>
               <div id="textbclic" class="textbotoclic">
                  {$LABEL_PAM}
                  <br>
                  {$LABEL_PAMB}
               </div>
            </a>
         </div>

         <div class="marcfinestra2 banner2 banner2-correccio">
            <a href="{$url_seue}" target="_blank" title="Seu electrònica">
               <div class="banner-image-ajuntaaunclic">
                  <img src="/images/ajunta/bnseue.jpg" width="204" height="144">
               </div>
               <div id="textbclic" class="textbotoclic">
                  {$LABEL_BSEU}
                  <br>
                  {$LABEL_BSEUB}
               </div>
            </a>
         </div>

         <div class="marcfinestra2 banner2 banner2-correccio">
            <a href="{$url_perfilc}" target="_self" title="Perfil del contractant">
               <div class="banner-image-ajuntaaunclic">
                  <img src="/images/ajunta/bnperfilc.jpg" width="204" height="144">
               </div>
               <div id="textbclic" class="textbotoclic">
                  {$LABEL_BPERFIL}
                  <br>
                  {$LABEL_BPERFILB}
               </div>
            </a>
         </div>

         <div class="marcfinestra2 banner2 banner2-correccio">
            <a href="http://www2.tortosa.cat/indicadors-transparencia?lang={$lang}" title="Portal de Transparència">
               <div class="banner-image-ajuntaaunclic">
                  <img src="/images/ajunta/bntranspa.jpg" width="204" height="144">
               </div>
               <div id="textbclic" class="textbotoclic">
                  {$LABEL_BTRANSPA}
                  <br>
                  {$LABEL_BTRANSPAB}
               </div>
            </a>
         </div>

         <div class="marcfinestra2 banner2 banner2-last banner2-correccio">
            <a href="{$url_pciutadana}" target="_self" title="Participació ciutadana">
               <div class="banner-image-ajuntaaunclic">
                  <img src="/images/ajunta/bnparticipa.jpg" width="204" height="144">
               </div>
               <div id="textbclic" class="textbotoclic">
                  {$LABEL_BPARTICIPA}
                  <br>
                  {$LABEL_BPARTICIPAB}
               </div>
            </a>
         </div>

         </div>

      </div>
      <!--/ BANNERS -->

      <!-- INFORMACIÓ ADMINISTRATIVA -->
      <div class="row">

         <div class="titolsSeccionsHome titolBig marge-titol-contingut">
            {$LABEL_INFADM}
         </div>

         <div class="column-left-iformacio-administrativa">

         <div class="div-info-administrativa1">

            <a href="{$url_plens_sessions}">
               <div class="marc-info-administrativa">
                  <img src="/images/ajunta/bsplens.jpg">
                  <div class="textbotoinf text-boto-info-administrativa">
                    {$LABEL_SPLENS}
                    <br>
                    {$LABEL_SPLENSB}
                  </div>
               </div>
            </a>

            <a href="{$url_jgl}">
               <div class="marc-info-administrativa">
                  <img src="/images/ajunta/bjgl.jpg">
                  <div class="textbotoinf text-boto-info-administrativa">
                     {$LABEL_JUNTESG}
                     <br>
                     {$LABEL_JUNTESGB}
                  </div>
               </div>
            </a>

            <a href="{$url_subvencions}">
               <div class="marc-info-administrativa marc-info-administrativa-last">
                  <img src="/images/ajunta/bsubvencions.jpg">
                  <div class="textbotoinf text-boto-info-administrativa">
                     {$LABEL_SUBVENCIONS}
                     <br>
                     {$LABEL_SUBVENCIONSB}
                  </div>
               </div>
            </a>
            
            <a href="https://www.seu-e.cat/web/tortosa/govern-obert-i-transparencia/accio-de-govern-i-normativa/normativa-plans-i-programes/ordenances-fiscals" target="_blank">
               <div class="marc-info-administrativa">
                  <img src="/images/ajunta/bofiscals.jpg">
                  <div class="textbotoinf text-boto-info-administrativa">
                     {$LABEL_ORDENANCESF}
                     <br>
                     {$LABEL_ORDENANCESFB}
                  </div>
               </div>
            </a>
            
            <a href="https://www.seu-e.cat/web/tortosa/govern-obert-i-transparencia/accio-de-govern-i-normativa/normativa-plans-i-programes/ordenances-reguladores-i-reglaments" target="_blank">
               <div class="marc-info-administrativa">
                  <img src="/images/ajunta/bordreg.jpg">
                  <div class="textbotoinf text-boto-info-administrativa">
                     {$LABEL_ORDENANCESR}
                     <br>
                     {$LABEL_ORDENANCESRB}
                  </div>
               </div>
            </a>
                
            <a href="http://www2.tortosa.cat/borsa/index.php?lang={$lang}" target="_blank">
               <div class="marc-info-administrativa marc-info-administrativa-last">
                  <img src="/images/ajunta/bbtreball.jpg">
                  <div class="textbotoinf text-boto-info-administrativa">
                     {$LABEL_BORSAT}
                     <br>
                     {$LABEL_BORSATB}
                  </div>
               </div>
            </a>
                
            <a href="{$url_infpub}">
               <div class="marc-info-administrativa">
                  <img src="/images/ajunta/binfpub.jpg">
                  <div class="textbotoinf text-boto-info-administrativa">
                     {$LABEL_INFPUB}
                     <br>
                     {$LABEL_INFPUBB}
                  </div>
               </div>
            </a>
                
            <a href="/estadistica-habitants/index.php">
               <div class="marc-info-administrativa">
                  <img src="/images/ajunta/besthab.jpg">
                  <div class="textbotoinf text-boto-info-administrativa">
                     {$LABEL_ESTADISTICA}
                     <br>
                     {$LABEL_ESTADISTICAB}
                  </div>
               </div>
            </a>
                
            <a href="http://www.tortosa.cat/tramits" target="_blank">
               <div class="marc-info-administrativa marc-info-administrativa-last">
                  <img src="/images/ajunta/btramits.jpg">
                  <div class="textbotoinf text-boto-info-administrativa">
                     <br>
                     {$LABEL_TRAMITS}
                  </div>
               </div>
            </a>

            </div>

         </div>

         <div class="column-right-iformacio-administrativa">

         <div class="div-info-administrativa2">
            
            <a href="{$url_iviapublica}">
               <div class="marc-banners-informacio-administrativa marge-inferior-boto-alt">
                  <img src="/images/ajunta/tbplanti.jpg">
                  <div class="text-boto-alt">
                    {$LABEL_INCIDEN}
                    <br>
                    {$LABEL_INCIDENB}
                  </div>
               </div>
            </a>

                <!--<a href="/iviapublica/formivp.php">
                <div id="botoalt2" class="marcterme baltres">
                    <img src="/images/ajunta/tbplanti.jpg">
                    <div class="textbotoalt">
                    {$LABEL_QUEIXES}
                    <br>
                    {$LABEL_QUEIXESB}
                    </div>
                </div>
                </a>-->

            <a href="{$url_defensorc}">
               <div class="marc-banners-informacio-administrativa marge-inferior-boto-alt marge-inferior-boto-alt-last">
                  <img src="/images/ajunta/tbplanti.jpg">
                  <div class="text-boto-alt">
                     {$LABEL_DEFENSOR}
                     <br>
                     {$LABEL_DEFENSORB}
                  </div>
               </div>
            </a>

            <a href="{$url_atencio_ciutadana}">
               <div class="marc-banners-informacio-administrativa marge-inferior-boto-alt">
                  <div class="textalt">
                     {$LABEL_ATENCIOC}
                     <br>
                     {$LABEL_ATENCIOCB}
                  </div>
               </div>
            </a>

            <a href="{$url_correus}">
               <div class="marc-banners-informacio-administrativa marge-inferior-boto-alt">
                  <div class="textalt">
                     {$LABEL_DIRECTORI}
                     <br>
                     {$LABEL_DIRECTORIB}
                  </div>
               </div>
            </a>

            <a href="{$url_protocols}">
               <div class="marc-banners-informacio-administrativa marge-inferior-boto-alt">
                  <div class="textalt">
                     {$LABEL_PRONOR}
                     <br>
                     {$LABEL_PRONORB}
                  </div>
               </div>
            </a>

            <a href="https://www.seu-e.cat/web/tortosa/govern-obert-i-transparencia/serveis-i-tramits/serveis/calendari-dies-inhabils" target="_blank">
               <div class="marc-banners-informacio-administrativa marge-inferior-boto-alt">
                  <div class="textalt">
                     {$LABEL_CALFES}
                     <br>
                     {$LABEL_CALFESB}
                  </div>
               </div>
            </a>

            <a href="https://www.seu-e.cat/documents/30004/0/Calendari+fiscal+2018/085be01c-a8ff-4602-93e3-b87745dfabcb" target="_blank">
               <div class="marc-banners-informacio-administrativa marge-inferior-boto-alt">
                  <div class="textalt">
                     {$LABEL_CALCON}
                     <br>
                     {$LABEL_CALCONB}
                  </div>
               </div>
            </a>

            <a href="{$url_ebop}">
               <div class="marc-banners-informacio-administrativa marge-inferior-boto-alt">
                  <div class="textalt">
                     {$LABEL_EBOP}
                     <br>
                     {$LABEL_EBOPB}
                  </div>
               </div>
            </a>

            </div>

         </div>
         
      </div>
      <!--/ INFORMACIÓ ADMINISTRATIVA -->

      <!-- AJUNTAMENT PER MATERIES -->
      <div class="row">

         <div class="titolsSeccionsHome titolBig marge-titol-contingut">
            {$LABEL_MATERIES}
         </div>

         <div class="div-altresMateries">

         <ul class="altres-materies">
            <li><a href="{$url_acciosocial}" hreflang="ca">{$LABEL_MAT1}</a></li>
            <li><a href="{$url_diversitatc}" hreflang="ca">{$LABEL_MAT2}</a></li>
            <li><a href="{$url_ensenyament}" hreflang="ca">{$LABEL_MAT3}</a></li>
                    
            <li><a href="{$url_urbanisme}">{$LABEL_MAT4}</a></li>    
            <li><a href="{$url_arxiu}" hreflang="ca">{$LABEL_MAT5}</a></li>
            <li><a href="{$url_gestiot}" hreflang="ca">{$LABEL_MAT6}</a></li>
                    
            <li><a href="{$url_pic}" hreflang="ca">{$LABEL_MAT7}</a></li>
            <li><a href="{$url_biblio}" target="_blank">{$LABEL_MAT8}</a></li>    
            <li><a href="{$url_cfo}" hreflang="ca" target="_blank">{$LABEL_MAT9}</a></li>
                    
            <li><a href="{$url_viver}" hreflang="ca" target="_blank">{$LABEL_MAT10}</a></li>
            <li><a href="{$url_pdocumental}" hreflang="ca">{$LABEL_MAT11}</a></li>
            <li><a href="{$url_joventut}" target="_blank">{$LABEL_MAT12}</a></li>
                        
            <li><a href="{$url_emusica}" hreflang="ca" target="_blank">{$LABEL_MAT13}</a></li>
            <li><a href="{$url_eteatre}" hreflang="ca" target="_blank">{$LABEL_MAT14}</a></li>
            <li><a href="{$url_tsport}" hreflang="ca" target="_blank">{$LABEL_MAT15}</a></li>

            <li><a href="{$url_aigues}" hreflang="ca" target="_blank">{$LABEL_MAT16}</a></li>
            <li><a href="{$url_memorial}" hreflang="ca">{$LABEL_MAT17}</a></li>
            <li><a href="{$url_bus}">{$LABEL_MAT18}</a></li>
                        
            <li><a href="{$url_deixalleria}" hreflang="ca">{$LABEL_MAT19}</a></li>
            <li><a href="{$url_museu}" hreflang="ca" target="_blank">{$LABEL_MAT20}</a></li>
            <li><a href="{$url_lgtbi}" hreflang="ca">{$LABEL_MAT21}</a></li>
         </ul>

         </div>

      </div>
      <!--/ AJUNTAMENT PER MATERIES -->

   </div>
</div>