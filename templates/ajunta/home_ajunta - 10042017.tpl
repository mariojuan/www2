<div id="page-wrap" class="ajunta">
    <div id="contajttotalcos">
        <div id="contciutatcos">
            <div id="bciutatpresen">
                <ul id="bciutatmenu">
                    <li >
                        <a href="http://www.tortosa.cat/webajt/ajunta/om/index.asp" target="_self">
                        <div id="ciutatbotoimatge">
                            <img src="/images/botons/bom.jpg" />
                        </div>
                        <div id="textbmenu1" class="textbotomciutat">
                            {$LABEL_BOM}
                            <br>
                            {$LABEL_BOMB}
                        </div>
                        </a>
                    </li>
                    <li >
                        <a href="/infeco/index.php" target="_self">
                            <div id="ciutatbotoimatge">
                                <img src="/images/botons/bpressup.jpg" />
                            </div>
                            <div id="textbmenu2" class="textbotomciutat">
                                {$LABEL_BPRESUP}
                                <br>
                                {$LABEL_BPRESUP2}
                            </div>
                        </a>
                    </li>
                    <li >
                        <a href="https://www.seu-e.cat/web/tortosa/govern-obert-i-transparencia" target="_blank">
                            <div id="ciutatbotoimatge">
                                <img src="/images/botons/btranspa.jpg" />
                            </div>
                            <div id="textbmenu3" class="textbotomciutat">
                                {$LABEL_BTRANSPA}
                            </div>
                        </a>
                    </li>
                    <li >
                        <a href="https://www.seu.cat/tortosa" target="_blank">
                            <div id="ciutatbotoimatge">
                                <img src="/images/botons/bseue.jpg" />
                            </div>
                            <div id="textbmenu4" class="textbotomciutat">
                                {$LABEL_BSEU}
                                <br>
                                {$LABEL_BSEUB}
                            </div>
                        </a>
                    </li>
                </ul>
                <div id="ciutatimatge">
                    <img src="/images/ajunta/ajuntament.jpg" />
                </div>
                <div id="ajuntaimatgeText" class="imatgeCapText">
                    <h1>
                    {$LABEL_TITOL}
                    &nbsp;&nbsp;
                    </h1>
                </div>
            </div>
            <div id="titolAdministracio" class="titolarea2">
                &nbsp;
                {$LABEL_TITOLS1B}
            </div>
            <div id="titolArees" class="titolarea2">
                &nbsp;
                {$LABEL_TITOLS1}
            </div>
            <div id="ServeisAjt" class="marcServeisCiutat">
                <ul class="ulajt">
                    <li>
                        <a href="http://www.tortosa.cat/webajt/correus.asp" target="_blank">
                            {$LABEL_ADM1}
                        </a>
                    </li>
                    <li>
                        <a href="/defensorc/index.php">
                            {$LABEL_ADM2}
                        </a>
                    </li>
                    <li>
                        <a href="http://www.tortosa.cat/webajt/ajunta/ebop.asp" target="_blank">
                            {$LABEL_ADM3}
                        </a>
                    </li>
                    <li>
                        <a href="/agermana/index.php">
                            {$LABEL_ADM15}
                        </a>
                    </li>
                    <li>
                        <a href="http://www.tortosa.cat/webajt/ajunta/alcaldia/eleccions.asp" target="_blank">
                            {$LABEL_ADM5}
                        </a>
                    </li>
                    <li>
                        <a href="http://www.tortosa.altanet.org/festes_locals.pdf" target="_blank">
                            {$LABEL_ADM6}
                        </a>
                    </li>
                    <li>
                        <a href="/iviapublica/index.php">
                            {$LABEL_ADM7}
                        </a>
                    </li>
                    <li>
                        <a href="http://www.tortosa.cat/webajt/ajunta/organs_govern/juntes_govern.asp" target="_blank">
                            {$LABEL_ADM8}
                        </a>
                    </li>
                    <li>
                        <a href="/iviapublica/formivp.php">
                            {$LABEL_ADM16}
                        </a>
                    </li>
                    <li>
                        <a href="/perfilc/index.php">
                            {$LABEL_ADM10}
                        </a>
                    </li>
                    <li>
                        <a href="http://www.tortosa.cat/webajt/ajunta/organs_govern/plens.asp" target="_blank">
                            {$LABEL_ADM11}
                        </a>
                    </li>
                    <li>
                        <a href="http://www.tortosa.cat/webajt/ajunta/subvencions/index.asp" target="_blank">
                            {$LABEL_ADM14}
                        </a>
                    </li>
                </ul>
            </div>
            <a href="/acciosocial/index.php">
                <div id="AreesAjunta" class="marcArees">
                    <img src="/images/botons/bA1.jpg" border="0" />
                </div>
                <div id="textaboto" class="textbotoa">
                    {$LABEL_BOTOA1}
                </div>
                <div id="texta2boto" class="textbotoa">
                    {$LABEL_BOTOA1B}
                </div>
            </a>
            <a href="/divcul/index.php">
                <div id="AreesAjuntab" class="marcArees">
                    <img src="/images/botons/bA2.jpg" />
                </div>
                <div id="textabotob" class="textbotoa">
                    {$LABEL_BOTOA2}
                </div>
                <div id="texta2botob" class="textbotoa">
                    {$LABEL_BOTOA2B}
                </div>
            </a>
            <a href="/ensenyament/index.php">
                <div id="AreesAjunta2" class="marcArees">
                    <img src="/images/botons/bA3.jpg" />
                </div>
                <div id="textaboto2" class="textbotoa">
                    {$LABEL_BOTOA3}
                </div>
            </a>
            <a href="http://www.tortosa.cat/webajt/ajunta/arxiu/index.asp" target="_blank">
                <div id="AreesAjunta2b" class="marcArees">
                    <img src="/images/botons/bA4.jpg" />
                </div>
                <div id="textaboto2b" class="textbotoa">
                    {$LABEL_BOTOA4}
                </div>
                <div id="texta2boto2b" class="textbotoa">
                    {$LABEL_BOTOA4B}
                </div>
            </a>
            <a href="/urbanisme/index.php">
                <div id="AreesAjunta3" class="marcArees">
                    <img src="/images/botons/bA5.jpg" />
                </div>
                <div id="textaboto3" class="textbotoa">
                    {$LABEL_BOTOA5}
                </div>
            </a>
            <a href="http://www.tortosa.cat/webajt/cadastre/index.asp" target="_blank">
                <div id="AreesAjunta3b" class="marcArees">
                    <img src="/images/botons/bA6.jpg" />
                </div>
                <div id="textaboto3b" class="textbotoa">
                    {$LABEL_BOTOA6}
                </div>
                <div id="texta2boto3b" class="textbotoa">
                    {$LABEL_BOTOA6B}
                </div>
            </a>
        </div>
        <br><br>
    </div>
</div>
