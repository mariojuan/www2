<!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->
<div id="page-wrap">

   <div class="contenedor-responsive">

      <div id="conttranstotalcos">

         <div id="conttranscos">

            <div id="btranspresen">

               <div id="transimatge">			
                  <img src="{$image}" class="img-slider-territori"/>			
               </div>

               <div id="transimatgeText">
                  <h1>
                     {$LABEL_TITOL1}

                     <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>

                  </h1>
               </div>

            </div>

            <ul id='menu_planA'>
               {foreach $MENU as $itemMenu}
               <a href={$itemMenu}>
                  <li id='menu_planA_item'>
                     {$itemMenu@key}
                  </li>
               </a>
               {/foreach}
            </ul>

            <div id="tcentre_planA">
               <font size="4" color="#666666">
               {$LABEL_TC0}
               </font>
               <br><br>
               {if $ntpl==1 or $ntpl==3}
               {$LABEL_TC1}
               <br><br>
               <center>
                  <iframe class="mapa-responsive" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12086.383970105255!2d0.5395746!3d40.7709098!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x2396f9a5895a3131!2sDeixalleria+Tortosa!5e0!3m2!1ses!2ses!4v1484645711568" width="550" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
               </center>
               <br><br>
               {else if $ntpl==2}
               {$LABEL_TC1}
               {else if $ntpl==4}
               {$LABEL_TC1}
               <br><br>
               {$LABEL_TC2}
               {/if}
            </div>

         </div>
      </div>
   </div>
</div>