<div id="footer">
	<div id="contenidortotalpeu">
		<div id="contenidorpeu">
			<div id="privacitat">
				<a href="/docs/{$lang}_pprivacitat.pdf" hreflang="{$ca}" target="_target">
				{$LABEL_POLPRIV}
				</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="/docs/{$lang}_alegal.pdf" hreflang="{$lang}" target="_blank">
				{$LABEL_AVISL}
				</a>
			</div>
			<div id="credencials">
				<p>&copy; 
				{$LABEL_COP}
				</p>
				<p>Pl. d'Espanya, 1 | 43500 Tortosa | Telf. 977 58 58 00 |
                    <a href="https://www.facebook.com/ajuntamentdetortosa/" target="_blank" title="facebook">
                        <i class="icon-facebook-official"></i>
                    </a>
                    <a href="https://twitter.com/Tortosa?ref_src=twsrc%5Etfw" target="_blank" title="twitter">
                        <i class="icon-twitter"></i>
                    </a>
                </p>
			</div>
            <div class="optimitzacio-navegadors">
                {$LABEL_WEB_OPTIMITZADA}
            </div>
		</div>
	</div>
</div>