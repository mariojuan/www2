<html>
	<head>

        {include file="head.tpl"}

	</head>
	<body>

		{include file="header.tpl"}

		<div id="page-wrap">

            <div class="contenedor-responsive">

    <div id="conttranstotalcos">
        <div id="conttranscos">
            <div id="btranspresen">
                <div id="transimatge">
                    <img src="{$image}" class="img-slider-territori"/>
                </div>
                <div id="transimatgeText">
                    <h1>
                        {$LABEL_TITOL1}
                        <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                    </h1>
                </div>
            </div>

            <!-- Menu -->
                {include file="junta-govern-local/menu.tpl"}
            <!--// Menu -->

            <div id="tcentre_planA" class="rectificat-junta-gov-local" style="margin-left: -36px; margin-top: -6px;">

                <font size="4" color="#666666">{$LABEL_TC0}</font>
                <p>{$LABEL_TC1}</p>
                <p>{$LABEL_TC2}</p>

                <table>
				<thead>
                  <tr>
                    <th>{$LABEL_FOTO}</th>
                    <th>{$LABEL_NOM}</th>
                    <th>{$LABEL_GRUP}</th>
					<th>{$LABEL_CARREC}</th>
                    </tr>
					</thead>
					<tbody>
                    	{foreach $JUNTAGOVERNLOCAL as $item}
                    	<tr>
                        	<td><a href="{$item['link']}" target="{$item['target']}"><img src="{$item['foto']}" class="avatar"/></a></td>
                        	<td>{$item['name']}</td>
                        	<td>{$item['group']}</td>
							<td>{$item['occupation']}</td>
                   		</tr>
                    	{/foreach}
						</tbody>
                </table>

                <!-- Membres -->
                <!--
                <ul>
                {foreach $JUNTAGOVERNLOCAL as $item}
                    <li>{$item['name']} - {$item['group']} - {$item['occupation']} - <a href="{$item['link']}" target="{$item['target']}"><i class="{$item['icon']}"></i></a></li> 
                {/foreach}
                </ul>
                -->

                <!-- Taula Decrets -->

                <div class="separator"></div>
                <div class="separator"></div>

                <h3>{$LABEL_TC7}</h3>
                <table>
                <thead>
                  <tr>
                    <th>{$LABEL_DATA}</th>
                    <th>{$LABEL_ACORD}</th>
                    <th>{$LABEL_DESC}</th>
                    </tr>
                    </thead>
                    {foreach $DECRETS as $item}
                    <tr>
                        <td>{$item['date']}</td>
                        <td><a href="{$item['link']}" target="{$item['target']}">{$item['agreement']}</a></td>
                        <td>{$item['desc']}</td>
                    </tr>
                    {/foreach}
                </table>

                <!-- //Taula Decrets -->



                

            </div>
        </div>
    </div>
    </div>
</div>

		{include file="footer.tpl"}

	</body>
</html>