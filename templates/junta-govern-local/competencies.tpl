<html>
   <head>
      {include file="head.tpl"}
   </head>
   <body>
      {include file="header.tpl"}
      <div id="page-wrap">
         <div class="contenedor-responsive">
            <div id="conttranstotalcos">
               <div id="conttranscos">
                  <div id="btranspresen">
                     <div id="transimatge">
                        <img src="{$image}" class="img-slider-territori"/>
                     </div>
                     <div id="transimatgeText">
                        <h1>
                           {$LABEL_TITOL1}
                           <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                        </h1>
                     </div>
                  </div>
                  <!-- Menu -->
                  {include file="junta-govern-local/menu.tpl"}
                  <!--// Menu -->
                  <div id="tcentre_planA" class="rectificat-junta-gov-local" style="margin-left: -36px; margin-top: -6px;">
                     <font size="4" color="#666666">{$LABEL_TC0}</font>
                     <p>{$LABEL_TC1}</p>
                     <p>{$LABEL_TC2}</p>
                     <ul>
                        <li>{$LABEL_DOC1} - <a href="https://www.diputaciodetarragona.cat/ebop/index.php?op=dwn&tipus=i&data=20150723&anyp=2015&num=07400&v=i" target="_blank">{$LABEL_DOC2}</a></li>
                     </ul>
                     <ul>
                        <li><a href="http://www.tortosa.cat/webajt/ajunta/om/2015/decret1453.pdf" target="_blank">{$LABEL_DOC3}</a> - <a href="https://www.diputaciodetarragona.cat/ebop/index.php?op=dwn&tipus=i&data=20150723&anyp=2015&num=07401&v=i" target="_blank"> {$LABEL_DOC4}</a></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      {include file="footer.tpl"}
   </body>
</html>