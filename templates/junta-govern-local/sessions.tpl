<html>
	<head>

        {include file="head.tpl"}

	</head>
	<body>

		{include file="header.tpl"}

		<div id="page-wrap">

            <div class="contenedor-responsive">

    <div id="conttranstotalcos">
        <div id="conttranscos">
            <div id="btranspresen">
                <div id="transimatge">
                    <img src="{$image}" class="img-slider-territori"/>
                </div>
                <div id="transimatgeText">
                    <h1>
                        {$LABEL_TITOL1}
                    </h1>
                </div>
            </div>

            <!-- Menu -->
                {include file="junta-govern-local/menu.tpl"}
            <!--// Menu -->

            <div id="tcentre_planA" style="margin-left: -36px; margin-top: -6px;">

                <font size="4" color="#666666">{$LABEL_TC0}</font>
                <p>{$LABEL_TC1}</p>
                <p>{$LABEL_TC2}</p>

                <!-- Decrets -->
                {foreach $DECRETS as $item}
                    <p><a href="{$item['link']}" target="{$item['target']}">{$item['name']}</a></p>
                {/foreach}

            </div>
        </div>
    </div>
    </div>
</div>

		{include file="footer.tpl"}

	</body>
</html>