<html>
   <head>
      {include file="head.tpl"}
   </head>
   <body>
      {include file="header.tpl"}
      <div id="page-wrap">

      <div class="contenedor-responsive">

         <div id="conttranstotalcos">
            <div id="conttranscos">
               <div id="btranspresen">
                  <div id="transimatge">
                     <img src="{$image}" class="img-slider-territori"/>
                  </div>
                  <div id="transimatgeText">
                     <h1>
                        {$LABEL_TITOL1}
                     </h1>
                  </div>
               </div>
               <div id="tcentre_municipi">
                  <font size="4" color="#666666">{$LABEL_TC0}</font>
                  <p>{$LABEL_TC1}</p>
                  <ul>
                     <li><a href="http://www2.tortosa.cat/organs/index.php?lang={$lang}" target="">{$LABEL_TC2}</a></li>
                     <li><a href="http://www2.tortosa.cat/organitzacio-govern-municipal/index.php?lang={$lang}" target="">{$LABEL_TC3}</a></li>
                     <li>{$LABEL_TC4}</li>
                     <li>{$LABEL_TC5}</li>
                  </ul>
                  <!-- <p>{$LABEL_TC6} <a href="" target="_blank">{$LABEL_TC7}</a></p> -->
                  <img src="http://www.tortosa.cat/webajt/ajunta/om/Estructura8.jpg" usemap="#Map" class="img-responsive"/>

                  <p>Items informatius</p>
                     <ul>
                        <li>
                           <a href="{$LABEL_CUADRE_PLE}" target="_self" alt="El Ple">El Ple</a>
                           <ul>
                              <li>
                                 <a href="{$LABEL_CUADRE_INFORMATIVES}" target="_blank" alt="Comissions Informatives">Comissions Informatives</a>
                              </li>
                              <li>
                                 <a href="{$LABEL_CUADRE_ESPECIAL_COMPTES}" target="_blank" alt="Comissió Especial de Comptes">Comissió Especial de Comptes</a>
                              </li>
                              <li>
                                 <a href="{$LABEL_CUADRE_PORTAVEUS}" target="_blank" alt="Junta de portaveus">Junta de portaveus</a>
                              </li>
                              <li>
                                 <a  href="{$LABEL_CUADRE_COORDINACIO_POBLES}" target="_blank" alt="Junta Coordinació de govern als pobles">Junta Coordinació de govern als pobles </a>
                              </li>
                              <li>
                                 <a href="{$LABEL_CUADRE_COORDINACIO_BARRIS}" target="_blank" alt="Junta Coordinació de govern als barris">Junta Coordinació de govern als barris</a>
                              </li>
                              <li>
                                 <a href="{$LABEL_CUADRE_ORGANS_PARTICIPACIO}" target="_blank" alt="Òrgans de participació">Òrgans de participació</a>
                              </li>
                           </ul>
                        </li>
                        <li>
                           <a href="{$LABEL_CUADRE_ALCALDIA}" target="_self" alt="Alcaldia-Presidència">Alcaldia-Presidència</a>
                           <ul>
                              <li>
                                 <a href="{$LABEL_CUADRE_TINENCIES}" target="_blank" alt="Tinències d'Alcaldia">Tinències d'Alcaldia</a>
                              </li>
                           </ul>
                        </li>
                        <li>
                           <a href="{$LABEL_CUADRE_JUNTA_GOV_LOCAL}" target="_self" alt="Junta Govern Local">Junta Govern Local</a>
                        </li>
                        <li>
                           <a href="{$LABEL_CUADRE_ORGANIT_GOVERN_MUNICIPAL}" target="_self" alt="Organització del govern municipal">Organització del govern municipal</a>
                        </li>
                        <li>
                           <a href="{$LABEL_CUADRE_ESTRUCT_ORGANITZ_ADMIN}" target="_self" alt="Estructura organitzativa de l'Administració">Estructura organitzativa de l'Administració</a>
                        </li>
                        <li>
                           <a href="{$LABEL_CUADRE_EMPLEATS_PUBLICS}" target="_self" alt="Empleats Públics">Empleats Públics</a>
                        </li>
                     </ul>
               </div>
               <div class="separator"></div>
            </div>
         </div>

         </div>

      </div>
      {include file="footer.tpl"}
   </body>
</html>