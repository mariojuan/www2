<html>
   <head>
      {include file="head.tpl"}
   </head>
   <body>
      {include file="header.tpl"}
      <div id="page-wrap">

      <div class="contenedor-responsive">

         <div id="conttranstotalcos">
            <div id="conttranscos">
               <div id="btranspresen">
                  <div id="transimatge">
                     <img src="{$image}" class="img-slider-territori"/>
                  </div>
                  <div id="transimatgeText">
                     <h1>
                        {$LABEL_TITOL1}
                     </h1>
                  </div>
               </div>
               <div id="tcentre_municipi">
                  <font size="4" color="#666666">{$LABEL_TC0}</font>
                  <p>{$LABEL_TC1}</p>
                  <ul>
                     <li><a href="http://www2.tortosa.cat/organs/index.php?lang={$lang}" target="">{$LABEL_TC2}</a></li>
                     <li><a href="http://www2.tortosa.cat/organitzacio-govern-municipal/index.php?lang={$lang}" target="">{$LABEL_TC3}</a></li>
                     <li>{$LABEL_TC4}</li>
                     <li>{$LABEL_TC5}</li>
                  </ul>
                  <!-- <p>{$LABEL_TC6} <a href="" target="_blank">{$LABEL_TC7}</a></p> -->
                  <img src="http://www.tortosa.cat/webajt/ajunta/om/Estructura8.jpg" usemap="#Map" class="img-responsive"/>
                  <map name="Map">
                     <area shape="rect" coords="54,169,245,218"  href="{$LABEL_CUADRE_PLE}" target="_self" alt="El Ple">
                     <area shape="rect" coords="364,168,550,222" href="{$LABEL_CUADRE_ALCALDIA}" target="_self">
                     <area shape="rect" coords="264,254,452,306" href="{$LABEL_CUADRE_JUNTA_GOV_LOCAL}" target="_self" alt="Junta Govern Local">
                     <area shape="rect" coords="13,267,161,337"  href="{$LABEL_CUADRE_INFORMATIVES}" target="_self" alt="Comissions Informatives">
                     <area shape="rect" coords="13,360,137,413"  href="{$LABEL_CUADRE_ESPECIAL_COMPTES}" target="_self" alt="Comissi&oacute; Especial de Comptes">
                     <area shape="rect" coords="14,431,138,483"  href="{$LABEL_CUADRE_PORTAVEUS}" target="_self" alt="Junta de Portaveus">
                     <area shape="rect" coords="578,261,686,327" href="{$LABEL_CUADRE_TINENCIES}" target="_self" alt="Tinences d'Alcaldia">
                     <area shape="rect" coords="13,574,725,630"  href="{$LABEL_CUADRE_ORGANIT_GOVERN_MUNICIPAL}" target="_self" alt="Organitzaci&oacute; del govern municipal">
                     <area shape="rect" coords="12,646,725,703"  href="{$LABEL_CUADRE_ESTRUCT_ORGANITZ_ADMIN}" target="_blank" alt="Estructura t&egrave;cnica administrativa">
                     <area shape="rect" coords="227,337,351,388" href="{$LABEL_CUADRE_COORDINACIO_POBLES}" target="_self" alt="Junta Coordinaci&oacute; de govern als pobles">
                     <area shape="rect" coords="229,393,353,444" href="{$LABEL_CUADRE_COORDINACIO_BARRIS}" target="_self" alt="Junta Coordinaci&oacute; als barris">
                     <area shape="rect" coords="14,720,725,774"  href="{$LABEL_CUADRE_EMPLEATS_PUBLICS}" target="_self" alt="Empleats P&uacute;blics">
                     <area shape="rect" coords="229,459,377,534" href="{$LABEL_CUADRE_ORGANS_PARTICIPACIO}" target="_self" alt="Òrgans de participació">
                  </map>
               </div>
               <div class="separator"></div>
            </div>
         </div>

         </div>

      </div>
      {include file="footer.tpl"}
   </body>
</html>