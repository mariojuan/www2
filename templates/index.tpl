<html>
	<head>
        <!-- it works the same with all jquery version from 1.x to 2.x -->
        <script type="text/javascript" src="/js/jquery-1.9.1.min.js"></script>
        <!-- use jssor.slider.mini.js (40KB) instead for release -->
        <!-- jssor.slider.mini.js = (jssor.js + jssor.slider.js) -->
        <script type="text/javascript" src="/js/jssor.js"></script>
        <script type="text/javascript" src="/js/jssor.slider.js"></script>
        <script type="text/javascript" src="/js/slider.js"></script>
        {include file="head.tpl"}
	</head>
	<body class="home">
        {if $test==1}
            <div class="container">
                {include file="header_v2.tpl"}
                {include file="home_v2.tpl"}
                {include file="footer_v2.tpl"}
            </div>
        {else}
            {include file="header.tpl"}
            {include file="home.tpl"}
            {include file="footer.tpl"}
        {/if}
	</body>
</html>