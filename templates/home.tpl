<div id="page-wrap">
    <div id="contenidortotalcos">

        <!-- FOTOS SLIDER -->
        <div class="rowCartellera">
            <!-- Jssor Slider Begin -->
            <!-- You can move inline styles to css file or css block. -->
            <div id="slider1_container" style="position: relative; top: 2px; left: 2px; width: 896px; height: 308px; overflow: hidden;">
                <!-- Loading Screen -->
                <div u="loading" style="position: absolute; top: 0px; left: 0px;">
                    <div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block;
                              background-color: #000; top: 0px; left: 0px;width: 100%;height:100%;"></div>
                    <div style="position: absolute; display: block; background: url(/images/img/loading.gif) no-repeat center center;
                              top: 0px; left: 0px;width: 100%;height:100%;"></div>
                </div>
                <!-- Slides Container -->
                <div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 896px; height: 308px; overflow: hidden;">
                    <!-- Farà de cartellera, quan no hi ha res posar les fotos 2, 3, 7,...,15 -->
                    {if $bannerslst_home|@count > 0}
                        {foreach $bannerslst_home as $banner}
                            {if $banner->TIPUS == 1}
                                <div>
                                    {if $banner->LINK!=""}
                                        <a href="{$banner->LINK}">
                                    {elseif $banner->FITXER_LINK!=""}
                                        <a href="public_files/banners/{$banner->FITXER_LINK}" target="_blank">
                                    {else}
                                        <a href="#">
                                    {/if}
                                        <img u="image" src="/public_files/banners/{$banner->IMATGE}" class="img-slider" alt="{$banner->TITOL}" title="{$banner->TITOL}"/>
                                    </a>
                                </div>
                            {/if}
                        {/foreach}
                    {else}
                        <div>
                            <a href="#">
                                <img u="image" src="/images/fotosportada/img18.jpg" class="img-slider"/>
                            </a>
                        </div>
                        <div>
                            <a href="#">
                                <img u="image" src="/images/fotosportada/img17.jpg" class="img-slider"/>
                            </a>
                        </div>
                        <div>
                            <a href="#">
                                <img u="image" src="/images/fotosportada/img16.jpg" class="img-slider"/>
                            </a>
                        </div>
                        <div>
                            <a href="#">
                                <img u="image" src="/images/fotosportada/img2.jpg" class="img-slider"/>
                            </a>
                        </div>
                        <div>
                            <a href="#">
                                <img u="image" src="/images/fotosportada/img3.jpg" class="img-slider"/>
                            </a>
                        </div>
                        <div>
                            <a href="#">
                                <img u="image" src="/images/fotosportada/img7.jpg" class="img-slider"/>
                            </a>
                        </div>
                        <div>
                            <a href="#">
                                <img u="image" src="/images/fotosportada/img8.jpg" class="img-slider"/>
                            </a>
                        </div>
                        <div>
                            <a href="#">
                                <img u="image" src="/images/fotosportada/img10.jpg" class="img-slider"/>
                            </a>
                        </div>
                        <div>
                            <a href="#">
                                <img u="image" src="/images/fotosportada/img11.jpg" class="img-slider"/>
                            </a>
                        </div>
                        <div>
                            <a href="#">
                                <img u="image" src="/images/fotosportada/img12.jpg" class="img-slider"/>
                            </a>
                        </div>
                        <div>
                            <a href="#">
                                <img u="image" src="/images/fotosportada/img13.jpg" class="img-slider"/>
                            </a>
                        </div>
                        <div>
                            <a href="#">
                                <img u="image" src="/images/fotosportada/img15.jpg" class="img-slider"/>
                            </a>
                        </div>
                    {/if}
                    <!--<div>
                        <a href="http://www.museudetortosa.cat/activitats/agenda/a-cel-obert-2017-festival-dintervencions-efimeres">
                            <img u="image" src="/images/fotosportada/imgcobert17.jpg" class="img-slider"/>
                        </a>
                    </div>
                    <div> 
                        <a href="http://www.museudetortosa.cat/activitats/agenda/a-cel-obert-2017-festival-dintervencions-efimeres">
                            <img u="image" src="/images/fotosportada/JEP2017.jpg" class="img-slider"/>
                        </a>
                    </div>-->
                </div>
                <!-- bullet navigator container -->
                <div u="navigator" class="jssorb03" style="position: absolute; bottom: 4px; right: 6px;">
                    <!-- bullet navigator item prototype -->
                    <div u="prototype" style="position: absolute; width: 21px; height: 21px; text-align:center; line-height:21px; color:white; font-size:12px;">
                        <div u="numbertemplate"></div>
                    </div>
                </div>
                <!-- Bullet Navigator Skin End -->
                <span u="arrowleft" class="jssora03l" style="width: 55px; height: 55px; top: 123px; left: 8px;"></span>
                <!-- Arrow Right -->
                <span u="arrowright" class="jssora03r" style="width: 55px; height: 55px; top: 123px; right: 8px"></span>
                <!-- Arrow Navigator Skin End -->
                <!-- <a style="display: none" href="http://www.jssor.com">bootstrap carousel</a> -->
            </div>
            <!-- Jssor Slider End -->
        </div>
        <!--/ FOTOS SLIDER -->

        <!-- BANNERS -->
        <div class="row">

        <div class="div-banners-home">

            <a href="http://www.tortosaturisme.cat/" target="_blank" title="Turisme Tortosa">
                <div class="marcfinestra singleBanner singleBanner-float-none">
                    <div id="bannerima">
                        <img src="/images/banners/bnturismei.jpg">
                        <!--<img src="/images/banners/bnpij.jpg">-->
                    </div>
                    <div id="bannerText5">
                        {$LABEL_BOTO1}

                        <br> {$LABEL_BOTO1B}

                    </div>
                </div>
            </a>

            <a href="http://www.festadelrenaixement.org" target="_blank" title="Festa del Renaixement">
                <div class="marcfinestra singleBanner singleBanner-float-none">
                    <div id="bannerima">
                        <img src="/images/banners/bnfrenaixi.jpg">
                    </div>
                    <div id="bannerText5">
                        {$LABEL_BOTO2}

                        <br> {$LABEL_BOTO2B}

                    </div>
                </div>
            </a>

            <a href="http://www.museudetortosa.cat" target="_blank" title="Museu de Tortosa">
                <div class="marcfinestra singleBanner singleBanner-float-none">
                    <div id="bannerima">
                        <img src="/images/banners/bnmuseui.jpg">
                    </div>
                    <div id="bannerText5">
                        {$LABEL_BOTO4}

                        <br> {$LABEL_BOTO4B}

                    </div>
                </div>
            </a>

            <a href="http://www.teatreauditoritortosa.cat" target="_blank" title="Teatre Auditori Felip Pedrell">
                <div class="marcfinestra singleBanner singleBanner-float-none">
                    <div id="bannerima">
                        <img src="/images/banners/bnteatrei.jpg">
                    </div>
                    <div id="bannerText5">
                        {$LABEL_BOTO5}

                        <br> {$LABEL_BOTO5B}

                    </div>
                </div>
            </a>

            <a href="{$url_negocis}" title="Negocis - Fira Tortosa">
                <div class="marcfinestra singleBannerLast singleBanner-float-none">
                    <div id="bannerima">
                        <img src="/images/banners/bnfirati.jpg">
                    </div>
                    <div id="bannerText5">
                        {$LABEL_BOTO3}
                        <br> {$LABEL_BOTO3B}
                    </div>
                </div>
            </a>

        </div>

        </div>
        <!--/ BANNERS -->

        <!--BLOC NOTICIES -->
        <div class="row">

            <!-- COL BLOCx4 NOTICIES -->
            <div class="column-left">

                <div class="titolsSeccionsHome titolBig">
                    <a href="http://www2.tortosa.cat/noticies/index.php">{$LABEL_TITOLS2}</a>
                </div>

                <div class="div-noticies">
                    {$LABEL_HDL}
                </div>

            </div>

            <!-- COL TWEETS -->
            <div class="column-center">

                <div class="titolsSeccionsHome TitolTweets">
                    Tweets
                </div>

                <div class="div-tweets">
                    <a style="margin-top: 20px;" class="twitter-timeline" data-width="220" data-height="380" href="https://twitter.com/Tortosa">Tweets by PLTortosa</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                </div>

            </div>

            <!-- COL CAMPANYES -->
            <div class="column-right">

            <div class="classter-left">

                <div class="titolsSeccionsHome titolCampanyes">
                    {$LABEL_TITOLS2C}
                </div>

                <div class="div-campanyes">
                <!-- Campanyes -->
                <ul class="bxslider">
                    {foreach $bannerslst_camp as $banner}
                        {if $banner->TIPUS == 2}
                            <li>
                                {if $banner->LINK!=""}
                                    <a href="{$banner->LINK}" target="_blank">
                                {elseif $banner->FITXER_LINK!=""}
                                    <a href="{$smarty.server.REQUEST_URI}public_files/banners/{$banner->FITXER_LINK}" target="_blank">
                                {else}
                                    <a href="#">
                                {/if}
                                    <img src="/public_files/banners/{$banner->IMATGE}" alt="{$banner->TITOL}" title="{$banner->TITOL}"/>
                                </a>
                            </li>
                        {/if}
                    {/foreach}
                </ul>
                </div>

                </div>

                <div class="classter-right">

                <div class="titolsSeccionsHome titolCampanyes titolCampanyes-alineat">
                    {$LABEL_TITOLS2D}
                </div>

                    <div class="div-social-noSocial">

                    <div class="rowInsideCol">

                        <div class="social twitter">
                            <a href="https://twitter.com/Tortosa" target="_blank">
                                <div id="texttw">
                                    <i class='icon-twitter'></i>
                                </div>
                            </a>
                        </div>

                        <div class="social youtube">
                            <a href="https://www.youtube.com/watch?v=eoYR1W2ugL8" target="_blank">
                                <div id="texttw">
                                    <i class='icon-youtube'></i>
                                </div>
                            </a>
                        </div> 

                        <div class="social facebook">
                            <a href="https://www.facebook.com/ajuntamentdetortosa" target="_blank">
                                <div id="texttw">
                                    <i class='icon-facebook'></i>
                                </div>
                            </a>
                        </div> 

                    </div>

                    

                <div class="marcfinestra follow-no-social">
                    <a href="http://agenda.tortosa.cat/agenda-tortosa/" target="_blank">
                        <div id="bannerima">
                            <img src="/images/banners/bnagenda.jpg" width="137">
                        </div>
                        <div id="textagenda">
                            {$LABEL_BAGENDA}        
                        </div>
                    </a>
                </div>

                <div class="marcfinestra follow-no-social">
                    <a href="http://www.tortosa.altanet.org/imact/agenda319.pdf" target="_blank">
                        <div id="bannerima">
                            <img src="/images/banners/bnagendac.jpg" width="137">
                        </div>
                        <div id="textagendac">
                            {$LABEL_BAGENDAC}       
                        </div>
                        <div id="textagendacb">
                            {$LABEL_BAGENDACB}      
                        </div>
                    </a>
                </div>

                <div class="marcfinestra follow-no-social follow-no-social-last">
                    <a href="http://www.radiotortosa.cat/" target="_blank">
                        <div id="bannerima">
                            <img src="/images/banners/bnradio.jpg" width="137">
                        </div>
                        <div id="textagendac">
                            {$LABEL_BRADIO}        
                        </div>
                        <div id="textagendacb">
                            {$LABEL_BRADIOB}      
                        </div>
                    </a>
                </div>

                </div>

                </div>

            </div>

        </div>
        <!--/ BLOC NOTICIES -->


        <!-- L'ALCALDE -->
        <div class="row">
            
            <div class="titolsSeccionsHome titolBig">
                Meritxell Roigé i Pedrola. <font color="#000">{$LABEL_TITOL2_2}</font>
            </div>

            <div class="div-alcalde">

            <div class="marcterme alcaldeLandScape">
                <a href="{$url_alcalde}" target="_self" title="{$LABEL_TITOL2_2}">
                    <img src="/images/alcalde/balcalde2.jpg" height="98" class="alcalde-responsive">
                </a>
            </div>
            
            <div class="marcterme banners-alcalde banner-alcalde-first">
                <a href="{$url_pam}" target="_self" title="PAM 2011-2015">
                    <img src="/images/banners/bnpam.jpg">
                    <div class="banners3text" id="banners3textb">
                        {$LABEL_BPAM}       
                    </div>
                    <div class="banners3text" id="banners3textbp">
                        {$LABEL_BPAMB}          
                    </div>
                </a>        
            </div>

            <div class="marcterme banners-alcalde banner-alcalde-center">
                <a href="{$url_defensor}" target="_self" title="Defensor de la ciutadania">
                    <img src="/images/banners/bndefensor2.jpg">
                    <div class="banners3text" id="banners3textb">
                        {$LABEL_BDEFENSOR}       
                    </div>
                    <div class="banners3text" id="banners3textbp">
                        {$LABEL_BDEFENSORB}          
                    </div>
                </a>        
            </div>

            <div class="marcterme banners-alcalde banner-alcalde-last">
                <a href="{$url_queixes}" target="_self" title="Suggeriments i queixes">
                    <img src="/images/banners/bnqueixes.jpg">
                    <div class="banners3text" id="banners3textb">
                        {$LABEL_BQUEIXES}      
                    </div>
                    <div class="banners3text" id="banners3textbp">
                        {$LABEL_BQUEIXES2}         
                    </div>
                </a>        
            </div>

            </div>

        </div>
        <!--/ L'ALCALDE -->
        

        <!-- L'AJUNTAMENT A UN CLIC -->
        <div class="row">

            <div class="titolsSeccionsHome titolBig">
                {$LABEL_TITOLS1}&nbsp;<font color="#000">{$LABEL_TITOLS1B}</font>
            </div>

            <a href="{$url_ajuntament}">
                <div class="marcterme sigleElementPosition">
                    <img class="tamany-img-aj-clic" src="/images/botons/ajuntaclic.jpg">
                </div>
            </a>

        </div>
        <!--/ L'AJUNTAMENT A UN CLIC -->

        

        <!-- + INFORMACIÓ -->
        <div class="row">

            <div class="titolsSeccionsHome titolBig">
                {$LABEL_TITOL3}
            </div>

            <div class="div-banners-mes-informacio">

            <a href="{$url_iviapublica}" target="_self" alt="Incidències a la via pública">
                <div class="marcfinestra banner-mes-informacio">
                
                    <div id="banner4ima">
                        <img src="/images/banners/bniviapub.jpg">
                    </div>
                    <div class="banners4text" id="banners4textb">
                        {$LABEL_BIVIAP}
                    </div>
                    <div class="banners4text" id="banners4textbp">
                        {$LABEL_BIVIAP2}
                    </div>
                </div>
            </a>

            <a href="{$url_mobilitat}" target="_self">
                <div class="marcfinestra banner-mes-informacio">
                    <div id="banner4ima">               
                        <img src="/images/banners/bnbus.jpg">
                    </div>
                    <div class="banners4text" id="banners4text">
                        {$LABEL_BBUS}
                    </div>
                </div>
            </a>

            <a href="/deixalleria/index.php">
                <div class="marcfinestra banner-mes-informacio">
                    <div id="banner4ima">               
                        <img src="/images/banners/bndeixa.jpg">
                    </div>
                    <div class="banners4text" id="banners4text">
                        {$LABEL_BDEIXA}
                    </div>
                </div>
            </a>

            <a href="{$url_turgencies}" target="_self">
                <div class="marcfinestra banner-mes-informacio">
                    <div id="banner4ima">               
                        <img src="/images/banners/bntelfurg.jpg">
                        <div class="banners4text" id="banners4textb">
                            {$LABEL_TITOL4_2}
                        </div>
                        <div class="banners4text" id="banners4textbp">
                            {$LABEL_TITOL4_2B}
                        </div>
                    </div>
                </div>
            </a>

            <!--
            <a href="/correus/index.php" alt="Directori telf. i correus">
                <div class="marcfinestra banner-mes-informacio">
                    <div id="banner4ima">               
                        <img src="/images/banners/bndirtelcorreus.jpg">
                        <div class="banners4text" id="banners4textb">
                            {$LABEL_BDIRECTORI}
                        </div>
                        <div class="banners4text" id="banners4textbp">
                            {$LABEL_BDIRECTORIB}
                        </div>
                    </div>
                </div>
            </a>
            -->

            <a href="https://www.seu-e.cat/web/tortosa/govern-obert-i-transparencia/serveis-i-tramits/serveis/calendari-dies-inhabils">
                <div class="marcfinestra banner-mes-informacio banner-mes-informacio-last">
                    <div id="banner4ima">               
                        <img src="/images/banners/bnfestius.jpg">
                        <div class="banners4text" id="banners4textb">
                            {$LABEL_BFESTIUS}
                        </div>
                        <div class="banners4text" id="banners4textbp">
                            {$LABEL_BFESTIUS2}
                        </div>
                    </div>
                </div>
            </a>

            </div>

        </div>
        <!--/ + INFORMACIÓ -->

        <!-- ALTRES WEBS -->
        <div class="row">

            <div class="titolsSeccionsHome titolBig">
                {$LABEL_TITOL4}
            </div>

            <div class="div-altres-webs">

            <div class="marcfinestra bannerwebs1 banner-webs-separation">
                <a href="http://www.bibliotecaspublicas.es/tortosa/index.jsp" target="_blank">
                    <img src="/images/banners/btbiblio4.jpg">
                </a>
            </div>

            <div class="marcfinestra bannerwebs1 banner-webs-separation">
                <a href="http://cfo.tortosa.cat/" target="_blank">
                    <img src="/images/banners/btcfo4.jpg">
                </a>
            </div>

            <div class="marcfinestra bannerwebs1 banner-webs-separation">
                <a href="http://viverempreses.tortosa.cat/" target="_blank">
                    <img src="/images/banners/btviver4.jpg">
                </a>
            </div>

            <div class="marcfinestra bannerwebs1 banners-web-last">
                <a href="http://documents.tortosa.cat/" target="_blank">
                    <img src="/images/banners/btpatrimdi4.jpg">
                </a>
            </div>

            <div class="marcfinestra bannerwebs2 banner-webs-separation">
                <a href="http://www.puntjove.tortosa.cat/" target="_blank">
                    <img src="/images/banners/btpij4.jpg">
                </a>
            </div>

            <div class="marcfinestra bannerwebs2 banner-webs-separation">
                <a href="http://www.escolamunicipaldemusicadetortosa.cat/" target="_blank">
                    <img src="/images/banners/btmusica4.jpg">
                </a>
            </div>

            <div class="marcfinestra bannerwebs2 banner-webs-separation">
                <a href="http://www.escolamunicipaldeteatredetortosa.cat/" target="_blank">
                    <img src="/images/banners/btteatre4.jpg">
                </a>
            </div>

            <div class="marcfinestra bannerwebs2 banners-web-last">
                <a href="http://tortosasport.cat/" target="_blank">
                    <img src="/images/banners/btsport4.jpg">
                </a>
            </div>

            </div>

        </div>
        <!--/ ALTRES WEBS -->

        <!-- ALTRES + CERCADOR -->
        <div class="row">

            <!-- LINKS -->
            <div class="column-left-altres-cercador">

                <div class="titolsSeccionsHome titolSmall">
                    {$LABEL_TITOL4_3}
                </div>

                <div class="div-altres">

                    <ul class="another-webs">
                        <li>
                            <a href="http://www.cpnl.cat/xarxa/cnlterresebre/" target="_blank">
                                <i class="icon-link-ext"></i>
                                <strong> CNL Terres de l'Ebre.</strong>
                                <br>
                                Consorci per a la Normalització Lingüística
                            </a>
                        </li>
                        <br>
                        <li>
                            <a href="{$url_lgtbi}">
                                <i class="icon-link-ext"></i>
                                <strong> Lesbianes, gais, transgèneres, bisexuals i intersexuals.</strong>
                                <br>Departament de Treball, Afers Socials i Famílies
                            </a>
                        </li>
                        <br>
                        <li>
                            
                                <i class="icon-link-ext"></i>
                                <a href="http://www.tortosa.altanet.org/ICDfullet_violenciesexuals.pdf">
                                <strong>Dia internacional contra la violència envers les dones</strong>
                                <br>
                                <a href="http://dones.gencat.cat/ca/ambits/sensibilitzacio/campanyes/dia_internacional_contra_violencia_dones/" target="_blank">
                                Institut Català de les Dones
                                <a>
                            </a>
                        </li>
                    </ul>

                </div>

            </div>

            <!-- CERCADOR -->
            <div class="column-right-altres-cercador">

                <div class="titolsSeccionsHome titolSmall">
                    {$LABEL_TITOL4_4}
                </div>

                <div class="div-cercador">

                <div class="cercador marcterme">
                    <form action="http://cercador.aocat.net" method="get" name="Cercador" id="Cercador" style="margin:0;padding:0;">
                        <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td >
                                    <input name="paraula" type="text" id="paraula" size="25" maxlength="180" title="Cerca, drecera alt + b" accesskey="b" />
                                    <input type="submit" name="Submit" value="Cercar" />
                                </td>
                                <td align="right" valign="top"><a href="http://www.aocat.net/" target="_blank">    <img style="float: right;" src="/images/icercaaoc.jpg" alt="Impulsat per AOC" border="0" /></a>
                                </td>
                            </tr>
                        </table>
                        <table  width="100%" border="0" cellspacing="0" cellpadding="0" id="cercadorAOC">
                            <tr>
                                <td width="27"><input name="Origen" type="radio" value="20" checked="checked"/></td>
                                <td width="108">Ajuntament</td>
                                <td width="25"><input name="Origen" type="radio" value="0" /></td>
                                <td width="206">Totes les administracions</td>
                                <td width="75"><input type="hidden" name="tipusCerca" value="A" />
                                <input type="hidden" name="OrigenCerca" value="43155" />
                                <input type="hidden" name="organisme" value="43155" /></td>
                            </tr>
                        </table>
                    </form>
                </div>

                </div>

            </div>

        </div>
        <!--/ ALTRES + CERCADOR -->

        <!-- INFOPARTICIPA -->
        <div class="row">

        <div class="div-infoparticipa">

            <div class="info-participa-banner separation-banner-info-participa">
                <a href="http://www.infoparticipa.com/index/mapa/" target="_blank">
                    <img src="/images/infoparticipa/2013.jpg">
                </a>
            </div>
            <div class="info-participa-banner separation-banner-info-participa">
                <a href="http://www.infoparticipa.com/index/mapa/" target="_blank">
                    <img src="/images/infoparticipa/2014.jpg">
                </a>
            </div>
            <div class="info-participa-banner separation-banner-info-participa">
                <a href="http://www.infoparticipa.com/index/mapa/" target="_blank">
                    <img src="/images/infoparticipa/2015.jpg">
                </a>
            </div>
            <div class="info-participa-banner info-participa-banner-last">
                <a href="http://www.infoparticipa.com/index/mapa/" target="_blank">
                    <img src="/images/infoparticipa/2016.jpg">
                </a>
            </div>

        <div>

        </div>
        <!--/ INFOPARTICIPA -->        

    </div>
</div>