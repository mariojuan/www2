<html>
	<head>
        {include file="head.tpl"}
        <style>
            #tcentre_planA .graf {
                width: 100%;
            }
        </style>
	</head>
	<body>
		{include file="header.tpl"}
        <div id="page-wrap">

        <div class="contenedor-responsive">

            <div id="conttranstotalcos">
                <div id="conttranscos">
                    <div id="btranspresen">
                        <div id="transimatge">
                            <img src=/images/laciutat/tira.jpg / class="img-slider-territori">
                        </div>
                        <div id="transimatgeText">
                            <h1>
                                {$LABEL_TITOL1}
                            </h1>
                        </div>
                    </div>
                    <div id="tcentre_planA" style="left: 0 !important;">
                        <!--<iframe src="http://43500.tortosa.cat:8081/WebAJT/EstadistiquesHabitantsServlet" width="900" height="600" frameborder="0" style="border:solid 0px; width: 900px;" scrolling="no"></iframe>-->
                        <h2>Població totals a data 18/12/2017</h2>
                        <div style="margin: 0 auto 25px auto">
                            <img src="/estadistica-habitants/taula-homes-dones-totals.jpg" alt="Taula Homes - Dones totals">
                        </div>
                        <div style="margin: 0 auto 25px auto">
                            <img src="/estadistica-habitants/homes-dones-totals.jpg" alt="Totals Homes - Dones" class="graf">
                        </div>
                        <h2>Població per anys</h2>
                        <div style="margin: 0 auto 25px auto">
                            <img src="/estadistica-habitants/taula-homes-dones-per-anys.jpg" alt="Taula Homes - Dones per anys">
                        </div>
                        <div style="margin: 0 auto 25px auto">
                            <img src="/estadistica-habitants/poblacio-total-per-anys.jpg" alt="Població total per anys" class="graf">
                        </div>
                        <div style="margin: 0 auto 25px auto">
                            <img src="/estadistica-habitants/homes-dones-per-anys.jpg" alt="Homes - Dones per anys" class="graf">
                        </div>
                        <h2>Població per nuclis a data 18/12/2017</h2>
                        <div style="margin: 0 auto 25px auto">
                            <img src="/estadistica-habitants/taula-poblacio-per-nuclis.jpg" alt="Taula Població per nuclis">
                        </div>
                        <div style="margin: 0 auto 25px auto">
                            <img src="/estadistica-habitants/poblacio-total-per-nuclis.jpg" alt="Població per nuclis" class="graf">
                        </div>
                        <h2>Població per pais de procedència a data 18/12/2017</h2>
                        <div style="margin: 0 auto 25px auto">
                            <img src="/estadistica-habitants/taula-poblacio-per-pais.jpg" alt="Taula Població per pais de procedència">
                        </div>
                    </div>
                    <div class="separator"></div>
                </div>
            </div>

        </div>            

        </div>
		{include file="footer.tpl"}
	</body>
</html>