<html>
	<head>
        {include file="head.tpl"}
        <style>
            #tcentre_planA .graf {
                width: 100%;
            }
        </style>
	</head>
	<body>
		{include file="header.tpl"}
        <div id="page-wrap">

        <div class="contenedor-responsive">

            <div id="conttranstotalcos">
                <div id="conttranscos">
                    <div id="btranspresen">
                        <div id="transimatge">
                            <img src=/images/laciutat/tira.jpg / class="img-slider-territori">
                        </div>
                        <div id="transimatgeText">
                            <h1>
                                {$LABEL_TITOL1}
                            </h1>
                        </div>
                    </div>
					
					<ul id='menu_planA'>
					{foreach $MENU_ESTADISTIQUES as $itemMenu}
						<a href={$itemMenu}&lang={$lang}>
						<li id='menu_planA_item'>
							{$itemMenu@key}
						</li>
						</a>
					{/foreach}
					</ul>
					
					<div id="tex1tintroinfeco" class="taulaicorpo">	
							{if $tipus!=NULL} 			    	
								{if $tipus=='TT'} 
									{$LABEL_TT}
								{else if $tipus=='NP'}
									{$LABEL_NP}
								{else if $tipus=='AN'}
									{$LABEL_AN}
								{else if $tipus=='EQ'}
									{$LABEL_EQ}
								{else if $tipus=='EG'}
									{$LABEL_EG}
								{else if $tipus=='PP'}
									{$LABEL_PP}
								{else if $tipus=='EN'}
									{$LABEL_EN}
								{else}
									{$LABEL_TC1}
								{/if}	
							{else}
								{$LABEL_TC1}
							{/if}	
							<br>
							<br>
							<br>
					</div>

                    <div class="separator"></div>
                </div>
            </div>

        </div>            

        </div>
		{include file="footer.tpl"}
	</body>
</html>