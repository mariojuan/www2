<html lang="es">

<head>
    <meta charset="utf-8"/>
    <title>Ajuntament de Tortosa - Consultes</title>
    <script type="text/javascript" src="http://www.tortosa.cat/webajt/scripts/data.js" language="javascript"></script>
    <link rel="stylesheet" href="{$path_eleccions}/style.css" type="text/css" media="screen" />
    <script type="text/javascript" src="{$path_charts}/fusioncharts.js"></script>
    <script type="text/javascript" src="/js/jquery.js"></script>
    <script src="http://www.tortosa.cat/js/google_analytics.js"></script>
    <script>
    	$( document ).ready(function() {
		    $( "#eleccions" ).change(function() {
			  window.location.href = 'consultes.php?accio=participacio_totals&eleccions='+( $( "#eleccions" ).val() );
			});
		});
    	function PrintElem(elem, web) {
    		$('#logos').css('display','block');
			$('#printer').css('display','none');
			switch (web) {
				case "participacio_totals":
					$('#chart-1').css('display','none');
					$('#chart-2').css('display','none');
					break;
				default:
					break;
			}
		    Popup(jQuery(elem).html());
		}

		function Popup(data) {
		    var mywindow = window.open('', 'my div', 'height=400,width=600');
		    mywindow.document.write('<html><head><title></title>');
		    mywindow.document.write('<link rel="stylesheet" href="{$path_eleccions}/style.css" type="text/css" type="text/css" />');  
		    mywindow.document.write('<style type="text/css">.test { color:red; } </style></head><body>');
		    mywindow.document.write(data);
		    mywindow.document.write('</body></html>');
		    mywindow.document.close();
		    mywindow.print();                        
		}

    </script>
</head>


<body>