<!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->
<div id="page-wrap">

<div class="contenedor-responsive">

    <div id="conttranstotalcos">
        <div id="conttranscos">
        
            <div id="btranspresen">
                <div id="transimatge">
                <img src="{$image}" class="img-slider-territori"/>
                </div>
                <div id="transimatgeText">
                    <h1>
                    {$LABEL_TITOL1}
                    <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                    </h1>
                </div>
            </div>

            <ul id='menu_planA'>
            {foreach $MENU as $itemMenu}
                <a href={$itemMenu['link']} target="_{$itemMenu['target']}">
                <li id='menu_planA_item'>
                    {$itemMenu['name']}
                </li>
                </a>
            {/foreach}
            </ul>
            
            <div id="tcentre_planA">
                {if $type==1}
                <!-- Presentació Arxiu-->
                    <font size=4 color=#666666>{$LABEL_TC0}</font>
                    <p>{$LABEL_TC1}</p>
                    <p>{$LABEL_TC2}</p>
                    <p>{$LABEL_TC3}</p>

                {elseif $type==2}
                 <!-- Tràmits de Consulta -->
                    <font size=4 color=#666666>{$LABEL_TC0}</font>
                  
                    <p>
                        {$LABEL_TC1}
                        <a href="mailto:arxiu.ajuntament@tortosa.cat" target="_blank">{$LABEL_TC2}</a>
                        {$LABEL_TC3}
                    </p>   

                {elseif $type==3}
                    <!-- Documents Consultables-->
                     <h2> {$LABEL_TC0}</h2>
                    
                    <div id="taulaicorpo arxiu_taula">
                        <!-- Capsal de la taula-->
                        <div id='filaicorpo'>
                            <div  class="colicorpo arxiu_col1 arxiu_col_cap">
                                 {$LABEL_TC0_C1}                                
                            </div>
                            <div  class="colicorpo arxiu_col2 arxiu_col_cap">
                                {$LABEL_TC0_C2}
                            </div>
                            <div  class="colicorpo arxiu_col3 arxiu_col_cap">
                                {$LABEL_TC0_C3}
                            </div>
                        </div>

                        
                        {foreach $TC_DOC_CONSULTABLES as $item}
                           <div id='filaicorpo'> 
                                {if $item['col1']/2==intval ($item['col1']/2)}<!-- columa color -->
                                    <div   class="colicorpo arxiu_col1 arxiu_col_alterna"> <!-- Columna 1 -->
                                        {$item['col1']}
                                    </div>
                                    <div   class="colicorpo arxiu_col2 arxiu_col_alterna"> <!-- Columna 2 -->
                                        {$item['col2']}
                                    </div>
                                    <div   class="colicorpo arxiu_col3 arxiu_col_alterna"> <!-- Columna 3 -->
                                        {$item['col3']}
                                    </div>
                                    </div> 
                                {else}
                                
                                    <div   class="colicorpo arxiu_col1"> <!-- Columna 1 -->
                                        {$item['col1']}
                                    </div>
                                    <div   class="colicorpo arxiu_col2"> <!-- Columna 2 -->
                                        {$item['col2']}
                                    </div>
                                    <div   class="colicorpo arxiu_col3"> <!-- Columna 3 -->
                                        {$item['col3']}
                                    </div>
                                    </div> 
                                {/if}

                        {/foreach}              
                        
                    </div>                    


                {elseif $type==4}
                 <!-- Dipòsit legal-->
                    <font size=4 color=#666666>{$LABEL_TC0}</font>
                    <p>{$LABEL_TC1}</p>  
                    <p>{$LABEL_TC2}</p>  
                    <p>
                        <a href="http://www.tortosa.cat/webajt/ajunta/arxiu/dl_2013_2015.pdf" target="_blank">{$LABEL_TC3} </a>
                    </p>  
                         
                {elseif $type==5}
                  <!-- Repositori Digital-->
                    <font size=4 color=#666666>{$LABEL_TC0}</font>
                    <p>
                        {$LABEL_TC1} <a href="http://documents.tortosa.cat/" target="_blank">  {$LABEL_TC2} </a>   {$LABEL_TC3}
                    </p>     
                  
                    <p>{$LABEL_TC4}</p>  
                    <p>{$LABEL_TC5}</p>  
                    <p>{$LABEL_TC6}</p> 

                {elseif $type==6}
                
                {/if}
            </div>
            <div class="separator"></div>
        </div>
    </div>
    </div>
</div>
