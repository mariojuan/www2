<div id="conttranstotalcos">
    {include file="{$path_templates}/header_web_antiga.tpl"}
    <div id="conttranscos">
        <div class="home">
            <p id="title">
                {$txt_title}
            </p>
            <p id="subtitle">
                {$txt_subtitle}
            </p>
            <form name="consult" id="consult" method="post" action="" novalidate="novalidate">
            <p id="formtitle">
                {$txt_formtitle}
            </p>
            <p>
                <label for="tdocument">{$txt_document_type}</label>
                <select id="tdocument" name="tdocument">
                    <option value="">{$txt_select}</option>
                    <option value="1">DNI / NIF</option>
                    <option value="2">{$txt_passport}</option>
                    <option value="3">{$txt_resident_card}</option>
                </select>
                <label id="tdocument-error" class="error" for="tdocument-error" style="display: none;"></label>
            </p>
            <p><div class="separator"></div></p>
            <p class="option1">
                <span>
                    {$txt_number}<br/>
                    <input type="text" id="dni_number" name="dni_number" value="{$txt_number}" maxlength="8">
                </span>
                <span>
                    {$txt_letter}<br/>
                    <input type="text" id="dni_letter" name="dni_letter" value="{$txt_letter}" maxlength="1">
                </span>
                <label id="option1-error" class="error" for="option1-error" style="display: none;"></label>
            </p>
            <p class="option2">
                <span>
                    {$txt_passport_number}<br/>
                    <input type="text" id="option2_passport_number" name="option2_passport_number" value="{$txt_passport_number}">
                </span>
                <label id="option2-error" class="error" for="option2-error" style="display: none;"></label>
            </p>
            <p class="option3">
                <span>
                    {$txt_letter}<br/>
                    <input type="text" id="tr_letter_1" name="tr_letter_1" value="{$txt_letter}" maxlength="1">
                </span>
                <span>
                    {$txt_number}<br/>
                    <input type="text" id="tr_number" name="tr_number" value="{$txt_number}" maxlength="8">
                </span>
                <span>
                    {$txt_letter}<br/>
                    <input type="text" id="tr_letter_2" name="tr_letter_2" value="{$txt_letter}" maxlength="1">
                </span>
                <label id="option3-error" class="error" for="option3-error" style="display: none;"></label>
            </p>
            <p class="captcha">
                {$txt_captcha}
                {$recaptcha}
                <label id="recaptcha-error" class="error" for="recaptcha-error" style="display: none;"></label>
            </p>
            <p>
                <input type="button" name="enviar" id="enviar" value="Acceptar">
                <input type="reset" name="esborrar" id="esborrar" value="Esborrar">
            </p>
            <p class="lopd">
                {$txt_lopd}
            </p>
            </form>
            <p>
                <div id="result">

                </div>
                <div id="no-result">
                    <p>{$txt_no_result_1}</p>
                </div>
            </p>
        </div>
    </div>
    {include file="{$path_templates}/footer_web_antiga.tpl"}
</div>