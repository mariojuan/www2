<!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->
<div id="page-wrap" >
   <div class="contenedor-responsive">
      <div id="contpdctotalcos">
         <div id="conttranscos">
            <div id="btranspresen">
               <div id="transimatge">			
                  <img src="{$image}" class="img-slider-territori"/>			
               </div>
               <div id="transimatgeText">
                  <h1>
                     {$LABEL_TITOL1}
                     <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                  </h1>
               </div>
            </div>
            <ul id='menu_planA'>
               {foreach $MENU as $itemMenu}
               <a href={$itemMenu}>
                  <li id='menu_planA_item'>
                     {$itemMenu@key}
                  </li>
               </a>
               {/foreach}
            </ul>
            <div id="tcentre_planA">
               <font size=4 color=#666666>
               {$LABEL_TITOL0}
               </font>
               {if $item==1}
               <p>{$LABEL_TC1}<br>
               <p>{$LABEL_TC2}<br>
               <p>{$LABEL_TC3}</p>
               <p>{$LABEL_TC4}</p>
               <font size=4 color=#666666>
               {$LABEL_TITOL20}<br>
               </font>
               <p>{$LABEL_TC5}<br>
               <p>{$LABEL_TC6}<br>
               <p>{$LABEL_TC7}</p>
               <p>{$LABEL_TC8}</p>
               <p>{$LABEL_TC9}</p>
               <ul>
                  <li>{$LABEL_TC10}</li>
                  <li>{$LABEL_TC11}</li>
                  <li>{$LABEL_TC12}</li>
               </ul>
               {else if $item==2}
               <center>
                  <a href="https://contractaciopublica.gencat.cat/ecofin_pscp/AppJava/cap.pscp?reqCode=viewDetail&keyword=tortosa&idCap=14665802&ambit=" target="_blank">
                     <div id="caixapdc1" class="marcfinestra caixapdcMod">
                        {$LABEL_TC1}<br>{$LABEL_TC11}
                     </div>
                  </a>
                  <div id="caixapdc2">
                     <i class="icon-link"></i>&nbsp;
                     <a href="http://www.tortosa.cat/webajt/gestiointerna/licitacions/partpublica/index3.asp" target="_blank">
                     {$LABEL_TC1}.&nbsp;
                     {$LABEL_TC12}
                     </a>
                  </div>
               </center>
               <div id="titolaltrespdc">
                  {$LABEL_TC8}
               </div>
               <ul class="ul_indexpcMod">
                  <li><i class="icon-link-ext"></i>&nbsp;<a href="http://transparencia.aiguesdetortosa.cat/ca/perfilcontractant" target="_blank">Empresa Municipal de Serveis Públics, SL</a></li>
                  <li><i class="icon-link-ext"></i>&nbsp;<a href="http://www.gumtsa.cat/licit.php" target="_blank">Empresa Gestió Urbanística Municipal de Tortosa, SA (GUMTSA)</a></li>
                  <li><i class="icon-link-ext"></i>&nbsp;<a href="https://contractaciopublica.gencat.cat/ecofin_pscp/AppJava/cap.pscp?reqCode=viewDetail&idCap=26439161" target="_blank">Tortosa Media, SL</a></li>
                  <li><i class="icon-link-ext"></i>&nbsp;<a href="http://www.tortosasport.cat/contractes-i-subvencions/" target="_blank">Tortosasport, SL</a></li>
                  <li><i class="icon-link-ext"></i>&nbsp;<a href="http://clinicaterresebre.cat/?cat=6" target="_blank">Tortosa Salut, SL</a></li>
                  <li><i class="icon-link-ext"></i>&nbsp;<a href="http://hospitalsantacreutortosa.cat/instruccions-gesat/" target="_blank">Gestió Sanitària i Assistencial de Tortosa SAM (GESAT)</a></li>
                  <li><i class="icon-link-ext"></i>&nbsp;<a href="http://hospitalsantacreutortosa.cat/instruccions-epel/" target="_blank">EPEL Hospital Llars de la Santa Creu</a></li>
               </ul>
               {else if $item==3}
                  <p><a href="http://www.tortosa.cat/webajt/pressupost/2017/2017ResumTercers.pdf" target="_blank">{$LABEL_TC1}</a></p>
                  <p><a href="http://www.tortosa.cat/webajt/pressupost/2016/2016ResumTercers.pdf" target="_blank">{$LABEL_TC2}</a></p>
                  <p><a href="http://www.tortosa.cat/webajt/pressupost/2015/2015ResumTercers.pdf" target="_blank">{$LABEL_TC3}</a></p>
               {/if}
            </div>
         </div>
      </div>
   </div>
</div>
