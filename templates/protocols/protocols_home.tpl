<!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->
<div id="page-wrap">
<div class="contenedor-responsive">
   <div id="conttranstotalcos">
      <div id="conttranscos">
         <div id="btranspresen">
            <div id="transimatge">          
               <img src="{$image}" class="img-slider-territori"/>           
            </div>
            <div id="transimatgeText">
               <h1>
                  {$LABEL_TITOL1}
                  <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
               </h1>
            </div>
         </div>
         <ul id='menu_planA'>
            {foreach $MENU as $itemMenu}
            <a href={$itemMenu['link']} target="_{$itemMenu['target']}">
               <li id='menu_planA_item'>
                  {$itemMenu['name']}
               </li>
            </a>
            {/foreach}
         </ul>
         <div id="tcentre_planA">
            <font size=4 color=#666666>{$LABEL_TITOL0}</font>
            {if $item==1}
            <p>{$LABEL_TC1}</p>
            {else if $item==4}
            <p>{$LABEL_TC1}<strong>{$LABEL_TC2}</strong>{$LABEL_TC3}</p>
            <p>{$LABEL_TC4}</p>
            <p><a href="http://www.tortosa.cat/webajt/decaleg.pdf" target="_blank">{$LABEL_TC5}</a></p>

            {else if $item==5}
            <div id="taulaicorpo" class="taulaBannersMod">
               <div id="filaicorpo">
                  <div class="colicorpo taulaBannersMod" id="col1icorpo">.TIF (6,97Mb) </div>
                  <div class="colicorpo taulaBannersMod"><a href="http://www.tortosa.cat/webajt/ajunta/imatge/horitzontal.tif" target="_blank"><img src="/images/protocols/logo1.jpg"></a></div>
                  <div class="colicorpo taulaBannersMod" id="col3icorpo">{$LABEL_TC4}</div>
               </div>
               <div id="filaicorpo">
                  <div class="colicorpo taulaBannersMod" id="col1icorpo">.JPG (461Kb) </div>
                  <div class="colicorpo taulaBannersMod" id="col2icorpo"><a href="http://www.tortosa.cat/webajt/ajunta/imatge/horitzontal.jpg" target="_blank"><img src="/images/protocols/logo1.jpg"></a></div>
                  <div class="colicorpo taulaBannersMod" id="col3icorpo">{$LABEL_TC4}</div>
               </div>
               <div id="filaicorpo">
                  <div class="colicorpo taulaBannersMod" id="col1icorpo">.TIF (5,61Mb) </div>
                  <div class="colicorpo taulaBannersMod" id="col2icorpo"><a href="http://www.tortosa.cat/webajt/ajunta/imatge/vertical.tif" target="_blank"><img src="/images/protocols/logo2.jpg"></a></div>
                  <div class="colicorpo taulaBannersMod" id="col3icorpo">{$LABEL_TC5}</div>
               </div>
               <div id="filaicorpo">
                  <div class="colicorpo taulaBannersMod" id="col1icorpo">.JPG (455Kb) </div>
                  <div class="colicorpo taulaBannersMod" id="col2icorpo"><a href="http://www.tortosa.cat/webajt/ajunta/imatge/vertical.jpg" target="_blank"><img src="/images/protocols/logo2.jpg"></a></div>
                  <div class="colicorpo taulaBannersMod" id="col3icorpo">{$LABEL_TC5}</div>
               </div>
               <div id="filaicorpo">
                  <div class="colicorpo taulaBannersMod" id="col1icorpo">.FH9 (121Kb) </div>
                  <div class="colicorpo taulaBannersMod" id="col2icorpo"><a href="http://www.tortosa.cat/webajt/ajunta/imatge/horitzontal.fh9" target="_blank"><img src="/images/protocols/logo3.jpg"></a></div>
                  <div class="colicorpo taulaBannersMod" id="col3icorpo">{$LABEL_TC6}</div>
               </div>
               <div id="filaicorpo">
                  <div class="colicorpo taulaBannersMod" id="col1icorpo">.PDF (45,4Kb) </div>
                  <div class="colicorpo taulaBannersMod" id="col2icorpo"><a href="http://www.tortosa.cat/webajt/ajunta/imatge/escuts.pdf" target="_blank"><img src="/images/protocols/logo3.jpg"></a></div>
                  <div class="colicorpo taulaBannersMod" id="col3icorpo">{$LABEL_TC6}</div>
               </div>
               <div id="filaicorpo">
                  <div class="colicorpo taulaBannersMod" id="col1icorpo">.JPG (125Kb) </div>
                  <div class="colicorpo taulaBannersMod" id="col2icorpo"><a href="http://www.tortosa.cat/webajt/ajunta/imatge/aiguestortosa.jpg" target="_blank"><img src="/images/protocols/logo4.jpg"></a></div>
                  <div class="colicorpo taulaBannersMod" id="col3icorpo">Aigües de Tortosa</div>
               </div>
               <div id="filaicorpo">
                  <div class="colicorpo taulaBannersMod" id="col1icorpo">.JPG (575Kb) </div>
                  <div class="colicorpo taulaBannersMod" id="col2icorpo"><a href="http://www.tortosa.cat/webajt/ajunta/imatge/tortosasport.jpg" target="_blank"><img src="/images/protocols/logo5.jpg"></a></div>
                  <div class="colicorpo taulaBannersMod" id="col3icorpo">Tortosasport</div>
               </div>
            </div>
            {/if}

         </div>
      </div>
   </div>
   </div>
</div>