<html>
   <head>
      {include file="head.tpl"}
   </head>
   <body>
      {include file="header.tpl"}
      <div id="page-wrap">

         <div class="contenedor-responsive">

            <div id="conttranstotalcos">

               <div id="conttranscos">

                  <div id="btranspresen">

                     <div id="transimatge">
                        <img src="{$image}" class="img-slider-territori"/>
                     </div>

                     <div id="transimatgeText">
                        <h1>
                           {$LABEL_TITOL1}
                           <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                        </h1>
                     </div>
                     
                  </div>
                  <!-- Menu -->
                  {include file="equip-govern/menu.tpl"}
                  <!--// Menu -->
                  <div id="tcentre_planA">
                     <font size="4" color="#666666">{$LABEL_TC0}</font>
                     <p>{$LABEL_TC1}</p>
                     <table>
                        <thead>
                           <tr>
                              <th>{$LABEL_FOTO}</th>
                              <th>{$LABEL_NOM}</th>
                              <th>{$LABEL_GRUP}</th>
                              <th>{$LABEL_CARREC}</th>
                           </tr>
                        </thead>
                        <tbody>
                           {foreach $PERSONAL as $item}
                           <tr>
                              <td><a href="{$item['link']}" target="{$item['target']}"><img src="{$item['foto']}" class="avatar"/></a></td>
                              <td>{$item['name']}</td>
                              <td>{$item['group']}</td>
                              <td>{$item['occupation']}</td>
                           </tr>
                           {/foreach}
                        </tbody>
                     </table>
                     <!--
                        {foreach $PERSONAL as $item}
                            <ul>
                                <li>{$item['name']} - {$item['group']} - {$item['occupation']}</li>
                            </ul>
                        {/foreach}
                        
                        -->
                     {foreach $DECRETS as $item}
                     <p><a href="{$item['link']}" target="{$item['target']}">{$item['name']}</a></p>
                     {/foreach}
                  </div>
                  <div class="separator"></div>
               </div>
            </div>
         </div>
      </div>
      {include file="footer.tpl"}
   </body>
</html>