<html>
    <head>
        {include file="head.tpl"}
    </head>
	<body id="borsa">
		{include file="header.tpl"}
        <div id="page-wrap">
            <div id="conttranstotalcos">
                <div id="conttranscos">
                    <div id="btranspresen">
                        <div id="transimatge">
                            <img src="/images/laciutat/municipi/tira.jpg" />
                        </div>
                        <div id="transimatgeText">
                            <h1>
                                {$LABEL_TITOL1}
                            </h1>
                        </div>
                    </div>
                    <ul id='menu_planA'>
                        {foreach $MENU as $key=>$itemMenu}
                            <a href={$itemMenu['link']} target="{$itemMenu['target']}">
                                <li id='menu_planA_item'>
                                    {$itemMenu['name']}
                                </li>
                            </a>
                        {/foreach}
                    </ul>
                    <div id="tcentre_planA">
                        <h2>{$elements[0]->denominacio}</h2>
                        {if $elements|count>0}
                            <p><div class="field">{$lblAnunci}</div><div class="value-field">{$elements[0]->anunci}</div></p>
                            <p><div class="field">{$lblObjecte}</div><div class="value-field">{$elements[0]->objecte}</div></p>
                            <p><div class="field">{$lblDenominacio}</div><div class="value-field">{$elements[0]->denominacio}</div></p>
                            <p><div class="field">{$lblRegimJuridic}</div><div class="value-field">{$elements[0]->regim_juridic}</div></p>
                            <p><div class="field">{$lblCaracter}</div><div class="value-field">{$elements[0]->caracter}</div></p>
                            <p><div class="field">{$lblEscala}</div><div class="value-field">{$elements[0]->escala}</div></p>
                            <p><div class="field">{$lblSubescala}</div><div class="value-field">{$elements[0]->subescala}</div></p>
                            <p><div class="field">{$lblClasse}</div><div class="value-field">{$elements[0]->classe}</div></p>
                            <p><div class="field">{$lblGrup}</div><div class="value-field">{$elements[0]->grup}</div></p>
                            <p><div class="field">{$lblNumPlaces}</div><div class="value-field">{$elements[0]->num_places}</div></p>
                            <p><div class="field">{$lblSeleccio}</div><div class="value-field">{$elements[0]->seleccio}</div></p>
                            <p><div class="field">{$lblRequisits}</div><div class="value-field">{$elements[0]->requisits}</div></p>
                            <p><div class="field">{$lblPresenSol}</div><div class="value-field">{$elements[0]->presen_solicituds}</div></p>
                            <p><div class="field">{$lblDataIniciPresentacio}</div><div class="value-field">{$elements[0]->data_inici_presen}</div></p>
                            <p><div class="field">{$lblDataFiPresentacio}</div><div class="value-field">{$elements[0]->data_final_presen}</div></p>
                            <p><div class="field">{$lblDretsExamen}</div><div class="value-field">{$elements[0]->drets_examen}</div></p>
                            <p><div class="field">{$lblDocumentacio}</div><div class="value-field"><a href="http://www.tortosa.cat/webajt/gestiointerna/borsa/bases/{$elements[0]->bases}" target="_blank">{$lblBases}</a></div></p>
                            <p>
                                <div class="field">{$lblResultats}</div>
                                <div class="value-field">
                                    <ul>
                                        {if $elements[0]->llista_admesos_exclosos|trim!=""}
                                        <li><i class="icon-file-pdf"></i> <a href="http://www.tortosa.cat/webajt/gestiointerna/borsa/adm_excl_prov/{$elements[0]->llista_admesos_exclosos}" target="_blank">{$lblLlistaAdmisosExclososProvisional}</a></li>
                                        {/if}
                                        {if $elements[0]->llista_admesos_exclosos_defini|trim!=""}
                                            <li><i class="icon-file-pdf"></i> <a href="http://www.tortosa.cat/webajt/gestiointerna/borsa/adm_excl_def/{$elements[0]->llista_admesos_exclosos_defini}" target="_blank">{$lblLlistaAdmisosExclososDefinitiva}</a></li>
                                        {/if}
                                        {if $elements[0]->resultats1|trim!=""}
                                            <li><i class="icon-file-pdf"></i> <a href="http://www.tortosa.cat/webajt/gestiointerna/borsa/resultats/{$elements[0]->resultats1}" target="_blank">{$lblResultats}</a></li>
                                        {/if}
                                        {if $elements[0]->borsa_provisional|trim!=""}
                                            <li><i class="icon-file-pdf"></i> <a href="http://www.tortosa.cat/webajt/gestiointerna/borsa/resultats/{$elements[0]->borsa_provisional}" target="_blank">{$lblResultatsBorsaTreball}</a></li>
                                        {/if}
                                        {if $elements[0]->borsa_definitiva|trim!=""}
                                            <li><i class="icon-file-pdf"></i> <a href="http://www.tortosa.cat/webajt/gestiointerna/borsa/borsa/{$elements[0]->borsa_definitiva}" target="_blank">{$lblLlistaBorsaTreball}</a></li>
                                        {/if}
                                    </ul>
                                </div>
                            </p>
                        {/if}
                        <div id="buttons-container">
                            <input type="button" name="enrere" id="enrere" value="{$lblTornar}" onclick="JavaScript:window.location.href='/borsa/index.php?lang={$lang}&page={$page}';">
                        </div>
                    </div>
                </div>
            </div>
        </div>
		{include file="footer.tpl"}
	</body>
</html>