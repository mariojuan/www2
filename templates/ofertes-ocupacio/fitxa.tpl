<html>
    <head>
        {include file="head.tpl"}
    </head>
	<body id="ofertes-ocupacio">
		{include file="header.tpl"}
        <div id="page-wrap">
            <div id="conttranstotalcos">
                <div id="conttranscos">
                    <div id="btranspresen">
                        <div id="transimatge">
                            <img src="/images/laciutat/municipi/tira.jpg" />
                        </div>
                        <div id="transimatgeText">
                            <h1>
                                {$LABEL_TITOL1}
                            </h1>
                        </div>
                    </div>
                    <ul id='menu_planA'>
                        {foreach $MENU as $key=>$itemMenu}
                            <a href={$itemMenu['link']} target="{$itemMenu['target']}">
                                <li id='menu_planA_item'>
                                    {$itemMenu['name']}
                                </li>
                            </a>
                        {/foreach}
                    </ul>
                    <div id="tcentre_planA">
                        <h2>{$elements[0]->ENS}</h2>
                        {if $elements|count>0}
                            {if $elements[0]->NUMERO_EXPEDIENT!=""}
                            <p><div class="field">{$lblExpedient}:</div><div class="value-field">{$elements[0]->NUMERO_EXPEDIENT}</div></p>
                            {/if}
                            {if $elements[0]->DESCRIPCIO_TIPUS_OFERTA!=""}
                            <p><div class="field">{$lblTipusOferta}:</div><div class="value-field">{$elements[0]->TIPUS_OFERTA}</div></p>
                            {/if}
                            {if $elements[0]->DESCRIPCIO_CURTA!=""}
                            <p><div class="field">{$lblDescripcioCurta}:</div><div class="value-field">{$elements[0]->DESCRIPCIO_CURTA}</div></p>
                            {/if}
                            {if $elements[0]->OBJECTE!=""}
                            <p><div class="field">{$lblObjecte}:</div><div class="value-field">{$elements[0]->OBJECTE}</div></p>
                            {/if}
                            {if $elements[0]->DENOMINACIO!=""}
                            <p><div class="field">{$lblDenominacio}:</div><div class="value-field">{$elements[0]->DENOMINACIO}</div></p>
                            {/if}
                            {if $elements[0]->REGIM_JURIDIC!=""}
                            <p><div class="field">{$lblRegimJuridic}:</div><div class="value-field">{$elements[0]->REGIM_JURIDIC}</div></p>
                            {/if}
                            {if $elements[0]->CARACTER!=""}
                            <p><div class="field">{$lblCaracter}:</div><div class="value-field">{$elements[0]->CARACTER}</div></p>
                            {/if}
                            {if $elements[0]->ESCALA!=""}
                            <p><div class="field">{$lblEscala}:</div><div class="value-field">{$elements[0]->ESCALA}</div></p>
                            {/if}
                            {if $elements[0]->SUBESCALA!=""}
                                <p><div class="field">{$lblSubescala}:</div><div class="value-field">{$elements[0]->SUBESCALA}</div></p>
                            {/if}
                            {if $elements[0]->CLASSE!=""}
                                <p><div class="field">{$lblClasse}:</div><div class="value-field">{$elements[0]->CLASSE}</div></p>
                            {/if}
                            {if $elements[0]->GRUP!=""}
                                <p><div class="field">{$lblGrup}:</div><div class="value-field">{$elements[0]->GRUP}</div></p>
                            {/if}
                            {if $elements[0]->NUM_PLACES!=""}
                                <p><div class="field">{$lblNumPlaces}:</div><div class="value-field">{$elements[0]->NUM_PLACES}</div></p>
                            {/if}
                            {if $elements[0]->SISTEMA_SELECCIO!=""}
                                <p><div class="field">{$lblSeleccio}:</div><div class="value-field">{$elements[0]->SISTEMA_SELECCIO}</div></p>
                            {/if}
                            {if $elements[0]->REQUISITS!=""}
                                <p><div class="field">{$lblRequisits}:</div><div class="value-field">{$elements[0]->REQUISITS}</div></p>
                            {/if}
                            {if $elements[0]->DATA_INICI_PRESENT_SOLLICITUDS!=""}
                                <p><div class="field">{$lblDataIniciPresentacioSollicituds}:</div><div class="value-field">{$elements[0]->DATA_INICI_PRESENT_SOLLICITUDS|date_format:"%e/%m/%Y"}</div></p>
                            {/if}
                            {if $elements[0]->DATA_FI_PRESENT_SOLLICITUDS!=""}
                                <p><div class="field">{$lblDataFiPresentacioSollicituds}:</div><div class="value-field">{$elements[0]->DATA_FI_PRESENT_SOLLICITUDS|date_format:"%e/%m/%Y"}</div></p>
                            {/if}
                            {if $elements[0]->DRETS_EXAMEN!=""}
                                <p><div class="field">{$lblDretsExamen}:</div><div class="value-field">{$elements[0]->DRETS_EXAMEN}</div></p>
                            {/if}
                            {if $elements[0]->DATA_INICI_PRESENT_ALEGACIONS!=""}
                                <p><div class="field">{$lblDataIniciPresentacioAllegacions}:</div><div class="value-field">{$elements[0]->DATA_INICI_PRESENT_ALEGACIONS|date_format:"%e/%m/%Y"}</div></p>
                            {/if}
                            {if $elements[0]->DATA_FI_PRESENT_ALEGACIONS!=""}
                                <p><div class="field">{$lblDataFiPresentacioAllegacions}:</div><div class="value-field">{$elements[0]->DATA_FI_PRESENT_ALEGACIONS|date_format:"%e/%m/%Y"}</div></p>
                            {/if}
                            {if $elements[0]->ESTAT!=""}
                                <p><div class="field">{$lblEstat}:</div><div class="value-field">{$elements[0]->ESTAT}</div></p>
                            {/if}
                            {if $elements[0]->DATA_INICI_VIGENCIA_BORSA_TREBALL!=""}
                                <p><div class="field">{$lblDataIniciVigenciaBorsaTreball}:</div><div class="value-field">{$elements[0]->DATA_INICI_VIGENCIA_BORSA_TREBALL|date_format:"%e/%m/%Y"}</div></p>
                            {/if}
                            {if $elements[0]->DURACIO_BORSA_TREBALL!=""}
                                <p><div class="field">{$lblDuracioBorsaTreball}:</div><div class="value-field">{$elements[0]->DURACIO_BORSA_TREBALL} {$lblDies}</div></p>
                            {/if}
                            {if $fitxers|COUNT>0}
                                <ul>
                                {foreach $fitxers as $fitxer}
                                     <li><a href="/public_files/ofertes_ocupacio/{$fitxer->NOM_FITXER}" target="_blank">{$fitxer->DESCRIPCIO}</li>
                                {/foreach}
                                </ul>
                            {/if}
                        {/if}
                        <div id="buttons-container">
                            <input type="button" name="enrere" id="enrere" value="{$lblTornar}" onclick="JavaScript:window.location.href='/borsa/index.php?lang={$lang}&page={$page}';">
                        </div>
                    </div>
                </div>
            </div>
        </div>
		{include file="footer.tpl"}
	</body>
</html>