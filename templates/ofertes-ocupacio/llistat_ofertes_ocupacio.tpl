<html>
   <head>
      {include file="head.tpl"}
   </head>
   <body id="borsa">
      {include file="header.tpl"}
      <div id="page-wrap">
      <div class="contenedor-responsive">
         <div id="conttranstotalcos">
            <div id="conttranscos">
               <div id="btranspresen">
                  <div id="transimatge">
                     <img src="/images/laciutat/municipi/tira.jpg" class="img-slider-territori"/>
                  </div>
                  <div id="transimatgeText">
                     <h1>
                        {$LABEL_TITOL1}
                        <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                     </h1>
                  </div>
               </div>
               <ul id='menu_planA'>
                  {foreach $MENU as $key=>$itemMenu}
                  <a href={$itemMenu['link']} target="{$itemMenu['target']}">
                     <li id='menu_planA_item'>
                        {$itemMenu['name']}
                     </li>
                  </a>
                  {/foreach}
               </ul>
               <div id="tcentre_planA">
                  <h2>{$LABEL_TITOL2}</h2>
                  <p>{$LABEL_TC1}</p>
                  <ul style="list-style-type: none; width: 80%">
                     {foreach from=$elements key=key item=value}
                        <li>
                           &#8226; {utf8_encode($value->DESCRIPCIO_CURTA)|trim} - <a href="/ofertes-ocupacio/fitxa.php?id={$value->ID}&page={$page}&lang={$lang}"><i class="icon-link" title="{utf8_encode($value->descripcio_curta)|trim}"></i></a>
                        <li>
                     {/foreach}
                  </ul>
                  </div>
                  <div id="pagination">
                     <ul>
                        {if $lastpage>1}
                        {if $page> 1}
                        {assign var="page_ant" value=$page - 1}
                        <li><a href="index.php?tipus={$tipus}&lang={$lang}&accio=list&page={$page_ant}">{$lblAnt}</a></li>
                        {/if}
                        {assign var="desp" value=$page + 4}
                        {assign var="firstpage" value=$page - 4}
                        {if $desp < $lastpage}
                        {assign var="lastpage" value=$page + 4}
                        {/if}
                        {if $firstpage < 1}
                        {assign var="firstpage" value=1}
                        {/if}
                        {for $counter=$firstpage to $lastpage}
                        {if $counter==$page}
                        <li>{$counter}</li>
                        {else}
                        <li><a href="index.php?tipus={$tipus}&lang={$lang}&accio=list&page={$counter}">{$counter}</a></li>
                        {/if}
                        {/for}
                        {if $page < $lastpage}
                        {assign var="page_seg" value=$page + 1}
                        <li><a href="index.php?tipus={$tipus}&lang={$lang}&accio=list&page={$page_seg}">{$lblSeg}</a></li>
                        {/if}
                        {else}
                        <li>1</li>
                        {/if}
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      {include file="footer.tpl"}
   </body>
</html>