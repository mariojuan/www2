<!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->
<div id="page-wrap">
   <div class="contenedor-responsive">
      <div id="conttranstotalcos">
         <div id="conttranscos">
            <div id="btranspresen">
               <div id="transimatge">			
                  <img src="{$image}" class="img-slider-territori"/>			
               </div>
               <div id="transimatgeText">
                  <h1>
                     {$LABEL_TITOL}
                  </h1>
               </div>
            </div>
            <div id="tcentre_municipi">
               {$LABEL_TC1}
               <br>
               <br>
               <b>{$lblRSessions}</b>{$LABEL_TC5}
               <br>
               <br>
               {if ($year)} 
               {$currentYear = $year}
               {else}
               {$currentYear = date("Y")}
               {/if}
               <form name="plens" id="fyear" method="post" action="index.php">
                  {$lblSelectAny}:&nbsp;&nbsp;
                  <select name="year" id="year" onChange="JavaScript:document.plens.submit();">
                     {for $cont=2007 to date("Y")}
                     {if ($cont == $currentYear)}
                     <option value='{$cont}' selected>{$cont}</option>
                     {else}
                     <option value='{$cont}'>{$cont}</option>
                     {/if}
                     {/for}
                  </select>
               </form>
               <br>

               <div class="div-plens-taula-separacio">

                  <div class="taulaPle">
                     {if $elements|count>0}
                     <div id="filaPlens">
                        <div class="colPlens ord-col1">
                           <h2>{$lblGener}</h2>
                           <ul>
                              {foreach from=$elements key=key item=value}
                              {if ($value->currentMonth == 1)}
                              <li>
                                 <a href="http://www.tortosa.cat/webajt/gestiointerna/organs_govern_simple/documents/{utf8_encode($value->nom_arxiu)}" target="_blank"  style="text-decoration:none; color:#000000">        
                                 {utf8_encode($value->data_inici)|date_format:"%D"} -  
                                 {utf8_encode($value->tipus_sessio)} - 
                                 {utf8_encode($value->tipus_document)}  
                                 </a>	
                              </li>
                              {/if}
                              {/foreach}
                           </ul>
                        </div>
                        <div class="colPlens ord-col2">
                           <h2>{$lblFebrer}</h2>
                           <ul>
                           {foreach from=$elements key=key item=value}
                           {if ($value->currentMonth == 2)}
                           <li>
                              <a href="http://www.tortosa.cat/webajt/gestiointerna/organs_govern_simple/documents/{utf8_encode($value->nom_arxiu)}" target="_blank"  style="text-decoration:none; color:#000000">        
                              {utf8_encode($value->data_inici)|date_format:"%D"} -  
                              {utf8_encode($value->tipus_sessio)} - 
                              {utf8_encode($value->tipus_document)}  
                              </a>	
                           </li>
                           {/if}
                           {/foreach}
                           <ul>
                        </div>
                        <div class="colPlens ord-col3">
                           <h2>{$lblMarc}</h2>
                           <ul>
                           {foreach from=$elements key=key item=value}
                           {if ($value->currentMonth == 3)}
                           <li>
                              <a href="http://www.tortosa.cat/webajt/gestiointerna/organs_govern_simple/documents/{utf8_encode($value->nom_arxiu)}" target="_blank"  style="text-decoration:none; color:#000000">        
                              {utf8_encode($value->data_inici)|date_format:"%D"} -  
                              {utf8_encode($value->tipus_sessio)} - 
                              {utf8_encode($value->tipus_document)}  
                              </a>	
                           </li>
                           {/if}
                           {/foreach}
                           <ul>
                        </div>
                     </div>
                     <div id="filaPlens">
                        <div class="colPlens ord-col1">
                           <h2>{$lblAbril}</h2>
                           <ul>
                              {foreach from=$elements key=key item=value}
                              {if ($value->currentMonth == 4)}
                              <li>
                                 <a href="http://www.tortosa.cat/webajt/gestiointerna/organs_govern_simple/documents/{utf8_encode($value->nom_arxiu)}" target="_blank"  style="text-decoration:none; color:#000000">        
                                 {utf8_encode($value->data_inici)|date_format:"%D"} -  
                                 {utf8_encode($value->tipus_sessio)} - 
                                 {utf8_encode($value->tipus_document)}  
                                 </a>	
                              </li>
                              {/if}
                              {/foreach}
                           </ul>
                        </div>
                        <div class="colPlens ord-col2">
                           <h2>{$lblMaig}</h2>
                           <ul>
                              {foreach from=$elements key=key item=value}
                              {if ($value->currentMonth == 5)}
                              <li>
                                 <a href="http://www.tortosa.cat/webajt/gestiointerna/organs_govern_simple/documents/{utf8_encode($value->nom_arxiu)}" target="_blank"  style="text-decoration:none; color:#000000">        
                                 {utf8_encode($value->data_inici)|date_format:"%D"} -  
                                 {utf8_encode($value->tipus_sessio)} - 
                                 {utf8_encode($value->tipus_document)}  
                                 </a>	
                              </li>
                              {/if}
                              {/foreach}
                           </ul>
                        </div>
                        <div class="colPlens ord-col3">
                           <h2>{$lblJuny}</h2>
                           <ul>
                              {foreach from=$elements key=key item=value}
                              {if ($value->currentMonth == 6)}
                              <li>
                                 <a href="http://www.tortosa.cat/webajt/gestiointerna/organs_govern_simple/documents/{utf8_encode($value->nom_arxiu)}" target="_blank"  style="text-decoration:none; color:#000000">        
                                 {utf8_encode($value->data_inici)|date_format:"%D"} -  
                                 {utf8_encode($value->tipus_sessio)} - 
                                 {utf8_encode($value->tipus_document)}  
                                 </a>	
                              </li>
                              {/if}
                              {/foreach}
                           </ul>
                        </div>
                     </div>
                     <div id="filaPlens">
                        <div class="colPlens ord-col1">
                           <h2>{$lblJuliol}</h2>
                           <ul>
                              {foreach from=$elements key=key item=value}
                              {if ($value->currentMonth == 7)}
                              <li>
                                 <a href="http://www.tortosa.cat/webajt/gestiointerna/organs_govern_simple/documents/{utf8_encode($value->nom_arxiu)}" target="_blank"  style="text-decoration:none; color:#000000">        
                                 {utf8_encode($value->data_inici)|date_format:"%D"} -  
                                 {utf8_encode($value->tipus_sessio)} - 
                                 {utf8_encode($value->tipus_document)}  
                                 </a>	
                              </li>
                              {/if}
                              {/foreach}
                           </ul>
                        </div>
                        <div class="colPlens ord-col2">
                           <h2>{$lblAgost}</h2>
                           <ul>
                              {foreach from=$elements key=key item=value}
                              {if ($value->currentMonth == 8)}
                              <li>
                                 <a href="http://www.tortosa.cat/webajt/gestiointerna/organs_govern_simple/documents/{utf8_encode($value->nom_arxiu)}" target="_blank"  style="text-decoration:none; color:#000000">        
                                 {utf8_encode($value->data_inici)|date_format:"%D"} -  
                                 {utf8_encode($value->tipus_sessio)} - 
                                 {utf8_encode($value->tipus_document)}  
                                 </a>	
                              </li>
                              {/if}
                              {/foreach}
                           </ul>
                        </div>
                        <div class="colPlens ord-col3">
                           <h2>{$lblSetembre}</h2>
                           <ul>
                              {foreach from=$elements key=key item=value}
                              {if ($value->currentMonth == 9)}
                              <li>
                                 <a href="http://www.tortosa.cat/webajt/gestiointerna/organs_govern_simple/documents/{utf8_encode($value->nom_arxiu)}" target="_blank"  style="text-decoration:none; color:#000000">        
                                 {utf8_encode($value->data_inici)|date_format:"%D"} -  
                                 {utf8_encode($value->tipus_sessio)} - 
                                 {utf8_encode($value->tipus_document)}  
                                 </a>	
                              </li>
                              {/if}
                              {/foreach}
                           </ul>
                        </div>
                     </div>
                     <div id="filaPlens">
                        <div class="colPlens ord-col1">
                           <h2>{$lblOctubre}</h2>
                           <ul>
                              {foreach from=$elements key=key item=value}
                              {if ($value->currentMonth == 10)}
                              <li>
                                 <a href="http://www.tortosa.cat/webajt/gestiointerna/organs_govern_simple/documents/{utf8_encode($value->nom_arxiu)}" target="_blank"  style="text-decoration:none; color:#000000">        
                                 {utf8_encode($value->data_inici)|date_format:"%D"} -  
                                 {utf8_encode($value->tipus_sessio)} - 
                                 {utf8_encode($value->tipus_document)}  
                                 </a>	
                              </li>
                              {/if}
                              {/foreach}
                           </ul>
                        </div>
                        <div class="colPlens ord-col2">
                           <h2>{$lblNovembre}</h2>
                           <ul>
                              {foreach from=$elements key=key item=value}
                              {if ($value->currentMonth == 11)}
                              <li>
                                 <a href="http://www.tortosa.cat/webajt/gestiointerna/organs_govern_simple/documents/{utf8_encode($value->nom_arxiu)}" target="_blank"  style="text-decoration:none; color:#000000">        
                                 {utf8_encode($value->data_inici)|date_format:"%D"} -  
                                 {utf8_encode($value->tipus_sessio)} - 
                                 {utf8_encode($value->tipus_document)}  
                                 </a>	
                              </li>
                              {/if}
                              {/foreach}
                           </ul>
                        </div>
                        <div class="colPlens ord-col3">
                           <h2>{$lblDesembre}</h2>
                           <ul>
                              {foreach from=$elements key=key item=value}
                              {if ($value->currentMonth == 12)}
                              <li>
                                 <a href="http://www.tortosa.cat/webajt/gestiointerna/organs_govern_simple/documents/{utf8_encode($value->nom_arxiu)}" target="_blank"  style="text-decoration:none; color:#000000">        
                                 {utf8_encode($value->data_inici)|date_format:"%D"} -  
                                 {utf8_encode($value->tipus_sessio)} - 
                                 {utf8_encode($value->tipus_document)}  
                                 </a>	
                              </li>
                              {/if}
                              {/foreach}
                           </ul>
                        </div>
                     </div>
                     {else} 
                     <div id="filaicorpo">
                        {$LABEL_TC7}
                     </div>
                     {/if}
                  </div>

               </div>

            </div>
            <br>
            <br>
            {$LABEL_TC6}<a href="mailto:{$lblEmailSec}">{$lblEmailSec}</a>
         </div>
      </div>
   </div>
</div>