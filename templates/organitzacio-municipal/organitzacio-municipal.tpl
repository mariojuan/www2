<html>
   <head>
      {include file="head.tpl"}
   </head>
   <body>
      {include file="header.tpl"}
      <div id="page-wrap">

         <div class="contenedor-responsive">

            <div id="conttranstotalcos">
               <div id="conttranscos">
                  <div id="btranspresen">

                     <div id="transimatge">
                        <img src="{$image}" class="img-slider-territori" />
                     </div>

                     <div id="transimatgeText">
                        <h1>
                           {$LABEL_TITOL1}
                        </h1>
                     </div>
                  </div>
                  <div id="tcentre_municipi">
                     <font size="4" color="#666666">{$LABEL_TC0}</font>
                     <p>{$LABEL_TC1}</p>
                     <p>{$LABEL_TC2}</p>
                     <p>{$LABEL_TC3} <a href="http://www2.tortosa.cat/organitzacio-municipal-entitats-municipals-descentralitzades/index.php?lang={$lang}" target="_blank">{$LABEL_MESINFO}</a></p>
                     <p>{$LABEL_TC4}</p>
                     <!-- <p>{$LABEL_TC5} <a href="" target="_blank">{$LABEL_TC6}</a></p> -->
                     <p>{$LABEL_TC7} <a href="http://www2.tortosa.cat/organitzacio-municipal-participacio-altres-ens?lang={$lang}" target="_blank">{$LABEL_MESINFO}</a></p>
                     <img src="http://www.tortosa.cat/webajt/ajunta/om/Estructura7.jpg" class="img-responsive"/>

                     <p>Items informatius</p>
                     <ul>
                        <li>
                           <a href="{$LABEL_CUADRE_PCIUTADANA}" target="_self" alt="Participaci&oacute; ciutadana">Participació ciutadana</a>
                        </li>
                        <li>
                           <a href="{$LABEL_CUADRE_ESTADISTICA}" target="_self" alt="Estad&iacute;stica d'habitants">Estadística d'habitants</a>
                        </li>
                        <li>
                           <a href="{$LABEL_CUADRE_ELECCIONS}" target="_blank" alt="Eleccions">Eleccions</a>
                        </li>
                        <li>
                           Grup municipal
                           <ul>
                              <li>
                                 <a href="{$LABEL_CUADRE_AJUNTAMENT}" target="_self">Ajuntament</a>
                              </li>
                              <li>
                                 Ens dependents
                                 <ul>
                                    <li>
                                       capital íntegrament municipal
                                       <ul>
                                          <li>
                                             <a href="http://www.tortosa.cat/webajt/ajunta/om/ens-dependents/tortosasport/fitxa-ens-dependent.pdf" target="_blank" alt="TortosaSport, SL">Tortosasport, SL</a>
                                          </li>
                                          <li>
                                             <a href="http://www.tortosa.cat/webajt/ajunta/om/ens-dependents/tortosa-media/fitxa-ens-dependent.pdf" target="_blank" alt="TortosaMedia, SL">Tortosamedia, SL</a>
                                          </li>
                                          <li>
                                             <a href="http://www.tortosa.cat/webajt/ajunta/om/ens-dependents/gumtsa/fitxa-ens-dependent.pdf" target="_blank" alt="GUMTSA">Gestió urbanística municipal de Tortosa, SA (GUMTSA)</a>
                                          </li>
                                          <li>
                                             <a  href="http://www.tortosa.cat/webajt/ajunta/om/ens-dependents/epel-hospital/fitxa-ens-dependent.pdf" target="_blank" alt="EPE Hospital i Llars de la Santa Creu">EPEL Hospital i Llars de la Santa Creu</a>
                                          </li>
                                          <li>
                                             <a href="http://www.tortosa.cat/webajt/ajunta/om/ens-dependents/gesat/fitxa-ens-dependent.pdf" target="_blank" alt="GESAT, SA">Gestió Sanitària Assistencial de Tortosa, SAM</a>
                                          </li>
                                          <li>
                                             <a href="http://www.tortosa.cat/webajt/ajunta/om/ens-dependents/tortosa-salut/fitxa-ens-dependent.pdf" target="_blank" alt="Tortosa Salut, SL">Tortosa Salut, SL</a>
                                          </li>
                                       </ul>
                                       <li>
                                          capital majoritariament municipal
                                          <ul>
                                             <li>
                                                <a href="http://www.tortosa.cat/webajt/ajunta/om/ens-dependents/emsp/fitxa-ens-dependent.pdf" target="_blank" alt="EMSP, SL">Empresa Municipal de Serveis Públics, SL</a>
                                             </li>
                                             <li>
                                                <a href="http://www.tortosa.cat/webajt/ajunta/om/ens-dependents/escorxador/fitxa-ens-dependent.pdf" target="_blank">Empresa Mixta Escorxador Tortosa, SL</a>
                                             </li>
                                          </ul>
                                       </li>
                                    </li>
                                 </ul>
                              </li>
                           </ul>
                        </li>
                        <li>
                           <a href="{$LABEL_CUADRE_DESCENTRALITZADES}" target="_self" alt="Entitats Municipals Descentralitzades de Jes&uacute;s">Entitats Municipals Descentralitzades</a>
                           <ul>
                              <li>
                                 <a href="http://www.bitem.altanet.org/" target="_blank" alt="EMD de B&iacute;tem">EMD de Bítem</a>
                              </li>
                              <li>
                                 <a href="http://www.campredo.cat/" target="_blank" alt="EMD de Campred&oacute;">EMD de Campredó</a>
                              </li>
                              <li>
                                 <a href="http://www.jesus.cat" target="_blank" alt="EMd de Jesús">EMD de Jesús</a>
                              </li>
                           </ul>
                        </li>
                        <li>
                           <a href="{$LABEL_CUADRE_ALTRES_ENS}" target="_self">Participació en altres ens</a>
                        </li>
                     </ul>
                  </div>
                  <div class="separator"></div>
               </div>
            </div>

         </div>

      </div>
      {include file="footer.tpl"}
   </body>
</html>