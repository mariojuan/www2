<html>
	<head>
        {include file="head.tpl"}
	</head>
	<body id="ordenances">
		{include file="header.tpl"}
		{if $accio=='detall'}
			{include file="ordenances/item_orire.tpl" image="/images/ordenances/tira.jpg"}
	    {else}
			{include file="ordenances/ordenances_custom_orire.tpl" image="/images/ordenances/tira.jpg"}
	    {/if}
		{include file="footer.tpl"}
	</body>
</html>