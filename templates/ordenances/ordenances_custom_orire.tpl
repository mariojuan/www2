<!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->

<div id="page-wrap">
<div id="contordenantotalcos">
	<div id="conttranscos">
		<div id="btranspresen">
			<div id="transimatge">			
			<img src={$image} />			
			</div>
			<div id="transimatgeText">
				<h1>
				{$LABEL_TITOL1}
				</h1>
			</div>
		</div>
		<ul id='menu_planA'>
		{foreach $MENU_ORIRE as $itemMenu}
			<a href={$itemMenu}&lang={$lang}>
			<li id='menu_planA_item'>
				{$itemMenu@key}
			</li>
			</a>
		{/foreach}
		</ul>
		
		<div id="tcentre_planA">
			<div id="textintroinfeco" class="taulaicorpo">	
				    {if $tipus!=NULL} 			    	
					    {if $tipus=='AL'}
					    	{$LABEL_TC6}
					    {else}
		                	{$LABEL_TC1ORIRE}
		                {/if}	
				    {else}
	                	{$LABEL_TC1ORIRE}
	                {/if}	
	                <br>
	                <br>
	                <br>


                   <div id="taulaicorpo">

	                {if $elements|count>0}
		               {foreach from=$elements key=key item=value}
		               <div id="filaicorpo">
			                <div class="colicorpo ord-col1">
			                    <a href="indexorire.php?tipus={$tipus}&lang={$lang}&accio=detall&codi={utf8_encode($value->Codi)}&page={$page}" style="text-decoration:none; color:#000000">
			                    	<i class="icon-link"></i>
			                    </a>
		   		            </div>
		   		            <div class="colicorpo ord-col2">	
		    	                <a href="http://www.tortosa.cat/webajt/gestiointerna/ordenances/documents/{utf8_encode($value->document)}" target="_blank"  style="text-decoration:none; color:#000000">
			                    	<i class="icon-file-pdf"></i>
				                </a>
		                    </div>
		                    <div class="colicorpo ord-col3">
			                    <a href="indexorire.php?tipus={$tipus}&lang={$lang}&accio=detall&codi={utf8_encode($value->Codi)}&page={$page}" style="text-decoration:none; color:#000000">
			                        {utf8_encode($value->Codi_ord1)} - {utf8_encode($value->codi_ord)}
			                    </a>
		                  	</div>                        
		                    <div class="colicorpo ord-col4">
			                    <a href="indexorire.php?tipus={$tipus}&lang={$lang}&accio=detall&codi={utf8_encode($value->Codi)}&page={$page}" style="text-decoration:none; color:#000000">
			                        {utf8_encode($value->descripcio_curta)}
			                    </a>
		                  	</div>                        
		                </div>
		                {/foreach}
	                {else} 
	                  <div id="filaicorpo">
	                    {$LABEL_TC7}
	                  </div>    
	                {/if}
	            	</div>
	        </div>

	        <br>

	        <div id="pagination">
	            <ul>
	                {if $lastpage>1}
	                    {if $page> 1}
	                        {assign var="page_ant" value=$page - 1}
	                        <li><a href="indexorire.php?tipus={$tipus}&lang={$lang}&accio=list&page={$page_ant}">{$lblAnt}</a></li>
	                    {/if}
	                    {assign var="desp" value=$page + 4}
	                    {assign var="firstpage" value=$page - 4}

	                    {if $desp < $lastpage}
	                        {assign var="lastpage" value=$page + 4}
	                    {/if}
	                    {if $firstpage < 1}
	                        {assign var="firstpage" value=1}
	                    {/if}

	                    {for $counter=$firstpage to $lastpage}
	                        {if $counter==$page}
	                            <li>{$counter}</li>
	                        {else}
	                            <li><a href="indexorire.php?tipus={$tipus}&lang={$lang}&accio=list&page={$counter}">{$counter}</a></li>
	                        {/if}
	                    {/for}
	                    {if $page < $lastpage}
	                        {assign var="page_seg" value=$page + 1}
	                        <li><a href="indexorire.php?tipus={$tipus}&lang={$lang}&accio=list&page={$page_seg}">{$lblSeg}</a></li>
	                    {/if}
	                {else}
	                  <li>1</li>	
	                {/if}
	            </ul>
	        </div>

		</div>
  	</div>
</div>
</div>


