<!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->

<div id="page-wrap">
<div id="conttranstotalcos">
    <div id="conttranscos">
        <div id="btranspresen">
            <div id="transimatge">          
            <img src={$image} />            
            </div>
            <div id="transimatgeText">
                <h1>
                {$LABEL_TITOL1}
                </h1>
            </div>
        </div>
        <ul id='menu_planA'>
        {foreach $MENU_ORIRE as $itemMenu}
            <a href={$itemMenu}&lang={$lang}>
            <li id='menu_planA_item'>
                {$itemMenu@key}
            </li>
            </a>
        {/foreach}
        </ul>
        
        <div id="tcentre_planA">
            <div id="textintroinfeco" class="memoria-democratica">                  
                <h2>{$LABEL_TITOL1ORIRE}</h2>

                <table id="item" name="item">
                    <tr>
                        <td>{$lblCodiOrdenansa}:</td>
                        <td>{utf8_encode($item->Codi_ord1)} - {utf8_encode($item->codi_ord)}</td>
                    </tr>
                    <tr>
                        <td>{$lblDescripcio}:</td>
                        <td>{utf8_encode($item->descripcio_curta)}</td>
                    </tr>
                    <tr>
                        <td>{$lblEnsGestor}:</td>
                        <td>{utf8_encode($item->servei_OA)}</td>
                    </tr>
                    <tr>
                        <td>{$lblDataEntradaVigor}:</td>
                        <td>{utf8_encode($item->data_entrada_vigor)}</td>
                    </tr>
                    <tr>
                        <td>{$lblOrganAprovacio}:</td>
                        <td>{utf8_encode($item->organ_aprovacio)}</td>
                    </tr>
                    <tr>
                        <td>{$lblDataAprovacio}:</td>
                        <td>{utf8_encode($item->data_versio_aprovacio)}</td>
                    </tr>
                    <tr>
                        <td>{$lblDataPublicacioAcordProv}:</td>
                        <td>{utf8_encode($item->data_publicacio_acord_prov)}</td>
                    </tr>
                    <tr>
                        <td>{$lblButlletiPublicacioAcordProv}:</td>
                        <td>{utf8_encode($item->butlleti_publicacio_acord_prov)}</td>
                    </tr>
                    <tr>
                        <td>{$lblDataPublicacioAcordDef}:</td>
                        <td>{utf8_encode($item->data_publicacio_acord_def)}</td>
                    </tr>
                    <tr>
                        <td>{$lblButlletiPublicacioAcordDef}:</td>
                        <td>{utf8_encode($item->butlleti_publicacio_acord_def)}</td>
                    </tr>
                </table>

                <div id="buttons-container">
                    <input type="button" name="enrere" id="enrere" value="{$lblBack}" onclick="JavaScript:window.location.href='/ordenances/indexorire.php?lang={$lang}&tipus={$tipus}&accio=list&page={$page}';">
                </div>


            </div>
        </div>
    </div>
</div>
</div>
