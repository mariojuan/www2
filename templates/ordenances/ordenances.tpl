<html>
	<head>
        {include file="head.tpl"}
	</head>
	<body id="ordenances">
		{include file="header.tpl"}
		{if $accio=='detall'}
			{include file="ordenances/item.tpl" image="/images/ordenances/tira.jpg"}
	    {else}
			{include file="ordenances/ordenances_custom.tpl" image="/images/ordenances/tira.jpg"}
	    {/if}
		{include file="footer.tpl"}
	</body>
</html>