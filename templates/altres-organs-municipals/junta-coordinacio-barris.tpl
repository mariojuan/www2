<html>
	<head>

        {include file="head.tpl"}

	</head>
	<body>

		{include file="header.tpl"}

		<div id="page-wrap">

        <div class="contenedor-responsive">

    <div id="conttranstotalcos">
        <div id="conttranscos">
            <div id="btranspresen">
                <div id="transimatge">
                    <img src="{$image}" class="img-slider-territori"/>
                </div>
                <div id="transimatgeText">
                    <h1>
                        {$LABEL_TITOL1}
                        <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                    </h1>
                </div>
            </div>

            <!-- Menu -->
                {include file="altres-organs-municipals/menu.tpl"}
            <!--// Menu -->

            <div id="tcentre_planA">
                <font size="4" color="#666666">{$LABEL_TC0}</font>
                <p>{$LABEL_TC2}</p>
                <p>{$LABEL_TC3}</p>
                <p>{$LABEL_T4}</p>
                <p>{$LABEL_TC5}</p>
                <p>{$LABEL_TC6}</p>
                <p>{$LABEL_TC7}</p>

                <!-- Decrets -->
                {foreach $DECRETS1 as $item}
                    <p><a href="{$item['link']}" target="{$item['target']}">{$item['name']}</a></p>
                {/foreach}

                <p>{$LABEL_TC9}</p>
                
                <ul>
                {foreach $COMPOSICIO1 as $item}
                    <li>{$item['occupation']} - {$item['name']}</li>
                {/foreach}
                </ul>

                <p  class="normativa"><strong>{$LABEL_NORMATIVA_E}</strong>{$LABEL_NORMATIVA}</p>
                
            </div>
        </div>
    </div>
    </div>
</div>

		{include file="footer.tpl"}

	</body>
</html>