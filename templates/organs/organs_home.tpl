<div id="page-wrap">

<div class="contenedor-responsive">

<div id="conttranstotalcos">
	<div id="conttranscos">
		<div id="btranspresen">
			<div id="transimatge">			
				<img src="{$image}" class="img-slider-territori">			
			</div>
			<div id="transimatgeText">
				<h1>
				{$LABEL_TITOL1}
				</h1>
			</div>
		</div>
		<div id="tcentre_municipi">
            <br>
            <p>{$LABEL_TC1_1} <a href="http://www.tortosa.cat/webajt/seuelectronica/OR/Reglaments/R12.pdf" target="_blank"><i class="icon-acrobat" style="font-size: 15px; border: 1px solid #ccc;"></i></a></p>
            <br>
			{$LABEL_TC1}
			<a href="{$url_alcalde}" style="text-decoration:none">
			<div class="caixa_organs">
				<div class="imatgeborgans">
					<img src="/images/organs/ialcalde.jpg">
				</div>
				{$LABEL_BT1}
			</div>
			</a>
			<div class="separator_organs"></div>
			<a href="{$url_ple}" style="text-decoration:none">
			<div class="caixa_organs">
				<div class="imatgeborgans">
					<img src="/images/organs/iple.jpg">
				</div>
				{$LABEL_BT2}
			</div>
			</a>
			<div class="separator_organs"></div>
			<a href="{$url_jgl}" style="text-decoration:none">
			<div class="caixa_organs">
				<div class="imatgeborgans">
					<img src="/images/organs/ijgl.jpg">
				</div>
				{$LABEL_BT3}
			</div>
			</a>
			<div class="separator_organs"></div>
			<a href="{$url_eg}" style="text-decoration:none">
			<div class="caixa_organs">
				<div class="imatgeborgans">
					<img src="/images/organs/iequipg.jpg">
				</div>
				{$LABEL_BT4}
			</div>
			</a>
			<div class="separator_organs"></div>
			<a href="{$url_altres}" style="text-decoration:none">
			<div class="caixa_organs">
				<div class="imatgeborgans">
					<img src="/images/organs/ialtres.jpg">
				</div>
				{$LABEL_BT5}
			</div>
			</a>
			<div class="separator_organs"></div>
			<a href="{$url_defensor}" style="text-decoration:none">
			<div class="caixa_organs">
				<div class="imatgeborgans">
					<img src="/images/organs/idefensor.jpg">
				</div>
				{$LABEL_BT6}
			</div>
			</a>
			<div class="separator_organs"></div>
			<br><br><br>
			{$LABEL_TC2}
			<a href="{$url_ep}" style="text-decoration:none">
			<div class="caixa_organs">
				<div class="imatgeborgans">
					<img src="/images/organs/iepublics.jpg">
				</div>
				{$LABEL_BT7}
			</div>
			</a>
		</div>
	
	</div>
</div>
</div>
</div>
