<div id="conttranstotalcos">
    {include file="templates/header_web_antiga.tpl"}
    <div id="conttranscos">
        {include file="templates/menu_web_antiga.tpl"}
        <div id="tcentre_planA">
            <table id="item" name="item">
                <tr>
                    <td>{$lblName}</td>
                    <td>{$item->NOM}</td>
                </tr>
                <tr>
                    <td>{$lblCause}</td>
                    <td>{$item->CAUSA}</td>
                </tr>
                <tr>
                    <td>{$lblPlace}</td>
                    <td>{$item->LLOC}</td>
                </tr>
                <tr>
                    <td>{$lblAge}</td>
                    <td>{$item->EDAT}</td>
                </tr>
                <tr>
                    <td>{$lblPlaceOfBirth}</td>
                    <td>{$item->LLOC_NAIXEMENT}</td>
                </tr>
                <tr>
                    <td>{$lblFiliation}</td>
                    <td>{$item->FILIACIO}</td>
                </tr>
                <tr>
                    <td>{$lblProfession}</td>
                    <td>{$item->Professio}</td>
                </tr>
                <tr>
                    <td>{$lblCivilStatus}</td>
                    <td>{$item->ESTAT_CIVIL}</td>
                </tr>
                <tr>
                    <td>{$lblCivilRegistration}</td>
                    <td>{$item->REG_CIVIL}</td>
                </tr>
                <tr>
                    <td>{$lblOrigin}</td>
                    <td>{$item->ORIGEN_INFORMACIO}</td>
                </tr>
                <tr>
                    <td>{$lblObservations}</td>
                    <td>{$item->OBSERVACIONS}</td>
                </tr>
            </table>
            <div id="button-container">
                <input type="button" name="enrere" id="enrere" value="{$lblBack}" onclick="JavaScript:window.location.href='memoria-democratica.php?accio=list&page={$page}';">
            </div>
        </div>
    </div>
    {include file="templates/footer_web_antiga.tpl"}
</div>