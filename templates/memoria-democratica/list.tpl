<div id="conttranstotalcos">
    {include file="templates/header_web_antiga.tpl"}
    <div id="conttranscos">
        {include file="templates/menu_web_antiga.tpl"}
        <div id="tcentre_planA">
            <table name="list-items" id="list-items">
                <tr>
                    <th>{$lblName}</th>
                    <th>{$lblCategory}</th>
                    <th>{$lblSeeItem}</th>
                </tr>
                {foreach $items as $item}
                    <tr>
                        <td>{$item->NOM}</td>
                        <td>{$item->CATEGORIA}</td>
                        <td class="link_item"><a href="memoria-democratica.php?accio=file&id={$item->ID}&page={$page}">[+]</a></td>
                    </tr>
                {/foreach}
            </table>
            <div id="pagination">
                <ul>
                    {if $lastpage>1}
                        {if $page> 1}
                            {assign var="page_ant" value=$page - 1}
                            <li><a href="memoria-democratica.php?accio=list&page={$page_ant}">{$lblAnt}</a></li>
                        {/if}
                        {assign var="desp" value=$page + 4}
                        {assign var="firstpage" value=$page - 4}

                        {if $desp < $lastpage}
                            {assign var="lastpage" value=$page + 4}
                        {/if}
                        {if $firstpage < 1}
                            {assign var="firstpage" value=1}
                        {/if}

                        {for $counter=$firstpage to $lastpage}
                            {if $counter==$page}
                                <li>{$counter}</li>
                            {else}
                                <li><a href="memoria-democratica.php?accio=list&page={$counter}">{$counter}</a></li>
                            {/if}
                        {/for}
                        {if $page < $lastpage}
                            {assign var="page_seg" value=$page + 1}
                            <li><a href="memoria-democratica.php?accio=list&page={$page_seg}">{$lblSeg}</a></li>
                        {/if}
                    {/if}
                </ul>
            </div>
        </div>
    </div>
    {include file="templates/footer_web_antiga.tpl"}
</div>