<div id="conttranstotalcos">
    {include file="templates/header_web_antiga.tpl"}
    <div id="conttranscos">
        {include file="templates/menu_web_antiga.tpl"}
        <div id="tcentre_planA" class="home">
            <p id="title">
                Base de dades de persones mortes durant la guerra civil
            </p>
            <p>
                L’Àrea de Serveis Centrals de l’Ajuntament de Tortosa va comptar amb una persona tècnica dedicada a la recopilació de dades sobre els morts a la ciutat durant la Guerra Civil de 1936-39 en el marc del compliment de la llei de memòria històrica. Aquestes dades són les que es posen a l’abast dels ciutadans a través d’aquest espai de consulta.
            </p>
            <p>
            Les dades són extretes de diverses fonts, principalment:
            <ul>
                <li>El Llibre registre d’enterraments del cementiri municipal</li>
                <li>El Registre Civil dels jutjats de Tortosa</li>
                <li>La diversa bibliografia publicada al respecte</li>
            </ul>
            </p>
            <p>
            La base de dades es pot interrogar per dos camps: pel nom dels difunts o per la causa de la seva mort. Per aquest concepte s’han creat diverses categories:
            <ul>
                <li>assassinats polítics entre el 1936 i el 1939, òbviament gairebé tots comesos per escamots incontrolats de la part republicana (566 casos documentats);</li>
                <li>morts per bombardejos, extrets del Llibre registre municipal d’enterraments (197);</li>
                <li>refugiats, que es refereix a gent de pas per la ciutat i recollits com a refugiats al Llibre registre municipal d’enterraments, tot i que la seva mort no té perquè haver estat violenta (118);</li>
                <li>morts al front, que només recull una part minsa del total, simplement hi són aquells que apareixen com a soldats difunts inscrits al Registre Civil de Tortosa (261);</li>
                <li>represaliats pel règim franquista, gairebé tots un cop acabada la guerra (392);</li>
                <li>trasllats al Valle de los Caídos (12);</li>
                <li>morts a les presons del règim (21).</li>
            </ul>
            <p>
                El resultat obtingut en la interrogació de la base de dades és un primer llistat d’entrades coincidents amb els criteris de cerca, cada una de les quals es pot ampliar clicant sobre l’ítem, on trobarem totes les dades individuals que se n’hagin pogut esbrinar de la persona seleccionada.
            </p>
        </div>
    </div>
    {include file="templates/footer_web_antiga.tpl"}
</div>