<title>{$title_page}</title>
<meta name="description" content="{$meta_description}">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1.0">

<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@tortosa" />
<meta name="twitter:title" content="{$title_page}" />
<meta name="twitter:description" content="{$meta_description}" />
<meta name="twitter:image" content="https://pbs.twimg.com/profile_images/1152077486/logo_twitter_400x400.jpg" />

<!-- Open Graph data -->
<meta property="og:title" content="{$title_page}" />
<meta property="og:url" content="{$url}" />
<meta property="og:image" content="https://www2.tortosa.cat/logos/logo-vertical.jpg" />
<meta property="og:description" content="{$meta_description}" />
<meta property="og:site_name" content="{$site_name}" />

<!-- Styles Libraries -->
<link rel="shortcut icon" href="/ajt.ico">
{if $test==1}
    <!-- Estils en desenvolupament -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <link href="/css/estils_v2_mjm.css" rel="stylesheet" type="text/css">
{else}
    <link href="/css/estils_v2.css" rel="stylesheet" type="text/css">
    <link href="/css/estils_yves.css" rel="stylesheet" type="text/css">
    <link href="/css/estils.css" rel="stylesheet" type="text/css">
    <link href="/css/responsive.css" rel="stylesheet" type="text/css">
{/if}
<link rel='stylesheet' href='/fonts/fontello-fafedd5f/css/search.css'>
<link rel='stylesheet' href='/fonts/fontello-fafedd5f/css/animation.css'>
<link rel="stylesheet" href="/admin/css/jquery-ui.css">
<link rel="stylesheet" href="/css/jquery.bxslider.css"/>

<!-- JS Libraries -->
<script src="/js/google_analytics.js"></script>
<script src="/admin/js/jquery-1.11.1.js"></script>
<script src="/js/jquery.bxslider.min.js"></script>
<script type='text/javascript' src='/js/datanova.js' language="javascript"></script>

<script src="/admin/js/jquery-ui.js" type="text/javascript"></script>
<script src="/admin/js/jquery.validate.js" type="text/javascript"></script>

{if $apartat=="eleccions"}
    <SCRIPT LANGUAGE="Javascript" SRC="/chart/js/FusionCharts.js"></SCRIPT>
{/if}

{if $apartat=="consulta-ciutadania"}
    <script type="text/javascript" src="/js/consulta-ciutadana.js"></SCRIPT>
{/if}

<script type="text/javascript" src="/chart/js/FusionCharts.js"></script>
<script>
    $( document ).ready(function() {
        {if $apartat=='home' || $apartat=='ciutat'}
        $('.bxslider').bxSlider({
            auto: true
        });
        {/if}
        /*
        $( "#eleccions" ).change(function() {
            window.location.href = 'eleccions.php?accio=participacio_totals&eleccions='+( $( "#eleccions" ).val() );
        });
        */
        $(window).click(function() {
            //Hide the menus if visible
            $("#widget_translate").css("display", "none");
        });
        $("#button_translate").click(function(event) {
            event.stopPropagation();
            $("#widget_translate").css("display", "block");
        });
        $("#google_translate_element").click(function(event) {
            event.stopPropagation();
        });

    });
    function PrintElem(elem, web) {
        $('#logos').css('display','block');
        $('#printer').css('display','none');
        switch (web) {
            case "participacio_totals":
                $('#chart-1').css('display','none');
                $('#chart-2').css('display','none');
                break;
            default:
                break;
        }
        Popup(jQuery(elem).html());
    }

    function Popup(data) {
        var mywindow = window.open('', 'my div', 'height=400,width=600');
        mywindow.document.write('<html><head><title></title>');
        mywindow.document.write('<link rel="stylesheet" href="{$path_eleccions}/style.css" type="text/css" type="text/css" />');
        mywindow.document.write('<style type="text/css">.test { color:red; } </style></head><body>');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');
        mywindow.document.close();
        mywindow.print();
    }

    function displayMenu() {
        $( "#menu_planA" ).slideToggle( "slow", function() {
        
        });
    }

</script>

