<!-- PLANTILLA COM LA "F" PERÒ SENSE MENÚ A L'ESQUERRA, NOMÉS UN IFRAME ASP -->

<div id="conttranstotalcos">
	<div id="conttranscos">
		<div id="btranspresen">
			<div id="transimatge">			
				<img src={$image} />			
			</div>
			<div id="transimatgeText">
				<h1>
				{$LABEL_TITOL1}
				</h1>
			</div>
		</div>
		
			{if ($ntpl==11)}
				<div id="tcentre_planFU">
				<iframe src="http://www.tortosa.cat/webajt/ciutat/findex.asp" width="760" height="580" scrolling="no" frameborder="0" marginheight="0" marginwidth="0">
				</iframe>
				</div>
			{else if ($ntpl==12)}
				<div id="tcentre_planFU">
				<iframe src="http://www.tortosa.cat/webajt/ajunta/viapublica/findex.asp" width="840" height="1750" scrolling="no" frameborder="0" marginheight="0" marginwidth="0">
				</iframe>
				</div>
			{else if ($ntpl==121)}
				<div id="tcentre_planFU">
					<div id="textintroivp">{$LABEL_TC1}</diV>
				</div>
				<div id="taulaivp">
    				<div id="filaivp">
        				<div id="colivp1" class="colivp"><strong>{$LABEL_TC11}</strong><br><br>900 131 326 (24h)</div>
        				<div id="colivp2" class="colivp"><strong>{$LABEL_TC12}</strong><br><br>092 / 977 585 801 (24h)</div>
        				<div id="colivp3" class="colivp"><strong>{$LABEL_TC13}</strong><br><br>900 194 998 (24h)</div>
    				</div>
    				<div id="filaivp">
        				<div id="colivp4" class="colivp"><strong>{$LABEL_TC14}</strong><br><br>600 927 902 (24h)</div>
        				<div id="colivp5" class="colivp"><strong>{$LABEL_TC15}</strong><br><br>977585875<br>662078632</div>
        				<div id="colivp6" class="colivp">
        					{$LABEL_TC16a}<br><br>
        					<a href="/iviapublica/formivp.php">
        						<i class='icon-doc-text'></i>
        						<i class='icon-pencil'></i>
        						<br>
        						<strong>{$LABEL_TC16}</strong>
        					</a>
        				</div>
    				</div>
				</div>
				<br><br>
			{else if ($ntpl==13)}
				<div id="tcentre_planFU">
				<iframe src="http://www.tortosa.cat/webajt/wn/correus.asp" width="840" height="2120" scrolling="no" frameborder="0" marginheight="0" marginwidth="0">
				</iframe>
				</div>
			{else if ($ntpl==14)}
				<div id="pam_intro">
					{$LABEL_TC1}
				</div>
				<div id="pam_titolA">
					&nbsp;{$LABEL_TC2}
				</div>
				<div id="pam_index">
				<ul>
					<li>
						{$LABEL_TC21}
						<ul>
							<li>
								{$LABEL_TC211}
								<ul>
									<li><a href="http://www.tortosa.cat/webajt/ajunta/planificacio/AcordG1519.pdf">{$LABEL_TC2111}</a></li>
									<li><a href="http://www.tortosa.cat/webajt/gestiointerna/headlines/partpublica/indexllistaheadlines.asp?codi=5563">{$LABEL_TC2112}</a></li>
									<li><a href="http://www.tortosa.cat/webajt/gestiointerna/headlines/partpublica/indexllistaheadlines.asp?codi=5561">{$LABEL_TC2113}</a></li>
								</ul>
							</li>
							<li>
								{$LABEL_TC212}
								<ul>
									<li><a href="http://www.tortosa.cat/webajt/PAM20152019.pdf">{$LABEL_TC2121}</a></li>
									<li><a href="http://www.tortosa.cat/webajt/PAM11-15V15.PDF">{$LABEL_TC2122}</a></li>
								</ul>
							</li>
						</ul>
					</li>
					<li>
						{$LABEL_TC22}
						<ul>
							<li>
								{$LABEL_TC221}
								<ul>
									<li><a href="http://www.tortosa.cat/webajt/PAM11-15V15.PDF">{$LABEL_TC2211}</a></li>
									<li><a href="http://www.tortosa.cat/webajt/PAM11-15V14.PDF">{$LABEL_TC2212}</a></li>
									<li><a href="http://www.tortosa.cat/webajt/PAM11-15V13.PDF">{$LABEL_TC2213}</a></li>
									<li><a href="http://www.tortosa.cat/webajt/PAM11-15V12.PDF">{$LABEL_TC2214}</a></li>
								</ul>
							</li>
						</ul>
					</li>
				</ul>
				</div>
				<br>
				<div id="pam_titolA">
					&nbsp;{$LABEL_TC3}
				</div>
				<div id="pam_index">
				<ul>
					<li>
						{$LABEL_TC31}
						<ul>
							<li><a href="http://www.tortosa.cat/webajt/poum3/index.asp">{$LABEL_TC311}</a></li>
						</ul>
					</li>
					<li>
						{$LABEL_TC32}
						<ul>
							<li><a href="http://www2.tortosa.cat/infeco/infeco_iplanif.php?lang=ca">{$LABEL_TC321}</a></li>
						</ul>
					</li>
				</ul>
				<br>
				</div>
			{else if ($ntpl==15)}
        		<div id="colivp0"><strong>{$LABEL_TC1}</strong><br><br><font size="6">112</font></div>
				<div id="taulaivp">
    				<div id="filaivp">
        				<div id="colivp1" class="colivp"><strong>{$LABEL_TC2}</strong><br><br><font size="6">092</font></div>
        				<div id="colivp2" class="colivp"><strong>{$LABEL_TC3}</strong><br><br><font size="6">091</font></font></div>
        				<div id="colivp3" class="colivp"><strong>{$LABEL_TC4}</strong><br><br><font size="6">088</font></div>
    				</div>
    				<div id="filaivp">
        				<div id="colivp4" class="colivp"><strong>{$LABEL_TC5}</strong><br><br><font size="6">085</font></div>
        				<div id="colivp5" class="colivp"><strong>{$LABEL_TC6}</strong><br><br><font size="6">061</font></div>
        				<div id="colivp6" class="colivp"><a href="http://www.coft.org/guardies/" target="_blank"><i class='icon-link-ext'></i><br><strong>{$LABEL_TC7}</strong></a><br><br><font size="6">012</font></div>
        				</a>
    				</div>
				</div>
				<br><br>
			{/if}
  	</div>
</div>


