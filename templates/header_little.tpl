<!-- BARRA CAPÇALERA -->
<div class="row row-header">

   <div id="contenidortotalcap1">
      <div id="contenidorcap">
         <div id="contenidorcap1">
            <div id="imatgecontenidorcap1">
               <img src="/images/escutcap1R2.jpg" title="Ajuntament de Tortosa" alt="Ajuntament de Tortosa"/>
            </div>
         </div>
         <div id="contenidorcap_text">
             <div class="nomcap">
                {$LABEL_COP}
             </div>
             <div class="idiomescap">
                 <ul id="idiomescapul">
                     {if $lang=="ca"}
                         {assign var="active_lang_ca" value="class='active'"}
                     {elseif $lang=="es"}
                         {assign var="active_lang_es" value="class='active'"}
                     {elseif $lang=="en"}
                         {assign var="active_lang_en" value="class='active'"}
                     {elseif $lang=="fr"}
                         {assign var="active_lang_fr" value="class='active'"}
                     {/if}
                     {if $url_ca==""}
                         <li><a href='/' hreflang="ca" {$active_lang_ca}>CAT</a></li>
                     {else}
                         <li><a href='{$url_ca}' hreflang="ca" {$active_lang_ca}>CAT</a></li>
                     {/if}
                     {if $url_es==""}
                         <!--
                  ::
                  <li><a href='{$url_ca}' hreflang="es" {$active_lang_es}>ESP</a></li>
                  -->
                     {else}
                         ::
                         <li><a href='{$url_es}' hreflang="es" {$active_lang_es}>ESP</a></li>
                     {/if}
                     {if $url_en==""}
                         <!--
                  ::
                  <li><a href='{$url_ca}' hreflang="en" {$active_lang_en}>ENG</a></li>
                  -->
                     {else}
                         ::
                         <li><a href='{$url_en}' hreflang="en" {$active_lang_en}>ENG</a></li>
                     {/if}
                     {if $url_fr==""}
                         <!--
                  ::
                  <li><a href='{$url_ca}' hreflang="fr" {$active_lang_fr}>FRA</a></li>
                  -->
                     {else}
                         ::
                         <li><a href='{$url_fr}' hreflang="fr" {$active_lang_fr}>FRA</a></li>
                     {/if}
                 </ul>
             </div>

             <div class="divIdiomaGoogle">
                 <div id="button_translate">{$LABEL_TRADUCCIO}</div>
                 <div id="widget_translate">
                     <div id="google_translate_element" style="display: inline-block; top: -10px; position: relative;"></div>
                     {literal}
                         <script type="text/javascript">
                             function googleTranslateElementInit() {
                                 new google.translate.TranslateElement({pageLanguage: 'ca', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, multilanguagePage: true, gaTrack: true, gaId: 'UA-23454522-1'}, 'google_translate_element');
                             }
                         </script>
                         <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
                     {/literal}
                 </div>
             </div>
         </div>
      </div>
   </div>
</div>