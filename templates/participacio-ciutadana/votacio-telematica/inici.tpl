<html>
<head>
    {include file="head.tpl"}
</head>

<body>

<body>
    {include file="header.tpl"}
    <div id="page-wrap">
        <div class="contenedor-responsive">
            <div id="conttranstotalcos">
                <div id="conttranscos">
                    <div id="btranspresen">
                        <div id="transimatge">
                            <img src="/images/pciutadana/tira.jpg" class="img-slider-territori"/>
                        </div>
                        <div id="transimatgeText">
                            <h1>
                                {$TITLE_1}
                            </h1>
                        </div>

                        <div class="subtitolinfeco subtitol_generic" id="subtitolinfeco2">
                            {$TITLE_3}
                            <a href="{$URL_PPART_BACK}">
                                <i class="icon-angle-double-up"></i>
                            </a>
                        </div>

                        <div class="formulari_generic">
                        <form id="pre_votacio_telematica_form" method="post" action="ident.php?lang={$lang}" novalidate="novalidate" enctype="multipart/form-data">
                            <input type="hidden" name="FirstTime" id="FirstTime" value="1">

                            <p>
                                {$TEXT_1}
                            </p>
                            <!--
                            <p>
                                {$TEXT_1_1}
                                <a href="http://www2.tortosa.cat/inscripcio-participacio/">{$AQUI}</a>
                                {$PUNT} 
                            </p>

                            <p>
                                {$TEXT_2} 
                            </p>
                            <p>

                                        - {$TEXT_3} 
                                        <a href="https://www2.tortosa.cat/participacio-ciutadana/votacio-telematica/propostes-pressupostos-participatius-2018.pdf" target="_blank">{$AQUI}</a>
                                        {$TEXT_4}
                            </p>
-->                            
                            <p>

                                        {if $en_termini} 
                                            <input type="submit" name="enviar" id="enviar" value="{$TEXT_5}">
                                        {else}
                                            <p>
                                                {$TEXT_6}
                                            </p>
                                        {/if}

                                        <!--
                                        {if $en_termini} 
                                            <a href="http://www2.tortosa.cat/participacio-ciutadana/votacio-telematica/ident.php">
                                        {/if}

                                        {$TEXT_5}

                                        {if $en_termini} 
                                            </a>
                                        {/if}
                                        -->

                            </p>
                        </form>

                            <!--<p>&nbsp;</p>-->
                        
                            <!--
                            <p class="title">
                                {$TITLE_LIST} 
                            </p>

                            <ul>
                                <li>
                                    {if $en_termini} 
                                        <a href="http://www2.tortosa.cat/participacio-ciutadana/votacio-telematica/ident.php">
                                    {/if}

                                    {$LIST_ELEMENT1}

                                    {if $en_termini} 
                                        </a>
                                    {/if}

                                </li>
                            </ul>
                            -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {include file="footer.tpl"}
</body>
</html>