<html>
<head>
    {include file="head.tpl"}
</head>

<body>

<body>
    {include file="header.tpl"}
    <div id="page-wrap">
        <div class="contenedor-responsive">
            <div id="conttranstotalcos">
                <div id="conttranscos">
                    <div id="btranspresen">
                        <div id="transimatge">
                            <img src="/images/pciutadana/tira.jpg" class="img-slider-territori"/>
                        </div>
                        <div id="transimatgeText">
                            <h1>
                                {$TITLE_1}
                            </h1>
                        </div>

                        <div class="subtitolinfeco subtitol_generic" id="subtitolinfeco2">
                            {$TITLE_3}
                            <a href="{$URL_PPART_BACK}">
                                <i class="icon-angle-double-up"></i>
                            </a>
                        </div>

                        <div class="formulari_generic">
                            <p>
                                {$TEXT_6} 
                            </p>
                            <p>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {include file="footer.tpl"}
</body>
</html>