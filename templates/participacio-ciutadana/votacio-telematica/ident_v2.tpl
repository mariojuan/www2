<html>
<head>
    {include file="head.tpl"}
    <script src='https://www.google.com/recaptcha/api.js?hl={$lang}'></script>
    <script type="text/javascript">
        var cont_propostes = 0;
        var form_validate     = false;
        var confirm_propostes = false;
        var captcha_value     = false;
        var RecaptchaOptions = {
            lang : 'ca'
        };
        $(document).ready(function() {
            $(".close").click(function() {
                tancapopup();
            });
            $("#popup_cancel").click(function() {
                tancapopup();
            });
            $("#popup_accept").click(function() {
                confirm_propostes = true;
                tancapopup();
                $("#votacio_telematica_form").submit();

            });
            $("input[name='propostes[]']").click(function() {
                if($(this).is(':checked')) {
                    if(cont_propostes<3) {
                        cont_propostes++;
                    }
                    else {
                        $(this).attr('checked', false);
                    }
                }
                else {
                    if(cont_propostes>0)
                        cont_propostes--;
                }
            });
            $(".proposta_container h2").click(function() {
                if($(this).parent().children(".list_propostes_container").css('display')=='block')
                    $(this).parent().children(".list_propostes_container").hide( "slow" );
                else
                    $(this).parent().children(".list_propostes_container").show( "slow" );
            });
            $("#votacio_telematica_form").validate({
                rules: {
                    tdocument:  {
                        required: true
                    },
                    pin: {
                        required: true
                    },
                    proposta1: {
                        required: false,
                        number: true
                    },
                    proposta2: {
                        required: false,
                        number: true
                    },
                    proposta3: {
                        required: false,
                        number: true
                    }
                },
                messages: {
                    tdocument:  {
                        required: "{$LABEL_ERROR_FIELD}"
                    },
                    pin: {
                        required: "{$LABEL_ERROR_FIELD}"
                    },
                    proposta1: {
                        number: "{$LABEL_ERROR_FIELD_INT}"
                    },
                    proposta2: {
                        number: "{$LABEL_ERROR_FIELD_INT}"
                    },
                    proposta3: {
                        number: "{$LABEL_ERROR_FIELD_INT}"
                    }

                },
                submitHandler: function(form) {
                    if(form_validate && confirm_propostes) {
                        form.submit();
                    }
                    else {
                        captcha();
                        if (captcha_value) {
                            form_validate = true;
                            if (cont_propostes == 0) {
                                obripopup();
                            }
                            else {
                                form.submit();
                            }
                        }
                    }
                }
            });
            jQuery.validator.addMethod("lettersonly", function(value, element) {
                return this.optional(element) || /^[a-z]+$/i.test(value);
            }, "Letters only please");
            $('#tdocument').change(function() {
                switch($( "#tdocument" ).val()){
                    case "1":
                        buida_camps();                    
                        $('.option0').css('display', 'none');
                        $( "#tdocument-error" ).css('display', 'none');
                        $('.option1').css('display', 'block');
                        $('.option2').css('display', 'none');
                        $('.option3').css('display', 'none');
                        $('#dni_number').rules("add",
                        {
                            required: true,
                            number: true,
                            maxlength: 8,
                            messages: {
                                required: "{$LABEL_ERROR_FIELD}",
                                number: "{$LABEL_FIELD_2_3}"
                            }
                        });
                        $('#dni_letter').rules("add",
                        {
                            required: true,
                            lettersonly: true,
                            maxlength: 1,
                            messages: {
                                required: "{$LABEL_ERROR_FIELD}",
                                lettersonly: "{$LABEL_FIELD_2_4}"
                            }
                        });
                    break;
                    case "2":
                        buida_camps();                    
                        $( "#tdocument-error" ).css('display', 'none');
                        $('.option1').css('display', 'none');
                        $('.option2').css('display', 'block');
                        $('.option3').css('display', 'none');
                        $('#option2_passport_number').rules("add",
                        {
                            required: true,
                            messages: {
                                required: "{$LABEL_ERROR_FIELD}"
                            }
                        });
                    break;
                    case "3":
                        buida_camps();                    
                        $( "#tdocument-error" ).css('display', 'none');
                        $('.option1').css('display', 'none');
                        $('.option2').css('display', 'none');
                        $('.option3').css('display', 'block');
                        $('#tr_letter_1').rules("add",
                        {
                            required: true,
                            messages: {
                                required: "{$LABEL_ERROR_FIELD}"
                            }
                        });
                        $('#tr_number').rules("add",
                        {
                            required: true,
                            messages: {
                                required: "{$LABEL_ERROR_FIELD}"
                            }
                        });
                        $('#tr_letter_2').rules("add",
                        {
                            required: true,
                            messages: {
                                required: "{$LABEL_ERROR_FIELD}"
                            }
                        });
                    break;
                    default:
                        buida_camps();                    
                        $('.option1').css('display', 'none');
                        $('.option2').css('display', 'none');
                        $('.option3').css('display', 'none');
                    break;
                }
            });
        });

        function captcha() {
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    var out = this.responseText;
                    if(out=='1') {
                        captcha_value = true;
                    }
                    else {
                        $('#recaptcha-error').css('display', 'block');
                    }
                }
            };
            xmlhttp.open("POST", "https://www2.tortosa.cat/libs/verify-recaptcha.php", false);
            xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            xmlhttp.send("g-recaptcha-response="+grecaptcha.getResponse());
        }
        function buida_camps() {
            $('#dni_number').val('');
            $('#dni_letter').val('');
            $('#option2_passport_number').val('');
            $('#tr_letter_1').val('');
            $('#tr_number').val('');
            $('#tr_letter_2').val('');
        }
        function obripopup() {
            var modal = $('#formulari_generic_modal');
            modal.css("display", "block");
        }
        function tancapopup() {
            var modal = $('#formulari_generic_modal');
            modal.css("display", "none");
        }
    </script>
</head>

<body>
    {include file="header.tpl"}
    <div id="page-wrap">
        <div class="contenedor-responsive">
            <div id="conttranstotalcos">
                <div id="conttranscos">
                    <div id="btranspresen">
                        <div id="transimatge">
                            <img src="/images/pciutadana/tira.jpg" class="img-slider-territori"/>
                        </div>
                        <div id="transimatgeText">
                            <h1>
                                {$TITLE_1}
                            </h1>                            
                        </div>
                    </div>

                    <div class="subtitolinfeco subtitol_generic" id="subtitolinfeco2">
                        {$TITLE_3}
                        <a href="{$URL_PPART_BACK}">
                            <i class="icon-angle-double-up"></i>
                        </a>
                    </div>

                    <div class="formulari_generic">
                        {if ($message=="OK!!")}
                            <p>{$VOTACIO_EFECTUADA}</p>
                            <p>{$GRACIES_PARTICIPAR}</p>                            
                        {else}
                            <div class="general_form_error">
                            {if ($message=="PIN_INCORRECTE")}
                                <p>{$PIN_INCORRECTE}</p>
                            {elseif ($message=="NO_VALIDAD")}
                                <p>{$NO_VALIDAD}</p>
                            {elseif ($message=="JA_HA_VOTAT")}
                                <p>{$JA_HA_VOTAT}</p>
                            {elseif ($message=="JA_HA_VOTAT_PRESENCIAL")}
                                <p>{$JA_HA_VOTAT_PRESENCIAL}</p>
                            {elseif ($message=="usuari_no_registrat")}
                                <p>{$usuari_no_registrat}</p>
                            {elseif ($message=="probrema_dades_registre")}
                                <p>{$probrema_dades_registre}</p>
                            {elseif ($message=="NO_HI_HA_PROPOSTA")}
                                <p>{$NO_HI_HA_PROPOSTA}</p>
                            {elseif ($message=="MATEIXA_PROPOSTA")}
                                <p>{$MATEIXA_PROPOSTA}</p>
                            {/if}
                            </div>
                                <h2 style="text-align: center">{$TITLE_6}</h2>
                                <p>
                                    {$TEXT_1} 
                                </p>
                                <!--<p>&nbsp;</p>-->

                                <form id="votacio_telematica_form" method="post" action="ident.php?lang={$lang}&test" novalidate="novalidate" enctype="multipart/form-data">
                                    <p class="title">{$TITLE_4}</p>
                                    <h2 style="font-size: 18px;">TORTOSA</h2>
                                    {if $propostes_accessibilitat|COUNT>0}
                                        <div class="proposta_container">
                                            <h2>{$TITLE_PROPOSTA_1}<i class="icon-angle-down"></i></h2>
                                            <div class="list_propostes_container">
                                            {foreach $propostes_accessibilitat as $item}
                                            <p>
                                                <input type="checkbox" name="propostes[]" value="{$item->NUM_PROPOSTA}" id=""> <span class="num_proposta">{$item->NUM_PROPOSTA}</span> <label>{$item->DESC_CURTA} ({$item->IMPORT|number_format:0:",":"."}€)</label>
                                            </p>
                                            {/foreach}
                                            </div>
                                        </div>
                                    {/if}

                                    {if $propostes_camins|COUNT>0}
                                        <div class="proposta_container">
                                            <h2>{$TITLE_PROPOSTA_2}<i class="icon-angle-down"></i></h2>
                                            <div class="list_propostes_container">
                                                {foreach $propostes_camins as $item}
                                                    <p>
                                                        <input type="checkbox" name="propostes[]" value="{$item->NUM_PROPOSTA}" id=""> <span class="num_proposta">{$item->NUM_PROPOSTA}</span> <label>{$item->DESC_CURTA} ({$item->IMPORT|number_format:0:",":"."}€)</label>
                                                    </p>
                                                {/foreach}
                                            </div>
                                        </div>
                                    {/if}

                                    {if $propostes_carrers|COUNT>0}
                                        <div class="proposta_container">
                                            <h2>{$TITLE_PROPOSTA_3}<i class="icon-angle-down"></i></h2>
                                            <div class="list_propostes_container">
                                                {foreach $propostes_carrers as $item}
                                                    <p>
                                                        <input type="checkbox" name="propostes[]" value="{$item->NUM_PROPOSTA}" id=""> <span class="num_proposta">{$item->NUM_PROPOSTA}</span> <label>{$item->DESC_CURTA} ({$item->IMPORT|number_format:0:",":"."}€)</label>
                                                    </p>
                                                {/foreach}
                                            </div>
                                        </div>
                                    {/if}

                                    {if $propostes_enllumenat|COUNT>0}
                                        <div class="proposta_container">
                                            <h2>{$TITLE_PROPOSTA_4}<i class="icon-angle-down"></i></h2>
                                            <div class="list_propostes_container">
                                                {foreach $propostes_enllumenat as $item}
                                                    <p>
                                                        <input type="checkbox" name="propostes[]" value="{$item->NUM_PROPOSTA}" id=""> <span class="num_proposta">{$item->NUM_PROPOSTA}</span> <label>{$item->DESC_CURTA} ({$item->IMPORT|number_format:0:",":"."}€)</label>
                                                    </p>
                                                {/foreach}
                                            </div>
                                        </div>
                                    {/if}

                                    {if $propostes_equipaments|COUNT>0}
                                        <div class="proposta_container">
                                            <h2>{$TITLE_PROPOSTA_5}<i class="icon-angle-down"></i></h2>
                                            <div class="list_propostes_container">
                                                {foreach $propostes_equipaments as $item}
                                                    <p>
                                                        <input type="checkbox" name="propostes[]" value="{$item->NUM_PROPOSTA}" id=""> <span class="num_proposta">{$item->NUM_PROPOSTA}</span> <label>{$item->DESC_CURTA} ({$item->IMPORT|number_format:0:",":"."}€)</label>
                                                    </p>
                                                {/foreach}
                                            </div>
                                        </div>
                                    {/if}

                                    {if $propostes_esports|COUNT>0}
                                        <div class="proposta_container">
                                            <h2>{$TITLE_PROPOSTA_6}<i class="icon-angle-down"></i></h2>
                                            <div class="list_propostes_container">
                                                {foreach $propostes_esports as $item}
                                                    <p>
                                                        <input type="checkbox" name="propostes[]" value="{$item->NUM_PROPOSTA}" id=""> <span class="num_proposta">{$item->NUM_PROPOSTA}</span> <label>{$item->DESC_CURTA} ({$item->IMPORT|number_format:0:",":"."}€)</label>
                                                    </p>
                                                {/foreach}
                                            </div>
                                        </div>
                                    {/if}

                                    {if $propostes_jocs|COUNT>0}
                                        <div class="proposta_container">
                                            <h2>{$TITLE_PROPOSTA_7}<i class="icon-angle-down"></i></h2>
                                            <div class="list_propostes_container">
                                                {foreach $propostes_jocs as $item}
                                                    <p>
                                                        <input type="checkbox" name="propostes[]" value="{$item->NUM_PROPOSTA}" id=""> <span class="num_proposta">{$item->NUM_PROPOSTA}</span> <label>{$item->DESC_CURTA} ({$item->IMPORT|number_format:0:",":"."}€)</label>
                                                    </p>
                                                {/foreach}
                                            </div>
                                        </div>
                                    {/if}

                                    {if $propostes_mascotes|COUNT>0}
                                        <div class="proposta_container">
                                            <h2>{$TITLE_PROPOSTA_8}<i class="icon-angle-down"></i></h2>
                                            <div class="list_propostes_container">
                                                {foreach $propostes_mascotes as $item}
                                                    <p>
                                                        <input type="checkbox" name="propostes[]" value="{$item->NUM_PROPOSTA}" id=""> <span class="num_proposta">{$item->NUM_PROPOSTA}</span> <label>{$item->DESC_CURTA} ({$item->IMPORT|number_format:0:",":"."}€)</label>
                                                    </p>
                                                {/foreach}
                                            </div>
                                        </div>
                                    {/if}

                                    {if $propostes_mobiliari_urba|COUNT>0}
                                        <div class="proposta_container">
                                            <h2>{$TITLE_PROPOSTA_9}<i class="icon-angle-down"></i></h2>
                                            <div class="list_propostes_container">
                                                {foreach $propostes_mobiliari_urba as $item}
                                                    <p>
                                                        <input type="checkbox" name="propostes[]" value="{$item->NUM_PROPOSTA}" id=""> <span class="num_proposta">{$item->NUM_PROPOSTA}</span> <label>{$item->DESC_CURTA} ({$item->IMPORT|number_format:0:",":"."}€)</label>
                                                    </p>
                                                {/foreach}
                                            </div>
                                        </div>
                                    {/if}

                                    {if $propostes_mobilitat|COUNT>0}
                                        <div class="proposta_container">
                                            <h2>{$TITLE_PROPOSTA_10}<i class="icon-angle-down"></i></h2>
                                            <div class="list_propostes_container">
                                                {foreach $propostes_mobilitat as $item}
                                                    <p>
                                                        <input type="checkbox" name="propostes[]" value="{$item->NUM_PROPOSTA}" id=""> <span class="num_proposta">{$item->NUM_PROPOSTA}</span> <label>{$item->DESC_CURTA} ({$item->IMPORT|number_format:0:",":"."}€)</label>
                                                    </p>
                                                {/foreach}
                                            </div>
                                        </div>
                                    {/if}

                                    {if $propostes_parcs|COUNT>0}
                                        <div class="proposta_container">
                                            <h2>{$TITLE_PROPOSTA_11}<i class="icon-angle-down"></i></h2>
                                            <div class="list_propostes_container">
                                                {foreach $propostes_parcs as $item}
                                                    <p>
                                                        <input type="checkbox" name="propostes[]" value="{$item->NUM_PROPOSTA}" id=""> <span class="num_proposta">{$item->NUM_PROPOSTA}</span> <label>{$item->DESC_CURTA} ({$item->IMPORT|number_format:0:",":"."}€)</label>
                                                    </p>
                                                {/foreach}
                                            </div>
                                        </div>
                                    {/if}

                                    {if $propostes_patrimoni|COUNT>0}
                                        <div class="proposta_container">
                                            <h2>{$TITLE_PROPOSTA_12}<i class="icon-angle-down"></i></h2>
                                            <div class="list_propostes_container">
                                                {foreach $propostes_patrimoni as $item}
                                                    <p>
                                                        <input type="checkbox" name="propostes[]" value="{$item->NUM_PROPOSTA}" id=""> <span class="num_proposta">{$item->NUM_PROPOSTA}</span> <label>{$item->DESC_CURTA} ({$item->IMPORT|number_format:0:",":"."}€)</label>
                                                    </p>
                                                {/foreach}
                                            </div>
                                        </div>
                                    {/if}

                                    {if $propostes_reciclatge|COUNT>0}
                                        <div class="proposta_container">
                                            <h2>{$TITLE_PROPOSTA_13}<i class="icon-angle-down"></i></h2>
                                            <div class="list_propostes_container">
                                                {foreach $propostes_reciclatge as $item}
                                                    <p>
                                                        <input type="checkbox" name="propostes[]" value="{$item->NUM_PROPOSTA}" id=""> <span class="num_proposta">{$item->NUM_PROPOSTA}</span> <label>{$item->DESC_CURTA} ({$item->IMPORT|number_format:0:",":"."}€)</label>
                                                    </p>
                                                {/foreach}
                                            </div>
                                        </div>
                                    {/if}

                                    {if $propostes_reguers|COUNT>0}
                                        <h2 style="font-size: 18px;">ELS REGUERS</h2>
                                        <div class="proposta_container">
                                            <h2>{$TITLE_PROPOSTA_14}<i class="icon-angle-down"></i></h2>
                                            <div class="list_propostes_container">
                                                {foreach $propostes_reguers as $item}
                                                    <p>
                                                        <input type="checkbox" name="propostes[]" value="{$item->NUM_PROPOSTA}" id=""> <span class="num_proposta">{$item->NUM_PROPOSTA}</span> <label>{$item->DESC_CURTA} ({$item->IMPORT|number_format:0:",":"."}€)</label>
                                                    </p>
                                                {/foreach}
                                            </div>
                                        </div>
                                    {/if}

                                    {if $propostes_vinallop|COUNT>0}
                                        <h2 style="font-size: 18px;">VINALLOP</h2>
                                        <div class="proposta_container">
                                            <h2>{$TITLE_PROPOSTA_15}<i class="icon-angle-down"></i></h2>
                                            <div class="list_propostes_container">
                                                {foreach $propostes_vinallop as $item}
                                                    <p>
                                                        <input type="checkbox" name="propostes[]" value="{$item->NUM_PROPOSTA}" id=""> <span class="num_proposta">{$item->NUM_PROPOSTA}</span> <label>{$item->DESC_CURTA} ({$item->IMPORT|number_format:0:",":"."}€)</label>
                                                    </p>
                                                {/foreach}
                                            </div>
                                        </div>
                                    {/if}

                                    <!--
                                    <p>
                                        <span>
                                            {$LABEL_3}
                                            <input type="text" name="proposta1" id="proposta1" value="{$proposta1}" maxlength="3" autocomplete="off" style="width: 90px">
                                        </span>
                                        <label id="proposta1-error">{$LABEL_ERROR_FIELD_NV}</label>
                                    </p>
                                    <p>
                                        <span>
                                            {$LABEL_4}
                                            <input type="text" name="proposta2" id="proposta2" value="{$proposta2}" maxlength="3" autocomplete="off" style="width: 90px">
                                        </span>
                                        <label id="proposta2-error">{$LABEL_ERROR_FIELD_NV}</label>
                                    </p>
                                    <p>
                                        <span>
                                            {$LABEL_5}
                                            <input type="text" name="proposta3" id="proposta3" value="{$proposta3}" maxlength="3" autocomplete="off" style="width: 90px">
                                        </span>
                                        <label id="proposta3-error">{$LABEL_ERROR_FIELD_NV}</label>
                                    </p>
                                    -->
                                    <!--<p>&nbsp;</p>-->

                                    <p class="title">{$TITLE_5}</p>

                                    <p>
                                        <label>{$LABEL_FIELD_1} *</label>
                                        <label>{$LABEL_FIELD_1B} </label>
                                        <select name="tdocument" id="tdocument" placeholder="{$LABEL_FIELD_1}">
                                            <option value="">{$LABEL_FIELD_1_1}</option>
                                            <option value="1" {if ($tdocument=="1")} selected {/if}>DNI</option>
                                            <option value="2" {if ($tdocument=="2")} selected {/if}>Passaport</option>
                                            <option value="3" {if ($tdocument=="3")} selected {/if}>NIE</option>
                                        </select>
                                    </p>
                                    <p class="option1" {if $tdocument=="1"} style="display:block"{/if}>
                                        <label>NIF *</label>
                                        <span>
                                            {$LABEL_FIELD_2_1}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="text" id="dni_number" name="dni_number" value="{$dni_number}" maxlength="8" style="width: 120px">
                                        </span>
                                        <span>
                                            {$LABEL_FIELD_2_2}
                                            <input type="text" id="dni_letter" name="dni_letter" value="{$dni_letter}" maxlength="1">
                                        </span>
                                         <label id="option1-error" class="error" for="option1-error" style="display: none;"></label>
                                    </p>
                                    <p class="option2" {if $tdocument=="2"} style="display:block"{/if}>
                                        <span>
                                            {$LABEL_FIELD_3_1}&nbsp;&nbsp;
                                            <input type="text" id="option2_passport_number" name="option2_passport_number" value="{$txt_passport_number}" style="width: 200px">
                                        </span>
                                        <label id="option2-error" class="error" for="option2-error" style="display: none;"></label>
                                    </p>
                                    <p class="option3" {if $tdocument=="3"} style="display:block"{/if}>
                                        <label>{$LABEL_FIELD_4_1} *</label>
                                        <span>
                                            {$LABEL_FIELD_2_2}
                                            <input type="text" id="tr_letter_1" name="tr_letter_1" value="{$tr_letter_1}" maxlength="1" style="width: 25px">
                                        </span>
                                        <span>
                                            {$LABEL_FIELD_2_1}
                                            <input type="text" id="tr_number" name="tr_number" value="{$tr_number}" maxlength="8" style="width: 110px">
                                        </span>
                                        <span>
                                            {$LABEL_FIELD_2_2}
                                            <input type="text" id="tr_letter_2" name="tr_letter_2" value="{$tr_letter_2}" maxlength="1" style="width: 25px; display: inline-block">
                                        </span>
                                        <label id="option3-error" class="error" for="option3-error" style="display: none;"></label>
                                    </p>

                                    <p>
                                        <span>
                                            {$LABEL_FIELD_5} * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="text" name="pin" id="pin" maxlength="10" style="width: 120px">
                                        </span>
                                    </p>

                                    <p>&nbsp;</p>

                                    <p>
                                        {$LABEL_FIELD_10}
                                    <div class="g-recaptcha" data-sitekey="6Lfdwh4TAAAAAKajPw-1Rj8msRd8YVZx14Vwova-"></div>
                                    <label id="recaptcha-error">{$LABEL_ERROR_FIELD}</label>
                                    </p>
                                    <p style="font-size: 12px">{$LABEL_1}</p>
                                    <p style="font-size: 12px">{$LABEL_TEXT_3}</p>
                                    <p>
                                        <input type="submit" name="enviar" id="enviar" value="{$LABEL_SUBMIT2}">
                                    </p>
                                    <p style="font-size: 12px">
                                        {$LABEL_TEXT_2}
                                    </p>
                                   </form>
                                <p>
                                    {$TEXT_7}
                                    <a href="https://www2.tortosa.cat/inscripcio-participacio/recuperacio_dades_acces.php" target="_blank">{$AQUI}</a>{$PUNT}
                                </p>
                            {/if}
                        </div>
                   </div>
            </div>
        </div>
    </div>

    <!-- The Modal -->
    <div id="formulari_generic_modal" class="modal">
        <!-- Modal content -->
        <div class="modal-content">
            <span class="close">&times;</span>
            <p>{$LABEL_AVIS_1}</p>
            <p>
                <input type="button" name="accept" value="{$LABEL_BUTTON_1}" id="popup_accept">

                <input type="button" name="cancel" value="{$LABEL_BUTTON_2}" id="popup_cancel">
            </p>
        </div>
    </div>
    {include file="footer.tpl"}
</body>
</html>