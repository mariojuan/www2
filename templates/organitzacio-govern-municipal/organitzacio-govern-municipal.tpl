<html>
   <head>
      {include file="head.tpl"}
   </head>
   <body>
      {include file="header.tpl"}
      <div id="page-wrap">

      <div class="contenedor-responsive">

         <div id="conttranstotalcos">
            <div id="conttranscos">
               <div id="btranspresen">

                  <div id="transimatge">
                     <img src="{$image}" class="img-slider-territori"/>
                  </div>

                  <div id="transimatgeText">
                     <h1>
                        {$LABEL_TITOL1}
                     </h1>
                  </div>

               </div>

               <div id="tcentre_municipi">

                  <font size="4" color="#666666">{$LABEL_TC0}</font>
                  <p>{$LABEL_TC1}</p>
                  <h3>{$LABEL_TC2}</h3>

                  <img src="http://www.tortosa.cat/webajt/ajunta/om/estructura10.jpg" class="img-responsive"/>

                  <div class="separator"></div>

                  {foreach $LINK_DELEGACIONS as $item}
                  <p><a href="{$item['link']}" target="{$item['target']}">{$item['name']}</a></p>
                  {/foreach}

                  <div class="separator"></div>
                  <h3>{$LABEL_TC3}</h3>
                  <p><b>{$LABEL_TC4}</b></p>

                  <div class="ogm-row">
                     <div class="ogm-col-left">

                        <img src="http://www.tortosa.cat/webajt/ajunta/om/mappobles.jpg" class="marcterme img-pobles-barris"/>
                        
                     </div>
                     <!-- <div class="separator"></div> -->
                     <div class="ogm-col-right">

                     <p>{$LABEL_TC5}:</p>
                        <ul>
                            <li><p>{$LABEL_TC51}</p></li>
                            <li><p>{$LABEL_TC6}</p></li>
                        </ul>
                     <p>{$LABEL_TC7}</p>

                        {foreach $LINK_POBLES as $item}
                        <a href="{$item['link']}" target="{$item['target']}">{$item['name']}</a>
                        {/foreach}

                     </div>
                  </div>

                  <div class="separator"></div>

                  <p><b>{$LABEL_TC8}</b></p>

                  <div class="ogm-row">
                     <div class="ogm-col-left">

                     <img src="http://www.tortosa.cat/webajt/ajunta/om/mapbarris.jpg" class="marcterme img-pobles-barris"/>

                     </div>
                     <div class="ogm-col-right">

                        <p>{$LABEL_TC9}</p>
                        <ul>
                           {foreach $ARRAY_BARRIS as $item}
                           <li><b>{$item['barri']}: </b> {$item['persona']}</li>
                           {/foreach}
                        </ul>
                        <p>{$LABEL_TC10}</p>
                        {foreach $LINK_BARRIS as $item}
                        <p><a href="{$item['link']}" target="{$item['target']}">{$item['name']}</a></p>
                        {/foreach}

                     </div>
                  </div>

               </div>
               <div class="separator"></div>
            </div>
         </div>
         </div>
      </div>
      {include file="footer.tpl"}
   </body>
</html>