<!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->
<div id="page-wrap">

   <div class="contenedor-responsive">

      <div id="conttranstotalcos">

         <div id="conttranscos">

            <div id="btranspresen">
               <div id="transimatge">			
                  <img src="{$image}" class="img-slider-territori"/>			
               </div>
               <div id="transimatgeText">
                  <h1>
                     {$LABEL_TITOL1}
                     <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                  </h1>
               </div>
            </div>

            <ul id='menu_planA'>
               {foreach $MENU as $itemMenu}
               <a href={$itemMenu}>
                  <li id='menu_planA_item'>
                     {$itemMenu@key}
                  </li>
               </a>
               {/foreach}
            </ul>

            <div id="tcentre_planA">
               <font size=4 color=#666666>
               {$LABEL_TITOL0}
               </font>
               <font size=4 color=#333>
               <p>{$LABEL_TITOL0b}</p>
               </font>
               <div class="caixapreguntes">
                  <div class="titolpregunta">
                     {$LABEL_TC14}
                  </div>
                  <div class="textpregunta">
                     {$LABEL_TC14B} <a href="https://www2.tortosa.cat/public_files/pressupostos-participatius/LlistaPropostes2018.pdf" target="_blank">{$LABEL_TC14C}</a>
                  </div>
                  <div class="textpregunta">
                     {$LABEL_TC2B}
                  </div>
                  <br>
                  <div class="titolpregunta">
                     {$LABEL_TC1}
                  </div>
                  <div class="textpregunta">
                     {$LABEL_TC1B}
                  </div>
                  <br>
                  <div class="titolpregunta" style="font-size: 20px">
                     {$LABEL_TC3}
                  </div>
                  <div class="textdatespregunta">
                     {$LABEL_TC6}
                  </div>
                  <div class="separator_organs"></div>
                  <a href="https://www2.tortosa.cat/inscripcio-participacio/" target="_blank" title="Pressupostos participatius 2018">
                      <div class="marcfinestra singleBanner singleBanner-float-none">
                          <div id="bannerima">
                              <img src="/images/pciutadana/binscriute.jpg">
                          </div>
                          <div id="bannertextpp">
                              {$LABEL_TC23}
                          </div>
                      </div>
                  </a>

                  <a href="https://www2.tortosa.cat/participacio-ciutadana/votacio-telematica/" target="_blank" title="Pressupostos participatius 2018">
                      <div class="marcfinestra singleBanner singleBanner-float-none">
                          <div id="bannerima">
                              <img src="/images/pciutadana/bvota.jpg">
                          </div>
                          <div id="bannertextpp">
                              {$LABEL_TC24}
                          </div>
                      </div>
                  </a>
                  <div class="separator_votacio"></div>
                  <div style="color: #ffffff; font-weight: bold; margin-top: 15px">{$LABEL_TC15}</div>
                  <p style="margin-bottom: 20px; margin-top: 0" class="textpregunta">{$LABEL_TC16}</p>
                  <p style="margin-bottom: 20px" class="textpregunta">
                      {$LABEL_TC17}
                      <ul style="list-style: none; padding-left: 10px">
                       <li class="textpregunta">{$LABEL_TC18}</li>
                       <li class="textpregunta">{$LABEL_TC19} <a href="https://www2.tortosa.cat/inscripcio-participacio/recuperacio_dades_acces.php" target="_blank">{$LABEL_TC14C}</a></li>
                      </ul>
                  </p>
                  <p class="textpregunta">
                    {$LABEL_TC20}
                        <ul style="list-style: none; padding-left: 10px">
                            <li class="textpregunta">{$LABEL_TC21} <a href="mailto:&#105;&#110;&#102;&#111;&#064;&#116;&#111;&#114;&#116;&#111;&#115;&#097;&#046;&#099;&#097;&#116;">&#105;&#110;&#102;&#111;&#064;&#116;&#111;&#114;&#116;&#111;&#115;&#097;&#046;&#099;&#097;&#116;</a></li>
                            <li class="textpregunta">{$LABEL_TC22}</li>
                        </ul>
                  </p>
                  <br>
                  <div class="titolpregunta" style="font-size: 20px">
                     {$LABEL_TC4}
                  </div>
                  <div class="textpregunta">
                     {$LABEL_TC4B}<br>{$LABEL_TC4B3}<a href="https://www2.tortosa.cat/public_files/pressupostos-participatius/PropostesVota2018.pdf" target="_blank">{$LABEL_TC4B2}</a>
                     <br>{$LABEL_TC4C}
                  </div>
                  <br>
                  <div class="textdatespregunta">
                     {$LABEL_TC7}<font font-size=12 color=#333>{$LABEL_TC7B}</font>
                  </div>
                  <div class="separator_votacio"></div>
                  <div class="textdatespregunta">
                     {$LABEL_TC7C} <font font-size=12 color=#333>{$LABEL_TC7D}</font>
                  </div>
                  <br>
                  <div class="textpregunta3">
                     {$LABEL_TC8}
                  </div>
                  <div class="textdatespregunta3">
                     {$LABEL_TC8B}
                  </div>
                  <div class="separator_votacio"></div>
                  <div class="textpregunta3">
                     {$LABEL_TC9}
                  </div>
                  <div class="textdatespregunta3">
                     {$LABEL_TC9B}
                  </div>
                  <div class="separator_votacio"></div>
                  <div class="textpregunta3">
                     {$LABEL_TC10}
                  </div>
                  <div class="textdatespregunta3">
                        {$LABEL_TC10B}
                  </div>
                  <div class="separator_votacio"></div>
                  <div class="textpregunta3">
                     {$LABEL_TC11}
                  </div>
                  <div class="textdatespregunta3">
                        {$LABEL_TC11B}
                  </div>
                  <div class="separator_votacio"></div>
                  <div class="textpregunta3">
                     {$LABEL_TC12}
                  </div>
                  <div class="textdatespregunta3">
                        {$LABEL_TC12B}
                  </div>
                  <div class="separator_votacio"></div>
                  <div class="textpregunta3">
                     {$LABEL_TC13}
                  </div>
                  <div class="textdatespregunta3">
                        {$LABEL_TC13B}
                  </div>
                  <div class="separator_organs"></div>
               </div>
               <br>

               
               <div class="titolpregunta3">
                  <a href="https://www2.tortosa.cat/pciutadana/2018/votacio.php">{$LABEL_TC5}</a>
               </div>

               <br>
              
            </div>

         </div>
      </div>
   </div>
</div>
