<html>
	<head>
        {include file="head.tpl"}
        {if $apartat==5}
            <script src='https://www.google.com/recaptcha/api.js?hl={$lang}'></script>
            <script type="text/javascript">
                var captcha_value = false;
                var accept_policy = false;
                $().ready(function() {
                    $("#popup_close").click(function() {
                        tancapopup();
                    });
                    $(".close").click(function() {
                        tancapopup();
                    });
                    $("#queixes_i_suggeriments_form").validate({
                        rules: {
                            nom_sollicitant:  {
                                required: true
                            },
                            cognom1_sollicitant: {
                                required: true
                            },
                            tipus_document_sollicitant: {
                                required: true
                            },
                            numero_document_sollicitant: {
                                required: true
                            },
                            tipus_via_sollicitant: {
                                required: true
                            },
                            tipus_nom_sollicitant: {
                                required: true
                            },
                            numero_sollicitant: {
                                required: true,
                                digits: true
                            },
                            lletra_sollicitant: {
                                required: true
                            },
                            km_sollicitant: {
                                digits: true
                            },
                            pis_sollicitant: {
                                digits: true
                            },
                            provincia_sollicitant: {
                                required: true
                            },
                            municipi_sollicitant: {
                                required: true
                            },
                            codi_postal_sollicitant: {
                                required: true,
                                digits: true
                            },
                            email_sollicitant: {
                                email:true
                            },
                            tipus_peticio_sollicitant: {
                                required: true
                            },
                            afecta_a_sollicitant: {
                                required: true
                            },
                            descripcio_sollicitant: {
                                required: true
                            },
                            accept_privacy_policy: {
                                required: true
                            }
                        },
                        messages: {
                            nom_sollicitant: {
                                required: "{$LABEL_ERROR_1}"
                            },
                            cognom1_sollicitant: {
                                required: "{$LABEL_ERROR_2}"
                            },
                            tipus_document_sollicitant: {
                                required: "{$LABEL_ERROR_3}"
                            },
                            numero_document_sollicitant: {
                                required: "{$LABEL_ERROR_4}"
                            },
                            tipus_via_sollicitant: {
                                required: "{$LABEL_ERROR_5}"
                            },
                            tipus_nom_sollicitant: {
                                required: "{$LABEL_ERROR_6}"
                            },
                            numero_sollicitant: {
                                required: "{$LABEL_ERROR_7}",
                                digits: "{$LABEL_ERROR_INTEGER}"
                            },
                            lletra_sollicitant: {
                                required: "{$LABEL_ERROR_8}"
                            },
                            km_sollicitant: {
                                digits: "{$LABEL_ERROR_INTEGER}"
                            },
                            pis_sollicitant: {
                                digits: "{$LABEL_ERROR_INTEGER}"
                            },
                            provincia_sollicitant: {
                                required: "{$LABEL_ERROR_9}"
                            },
                            municipi_sollicitant: {
                                required: "{$LABEL_ERROR_10}"
                            },
                            codi_postal_sollicitant: {
                                required: "{$LABEL_ERROR_11}",
                                digits: "{$LABEL_ERROR_INTEGER}"
                            },
                            email_sollicitant: {
                                email: "{$LABEL_ERROR_EMAIL}"
                            },
                            tipus_peticio_sollicitant: {
                                required: "{$LABEL_ERROR_12}"
                            },
                            afecta_a_sollicitant: {
                                required: "{$LABEL_ERROR_13}"
                            },
                            descripcio_sollicitant: {
                                required: "{$LABEL_ERROR_14}"
                            },
                            accept_privacy_policy: {
                                required: "{$LABEL_16}"
                            }
                        },
                        submitHandler: function(form) {
                            if(captcha_value) {
                                if(accept_policy) {
                                    $('#queixes_i_suggeriments_form input[type="submit"]').css('display', 'none');
                                    form.submit();
                                }
                                else {
                                    $('#privacy_policy_error').css('display', 'block');
                                }
                            }
                            else {
                                $('#recaptcha-error').css('display', 'block');
                            }
                        }
                    });

                    $("#incidencies_via_publica_form").validate();
                });

                function obripopup() {
                    var modal = $('#formulari_generic_modal');
                    modal.css("display", "block");
                    accept_policy = true;
                }
                function tancapopup() {
                    var modal = $('#formulari_generic_modal');
                    modal.css("display", "none");
                }

                function captcha() {
                    var xmlhttp = new XMLHttpRequest();
                    xmlhttp.onreadystatechange = function() {
                        if (this.readyState == 4 && this.status == 200) {
                            var out = this.responseText;
                            if(out==1) {
                                captcha_value = true;
                                $('#recaptcha-error').css('display', 'none');
                            }
                            else {
                                captcha_value = false;
                                $('#recaptcha-error').css('display', 'block');
                            }
                        }
                    };
                    xmlhttp.open("POST", "https://www2.tortosa.cat/libs/verify-recaptcha.php", false);
                    xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                    xmlhttp.send("g-recaptcha-response="+grecaptcha.getResponse());
                }
                function recaptchaCallback() {
                    captcha();
                }
            </script>
        {/if}
	</head>
	<body>
		{include file="header.tpl"}
        {if $apartat==5}
            {include file="pciutadana/queixes_suggeriments.tpl" image="/images/pciutadana/tira.jpg" ntitol=2}
        {else}
		    {include file="pciutadana/sistemes_home.tpl" image="/images/pciutadana/tira.jpg" ntitol=2}
        {/if}
		{include file="footer.tpl"}
	</body>
</html>