<!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->
<div id="page-wrap">

   <div class="contenedor-responsive">

      <div id="conttranstotalcos">

         <div id="conttranscos">

            <div id="btranspresen">
               <div id="transimatge">			
                  <img src="{$image}" class="img-slider-territori"/>			
               </div>
               <div id="transimatgeText">
                  <h1>
                     {$LABEL_TITOL1}
                     <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                  </h1>
               </div>
            </div>

            <ul id='menu_planA'>
               {foreach $MENU as $itemMenu}
               <a href={$itemMenu}>
                  <li id='menu_planA_item'>
                     {$itemMenu@key}
                  </li>
               </a>
               {/foreach}
            </ul>

            <div id="tcentre_planA">
               <font size=4 color=#666666>
               {$LABEL_TITOL0}
               </font>
               {if $item==1}
               <p>{$LABEL_TC1}<br>
                  <i>{$LABEL_TC1B}</i>
               </p>
               <p>{$LABEL_TC2}<br>
                  <i>{$LABEL_TC2B}</i>
               </p>
               <p>{$LABEL_TC3}</p>
               <p>{$LABEL_TC4}</p>
               <p>{$LABEL_TC5}</p>
               <p>{$LABEL_TC6}</p>
               <p>{$LABEL_TC7}</p>
               <img src="/images/pciutadana/esquema.jpg" class="img-responsive">
               {elseif $item==2}
               <p>{$LABEL_TC3}&nbsp;<a href="/docs/audiencies/audiencia2018.pdf">{$LABEL_TC1C}</a><br>
                  <a href="http://www2.tortosa.cat/noticies/noticia.php?lang=ca&id=12388" target="_blank">
                  <i>{$LABEL_TC1B}</i></a>
               </p>

               <p>{$LABEL_TC1}&nbsp;<a href="/docs/audiencies/audiencia2017.pdf">{$LABEL_TC1C}</a><br>
                  <a href="http://www2.tortosa.cat/noticies/noticia.php?lang=ca&id=5901" target="_blank">
                  <i>{$LABEL_TC1B}</i></a>
               </p>
               
               <p>{$LABEL_TC2}<br>
                  <a href="http://www2.tortosa.cat/noticies/noticia.php?lang=ca&id=5721" target="_blank">
                  <i>{$LABEL_TC1B}</i></a>
               </p>
               
               {elseif $item==3}
               <p>{$LABEL_TC1}<br>
               <p>{$LABEL_TC2}<br>
                  <a href="https://www2.tortosa.cat/pciutadana/2016/consultamonument.php" target="_blank">
                  <i>{$LABEL_TC2B}</i>
               </p>
               </a>
               {/if}
               <br><br><br>
            </div>

         </div>
      </div>
   </div>
</div>
