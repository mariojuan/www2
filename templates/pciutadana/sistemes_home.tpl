<!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->
<style type="text/css">
   <!--
      .Estilo3 {
         font-family: Verdana, Arial, Helvetica, sans-serif;
         color: #FFFFFF;
         font-size: 21px;
      }
      -->
</style>
<div id="page-wrap">
   <div class="contenedor-responsive">
      <div id="conttranstotalcos">
         <div id="conttranscos">
            <div id="btranspresen">
               <div id="transimatge">        
                  <img src="{$image}" class="img-slider-territori"/>       
               </div>
               <div id="transimatgeText">
                  <h1>
                     {$LABEL_TITOL1}
                     <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                  </h1>
               </div>
            </div>
            <div class="subtitolinfeco" id="subtitolinfeco2">
               {$LABEL_TITOL1b}
            </div>
            <div class="marcback">
               <a href="{$url_sistemes_back}">
               <i class="icon-angle-double-up"></i>
               </a>
            </div>
            <ul id='menu_planA'>
               {foreach $MENU as $itemMenu}
               <a href={$itemMenu}>
                  <li id='menu_planA_item'>
                     {$itemMenu@key}
                  </li>
               </a>
               {/foreach}
            </ul>
            <div id="tcentre_planA">
               <font size=4 color=#666666>
               {$LABEL_TITOL0}
               </font>
               {if $item==1}
               <p>{$LABEL_TC1}</p>
               <p>{$LABEL_TC2}</p>
               {elseif $item==2}

                <div id="titolsacparticipa" class="titolaltrespuntsparticipa titolaltrespuntsparticipaMod titolqueixessuggeriments">
                    <a href="/pciutadana/queixes_suggeriments.php?lang={$lang}">
                        {$LABEL_TC140}
                    </a>
                </div>

               <!-- Secció digueu la vostra -->

               <div class="div-row-banners-social-media">

               <a href="https://www.facebook.com/ajuntamentdetortosa/" target="_blank">
                  <div class="caixafMod">
                     <img src="/images/pciutadana/tfacebook.jpg">
                  </div>
               </a>

               <a href="https://twitter.com/Tortosa?ref_src=twsrc%5Etfw" target="_blank">
                  <div class="caixatMod">
                     <img src="/images/pciutadana/ttwitter.jpg">
                  </div>
               </a>

               </div>

               <!-- Servei atenció ciutadana -->

               <div id="titolsacparticipa" class="titolaltrespuntsparticipa titolaltrespuntsparticipaMod">{$LABEL_TC1}</div>
               <br>
               <div id="caixacanalssac">
                  {$LABEL_TC11}
                  <ul>
                     <li>{$LABEL_TC12} <a href="http://www.seu.cat/tortosa" target="_self">www.seu.cat/tortosa</a>{$LABEL_TC13} <a href="http://www.tortosa.cat" target="_self">www.tortosa.cat</a></li>
                     <br>
                     <li><strong>{$LABEL_TC14}</strong><br><br>
                        <u>{$LABEL_TC15}</u><br> 
                        {$LABEL_TC16}<br>
                        {$LABEL_TC17}<br>
                        {$LABEL_TC18}<br><br>
                        <u>{$LABEL_TC19}</u><br>
                        {$LABEL_TC110} 977585800
                        <br><br>
                        <a href="mailto:sac.tortosa@tortosa.cat">sac.tortosa@tortosa.cat</a><br><br>
                     <li><strong>{$LABEL_TC111}</strong><br>
                        <br>
                        <u>{$LABEL_TC112}</u><br>
                        {$LABEL_TC113}<br>
                        {$LABEL_TC114}
                        <br><br>
                        <u>{$LABEL_TC19}</u><br>{$LABEL_TC110} 977474024
                        <br><br>
                        <a href="mailto:reguers@tortosa.cat">reguers@tortosa.cat</a><br><br>
                     <li><strong>{$LABEL_TC115}</strong><br>
                        <br>
                        <u>{$LABEL_TC112}</u><br>
                        {$LABEL_TC116}<br>
                        {$LABEL_TC117}
                        <br><br><u>{$LABEL_TC19}</u>
                        <br>{$LABEL_TC110} 977474399<br><br>
                        <a href="mailto:vinallop@tortosa.cat">vinallop@tortosa.cat</a><br>
                     </li>
                  </ul>
               </div>
               <!-- /Servei atenció ciutadana -->
               <!-- Policia local -->
               <div id="titolsacparticipa" class="titolaltrespuntsparticipa titolaltrespuntsparticipaMod">{$LABEL_TC2}</div>
               <br>
               <div id="caixacanalssac">
                  {$LABEL_TC11}
                  <ul>
                     <li><strong>{$LABEL_TC118}</strong><br><br>
                        <u>{$LABEL_TC15}</u><br> 
                        Pl. Immaculada, 16. 43500 - Tortosa<br>
                        {$LABEL_TC119}<br><br>
                        <u>{$LABEL_TC19}</u><br>
                        {$LABEL_TC110} 977448622 i 092
                        <br><br>
                        <a href="mailto:pol.tortosa@tortosa.cat">pol.tortosa@tortosa.cat</a>
                     </li>
                  </ul>
               </div>
               <!-- /Policia local -->
               <!-- Altres punts per dir la vostra -->
               <div id="titolsacparticipa" class="titolaltrespuntsparticipa titolaltrespuntsparticipaMod">{$LABEL_TC3}</div>
               <br>
               <div id="caixacanalssac">
                  {$LABEL_TC11}
                  <ul id="altrespuntinforma">
                     <li>{$LABEL_TC120}</li>
                     <ul id="altressubpuntinforma">
                        <li><i class="icon-comment-empty"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/gestiointerna/headlines/partpublica/indexllistaheadlines.asp?codi=5782" target="_blank">{$LABEL_TC121}</a></li>
                        <li><i class="icon-codeopen"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/ajunta/consulta/index.asp" target="_blank">{$LABEL_TC122}</a></li>
                        <li><i class="icon-comment-empty"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/gestiointerna/headlines/partpublica/indexllistaheadlines.asp?codi=5962" target="_blank">{$LABEL_TC123}</a></li>
                        <li><i class="icon-codeopen"></i>&nbsp;<a href="/pciutadana/prespar2017.php" target="_blank">{$LABEL_TC124}</a></li>
                     </ul>
                     <li>{$LABEL_TC125}</li>
                     <ul id="altressubpuntinforma">
                        <li><i class="icon-doc-text"></i>&nbsp;<a href="http://www.tortosa.cat/alcalde#escriualcalde" target="_blank">{$LABEL_TC126}</a></li>
                        <li><i class="icon-doc-text"></i>&nbsp;<a href="/pciutadana/queixes_suggeriments.php?lang={$lang}" target="_blank">{$LABEL_TC127}</a></li>
                         <li><i class="icon-doc-text"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/ajunta/viapublica/index.asp?lang={$lang}" target="_blank">{$LABEL_TC141}</a></li>
                        <li><i class="icon-doc-text"></i>&nbsp;<a href="http://www.puntjove.tortosa.cat/?page_id=22" target="_blank">{$LABEL_TC128}</a></li>
                        <li><i class="icon-doc-text"></i>&nbsp;<a href="http://www.radiotortosa.cat/ca/contacta" target="_blank">{$LABEL_TC129}</a></li>
                        <li><i class="icon-doc-text"></i>&nbsp;<a href="http://www.firatortosa.cat/index.php/ca/contacto" target="_blank">{$LABEL_TC130}</a></li>
                        <li><i class="icon-doc-text"></i>&nbsp;<a href="http://www.museudetortosa.cat/contacte/contacte" target="_blank">{$LABEL_TC131}</a></li>
                        <li><i class="icon-doc-text"></i>&nbsp;<a href="http://www.tortosaturisme.cat/contacta/" target="_blank">{$LABEL_TC132}</a></li>
                        <li><i class="icon-doc-text"></i>&nbsp;<a href="http://www.gumtsa.cat/contacte.php" target="_blank">{$LABEL_TC133}</a></li>
                        <li><i class="icon-doc-text"></i>&nbsp;<a href="http://www.clinicaterresebre.cat/index.php/contacteu" target="_blank">{$LABEL_TC134}</a></li>
                        <li><i class="icon-doc-text"></i>&nbsp;<a href="http://www.aiguesdetortosa.cat/Empresa/Contacte/" target="_blank">{$LABEL_TC135}</a></li>
                     </ul>
                     <li>{$LABEL_TC136}</li>
                     <ul id="altressubpuntinforma">
                        <li><i class="icon-phone"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/correus.asp" target="_blank">{$LABEL_TC137}</a></li>
                        <li><i class="icon-mail"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/correus.asp" target="_blank">{$LABEL_TC138}</a></li>
                        <li><i class="icon-check"></i>&nbsp;<a href="http://www.radiotortosa.cat/#enquesta" target="_blank">{$LABEL_TC139}</a></li>
                     </ul>
                     <br><br><br><br>
                     <br><br><br><br>
                  </ul>
               </div>
               <!-- /Altres punts per dir la vostra -->
            </div>
            <!-- /Secció digueu la vostra -->

            {elseif $item==3}
            <div id="caixaaltresparticipa">
               <!--<div id="titolaltrespuntsparticipa">
                  Altres punts d'informació</div>
                  <br>-->
               <ul id="altrespuntinforma">
                  <li>Notícies</li>
                  <ul id="altressubpuntinforma">
                     <li><i class="icon-newspaper"></i>&nbsp;<a href="/noticies/index.php" target="_self">Notes de premsa - Ajuntament de Tortosa</a></li>
                     <li><i class="icon-newspaper"></i>&nbsp;<a href="http://www.escolamunicipaldemusicadetortosa.cat/index.php/informacions/noticies-arxivades" target="_blank">Escola de Música</a></li>
                     <li><i class="icon-newspaper"></i>&nbsp;<a href="http://www.puntjove.tortosa.cat/" target="_blank">Punt Jove</a></li>
                     <li><i class="icon-newspaper"></i>&nbsp;<a href="http://www.escolamunicipaldeteatredetortosa.cat/index.php/informacions/noticiesarxivades" target="_blank">Escola de Teatre</a></li>
                     <li><i class="icon-newspaper"></i>&nbsp;<a href="http://www.festadelrenaixement.org/noticies" target="_blank">Festa del Renaixement</a></li>
                     <li><i class="icon-newspaper"></i>&nbsp;<a href="http://www.museudetortosa.cat/premsa/noticies" target="_blank">Museu de Tortosa</a></li>
                     <li><i class="icon-newspaper"></i>&nbsp;<a href="http://www.teatreauditoritortosa.cat/noticies/" target="_blank">Teatre Auditori Felip Pedrell</a></li>
                     <li><i class="icon-newspaper"></i>&nbsp;<a href="http://agencia.tortosa.cat/noticias" target="_blank">Centre Formació Ocupacional</a></li>
                  </ul>
                  <hr height="1px" color="#cccccc">
                  <li>Agendes</li>
                  <ul id="altressubpuntinforma">
                     <li><i class="icon-calendar"></i>&nbsp;<a href="http://agenda.tortosa.cat/agenda-tortosa/" target="_blank">Agenda Tortosa</a></li>
                     <li><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.altanet.org/imact/agenda296.pdf" target="_blank">Cultura (pdf)</a></li>
                     <li><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.altanet.org/biblio/bmd_prog.pdf" target="_blank">Biblioteca (pdf)</a></li>
                     <li><i class="icon-calendar"></i>&nbsp;<a href="http://www.museudetortosa.cat/activitats/agenda" target="_blank">Museu de Tortosa</a></li>
                     <li><i class="icon-calendar"></i>&nbsp;<a href="http://www.bibliotecaspublicas.es/generico/agenda.jsp?&sedeweb=62&pw=0" target="_blank">Biblioteca</a></li>
                     <li><i class="icon-calendar"></i>&nbsp;<a href="http://www.escolamunicipaldeteatredetortosa.cat/index.php/informacions/calendaris" target="_blank">Escola Teatre</a></li>
                     <li><i class="icon-calendar"></i>&nbsp;<a href="http://www.teatreauditoritortosa.cat/calendari/?view=all" target="_blank">Programació Teatre Auditori Felip Pedrell</a></li>
                     <li><i class="icon-calendar"></i>&nbsp;<a href="http://www.escolamunicipaldemusicadetortosa.cat/index.php/informacions/calendaris" target="_blank">Escola de Música</a></li>
                     <li><i class="icon-calendar"></i>&nbsp;<a href="http://agencia.tortosa.cat/formacion" target="_blank">Centre Formació Ocupacional</a></li>
                  </ul>
                  <hr height="1px" color="#cccccc">
                  <li>Facebook</li>
                  <ul id="altressubpuntinforma">
                     <li><i class="icon-facebook-official"></i>&nbsp;<a href="https://www.facebook.com/ajuntamentdetortosa/" target="_blank">facebook Tortosa</a></li>
                     <li><i class="icon-facebook-official"></i>&nbsp;<a href="https://www.facebook.com/login/?next=https%3A%2F%2Fwww.facebook.com%2Fgroups%2F136741733017303%2F%3Ffref%3Dts" target="_blank">Escola de Música</a></li>
                     <li><i class="icon-facebook-official"></i>&nbsp;<a href="https://www.facebook.com/escolam.teatre.tortosa" target="_blank">Escola de Teatre</a></li>
                     <li><i class="icon-facebook-official"></i>&nbsp;<a href="https://www.facebook.com/login.php?api_key=127760087237610" target="_blank">Ràdio Tortosa</a></li>
                     <li><i class="icon-facebook-official"></i>&nbsp;<a href="https://www.facebook.com/tortosasport" target="_blank">Tortosa Sport</a></li>
                     <li><i class="icon-facebook-official"></i>&nbsp;<a href="https://www.facebook.com/pages/Festa-del-Renaixement/104122546304712" target="_blank">Festa del Renaixement</a></li>
                     <li><i class="icon-facebook-official"></i>&nbsp;<a href="https://www.facebook.com/MuseuDeTortosa" target="_blank">Museu de Tortosa</a></li>
                     <li><i class="icon-facebook-official"></i>&nbsp;<a href="https://www.facebook.com/tortosaturisme?fref=ts" target="_blank">Turisme</a></li>
                     <li><i class="icon-facebook-official"></i>&nbsp;<a href="https://www.facebook.com/vivertortosa" target="_blank">Viver d'empreses</a></li>
                  </ul>
                  <hr height="1px" color="#cccccc">
                  <li>Twitter</li>
                  <ul id="altressubpuntinforma">
                     <li><i class="icon-twitter"></i>&nbsp;<a href="https://twitter.com/Tortosa" target="_blank">twitter Tortosa</a></li>
                     <li><i class="icon-twitter"></i>&nbsp;<a href="https://twitter.com/EMMTortosa" target="_blank">Escola de Música</a></li>
                     <li><i class="icon-twitter"></i>&nbsp;<a href="https://twitter.com/ETEATRETORTOSA" target="_blank">Escola de Teatre</a></li>
                     <li><i class="icon-twitter"></i>&nbsp;<a href="https://twitter.com/intent/follow?original_referer=http%3A%2F%2Fwww.radiotortosa.cat%2F&ref_src=twsrc%5Etfw&screen_name=radiotortosa&tw_p=followbutton" target="_blank">Ràdio Tortosa</a></li>
                     <li><i class="icon-twitter"></i>&nbsp;<a href="https://twitter.com/MuseudeTortosa" target="_blank">Museu de Tortosa</a></li>
                     <li><i class="icon-twitter"></i>&nbsp;<a href="https://twitter.com/cucaferatortosa" target="_blank">Turisme</a></li>
                     <li><i class="icon-twitter"></i>&nbsp;<a href="https://twitter.com/vivertortosa" target="_blank">Viver d'empreses</a></li>
                  </ul>
                  <hr height="1px" color="#cccccc">
                  <li>Altres</li>
                  <ul id="altressubpuntinforma">
                     <li><i class="icon-mic"></i>&nbsp;<a href="http://www.radiotortosa.cat/ca/podcasts" target="_blank">Podcasts Ràdio Tortosa</a></li>
                     <li><i class="icon-camera"></i>&nbsp;<a href="https://www.instagram.com/radiotortosa/" target="_blank">Instagram Ràdio Tortosa</a></li>
                     <li><i class="icon-camera"></i>&nbsp;<a href="https://www.instagram.com/cucaferatortosa/" target="_blank">Instagram Turisme</a></li>
                     <li><i class="icon-youtube"></i>&nbsp;<a href="https://www.youtube.com/user/TortosaTurisme" target="_blank">Youtube Turisme</a></li>
                     <li><i class="icon-graduation-cap"></i>&nbsp;<a href="http://agencia.tortosa.cat/formacion" target="_blank">Ofertes de formació i de feina del CFO</a></li>
                  </ul>
               </ul>
            </div>
            </td>
            </tr>
            </table>
            <br><br>
            {/if}
            <br><br><br>
         </div>
      </div>
   </div>
</div>
</div>