<div id="page-wrap">
   <div class="contenedor-responsive">
      <div id="conttranstotalcos" style="padding-bottom: 0">
         <div id="conttranscos">
            <div id="btranspresen">
               <div id="transimatge">        
                  <img src="{$image}" class="img-slider-territori"/>       
               </div>
               <div id="transimatgeText">
                  <h1>
                     {$LABEL_TITOL1}
                     <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                  </h1>
               </div>
            </div>
            <div class="subtitolinfeco" id="subtitolinfeco2">
               {$LABEL_TITOL1b}
            </div>
            <div class="marcback">
               <a href="{$url_sistemes_back}">
               <i class="icon-angle-double-up"></i>
               </a>
            </div>
            <ul id='menu_planA'>
               {foreach $MENU as $itemMenu}
               <a href={$itemMenu}>
                  <li id='menu_planA_item'>
                     {$itemMenu@key}
                  </li>
               </a>
               {/foreach}
            </ul>
            <div id="tcentre_planA" class="queixes_i_suggeriments">
               <p>{$LABEL_TITOL0}</p>
               <div>
                   <p>{$LABEL_TITOL5}<a href="{$LABEL_TITOL5_2}">{$LABEL_TITOL5_1}</a>.</p>
               {if !$request}
               <form id="queixes_i_suggeriments_form" method="post" action="queixes_suggeriments.php?lang={$lang}" novalidate="novalidate" enctype="multipart/form-data">
                   <div class="row_full title">
                       {$LABEL_TITOL2}
                   </div>
                   <div class="field_container">
                       <label>{$LABEL_NOM}:*</label>
                       <input type="text" name="nom_sollicitant" id="nom_sollicitant" placeholder="{$LABEL_NOM}" aria-required="true">
                   </div>
                   <div class="field_container">
                       <label>{$LABEL_COGNOM1}:*</label>
                       <input type="text" name="cognom1_sollicitant" id="cognom1_sollicitant" placeholder="{$LABEL_COGNOM1}" aria-required="true">
                   </div>
                   <div class="field_container">
                       <label>{$LABEL_COGNOM2}:</label>
                       <input type="text" name="cognom2_sollicitant" id="cognom2_sollicitant" placeholder="{$LABEL_COGNOM2}">
                   </div>
                   <div class="field_container full">
                       <label>{$LABEL_RAO_SOCIAL}:</label>
                       <input type="text" name="rao_social_sollicitant" id="rao_social_sollicitant" placeholder="{$LABEL_RAO_SOCIAL}">
                   </div>
                   <div class="new_row"></div>
                   <div class="field_container">
                       <label>{$LABEL_TIPUS_DOCUMENT}:*</label>
                       <select name="tipus_document_sollicitant" id="tipus_document_sollicitant">
                           <option name=""></option>
                           <option name="DNI">DNI</option>
                           <option name="NIF">NIF</option>
                           <option name="Passaport">{$OPTION_TIPUS_DOCUMENT_1}</option>
                       </select>
                   </div>
                   <div class="field_container">
                       <label>{$LABEL_NUMERO_DOCUMENT}:*</label>
                       <input type="text" name="numero_document_sollicitant" id="numero_document_sollicitant" placeholder="{$LABEL_NUMERO_DOCUMENT}">
                   </div>
                   <div class="new_row"></div>
                   <div class="field_container">
                       <label class="tipus_via_sollicitant">{$LABEL_TIPUS_VIA}:*</label>
                       <input type="text" name="tipus_via_sollicitant" id="tipus_via_sollicitant" class="tipus_via_sollicitant" placeholder="{$LABEL_TIPUS_VIA}">
                   </div>
                   <div class="field_container">
                       <label>{$LABEL_NOM_VIA}:*</label>
                       <input type="text" name="nom_via_sollicitant" id="nom_via_sollicitant" placeholder="{$LABEL_NOM_VIA}">
                   </div>
                   <div class="field_container">
                       <label class="numero_sollicitant">{$LABEL_NUMERO}:*</label>
                       <input type="text" name="numero_sollicitant" id="numero_sollicitant" class="numero_sollicitant" placeholder="{$LABEL_NUMERO}">
                   </div>
                   <div class="field_container">
                       <label class="numero_sollicitant">{$LABEL_LLETRA}:*</label>
                       <input type="text" name="lletra_sollicitant" id="lletra_sollicitant" class="lletra_sollicitant" placeholder="{$LABEL_LLETRA}">
                   </div>
                   <div class="field_container">
                       <label class="km_sollicitant">{$LABEL_KM}:</label>
                       <input type="text" name="km_sollicitant" id="km_sollicitant" class="km_sollicitant" placeholder="{$LABEL_KM}">
                   </div>
                   <div class="field_container">
                       <label class="bloc_sollicitant">{$LABEL_BLOC}:</label>
                       <input type="text" name="bloc_sollicitant" id="bloc_sollicitant" class="bloc_sollicitant" placeholder="{$LABEL_BLOC}">
                   </div>
                   <div class="field_container">
                       <label class="escala_sollicitant">{$LABEL_ESCALA}:</label>
                       <input type="text" name="escala_sollicitant" id="escala_sollicitant" class="escala_sollicitant" placeholder="{$LABEL_ESCALA}">
                   </div>
                   <div class="field_container">
                       <label class="pis_sollicitant">{$LABEL_PIS}:</label>
                       <input type="text" name="pis_sollicitant" id="pis_sollicitant" class="pis_sollicitant" placeholder="{$LABEL_PIS}">
                   </div>
                   <div class="field_container">
                       <label class="porta_sollicitant">{$LABEL_PORTA}:</label>
                       <input type="text" name="porta_sollicitant" id="porta_sollicitant" class="porta_sollicitant" placeholder="{$LABEL_PORTA}">
                   </div>
                   <div class="field_container">
                       <label class="nucli_barri_sollicitant">{$LABEL_NUCLI_BARRI}:</label>
                       <input type="text" name="nucli_barri_sollicitant" id="nucli_barri_sollicitant" class="nucli_barri_sollicitant" placeholder="{$LABEL_NUCLI_BARRI}">
                   </div>
                   <div class="field_container">
                       <label class="provincia_sollicitant">{$LABEL_PROVINCIA}:*</label>
                       <input type="text" name="provincia_sollicitant" id="provincia_sollicitant" class="provincia_sollicitant" placeholder="{$LABEL_PROVINCIA}">
                   </div>
                   <div class="field_container">
                       <label class="municipi_sollicitant">{$LABEL_MUNICIPI}:*</label>
                       <input type="text" name="municipi_sollicitant" id="municipi_sollicitant" class="municipi_sollicitant" placeholder="{$LABEL_MUNICIPI}">
                   </div>
                   <div class="field_container">
                       <label class="codi_postal_sollicitant">{$LABEL_CODI_POSTAL}:*</label>
                       <input type="text" name="codi_postal_sollicitant" id="codi_postal_sollicitant" class="codi_postal_sollicitant" placeholder="{$LABEL_CODI_POSTAL}">
                   </div>
                   <div class="field_container">
                       <label class="telefon_fix_sollicitant">{$LABEL_TELEFON_FIX}:</label>
                       <input type="text" name="telefon_fix_sollicitant" id="telefon_fix_sollicitant" class="telefon_fix_sollicitant" placeholder="{$LABEL_TELEFON_FIX}">
                   </div>
                   <div class="field_container">
                       <label class="telefon_mobil_sollicitant">{$LABEL_TELEFON_MOBIL}:</label>
                       <input type="text" name="telefon_mobil_sollicitant" id="telefon_mobil_sollicitant" class="telefon_mobil_sollicitant" placeholder="{$LABEL_TELEFON_MOBIL}">
                   </div>
                   <div class="field_container">
                       <label class="email_sollicitant">{$LABEL_EMAIL}:</label>
                       <input type="text" name="email_sollicitant" id="email_sollicitant" class="email_sollicitant" placeholder="{$LABEL_EMAIL}">
                   </div>
                   <div class="queixes_i_suggeriments_separator">&nbsp;</div>
                   <div class="row_full title">
                       {$LABEL_TITOL3}
                   </div>
                   <div class="field_container">
                       <label>{$LABEL_TIPUS_PETICIO}:*</label>
                       <select name="tipus_peticio_sollicitant" id="tipus_peticio_sollicitant">
                           <option name=""></option>
                           <option name="Consulta">{$OPTION_TIPUS_PETICIO_1}</option>
                           <option name="Queixa">{$OPTION_TIPUS_PETICIO_2}</option>
                           <option name="Suggeriment">{$OPTION_TIPUS_PETICIO_3}</option>
                       </select>
                   </div>
                   <div class="field_container">
                       <label>{$LABEL_AFECTA_A}:*</label>
                       <select name="afecta_a_sollicitant" id="afecta_a_sollicitant">
                           <option name=""></option>
                           <option name="Consulta">{$OPTION_AFECTA_A_1}</option>
                           <option name="Queixa">{$OPTION_AFECTA_A_2}</option>
                           <option name="Infrastructures municipals">{$OPTION_AFECTA_A_3}</option>
                           <option name="Edificis privats">{$OPTION_AFECTA_A_4}</option>
                           <option name="Veïns">{$OPTION_AFECTA_A_5}</option>
                           <option name="Altres">{$OPTION_AFECTA_A_6}</option>
                       </select>
                   </div>
                   <div class="new_row"></div>
                   <div class="field_container full">
                       <label class="descripcio_sollicitant">{$LABEL_DESCRIPCIO}:*</label>
                       <textarea name="descripcio_sollicitant" id="descripcio_sollicitant" class="descripcio_sollicitant" placeholder="{$LABEL_DESCRIPCIO}"></textarea>
                   </div>
                   <div class="field_container full">
                       <div class="g-recaptcha" data-sitekey="6Lfdwh4TAAAAAKajPw-1Rj8msRd8YVZx14Vwova-" data-callback="recaptchaCallback"></div>
                       <label id="recaptcha-error">{$LABEL_ERROR_15}</label>
                   </div>
                   <div class="field_container full">
                       <span>{$FIELD_15} <a href="JavaScript:obripopup();">{$FIELD_16}</a></span> <input type="checkbox" name="accept_privacy_policy" id="accept_privacy_policy" style="float: left">
                       <label id="privacy_policy_error" style="width: 100%; margin-top: 5px; color: #ED1F33; font-size: 12px">{$LABEL_17}</label>
                   </div>
                   <div class="field_container buttons">
                       <input class="submit" type="submit" value="{$BUTTON_SUBMIT}">
                   </div>
               </form>
               {else}
                   {$LABEL_RESPOSTA}
               {/if}
               </div>
            </div>
         </div>
      </div>
   </div>         
</div>
<!-- The Modal -->
<div id="formulari_generic_modal" class="modal">
    <!-- Modal content -->
    <div class="modal-content">
        <span class="close">&times;</span>
        <p>{$FIELD_16}</p>
        <p style="font-size: 10px">{$LABEL_TC5}</p>
        <p>
            <input type="button" name="accept" value="{$LABEL_BUTTON_1}" id="popup_close">
        </p>
    </div>
</div>