<!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->
<div id="page-wrap">

   <div class="contenedor-responsive">
      <div id="conttranstotalcos">
         <div id="conttranscos">
            <div id="btranspresen">
               <div id="transimatge">			
                  <img src="{$image}" class="img-slider-territori"/>			
               </div>
               <div id="transimatgeText">
                  <h1>
                     {$LABEL_TITOL1}
                     <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                  </h1>
               </div>
            </div>
            <div class="subtitolinfeco" id="subtitolinfeco2">
               {$LABEL_TITOL1b}
               
            </div>
            <div class="marcback">
               <a href="{$url_organs_back}">
               <i class="icon-angle-double-up"></i>
               </a>
            </div>
            <ul id='menu_planA'>
               {foreach $MENU as $itemMenu}
               <a href={$itemMenu}>
                  <li id='menu_planA_item'>
                     {$itemMenu@key}
                  </li>
               </a>
               {/foreach}
            </ul>

            <div id="tcentre_planA">
               <font size=4 color=#666666 >
               {$LABEL_TITOL0}
               </font>

               {if $item==1}
               <p>{$LABEL_TC1}</p>
               <p>{$LABEL_TC2}</p>

               {elseif $item==2}

               <!-- Relació organs -->
                  
                  <!-- Consell ciutat -->

                  <a href="http://www.tortosa.cat/webajt/ajunta/participacio/conciu.pdf">
                     <div id="" class="" style="margin-top: 10px;">
                     <i class="icon-users colorBlack"></i>
                        {$LABEL_TC1} {$LABEL_TC4}
                     </div>
                  </a>
                  <br>
                  <!-- /Consell ciutat -->

                  <!-- Consell de pobles -->
                  <a href="http://www.tortosa.cat/webajt/ajunta/participacio/conpob.pdf">
                     <div id="" class="">
                     <i class="icon-users colorBlack"></i>
                        {$LABEL_TC2} {$LABEL_TC5}
                     </div>
                  </a>

                  <ul id="" class="">
                     <li><a href="http://www.tortosa.cat/webajt/ajunta/participacio/ConsellReguers.pdf">Els Reguers</a></li>
                     <li><a href="http://www.tortosa.cat/webajt/ajunta/participacio/ConsellVinallop.pdf">Vinallop</a></li>
                  </ul>
                  <br>

                  <!-- /Consell de pobles -->

                  <!-- Consell de barri -->
                  <a href="http://www.tortosa.cat/webajt/ajunta/participacio/conpob.pdf">
                     <div id="" class="">
                     <i class="icon-users colorBlack"></i>
                        {$LABEL_TC2} {$LABEL_TC6}
                     </div>
                  </a>
                  <br>
                  <!-- /Consell de barri -->

                  <!-- Consells sectorials -->
                  <a href="http://www.tortosa.cat/webajt/ajunta/participacio/consec.pdf">
                     <div id="" class="">
                     <i class="icon-users colorBlack"></i>
                        {$LABEL_TC2} {$LABEL_TC7}
                     </div>
                  </a>

                  <ul id="" class="">
                     <li><a href="/acciosocial/as7.php">{$LABEL_TC9}</a></li>
                     <li><a href="http://www.tortosa.cat/webajt/ajunta/participacio/ConsellEscolar.pdf">{$LABEL_TC10}</a></li>
                     <li><a href="http://www.museudetortosa.cat/museu/consell-assessor">{$LABEL_TC11}</a></li>
                     <li><a href="http://www.tortosa.cat/webajt/ajunta/participacio/ConsellSSocials.pdf">{$LABEL_TC12}</a></li>
                     <li><a href="http://www2.tortosa.cat/lgtbi/index.php">{$LABEL_TC13}</a></li>
                  </ul>

                  <!-- /Consells sectorials -->

                  <!-- Comissions específiques -->
                  <a href="http://www.tortosa.cat/webajt/ajunta/participacio/comes.pdf">
                     <div id="" class="">
                     <i class="icon-users colorBlack"></i>
                        {$LABEL_TC3} {$LABEL_TC8}
                     </div>
                  </a>
                  <!-- /Comissions específiques -->
               
               <!-- /Relació organs -->

               {elseif $item==3}
               <ul>
                  <li>
                     {$LABEL_TC1} de Vinallop
                     <br/>
                     <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/vinallop-acta-25112015.pdf" target="_blanl">2015-1</a>, <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/acta03122015_2.pdf" target="_blanl">2015-2</a>, <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/acta19052016_1.pdf" target="_blanl">2016-1</a>, <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/acta02082016_2.pdf" target="_blanl">2016-2</a>, <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/acta03112016_3.pdf" target="_blanl">2016-3</a>, <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/acta09032017_1.pdf" target="_blanl">2017-1</a>, <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/acta06072017_2.pdf" target="_blanl">2017-2</a>
                  </li>
                  <br><br>
                  <li>
                     {$LABEL_TC1} dels Reguers<br/>
                     <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/acta12016r.pdf" target="_blank">2016/1</a>, <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/acta22016r.pdf" target="_blank">2016/2</a>, <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/acta32017r.pdf" target="_blank">2017/3</a>
                  </li>
                  <br><br>
                  <li>
                     {$LABEL_TC1} {$LABEL_TC11}<br/>
                     <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/acta-consell-assessor-museu-30-09-13.pdf" target="_blank">2013</a>, <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/acta-consell-assessor-museu-27-02-14.pdf" target="_blank">2014</a>, <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/acta-consell-assessor-museu-10-02-15.pdf" target="_blank">2015</a>, <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/CCE15032017_0001.pdf" target="_blank">2016</a>, <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/ConsellAssessorMuseu27-01-17.pdf" target="_blank">2017</a> 
                  </li>
                  <br><br>
                  <li>
                     {$LABEL_TC1} {$LABEL_TC10}<br/>
                     <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/acta-reunio-26022010.pdf" target="_blank">2010/1</a>,
                     <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/acta-reunio-11062010.pdf" target="_blank">2010/2</a>,
                     <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/acta-reunio-09062011.pdf" target="_blank">2011</a>,
                     <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/acta-reunio-21062012.pdf" target="_blank">2012</a>,
                     <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/acta-reunio-09042014.pdf" target="_blank">2014</a>
                     (<a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/annex-1-2014.pdf" target="_blank">Annex 1</a>, <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/annex-2-2014.pdf" target="_blank">Annex 2</a>,
                     <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/annex-3-2014.pdf" target="_blank">Annex 3</a>),
                     <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/acta-reunio-28052015.pdf" target="_blank">2015</a>,
                     <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/acta-reunio-CEM-11-05-2016.pdf" target="_blank">2016</a>,
                     <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/CEM14-06-2017.pdf" target="_blank">2017</a>
                  </li>
                  <br><br>
                  <li>
                     {$LABEL_TC1} {$LABEL_TC12}<br/>
                     <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/Acta1_110516_ServeisSocials.pdf" target="_blank">2016/1</a>
                     <a href="http://www.tortosa.cat/webajt/ajunta/participacio/actes/Acta2_021216_ServeisSocials.pdf" target="_blank">2016/2</a>
                  </li>
               </ul>
               {/if}

               <br><br><br>
            </div>

         </div>
      </div>
   </div>
</div>