<!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->
<div id="page-wrap">

   <div class="contenedor-responsive">
      <div id="conttranstotalcos">
         <div id="conttranscos">
            <div id="btranspresen">
               <div id="transimatge">     
                  <img src="{$image}" class="img-slider-territori"/>      
               </div>
               <div id="transimatgeText">
                  <h1>
                     {$LABEL_TITOL1}
                     <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                  </h1>
               </div>
            </div>
            <div class="subtitolinfeco" id="subtitolinfeco2">
               {$LABEL_TITOL1b}
            </div>
            <div class="marcback">
               <a href="../presparticipa.php">
               <i class="icon-angle-double-up"></i>
               </a>
            </div>
            <ul id='menu_planA'>
               {foreach $MENU as $itemMenu}
               <a href={$itemMenu}>
                  <li id='menu_planA_item'>
                     {$itemMenu@key}
                  </li>
               </a>
               {/foreach}
            </ul>
            <div id="tcentre_planA">
               <font size=4 color=#666666>
               {$LABEL_TITOL0}
               </font>
               {if $item==1}
                  <p>{$LABEL_TC1}</p>
                  <p>{$LABEL_TC2}</p>
               {elseif $item==2}
                  <ul style="list-style: circle">
                     <li><strong>{$LABEL_TC1t1}</strong><br>
                        {$LABEL_TC1c1}<br><br>
                     </li>
                     <li><strong>{$LABEL_TC1t2}</strong><br>
                        {$LABEL_TC1c2}<br><br>
                     </li>
                     <li><strong>{$LABEL_TC1t3}</strong><br>
                        <!--{$LABEL_TC1c3}<br>
                        {$LABEL_TC1c3b}<br>
                        {$LABEL_TC1c3c}<br><br>-->
                        {$LABEL_TC1c30}<br><br>
                     </li>
                     <li><strong>{$LABEL_TC1t4}</strong><br><br></li>
                     <li><strong>{$LABEL_TC1t5}</strong><br>
                        {$LABEL_TC1c5}<br>
                        {$LABEL_TC1c5b}<br><br>
                     </li>
                     <li><strong>{$LABEL_TC1t6}</strong><br>
                        <!--{$LABEL_TC1c6}<br><br>-->
                        {$LABEL_TC1c60}<br><br>
                     </li>
                  </ul>
                  <img src="/images/pciutadana/fases18.jpg" class="img-responsive">
               {elseif $item==3}
                  <ul style="list-style: circle">
                     <li>
                        {$LABEL_TC2u1}<br><br>
                        <ul>
                           <li><a href="http://www.tortosa.cat/webajt/ajunta/consulta/pressupostos_participatius_2018/guia-pressupostos-participatius-tortosa-2018.pdf" target="_blank">{$LABEL_TC2u11}</a></li>
                           <!--<li><a href="http://www.tortosa.cat/webajt/ajunta/consulta/pressupostos_participatius_2017/presentacio-pressupostos-participatius-2017.pdf" target="_blank">{$LABEL_TC2u12}</a></li>-->
                        </ul>
                        <br><br>
                     </li>

                  </ul>
               {elseif $item==4}  
                  <div class="faqconsultapopularMod" id="faqcons1">
                     <div class="titol_proposta">Per què fem els Pressupostos Participatius 2018?</div>
                     Els Pressupostos Participatius són un mecanisme de participació i gestió del municipi, mitjançant el qual la ciutadania pot proposar i decidir sobre la destinació d’una part dels recursos municipals per a inversions del Pressupost 2018 establint així un canal efectiu de democràcia participativa.        
                  </div>
                  <br>
                  <div class="faqconsultapopularMod" id="faqcons2">
                     <div class="titol_proposta">Com puc obtenir més informació?</div>
                     Al web de l’Ajuntament de Tortosa (<a href="http://www.tortosa.cat" target="_blank">www.tortosa.cat</a>).<br/> 
                     Al telèfon 977 585 805.<br/> 
                     Per correu electrònic <a href="mailto:sac.tortosa@tortosa.cat">sac.tortosa@tortosa.cat</a>.<br/>
                     Presencialment al Servei d’Atenció al Ciutadana (SAC), en horari de dilluns a divendres de 9h a 14h, i dijous de 16.30h a 18.30h.        
                  </div>
                  <br>
                  <div class="faqconsultapopularMod" id="faqcons1">
                     <div class="titol_proposta">Com puc participar?</div>
                     Podeu participar de les següents formes:<br/>
                     - informant-vos a la sessió de presentació.<br/>
                     - presentant el formulari de les propostes, presencialment o via online.<br/>
                     - participant en la sessió de debat.<br/>
                     - participant en la votació.<br/>
                     - assistint a la sessió de tancament, on es retrà compte del resultat.        
                  </div>
                  <br>
                  <div class="faqconsultapopularMod" id="faqcons2">
                     <div class="titol_proposta">Qui pot fer propostes?</div>
                     Podran fer propostes totes aquelles persones residents i/o empadronades al municipi de Tortosa.        
                  </div>
                  <br>
                  <div class="faqconsultapopularMod" id="faqcons1">
                     <div class="titol_proposta">Quantes propostes puc fer?</div>
                     Cada persona podrà presentar un únic formulari amb un màxim de 3 propostes.        
                  </div>
                  <br>
                  <div class="faqconsultapopularMod" id="faqcons2">
                     <div class="titol_proposta">Criteris que han de complir les propostes.</div>
                     <ol>
                        <li>Que facin referència als nuclis de Tortosa, els Reguers i Vinallop. S’exceptuen els àmbits territorials de les EMD, ateses les competències de les quals disposen.</li>
                        <li>Que siguin legals.</li>
                        <li>Que facin referència a competències directes i assumides per l’Ajuntament.</li>
                        <li>
                           Que no tinguin un cost superior a:<br/>
                           - Tortosa ciutat: 170.000€.<br/>
                           - Els Reguers: 15.000€.<br/>
                           - Vinallop: 15.000€.                
                        </li>
                        <li>Que siguin concretes i avaluables econòmicament.</li>
                        <li>Que tinguin la consideració d’inversions.</li>
                        <li>Que siguin viables tècnicament, és a dir, que no existeixin impediments tècnics per a executar la proposta i que aquesta s’ajusti a la normativa jurídica vigent, en base a les valoracions realitzades pel personal tècnic municipal.</li>
                        <li>Que no plantegin accions insostenibles, és a dir, que no comprometin les necessitats i les possibilitats de desenvolupament de les generacions presents i futures.</li>
                        <li>Que no plantegin accions d’exclusió social.</li>
                        <li>Que siguin d’interès general.</li>
                        <li>Que les propostes incorporin la seva localització.</li>
                        <li>Que vagin acompanyades de les dades identificatives de la persona que les realitza (nom i cognoms, telèfon de contacte i correu electrònic, si se’n disposa), per tal de poder aclarir possibles dubtes.</li>
                     </ol>
                  </div>
                  <br>
                  <div class="faqconsultapopularMod" id="faqcons1">
                     <div class="titol_proposta">Com i on presento les propostes?</div>
                     <p>
                        - <a href="http://lsv206.gabinetceres.com/index.php?r=survey/index&sid=875889&lang=ca" target="_blank">Accedint al formulari online</a>.          
                     </p>
                     <p>
                        - Presencialment: Omplint la butlleta de propostes i presentant-la a les oficines d'atenció a la ciutadania de l'Ajuntament de Tortosa, d'Els Reguers, de Vinallop, de l'EMD de Bítem, de l'EMD de Campredó, de l'EMD de Jesús. També a la Biblioteca municipal Marcel·lí Domingo i al Centre Cívic de Ferreries. Les butlletes les podeu trobar als mateixos llocs, o podeu descarregar-la i imprimir-la des del següent enllaç: <a href="http://www.tortosa.cat/webajt/ajunta/consulta/pressupostos_participatius_2018/formulari-web-pressupostos-participatius-2018.pdf" target="_blank">Formulari en format pdf</a>.          
                     </p>
                  </div>
                  <br>
                  <div class="faqconsultapopularMod" id="faqcons2">
                     <div class="titol_proposta">Quan puc presentar les propostes?</div>
                     El termini durant el qual es poden presentar les propostes és del 5 al 16 de març, ambdós inclosos, en els horaris habituals d'atenció al públic.        
                  </div>
                  <br>
                  <div class="faqconsultapopularMod" id="faqcons1">
                     <div class="titol_proposta">Què és la valoració tècnica i qui la fa?</div>
                     Els serveis tècnics municipals realitzaran la valoració en base als criteris, la viabilitat tècnica i econòmica de les propostes auxí com, el compliment dels criteris establerts a la guia.<br/>
                     Una vegada valorades es divulgaran a través dels mitjans disponibles a nivell municipal, per tal que la ciutadania en tingui coneixement i pugui votar-les posteriorment.        
                  </div>
                  <br>
                  <div class="faqconsultapopularMod" id="faqcons1">
                     <div class="titol_proposta">Quines propostes es podran votar?</div>
                     Totes les que s'hagin formulat i compleixin els criteris establerts.        
                  </div>
                  <br>
                  <div class="faqconsultapopularMod" id="faqcons2">
                     <div class="titol_proposta">Qui pot votar?</div>
                     Podran fer-ho totes aquelles persones que a data 31 de desembre de 2017 tinguin 16 anys o més i constin, en aquella data, empadronades al municipi de Tortosa.        
                  </div>
                  <br>
                  <div class="faqconsultapopularMod" id="faqcons2">
                     <div class="titol_proposta">Quantes propostes puc escollir?</div>
                     Cada persona només podrà votar un cop.<br/>
                     Es podrà escollir fins un màxim de tres propostes.        
                  </div>
                  <br>
                  <div class="faqconsultapopularMod" id="faqcons1">
                     <div class="titol_proposta">Quan i on es podrà votar?</div>
                     La votació es realitzarà de forma presencial i acreditant la identitat. (DNI, NIE, passaport o carnet de conduir).<br/>
                     Les dates de votació seran:<br/><br/>
                     - del 28 d’abril a l’1 de maig en el marc de la Fira ExpoEbre, en un estand habilitat per a l’ocasió.<br/><br/><br/>
                     - del 2 a l'11 de maig
                     <br/><br/>
                     <table width="100%" border="1" cellspacing="5" bordercolor="#eeeeee" background="#ffffff" id="faqcons">
                        <tr>
                           <th>Ajuntament de Tortosa</th>
                           <th class="adreca-sac">Servei d'Atenció Ciutadana</th>
                        </tr>
                        <tr>
                           <td valign="middle" colspan="2">- de dilluns a divendres de 9 a 14 hores.</td>
                        </tr>
                        <tr>
                           <td valign="middle" colspan="2">- dijous de 17 a 20 hores.</td>
                        </tr>
                        <tr>
                           <td colspan="2" class="empty">&nbsp</td>
                        </tr>
                        <tr>
                           <th>EMD Bítem</th>
                           <th class="adreca-sac">Servei d'Atenció Ciutadana</th>
                        </tr>
                        <tr>
                           <td valign="middle" colspan="2">- de dilluns a divendres de 8 a 14 hores.</td>
                        </tr>
                        <tr>
                           <td valign="middle" colspan="2">- dimarts i dimecres de 16 a 20 hores.</td>
                        </tr>
                        <tr>
                           <td colspan="2" class="empty">&nbsp</td>
                        </tr>
                        <tr>
                           <th>EMD Campredó</th>
                           <th class="adreca-sac">Servei d'Atenció Ciutadana</th>
                        </tr>
                        <tr>
                           <td valign="middle" colspan="2">- de dilluns a divendres de 11 a 14 hores.</td>
                        </tr>
                        <tr>
                           <td valign="middle" colspan="2">- dimarts i dijous de 16:30 a 19 hores.</td>
                        </tr>
                        <tr>
                           <td colspan="2" class="empty">&nbsp</td>
                        </tr>
                        <tr>
                           <th>
                              EMD Jesús                
                           </th>
                           <th class="adreca-sac">Servei d'Atenció Ciutadana</th>
                        </tr>
                        <tr>
                           <td valign="middle" colspan="2">- de dilluns a divendres de 9 a 14 hores.</td>
                        </tr>
                        <tr>
                           <td valign="middle" colspan="2">- dijous de 18 a 19 hores.</td>
                        </tr>
                        <tr>
                           <td colspan="2" class="empty">&nbsp</td>
                        </tr>
                        <tr>
                           <th>
                              Els Reguers                
                           </th>
                           <th class="adreca-sac">Oficines Municipals</th>
                        </tr>
                        <tr>
                           <td valign="middle" colspan="2">- de dimarts a divendres de 9 a 14 hores.</td>
                        </tr>
                        <tr>
                           <td colspan="2" class="empty">&nbsp</td>
                        </tr>
                        <tr>
                           <th>
                              Vinallop                
                           </th>
                           <th class="adreca-sac">Oficines Municipals</th>
                        </tr>
                        <tr>
                           <td valign="middle" colspan="2">- dimecres de 17 a 20 hores.</td>
                        </tr>
                     </table>
                     <br><br>
                     * En aquesta edició es planteja oferir la possibilitat del vot telemàtic, sempre i quan el temps, els recursos i mitjans tècnics disponibles permetin fer-ho amb les garanties adequades. Cas que s’ofereixi, els detalls del vot telemàtic es publicaran al web municipal i se’n farà la difusió corresponent.

                     
                  </div>
                  <br>
                  <div class="faqconsultapopularMod" id="faqcons1">
                     <div class="titol_proposta">Quan es coneixeran els resultats de la votació?</div>
                     Setmana del 14 de maig.<br/>
                     Els resultats de la votació es donaran a conèixer pels canals habituals d'informació.        
                  </div>
               {elseif $item==5} 
                  <p>{$LABEL_INTRO1}</p>
                  <p>{$LABEL_INTRO2}</p>
                  <br>
                  <p><font size=3 color="#389695">{$LABEL_TITOL3}</font></p>
                  <ul style="list-style: circle"> 
                     
                           <li><a href="https://www2.tortosa.cat/public_files/pressupostos-participatius/ResultatVT2018.pdf" target="_blank">{$LABEL_TC2u31}</a></li>
                           <li><a href="https://www2.tortosa.cat/public_files/pressupostos-participatius/LlistaPropostes2018.pdf" target="_blank">{$LABEL_TC2u32}</a></li>
                        
                  </ul>
               {elseif $item==6}
                  <p>{$LABEL_INTRO1}</p>
                  <p>{$LABEL_INTRO2}</p>
                  <p>{$LABEL_INTRO3}</p>
                  <p>{$LABEL_INTRO4}</p>
                  <p>{$LABEL_INTRO5}</p>
                  <ul>
                     <li>{$LABEL_INTRO5a}</li><br>
                     <li>{$LABEL_INTRO5b}</li>
                  </ul>
                  <br/><br/>
                     <table width="100%" border="1" cellspacing="5" bordercolor="#eeeeee" background="#ffffff" id="faqcons">
                        <tr>
                           <th>Ajuntament de Tortosa</th>
                           <th class="adreca-sac">Servei d'Atenció Ciutadana</th>
                        </tr>
                        <tr>
                           <td valign="middle" colspan="2">- de dilluns a divendres de 9 a 14 hores.</td>
                        </tr>
                        <tr>
                           <td valign="middle" colspan="2">- dijous de 17 a 20 hores.</td>
                        </tr>
                        <tr>
                           <td colspan="2" class="empty">&nbsp</td>
                        </tr>
                        <tr>
                           <th>EMD Bítem</th>
                           <th class="adreca-sac">Servei d'Atenció Ciutadana</th>
                        </tr>
                        <tr>
                           <td valign="middle" colspan="2">- de dilluns a divendres de 8 a 14 hores.</td>
                        </tr>
                        <tr>
                           <td valign="middle" colspan="2">- dimarts i dimecres de 16 a 20 hores.</td>
                        </tr>
                        <tr>
                           <td colspan="2" class="empty">&nbsp</td>
                        </tr>
                        <tr>
                           <th>EMD Campredó</th>
                           <th class="adreca-sac">Servei d'Atenció Ciutadana</th>
                        </tr>
                        <tr>
                           <td valign="middle" colspan="2">- de dilluns a divendres de 11 a 14 hores.</td>
                        </tr>
                        <tr>
                           <td valign="middle" colspan="2">- dimarts i dijous de 16:30 a 19 hores.</td>
                        </tr>
                        <tr>
                           <td colspan="2" class="empty">&nbsp</td>
                        </tr>
                        <tr>
                           <th>
                              EMD Jesús                
                           </th>
                           <th class="adreca-sac">Servei d'Atenció Ciutadana</th>
                        </tr>
                        <tr>
                           <td valign="middle" colspan="2">- de dilluns a divendres de 9 a 14 hores.</td>
                        </tr>
                        <tr>
                           <td valign="middle" colspan="2">- dijous de 18 a 19 hores.</td>
                        </tr>
                        <tr>
                           <td colspan="2" class="empty">&nbsp</td>
                        </tr>
                        <tr>
                           <th>
                              Els Reguers                
                           </th>
                           <th class="adreca-sac">Oficines Municipals</th>
                        </tr>
                        <tr>
                           <td valign="middle" colspan="2">- de dimarts a divendres de 9 a 14 hores.</td>
                        </tr>
                        <tr>
                           <td colspan="2" class="empty">&nbsp</td>
                        </tr>
                        <tr>
                           <th>
                              Vinallop                
                           </th>
                           <th class="adreca-sac">Oficines Municipals</th>
                        </tr>
                        <tr>
                           <td valign="middle" colspan="2">- dimecres de 17 a 20 hores.</td>
                        </tr>
                     </table>
                     <br><br>
                  <p>{$LABEL_INTRO6}</p>
                  

                  <!--<ul style="list-style: circle">   
                     
                           <li><a href="http://www.tortosa.cat/webajt/ajunta/consulta/pressupostos_participatius_2017/PROPOSTESOK.pdf" target="_blank">{$LABEL_TC2u41}</a></li>
                           <li><a href="http://www.tortosa.cat/webajt/ajunta/consulta/pressupostos_participatius_2017/quicomon.pdf" target="_blank">{$LABEL_TC2u42}</a></li>
                           <li><a href="http://www2.tortosa.cat/noticies/noticia.php?lang=ca&id=5976" target="_blank">{$LABEL_TC2u43}</a></li>
                        
                  </ul>-->
               {elseif $item==7}  
                  <p>{$LABEL_INTRO1}</p>
                  <!--<ul style="list-style: circle">    
                     
                           <li><a href="http://www.tortosa.cat/webajt/ajunta/consulta/pressupostos_participatius_2017/NP17.pdf" target="_blank">{$LABEL_TC2u51}</a></li>
                           <li><a href="http://www.tortosa.cat/webajt/ajunta/consulta/pressupostos_participatius_2017/presentacio17.pdf" target="_blank">{$LABEL_TC2u52}</a></li>
                           <li><a href="http://www.tortosa.cat/webajt/ajunta/consulta/pressupostos_participatius_2017/resultats2017.pdf" target="_blank">{$LABEL_TC2u53}</a></li>
                        
                  </ul>
                  <div>
                     <img src="/images/pciutadana/resultats-tortosa-2017.png" style="clear: both; max-width: 565px; margin-bottom: 25px;">
                     <img src="/images/pciutadana/resultats-reguers-2017.png" style="clear: both; max-width: 565px;margin-bottom: 25px;">
                     <img src="/images/pciutadana/resultats-vinallop-2017.png" style="clear: both; max-width: 565px;margin-bottom: 25px;">
                  </div> -->
               {elseif $item==0} 
                  <p>{$LABEL_INTRO1}</p>
                  <p>{$LABEL_INTRO1a}</p> 
                  <p>{$LABEL_INTRO1b}</p>
                  <p>{$LABEL_INTRO2}</p>
                  <ul>
                        <li>{$LABEL_TC2u22}</li>
                  </ul> 
                  <br>              
                  <p>{$LABEL_INTRO3}</p>
                  <ul>
                        <li>
                           {$LABEL_INTRO31}<br><br>
                           {$LABEL_INTRO32}
                           <a href="http://www.tortosa.cat/webajt/ajunta/consulta/pressupostos_participatius_2018/formulari-web-pressupostos-participatius-2018.pdf" target="_blank">{$LABEL_TC2u21}</a>
                        </li>
                  </ul>
               {elseif $item==9}
                   <!-- Propostes -->

               {elseif $item==10}
                   <!-- Votació Online -->
                   
               {/if}
            </div>
         </div>
      </div>
   </div>
</div>