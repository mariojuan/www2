<html>
	<head>
        {include file="head.tpl"}
        <link rel="stylesheet" href="/css/prettyPhoto.css">
        <script type="text/javascript" src="/js/imagesloaded.pkgd.min.js"></script>
        <script type="text/javascript" src="/js/jquery.prettyPhoto.js"></script>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function(){
                $("a[rel^='prettyPhoto']").prettyPhoto({
                    allow_resize
                });
        </script>
        <style>
            div.thumbnail_fotos_actuacio {
                width: 100%;
                display: table-row;
            }
            div.thumbnail_fotos_actuacio div {
                display: table-cell;
                padding-right: 5px;
            }
            div.thumbnail_fotos_actuacio div img {
                width: 125px;
                margin-top: 10px;
                margin-bottom: 20px;
            }
        </style>
	</head>
	<body>
		{include file="header.tpl"}
		{include file="pciutadana/2018/prespar_home.tpl" image="/images/pciutadana/tira18.jpg" ntitol=2}
		{include file="footer.tpl"}
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function(){
                $("a[rel^='prettyPhoto']").prettyPhoto();
            });
        </script>
	</body>
</html>