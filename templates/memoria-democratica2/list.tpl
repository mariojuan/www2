<html>
<head>
    {include file="head.tpl"}
</head>
<body>
{include file="header.tpl"}
<div id="page-wrap">
    <div id="conttranstotalcos">
        <div id="conttranscos">
            <div id="btranspresen">
                <div id="transimatge">
                    <img src="/images/laciutat/municipi/tira.jpg" class="img-slider-territori"/>
                </div>
                <div id="transimatgeText">
                    <h1>
                        {$LABEL_TITOL1}
                    </h1>
                </div>
            </div>
            <ul id='menu_planA'>
                {foreach $MENU as $itemMenu}
                    <a href={$itemMenu}>
                        <li id='menu_planA_item'>
                            {$itemMenu@key}
                        </li>
                    </a>
                {/foreach}
            </ul>
            <div id="tcentre_planA" class="memoria-democratica">
                <h2>{$LABEL_TITOL1}</h2>
                <div>
                    <form name="cerca" id="cerca" action="base-dades.php?lang={$lang}" method="post">
                        <input type="hidden" name="accio" id="accio" value="list">
                        <ul>
                            <li>
                                <label>{$lblName}</label>
                                <input type="text" name="nom" id="nom" value=""">
                            </li>
                            <li>
                                <label>{$lblCategory}</label>
                                <select name="categoria" id="categoria">
                                    <option value="">-- {$lblSelect} --</option>
                                    {foreach $categories as $category}
                                    <option value="{$category->CATEGORIA}">{$category->CATEGORIA}</th>
                                        {/foreach}
                                </select>
                            </li>
                            <li id="buttons_container">
                                <input type="submit" name="consultar" id="consultar" value="{$lblConsult}">
                                <input type="reset" name="esborrar" id="esborrar" value="{$lblDelete}">
                            </li>
                        </ul>
                    </form>
                </div>
                <table name="list-items" id="list-items">
                    <tr>
                        <th>{$lblName}</th>
                        <th>{$lblCategory}</th>
                        <th>{$lblSeeItem}</th>
                    </tr>
                    {foreach $items as $item}
                        <tr>
                            <td>{$item->NOM}</td>
                            <td>{$item->CATEGORIA}</td>
                            <td class="link_item"><a href="/memoria-democratica/base-dades.php?lang={$lang}&accio=file&id={$item->ID}&page={$page}">[+]</a></td>
                        </tr>
                    {/foreach}
                </table>
                <div id="pagination">
                    <ul>
                        {if $lastpage>1}
                            {if $page> 1}
                                {assign var="page_ant" value=$page - 1}
                                <li><a href="/memoria-democratica/base-dades.php?lang={$lang}&accio=list&page={$page_ant}">{$lblAnt}</a></li>
                            {/if}
                            {assign var="desp" value=$page + 4}
                            {assign var="firstpage" value=$page - 4}

                            {if $desp < $lastpage}
                                {assign var="lastpage" value=$page + 4}
                            {/if}
                            {if $firstpage < 1}
                                {assign var="firstpage" value=1}
                            {/if}

                            {for $counter=$firstpage to $lastpage}
                                {if $counter==$page}
                                    <li>{$counter}</li>
                                {else}
                                    <li><a href="/memoria-democratica/base-dades.php?lang={$lang}&accio=list&page={$counter}">{$counter}</a></li>
                                {/if}
                            {/for}
                            {if $page < $lastpage}
                                {assign var="page_seg" value=$page + 1}
                                <li><a href="/memoria-democratica/base-dades.php?lang={$lang}&accio=list&page={$page_seg}">{$lblSeg}</a></li>
                            {/if}
                        {/if}
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
{include file="footer.tpl"}
</body>
</html>