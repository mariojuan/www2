<html>
<head>
    {include file="head.tpl"}
</head>
<body>
{include file="header.tpl"}
<div id="page-wrap">
    <div id="conttranstotalcos">
        <div id="conttranscos">
            <div id="btranspresen">
                <div id="transimatge">
                    <img src="/images/laciutat/municipi/tira.jpg" class="img-slider-territori"/>
                </div>
                <div id="transimatgeText">
                    <h1>
                        {$LABEL_TITOL1}
                    </h1>
                </div>
            </div>
            <ul id='menu_planA'>
                {foreach $MENU as $itemMenu}
                    <a href={$itemMenu}>
                        <li id='menu_planA_item'>
                            {$itemMenu@key}
                        </li>
                    </a>
                {/foreach}
            </ul>
            <div id="tcentre_planA" class="memoria-democratica">
                <h2>{$LABEL_TITOL1}</h2>
                <table id="item" name="item">
                    <tr>
                        <td>{$lblName}</td>
                        <td>{$item->NOM}</td>
                    </tr>
                    <tr>
                        <td>{$lblCause}</td>
                        <td>{$item->CAUSA}</td>
                    </tr>
                    <tr>
                        <td>{$lblPlace}</td>
                        <td>{$item->LLOC}</td>
                    </tr>
                    <tr>
                        <td>{$lblAge}</td>
                        <td>{$item->EDAT}</td>
                    </tr>
                    <tr>
                        <td>{$lblPlaceOfBirth}</td>
                        <td>{$item->LLOC_NAIXEMENT}</td>
                    </tr>
                    <tr>
                        <td>{$lblFiliation}</td>
                        <td>{$item->FILIACIO}</td>
                    </tr>
                    <tr>
                        <td>{$lblProfession}</td>
                        <td>{$item->Professio}</td>
                    </tr>
                    <tr>
                        <td>{$lblCivilStatus}</td>
                        <td>{$item->ESTAT_CIVIL}</td>
                    </tr>
                    <tr>
                        <td>{$lblCivilRegistration}</td>
                        <td>{$item->REG_CIVIL}</td>
                    </tr>
                    <tr>
                        <td>{$lblOrigin}</td>
                        <td>{$item->ORIGEN_INFORMACIO}</td>
                    </tr>
                    <tr>
                        <td>{$lblObservations}</td>
                        <td>{$item->OBSERVACIONS}</td>
                    </tr>
                </table>
                <div id="buttons-container">
                    <input type="button" name="enrere" id="enrere" value="{$lblBack}" onclick="JavaScript:window.location.href='/memoria-democratica/base-dades.php?lang={$lang}&accio=list&page={$page}';">
                </div>
            </div>
        </div>
    </div>
</div>
{include file="footer.tpl"}
</body>
</html>


