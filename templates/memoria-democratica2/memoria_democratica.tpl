<html>
    <head>
        {include file="head.tpl"}
    </head>
	<body>
		{include file="header.tpl"}
        <div id="page-wrap">

        <div class="contenedor-responsive">

            <div id="conttranstotalcos">
                <div id="conttranscos">
                    <div id="btranspresen">
                        <div id="transimatge">
                            <img src="/images/laciutat/municipi/tira.jpg" class="img-slider-territori"/>
                        </div>
                        <div id="transimatgeText">
                            <h1>
                                {$LABEL_TITOL1}
                                <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                            </h1>
                        </div>
                    </div>
                    <ul id='menu_planA'>
                        {foreach $MENU as $itemMenu}
                            <a href={$itemMenu}>
                                <li id='menu_planA_item'>
                                    {$itemMenu@key}
                                </li>
                            </a>
                        {/foreach}
                    </ul>
                    <div id="tcentre_planA">
                        <h2>{$LABEL_TITOL2}</h2>
                        <p>{$LABEL_TC1}</p>
                        <p>{$LABEL_TC2}</p>
                        <ul>
                            <li>{$LABEL_TC3}</li>
                            <li>{$LABEL_TC4}</li>
                            <li>{$LABEL_TC5}</li>
                        </ul>
                        <p>{$LABEL_TC6}</p>
                        <ul>
                            <li>{$LABEL_TC7}</li>
                            <li>{$LABEL_TC8}</li>
                            <li>{$LABEL_TC9}</li>
                            <li>{$LABEL_TC10}</li>
                            <li>{$LABEL_TC11}</li>
                            <li>{$LABEL_TC12}</li>
                            <li>{$LABEL_TC13}</li>
                        </ul>
                        <p>{$LABEL_TC14}</p>
                    </div>
                </div>
            </div>
            </div>
        </div>
		{include file="footer.tpl"}
	</body>
</html>