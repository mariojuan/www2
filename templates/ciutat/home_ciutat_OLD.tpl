<div id="page-wrap" class="ciutat">
<div id="contciutattotalcos">
	<div id="contciutatcos">
		<div id="bciutatpresen">
			<ul id="bciutatmenu">
				<li >
				<a href="{$url_municipi}">
				<div id="ciutatbotoimatge">
					<img src="/images/botons/bmunicipi.jpg" />									
				</div>
				<div id="textbmenu1" class="textbotomciutat">
					{$LABEL_MENUV1A}
				</div>
				</a>
				</li>
				<li >
				<a href="{$url_estadistica_habitants}">
				<div id="ciutatbotoimatge">
					<img src="/images/botons/besta.jpg" />									
				</div>
				<div id="textbmenu2" class="textbotomciutat">
					{$LABEL_MENUV3}
					<br>
					{$LABEL_MENUV3B}
				</div>
				</a>
				</li>
				<li>
				<a href="{$url_imatges}">
				<div id="ciutatbotoimatge">
					<img src="/images/botons/bimatges.jpg" />									
				</div>
				<div id="textbmenu3" class="textbotomciutat">
					{$LABEL_MENUV1}
					<br>
					{$LABEL_MENUV1B}
				</div>
				</a>
				</li>
				<li >
				<a href="{$url_territori}">
				<div id="ciutatbotoimatge">
					<img src="/images/botons/bterritori.jpg" />									
				</div>
				<div id="textbmenu4" class="textbotomciutat">
					{$LABEL_MENUV2A}	
				</div>
				</a>
				</li>
			</ul>
			<div id="ciutatimatge">			
			<img src="/images/laciutat/laciutat.jpg" />			
			</div>
			<div id="ciutatimatgeText" class="imatgeCapText">
				<h1>
				{$LABEL_TITOL}&nbsp;&nbsp;
				</h1>
			</div>
		</div>
		<div class="marcfinestra" id="marcbciutata">
			<div id="banner4ima">
					<a href="http://agenda.tortosa.cat/agenda-tortosa/" target="_blank"><img src="/images/banners/bnagenda2.jpg" title="Agenda" alt="Agenda"></a>			
			</div>
			<div class="banners4text" id="bannerlateraltext">
					{$LABEL_BAGENDAC}
			</div>
		</div>
		<div class="marcfinestra" id="marcbciutatb">
			<div id="banner4ima">
					<a href="http://www.tortosa.altanet.org/imact/agenda309.pdf" target="_blank"><img src="/images/banners/bnagecul.jpg" title="Agenda Cultural" alt="Agenda Cultural"></a>			
			</div>
			<div class="banners4text" id="bannerlateraltext">
					{if $lang=='en'}
						{$LABEL_BAGENDACB}
					{else}
						{$LABEL_BAGENDAC}
					{/if}
			</div>
			<div class="banners4text" id="banners4textp">
					{if $lang=='en'}
						{$LABEL_BAGENDAC}
					{else}
						{$LABEL_BAGENDACB}
					{/if}
			</div>
		</div>
		<div class="marcfinestra" id="marcbciutatc">
			<div id="banner4ima">
					<a href="http://www.museudetortosa.cat" target="_blank"><img src="/images/banners/bnmuseuf.jpg" title="Museu de Tortosa" alt="Museu de Tortosa"></a>			
			</div>
			<div class="banners4text" id="bannerlateraltext">
					{$LABEL_BMUSEUC}
			</div>
			<div class="banners4text" id="banners4textp">
					{$LABEL_BMUSEUCB}
			</div>
		</div>
		<div class="marcfinestra" id="marcbciutatd">
			<div id="banner4ima">
					<a href="http://www.teatreauditoritortosa.cat" target="_blank"><img src="/images/banners/bnteatref.jpg" title="Teatre Auditori Felip Pedrell" alt="Teatre Auditori Felip Pedrell"></a>			
			</div>
			<div class="banners4text" id="bannerlateraltext">
					{$LABEL_BTEATREC}
			</div>
			<div class="banners4text" id="banners4textp">
					{$LABEL_BTEATRECB}
			</div>
		</div>
		<div class="marcfinestra" id="marcbciutate">
			<div id="banner4ima">
					<a href="http://www.radiotortosa.cat" target="_blank"><img src="/images/banners/bnradiot.jpg" title="Ràdio Tortosa" alt="Ràdio Tortosa"></a>			
			</div>
			<div class="banners4text" id="bannerlateraltext">
					{$LABEL_BRADIOC}
			</div>
			<div class="banners4text" id="banners4textp">
					{$LABEL_BRADIOB}
			</div>
		</div>
		
		<div id="titolServeis" class="titolarea2">
			{$LABEL_TITOLLM}
		</div>
		<!--<div id="titolMiscel" class="titolarea2">
			&nbsp;
			{$LABEL_TITOLS1B}
		</div>-->
		<a href="{$url_cultura}" target="_self" title="{$LABEL_BCIUTAT1}">
		<div id="bciutat1" class="marcfinestra2 bciutat">
				<img src="/images/botons/bciutatcul.jpg">
				<div class="banners3text" id="tciutat">
					{$LABEL_BCIUTAT1}	
				</div>					
		</div>
		</a>
		<a href="http://tortosasport.cat/" target="_blank" title="{$LABEL_BCIUTAT2}">
		<div id="bciutat2" class="marcfinestra2 bciutat">
				<img src="/images/botons/bciutatesport.jpg">
				<div class="banners3text" id="tciutat">
					{$LABEL_BCIUTAT2}	
				</div>					
		</div>
		</a>
		<a href="{$url_educacio}" target="_self" title="{$LABEL_BCIUTAT3}">
		<div id="bciutat3" class="marcfinestra2 bciutat">
				<img src="/images/botons/bciutatens.jpg">
				<div class="banners3text" id="tciutat">
					{$LABEL_BCIUTAT3}	
				</div>					
		</div>
		</a>
		<a href="{$url_salut}" target="_self" title="{$LABEL_BCIUTAT4}">
		<div id="bciutat4" class="marcfinestra2 bciutat">
				<img src="/images/botons/bciutatsalut.jpg">
				<div class="banners3text" id="tciutat">
					{$LABEL_BCIUTAT4}	
				</div>					
		</div>
		</a>
		<a href="{$url_transport}" target="_self" title="{$LABEL_BCIUTAT5}">
		<div id="bciutat5" class="marcfinestra2 bciutat">
				<img src="/images/botons/bciutattrans.jpg">
				<div class="banners3text" id="tciutat">
					{$LABEL_BCIUTAT5}	
				</div>					
		</div>
		</a>
		<a href="{$url_festes}" target="_self" title="{$LABEL_BCIUTAT6}">
		<div id="bciutat6" class="marcfinestra2 bciutat">
				<img src="/images/botons/bciutatfes.jpg">
				<div class="banners3text" id="tciutat">
					{$LABEL_BCIUTAT6}	
				</div>					
		</div>
		</a>
		<a href="{$url_ciutathistoria}" title="{$LABEL_BCIUTAT7}">
		<div id="bciutat7" class="marcfinestra2 bciutat">
				<img src="/images/botons/bciutathis.jpg">
				<div class="banners3text" id="tciutat">
					{$LABEL_BCIUTAT7}	
				</div>					
		</div>
		</a>
		<a href="/agermana/index.php" target="_self" title="{$LABEL_AGERMANAMENTS}">
		<div id="bciutat8" class="marcfinestra2 bciutat">
				<img src="/images/botons/bagermana.jpg">
				<div class="banners3text" id="tciutat">
					{$LABEL_AGERMANAMENTS}	
				</div>					
		</div>
		</a>
		<a href="/turisme-oci/index.php" target="_self" title="{$LABEL_BCIUTAT9}">
		<div id="bciutat9" class="marcfinestra2 bciutat">
				<img src="/images/botons/bciutattur.jpg">
				<div class="banners3text" id="tciutat">
					{$LABEL_BCIUTAT9}	
				</div>					
		</div>
		</a>
        <div class="ajunta-agenda">
            <iframe width="340" height="425" frameborder="0" src="http://agenda.tortosa.cat/agenda-tortosa-widget/"></iframe>
        </div>
		<div class="infgeneral" id="infpobles">
			<div id="taulaNCiutat">
    			<div id="filaNCiutat">
    				
        			<div class="colNegocis" id="col1NPobles"><a href="http://www.bitem.altanet.org/" target="_blank">{$LABEL_BITEM}</a></div>
        			
        			
        			<div class="colNegocis" id="col2NPobles"><a href="http://www.campredo.cat/" target="_blank">{$LABEL_CAMPREDO}</a></div>
        			
        			
        			<div class="colNegocis" id="col3NPobles"><a href="{$url_reguers}" target="_self">{$LABEL_ELSREGUERS}</a></div>
        			
        			
        			<div class="colNegocis" id="col4NPobles"><a href="http://www.jesus.cat" target="_blank">{$LABEL_JESUS}</a></div>
        			
        			
        			<div class="colNegocis" id="col5NPobles"><a href="{$url_vinallop}" target="_self">{$LABEL_VINALLOP}</a></div>
        			
    			</div>
    		</div>
		</div>
		<div id="titolelTemps" class="titolarea2">
			{$LABEL_TITOLTEMPS}
		</div>
		<div id="elTemps">
		<script type='text/javascript' src='http://www.aemet.es/ca/eltiempo/prediccion/municipios/launchwidget/tortosa-id43155?w=g4p01010000ohm99ccffw894z107x6699fft99ccffr0s4n1'></script><noscript><a target='_blank' style='font-weight: bold;font-size: 1.20em;' href='http://www.aemet.es/ca/eltiempo/prediccion/municipios/tortosa-id43155'> El Temps. Consulteu la predicció de l'AEMET per Tortosa</a></noscript>
		</div>
		<div class="infgeneral" id="infgeneral">
			<div id="taulaNCiutat">
    			<div id="filaNCiutat">
    				
        			<div class="colNegocis" id="col1NCiutat"><a href="http://www.meteotortosa.cat/meteo/" target="_blank">{$LABEL_CLIMA}</a></div>
        			
        			
        			<div class="colNegocis" id="col1NCiutat"><a href="{$url_planols}">{$LABEL_PLANOLS}</a></div>
        			
        			
        			<div class="colNegocis" id="col1NCiutat"><a href="http://serveisoberts.gencat.cat/equipaments#?adreca.municipi=Tortosa" target="_blank">{$LABEL_EQUIPAMENTS}</a></div>
        			
        			
        			<div class="colNegocis" id="col1NCiutat"><a href="{$url_agermana}">{$LABEL_AGERMANAMENTS}</a></div>
        			
        			
        			<div class="colNegocis" id="col1NCiutat"><a href="http://documents.tortosa.cat/" target="_blank">{$LABEL_PATRIMONI}</a></div>
        			
    			</div>
    		</div>
		</div>
	</div>
</div>
</div>
