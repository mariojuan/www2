<div id="page-wrap">

<div class="contenedor-responsive">

<div id="conttranstotalcos">
	<div id="conttranscos">
		<div id="btranspresen">
			<div id="transimatge">			
				<img src="{$image}" class="img-slider-territori">			
			</div>
			<div id="transimatgeText">
				<h1>
				{$LABEL_TITOL1}
				</h1>
			</div>
		</div>
		<div id="tcentre_municipi">
			{$LABEL_PARAG1}
			<br><br>
			{$LABEL_PARAG2}
			<br><br>
			<div id="ciutatmap1">
					<img src="/images/laciutat/municipi/Imagen1.jpg" /><br>{$LABEL_MAP1}								
			</div>
			<div id="ciutatmap2">
					<img src="/images/laciutat/municipi/Imagen2.jpg" /><br>{$LABEL_MAP2}								
			</div>
			<br><br><br>
			<div id="taulaNMunicipi">
    			<div id="filaNXifres">
        			<div class="colNegocis" id="col1NXifres">{$LABEL_TAULA1}</div>
        			<div class="colNegocis" id="col2NXifres">33.743</div>
    			</div>
    			<div id="filaNXifres">
        			<div class="colNegocis" id="col1NXifres">{$LABEL_TAULA2}</div>
        			<div class="colNegocis" id="col2NXifres">218,5</div>
    			</div>
    			<div id="filaNXifres">
        			<div class="colNegocis" id="col1NXifres">{$LABEL_TAULA3}</div>
        			<div class="colNegocis" id="col2NXifres">12</div>
    			</div>
    			<div id="filaNXifres">
        			<div class="colNegocis" id="col1NXifres">{$LABEL_TAULA4}</div>
        			<div class="colNegocis" id="col2NXifres">0,523347</div>
    			</div>
    			<div id="filaNXifres">
        			<div class="colNegocis" id="col1NXifres">{$LABEL_TAULA5}</div>
        			<div class="colNegocis" id="col2NXifres"> 40,812064</div>
    			</div>
			</div>
			<br><br style="clear: both">
			{$LABEL_PARAG3}
			<br><br>
			{$LABEL_PARAG4}
			<br><br>
			<div id="ciutatmap3" >
					<img src="/images/laciutat/municipi/Imagen3.jpg" class="img-mapa-territori1"/>								
			</div>
			<br><br>
			{$LABEL_PARAG5}
			<br><br>
			{$LABEL_PARAG6}
			<br><br>
			<div id="ciutatmap1">
					<img src="/images/laciutat/municipi/Imagen4.jpg" />								
			</div>
			<div id="ciutatmap2">
					<img src="/images/laciutat/municipi/Imagen5.jpg" />								
			</div>
			<div style="margin-bottom: 25px">
			    {$LABEL_PARAG7}
			</div>
            <div style="margin-bottom: 25px">
                {$LABEL_PARAG8}
            </div>
            <div style="margin-bottom: 25px">
                {$LABEL_PARAG9}
            </div>
            <div style="margin-bottom: 25px">
                {$LABEL_PARAG10}
            </div>
            <div style="margin-bottom: 25px">
                {$LABEL_PARAG11}
            </div>
            <div style="margin-bottom: 25px">
                {$LABEL_PARAG12}
            </div>
            <div style="margin-bottom: 25px">
                {$LABEL_PARAG13}
            </div>
            <div style="margin-bottom: 25px">
                {$LABEL_PARAG14}
            </div>
            <div style="margin-bottom: 25px">
                {$LABEL_PARAG15}
            </div>

			<ul class="municipi">
                <a href="http://www.bitem.altanet.org/" target="_blank">
                    <li><i class="icon-link"></i> {$LABEL_LINK10}</li>
                </a>
                <a href="http://www.campredo.cat" target="_blank">
                    <li><i class="icon-link"></i> {$LABEL_LINK12}</li>
                </a>
                <a href="{$url_reguers}" target="_blank">
                    <li><i class="icon-link"></i> {$LABEL_LINK14}</li>
                </a>
                <a href="http://www.jesus.cat/" target="_blank">
                    <li><i class="icon-link"></i> {$LABEL_LINK11}</li>
                </a>
                <a href="{$url_vinallop}" target="_self">
                    <li><i class="icon-link"></i> {$LABEL_LINK13}</li>
                </a>
                <a href="{$url_historia}" target="_self">
				<li><i class="icon-link"></i> {$LABEL_LINK9}</li>
				</a>
				<a href="http://www2.tortosa.cat/ciutat/estadistica-habitants/" target="_blank">
				<li><i class="icon-link"></i> {$LABEL_LINK1}</li>
				</a>
				<a href="{$url_equipa}">
				<li><i class="icon-link"></i> {$LABEL_LINK2}</li>
				</a>
				<a href="http://serveisoberts.gencat.cat/equipaments#?adreca.municipi=Tortosa" target="_blank">
				<li><i class="icon-link-ext"></i> {$LABEL_LINK3}</li>
				</a>
				<a href="http://www.tortosa.cat/webajt/MunicipiIdescat.asp" target="_blank">
				<li><i class="icon-link"></i> {$LABEL_LINK5}</li>
				</a>
				<a href="{$url_planols}" target="_self">
				<li><i class="icon-link"></i> {$LABEL_LINK6}</li>
				</a>
				<!--<a href="http://www2.tortosa.cat/ciutat/planols.php" target="_self">
				<li><i class="icon-link"></i> {$LABEL_LINK7}</li>
				</a>
				<a href="http://www2.tortosa.cat/ciutat/planols.php" target="_self">
				<li><i class="icon-link"></i> {$LABEL_LINK8}</li>
				</a>-->
			</ul>
			<br><br><br>
		</div>
	
	</div>
</div>

</div>

</div>
