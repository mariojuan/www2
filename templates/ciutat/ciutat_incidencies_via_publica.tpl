<html>
	<head>
        {include file="head.tpl"}
        <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyAgGXTi0OfrjbhveozxlIpPH3fTxHo8HnU"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <script type="text/javascript">
            var captcha_value = false;
            var accept_policy = false;
            $().ready(function() {
                $("#popup_close").click(function() {
                    tancapopup();
                });
                $(".close").click(function() {
                    tancapopup();
                });
                $('#latitud').prop('disabled', true);
                $('#longitud').prop('disabled', true);
                $('input[type=radio][id=localitzacio_mapa_si]').change(function() {
                    if($('#localitzacio_mapa_si').is(':checked')) {
                        $('#longitud').rules("add",
                        {
                             required: true,
                             messages: {
                                 required: "Els camps localització són obligatoris"
                             }
                        });
                    }
                });
                $('input[type=radio][id=localitzacio_mapa_no]').change(function() {
                    if($('#localitzacio_mapa_no').is(':checked')) {
                        $('#nucli').rules("add",
                        {
                            required: true,
                            messages: {
                                required: "{$LABEL_7}"
                            }
                        });
                        $('#barri').rules("add",
                        {
                            required: true,
                            messages: {
                                required: "{$LABEL_8}"
                            }
                        });
                        $('#carrer').rules("add",
                        {
                            required: true,
                            messages: {
                                required: "{$LABEL_9}"
                            }
                        });
                        $('#num').rules("add",
                        {
                            required: true,
                            messages: {
                                required: "{$LABEL_10}"
                            }
                        });
                    }
                });

                $("#incidencies_via_publica_form").validate({
                    rules: {
                        descripcio:  {
                            required: true
                        },
                        nom:  {
                            required: true
                        },
                        cognoms:  {
                            required: true
                        },
                        email:  {
                            required: true
                        },
                        telefon:  {
                            required: true
                        },
                        accept_privacy_policy: {
                            required: true
                        }
                    },
                    messages: {
                        descripcio: {
                            required: "{$LABEL_6}"
                        },
                        nom: {
                            required: "{$LABEL_11}"
                        },
                        cognoms: {
                            required: "{$LABEL_12}"
                        },
                        email: {
                            required: "{$LABEL_13}"
                        },
                        telefon: {
                            required: "{$LABEL_14}"
                        },
                        accept_privacy_policy: {
                            required: "{$LABEL_16}"
                        }
                    },
                    submitHandler: function(form) {
                        if(captcha_value) {
                            if(accept_policy)
                                form.submit();
                            else
                                $('#privacy_policy_error').css('display', 'block');
                        }
                        else {
                            $('#recaptcha-error').css('display', 'block');
                        }
                    }
                });

                $("#incidencies_via_publica_form").validate();

                $('#nucli').change(function() {
                    var nucli_id = $('#nucli').val();
                    $('#nucli_name').val($('#nucli option[value="'+nucli_id+'"]').text());
                    var xmlhttp = new XMLHttpRequest();
                    xmlhttp.onreadystatechange = function() {
                        if (this.readyState == 4 && this.status == 200) {
                            var barris = this.responseText.split("||");
                            $('#barri').empty();
                            $('#barri').append('<option value="">-- Barri --</option>');
                            $.each(barris, function(key, barri) {
                                var barri_item = barri.split("|");
                                $('#barri').append('<option value="'+barri_item[0]+'">'+barri_item[1]+'</option>');
                            });
                        }
                    };
                    xmlhttp.open("POST", "https://www2.tortosa.cat/ciutat/getbarris.php", true);
                    xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                    xmlhttp.send("nucli_id=" + nucli_id);
                });
                $('#barri').change(function() {
                    var barri_id = $('#barri').val();
                    $('#barri_name').val($('#barri option[value="'+barri_id+'"]').text());
                });

                $('#carrer').keyup(function() {
                    $('#suggestions').css('display', 'block');
                    var nucli_id = $('#nucli').val();
                    var xmlhttp = new XMLHttpRequest();
                    xmlhttp.onreadystatechange = function() {
                        if (this.readyState == 4 && this.status == 200) {
                            var carrer = this.responseText;
                            $('#suggestions').fadeIn(1000).html(carrer);
                            //Al hacer click en algua de las sugerencias
                            $('.suggest-element a').click(function(){
                                //Obtenemos la id unica de la sugerencia pulsada
                                var id = $(this).attr('id');
                                //Editamos el valor del input con data de la sugerencia pulsada
                                $('#carrer').val($('#'+id).attr('data'));
                                //Hacemos desaparecer el resto de sugerencias
                                $('#suggestions').fadeOut(1000);
                            });
                        }
                    };
                    xmlhttp.open("POST", "https://www2.tortosa.cat/ciutat/getcarrers.php", true);
                    xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                    xmlhttp.send("carrer="+$('#carrer').val()+"&nucli="+$('#nucli').val());
                });
            });

            function obripopup() {
                var modal = $('#formulari_generic_modal');
                modal.css("display", "block");
                accept_policy = true;
            }
            function tancapopup() {
                var modal = $('#formulari_generic_modal');
                modal.css("display", "none");
            }

            function captcha() {
                var xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        var out = this.responseText;
                        if(out==1) {
                            $('#recaptcha-error').css('display', 'none');
                            captcha_value = true;
                        }
                        else {
                            $('#recaptcha-error').css('display', 'block');
                            captcha_value = false;
                        }
                    }
                };
                xmlhttp.open("POST", "https://www2.tortosa.cat/libs/verify-recaptcha.php", false);
                xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                xmlhttp.send("g-recaptcha-response="+grecaptcha.getResponse());
            }

            function conmutalocalitzacioradios(val) {
                if(val) {
                    $('#localitzacio_mapa_si').attr('checked', true);
                    $('#localitzacio_mapa_no').attr('checked', false);
                    $('#localitzacio_mapa').css('display', 'block');
                    $('#localitzacio_no_mapa').css('display', 'none');
                    $('#map_canvas').css('display', 'block');
                }
                else {
                    $('#localitzacio_mapa_si').attr('checked', false);
                    $('#localitzacio_mapa_no').attr('checked', true);
                    $('#localitzacio_mapa').css('display', 'none');
                    $('#localitzacio_no_mapa').css('display', 'block');
                    $('#map_canvas').css('display', 'none');
                }
            }
            function recaptchaCallback() {
                captcha();
                console.log(captcha_value);
            }
        </script>
        <style>
            .suggest-element{
                margin-left:175px;
                margin-top:5px;
                width:320px;
                cursor:pointer;
            }
            #suggestions {
                height:150px;
                overflow: auto;
                display: none;
                margin-top: -15px;
            }
        </style>
	</head>
	<body id="incidencies_via_publica">
		{include file="header.tpl"}
		{include file="ciutat/incidencies_via_publica.tpl" image="/images/iviapublica/tira.jpg"}
		{include file="footer.tpl"}
	</body>
</html>