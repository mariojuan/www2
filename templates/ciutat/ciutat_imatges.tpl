<html>
	<head>
        {include file="head.tpl"}
        <link rel="stylesheet" href="/css/woomark/main.css">
        <link rel="stylesheet" href="/css/woomark/normalize.css">
        <link rel="stylesheet" href="/css/prettyPhoto.css">
        <script type="text/javascript" src="/js/imagesloaded.pkgd.min.js"></script>
        <script type="text/javascript" src="/js/wookmark.js"></script>
        <script type="text/javascript" src="/js/jquery.prettyPhoto.js"></script>
	</head>
	<body>
        {include file="header.tpl"}

        <div class="contenedor-responsive">

        <div id="conttranstotalcos">
            <div id="conttranscos">
                <div id="btranspresen">
                    <div id="transimatge">
                        <img src="/images/laciutat/tira.jpg" class="img-slider-territori"/>
                    </div>
                    <div id="transimatgeText">
                        <h1>
                            {$LABEL_TITOL1}
                        </h1>
                    </div>
                </div>
                <div role="main" style="clear: both">
                <ul id="container" class="tiles-wrap animated wookmark-initialised" style="display: block; height: 1860px;">
                    <li class="" data-wookmark-id="0" data-wookmark-height="343" data-wookmark-top="10" style="position: absolute; top: 10px; left: 69px;">
                        <a href="http://www2.tortosa.cat/images/laciutat/imatges_ciutat/museu-de-tortosa.jpg" rel="prettyPhoto">
                            <img src="http://www2.tortosa.cat/images/laciutat/imatges_ciutat/museu-de-tortosa_min.jpg" alt="{$FOTO1}"><p>{$FOTO1}</p>
                        </a>
                    </li>
                    <li class="" data-wookmark-id="1" data-wookmark-height="360" data-wookmark-top="10" style="position: absolute; top: 10px; left: 284px;">
                        <a href="http://www2.tortosa.cat/images/laciutat/imatges_ciutat/campament-festa-renaixement.jpg" rel="prettyPhoto">
                            <img src="http://www2.tortosa.cat/images/laciutat/imatges_ciutat/campament-festa-renaixement_min.jpg" alt="{$FOTO2}"><p>{$FOTO2}</p>
                        </a>
                    </li>
                    <li class="" data-wookmark-id="2" data-wookmark-height="312" data-wookmark-top="10" style="position: absolute; top: 10px; left: 499px;">
                        <a href="http://www2.tortosa.cat/images/laciutat/imatges_ciutat/avancades-sant-joan.jpg" rel="prettyPhoto">
                            <img src="http://www2.tortosa.cat/images/laciutat/imatges_ciutat/avancades-sant-joan_min.jpg" alt="{$FOTO3}"><p>{$FOTO3}</p>
                        </a>
                    </li>
                    <li class="" data-wookmark-id="3" data-wookmark-height="218" data-wookmark-top="10" style="position: absolute; top: 10px; left: 714px;">
                        <a href="http://www2.tortosa.cat/images/laciutat/imatges_ciutat/espectacle-festa-del-renaixement.jpg" rel="prettyPhoto">
                            <img src="http://www2.tortosa.cat/images/laciutat/imatges_ciutat/espectacle-festa-del-renaixement_min.jpg" alt="{$FOTO4}"><p>{$FOTO4}</p>
                        </a>
                    </li>

                    <li class="" data-wookmark-id="4" data-wookmark-height="360" data-wookmark-top="10" style="position: absolute; top: 10px; left: 929px;">
                        <a href="http://www2.tortosa.cat/images/laciutat/imatges_ciutat/centre-interpretacio-setmana-santa-tortosa.jpg" rel="prettyPhoto">
                            <img src="http://www2.tortosa.cat/images/laciutat/imatges_ciutat/centre-interpretacio-setmana-santa-tortosa_min.jpg" alt="{$FOTO5}"><p>{$FOTO5}</p>
                        </a>
                    </li>
                    <li class="" data-wookmark-id="5" data-wookmark-height="357" data-wookmark-top="10" style="position: absolute; top: 10px; left: 1144px;">
                        <a href="http://www2.tortosa.cat/images/laciutat/imatges_ciutat/la-cucafera-de-tortosa.jpg" rel="prettyPhoto">
                            <img src="http://www2.tortosa.cat/images/laciutat/imatges_ciutat/la-cucafera-de-tortosa_min.jpg" alt="{$FOTO6}"><p>{$FOTO6}</p>
                        </a>
                    </li>
                    <li class="" data-wookmark-id="6" data-wookmark-height="260" data-wookmark-top="233" style="position: absolute; top: 233px; left: 714px;">
                        <a href="http://www2.tortosa.cat/images/laciutat/imatges_ciutat/museu-de-tortosa-interior.jpg" rel="prettyPhoto">
                            <img src="http://www2.tortosa.cat/images/laciutat/imatges_ciutat/museu-de-tortosa-interior_min.jpg" alt="{$FOTO7}"><p>{$FOTO7}</p>
                        </a>
                    </li>
                    <li class="" data-wookmark-id="7" data-wookmark-height="260" data-wookmark-top="327" style="position: absolute; top: 327px; left: 499px;">
                        <a href="http://www2.tortosa.cat/images/laciutat/imatges_ciutat/festa-renaixement.jpg" rel="prettyPhoto">
                            <img src="http://www2.tortosa.cat/images/laciutat/imatges_ciutat/festa-renaixement_min.jpg" alt="{$FOTO8}"><p>{$FOTO8}</p>
                        </a>
                    </li>
                    <li class="" data-wookmark-id="8" data-wookmark-height="458" data-wookmark-top="358" style="position: absolute; top: 358px; left: 69px;">
                        <a href="http://www2.tortosa.cat/images/laciutat/imatges_ciutat/call-jueu.jpg" rel="prettyPhoto">
                            <img src="http://www2.tortosa.cat/images/laciutat/imatges_ciutat/call-jueu_min.jpg" alt="{$FOTO9}"><p>{$FOTO9}</p>
                        </a>
                    </li>
                    <li class="" data-wookmark-id="9" data-wookmark-height="327" data-wookmark-top="372" style="position: absolute; top: 372px; left: 1144px;">
                        <a href="http://www2.tortosa.cat/images/laciutat/imatges_ciutat/centre-interpretacio-renaixement.jpg" rel="prettyPhoto">
                            <img src="http://www2.tortosa.cat/images/laciutat/imatges_ciutat/centre-interpretacio-renaixement_min.jpg" alt="{$FOTO10}"><p>{$FOTO10}</p>
                        </a>
                    </li>
                    <li class="" data-wookmark-id="10" data-wookmark-height="343" data-wookmark-top="375" style="position: absolute; top: 375px; left: 284px;">
                        <a href="http://www2.tortosa.cat/images/laciutat/imatges_ciutat/muleta-pel-riu.jpg" rel="prettyPhoto">
                            <img src="http://www2.tortosa.cat/images/laciutat/imatges_ciutat/muleta-pel-riu_min.jpg" alt="{$FOTO11}"><p>{$FOTO11}</p>
                        </a>
                    </li>
                    <li class="" data-wookmark-id="11" data-wookmark-height="360" data-wookmark-top="375" style="position: absolute; top: 375px; left: 929px;">
                        <a href="http://www2.tortosa.cat/images/laciutat/imatges_ciutat/catedral-de-tortosa.jpg" rel="prettyPhoto">
                            <img src="http://www2.tortosa.cat/images/laciutat/imatges_ciutat/catedral-de-tortosa_min.jpg" alt="{$FOTO12}"><p>{$FOTO12}</p>
                        </a>
                    </li>
                    <!--
                    <li class="" data-wookmark-id="12" data-wookmark-height="312" data-wookmark-top="498" style="position: absolute; top: 498px; left: 714px;"><img src="http://plugin.wookmark.com/sample-images/image_3.jpg"><p>13</p></li>
                    <li class="" data-wookmark-id="13" data-wookmark-height="218" data-wookmark-top="592" style="position: absolute; top: 592px; left: 499px;"><img src="http://plugin.wookmark.com/sample-images/image_4.jpg"><p>14</p></li>
                    <li class="" data-wookmark-id="14" data-wookmark-height="360" data-wookmark-top="704" style="position: absolute; top: 704px; left: 1144px;"><img src="http://plugin.wookmark.com/sample-images/image_5.jpg"><p>15</p></li>
                    <li class="" data-wookmark-id="15" data-wookmark-height="357" data-wookmark-top="723" style="position: absolute; top: 723px; left: 284px;"><img src="http://plugin.wookmark.com/sample-images/image_6.jpg"><p>16</p></li>
                    <li class="" data-wookmark-id="16" data-wookmark-height="260" data-wookmark-top="740" style="position: absolute; top: 740px; left: 929px;"><img src="http://plugin.wookmark.com/sample-images/image_7.jpg"><p>17</p></li>
                    <li class="" data-wookmark-id="17" data-wookmark-height="260" data-wookmark-top="815" style="position: absolute; top: 815px; left: 499px;"><img src="http://plugin.wookmark.com/sample-images/image_8.jpg"><p>18</p></li>
                    <li class="" data-wookmark-id="18" data-wookmark-height="458" data-wookmark-top="815" style="position: absolute; top: 815px; left: 714px;"><img src="http://plugin.wookmark.com/sample-images/image_9.jpg"><p>19</p></li>
                    <li class="" data-wookmark-id="19" data-wookmark-height="327" data-wookmark-top="821" style="position: absolute; top: 821px; left: 69px;"><img src="http://plugin.wookmark.com/sample-images/image_10.jpg"><p>20</p></li>
                    <li class="" data-wookmark-id="20" data-wookmark-height="343" data-wookmark-top="1005" style="position: absolute; top: 1005px; left: 929px;"><img src="http://plugin.wookmark.com/sample-images/image_1.jpg"><p>21</p></li>
                    <li class="" data-wookmark-id="21" data-wookmark-height="360" data-wookmark-top="1069" style="position: absolute; top: 1069px; left: 1144px;"><img src="http://plugin.wookmark.com/sample-images/image_2.jpg"><p>22</p></li>
                    <li class="" data-wookmark-id="22" data-wookmark-height="312" data-wookmark-top="1080" style="position: absolute; top: 1080px; left: 499px;"><img src="http://plugin.wookmark.com/sample-images/image_3.jpg"><p>23</p></li>
                    <li class="" data-wookmark-id="23" data-wookmark-height="218" data-wookmark-top="1085" style="position: absolute; top: 1085px; left: 284px;"><img src="http://plugin.wookmark.com/sample-images/image_4.jpg"><p>24</p></li>
                    <li class="" data-wookmark-id="24" data-wookmark-height="360" data-wookmark-top="1153" style="position: absolute; top: 1153px; left: 69px;"><img src="http://plugin.wookmark.com/sample-images/image_5.jpg"><p>25</p></li>
                    <li class="" data-wookmark-id="25" data-wookmark-height="357" data-wookmark-top="1278" style="position: absolute; top: 1278px; left: 714px;"><img src="http://plugin.wookmark.com/sample-images/image_6.jpg"><p>26</p></li>
                    <li class="" data-wookmark-id="26" data-wookmark-height="260" data-wookmark-top="1308" style="position: absolute; top: 1308px; left: 284px;"><img src="http://plugin.wookmark.com/sample-images/image_7.jpg"><p>27</p></li>
                    <li class="" data-wookmark-id="27" data-wookmark-height="260" data-wookmark-top="1353" style="position: absolute; top: 1353px; left: 929px;"><img src="http://plugin.wookmark.com/sample-images/image_8.jpg"><p>28</p></li>
                    <li class="" data-wookmark-id="28" data-wookmark-height="458" data-wookmark-top="1397" style="position: absolute; top: 1397px; left: 499px;"><img src="http://plugin.wookmark.com/sample-images/image_9.jpg"><p>29</p></li>
                    <li class="" data-wookmark-id="29" data-wookmark-height="327" data-wookmark-top="1434" style="position: absolute; top: 1434px; left: 1144px;"><img src="http://plugin.wookmark.com/sample-images/image_10.jpg"><p>30</p></li>
                    -->
                </ul>
            </div>
                <script type="text/javascript">
                    (function ($) {
                        var loadedImages = 0, // Counter for loaded images
                                $progressBar = $('.progress-bar'),
                                container = '#container',
                                $container = $(container),
                                tileCount = 30,
                                wookmark;
                        /*
                        for (var i = 0; i < tileCount; i++) {
                            var newItemHtml = '<li class="tile-loading"><img src="http://plugin.wookmark.com/sample-images/image_' + (1 + i % 10) + '.jpg"><p>' + (1 + i) + '</p></li>';
                            $container.append(newItemHtml);
                        }
                        */
                        // Initialize Wookmark
                        wookmark = new Wookmark(container, {
                            offset: 5, // Optional, the distance between grid items
                            outerOffset: 10, // Optional, the distance to the containers border
                            itemWidth: 210 // Optional, the width of a grid item
                        });

                        $container.imagesLoaded()
                                .always(function () {
                                    $progressBar.hide();
                                })
                                .progress(function (instance, image) {
                                    // Update progress bar after each image has loaded and remove loading state
                                    $(image.img).closest('li').removeClass('tile-loading');
                                    $progressBar.css('width', (++loadedImages / tileCount * 100) + '%');
                                    wookmark.updateOptions();
                                });
                    })(jQuery);
                </script>
            </div>
        </div>

        </div>

		{include file="footer.tpl"}
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function(){
                $("a[rel^='prettyPhoto']").prettyPhoto();
            });
        </script>
	</body>
</html>