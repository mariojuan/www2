<div id="page-wrap">

<div class="territori-contenedor-responsive">

   <div id="conttranstotalcos">
      <div id="conttranscos">

         <div id="btranspresen">

            <div id="transimatge">			
               <img src="{$image}" class="img-slider-territori">			
            </div>

            <div id="transimatgeText">
               <h1>
                  {$LABEL_TITOL1}
               </h1>
            </div>

         </div>

         <div id="tcentre_municipi">
         <p>
            {$LABEL_PARAG1}
            <br><br>
            {$LABEL_PARAG2}
            <br><br>
            {$LABEL_PARAG3}
            <br><br>
            {$LABEL_PARAG4}
            <br><br>
            {$LABEL_PARAG5}
            <br><br>
            </p>
         </div>

      </div>
   </div>

</div>

</div>