<div id="page-wrap">
   <div id="contenidortotalcos">
      <!-- FOTOS SLIDER -->
      <div class="rowCartellera">
         <div id="bciutatpresen">
            <ul id="bciutatmenu">
               <li >
                  <a href="{$url_municipi}">
                     <div id="ciutatbotoimatge">
                        <img src="/images/botons/bmunicipi.jpg" />                           
                     </div>
                     <div id="textbmenu1" class="textbotomciutat">
                        {$LABEL_MENUV1A}
                     </div>
                  </a>
               </li>
               <li >
                  <a href="{$url_estadistica_habitants}">
                     <div id="ciutatbotoimatge">
                        <img src="/images/botons/besta.jpg" />                         
                     </div>
                     <div id="textbmenu2" class="textbotomciutat">
                        {$LABEL_MENUV3}
                        <br>
                        {$LABEL_MENUV3B}
                     </div>
                  </a>
               </li>
               <li>
                  <a href="{$url_imatges}">
                     <div id="ciutatbotoimatge">
                        <img src="/images/botons/bimatges.jpg" />                         
                     </div>
                     <div id="textbmenu3" class="textbotomciutat">
                        {$LABEL_MENUV1}
                        <br>
                        {$LABEL_MENUV1B}
                     </div>
                  </a>
               </li>
               <li >
                  <a href="{$url_territori}">
                     <div id="ciutatbotoimatge">
                        <img src="/images/botons/bterritori.jpg" />                          
                     </div>
                     <div id="textbmenu4" class="textbotomciutat">
                        {$LABEL_MENUV2A}  
                     </div>
                  </a>
               </li>
            </ul>
            <div id="ciutatimatge" class="bx-wrapper">
                <div class="bx-viewport" aria-live="polite" style="width: 100%; overflow: hidden; position: relative;">
                    <!-- Slider de la Ciutat -->
                    <ul class="bxslider" style="width: 4215%; position: relative; transition-duration: 0.5s; transform: translate3d(-438px, 0px, 0px);">
                        {foreach $bannerslst as $banner}
                            {if $banner->TIPUS == 3}
                                <li style="float: left; list-style: outside none none; position: relative;" class="bx-clone" aria-hidden="true">
                                    <img src="/public_files/banners/{$banner->IMATGE}" class="img-slider-ciutat" alt="{$banner->TITOL}" title="{$banner->TITOL}"/>
                                </li>
                            {/if}
                        {/foreach}
                    </ul>
                </div>
            </div>
            <div id="ciutatimatgeText" class="imatgeCapText">
               <h1>
                  {$LABEL_TITOL}&nbsp;&nbsp;
               </h1>
            </div>
         </div>
      </div>
      <!--/ FOTOS SLIDER -->
      <!-- BANNERS -->
      <div class="row">

         <div class="div-banners-ciutat">

         <div class="marcfinestra banner-mes-informacio">
            <div id="banner4ima">
               <a href="http://agenda.tortosa.cat/agenda-tortosa/" target="_blank">
               <img src="/images/banners/bnagenda2.jpg" title="Agenda" alt="Agenda">
               </a>         
            </div>
            <div class="banners4text" id="bannerlateraltext">
               {$LABEL_BAGENDAC}
            </div>
         </div>
         <div class="marcfinestra banner-mes-informacio">
            <div id="banner4ima">
               <a href="http://www.tortosa.altanet.org/imact/agenda319.pdf" target="_blank"><img src="/images/banners/bnagecul.jpg" title="Agenda Cultural" alt="Agenda Cultural"></a>       
            </div>
            <div class="banners4text" id="bannerlateraltext">
               {$LABEL_BAGENDAC}
            </div>
            <div class="banners4text" id="banners4textp">
               {$LABEL_BAGENDACB}
            </div>
         </div>
         <div class="marcfinestra banner-mes-informacio">
            <div id="banner4ima">
               <a href="http://www.museudetortosa.cat" target="_blank"><img src="/images/banners/bnmuseuf.jpg" title="Museu de Tortosa" alt="Museu de Tortosa"></a>        
            </div>
            <div class="banners4text" id="bannerlateraltext">
               {$LABEL_BMUSEUC}
            </div>
            <div class="banners4text" id="banners4textp">
               {$LABEL_BMUSEUCB}
            </div>
         </div>
         <div class="marcfinestra banner-mes-informacio">
            <div id="banner4ima">
               <a href="http://www.teatreauditoritortosa.cat" target="_blank"><img src="/images/banners/bnteatref.jpg" title="Teatre Auditori Felip Pedrell" alt="Teatre Auditori Felip Pedrell"></a>       
            </div>
            <div class="banners4text" id="bannerlateraltext">
               {$LABEL_BTEATREC}
            </div>
            <div class="banners4text" id="banners4textp">
               {$LABEL_BTEATRECB}
            </div>
         </div>
         <div class="marcfinestra banner-mes-informacio banner-mes-informacio-last">
            <div id="banner4ima">
               <a href="http://www.radiotortosa.cat" target="_blank"><img src="/images/banners/bnradiot.jpg" title="Ràdio Tortosa" alt="Ràdio Tortosa"></a>       
            </div>
            <div class="banners4text" id="bannerlateraltext">
               {$LABEL_BRADIOC}
            </div>
            <div class="banners4text" id="banners4textp">
               {$LABEL_BRADIOB}
            </div>
         </div>

         </div>

      </div>
      <!--/ BANNERS -->
      <!-- VIU LA CIUTAT -->
      <div class="row">

         <div class="titolsSeccionsHome titolBig marge-titol-contingut">
            {$LABEL_TITOLLM}
         </div>

         <div class="div-banners-viuLaCiutat">

         <div class="column-left-viulaciutat">
            <a href="{$url_cultura}" target="_self" title="{$LABEL_BCIUTAT1}">
               <div class="marcfinestra2 banner-viulaciutat">
                  <img src="/images/botons/bciutatcul.jpg">
                  <div class="banners3text" id="tciutat">
                     {$LABEL_BCIUTAT1} 
                  </div>
               </div>
            </a>
            <a href="http://tortosasport.cat/" target="_blank" title="{$LABEL_BCIUTAT2}">
               <div class="marcfinestra2 banner-viulaciutat">
                  <img src="/images/botons/bciutatesport.jpg">
                  <div class="banners3text" id="tciutat">
                     {$LABEL_BCIUTAT2} 
                  </div>
               </div>
            </a>
            <a href="{$url_educacio}" target="_self" title="{$LABEL_BCIUTAT3}">
               <div class="marcfinestra2 banner-viulaciutat">
                  <img src="/images/botons/bciutatens.jpg">
                  <div class="banners3text" id="tciutat">
                     {$LABEL_BCIUTAT3} 
                  </div>
               </div>
            </a>
            <a href="{$url_salut}" target="_self" title="{$LABEL_BCIUTAT4}">
               <div class="marcfinestra2 banner-viulaciutat">
                  <img src="/images/botons/bciutatsalut.jpg">
                  <div class="banners3text" id="tciutat">
                     {$LABEL_BCIUTAT4} 
                  </div>
               </div>
            </a>
            <a href="{$url_transport}" target="_self" title="{$LABEL_BCIUTAT5}">
               <div class="marcfinestra2 banner-viulaciutat">
                  <img src="/images/botons/bciutattrans.jpg">
                  <div class="banners3text" id="tciutat">
                     {$LABEL_BCIUTAT5} 
                  </div>
               </div>
            </a>
            <a href="{$url_festes}" target="_self" title="{$LABEL_BCIUTAT6}">
               <div class="marcfinestra2 banner-viulaciutat">
                  <img src="/images/botons/bciutatfes.jpg">
                  <div class="banners3text" id="tciutat">
                     {$LABEL_BCIUTAT6} 
                  </div>
               </div>
            </a>
            <a href="{$url_ciutathistoria}" title="{$LABEL_BCIUTAT7}">
               <div class="marcfinestra2 banner-viulaciutat">
                  <img src="/images/botons/bciutathis.jpg">
                  <div class="banners3text" id="tciutat">
                     {$LABEL_BCIUTAT7} 
                  </div>
               </div>
            </a>
            <a href="/agermana/index.php" target="_self" title="{$LABEL_AGERMANAMENTS}">
               <div class="marcfinestra2 banner-viulaciutat">
                  <img src="/images/botons/bagermana.jpg">
                  <div class="banners3text" id="tciutat">
                     {$LABEL_AGERMANAMENTS}  
                  </div>
               </div>
            </a>
            <a href="/turisme-oci/index.php" target="_self" title="{$LABEL_BCIUTAT9}">
               <div class="marcfinestra2 banner-viulaciutat">
                  <img src="/images/botons/bciutattur.jpg">
                  <div class="banners3text" id="tciutat">
                     {$LABEL_BCIUTAT9} 
                  </div>
               </div>
            </a>
         </div>

         </div>

         <div class="column-right-viulaciutat">

         <div class="div-ciutat-agenda">

            <iframe width="100%" height="565" frameborder="0" src="http://agenda.tortosa.cat/agenda-tortosa-widget/"></iframe>

         </div>

         </div>

      </div>
      <!--/ VIU LA CIUTAT -->
      <!-- POBLES -->
      <div class="row">
         <div class="div-pobles">
         <div class="banner-pobles-ciutat">
            <div class="taula-n-ciutat">
               <div class="col-negocis" id="col1NPobles"><a href="http://www.bitem.altanet.org/" target="_blank">{$LABEL_BITEM}</a></div>
               <div class="col-negocis" id="col2NPobles"><a href="http://www.campredo.cat/" target="_blank">{$LABEL_CAMPREDO}</a></div>
               <div class="col-negocis" id="col3NPobles"><a href="{$url_reguers}" target="_self">{$LABEL_ELSREGUERS}</a></div>
               <div class="col-negocis" id="col4NPobles"><a href="http://www.jesus.cat" target="_blank">{$LABEL_JESUS}</a></div>
               <div class="col-negocis" id="col5NPobles"><a href="{$url_vinallop}" target="_self">{$LABEL_VINALLOP}</a></div>
            </div>
         </div>
         </div>
      </div>
      <!--/ POBLES -->
      <!-- EL TEMPS -->
      <div class="row responsive-ocult">

         <div class="titolsSeccionsHome titolBig marge-titol-contingut">
            {$LABEL_TITOLTEMPS}
         </div>

            
            <script type='text/javascript' src='http://www.aemet.es/ca/eltiempo/prediccion/municipios/launchwidget/tortosa-id43155?w=g4p01010000ohm99ccffw894z107x6699fft99ccffr0s4n1'>
            </script>
            <noscript>
               <a target='_blank' style='font-weight: bold;font-size: 1.20em;' href='http://www.aemet.es/ca/eltiempo/prediccion/municipios/tortosa-id43155'> El Temps. Consulteu la predicció de l'AEMET per Tortosa</a>
            </noscript>
            

      </div>
      <!--/ EL TEMPS -->

      <!-- +INFO -->
      <div class="row">

         <div class="div-mesInfo">

         <div class="info-general-ciutat">
               <div class="taula-n-ciutat-2">
               
                  <div class="col-negocis col-negocis2"><a href="http://www.meteotortosa.cat/meteo/" target="_blank">{$LABEL_CLIMA}</a></div>

                  <div class="col-negocis col-negocis2"><a href="{$url_planols}">{$LABEL_PLANOLS}</a></div>
               
                  <div class="col-negocis col-negocis2"><a href="http://serveisoberts.gencat.cat/equipaments#?adreca.municipi=Tortosa" target="_blank">{$LABEL_EQUIPAMENTS}</a></div>
               
                  <div class="col-negocis col-negocis2"><a href="{$url_entitats}">{$LABEL_ENTITATS}</a></div>

                  <div class="col-negocis col-negocis2"><a href="http://documents.tortosa.cat/" target="_blank">{$LABEL_PATRIMONI}</a></div>
               
               </div>
            </div>

            </div>

      </div>
      <!--/ +INFO -->

   </div>
</div>