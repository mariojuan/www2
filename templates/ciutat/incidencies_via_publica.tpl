<div id="page-wrap">
<div id="conttranstotalcos">
	<div id="conttranscos">
		<div id="btranspresen">
			<div id="transimatge">			
				<img src={$image}>			
			</div>
			<div id="transimatgeText">
				<h1>
				{$LABEL_TITOL1}
				</h1>
			</div>
		</div>
		<div id="tcentre_municipi">
            {if !$request}
                <div  style="margin-bottom: 20px">{$LABEL_TC1}</diV>
                <div id="taulaivp">
                    <div id="filaivp">
                        <div id="colivp1" class="colivp"><strong>{$LABEL_TC11}</strong><br><br>900 131 326 (24h)</div>
                        <div id="colivp2" class="colivp"><strong>{$LABEL_TC12}</strong><br><br>092 / 977 585 801 (24h)</div>
                        <div id="colivp3" class="colivp"><strong>{$LABEL_TC13}</strong><br><br>900 194 998 (24h)</div>
                    </div>
                    <div id="filaivp">
                        <div id="colivp4" class="colivp"><strong>{$LABEL_TC14}</strong><br><br>600 927 902 (24h)</div>
                        <div id="colivp5" class="colivp"><strong>{$LABEL_TC15}</strong><br><br>977585875<br>662078632</div>
                        <div id="colivp6" class="colivp">
                        </div>
                    </div>
                </div>
                <p>{$LABEL_2}</p>
                <p>{$LABEL_3}</p>
                <form id="incidencies_via_publica_form" method="post" action="incidencies_via_publica.php?lang={$lang}" novalidate="novalidate" enctype="multipart/form-data">
                    <input type="hidden" name="nucli_name" id="nucli_name" value="">
                    <input type="hidden" name="barri_name" id="barri_name" value="">
                    <input type="hidden" name="latitud_value" id="latitud_value" value="">
                    <input type="hidden" name="longitud_value" id="longitud_value" value="">
                    <p>
                        <label>{$FIELD_2} *</label>
                        <textarea name="descripcio" id="descripcio" aria-required="true" aria-invalid="true"></textarea>
                    </p>
                    <p>
                        <label>{$FIELD_3} *</label>
                    </p>
                    <p>
                        <label style="width: 45%;">{$FIELD_3_0}</label>
                        <input name="localitzacio_mapa_si" id="localitzacio_mapa_si" class="radio" value="1" checked="" onclick="conmutalocalitzacioradios(true);" type="radio">
                        <label style="width: 15%;"> Sí </label>

                        <input name="localitzacio_mapa_no" id="localitzacio_mapa_no" class="radio" value="0" onclick="conmutalocalitzacioradios(false);" type="radio">
                        <label style="width: 15%;"> No </label>
                    </p>
                    <div id="localitzacio_mapa">
                        <p>
                            <div id='map_canvas'></div>
                            {$FIELD_3_COMMENT_1}
                            <div id="current">
                                <ul id="latitud_longitud">
                                    <li>
                                    {$FIELD_3_OPTION_1} <input type="text" name="latitud" id="latitud" aria-required="true" aria-invalid="true">
                                    </li>
                                    <li>
                                    {$FIELD_3_OPTION_2} <input type="text" name="longitud" id="longitud" aria-required="true" aria-invalid="true">
                                    </li>
                                </ul>
                            </div>
                            <script type="text/javascript">
                                var map = new google.maps.Map(document.getElementById('map_canvas'), {
                                    zoom: 16,
                                    center: new google.maps.LatLng(40.811, 0.521),
                                    mapTypeId: google.maps.MapTypeId.SATELLITE
                                });

                                var myMarker = new google.maps.Marker({
                                    position: new google.maps.LatLng(40.811, 0.521)
                                });

                                map.addListener('click', function (evt) {
                                    var latlng = new google.maps.LatLng(evt.latLng.lat(), evt.latLng.lng());
                                    myMarker.setPosition(latlng);
                                    /*
                                    $('#latitud').prop('enabled', true);
                                    $('#longitud').prop('enabled', true);
                                    */
                                    document.getElementById('latitud').value = evt.latLng.lat();
                                    document.getElementById('longitud').value = evt.latLng.lng();
                                    /*
                                    $('#latitud').prop('disabled', true);
                                    $('#longitud').prop('disabled', true);
                                    */
                                    $('#latitud_value').val(evt.latLng.lat());
                                    $('#longitud_value').val(evt.latLng.lng());
                                    //document.getElementById('current').innerHTML = '<p>Marker dropped: Current Lat: ' + evt.latLng.lat().toFixed(3) + ' Current Lng: ' + evt.latLng.lng().toFixed(3) + '</p>';
                                });
                                /*
                                google.maps.event.addListener(myMarker, 'dragstart', function (evt) {
                                    document.getElementById('latitud').value = "";
                                    document.getElementById('latitud').value = "";
                                    //document.getElementById('current').innerHTML = '<p>Currently dragging marker...</p>';
                                });
                                */
                                map.setCenter(myMarker.position);
                                myMarker.setMap(map);
                            </script>
                        </p>
                    </div>
                    <div id="localitzacio_no_mapa" style="display: none">
                        {$FIELD_3_COMMENT_2}
                        <p>
                            <label>{$FIELD_11} *</label>
                            <select name="nucli" id="nucli" placeholder=" Nucli">
                                <option value="">-- Nucli --</option>
                                {foreach from=$nuclis key=key item=value}
                                    {$value|var_dump}
                                    <option value="{$value->ID}">{$value->DESCRIPCIO}</option>
                                {/foreach}
                            </select>
                            <select name="barri" id="barri">
                                <option value="">-- Barri --</option>
                                {foreach from=$barris key=key item=value}
                                    {$value|var_dump}
                                    <option value="{$value->ID}">{$value->DESCRIPCIO}</option>
                                {/foreach}
                            </select>
                        </p>
                        <p>
                            <label>{$FIELD_13} *</label>
                            <input type="text" name="carrer" id="carrer" aria-required="true" aria-invalid="true" style="width: 320px">
                            <div id="suggestions"></div>
                            <label>{$FIELD_14} *</label>
                            <input type="text" name="num" id="num" aria-required="true" aria-invalid="true">
                        </p>
                    </div>
                    <div>
                        {$FIELD_15_COMMENT_1}
                        <p>
                            <textarea name="descripcio_localitzacio" id="descripcio_localitzacio"></textarea>
                        </p>
                    </div>
                    <p>{$LABEL_4}</p>
                    <p>
                        <label>{$FIELD_4} *</label>
                        <input type="text" name="nom" id="nom" aria-required="true" aria-invalid="true">
                    </p>
                    <p>
                        <label>{$FIELD_5} *</label>
                        <input type="text" name="cognoms" id="cognoms" aria-required="true" aria-invalid="true">
                    </p>
                    <p>
                        <label>{$FIELD_6} *</label>
                        <input type="text" name="email" id="email" aria-required="true" aria-invalid="true">
                    </p>
                    <p>
                        <label>{$FIELD_7} *</label>
                        <input type="text" name="telefon" id="telefon" aria-required="true" aria-invalid="true">
                    </p>
                    <p>
                        <label>{$FIELD_8} </label>
                        <input type="text" name="adreca" id="adreca" aria-required="true" aria-invalid="true">
                    </p>
                    <p>
                        <label>{$FIELD_9} </label>
                        <input type="text" name="poblacio" id="poblacio" aria-required="true" aria-invalid="true">
                    </p>
                    <p>
                        <label>{$FIELD_10} </label>
                        <input type="text" name="codi_postal" id="codi_postal" aria-required="true" aria-invalid="true">
                    </p>
                    <p>
                        {$LABEL_TC3}
                        <div class="g-recaptcha" data-sitekey="6Lfdwh4TAAAAAKajPw-1Rj8msRd8YVZx14Vwova-" data-callback="recaptchaCallback"></div>
                        <label id="recaptcha-error">{$LABEL_15}</label>
                    </p>
                    <p>
                        <span>{$FIELD_15} <a href="JavaScript:obripopup();">{$FIELD_16}</a></span> <input type="checkbox" name="accept_privacy_policy" id="accept_privacy_policy" style="float: left">
                        <label id="privacy_policy_error">{$LABEL_17}</label>
                    </p>
                    <p>
                        <input name="submit" class="submit" type="submit" value="{$BUTTON_SUBMIT}">
                    </p>
                    <!--
                    <p class="footer-lopd">
                        {$LABEL_TC5}
                    </p>
                    -->
                </form>
            {else}
                {$LABEL_5} {$num_incidencia}.
            {/if}
		</div>
	
	</div>
</div>
</div>
<!-- The Modal -->
<div id="formulari_generic_modal" class="modal">
    <!-- Modal content -->
    <div class="modal-content">
        <span class="close">&times;</span>
        <p>{$FIELD_16}</p>
        <p style="font-size: 10px">{$LABEL_TC5}</p>
        <p>
            <input type="button" name="accept" value="{$LABEL_BUTTON_1}" id="popup_close">
        </p>
    </div>
</div>