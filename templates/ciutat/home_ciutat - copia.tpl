<div id="page-wrap" class="ciutat">
<div id="contciutattotalcos">
	<div id="contciutatcos">
		<div id="bciutatpresen">
			<ul id="bciutatmenu">
				<li >
				<a href="{$url_municipi}">
				<div id="ciutatbotoimatge">
					<img src="/images/botons/bmunicipi.jpg" />									
				</div>
				<div id="textbmenu1" class="textbotomciutat">
					{$LABEL_MENUV1A}
				</div>
				</a>
				</li>
				<li >
				<a href="{$url_estadistica_habitants}">
				<div id="ciutatbotoimatge">
					<img src="/images/botons/besta.jpg" />									
				</div>
				<div id="textbmenu2" class="textbotomciutat">
					{$LABEL_MENUV3}
					<br>
					{$LABEL_MENUV3B}
				</div>
				</a>
				</li>
				<li>
				<a href="{$url_imatges}">
				<div id="ciutatbotoimatge">
					<img src="/images/botons/bimatges.jpg" />									
				</div>
				<div id="textbmenu3" class="textbotomciutat">
					{$LABEL_MENUV1}
					<br>
					{$LABEL_MENUV1B}
				</div>
				</a>
				</li>
				<li >
				<a href="{$url_territori}">
				<div id="ciutatbotoimatge">
					<img src="/images/botons/bterritori.jpg" />									
				</div>
				<div id="textbmenu4" class="textbotomciutat">
					{$LABEL_MENUV2A}	
				</div>
				</a>
				</li>
			</ul>
			<div id="ciutatimatge">			
			<img src="/images/laciutat/laciutat.jpg" />			
			</div>
			<div id="ciutatimatgeText" class="imatgeCapText">
				<h1>
				{$LABEL_TITOL}&nbsp;&nbsp;
				</h1>
			</div>
		</div>
		<!--<div id="titolServeis" class="titolarea2">
			&nbsp;
			{$LABEL_TITOLS1}
		</div>
		<div id="titolMiscel" class="titolarea2">
			&nbsp;
			{$LABEL_TITOLS1B}
		</div>-->
		<a href="{$url_cultura}" target="_self" title="{$LABEL_BCIUTAT1}">
		<div id="bciutat1" class="marcfinestra2 bciutat">
				<img src="/images/botons/bciutatcul.jpg">
				<div class="banners3text" id="tciutat">
					{$LABEL_BCIUTAT1}	
				</div>					
		</div>
		</a>
		<a href="http://tortosasport.cat/" target="_blank" title="{$LABEL_BCIUTAT2}">
		<div id="bciutat2" class="marcfinestra2 bciutat">
				<img src="/images/botons/bciutatesport.jpg">
				<div class="banners3text" id="tciutat">
					{$LABEL_BCIUTAT2}	
				</div>					
		</div>
		</a>
		<a href="{$url_educacio}" target="_self" title="{$LABEL_BCIUTAT3}">
		<div id="bciutat3" class="marcfinestra2 bciutat">
				<img src="/images/botons/bciutatens.jpg">
				<div class="banners3text" id="tciutat">
					{$LABEL_BCIUTAT3}	
				</div>					
		</div>
		</a>
		<a href="{$url_salut}" target="_self" title="{$LABEL_BCIUTAT4}">
		<div id="bciutat4" class="marcfinestra2 bciutat">
				<img src="/images/botons/bciutatsalut.jpg">
				<div class="banners3text" id="tciutat">
					{$LABEL_BCIUTAT4}	
				</div>					
		</div>
		</a>
		<a href="{$url_transport}" target="_self" title="{$LABEL_BCIUTAT5}">
		<div id="bciutat5" class="marcfinestra2 bciutat">
				<img src="/images/botons/bciutattrans.jpg">
				<div class="banners3text" id="tciutat">
					{$LABEL_BCIUTAT5}	
				</div>					
		</div>
		</a>
		<a href="/festes/index.php" target="_self" title="{$LABEL_BCIUTAT6}">
		<div id="bciutat6" class="marcfinestra2 bciutat">
				<img src="/images/botons/bciutatfes.jpg">
				<div class="banners3text" id="tciutat">
					{$LABEL_BCIUTAT6}	
				</div>					
		</div>
		</a>
		<a href="{$url_ciutathistoria}" title="{$LABEL_BCIUTAT7}">
		<div id="bciutat7" class="marcfinestra2 bciutat">
				<img src="/images/botons/bciutathis.jpg">
				<div class="banners3text" id="tciutat">
					{$LABEL_BCIUTAT7}	
				</div>					
		</div>
		</a>
		<a href="/agermana/index.php" target="_self" title="{$LABEL_AGERMANAMENTS}">
		<div id="bciutat8" class="marcfinestra2 bciutat">
				<img src="/images/botons/bagermana.jpg">
				<div class="banners3text" id="tciutat">
					{$LABEL_AGERMANAMENTS}	
				</div>					
		</div>
		</a>
		<a href="/turisme-oci/index.php" target="_self" title="{$LABEL_BCIUTAT9}">
		<div id="bciutat9" class="marcfinestra2 bciutat">
				<img src="/images/botons/bciutattur.jpg">
				<div class="banners3text" id="tciutat">
					{$LABEL_BCIUTAT9}	
				</div>					
		</div>
		</a>
		<!--<div id="ServeisCiutat" class="marcServeisCiutat">
			<ul class="ulserveis">
				<li><a href="http://www.bibliotecaspublicas.es/tortosa/index.jsp" target="_blank">
				{$LABEL_SERVEIS9}</a></li>
				<li><a href="http://www.tortosa.cat/webajt/cfo/index.asp" target="_blank">
				{$LABEL_SERVEIS2}</a></li>
				<li><a href="/deixalleria/index.php">
				{$LABEL_SERVEIS3}</a></li>
				<li><a href="http://www.escolamunicipaldemusicadetortosa.cat/" target="_blank">
				{$LABEL_SERVEIS4}</a></li>
				<li><a href="http://www.escolamunicipaldeteatredetortosa.cat/" target="_blank">
				{$LABEL_SERVEIS5}</a></li>
				<li><a href="http://www.puntjove.tortosa.cat/" target="_blank">
				{$LABEL_SERVEIS7}</a></li>
				<li><a href="http://www.cpnl.cat/xarxa/cnlterresebre/" target="_blank">
				{$LABEL_SERVEIS8}</a></li>
				<li><a href="http://www.teatreauditoritortosa.cat/" target="_blank">
				{$LABEL_SERVEIS1}</a></li>
				<li><a href="/telecentre/index.php">
				{$LABEL_SERVEIS10}</a></li>
			</ul>
		</div>
		<div id="MiscelCiutat" class="marcServeisCiutat">
			<ul class="ulserveis">
				<li ><a href="/agermana/index.php">
				{$LABEL_MISCE1}</a></li>
				<li><a href="/butlletins/index.php">
				{$LABEL_MISCE2}</a></li>
				<li><a href="/ciutat/cuniversitaria.php">
				{$LABEL_MISCE3}</a></li>
				<li><a href="http://www.tortosa.cat/webajt/Regentitats2013.pdf" target="_blank">
				{$LABEL_MISCE4}</a></li>
				<li><a href="http://43500.tortosa.cat:8081/WebAJT/EstadistiquesHabitantsServlet" target="_blank">
				{$LABEL_MISCE5}</a></li>
				<li><a href="/ciutat/imatges.php">
				{$LABEL_MISCE6}</a></li>
				<li><a href="http://www.meteotortosa.cat/meteo/" target="_blank">
				{$LABEL_MISCE7}</a></li>
				<li><a href="/pam/index.php">
				{$LABEL_MISCE8}</a></li>
				<li><a href="/ciutat/planols.php">
				{$LABEL_MISCE9}</a></li>
				<li><a href="http://www.hife.es/ca-ES/LineaUrbana/8" target="_blank">
				{$LABEL_MISCE10}</a></li>
			</ul>
		</div>-->
		<div class="marcfinestra" id="marcbciutata">
			<div id="banner4ima">
					<a href="http://agenda.tortosa.cat/agenda-tortosa/" target="_blank"><img src="/images/banners/bnagenda2.jpg" title="Agenda" alt="Agenda"></a>			
			</div>
			<div class="banners4text" id="bannerlateraltext">
					{$LABEL_BAGENDAC}
			</div>
		</div>
		<div class="marcfinestra" id="marcbciutatb">
			<div id="banner4ima">
					<a href="http://www.tortosa.altanet.org/imact/agenda309.pdf" target="_blank"><img src="/images/banners/bnagecul.jpg" title="Agenda Cultural" alt="Agenda Cultural"></a>			
			</div>
			<div class="banners4text" id="bannerlateraltext">
					{$LABEL_BAGENDAC}
			</div>
			<div class="banners4text" id="banners4textp">
					{$LABEL_BAGENDACB}
			</div>
		</div>
		<div class="marcfinestra" id="marcbciutatc">
			<div id="banner4ima">
					<a href="http://www.museudetortosa.cat" target="_blank"><img src="/images/banners/bnmuseuf.jpg" title="Museu de Tortosa" alt="Museu de Tortosa"></a>			
			</div>
			<div class="banners4text" id="bannerlateraltext">
					{$LABEL_BMUSEUC}
			</div>
			<div class="banners4text" id="banners4textp">
					{$LABEL_BMUSEUCB}
			</div>
		</div>
		<div class="marcfinestra" id="marcbciutatd">
			<div id="banner4ima">
					<a href="http://www.teatreauditoritortosa.cat" target="_blank"><img src="/images/banners/bnteatref.jpg" title="Teatre Auditori Felip Pedrell" alt="Teatre Auditori Felip Pedrell"></a>			
			</div>
			<div class="banners4text" id="bannerlateraltext">
					{$LABEL_BTEATREC}
			</div>
			<div class="banners4text" id="banners4textp">
					{$LABEL_BTEATRECB}
			</div>
		</div>
		<div class="marcfinestra" id="marcbciutate">
			<div id="banner4ima">
					<a href="http://www.radiotortosa.cat" target="_blank"><img src="/images/banners/bnradiot.jpg" title="Ràdio Tortosa" alt="Ràdio Tortosa"></a>			
			</div>
			<div class="banners4text" id="bannerlateraltext">
					{$LABEL_BRADIOC}
			</div>
			<div class="banners4text" id="banners4textp">
					{$LABEL_BRADIOB}
			</div>
		</div>
		<div id="infgeneral">
			<div id="taulaNCiutat">
    			<div id="filaNCiutat">
    				
        			<div class="colNegocis" id="col1NCiutat"><a href="http://www.meteotortosa.cat/meteo/" target="_blank">{$LABEL_CLIMA}</a></div>
        			
        			
        			<div class="colNegocis" id="col1NCiutat"><a href="{$url_planols}">{$LABEL_PLANOLS}</a></div>
        			
        			
        			<div class="colNegocis" id="col1NCiutat"><a href="http://serveisoberts.gencat.cat/equipaments#?adreca.municipi=Tortosa" target="_blank">{$LABEL_EQUIPAMENTS}</a></div>
        			
        			
        			<div class="colNegocis" id="col1NCiutat"><a href="{$url_agermana}">{$LABEL_AGERMANAMENTS}</a></div>
        			
        			
        			<div class="colNegocis" id="col1NCiutat"><a href="http://documents.tortosa.cat/" target="_blank">{$LABEL_PATRIMONI}</a></div>
        			
    			</div>
    		</div>
		</div>
	</div>
</div>
</div>
