<html>
   <head>
      {include file="head.tpl"}
   </head>
   <body>
      {include file="header.tpl"}
      <div id="page-wrap">

         <div class="contenedor-responsive">

            <div id="conttranstotalcos">

               <div id="conttranscos">

                  <div id="btranspresen">

                     <div id="transimatge">
                        <img src="/images/agermana/tira.jpg" class="img-slider-territori"/>
                     </div>

                     <div id="transimatgeText">
                        <h1>
                           {$LABEL_TITOL1}
                           <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                        </h1>
                     </div>

                  </div>

                  <ul id='menu_planA'>
                     {foreach $MENU as $itemMenu}
                     <li id='menu_planA_item'>
                        <a href={$itemMenu}>
                        {$itemMenu@key}
                        </a>
                     </li>
                     {/foreach}
                  </ul>

                  <div id="tcentre_planA">
                     <h2>{$LABEL_TC1}</h2>
                     {if isset($LABEL_TC2)}
                     <p>{$LABEL_TC2}</p>
                     {/if}
                     {if isset($URL_TC1)}
                     <p><a href="http://{$URL_TC1}" target="_blank">{$URL_TC1}</a></p>
                     {/if}
                     {if isset($IMG_TC1)}
                     <p><img src="{$IMG_TC1}" alt="{$LABEL_TC1}" class="img-responsive"></p>
                     {/if}
                     {if isset($LABEL_TC3)}
                     <p>{$LABEL_TC3}</p>
                     {/if}
                     {if isset($LABEL_TC4)}
                     <p>{$LABEL_TC4}</p>
                     {/if}
                     {if isset($LABEL_TC5)}
                     <p>{$LABEL_TC5}</p>
                     {/if}
                     {if isset($LABEL_TC6)}
                     <p>{$LABEL_TC6}</p>
                     {/if}
                     {if isset($LABEL_TC7)}
                     <p>{$LABEL_TC7}</p>
                     {/if}
                     {if isset($LABEL_TC8)}
                     <p>{$LABEL_TC8}</p>
                     {/if}
                     {if isset($LABEL_TC9)}
                     <p>{$LABEL_TC9}</p>
                     {/if}
                  </div>

               </div>
            </div>
         </div>
      </div>
      {include file="footer.tpl"}
   </body>
</html>