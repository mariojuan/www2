<html>
   <head>
      {include file="head.tpl"}
   </head>
   <body>
      {include file="header.tpl"}
      <div id="page-wrap">

         <div class="contenedor-responsive">

            <div id="conttranstotalcos">

               <div id="conttranscos">

                  <div id="btranspresen">

                     <div id="transimatge">
                        <img src="/images/agermana/tira.jpg" class="img-slider-territori"/>
                     </div>

                     <div id="transimatgeText">
                        <h1>
                           {$LABEL_TITOL1}
                           <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                        </h1>
                     </div>

                  </div>

                  <ul id='menu_planA'>
                     {foreach $MENU as $itemMenu}
                     <li id='menu_planA_item'>
                        <a href={$itemMenu}>
                        {$itemMenu@key}
                        </a>
                     </li>
                     {/foreach}
                  </ul>

                  <div id="tcentre_planA">
                     <h2>{$LABEL_TC1}</h2>
                     <div>
                        <img src='../images/agermana/imatge.jpg' class="img-responsive">
                     </div>
                     <p>{$LABEL_TC2}</p>
                     <p>{$LABEL_TC3}</p>
                     <p>{$LABEL_TC4}</p>
                     <p>
                        {$LABEL_TC5} <a href="/agermana/agavunyo.php">{$LABEL_TC6}</a> {$LABEL_TC7}, <a href="/agermana/agalcanyis.php">{$LABEL_TC8}</a> {$LABEL_TC9}, <a href="/agermana/agvercelli.php">{$LABEL_TC10}</a> {$LABEL_TC11}, <a href="/agermana/aglepuy.php">{$LABEL_TC12}</a> {$LABEL_TC13}, <a href="/agermana/aglepuy.php">{$LABEL_TC14}</a> {$LABEL_TC15}
                     </p>
                  </div>

               </div>
            </div>
         </div>
      </div>
      {include file="footer.tpl"}
   </body>
</html>