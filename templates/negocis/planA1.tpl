<!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->

<div id="page-wrap">

<div class="contenedor-responsive">


<div id="conttranstotalcos">
	<div id="conttranscos">
		<div id="btranspresen">
			<div id="transimatge">			
			<img src="{$image}" class="img-slider-territori"/>			
			</div>
			<div id="transimatgeText">
				<h1>
				{$LABEL_TITOL1}
				<i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
				</h1>
			</div>
		</div>

		<ul id='menu_planA'>
		{foreach $MENU as $itemMenu}
			<a href={$itemMenu}>
			<li id='menu_planA_item'>
				{$itemMenu@key}
			</li>
			</a>
		{/foreach}
		</ul>

		<div id="tcentre_planA">
			<font style="margin-top:20px;" size=4 color=#666666>
			{$LABEL_TC1}
			</font>
			<br><br>
			<font size=2 color=#333333>
			<strong>
			{$LABEL_TC1_1}
			</strong>
			</font>
			<br><br>
			{$LABEL_TC1_11}
			<br><br>
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1546236.9239863667!2d-0.5995881688499914!3d40.80612581051421!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a0e4a4887de549%3A0x4750f0d9aa735b5c!2sTortosa%2C+Tarragona!5e0!3m2!1ses!2ses!4v1488975737835" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
			<br><br>
			<br><br>
			<font size=2 color=#333333>
			<strong>
			{$LABEL_TC1_2}
			</strong>		
			</font><br><br>
			{$LABEL_TC1_21}
			<ul class="Negocis_comunica">
				<li>{$LABEL_TC1_211}</li>
				<ul class="Negocis_comunica">
					<li>{$LABEL_TC1_2111}</li>
					<li>{$LABEL_TC1_2112}</li>
				</ul>
				<li>{$LABEL_TC1_212}</li>
				<ul class="Negocis_comunica">
					<li>{$LABEL_TC1_2121}</li>
					<li>{$LABEL_TC1_2122}</li>
				</ul>
				<li>{$LABEL_TC1_213}</li>
				<ul class="Negocis_comunica">
					<li>{$LABEL_TC1_2131}</li>
				</ul>
			</ul>
			{$LABEL_TC1_22}
			<br>
			{$LABEL_TC1_22b}
			<ul class="Negocis_comunica">
				<li>{$LABEL_TC1_221}</li>
				<li>{$LABEL_TC1_222}</li>
				<li>{$LABEL_TC1_223}</li>
				<li>{$LABEL_TC1_224}</li>
			</ul>
			{$LABEL_TC1_23}
			<br>
			{$LABEL_TC1_23b}
			<ul class="Negocis_comunica">
				<li>{$LABEL_TC1_231}</li>
				<li>{$LABEL_TC1_232}</li>
				<li>{$LABEL_TC1_233}</li>
				<li>{$LABEL_TC1_234}</li>
				<li>{$LABEL_TC1_235}</li>
			</ul>
			<br><br>
			<font size=2 color=#333333>
			<strong>
			{$LABEL_TC1_5}
			</strong>
			</font>
			<br><br>
			{$LABEL_TC1_51}
			<br><br><br>
			<font size=2 color=#333333>
			<strong>
			{$LABEL_TC1_6}
			</strong>
			</font>
			<br><br>
			{$LABEL_TC1_61}
			<br><br>
			{$LABEL_TC1_62}
			<br><br>
			<font size=2 color=#333333>
			<strong>
			<a href="http://ebrebiosfera.org" target="_blank">
				<img src="/images/negocis/banners/bbiosfera.jpg" class="img-responsive">	
			</a>
			</a>
			</strong>
			</font>
			<br><br><br>
			<font size=2 color=#333333>
			<strong>
			{$LABEL_TC1_4}
			</strong>
			</font>
			<br><br>
			{$LABEL_TC1_41}
			<br><br>
			{$LABEL_TC1_42}
			<ul class="Negocis_comunica">
				<a href="http://www.tortosa.altanet.org/planols_tortosa/BAIXEBRE.pdf" target="_blank">
				<li>{$LABEL_TC1_421}</li>
				</a>
				<li>{$LABEL_TC1_422}</li>
				<a href="http://www.tortosa.altanet.org/planols_tortosa/CatalunyaSud.pdf" target="_blank">
				<li>{$LABEL_TC1_423}</li>
				</a>
			</ul>
			<br>
			{$LABEL_TC1_433}<a href="{$URL_TC1_1}">{$LABEL_TC1_434}</a>
			<br><br><br>
			<font size=2 color=#333333>
			<strong>
			{$LABEL_TC1_3}
			</strong>
			</font>
			<br><br>
			{$LABEL_TC1_31}
			<br><br>
			<a href="https://www.seu-e.cat/web/tortosa/govern-obert-i-transparencia/accio-de-govern-i-normativa/normativa-plans-i-programes/ordenances-fiscals" target="_blank">{$LABEL_TC1_32}</a>
			
			
			
		</div>
  	</div>
</div>
</div>
</div>
<br><br><br><br>


