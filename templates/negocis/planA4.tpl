<!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->

<div id="page-wrap">

<div class="contenedor-responsive">

<div id="conttranstotalcos">
	<div id="conttranscos">
		<div id="btranspresen">
			<div id="transimatge">			
			<img src="{$image}" class="img-slider-territori"/>			
			</div>
			<div id="transimatgeText">
				<h1>
				{$LABEL_TITOL1}
				<i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
				</h1>
			</div>
		</div>
		<ul id='menu_planA'>
		{foreach $MENU as $itemMenu}
			<a href={$itemMenu}>
			<li id='menu_planA_item'>
				{$itemMenu@key}
			</li>
			</a>
		{/foreach}
		</ul>
		<div id="tcentre_planA">
			<font size=4 color=#666666>
			{$LABEL_TC1}
			</font>
			<br><br>
			<font size=2 color=#333333>
			<strong>
			{$LABEL_TC1_1}
			</strong>
			</font>
			<br><br>
			{$LABEL_TC1_11}
			<br><br>
			{$LABEL_TC1_12}
			<br><br>
			{$LABEL_TC1_13}
			<br><br>
			{$LABEL_TC1_14}
			<br><br>
			<font size=2 color=#444444>
			{$LABEL_TC1_2}
			</font>
			<br><br>

			<font size=2 color=#555555>
			<strong>
			{$LABEL_TC1_21}
			</strong>
			</font>
			<br><br>
			<div id="taulaNXifres">
    			<div id="filaNXifres">
        			<div class="colNegocis" id="col1NContacta">{$LABEL_TC1_2T1}</div>
        			<div class="colNegocis" id="col2NContacta">{$LABEL_TC1_211}</div>
    			</div>
    			<div id="filaNXifres">
        			<div class="colNegocis" id="col1NContacta">{$LABEL_TC1_2T2}</div>
        			<div class="colNegocis" id="col2NContacta">{$LABEL_TC1_212}</div>
    			</div>
    			<div id="filaNXifres">
        			<div class="colNegocis" id="col1NContacta">{$LABEL_TC1_2T3}</div>
        			<div class="colNegocis" id="col2NContacta">{$LABEL_TC1_213}</div>
    			</div>
    			<div id="filaNXifres">
        			<div class="colNegocis" id="col1NContacta">{$LABEL_TC1_2T4}</div>
        			<div class="colNegocis" id="col2NContacta">{$LABEL_TC1_214}</div>
    			</div>
    			<div id="filaNXifres">
        			<div class="colNegocis" id="col1NContacta">{$LABEL_TC1_2T5}</div>
        			<div class="colNegocis" id="col2NContacta">{$LABEL_TC1_215}</div>
    			</div>
    			<div id="filaNXifres">
        			<div class="colNegocis" id="col1NContacta">{$LABEL_TC1_2T6}</div>
        			<div class="colNegocis" id="col2NContacta">
        				<a href="http://www.tortosa.altanet.org/planols_tortosa/CatalunyaSud.pdf" target="_blank">
        				<img src="/images/negocis/ipolcs.jpg">
        				</a>
        			</div>
    			</div>
			</div>
			<br><br>

			<font size=2 color=#555555>
			<strong>
			{$LABEL_TC1_31}
			</strong>
			</font>
			<br><br>
			<div id="taulaNXifres">
    			<div id="filaNXifres">
        			<div class="colNegocis" id="col1NContacta">{$LABEL_TC1_2T1}</div>
        			<div class="colNegocis" id="col2NContacta">{$LABEL_TC1_311}</div>
    			</div>
    			<div id="filaNXifres">
        			<div class="colNegocis" id="col1NContacta">{$LABEL_TC1_2T2}</div>
        			<div class="colNegocis" id="col2NContacta">{$LABEL_TC1_312}</div>
    			</div>
    			<div id="filaNXifres">
        			<div class="colNegocis" id="col1NContacta">{$LABEL_TC1_2T3}</div>
        			<div class="colNegocis" id="col2NContacta">{$LABEL_TC1_313}</div>
    			</div>
    			<div id="filaNXifres">
        			<div class="colNegocis" id="col1NContacta">{$LABEL_TC1_2T4}</div>
        			<div class="colNegocis" id="col2NContacta">{$LABEL_TC1_314}</div>
    			</div>
    			<div id="filaNXifres">
        			<div class="colNegocis" id="col1NContacta">{$LABEL_TC1_2T5}</div>
        			<div class="colNegocis" id="col2NContacta">{$LABEL_TC1_315}</div>
    			</div>
    			<div id="filaNXifres">
        			<div class="colNegocis" id="col1NContacta">{$LABEL_TC1_2T6}</div>
        			<div class="colNegocis" id="col2NContacta">
        				<a href="http://www.tortosa.altanet.org/planols_tortosa/BAIXEBRE.pdf" target="_blank">
        				<img src="/images/negocis/ipolbe.jpg">
        				</a>
        			</div>
    			</div>
			</div>
			<br><br>

			<font size=2 color=#555555>
			<strong>
			{$LABEL_TC1_41}
			</strong>
			</font>
			<br><br>
			<div id="taulaNXifres">
    			<div id="filaNXifres">
        			<div class="colNegocis" id="col1NContacta">{$LABEL_TC1_2T1}</div>
        			<div class="colNegocis" id="col2NContacta">{$LABEL_TC1_411}</div>
    			</div>
    			<div id="filaNXifres">
        			<div class="colNegocis" id="col1NContacta">{$LABEL_TC1_2T2}</div>
        			<div class="colNegocis" id="col2NContacta">{$LABEL_TC1_412}</div>
    			</div>
    			<div id="filaNXifres">
        			<div class="colNegocis" id="col1NContacta">{$LABEL_TC1_2T3}</div>
        			<div class="colNegocis" id="col2NContacta">{$LABEL_TC1_413}</div>
    			</div>
    			<div id="filaNXifres">
        			<div class="colNegocis" id="col1NContacta">{$LABEL_TC1_2T4}</div>
        			<div class="colNegocis" id="col2NContacta">{$LABEL_TC1_414}</div>
    			</div>
    			<div id="filaNXifres">
        			<div class="colNegocis" id="col1NContacta">{$LABEL_TC1_2T5}</div>
        			<div class="colNegocis" id="col2NContacta">{$LABEL_TC1_415}</div>
    			</div>
			</div>

			<br><br>
			<font size=2 color=#333333>
			<strong>
			{$LABEL_TC1_5}
			</strong>
			</font>
			<br><br>
			{$LABEL_TC1_51}
			<br><br>
			{$LABEL_TC1_52}
			<br><br>
			<font size=2 color=#666666>
			<a href="http://www.tortosaturisme.cat/que-ver/el-mercado-municipal-1884-1887" target="_blank">
			{$LABEL_TC1_53}
			</a>
			</font>
			<br><br>
			<a href="http://www.tortosaturisme.cat/que-ver/el-mercado-municipal-1884-1887" target="_blank">
				<img src="/images/negocis/mercat.jpg" class="img-responsive">
			</a>
			<br><br>
			{$LABEL_TC1_54}
			<br><br><br>
			<font size=2 color=#333333>
			<strong>
			{$LABEL_TC1_6}
			</strong>
			</font>
			<br><br>
			{$LABEL_TC1_61}
			<br><br>
			{$LABEL_TC1_62}
			<br><br>
		</div>
  	</div>
  	</div>
</div>
</div>
<br><br><br><br>


