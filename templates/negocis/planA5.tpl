<!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->
<div id="page-wrap">
   <div class="contenedor-responsive">
      <div id="conttranstotalcos">
         <div id="conttranscos">
            <div id="btranspresen">
               <div id="transimatge">           
                  <img src="{$image}" class="img-slider-territori"/>            
               </div>
               <div id="transimatgeText">
                  <h1>
                     {$LABEL_TITOL1}
                     <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                  </h1>
               </div>
            </div>
            <ul id='menu_planA'>
               {foreach $MENU as $itemMenu}
               <a href={$itemMenu}>
                  <li id='menu_planA_item'>
                     {$itemMenu@key}
                  </li>
               </a>
               {/foreach}
            </ul>
            <div id="tcentre_planA">
               <font size=4 color=#666666>
               {$LABEL_TC1}
               </font>
               <br><br>
               <div class="div-contenedor-banners-negocis5">
                  <div class="banner-negocis5">
                     <a href="http://www.cambratortosa.com/" target="_blank">
                     <img src="/images/negocis/banners/bccomer.jpg">
                     </a>
                  </div>
                  <div class="banner-negocis5 ">
                     <a href="http://tortosames.com/" target="_blank">
                     <img src="/images/negocis/banners/bfcom.jpg">
                     </a>
                  </div>
                  <div class="banner-negocis5 ">
                     <a href="http://www.idece.cat/" target="_blank">
                     <img src="/images/negocis/banners/bidece.jpg">
                     </a>
                  </div>
                  <div class="banner-negocis5 ">
                     <a href="http://www.urv.cat/es/vida-campus/campus/terres-ebre/" target="_blank">
                     <img src="/images/negocis/banners/burv.jpg">
                     </a>
                  </div>
                  <div class="banner-negocis5 ">
                     <a href="http://www2.uned.es/ca-tortosa/" target="_blank">
                     <img src="/images/negocis/banners/buned.jpg">
                     </a>
                  </div>
                  <div class="banner-negocis5 ">
                     <a href="http://www.uoc.edu/portal/es/universitat/contacte-seus/on-som/tortosa/index.html" target="_blank">
                     <img src="/images/negocis/banners/buoc.jpg">
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<br><br><br><br>