<!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->

<div id="page-wrap">

<div class="contenedor-responsive">

<div id="conttranstotalcos">
	<div id="conttranscos">
		<div id="btranspresen">
			<div id="transimatge">			
			<img src="{$image}" class="img-slider-territori"/>			
			</div>
			<div id="transimatgeText">
				<h1>
				{$LABEL_TITOL1}
				<i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
				</h1>
			</div>
		</div>
		<ul id='menu_planA'>
		{foreach $MENU as $itemMenu}
			<a href={$itemMenu}>
			<li id='menu_planA_item'>
				{$itemMenu@key}
			</li>
			</a>
		{/foreach}
		</ul>
		<div id="tcentre_planA">
			<font size=4 color=#666666>
			{$LABEL_TC1}
			</font>
			<br><br>
			<div id="taulaNXifres">
    			<div id="filaNXifres">
        			<div class="colNegocis" id="col1NXifres">{$LABEL_TC1_1}</div>
        			<div class="colNegocis" id="col2NXifres">33.743</div>
    			</div>
    			<div id="filaNXifres">
        			<div class="colNegocis" id="col1NXifres">{$LABEL_TC1_2}</div>
        			<div class="colNegocis" id="col2NXifres">6.203 (2015)</div>
    			</div>
    			<div id="filaNXifres">
        			<div class="colNegocis" id="col1NXifres">{$LABEL_TC1_3}</div>
        			<div class="colNegocis" id="col2NXifres">218,5km2</div>
    			</div>
    			<div id="filaNXifres">
        			<div class="colNegocis" id="col1NXifres">{$LABEL_TC1_4}</div>
        			<div class="colNegocis" id="col2NXifres">26,8 (milers €) (2014)</div>
    			</div>
    			<div id="filaNXifres">
        			<div class="colNegocis" id="col1NXifres">{$LABEL_TC1_6}</div>
        			<div class="colNegocis" id="col2NXifres">748 (idescat)</div>
    			</div>
    			<div id="filaNXifres">
        			<div class="colNegocis" id="col1NXifres">{$LABEL_TC1_7}</div>
        			<div class="colNegocis" id="col2NXifres">34.026 (of. Turisme 2016)</div>
    			</div>
			</div>
		</div>
  	</div>
  	</div>
</div>
</div>
<br><br><br><br>


