<!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->

<div id="page-wrap">

<div class="contenedor-responsive">

<div id="conttranstotalcos">
	<div id="conttranscos">
		<div id="btranspresen">
			<div id="transimatge">			
			<img src="{$image}" class="img-slider-territori"/>			
			</div>
			<div id="transimatgeText">
				<h1>
				{$LABEL_TITOL1}
				<i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
				</h1>
			</div>
		</div>

		<ul id='menu_planA'>
		{foreach $MENU as $itemMenu}
			<a href={$itemMenu}>
			<li id='menu_planA_item'>
				{$itemMenu@key}
			</li>
			</a>
		{/foreach}
		</ul>

		<div id="tcentre_planA">
			<font style="margin-top: 20px;" size=4 color=#666666>
			{$LABEL_TC1}
			</font>
			<br><br>

			<p><b>{$LABEL_TC1_1}:</b> 43500 - Tortosa</p>
			<p><b>{$LABEL_TC1_2}:</b> Carretera de la Nova Estació, 21</p>
			<p><b>{$LABEL_TC1_3}:</b> 663377751 – 977585836</p>
			<p><b>{$LABEL_TC1_4}:</b> ceviver@tortosa.cat</p>
			<p><b>{$LABEL_TC1_5}:</b> <a href="http://viverempreses.tortosa.cat">viverempreses.tortosa.cat</a></p>    			
    			
			</div>
		</div>
		
  	</div>
  	</div>
</div>
</div>
<br><br><br><br>


