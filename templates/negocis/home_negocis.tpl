<div id="page-wrap">



   <div id="contenidortotalcos">

      <!-- FOTOS SLIDER -->
      <div class="rowCartellera">
         <div id="bciutatpresen" class="bciutatpresen-rectificat">

            <div id="ciutatimatge ciutatimatge-rectificat">
               <img src="/images/negocis/cap.jpg" class="img-slider-negocis"/>
            </div>
            <div id="negocisimatgeText" class="imatgeCapText">
               <h1>{$LABEL_TITOL}&nbsp;&nbsp;</h1>
            </div>

         </div>
      </div>
      <!--/ FOTOS SLIDER -->

      <!-- TEXT -->
      <div class="row">

         <p class="paragraf-personalitzat">{$LABEL_TEXTINTRO}</p>
         
         {if $LABEL_TEXTINTRO1!="" || $LABEL_TEXTINTRO2!="" || $LABEL_TEXTINTRO3!=""}
            <p class="paragraf-personalitzat">&#9679; {$LABEL_TEXTINTRO1}</p>
            <p class="paragraf-personalitzat">&#9679; {$LABEL_TEXTINTRO2}</p>
            <p class="paragraf-personalitzat">&#9679; {$LABEL_TEXTINTRO3}</p>
         {/if}
         
      </div>
      <!--/ TEXT -->

      <!-- NEGOCIS I AMBITS -->
      <div class="row">

         <div class="column-left-negocis">

            <div class="titolsSeccionsHome titolBig marge-titol-contingut">
               {$LABEL_TITOLS1B}
            </div>

            <div class="marc-serveis-ciutat">
               <ul class="negocis-ajuntament-list">
                  <li>
                     <a href="{$URL_HOME1}">
                        {$LABEL_ADM1}
                     </a>
                  </li>
                  <li>
                     <a href="{$URL_HOME2}">
                        {$LABEL_ADM2}
                     </a>
                  </li>
                   
                  <li>
                     <a href="{$URL_HOME4}">
                        {$LABEL_ADM4}
                     </a>
                  </li>
                  <li>
                     <a href="{$URL_HOME5}">
                        {$LABEL_ADM5}
                     </a>
                  </li>
                  <li>
                     <a href="{$URL_HOME3}">
                        {$LABEL_ADM3}
                     </a>
                  </li>
               </ul>
            </div>

         </div>

         <div class="column-right-negocis">

            <div class="titolsSeccionsHome titolBig marge-titol-contingut">
               {$LABEL_TITOLS1}
            </div>

            <a href="http://www.firatortosa.cat/">
                <div class="marcArees arees-negocis">
                    <img src="/images/negocis/bfira.jpg" border="0" />
                </div>
            </a>

            <a href="http://viverempreses.tortosa.cat/">
                <div class="marcArees arees-negocis arees-negocis-last">
                    <img src="/images/negocis/bviver.jpg" />
                </div>
            </a>

            <a href="http://www.tortosaturisme.cat/">
                <div class="marcArees arees-negocis">
                    <img src="/images/negocis/bturisme.jpg" />
                </div>
            </a>

            <a href="https://agencia.tortosa.cat/">
                <div class="marcArees arees-negocis arees-negocis-last">
                    <img src="/images/negocis/bcfo.jpg" />
                    <div id="textNboto2b" class="textbotoa">
                        {$LABEL_BOTOA4}
                    </div>
                    <div id="textN2boto2b" class="textbotoa">
                        {$LABEL_BOTOA4B}
                    </div>
                </div>
            </a>
            
         </div>
         
      </div>
      <!--/ NEGOCIS I AMBITS -->

   </div>
</div>