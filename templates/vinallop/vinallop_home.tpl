<!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->
<div id="page-wrap">

   <div class="contenedor-responsive">

      <div id="conttranstotalcos">

         <div id="conttranscos">

            <div id="btranspresen">

               <div id="transimatge">			
                  <img src="{$image}" class="img-slider-territori"/>			
               </div>

               <div id="transimatgeText">
                  <h1>
                     {$LABEL_TITOL1}
                     <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                  </h1>
               </div>

            </div>

            <ul id='menu_planA'>
               {foreach $MENU as $itemMenu}
               <a href={$itemMenu}>
                  <li id='menu_planA_item'>
                     {$itemMenu@key}
                  </li>
               </a>
               {/foreach}
            </ul>

            <div id="tcentre_planA">
               <font size=4 color=#666666>{$LABEL_TITOL0}</font>
               {if $item==1}
               <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong>{$LABEL_TC1}</strong></p>
               <p>{$LABEL_TC2}</p>
               <p>{$LABEL_TC3}</p>
               <p>{$LABEL_TC4}</p>
               <p>{$LABEL_TC5}&nbsp;<a href="mailto:vinallop@tortosa.cat" target="_blank">vinallop@tortosa.cat</a></p>
               <p>{$LABEL_TC6}</p>
               <br><br>
               <div class="marcterme"><img src="/images/vinallop/mapa.jpg" class="img-responsive"></div>
               {else if $item==2}
               <ul class="subvencions1">
                  <li>
                     <i class="icon-location"></i>&nbsp;<strong>{$LABEL_TC1}</strong>
                     <ul>
                        <li>{$LABEL_TC2}</li>
                        <li>{$LABEL_TC3}</li>
                     </ul>
                  </li>
                  <li>
                     <i class="icon-location"></i>&nbsp;<strong>{$LABEL_TC4}</strong>
                     <ul>
                        <li>{$LABEL_TC5}</li>
                        <li>{$LABEL_TC6}</li>
                        <li>{$LABEL_TC7}</li>
                     </ul>
                  </li>
                  <li>
                     <i class="icon-location"></i>&nbsp;<strong>{$LABEL_TC8}</strong>
                     <ul>
                        <li>{$LABEL_TC9}</li>
                        <li>{$LABEL_TC10}</li>
                        <li>{$LABEL_TC11}</li>
                     </ul>
                  </li>
                  <li>
                     <i class="icon-location"></i>&nbsp;<strong>{$LABEL_TC12}</strong>
                  </li>
               </ul>
               {else if $item==3}
               <p>{$LABEL_TC1}</p>
               <p>{$LABEL_TC2}</p>
               <p>{$LABEL_TC3}</p>
               <p>{$LABEL_TC4}</p>
               <p>{$LABEL_TC5}</p>
               <p>{$LABEL_TC6}</p>
               <p>{$LABEL_TC7}</p>
               <p>{$LABEL_TC8}</p>
               <p>{$LABEL_TC9}</p>
               <p>{$LABEL_TC10}</p>
               <p>{$LABEL_TC11}</p>
               <p>{$LABEL_TC12}</p>
               {else if $item==4}
               <p>{$LABEL_TC1}</p>
               <p>{$LABEL_TC2}<strong>{$LABEL_TC3}</strong>{$LABEL_TC4}</p>
               <p>{$LABEL_TC5}<strong>{$LABEL_TC6}</strong>{$LABEL_TC7}</p>
               <p>{$LABEL_TC8}<strong>{$LABEL_TC9}</strong>{$LABEL_TC10}</p>
               <p>{$LABEL_TC11}<strong>{$LABEL_TC12}</strong>{$LABEL_TC13}<strong>{$LABEL_TC14}</strong>{$LABEL_TC15}</p>
               <p><strong>{$LABEL_TC16}</strong>{$LABEL_TC17}</p>
               <p>{$LABEL_TC18}<strong>{$LABEL_TC19}</strong>{$LABEL_TC20}</p>
               {else if $item==5}
               <p>{$LABEL_TC1}</p>
               {/if}
            </div>

         </div>
      </div>
   </div>
</div>