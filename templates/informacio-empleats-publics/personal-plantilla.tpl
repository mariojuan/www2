<html>
   <head>
      {include file="head.tpl"}
   </head>
   <body>
      {include file="header.tpl"}
      <div id="page-wrap">
         <div class="contenedor-responsive">
            <div id="conttranstotalcos">
               <div id="conttranscos">
                  <div id="btranspresen">
                     <div id="transimatge">
                        <img src="{$image}" class="img-slider-territori"/>
                     </div>
                     <div id="transimatgeText">
                        <h1>
                           {$LABEL_TITOL1}
                           <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                        </h1>
                     </div>
                  </div>
                  <!-- Menu -->
                  {include file="informacio-empleats-publics/menu.tpl"}
                  <!--// Menu -->
                  <div id="tcentre_planA">
                     <font size="4" color="#666666">{$LABEL_TC0}</font>
                     <ul class="llista_educacio">
                        <a href="http://www.tortosa.cat/webajt/pressupost/2018/2018Plantilla.pdf" target="_blank">
                        <li>
                           <i class="icon-file-pdf"></i>&nbsp;{$LABEL_TC4}
                        </li>
                        </a>
                        <a href="http://www.tortosa.cat/webajt/pressupost/2018/2018PlantillaModif.pdf" target="_blank">
                        <li>
                           <i class="icon-file-pdf"></i>&nbsp;{$LABEL_TC5}
                        </li>
                        </a>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      {include file="footer.tpl"}
   </body>
</html>