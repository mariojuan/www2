<html>
	<head>

        {include file="head.tpl"}

	</head>
	<body>

		{include file="header.tpl"}

		<div id="page-wrap">

            <div class="contenedor-responsive">

    <div id="conttranstotalcos">
        <div id="conttranscos">
            <div id="btranspresen">
                <div id="transimatge">
                    <img src="{$image}" class="img-slider-territori"/>
                </div>
                <div id="transimatgeText">
                    <h1>
                        {$LABEL_TITOL1}
                        <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                    </h1>
                </div>
            </div>

            <!-- Menu -->
                {include file="informacio-empleats-publics/menu.tpl"}
            <!--// Menu -->

            <div id="tcentre_planA">

                <font size="4" color="#666666">{$LABEL_TC0}</font>

                <p>{$LABEL_TC4}</p>

                <!-- Personal -->
                
                <table>
                <thead>
                  <tr>
                    <th>{$LABEL_FOTO}</th>
                    <th>{$LABEL_NOM}</th>
                    <th>{$LABEL_CARREC}</th>
                    <th>@</th>
                    </tr>
                    </thead>
                    {foreach $ARRAY_PERSONAL as $item}
                    <tr>
                        <td><a href="{$item['link']}" target="{$item['target']}"><img src="{$item['pic']}" class="avatar"/></a></td>
                        <td><a href="{$item['link']}" target="{$item['target']}">{$item['name']}</a></td>
                        <td>{$item['occupation']}</td>
                        <td><a href="mailto:{$item['correu']}"><i class="icon-mail"></i></a></td>
                    </tr>
                    {/foreach}
                </table>
                
            </div>
        </div>
        </div>
    </div>
</div>

		{include file="footer.tpl"}

	</body>
</html>