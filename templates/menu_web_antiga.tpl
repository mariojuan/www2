{if $MENU|@count>0}
	<div id="menu_planA">
        {if $apartat=="eleccions"}
		<select name="eleccions" id="eleccions">
			{foreach from=$eleccions key=key item=value}
				{if $value->IdEleccio==$id_eleccio}
					<option value="{$value->IdEleccio}" selected>{$value->Denominacio}</option>
				{else}
					<option value="{$value->IdEleccio}">{$value->Denominacio}</option>
				{/if}
			{/foreach}	
		</select>
        {/if}
		{foreach from=$MENU key=key item=value}
			{if $key!='Electes 2015'}
				{if $value[0]==""}
					<p class="titol_menu">{$key}</p>
				{elseif $value[1]!=""}
					<p>- <a href="{$value[0]}" target="{$value[1]}">{$key}</a></p>
				{else}
					<p>- <a href="{$value[0]}">{$key}</a></p>
				{/if}
			{else}
				{if $id_eleccio=='2015MUN'}
					<p>- <a href="{$value[0]}">{$key}</a></p>
				{/if}
			{/if}
		{/foreach}
        {if $apartat=="memoria-democratica"}
            <form name="cerca" id="cerca" action="memoria-democratica.php" method="post">
                <input type="hidden" name="accio" id="accio" value="list">
                <p>
                    <label>{$lblName}</label>
                    <input type="text" name="nom" id="nom" value=""">
                </p>
                <p>
                    <label>{$lblCategory}</label>
                    <select name="categoria" id="categoria">
                        <option value="">-- {$lblSelect} --</option>
                        {foreach $categories as $category}
                            <option value="{$category->CATEGORIA}">{$category->CATEGORIA}</th>
                        {/foreach}
                    </select>
                </p>
                <p id="buttons_container">
                    <input type="submit" name="consultar" id="consultar" value="Consultar">
                    <input type="reset" name="esborrar" id="esborrar" value="Esborrar">
                </p>
            </form>
            {foreach from=$MENU_MEM_DEM key=key item=value}
                {if $value[0]==""}
                    <p class="titol_menu">{$key}</p>
                {elseif $value[1]!=""}
                    <p>- <a href="{$value[0]}" target="{$value[1]}">{$key}</a></p>
                {else}
                    <p>- <a href="{$value[0]}">{$key}</a></p>
                {/if}
            {/foreach}
        {/if}
	</div>
{/if}