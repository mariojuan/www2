<html>
	<head>
        {include file="head.tpl"}
	</head>
	<body>
		{include file="header.tpl"}
        <div id="page-wrap">

        <div class="contenedor-responsive">

            <div id="conttranstotalcos">
                <div id="conttranscos">
                    <div id="btranspresen">
                        <div id="transimatge">
                            <img src=/images/laciutat/tira.jpg / class="img-slider-territori">
                        </div>
                        <div id="transimatgeText">
                            <h1>
                                {$LABEL_TITOL1}
                            </h1>
                        </div>
                    </div>
                    <div id="tcentre_planA" style="left: 0 !important;">
                        <p>{$LABEL_TC1}</p>
                        <p>{$LABEL_TC2} <a href="{$URL_1}">{$LABEL_TC3}</a> {$LABEL_TC4}</p>
                        <p><a href="{$URL_2}">{$LABEL_TC5}</a> {$LABEL_TC6}</p>

                        <h2 style="margin-top: 50px">{$LABEL_TITOL1_ES}</h2>
                        <p>{$LABEL_TC1_ES}</p>
                        <p>{$LABEL_TC2_ES} <a href="{$URL_1_ES}">{$LABEL_TC3_ES}</a> {$LABEL_TC4_ES}</p>
                        <p><a href="{$URL_2_ES}">{$LABEL_TC5_ES}</a> {$LABEL_TC6_ES}</p>

                        <h2 style="margin-top: 50px">{$LABEL_TITOL1_EN}</h2>
                        <p>{$LABEL_TC1_EN}</p>
                        <p>{$LABEL_TC2_EN} <a href="{$URL_1_EN}">{$LABEL_TC3_EN}</a> {$LABEL_TC4_EN}</p>
                        <p><a href="{$URL_2_EN}">{$LABEL_TC5_EN}</a> {$LABEL_TC6_EN}</p>
                    </div>

                    <div class="separator"></div>
                </div>
            </div>

        </div>            

        </div>
		{include file="footer.tpl"}
	</body>
</html>