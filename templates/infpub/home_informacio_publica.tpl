<!-- PLANTILLA SENSE MENÚ A L'ESQUERRA AMB FILES DE 3 COLUMNES DE CAIXES/BOTONS -->

<!--<link rel='stylesheet' href='/fonts/fontello-a238a014/css/fontello.css'>
<link rel='stylesheet' href='/fonts/fontello-a238a014/css/animation.css'>-->

<div id="page-wrap">

<div class="contenedor-responsive">

<div id="conttranstotalcos">
	<div id="conttranscos">
		<div id="btranspresen">
			<div id="transimatge">			
				<img src="{$image}" class="img-slider-territori">			
			</div>
            <div id="transimatgeText">
				<h1>
				{$LABEL_TITOL1}
				</h1>
	       </div>
	   </div>


        <div id="tcentre_municipi">
                {$LABEL_TC1}    

                <br>
                <br>

                {if $elements|count>0}

                <ul class="subvencions1 paddingleft0">

               {foreach from=$elements key=key item=value}
                  <li>
                    <a href="http://www.tortosa.cat/webajt/gestiointerna/inf_publica/documents/{utf8_encode($value->NOM_FITXER)}" target="_blank"  style="text-decoration:none; color:#000000">                       
                    <i class="icon-file-pdf"></i>
                    &nbsp;
                    {utf8_encode($value->TITOL)}
                    </a>
                  </li>    
                {/foreach}

                {else} 
                  <br>
                  <br>
                  <li>
                    {$LABEL_TC2}
                  </li>    
                {/if}
              </ul>

        <br>

        <div id="pagination">
            <ul>
                {if $lastpage>1}
                    {if $page> 1}
                        {assign var="page_ant" value=$page - 1}
                        <li><a href="index.php?lang={$lang}&accio=list&page={$page_ant}">{$lblAnt}</a></li>
                    {/if}
                    {assign var="desp" value=$page + 4}
                    {assign var="firstpage" value=$page - 4}

                    {if $desp < $lastpage}
                        {assign var="lastpage" value=$page + 4}
                    {/if}
                    {if $firstpage < 1}
                        {assign var="firstpage" value=1}
                    {/if}

                    {for $counter=$firstpage to $lastpage}
                        {if $counter==$page}
                            <li>{$counter}</li>
                        {else}
                            <li><a href="index.php?lang={$lang}&accio=list&page={$counter}">{$counter}</a></li>
                        {/if}
                    {/for}
                    {if $page < $lastpage}
                        {assign var="page_seg" value=$page + 1}
                        <li><a href="index.php?lang={$lang}&accio=list&page={$page_seg}">{$lblSeg}</a></li>
                    {/if}
                {else}
                  <li>1</li>
                {/if}
            </ul>
        </div>

         </div>

    </div>
</div>
</div>
</div>







