<html>
<head>
    {include file="head.tpl"}
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script type="text/javascript">
        var captcha_value = false;
        $().ready(function() {
            $("#EscriuAlcaldeForm").validate({
                rules: {
                    nom:  {
                        required: true,
                        maxlength: 150
                    },
                    email:  {
                        required: true,
                        email: true
                    },
                    poblacio:   {
                        required: true
                    },
                    assumpte:   {
                        required: true
                    },
                    comentari:   {
                        required: true
                    }
                },
                messages: {
                    nom: {
                        required: "{$FIELD_1_REQUIRED}",
                        maxlength: "{$FIELD_1_OPTION}"
                    },
                    email: {
                        required: "{$FIELD_2_REQUIRED}",
                        email: "{$FIELD_2_EMAILVALID}"
                    },
                    poblacio: {
                        required: "{$FIELD_3_REQUIRED}"
                    },
                    assumpte: {
                        required: "{$FIELD_6_REQUIRED}"
                    },
                    comentari: {
                        required: "{$FIELD_7_REQUIRED}"
                    }
                },
                submitHandler: function(form) {
                    captcha();
                    if(captcha_value) {
                        form.submit();
                    }
                }
            });
        });

        function captcha() {
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    var out = this.responseText;
                    if(out=='1') {
                        captcha_value = true;
                    }
                    else {
                        $('#recaptcha-error').css('display', 'block');
                    }
                }
            };
            xmlhttp.open("POST", "http://www2.tortosa.cat/libs/verify-recaptcha.php", false);
            xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            xmlhttp.send("g-recaptcha-response="+grecaptcha.getResponse());
        }
    </script>
</head>
<body>
    {include file="header.tpl"}
    <div id="page-wrap">
    <div class="contenedor-responsive">
        <div id="conttranstotalcos">
            <div id="conttranscos">
                <div id="btranspresen">
                    <div id="transimatge">
                        <img src="/images/alcalde/palcalde.jpg" class="img-slider-territori"/>
                    </div>
                    <div id="transimatgeText">
                        <h1>
                            {$LABEL_TITOL1}
                            <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                        </h1>
                    </div>
                </div>
                <ul id='menu_planA'>
                    {foreach $MENU as $itemMenu}
                        <a href={$itemMenu}>
                            <li id='menu_planA_item'>
                                {$itemMenu@key}
                            </li>
                        </a>
                    {/foreach}
                </ul>
                <div id="tcentre_planA">
                    <p class="title escriu_alcalde">{$LABEL_TC1}</p>
                    {if !$request}
                    <p>{$LABEL_TC6}</p>
                    <p>{$LABEL_TC7}</p>
                    <form id="EscriuAlcaldeForm" method="post" action="alccontac.php?lang={$lang}" novalidate="novalidate">
                        <input type="hidden" name="accio" id="accio" value="edit_headlines">
                        <p>
                            <label>{$FIELD_1} *</label>
                            <input type="text" name="nom" id="nom" value="" aria-required="true" class="error" aria-invalid="true" maxlength="50">
                        </p>
                        <p>
                            <label>{$FIELD_2} *</label>
                            <input type="text" name="email" id="email" value="" aria-required="true" class="error" aria-invalid="true" maxlength="75">
                        </p>
                        <p>
                            <label>{$FIELD_3} *</label>
                            <input type="text" name="poblacio" id="poblacio" value="" aria-required="true" class="error" aria-invalid="true" maxlength="75">
                        </p>
                        <p>
                            <label>{$FIELD_4}</label>
                            <input type="text" name="comarca" id="comarca" value="" aria-required="true" class="error" aria-invalid="true" maxlength="75">
                        </p>
                        <p>
                            <label>{$FIELD_5}</label>
                            <input type="text" name="pais" id="pais" value="" aria-required="true" class="error" aria-invalid="true" maxlength="75">
                        </p>
                        <p>
                            <label>{$FIELD_6} *</label>
                            <input type="text" name="assumpte" id="assumpte" value="" aria-required="true" class="error" aria-invalid="true" maxlength="75">
                        </p>
                        <p>
                            <label>{$FIELD_7} *</label>
                            <textarea name="comentari" id="comentari" aria-required="true" class="error" aria-invalid="true"></textarea>
                        </p>
                        <p>
                            {$LABEL_TC3}
                            <div class="g-recaptcha" data-sitekey="6Lfdwh4TAAAAAKajPw-1Rj8msRd8YVZx14Vwova-"></div>
                            <label id="recaptcha-error">{$FIELD_9_REQUIRED}</label>
                        </p>
                        <p>
                            <input class="submit" type="submit" value="{$BUTTON_SUBMIT}">
                        </p>
                        <p>
                            <label>* {$FIELD_8_REQUIRED}</label>
                        </p>
                        <p class="footer-lopd">
                            {$LABEL_TC5}
                        </p>
                    </form>
                    {else}
                        {$LABEL_TC4}
                    {/if}
                </div>
            </div>
        </div>
        </div>
    </div>
    {include file="footer.tpl"}
</body>
</html>