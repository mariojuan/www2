<html>
    <head>
        {include file="head.tpl"}
    </head>
	<body>
		{include file="header.tpl"}
        <div id="page-wrap">

            <div class="contenedor-responsive">

            <div id="conttranstotalcos">
                <div id="conttranscos">
                    <div id="btranspresen">
                        <div id="transimatge">
                            <img src="/images/alcalde/palcalde.jpg" class="img-slider-territori"/>
                        </div>
                        <div id="transimatgeText">
                            <h1>
                                {$LABEL_TITOL0}
                                <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                            </h1>
                        </div>
                    </div>
                    <ul id='menu_planA'>
                        {foreach $MENU as $itemMenu}
                            <a href={$itemMenu}>
                                <li id='menu_planA_item'>
                                    {$itemMenu@key}
                                </li>
                            </a>
                        {/foreach}
                    </ul>
                    <div id="tcentre_planA">
                        <h2>{$LABEL_TITOL1}</h2>
                        <p><iframe src="https://calendar.google.com/calendar/embed?showTitle=0&amp;height=600&amp;wkst=2&amp;hl=ca&amp;bgcolor=%23FFFFFF&amp;src=tortosaalcalde%40gmail.com&amp;color=%231B887A&amp;ctz=Europe%2FMadrid" style="border-width:0" width="100%" height="600" frameborder="0" scrolling="no"></iframe></p>

                    </div>
                </div>
            </div>
        </div>
        </div>
		{include file="footer.tpl"}
	</body>
</html>