<html>
    <head>
        {include file="head.tpl"}
    </head>
	<body>
		{include file="header.tpl"}
        <div id="page-wrap">

            <div class="contenedor-responsive">

            <div id="conttranstotalcos">
                <div id="conttranscos">
                    <div id="btranspresen">
                        <div id="transimatge">
                            <img src="/images/alcalde/palcalde.jpg" class="img-slider-territori"/>
                        </div>
                        <div id="transimatgeText">
                            <h1>
                                {$LABEL_TITOL0}
                                <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                            </h1>
                        </div>
                    </div>
                    <ul id='menu_planA'>
                        {foreach $MENU as $itemMenu}
                            <a href={$itemMenu}>
                                <li id='menu_planA_item'>
                                    {$itemMenu@key}
                                </li>
                            </a>
                        {/foreach}
                    </ul>
                    <div id="tcentre_planA">
                        <h2>{$LABEL_TITOL1}</strong></h2>
                        <p>{$LABEL_TC1}</p>
                        <p>{$LABEL_TC2}</p>
                        <p>{$LABEL_TC3}</p>
                        <p>{$LABEL_TC4}</p>
                        <p>{$LABEL_TC5}</p>
                        <p>{$LABEL_TC6}</p>
                        <p>{$LABEL_TC7}</p>
                        <p class="footer-lopd">{$LABEL_TC8}</p>
                        <p class="separator"></p>
                        <h2>{$LABEL_TITOL2}</h2>
                        <p>{$LABEL_TC9}</p>
                        <p>{$LABEL_TC10}</p>
                        <p>{$LABEL_TC11}</p>
                        <p>{$LABEL_TC12}</p>
                        <ul class="subvencions1">
                            <li>
                                <i class="icon-file-pdf"></i>&nbsp;<a href="{$LINK_DEL1}" target="_blank">{$LABEL_TC13}</a>
                            </li>
                            <li>
                                <i class="icon-file-pdf"></i>&nbsp;<a href="{$LINK_DEL2}" target="_blank">{$LABEL_TC14}</a>
                            </li>
                            <li>
                                <i class="icon-file-pdf"></i>&nbsp;<a href="{$LINK_DEL3}" target="_blank">{$LABEL_TC15}</a>
                            </li>
                            <li>
                                <i class="icon-file-pdf"></i>&nbsp;<a href="{$LINK_DEL4}" target="_blank">{$LABEL_TC16}</a>
                            </li>
                            <li>
                                <i class="icon-file-pdf"></i>&nbsp;<a href="{$LINK_DEL5}" target="_blank">{$LABEL_TC17}</a>
                            </li>
                        </ul>
                        <p class="separator"></p>
                        <h2>{$LABEL_TITOL3}</h2>
                        <ul class="subvencions1">
                            <li>
                                <i class="icon-file-pdf"></i>&nbsp;<a href="{$LINK_HIS1}" target="_blank">{$LABEL_TC18}</a>
                            </li>
                            <li>
                                <i class="icon-file-pdf"></i>&nbsp;<a href="{$LINK_HIS2}" target="_blank">{$LABEL_TC19}</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        </div>
		{include file="footer.tpl"}
	</body>
</html>