<!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->
<div id="page-wrap">

   <div class="contenedor-responsive">

      <div id="conttranstotalcos">

         <div id="conttranscos">

            <div id="btranspresen">

               <div id="transimatge">
                  <img src="{$image}" class="img-slider-territori"/>
               </div>

               <div id="transimatgeText">
                  <h1>
                     {$LABEL_TITOL1}
                     <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                  </h1>
               </div>

            </div>

            <ul id='menu_planA'>
               {foreach $MENU as $itemMenu}
               <a href={$itemMenu['link']} target="_{$itemMenu['target']}">
                  <li id='menu_planA_item'>
                     {$itemMenu['name']}
                  </li>
               </a>
               {/foreach}
            </ul>

            <div id="tcentre_planA">
               <h2>{$LABEL02}</h2>

               <div id="taulapubilles">  
                  <div id="filapubilles">
                     <div class="colunapubilla">
                        <a href="http://www.tortosa.cat/webajt/festes/cinta17/programa/prog2017.pdf" target="_blank">
                           <img class="marcprograma" src="/images/festes/cinta17/programa/iprograma.jpg" width="150" height="212">
                        </a>
                        <br><br>
                        <div id="textnegreta">Programa</div>
                        <div id="textnormal"><i class="icon-file-pdf"></i></div>
                        <div id="textnormal">Del 31/8 al 4/9</div>
                     </div>                        
                  </div>
               </div>
      
                <div class="separator"></div> 
                <br>

               <div id="taulapubilles">  
                  <div id="filapubilles">
                     <div class="colprograma">
                        <a href="http://www.tortosa.cat/webajt/festes/cinta17/programa/prog2608.pdf" target="_blank">
                           <img class="marcprograma" src="/images/festes/cinta17/programa/i2608.jpg" width="125" height="177">
                        </a>                           
                        <br><br>
                        <div id="textnormal">Dissabte, 26 d'agost</div>
                        <div id="textnormal"><i class="icon-file-pdf"></i></div>
                     </div>
                     <div class="colprograma"> 
                        <a href="http://www.tortosa.cat/webajt/festes/cinta17/programa/prog3108.pdf" target="_blank">
                           <img class="marcprograma" src="/images/festes/cinta17/programa/i3108.jpg" width="125" height="177">
                        </a>
                        <br><br>
                        <div id="textnormal">Dijous, 31 d'agost</div>
                        <div id="textnormal"><i class="icon-file-pdf"></i></div>
                     </div>
                     <div class="colprograma">
                        <a href="http://www.tortosa.cat/webajt/festes/cinta17/programa/prog0109.pdf" target="_blank">
                           <img class="marcprograma" src="/images/festes/cinta17/programa/i0109.jpg" width="125" height="177">
                        </a>
                        <br><br>
                        <div id="textnormal">Divendres, 1 de setembre</div>
                        <div id="textnormal"><i class="icon-file-pdf"></i></div>
                     </div> 
                     <div class="colprograma">
                        <a href="http://www.tortosa.cat/webajt/festes/cinta17/programa/prog0209.pdf" target="_blank">
                           <img class="marcprograma" src="/images/festes/cinta17/programa/i0209.jpg" width="125" height="177">
                        </a>
                        <br><br>
                        <div id="textnormal">Dissabte, 2 de setembre</div>
                        <div id="textnormal"><i class="icon-file-pdf"></i></div>                       
                     </div>                       
                  </div>

                  <div id="filapubilles">
                     
                     <div class="colprograma"> 
                        <a href="http://www.tortosa.cat/webajt/festes/cinta17/programa/prog0309.pdf" target="_blank">
                           <img class="marcprograma" src="/images/festes/cinta17/programa/i0309.jpg" width="125" height="177">
                        </a>
                        <br><br>
                        <div id="textnormal">Diumenge, 3 de setembre</div>
                        <div id="textnormal">Dia de la Cinta</div> 
                        <div id="textnormal"><i class="icon-file-pdf"></i></div>                       
                     </div>
                     <div class="colprograma">
                        <a href="http://www.tortosa.cat/webajt/festes/cinta17/programa/prog0409.pdf" target="_blank">
                           <img class="marcprograma" src="/images/festes/cinta17/programa/i0409.jpg" width="125" height="177">
                        </a>
                        <br><br>
                        <div id="textnormal">Dilluns, 4 de setembre</div>
                        <div id="textnormal">Diada del Riu</div> 
                        <div id="textnormal"><i class="icon-file-pdf"></i></div>                       
                     </div>      
                     <div class="colprograma">
                        <a href="http://www.tortosa.cat/webajt/festes/cinta17/programa/progAltres.pdf" target="_blank">
                           <img class="marcprograma" src="/images/festes/cinta17/programa/iAltres.jpg" width="125" height="177">
                        </a>
                        <br><br>
                        <div id="textnormal">Altres activitats</div>
                        <div id="textnormal"><i class="icon-file-pdf"></i></div>                        
                     </div>
                     <div class="colprograma"> 
                        <a href="http://www.tortosa.cat/webajt/festes/cinta17/programa/progInforma.pdf" target="_blank">
                           <img class="marcprograma" src="/images/festes/cinta17/programa/iInforma.jpg" width="125" height="177">
                        </a>
                        <br><br>
                        <div id="textnormal">L'Ajuntament informa</div>
                        <div id="textnormal"><i class="icon-file-pdf"></i></div>                        
                     </div>                      
                  </div>

               </div>
            </div>

            <div class="separator"></div>

         </div>
      </div>
   </div>
</div>