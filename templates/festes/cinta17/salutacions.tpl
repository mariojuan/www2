<!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->
<div id="page-wrap">

   <div class="contenedor-responsive">

      <div id="conttranstotalcos">

         <div id="conttranscos">

            <div id="btranspresen">

               <div id="transimatge">
                  <img src="{$image}" class="img-slider-territori"/>
               </div>

               <div id="transimatgeText">
                  <h1>
                     {$LABEL_TITOL1}
                     <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                  </h1>
               </div>

            </div>

            <ul id='menu_planA'>
               {foreach $MENU as $itemMenu}
               <a href={$itemMenu['link']} target="_{$itemMenu['target']}">
                  <li id='menu_planA_item'>
                     {$itemMenu['name']}
                  </li>
               </a>
               {/foreach}
            </ul>

            <div id="tcentre_planA">
               <h2>{$LABEL01}</h2>

               <div id="taulapubilles">  
                  <div id="filapubilles">
                     <div class="colpubilles">
                        <a href="/docs/festes/cinta17/saludapr.pdf" target="_blank">
                              <img class="avatar3" src="/images/festes/cinta17/saludes/pgencat.jpg">
                        </a>                           
                        <br><br>
                        <div id="textnegreta">Carles Puigdemont i Casamajó</div>
                        <div id="textnormal">Molt Honorable President de la Generalitat de Catalunya</div>
                        <div id="textnormal"></div>
                     </div>
                     <div class="colpubilles"> 
                        <a href="/docs/festes/cinta17/saludaal.pdf" target="_blank">
                              <img class="avatar3" src="/images/festes/cinta17/saludes/atortosa.jpg">
                        </a>
                        <br><br>
                        <div id="textnegreta">Ferran Bel i Accensi</div>
                        <div id="textnormal">alcalde de Tortosa</div>
                        <div id="textnormal"></div>
                     </div>
                     <div class="colpubilles"> 
                        <a href="/docs/festes/cinta17/saludapg.pdf" target="_blank">
                              <img class="avatar3" src="/images/festes/cinta17/saludes/prego.jpg">
                        </a>
                        <br><br>
                        <div id="textnegreta">Pere Estupinyà Giné</div>
                        <div id="textnormal">pregoner festes de la Cinta 2017</div>
                        <div id="textnormal"></div>
                     </div>                   
                  </div>

                  <div id="filapubilles">
                     <div class="colpubilles">
                        <a href="/docs/festes/cinta17/saludabi.pdf" target="_blank">
                              <img class="avatar3" src="/images/festes/cinta17/saludes/btortosa.jpg">
                        </a>                           
                        <br><br>
                        <div id="textnegreta">Enrique Benavent Vidal</div>
                        <div id="textnormal">bisbe de Tortosa</div>
                        <div id="textnormal"></div>
                     </div>
                     <div class="colpubilles"> 
                        <a href="/docs/festes/cinta17/saludarg.pdf" target="_blank">
                              <img class="avatar3" src="/images/festes/cinta17/saludes/rfestes.jpg">
                        </a>
                        <br><br>
                        <div id="textnegreta">Domingo Tomàs i Audí</div>
                        <div id="textnormal">regidor de festes de Tortosa</div>
                        <div id="textnormal"></div>
                     </div>
                     <div class="colpubilles"> 
                        <a href="/docs/festes/cinta17/saludama.pdf" target="_blank">
                              <img class="avatar3" src="/images/festes/cinta17/saludes/pmajor.jpg">
                        </a>
                        <br><br>
                        <div id="textnegreta">Francesc Viñes i Albacar</div>
                        <div id="textnormal">primer majordom de la Reial Arxiconfraria Nostra Senyora de la Cinta</div>
                        <div id="textnormal"></div>
                     </div>                   
                  </div>

               </div>
            </div>

            <div class="separator"></div>

         </div>
      </div>
   </div>
</div>