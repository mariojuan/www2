<html>
	<head>
		<title>

		</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        {include file="head.tpl"}
	</head>
	<body>
		{include file="header.tpl"}

		{if $opcio=='S'}
			<!-- SALUTACIONS -->
			{include file="festes/cinta17/salutacions.tpl" image="/images/festes/cinta17/SLIDER.png"}
	    {else if $opcio=='PF'}
			<!-- PROGRAMA FESTES -->
			{include file="festes/cinta17/programa.tpl" image="/images/festes/cinta17/SLIDER.png"}
	    {else if $opcio=='MF'}
			<!-- MUSICA FESTES -->
			{include file="festes/cinta17/cinta17_custom.tpl" image="/images/festes/cinta17/SLIDER.png"}
	    {else if $opcio=='PG'}
			<!-- PUBILLES -->
			{include file="festes/cinta17/pubilles.tpl" image="/images/festes/cinta17/SLIDER.png"}
	    {else if $opcio=='PP'}
			<!-- PUBILLETES -->
			{include file="festes/cinta17/pubilletes.tpl" image="/images/festes/cinta17/SLIDER.png"}
	    {else if $opcio=='C'} 
			<!-- CORREFOC -->
			{include file="festes/cinta17/cinta17_custom.tpl" image="/images/festes/cinta17/SLIDER.png"}
	    {else if $opcio=='TG'}
			<!-- TROBADA GEGANTS -->
			{include file="festes/cinta17/cinta17_custom.tpl" image="/images/festes/cinta17/SLIDER.png"}
	    {else if $opcio=='JC'}
			<!-- Jornada castellera -->
			{include file="festes/cinta17/cinta17_custom.tpl" image="/images/festes/cinta17/SLIDER.png"}
	    {else if $opcio=='UCU'}
			<!-- UN COP D'ULL -->
			{include file="festes/cinta17/cinta17_custom.tpl" image="/images/festes/cinta17/SLIDER.png"}		
	    {else}
			{include file="festes/cinta17/cinta17_custom.tpl" image="/images/festes/cinta17/SLIDER.png"}		
	    {/if}

		{include file="footer.tpl"}
	</body>
</html>