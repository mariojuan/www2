<!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->
<div id="page-wrap">

   <div class="contenedor-responsive">

      <div id="conttranstotalcos">

         <div id="conttranscos">

            <div id="btranspresen">

               <div id="transimatge">
                  <img src="{$image}" class="img-slider-territori"/>
               </div>

               <div id="transimatgeText">
                  <h1>
                     {$LABEL_TITOL1}
                     <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                  </h1>
               </div>

            </div>

            <ul id='menu_planA'>
               {foreach $MENU as $itemMenu}
               <a href={$itemMenu['link']} target="_{$itemMenu['target']}">
                  <li id='menu_planA_item'>
                     {$itemMenu['name']}
                  </li>
               </a>
               {/foreach}
            </ul>

            <div id="tcentre_planA">
               <!--<h2 style="text-align: center">{$LABEL00}</h2>-->

               {if $opcio=='TG'}  
                  <h2>{$LABEL07}</h2>             
                  <p style="text-align: center"><img src="/images/festes/cinta17/60e-aniversari-rufo-i-rubi.jpg" class="img-responsive"></p>
               {else if $opcio=='C'}
                  <h2>{$LABEL06}</h2>
                  <p style="text-align: center"><img src="/images/festes/cinta17/XXXIVEbrecorrefoc.png" class="img-responsive"></p>                  
               {else if $opcio=='JC'}
                  <h2>{$LABEL09}</h2>
                  <p style="text-align: center"><img src="/images/festes/cinta17/Castellera2017.jpg" class="img-responsive"></p>
               {else if $opcio=='MF'}
                  <h2>{$LABEL03}</h2>
                  <p style="text-align: center">
                     <a href="/images/festes/cinta17/La-musica-de-festes.jpg" target="_blank">
                     <img src="/images/festes/cinta17/La-musica-de-festes.jpg" class="img-responsive"><br>
                     </a>
                  </p>
               {else}
                  <p style="text-align: center">
                     <a href="https://itunes.apple.com/us/app/cinta-2016/id1145496074?l=es&ls=1&mt=8" target="_blank">
                        <img src="/images/festes/cinta17/ico-appstore.png">
                     </a>
                     <a href="https://play.google.com/store/apps/details?id=com.globalsprojects.festesdelacinta2016" target="_blank">
                        <img src="/images/festes/cinta17/ico-androidstore.png">
                     </a>
                  </p>
                  <p style="text-align: center"><img src="/images/festes/cinta17/cartell.jpg" class="img-responsive"></p>
               {/if}

            </div>

            <div class="separator"></div>

         </div>
      </div>
   </div>
</div>