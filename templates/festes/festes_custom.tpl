<!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->
<div id="page-wrap">

   <div class="contenedor-responsive">

      <div id="conttranstotalcos">

         <div id="conttranscos">

            <div id="btranspresen">

               <div id="transimatge">
                  <img src="{$image}" class="img-slider-territori"/>
               </div>

               <div id="transimatgeText">
                  <h1>
                     {$LABEL_TITOL1}
                     <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                  </h1>
               </div>

            </div>

            <ul id='menu_planA'>
               {foreach $MENU as $itemMenu}
               <a href={$itemMenu['link']} target="_{$itemMenu['target']}">
                  <li id='menu_planA_item'>
                     {$itemMenu['name']}
                  </li>
               </a>
               {/foreach}
            </ul>

            <div id="tcentre_planA">
               {if $type==1}
               <h2>{$LABEL_TC0}</h2>
               <p>{$LABEL_TC1}</p>
               <p>{$LABEL_TC2}</p>
               <p>{$LABEL_TC3}</p>
               <p>{$LABEL_TC4}</p>
               {elseif $type==6}
               <h2>{$LABEL_TC00}</h2>
               <p>{$LABEL_TC6}</p>
               <p><strong>{$LABEL_TC7}</strong>{$LABEL_TC7b}</p>
               <p><strong>{$LABEL_TC8}</strong>{$LABEL_TC8b}</p>
               <p><strong>{$LABEL_TC9}</strong>{$LABEL_TC9b}</p>
               <p><strong>{$LABEL_TC10}</strong> <a href="mailto:{$LABEL_TC11}">{$LABEL_TC11}</a></p>
               {elseif $type==7}
               <h2>{$LABEL_TC000}</h2>
               <p>{$LABEL_TC12b}</p>
               <ul class="subvencions1">
                  <li><i class="icon-sun"></i>&nbsp;{$LABEL_TC13}</li>
                  <li><i class="icon-sun"></i>&nbsp;{$LABEL_TC14}</li>
                  <li><i class="icon-sun"></i>&nbsp;{$LABEL_TC15}</li>
                  <li><i class="icon-sun"></i>&nbsp;{$LABEL_TC16}</li>
                  <li><i class="icon-sun"></i>&nbsp;{$LABEL_TC17}</li>
               </ul>
               {elseif $type==8}
               <h2>{$LABEL_TC0000}</h2>
               <p>{$LABEL_TC12a}</p>
               <ul class="subvencions1">
                  <li><i class="icon-sun"></i>&nbsp;<a href="http://www.bitem.altanet.org/municipi/festesmajors/index.php" target="_blank"><strong>{$LABEL_TC18}</strong></a>:&nbsp;{$LABEL_TC19}</li>
                  <li><i class="icon-sun"></i>&nbsp;<a href="http://www.campredo.cat/ca/area/festes" target="_blank"><strong>{$LABEL_TC20}</strong></a>:&nbsp;{$LABEL_TC21}</li>
                  <li><i class="icon-sun"></i>&nbsp;<strong>{$LABEL_TC22}</strong>:&nbsp;{$LABEL_TC23}</li>
                  <li><i class="icon-sun"></i>&nbsp;<a href="http://www.emdjesus.cat/index.php?id=2" target="_blank"><strong>{$LABEL_TC24}</strong></a>:&nbsp;{$LABEL_TC25}</li>
                  <li><i class="icon-sun"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/pobles/vinallop/festes.asp" target="_blank"><strong>{$LABEL_TC26}</strong></a>:&nbsp;{$LABEL_TC27}</li>
               </ul>
               {elseif $type==4}
               <h2>{$LABEL_TC0}</h2>
               <p>{$LABEL_TC15}</p>
               <p>{$LABEL_TC16}</p>
               <p>{$LABEL_TC17}</p>
               <p>{$LABEL_TC18}</p>
               {elseif $type==2}
               <h2 style="text-align: center">{$LABEL_TC14}</h2>
               <p style="text-align: center"><img src="/images/laciutat/ifestes.jpg" class="img-responsive"></p>
               {elseif $type==3}
                  <h2>{$LABEL_TC9}</h2>
                  <p>{$LABEL_TC1} <a href="mailto:festes@tortosa.cat">festes@tortosa.cat</a> {$LABEL_TC2} <strong>{$LABEL_TC3}</strong></p>
                  <p><strong>{$LABEL_TC4}</strong>: {$LABEL_TC6}</p>
                  <p><strong>{$LABEL_TC5}</strong>: {$LABEL_TC7}</p>
                  <div class="separator"></div>
                  <p><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/festes/Fitxapubilla.pdf">{$LABEL_TC8} {$LABEL_TC4}</a></p>
                  <p><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/festes/Fitxapubilleta.pdf">{$LABEL_TC8} {$LABEL_TC5}</a></p>
               {elseif $type==5}
               <h2>{$LABEL_TC10}</h2>
               <p>{$LABEL_TC11} <a href="mailto:festes@tortosa.cat">festes@tortosa.cat</a> {$LABEL_TC2} <strong>{$LABEL_TC3}</strong></p>
               <p><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/festes/FormulariOfrena.pdf">{$LABEL_TC8} {$LABEL_TC12}</a></p>
               <p><i class="icon-file-pdf"></i>&nbsp;<a href="http://www.tortosa.cat/webajt/festes/FormulariActivitats.pdf">{$LABEL_TC8} {$LABEL_TC13}</a></p>
               {elseif $type==9}
                  <h2>{$LABEL_TC00}</h2>
                  {foreach $PORTADES as $itemPortada}
                     <a href={$itemPortada['link']} target="_{$itemPortada['target']}" style="text-decoration:none">
                        <div class="list_portades_festes">
                           <div class="portades_item_img">
                              <img src="/images/festes/portades/cinta{$itemPortada['name']}.jpg" width="93" height="135">
                           </div>
                           <div class="portades_title_item">{$LABEL_TC19}{$itemPortada['name']}</div>
                           <div class="portades_subtitle_item">Tortosa,{$itemPortada['subname']}</div>
                        </div>
                     </a>
                  {/foreach}
               {elseif $type==10}
                  <h2>{$LABEL_TC20}</h2>
                  <iframe width="560" height="315" src="https://www.youtube.com/embed/tzn5W5AnB8w" frameborder="0" allowfullscreen></iframe>               
               {elseif $type==11}
                  <h2>{$LABEL_TC21}</h2>
                  <br>
                  <br>
                  <center>
                     <video width="300"  height="30" controls="" autoplay="" name="media"><source src="/festes/pdt.mp3" type="audio/mpeg"></video>                  
                  </center>
               {/if}
            </div>

            <div class="separator"></div>

         </div>
      </div>
   </div>
</div>