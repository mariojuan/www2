<!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->

<div id="page-wrap">

<div class="contenedor-responsive">

    <div id="conttranstotalcos">

        <div id="conttranscos">

            <div id="btranspresen">

                <div id="transimatge">
                    <img src="{$image}" class="img-slider-territori"/>
                </div>

                <div id="transimatgeText">
                    <h1>
                    {$LABEL_TITOL1}
                    <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                    </h1>
                </div>

            </div>

            <div class="subtitolinfeco" id="subtitolinfeco2">
            {$LABEL_TITOL1b}
            </div>

            <div class="marcback">
                    <a href="index.php">
                    <i class="icon-angle-double-up"></i>
                    </a>
            </div>

            <ul id='menu_planA'>
            {foreach $MENU as $itemMenu}
                <a href={$itemMenu['link']} target="_{$itemMenu['target']}">
                <li id='menu_planA_item'>
                    {$itemMenu['name']}
                </li>
                </a>
            {/foreach}
            </ul>

            <div id="tcentre_planA">
                {if $item==1}
                    <h2>{$LABEL_TC0}</h2>
                    <div class="marcterme">
                    <img src="/images/festes/elements.jpg" class="img-responsive">
                    </div>
                {elseif $item==2}
                    <h2>{$LABEL_TC0}</h2>
                    <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong>{$LABEL_TC1}</strong></p>
                    <p>{$LABEL_TC2}</p>
                    <p>{$LABEL_TC3}</p>
                    <div class="marcterme" style="left:200px;">
                    <img src="/images/festes/cuca1.jpg">
                    </div>
                    <div class="separator"></div>
                    <p>{$LABEL_TC4}</p>
                    <div class="separator"></div>
                    <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong>{$LABEL_TC5}</strong></p>
                    <p>{$LABEL_TC6}</p>
                    <p>{$LABEL_TC7}</p>
                    <div class="marcterme" style="left:200px;">
                    <img src="/images/festes/cuca2.jpg">
                    </div>
                    <div class="separator"></div>
                    <p>{$LABEL_TC8}</p>
                    <p>{$LABEL_TC9}</p>
                    <div class="separator"></div>
                    <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong>{$LABEL_TC10}</strong></p>
                    <p>{$LABEL_TC11}</p>
                    <p>{$LABEL_TC12}</p>
                    <p align="center"><i>{$LABEL_TC13}<br>
                    {$LABEL_TC14}<br>
                    {$LABEL_TC15}<br>
                    {$LABEL_TC16}</i></p>
                    <div class="marcterme rectificacio-marcTerme-img-ssanta2017" style="left:60px;">
                    <img src="/images/festes/cuca3.jpg" class="img-responsive">
                    </div>
                    <div class="separator"></div>
                    <p>{$LABEL_TC17}</p>
                    <p>{$LABEL_TC18}</p>
                    <p>{$LABEL_TC19}</p>
                    <p>{$LABEL_TC20}</p>
                    <p>{$LABEL_TC21}</p>
                    <p align="center"><i>{$LABEL_TC22}<br>
                    {$LABEL_TC23}<br>
                    {$LABEL_TC24}<br>
                    {$LABEL_TC25}</i></p>
                    <p>{$LABEL_TC26}</p>
                    <p>{$LABEL_TC27}</p>
                    <p>{$LABEL_TC28}</p>
                    <p>{$LABEL_TC29}</p>
                    <p>{$LABEL_TC30}</p>
                    <p>{$LABEL_TC31}</p>
                {elseif $item==3}
                    <h2>{$LABEL_TC0}</h2>
                    <div class="separator"></div>
                    <div class="marcterme rectificacio-marcTerme-img-ssanta2017" style="left:10px;">
                    <img src="/images/festes/gegants.jpg" class="img-responsive">
                    </div>
                    <div class="separator"></div>
                    <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong>{$LABEL_TC1}</strong></p>
                    <p><strong>{$LABEL_TC2}</strong></p>
                    <p>{$LABEL_TC3}</p>
                    <p>{$LABEL_TC4}</p>
                    <p>{$LABEL_TC5}</p>
                    <p>{$LABEL_TC6}</p>
                    <p><strong>{$LABEL_TC7}</strong></p>
                    <p>{$LABEL_TC8}</p>
                    <p>{$LABEL_TC9}</p>
                    <p>{$LABEL_TC10}</p>
                    <p>{$LABEL_TC11}</p>
                    <p>{$LABEL_TC12}</p>
                    <p>{$LABEL_TC13}</p>
                    <p>{$LABEL_TC14}</p>
                    <div class="separator"></div>
                    <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong>{$LABEL_TC15}</strong></p>
                    <p><strong>{$LABEL_TC16}</strong></p>
                    <p>{$LABEL_TC17}</p>
                    <p>{$LABEL_TC18}</p>
                    <p>{$LABEL_TC19}</p>
                    <p>{$LABEL_TC20}</p>
                    <p>{$LABEL_TC21}</p>
                    <p><strong>{$LABEL_TC7}</strong></p>
                    <p>{$LABEL_TC22}</p>
                    <p>{$LABEL_TC23}</p>
                    <p>{$LABEL_TC24}</p>
                    <p>{$LABEL_TC25}</p>
                    <p>{$LABEL_TC26}</p>
                    <div class="separator"></div>
                    <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong>{$LABEL_TC27}</strong></p>
                    <p><strong>{$LABEL_TC16}</strong></p>
                    <p>{$LABEL_TC28}</p>
                    <p>{$LABEL_TC29}</p>
                    <p>{$LABEL_TC30}</p>
                    <p>{$LABEL_TC31}</p>
                    <p>{$LABEL_TC32}</p>
                    <p><strong>{$LABEL_TC7}</strong></p>
                    <p>{$LABEL_TC33}</p>
                    <p>{$LABEL_TC34}</p>
                    <div class="separator"></div>
                    <p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong>{$LABEL_TC35}</strong></p>
                    <p><strong>{$LABEL_TC2}</strong></p>
                    <p>{$LABEL_TC36}</p>
                    <p>{$LABEL_TC37}</p>
                    <p>{$LABEL_TC38}</p>
                    <p>{$LABEL_TC39}</p>
                    <p>{$LABEL_TC40}</p>
                    <p>{$LABEL_TC41}</p>
                    <p>{$LABEL_TC42}</p>
                    <p><strong>{$LABEL_TC7}</strong></p>
                    <p>{$LABEL_TC43}</p>
                    <p>{$LABEL_TC44}</p>
                    <p>{$LABEL_TC45}</p>
                {elseif $item==4}
                    <h2>{$LABEL_TC0}</h2>
                    <p>{$LABEL_TC1}</p>
                    <p>{$LABEL_TC2}</p>
                    <div class="separator"></div>
                    <div class="marcterme rectificacio-marcTerme-img-ssanta2017" style="left:140px;">
                    <img src="/images/festes/aguila1.jpg" class="img-responsive">
                    </div>
                    <div class="separator"></div>
                    <p>{$LABEL_TC3}</p>
                    <p>{$LABEL_TC4}</p>
                    <p>{$LABEL_TC5}</p>
                    <div class="separator"></div>
                    <div class="marcterme rectificacio-marcTerme-img-ssanta2017" style="left:160px;">
                    <img src="/images/festes/aguila2.jpg" class="img-responsive">
                    </div>
                    <div class="separator"></div>
                    <p>{$LABEL_TC6}</p>
                    <p>{$LABEL_TC7}</p>
                    <p>{$LABEL_TC8}</p>
                    <p>{$LABEL_TC9}</p>
                    <p>{$LABEL_TC10}</p>
                    <p>{$LABEL_TC11}</p>
                    <p>{$LABEL_TC12}</p>
                    <div class="separator"></div>
                    <div class="marcterme rectificacio-marcTerme-img-ssanta2017" style="left:150px;">
                    <img src="/images/festes/aguila3.jpg" class="img-responsive">
                    </div>
                    <div class="separator"></div>
                    <p>{$LABEL_TC13}</p>
                    <p>{$LABEL_TC14}</p>
                {/if}
            </div>

        </div>
    </div>
</div>
</div>


