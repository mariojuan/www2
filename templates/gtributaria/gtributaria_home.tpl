<!-- PLANTILLA DE MENÚ A L'ESQUERRA I TEXT AL CENTRE (AMB FOTO/IMATGE O SENSE) -->

<div id="page-wrap">

<div class="contenedor-responsive">

<div id="conttranstotalcos">
	<div id="conttranscos">
		<div id="btranspresen">
			<div id="transimatge">			
			<img src="{$image}" class="img-slider-territori"/>			
			</div>
			<div id="transimatgeText">
				<h1>
				{$LABEL_TITOL1}
				<i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
				</h1>
			</div>
		</div>

		<ul id='menu_planA'>
		{foreach $MENU as $itemMenu}
                <a href={$itemMenu['link']} target="_{$itemMenu['target']}">
                <li id='menu_planA_item'>
                    {$itemMenu['name']}
                </li>
                </a>
        {/foreach}
		</ul>

		<div id="tcentre_planA">
			<font size=4 color=#666666>{$LABEL_TITOL0}</font>
			{if $item==1}
				<p>{$LABEL_TC1}</p>
			{else if $item==2}
				<!--<p style="text-align:center"><img src="/images/gtributaria/logopic.jpg"></p>-->
				<p>{$LABEL_PARAG1}</p>
				<ul>
				<li>{$LABEL_PARAG2}</li>
				<li>{$LABEL_PARAG3}</li>
				<li>{$LABEL_PARAG4}</li>
				</ul>
				<p style="text-align:center">{$LABEL_PARAG5}</p>
				<p style="text-align:center">{$LABEL_PARAG6}</p>
				<p>{$LABEL_PARAG7}<a href="http://www.tortosa.cat/webajt/cadastre/autoritzacio.pdf" target="_blank">{$LABEL_PARAG8}</a></p>
				<p>{$LABEL_PARAG9}</p>
				<p></p>
				<p style="text-align:center"><a href="http://www.sedecatastro.gob.es/" target="_blank"><img src="/images/gtributaria/ipic.jpg"></a></p>
				<p style="text-align:center">{$LABEL_PARAG10}</p>
			{else if $item==3}
				<p>{$LABEL_PARAG1}977 585 820</p>
				<p>{$LABEL_PARAG2}977 585 852</p>
				<p>{$LABEL_PARAG3}<a href="mailto:gin.tortosa@tortosa.cat" target"_blank">gin.tortosa@tortosa.cat</a></p>
				<p>{$LABEL_PARAG4}<a href="http://www2.tortosa.cat/gtributaria/index.php">www2.tortosa.cat/gtributaria</a></p>
				<p>&nbsp;</p>
				<p style="border-bottom: 1px solid #333333"><i class="icon-check"></i><strong>{$LABEL_PARAG5}</strong></p>
				<p>{$LABEL_PARAG6}</p>
				<p>{$LABEL_PARAG7}{$LABEL_PARAG8}</p>
			{else if $item==4}
				<ul style="line-height: 24px">
					<li>{$LABEL_PARAG1}</li>
					<li>{$LABEL_PARAG2}</li>
					<li>{$LABEL_PARAG3}</li>
					<li>{$LABEL_PARAG4}</li>
				</ul>
			{else if $item==5}
				<ul class="subvencions1">
					<li><i class="icon-file-pdf"></i>&nbsp;<a href="timpostos.pdf" target="_blank">{$LABEL_PARAG1}</a></li>
					<li><i class="icon-file-pdf"></i>&nbsp;<a href="ttaxes.pdf" target="_blank">{$LABEL_PARAG2}</a></li>
					<li><i class="icon-file-pdf"></i>&nbsp;<a href="tppublics.pdf" target="_blank">{$LABEL_PARAG3}</a></li>
					<li><i class="icon-file-pdf"></i>&nbsp;<a href="ttarifes.pdf" target="_blank">{$LABEL_PARAG4}</a></li>
				</ul>
			{/if}
		</div>

  	</div>
</div>
</div>
</div>


