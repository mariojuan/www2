<html>
	<head>

        {include file="head.tpl"}

	</head>
	<body>
		{include file="header.tpl"}

		<div id="page-wrap">

            <div class="contenedor-responsive">

    <div id="conttranstotalcos">
        <div id="conttranscos">
            <div id="btranspresen">
                <div id="transimatge">
                    <img src="{$image}" class="img-slider-territori"/>
                </div>
                <div id="transimatgeText">
                    <h1>
                        {$LABEL_TITOL1}
                        <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                    </h1>
                </div>
            </div>

            <!-- Menu -->
                {include file="ple/menu.tpl"}
            <!--// Menu -->

            <div id="tcentre_planA">

                <font size="4" color="#666666">{$LABEL_TC0}</font>
                <h3>{$LABEL_TC3}</h3>

                <!-- Taula ELECTES -->

                <h3>{$LABEL_TC6}</h3>
                <table class="tableYves">
                  <tr>
                    <thead>
                    <th>{$LABEL_NAME}</th>
                    <th>{$LABEL_POSITION}</th>
                    <th>{$LABEL_FILE}</th>
                    <th>{$LABEL_MODFILE}</th>
                    </thead>
                    </tr>
                    {foreach $ELECTES as $item}
                    <tr>
                        <td>{$item['name']}</td>
                        <td>{$item['position']}</td>
                        <td><a href="{$item['file']}" target="{$item['target']}"><i class="icon-file-pdf"></i><a></td>

                        <td>
                        {if $item['modfile'] != ''}
                            <a href="{$item['modfile']}" target="{$item['target']}"><i class="icon-file-pdf"></i><a>
                        {/if}
                        </td>
                    </tr>
                    {/foreach}
                </table>

                <!-- //Taula ELECTES -->

                <!-- Taula PUBLICS EVENTUALS -->

                <h3>{$LABEL_PUBLICSEVENTUALS}</h3>
                <table class="tableYves">
                  <tr>
                    <thead>
                    <th>{$LABEL_NAME}</th>
                    <th>{$LABEL_POSITION}</th>
                    <th>{$LABEL_FILE}</th>
                    <th>{$LABEL_MODFILE}</th>
                    </thead>
                    </tr>
                    {foreach $PUBLICSEVENTUALS as $item}
                    <tr>
                        <td>{$item['name']}</td>
                        <td>{$item['position']}</td>
                        <td><a href="{$item['file']}" target="{$item['target']}"><i class="icon-file-pdf"></i><a></td>

                        <td>
                        {if $item['modfile'] != ''}
                            <a href="{$item['modfile']}" target="{$item['target']}"><i class="icon-file-pdf"></i><a>
                        {/if}
                        </td>
                    </tr>
                    {/foreach}
                </table>

                <!-- //Taula PUBLICS EVENTUALS -->

                <!-- Taula Gerents de les Societats Municipals -->

                <h3>{$LABEL_G_SOCIET_MUNICIPALS}</h3>
                <table class="tableYves">
                  <tr>
                    <thead>
                    <th>{$LABEL_NAME}</th>
                    <th>{$LABEL_POSITION}</th>
                    <th>{$LABEL_FILE}</th>
                    <th>{$LABEL_MODFILE}</th>
                    </thead>
                    </tr>
                    {foreach $G_SOCIET_MUNICIPALS as $item}
                    <tr>
                        <td>{$item['name']}</td>
                        <td>{$item['position']}</td>
                        <td><a href="{$item['file']}" target="{$item['target']}"><i class="icon-file-pdf"></i><a></td>

                        <td>
                        {if $item['modfile'] != ''}
                            <a href="{$item['modfile']}" target="{$item['target']}"><i class="icon-file-pdf"></i><a>
                        {/if}
                        </td>
                    </tr>
                    {/foreach}
                </table>

                <!--// Taula Gerents de les Societats Municipals -->

                <!-- Taula Hist càrrecs electes -->

                <h3>{$LABEL_HIST_CARRECS_ELECTES}</h3>
                <table class="tableYves">
                  <tr>
                    <thead>
                    <th>{$LABEL_NAME}</th>
                    <th>{$LABEL_POSITION}</th>
                    <th>{$LABEL_FILE}</th>
                    <th>{$LABEL_MODFILE}</th>
                    </thead>
                    </tr>
                    {foreach $HIST_CARRECS_ELECTES as $item}
                    <tr>
                        <td>{$item['name']}</td>
                        <td>{$item['position']}</td>
                        <td><a href="{$item['file']}" target="{$item['target']}"><i class="icon-file-pdf"></i><a></td>

                        <td>
                        {if $item['modfile'] != ''}
                            <a href="{$item['modfile']}" target="{$item['target']}"><i class="icon-file-pdf"></i><a>
                        {/if}
                        </td>
                    </tr>
                    {/foreach}
                </table>

                <!--// Taula Gerents de les Societats Municipals -->

                <!-- Taula Hist càrrecs eventuals -->

                <h3>{$LABEL_HIST_CARRECS_EVENTUALS}</h3>
                <table class="tableYves">
                  <tr>
                    <thead>
                    <th>{$LABEL_NAME}</th>
                    <th>{$LABEL_POSITION}</th>
                    <th>{$LABEL_FILE}</th>
                    <th>{$LABEL_MODFILE}</th>
                    </thead>
                    </tr>
                    {foreach $HIST_CARRECS_EVENTUALS as $item}
                    <tr>
                        <td>{$item['name']}</td>
                        <td>{$item['position']}</td>
                        <td><a href="{$item['file']}" target="{$item['target']}"><i class="icon-file-pdf"></i><a></td>

                        <td>
                        {if $item['modfile'] != ''}
                            <a href="{$item['modfile']}" target="{$item['target']}"><i class="icon-file-pdf"></i><a>
                        {/if}
                        </td>
                    </tr>
                    {/foreach}
                </table>

                <!--// Taula Hist càrrecs eventuals -->

            </div>
            <div class="separator"></div>
        </div>
    </div>
    </div>
</div>

		{include file="footer.tpl"}
	</body>
</html>