<html>
<head>

    {include file="head.tpl"}

</head>
<body>

  {include file="header.tpl"}

  <div id="page-wrap">
  <div class="contenedor-responsive">
    <div id="conttranstotalcos">
        <div id="conttranscos">
            <div id="btranspresen">
                <div id="transimatge">
                    <img src="{$image}" class="img-slider-territori"/>
                </div>
                <div id="transimatgeText">
                    <h1>
                        {$LABEL_TITOL1}
                        <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                    </h1>
                </div>
            </div>
            
            <!-- Menu -->
                {include file="ple/menu.tpl"}
            <!--// Menu -->

            <div id="tcentre_planA">

                <!-- Taula compos -->

                <font size="4" color="#666666">{$LABEL_TITOLSECCIO}</font>

                <p>{$LABEL_TC0}</p>
                <p>{$LABEL_TC1}</p>

                <table class="tableYves">
                  <tr>
                    <thead>
                    <th>{$LABEL_NOM}</th>
                    <th>{$LABEL_GRUP}</th>
                    <th>{$LABEL_FITXA}</th>
                    </thead>
                    </tr>
                    {foreach $PERSONAL as $item}
                    <tr>
                        <td>{$item['name']}</td>
                        <td>{$item['group']}</td>
                        <td><a href="{$item['link']}" target="{$item['target']}"><i class="{$item['icon']}"></i><a></td>
                    </tr>
                    {/foreach}
                </table>

                <!-- //Taula compos -->

            </div>
            <div class="separator"></div>
        </div>
        </div>
    </div>
</div>
{include file="footer.tpl"}
</body>
</html>