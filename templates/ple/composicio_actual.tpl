<html>
<head>

    {include file="head.tpl"}

</head>
<body>

  {include file="header.tpl"}

  <div id="page-wrap">

  <div class="contenedor-responsive">

    <div id="conttranstotalcos">
        <div id="conttranscos">
            <div id="btranspresen">
                <div id="transimatge">
                    <img src="{$image}" class="img-slider-territori" />
                </div>
                <div id="transimatgeText">
                    <h1>
                        {$LABEL_TITOL1}
                        
                    </h1>
                </div>
            </div>
            
            <!-- Menu -->
                <!-- {include file="ple/menu.tpl"} -->
            	<!--// Menu -->

            <div id="tcentre_municipi">

                <font size="4" color="#666666">{$LABEL_TITOLSECCIO}</font>
                <p><a href="{$LINK_PLE}" target="_self">{$LABEL_LINK_PLE}</a>{$LABEL_TC0}</p>
                <p>{$LABEL_TC1}</p>
                <p>{$LABEL_TC2}</p>
                <p>{$LABEL_TC3}<a href="{$LINK_REGISTRE_BENS}" target="_self">{$LABEL_LINK_REGISTRE_BENS}</a></p>
                <p>{$LABEL_TC4}<a href="{$LINK_RETRIBUCIONS_CARRECS_ELECTES}" target="_self">{$LABEL_LINK_RETRIBUCIONS_CARRECS_ELECTES}</a></p>
                <p>{$LABEL_TC5}</p>

                <div class="separator"></div>

                <!-- Taula càrrecs -->

                <table >
                <thead>
                  <tr>
                    <th>{$LABEL_FOTO}</th>
                    <th>{$LABEL_NOM}</th>
                    <th>{$LABEL_CARREC}</th>
                    <th>{$LABEL_EMAIL}</th>
                    <th>{$LABEL_GRUP}</td>
                    </tr>
                    </thead>
                    {foreach $CARRECS as $item}
                    <tr>
                        <td><a href="{$item['link']}" target="{$item['target']}"><img src="{$item['pic']}" class="avatar"/></a></td>
                        <td class="colMesAmpla"><a href="{$item['link']}" target="{$item['target']}">{$item['name']}</a></td>
                        <td>{$item['occupation']}</td>
                        <td><a href="mailto:{$item['email']}">@</a></td>
                        <td><a href="{$item['linkGrup']}" target="{$item['target']}">{$item['group']}</a></td>
                    </tr>
                    {/foreach}
                </table>

                <!-- //Taula càrrecs-->

                <!-- Taula compos -->

                <div class="separator"></div>

                <h3>{$LABEL_TC6}</h3>
                <table>
                <thead>
                  <tr>
                    <th>{$LABEL_DATA}</th>
                    <th>{$LABEL_NOM}</th>
                    <th>{$LABEL_GRUP}</th>
                    <th>{$LABEL_CANVI}</th>
                    </tr>
                    </thead>
                    {foreach $CANVIS_COMPOS as $item}
                    <tr>
                        <td>{$item['date']}</td>
                        <td><a href="{$item['link']}" target="{$item['target']}">{$item['name']}</a></td>
                        <td><a href="{$item['linkGrup']}" target="{$item['target']}">{$item['group']}</a></td>
                        <td>{$item['change']}</td>
                    </tr>
                    {/foreach}
                </table>

                <!-- //Taula compos -->

                <!-- Taula cartipàs -->

                <div class="separator"></div>
                <div class="separator"></div>

                <h3>{$LABEL_TC7}</h3>
                <table>
                <thead>
                  <tr>
                    <th>{$LABEL_DATA}</th>
                    <th>{$LABEL_ACORD}</th>
                    <th>{$LABEL_DESC}</th>
                    </tr>
                    </thead>
                    {foreach $CANVIS_CARTIP as $item}
                    <tr>
                        <td>{$item['date']}</td>
                        <td><a href="{$item['link']}" target="{$item['target']}">{$item['agreement']}</a></td>
                        <td>{$item['desc']}</td>
                    </tr>
                    {/foreach}
                </table>

                <!-- //Taula cartipàs -->

            </div>
            <div class="separator"></div>
        </div>
    </div>
    </div>
</div>
{include file="footer.tpl"}
</body>
</html>