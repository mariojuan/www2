<html>
   <head>
      {include file="head.tpl"}
   </head>
   <body>
      {include file="header.tpl"}
      <div id="page-wrap">

         <div class="contenedor-responsive">

            <div id="conttranstotalcos">

               <div id="conttranscos">

                  <div id="btranspresen">

                     <div id="transimatge">
                        <img src="{$image}" class="img-slider-territori"/>
                     </div>

                     <div id="transimatgeText">
                        <h1>
                           {$LABEL_TITOL1}
                           <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                        </h1>
                     </div>

                  </div>

                  <!-- Menu -->
                  {include file="ple/menu.tpl"}
                  <!--// Menu -->
                  <div id="tcentre_planA">
                     <font size="4" color="#666666">{$LABEL_TC0}</font>
                     <p>{$LABEL_TC1}</p>
                     <p>{$LABEL_TC2}</p>
                     <p><i><a href="http://www.tortosa.cat/webajt/ajunta/om/retribucions/AcordPle.pdf" target="_blank">{$LABEL_TC3}</a></i></p>
                     <h3>{$LABEL_TC4}</h3>
                     <p>
                        {foreach $EXERCICIS as $item name=identificador}
                        <i class="icon-file-pdf"></i> <a href="{$item['link']}" target="{$item['target']}">{$item['year']}</a>
                        {if not $smarty.foreach.identificador.last} / {/if} 
                        {/foreach}
                     </p>
                  </div>

                  <div class="separator"></div>
                  
               </div>
            </div>
         </div>
      </div>
      {include file="footer.tpl"}
   </body>
</html>