<html>
   <head>
      {include file="head.tpl"}
   </head>
   <body>
      {include file="header.tpl"}
      <div id="page-wrap">

         <div class="contenedor-responsive">

            <div id="conttranstotalcos">

               <div id="conttranscos">

                  <div id="btranspresen">

                     <div id="transimatge">
                        <img src="{$image}" class="img-slider-territori"/>
                     </div>

                     <div id="transimatgeText">
                        <h1>
                           {$LABEL_TITOL1}
                           <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                        </h1>
                     </div>

                  </div>

                  <!-- Menu -->
                  {include file="ple/menu.tpl"}
                  <!--// Menu -->
                  <div id="tcentre_planA">
                     <!-- Competències-->
                     <font size="4" color="#666666">{$LABEL_TC0}</font>
                     <h3>{$LABEL_TC01}</h3>
                     <p>{$LABEL_TC1}</p>
                     <p>{$LABEL_TC2}</p>
                     <p>{$LABEL_TC21}</p>
                     <p>{$LABEL_TC3}</p>
                     <p>{$LABEL_TC4}</p>
                     <p>{$LABEL_TC5}</p>
                     <!-- Delegacions-->
                     <h3>{$LABEL_TC6}</h3>
                     <p>{$LABEL_TC7}</p>
                     <p><i class="icon-file-pdf"></i><a href="http://www.tortosa.cat/webajt/ajunta/om/2015/APleDelegacioJGLn.pdf" target="_blank"> {$LABEL_TC8}</a></p>
                     <p class="normativa"><strong>{$LABEL_TC9}</strong> {$LABEL_TC10}</p>
                  </div>
                  
                  <div class="separator"></div>
               </div>
            </div>
         </div>
      </div>
      {include file="footer.tpl"}
   </body>
</html>