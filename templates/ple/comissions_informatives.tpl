<html>
    <head>

        {include file="head.tpl"}

    </head>
    <body>

        {include file="header.tpl"}

        <div id="page-wrap">
        <div class="contenedor-responsive">
            <div id="conttranstotalcos">
                <div id="conttranscos">
                    <div id="btranspresen">
                        <div id="transimatge">
                            <img src="{$image}" class="img-slider-territori"/>
                        </div>
                        <div id="transimatgeText">
                            <h1>
                                {$LABEL_TITOL1}
                                <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                            </h1>
                        </div>
                    </div>

                    <!-- Menu -->
                    {include file="ple/menu.tpl"}
                    <!--// Menu -->

                    <div id="tcentre_planA">

                        <font size="4" color="#666666">{$LABEL_TITOLSECCIO}</font>

                        <p>{$LABEL_TC1}</p>
                        <p>{$LABEL_TC2}</p>
                        <p>{$LABEL_TC3}</p>
                        <p>{$LABEL_TC4}</p>

                        <!-- Taula compos -->

                        <table class="tableYves">
                            <tr>
                                <thead>
                                <th>{$LABEL_REPRESENTANT}</th>
                                <th>{$LABEL_REGIDORS}</th>
                                </thead>
                            </tr>

                            {foreach $COMISSIONS_REGIDORS as $item}
                                <tr>
                                    <td>{$item['group']}</td>
                                    <td>{$item['num']}</td>
                                </tr>
                            {/foreach}

                        </table>

                        <!-- //Taula compos -->

                        <!-- Bloc 2 -->

                        <h3>{$LABEL_ACORDS_ADOPTATS}</h3>

                        <ul class="listSenseBullets">

                            {foreach $FILES as $item}
                                <li>
                                    <i class="icon-file-pdf"></i><a href="{$item['file']}" target="{$item['target']}">{$item['name']}</a> 
                                </li>
                            {/foreach}

                        </ul>

                        <!--// Bloc 2 -->

                        <div class="separator"></div>

                        <!--Bloc3-->

                        <h3>{$LABEL_ACORDS_COM_INFO_VIG}</h3>

                        <ul class="listSenseBullets">
                            {foreach $COMISSIONS as $item}
                                <li>
                                    <i class="icon-link"></i><a href="{$item['file']}" target="{$item['target']}">{$item['name']}</a>
                                </li>
                            {/foreach}
                        </ul>

                        </table>

                        <div class="separator"></div>
                        <!--//Bloc3-->

                        <p class="normativa"><strong>{$LABEL_TC5}</strong> {$LABEL_TC50}</p>

                    </div>

                    <div class="separator"></div>
                </div>
            </div>
            </div>
        </div>
        {include file="footer.tpl"}
    </body>
</html>