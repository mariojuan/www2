<html>
<head>
	{include file="head.tpl"}
</head>
<body>

	{include file="header.tpl"}

<div id="page-wrap">
<div id="conttranstotalcos">
	<div id="conttranscos">

		<div id="btranspresen">
			<div id="transimatge">			
			<img src={$image} />			
			</div>
			<div id="transimatgeText">
				<h1>
				{$LABEL_TITOL1}
				</h1>
			</div>
		</div>

		<div class="subtitolinfeco" id="subtitolinfeco2">
            {$LABEL_TCSUB}
        </div>

        <div class="marcback">
                    <a href="/ple/comissions_informatives.php">
                        <i class="icon-angle-double-up"></i>
                    </a>
        </div>

		<!-- Menu -->
                {include file="ple/comissio-informativa/menu.tpl"}
        <!--// Menu -->

		<div id="tcentre_planA">

		<font size="4" color="#666666">{$LABEL_TC0}</font>

		<h3>{$LABEL_TC1}</h3>

				<table>
				<thead>
                  <tr>
                    <th>{$LABEL_FOTO}</th>
                    <th>{$LABEL_NOM}</th>
                    <th>{$LABEL_GRUP}</th>
					<th>{$LABEL_CARREC}</th>
                    </tr>
					</thead>
					<tbody>
                    	{foreach $COMPOSICIO_MEMBRES as $item}
                    	<tr>
                        	<td><a href="{$item['link']}" target="{$item['target']}"><img src="{$item['foto']}" class="avatar"/></a></td>
                        	<td>{$item['name']}</td>
                        	<td>{$item['group']}</td>
							<td>{$item['occupation']}</td>
                   		</tr>
                    	{/foreach}
						</tbody>
                </table>

		<h3>{$LABEL_TCSEC}</h3>
		
		<ul>
		{foreach $COMPOSICIO_SECRETARIS as $item}
			<li>{$item['name']} - {$item['occupation']}</li>
		{/foreach}
		</ul>
		
		
		<h3>{$LABEL_TC2}</h3>

		<ul>
		{foreach $COMPETENCIES as $item}
			<li>{$item['name']}</li>
		{/foreach}
		</ul>

		<h3>{$LABEL_TC3}</h3>
		<p>{$LABEL_TC4}</p>

		</div>
  	</div>
</div>
</div>
{include file="footer.tpl"}

</body>
</html>


