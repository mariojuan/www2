<html>

<head>

    {include file="head.tpl"}

</head>

<body>
    {include file="header.tpl"}

    <div id="page-wrap">
    <div class="contenedor-responsive">
        <div id="conttranstotalcos">
            <div id="conttranscos">
                <div id="btranspresen">
                    <div id="transimatge">
                        <img src="{$image}" class="img-slider-territori"/>
                    </div>
                    <div id="transimatgeText">
                        <h1>
                            {$LABEL_TITOL1}
                            <i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
                        </h1>
                    </div>
                </div>

                <!-- Menu -->
                {include file="ple/menu.tpl"}
            	<!--// Menu -->

                <div id="tcentre_planA">
                
                    <font size="4" color="#666666">{$LABEL_TC0}</font>
                    <p>{$LABEL_TC1}</p>
                    <p>{$LABEL_TC2}</p>
                    <p>{$LABEL_TC3}</p>
                    <p>{$LABEL_TC4}</p>
                    <p>{$LABEL_TC5}</p>

                    <ul>
                        {foreach $COMISSIO as $item}
                        <li>{$item['name']} - {$item['group']} 

                            {if $item['occupation'] != ''}
                                - {$item['occupation']}
                            {/if}

                            {if $item['link'] != ''}
                                <a href="{$item['link']}" target="{$item['target']}"><i class="{$item['icon']}"></i></a>
                            {/if}

                        </li>
                        {/foreach}
                    </ul>

                    <p class="normativa"><strong>{$LABEL_TC6}</strong> {$LABEL_TC60}</p>

                </div>
                <div class="separator"></div>
            </div>
        </div>
        </div>
    </div>

    {include file="footer.tpl"}
</body>

</html>