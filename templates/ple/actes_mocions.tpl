<html>
<head>
	{include file="head.tpl"}
</head>
<body>

	{include file="header.tpl"}

	<div id="page-wrap">

	<div class="contenedor-responsive">
	
		<div id="conttranstotalcos">
			<div id="conttranscos">
				<div id="btranspresen">
					<div id="transimatge">
						<img src="{$image}" class="img-slider-territori"/>
					</div>
					<div id="transimatgeText">
						<h1>
							{$LABEL_TITOL1}
							<i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
						</h1>
					</div>
				</div>

				<!-- Menu -->
                {include file="ple/menu.tpl"}
            	<!--// Menu -->

				<div id="tcentre_planA">

					<!-- Mocions Grups Polítics -->

					<font size="4" color="#666666">{$LABEL_TITOLSECCIO}</font>

					<p>{$LABEL_TC10}</p>

					<h3>{$LABEL_TC1}</h3>

					<p>{$LABEL_TC0}</p>
					

					<h3>{$LABEL_20152019}</h3>
					<p><i>{$LABEL_EXTRACTES}</i></p>

					<ul class="listSenseBullets">
						<li><i class="icon-file-pdf"></i> {$LABEL_TC2015} - <a href="http://www.tortosa.cat/webajt/ajunta/om/dades-plens/mocions-2015-2.pdf">{$LABEL_EXTRACTEMOCIONS}</a></li>
						<li><i class="icon-file-pdf"></i> {$LABEL_TC2016} - <a href="http://www.tortosa.cat/webajt/ajunta/om/dades-plens/mocions-2016.pdf">{$LABEL_EXTRACTEMOCIONS}</a></li>
						<li><i class="icon-file-pdf"></i> {$LABEL_TC2017} - <a href="http://www.tortosa.cat/webajt/ajunta/om/dades-plens/mocions-2017.pdf">{$LABEL_EXTRACTEMOCIONS}</a></li>
					</ul>

					<div class="separator"></div>

					<h3>{$LABEL_20112015}</h3>
					<p><i>{$LABEL_EXTRACTES}</i></p>
					<ul class="listSenseBullets">
						<li><i class="icon-file-pdf"></i> {$LABEL_TC2011} - <a href="http://www.tortosa.cat/webajt/ajunta/om/dades-plens/mocions-2011.pdf">{$LABEL_EXTRACTEMOCIONS}</a></li>
						<li><i class="icon-file-pdf"></i> {$LABEL_TC2012} - <a href="http://www.tortosa.cat/webajt/ajunta/om/dades-plens/mocions-2012.pdf">{$LABEL_EXTRACTEMOCIONS}</a></li>
						<li><i class="icon-file-pdf"></i> {$LABEL_TC2013} - <a href="http://www.tortosa.cat/webajt/ajunta/om/dades-plens/mocions-2013.pdf">{$LABEL_EXTRACTEMOCIONS}</a></li>
						<li><i class="icon-file-pdf"></i> {$LABEL_TC2014} - <a href="http://www.tortosa.cat/webajt/ajunta/om/dades-plens/mocions-2014.pdf">{$LABEL_EXTRACTEMOCIONS}</a></li>
						<li><i class="icon-file-pdf"></i> {$LABEL_TC2015} - <a href="http://www.tortosa.cat/webajt/ajunta/om/dades-plens/mocions-2015-1.pdf">{$LABEL_EXTRACTEMOCIONS}</a></li>
					</ul>

					<p>{$LABEL_TCTRACTAMENT_ESTADISTIC}</p>
					<p><i>{$LABEL_TCINFORMACIO_TABULADA}</i></p>

					<a href="http://www.tortosa.cat/webajt/ajunta/om/dades-plens/estadistiques-grafiques-mocions-2011-2015.pdf">
						<img src="/templates/ple/img/grafic-mocions-mandat-2011-2015.jpg" height="130" width="300" class="marcterme img-responsive-95" />
					</a>

					<div class="separator"></div>

					<!-- Mocions per inicitiava popular -->

					<h3>{$LABEL_TC2}</h3>
					<p>{$LABEL_TC3}</p>
					<ul>
						<li>{$LABEL_TC4}</li>
						<li>{$LABEL_TC5}</li>
						<li>{$LABEL_TC6}</li>
					</ul>
				</div>
			</div>
			</div>
		</div>
	</div>


	{include file="footer.tpl"}
</body>
</html>