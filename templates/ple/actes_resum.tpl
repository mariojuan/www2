<html>
<head>
	{include file="head.tpl"}
</head>
<body>
	{include file="header.tpl"}

	<div id="page-wrap">

	<div class="contenedor-responsive">
	
		<div id="conttranstotalcos">
			<div id="conttranscos">
				<div id="btranspresen">
					<div id="transimatge">
						<img src="{$image}" class="img-slider-territori"/>
					</div>
					<div id="transimatgeText">
						<h1>
							{$LABEL_TITOL1}
							<i id="botoMenu" class="icon-menu-3 button-menu" onclick="displayMenu()"></i>
						</h1>
					</div>
				</div>

				<!-- Menu -->
                {include file="ple/menu.tpl"}
            	<!--// Menu -->

				<div id="tcentre_planA">

					<!-- Competències-->

					<font size="4" color="#666666">{$LABEL_TITOLSECCIO}</font>
					<p>{$LABEL_TC0}</p>
					<p>{$LABEL_TC1}</p>
					<p>{$LABEL_TC2}</p>

					

					<!-- Mandat 2015-2019-->

					
					<h3>{$LABEL_TC4}</h3>
					<p><i>{$LABEL_TCRESUMS}</i></p>
					<ul class="listSenseBullets">
						<li><i class="icon-file-pdf"></i> {$LABEL_TC2015} - <a href="http://www.tortosa.cat/webajt/ajunta/om/dades-plens/resum-per-temes-2015-2.pdf">{$LABEL_TCRESUM_AREES}</a></li>
						<li><i class="icon-file-pdf"></i> {$LABEL_TC2016} - <a href="http://www.tortosa.cat/webajt/ajunta/om/dades-plens/resum-per-temes-2016.pdf">{$LABEL_TCRESUM_AREES}</a></li>
						<li><i class="icon-file-pdf"></i> {$LABEL_TC2017} - <a href="http://www.tortosa.cat/webajt/ajunta/om/dades-plens/resum-per-temes-2017.pdf">{$LABEL_TCRESUM_AREES}</a></li>
					</ul>

					<!-- <p><i>{$LABEL_TCTRACTAMENT_ESTADISTIC}</i></p> -->

					<div class="separator"></div>

					<!-- Mandat 2011-2015 -->

					
					<h3>{$LABEL_TC3}</h3>
					<p><i>{$LABEL_TCRESUMS}</i></p>
					<ul class="listSenseBullets">
						<li><i class="icon-file-pdf"></i> {$LABEL_TC2011} - <a href="http://www.tortosa.cat/webajt/ajunta/om/dades-plens/resum-per-temes-2011.pdf">{$LABEL_TCRESUM_AREES}</a></li>
						<li><i class="icon-file-pdf"></i> {$LABEL_TC2012} - <a href="http://www.tortosa.cat/webajt/ajunta/om/dades-plens/resum-per-temes-2012.pdf">{$LABEL_TCRESUM_AREES}</a></li>
						<li><i class="icon-file-pdf"></i> {$LABEL_TC2013} - <a href="http://www.tortosa.cat/webajt/ajunta/om/dades-plens/resum-per-temes-2013.pdf">{$LABEL_TCRESUM_AREES}</a></li>
						<li><i class="icon-file-pdf"></i> {$LABEL_TC2014} - <a href="http://www.tortosa.cat/webajt/ajunta/om/dades-plens/resum-per-temes-2014.pdf">{$LABEL_TCRESUM_AREES}</a></li>
						<li><i class="icon-file-pdf"></i> {$LABEL_TC2015} - <a href="http://www.tortosa.cat/webajt/ajunta/om/dades-plens/resum-per-temes-2015-1.pdf">{$LABEL_TCRESUM_AREES}</a></li>
					</ul>

					<p><i>{$LABEL_TCTRACTAMENT_ESTADISTIC}</i></p>
					<p>{$LABEL_TCINFORMACIO_TABULADA}</p>

					<a href="http://www.tortosa.cat/webajt/ajunta/om/dades-plens/estadistiques-grafiques-ple-per-temes-mandat-2011-2015.pdf">
						<img src="/templates/ple/img/grafic-mandat-2011-2015.jpg" height="130" width="300" class="marcterme img-responsive-95" />
					</a>

				</div>
			</div>
		</div>
		</div>
	</div>

	{include file="footer.tpl"}

</body>
</html>