<html>

<head>

    {include file="head.tpl"}

</head>

<body>

{include file="header_little.tpl"}

    <div id="page-wrap">
        <div id="conttranstotalcos">
            <div id="conttranscos">
                

                <div id="tcentre_municipi">

                    <div class="mainDetails">
                        
                        <div id="name">

                        <div class="quickFade delayTwo">
                            <img src="{$LABEL_IMG}" class="avatar2" />
                        </div>

                            <h1 class="quickFade delayTwo">{$LABEL_NOM}</h1>
                            <p class="quickFade delayTwo">{$LABEL_NAIXEMENT}</p>
                            <p class="quickFade delayTwo">{$LABEL_PARTIT}</p>

                            <h3 class="quickFade delayTwo">{$LABEL_TC2}</h3>
                            <ul class="quickFade delayTwo">

                            {foreach $CARREC as $item}
                            <li>{$item['name']}</li>
                            {/foreach}
                            
                            </ul>

                            {if  isset($ALTRESCARRECS)}

                            <h3 class="quickFade delayThree">{$LABEL_TC4}</h2>
                                {foreach $ALTRESCARRECS as $item}
                                <ul class="quickFade delayTwo">
                                    <li>
                                        {$item['name']}
                                    </li>
                                </ul>
                                {/foreach}

                                {/if}

                                {if  isset($DOCS)}

                            <h3 class="quickFade delayThree">{$LABEL_DOCS}</h2>
                                {foreach $DOCS as $item}
                                <ul class="quickFade delayTwo">
                                    <li>
                                        <a href="{$item['file']}" target="{$item['target']}">{$item['name']}</a>
                                    </li>
                                </ul>
                                {/foreach}

                                {/if}

                        </div>

                        <div class="clear"></div>
                    </div>

                    <div id="mainArea" class="quickFade delayFive">

                     {if  isset($AREES)}

                        <section>

                            <div class="sectionTitle">
                                <h1>{$LABEL_AMBITS}</h1>
                            </div>

                            <div class="sectionContent">

                                {foreach $AREES as $item}
                                <article>
                                    <p><b>{$item['area']}</b></br>{$item['nom']}</p>
                                </article>
                                {/foreach}

                            </div>

                            <div class="clear"></div>
                        </section>

                        {/if}

                        <section>
                            <article>
                                <div class="sectionTitle">
                                    <h1>{$LABEL_TC5}</h1>
                                </div>

                                <div class="sectionContent">

                                    {foreach $CURRICULUM as $item}

                                    <p>{$item['name']}</p>

                                    {/foreach}

                                </div>
                            </article>
                            <div class="clear"></div>
                        </section>

                        {if  isset($TRAJECTORIA)}
                        
                        <section>

                            <div class="sectionTitle">
                                <h1>{$LABEL_TC6}</h1>
                            </div>

                            <div class="sectionContent">

                                {foreach $TRAJECTORIA as $item}
                                <article>
                                    <p>{$item['name']}</br>{$item['year']}</p>
                                </article>
                                {/foreach}

                            </div>

                            <div class="clear"></div>
                        </section>
                        
                        {/if} 

                        <section>
                            <article>
                                <div class="sectionTitle">
                                    <h1>{$LABEL_TC7}</h1>
                                </div>

                                <div class="sectionContent">

                                    {foreach $RETRI as $item}

                                    <p>{$item['retribucio']}: {$item['import']}, {$item['tregim']}: {$item['regim']}</p>

                                    {/foreach}
                                    

                                </div>
                            </article>
                            <div class="clear"></div>
                        </section>

                    </div>

                </div>
                <div class="separator"></div>
            </div>
        </div>
    </div>

    {include file="footer.tpl"}
</body>

</html>